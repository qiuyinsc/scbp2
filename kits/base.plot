set autoscale
set style data fsteps
set xlabel "Date\nTime"
set timefmt "%H:%M:%S"
set xdata time
set ylabel "Concentration\nmg/l"
set format x "%H:%M:%S"
set grid
set key left

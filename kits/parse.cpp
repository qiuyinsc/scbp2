#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <fstream>

#include <vector>
#include <boost/algorithm/string.hpp>
#include <boost/lexical_cast.hpp>

using namespace std;
using namespace boost;

ifstream input;

typedef vector<string> VECSTR;

namespace {
    int g_totalBuy = 0;
    int g_totalSell = 0;
    int g_trades = 0;
    int g_commission = 0;
    double g_buyCost = 0;
    double g_sellGot = 0;
};

int main(int argc, char* argv[]) {
    if (argc < 2) {
        cout << "help: " << argv[0] << " [filename]\n";
        exit(1);
    }
    input.open(argv[1]);

    while (true) {
        char line[256] = {0};
        bool b = input.getline(line, 256);
        if (!b) {
            break;
        }
        VECSTR vec;
        boost::split(vec, line, boost::is_any_of(" "));
        if (vec.size() < 8) {
            continue;
        }

        g_trades++;

        // parse size       
        int size = boost::lexical_cast<int>(vec[4]);
        if (size > 0) {
            g_totalBuy += size;
        } else {
            g_totalSell += ((-1) * size);
        }

        // commission
        double comm = boost::lexical_cast<double>(vec[7]);
        g_commission += comm;

        // buyCost and sellGot
        double price = boost::lexical_cast<double>(vec[5]);

        if (size > 0) {
            g_buyCost += (price * size + comm);
        } else {
            g_sellGot += ((-1) * price * size - comm);
        }
    }
   
    double net = (g_sellGot - g_buyCost);
    cout << "trades: " << g_trades
         << "\ntotal buy contracts: " << g_totalBuy
         << "\ntotal sell contracts: " << g_totalSell
         << "\ncommission cost: " << g_commission
         << "\nbuy cost: " << g_buyCost
         << "\nsell got: " << g_sellGot
         << "\nnet: " << net
         << "\nnet per trades: " << (net / g_trades)
         << "\nnet per contracts: " << (net / (g_totalBuy + g_totalSell))
         << endl;
    return 0;
}

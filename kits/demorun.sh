#!/bin/sh

rm -rf alphaless.ordertracker.TEST0-20160812.bson
./alphaless --start-date "2016-08-12" --end-date "2016-08-13" --id jorge_test -a TEST0 --strategy-config strategy_config.lua --demo-tag today | tf -dg > ~/tmp.o

rm -rf alphaless.ordertracker.TEST0-20160813.bson
./alphaless --start-date "2016-08-13" --end-date "2016-08-14" --id jorge_test -a TEST0 --strategy-config strategy_config.lua --demo-tag nextday | tf -dg > ~/tmp.o

./demoshow.sh

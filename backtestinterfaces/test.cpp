#include <Python.h>
#include <string>
#include <iostream>

#include <bb/trading/ITrader.h>
#include <bb/trading/Order.h>
#include <bb/core/mktdest.h>
#include <bb/clientcore/IBook.h>
#include "base.h"

using namespace std;

int sendOrder(void* addr, void* instr, int vol, double price, int dir = 0) {
    bb::trading::OrderPtr order(new bb::trading::Order);
    bb::dir_t direction = bb::BUY; // BUY 0; SELL 1
    if (dir != 0) {
        direction = bb::SELL;
    }

    order->orderInfo()
        .setInstrument(*(bb::instrument_t*)instr)
        .setPrice(price)
        .setDir(direction)
        .setTimeInForce(bb::TIF_DAY)
        .setDesiredSize(vol)
        .setMktDest(bb::MKT_SHSE);
    bb::trading::ITrader* trader = (bb::trading::ITrader*)addr;
    unsigned int ret = trader->sendOrder(order);
    if (ret == bb::trading::ITrader::SEND_ORDER_FAILED) {
        LOG_PANIC << "PLACED ORDER FAILED: " << *order << bb::endl;
    }
    bb::trading::OrderInfo& info = order->orderInfo();
    LOG_INFO << "[call by python] OpenOrderInfo: instrument=" << info.getInstrument()
             << ", Price=" << info.getPrice()
             << ", Direction=" << info.getDir()
             << ", TimeInForce=" << info.getTimeInForce()
             << ", DesiredSize=" << info.getDesiredSize()
             << ", MktDest=" << info.getMktDest()
             << bb::endl;
    return ret;
}

//////////////////////////////////////////////////////////
// bind mapping

static PyObject* py_sendOrder(PyObject* self, PyObject* args) {
    // parse parameters
    long addr;
    long instr;
    int vol;
    double price;
    int dir;
    PyArg_ParseTuple(args, "llidi", &addr, &instr, &vol, &price, &dir);

    int ret = sendOrder((void*)addr, (void*)instr, vol, price, dir);
    return Py_BuildValue("i", ret);
}

static PyObject* py_getBook(PyObject* self, PyObject* args) {
    long addr;
    PyArg_ParseTuple(args, "l", &addr);

    bb::IBook* pBook = (bb::IBook*)addr;

    int sz[20];
    double pc[20];
    // cout << "interfacelib info:";
    for (int i = 0; i < 10; i++) {
        const bb::PriceSize& bdps = pBook->getNthSide(i, bb::BID);
        const bb::PriceSize& akps = pBook->getNthSide(i, bb::ASK);

        // set size
        sz[i] = bdps.getSize();
        sz[10 + i] = akps.getSize();

        // set price
        pc[i] = bdps.getPrice();
        pc[10 + i] = akps.getPrice();

        /*
        cout << " mkt" << i
             << ", bid:" << sz[i] << ",  " << pc[i]
             << ", ask:" << sz[10 + i] << ",  " << pc[10 + i];
             */
    }
    // cout << endl;

    return Py_BuildValue("iiiiiiiiiiiiiiiiiiiidddddddddddddddddddd",
                    sz[0], sz[1], sz[2], sz[3], sz[4],
                    sz[5], sz[6], sz[7], sz[8], sz[9],
                    sz[10], sz[11], sz[12], sz[13], sz[14],
                    sz[15], sz[16], sz[17], sz[18], sz[19],
                    pc[0], pc[1], pc[2], pc[3], pc[4],
                    pc[5], pc[6], pc[7], pc[8], pc[9],
                    pc[10], pc[11], pc[12], pc[13], pc[14],
                    pc[15], pc[16], pc[17], pc[18], pc[19]
                    );
}

static PyObject* py_basicSet(PyObject* self, PyObject* args)
{
  char* arg1;
  char* arg2;
  PyArg_ParseTuple(args, "ss", &arg1, &arg2);
  string key(arg1);
  string value(arg2);
  m_userGlobalSpace.set(key, value);

  return Py_BuildValue("s", "");
}

static PyObject* py_basicGet(PyObject* self, PyObject* args)
{
  char* arg;
  PyArg_ParseTuple(args, "s", &arg);
  string key(arg);
  string value = m_userGlobalSpace.get(key);
  return Py_BuildValue("s", value.c_str());
}

static PyObject* py_test(PyObject* self, PyObject* args) {
    return Py_BuildValue("id", 1, 3.14);
}

static PyMethodDef libtest_methods[] = {
  {"sendOrder", py_sendOrder, METH_VARARGS},
  {"getBook", py_getBook, METH_VARARGS},
  {"basicSet", py_basicSet, METH_VARARGS},
  {"basicGet", py_basicGet, METH_VARARGS},
  {"test", py_test, METH_VARARGS},
  {NULL, NULL}
};

extern "C" void initlibtest()
{
  (void) Py_InitModule("libtest", libtest_methods);
}

#include <iostream>
#include <Python.h>

#include "../base.h"

int main(int argc, char** argv)
{
    Py_Initialize();

    if (!Py_IsInitialized())
    {
        return -1;
    }

    PySys_SetArgv(argc, argv);

    if (!PyRun_SimpleString("execfile('./test.py')"))
    {
        return -1;
    }

    Py_Finalize();
    return 0;
}

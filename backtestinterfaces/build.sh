#!/bin/sh

g++ -c -fPIC test.cpp -o test.o -I/usr/include/python2.7/ -I/with/bb/root/include/ -I/usr/include/lua5.1/
g++ -shared -Wl,-soname,libtest.so -o libtest.so  test.o -L/with/bb/root/lib/ -lbbtrading -lboost_system

#python test.py

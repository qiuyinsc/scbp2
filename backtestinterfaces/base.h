#ifndef __BASE_H__
#define __BASE_H__

#include <iostream>
#include <map>

typedef std::map<std::string, std::string> BasicStringMap;

class UserDefineGlobalSpace {
public:
    void set(std::string key, std::string value) {
        BasicStringMap::iterator it = m_data.find(key);
        if (m_data.end() == it) {
            m_data.insert(std::make_pair(key, value));
        } else {
            it->second = value;
        }
    }

    std::string get(std::string key) {
        BasicStringMap::iterator it = m_data.find(key);
        if (m_data.end() != it) {
            return (it->second);
        }

        return std::string("");
    }

private:
    BasicStringMap m_data;
};

UserDefineGlobalSpace m_userGlobalSpace;

#endif

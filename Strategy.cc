#include "Strategy.h"

// remove io stream when cout (printfs) go away
#include <iostream>
#include <fstream>
#include <iomanip>
#include <string>
#include <map>

#include <bb/core/Log.h>
#include <bb/core/env.h>
#include <bb/core/Error.h>
#include <bb/core/EFeedType.h>
#include <bb/core/EquitySecurityInfo.h>
#include <bb/core/messages_autogen.h>
#include <bb/core/usermsg.h>
#include <bb/core/ptime.h>
#include <bb/core/CommoditiesSpecifications.h>

#include <bb/db/TradingCalendar.h>
#include <bb/db/DayInfo.h>

#include <bb/clientcore/IBook.h>
#include <bb/clientcore/BookBuilder.h>
#include <bb/clientcore/IClientTimer.h>
#include <bb/clientcore/PriceProvider.h>
#include <bb/clientcore/BbL1TickProvider.h>
#include <bb/clientcore/EventDist.h>
#include <bb/trading/IssuedOrderTracker.h>
#include <bb/trading/TradingContext.h>

#include <bb/trading/Trader.h>
#include <bb/trading/IssuedOrderTracker.h>
#include <bb/trading/trading.h>
#include <bb/trading/OrderPositionTracker.h>
#include <bb/trading/InstrumentContext.h>
#include <bb/trading/Order.h>
#include <bb/trading/IPositionProvider.h>
#include <bb/trading/PnLProvider.h>
#include <fstream>
#include <boost/shared_ptr.hpp>
#include <boost/algorithm/string.hpp>
#include <boost/lexical_cast.hpp>

#include </usr/include/python2.7/Python.h>

#include "SSETickProvider.h"
#include "SSEBook.h"

using namespace bb;
using namespace std;

namespace {
string positionCacheFile("instrument_info.dat");
string g_today(""); // format: YYYY-MM-DD

struct DateTime {
    int year;
    int month;
    int day;
    int hour;
    int min;
    int sec;
};
typedef boost::shared_ptr<DateTime> DateTimePtr;

struct OrderInstruction {
    DateTimePtr dt;
    instrument_t ins;
    dir_t dir;
    int size;
    double price;

    void output() {
        cout << "OrderInstruction: tv=?"
             <<  ", instrument=" << ins.toString() 
             <<  ", direction=" << dir
             <<  ", size=" << size
             <<  ", price=" << price
             << std::endl;
    }
};

typedef boost::shared_ptr<OrderInstruction> OrderInstructionPtr;
typedef vector<OrderInstructionPtr> OrderInstructionVec;
OrderInstructionVec g_oies;

typedef vector<string> VECSTR;

struct InstrumentInfo {
    InstrumentInfo(int p, double t)
        : position(p)
        , turnover(t) {}

    int position;
    double turnover;
};
typedef boost::shared_ptr<InstrumentInfo> InstrumentInfoPtr;
typedef map<string, InstrumentInfoPtr> InstrumentInfoMap;
InstrumentInfoMap g_iies;
InstrumentInfoMap g_iies_baseline;

void updateInsInfo(string ins, dir_t dir, int size, double price) {
    if (dir == bb::SELL) {
        size *= (-1);
    }

    InstrumentInfoMap::iterator it = g_iies.find(ins);
    if (it != g_iies.end()) {
        InstrumentInfoPtr ii = it->second;
        ii->position += size;
        ii->turnover += (size * price);
    } else {
        g_iies.insert(make_pair(ins, InstrumentInfoPtr(new InstrumentInfo(size, (size * price)))));
    }
}

void outputInsInfo() {
    ofstream ofs(positionCacheFile.c_str(), ofstream::out);
    InstrumentInfoMap::iterator it = g_iies.begin();
    ostringstream os;
    while (it != g_iies.end()) {
        InstrumentInfoPtr ii = it->second;
        os   << g_today
             << " " << it->first
             << " " << ii->position
             << " " << ii->turnover
             << std::fixed << std::endl;
        it++;
    }
    cout << os.str() << std::endl;
    ofs << os.str();
    ofs.close();
}

void gotCommandList(string filename) {
    g_oies.clear();
    ifstream input;
    input.open(filename.c_str());

    try {
        while (true) {
            char line[256] = {0};
            if (!(input.getline(line, 256))) {
                break;
            }

            VECSTR vec;
            boost::split(vec, line, boost::is_any_of(" "));
            if (vec.size() < 6) {
                continue;
            }

            // parse datetime
            DateTimePtr dt(new DateTime());
            {
                VECSTR vv;
                boost::split(vv, vec[0], boost::is_any_of("_"));
                if (vv.size() < 3) {
                    continue;
                }
                VECSTR vvtm;
                boost::split(vvtm, vec[1], boost::is_any_of(":"));
                if (vvtm.size() < 3) {
                    continue;
                }
                dt->year = boost::lexical_cast<int>(vv[0]);
                dt->month = boost::lexical_cast<int>(vv[1]);
                dt->day = boost::lexical_cast<int>(vv[2]);
                dt->hour = boost::lexical_cast<int>(vvtm[0]);
                dt->min = boost::lexical_cast<int>(vvtm[1]);
                dt->sec = boost::lexical_cast<int>(vvtm[2]);

                /// init the today string by the way
                {
                    if (g_today.empty()) {
                        ostringstream os;
                        os << dt->year << "-"
                           << dt->month << "-"
                           << dt->day;
                        g_today = os.str();
                    }
                }
            }

            OrderInstructionPtr oi(new OrderInstruction());
            oi->dt = dt;
            oi->ins = instrument_t::fromString(string("SHSE_") + vec[2]);
            if (string("buy") == vec[3]) {
                oi->dir = bb::BUY;
            } else {
                oi->dir = bb::SELL;
            }
            oi->size = boost::lexical_cast<int>(vec[4]);
            oi->price = boost::lexical_cast<double>(vec[5]);

            g_oies.push_back(oi);
            oi->output();
        }
    } catch (...) {
        cout << "gotCommandList confront errors\n";
    }

    input.close();
}

void loadBaseline() {
    ifstream input;
    input.open(positionCacheFile.c_str());
    try {
        while (true) {
            char line[256] = {0};
            if (!(input.getline(line, 256))) {
                break;
            }

            VECSTR vec;
            boost::split(vec, line, boost::is_any_of(" "));
            if (vec.size() < 4) {
                continue;
            }

            InstrumentInfoPtr ins(new InstrumentInfo(boost::lexical_cast<int>(vec[2]), boost::lexical_cast<double>(vec[3])));

            // initialize the total position map
            g_iies_baseline.insert(make_pair(vec[1], ins));
            g_iies.insert(make_pair(vec[1], ins));
        }
    } catch (...) {
        cout << "loadBaseline confront errors\n";
    }
    input.close();
}
};

namespace alphaless {

/// This is an implementation of IBookListener which prints a MarketLevel on every update.
// TradingContext
// virtual const InstrumentContext& getInstrumentContext( const instrument_t& instr );
Strategy::Strategy(const InstrVector& instruments,
                    const bb::trading::TradingContextPtr& tradingContext,
                    const StrategySettings &strategySettings,
                    const bb::source_t& source,
                    std::string& csvfile,
                    bool baseline
                    ) 
    : m_instrs (instruments)
    , m_strategySettings (strategySettings)
    , m_market(bb::str2mktdest(strategySettings.market.c_str()))
    , m_tradingContext(tradingContext)
    , m_clientContext(tradingContext->getClientContext())
    , m_clockMonitor(m_clientContext->getClockMonitor())
    , m_timer(m_clientContext->getClientTimer())
    , m_baseline(baseline)
{
    // cerr << "m_baseline=" << m_baseline << std::endl;
    bb::date_t sd = bb::date_t(m_clientContext->getStartTimeval());
    m_startTime = bb::timeval_t::make_time(sd.year(), sd.month(), sd.day(), m_strategySettings.start_hour, m_strategySettings.start_minute, m_strategySettings.start_second);
    // sd = bb::date_t(m_clientContext->getEndTimeval());
    m_endTime = bb::timeval_t::make_time(sd.year(), sd.month(), sd.day(), m_strategySettings.end_hour, m_strategySettings.end_minute, m_strategySettings.end_second);
    m_entryTime = bb::timeval_t::make_time(sd.year(), sd.month(), sd.day(), 8, 0, 0);
    m_exitTime = bb::timeval_t::make_time(sd.year(), sd.month(), sd.day(), 11, 0, 0);
    m_trade = false;  // gets set to true after 9:30.  It is also toggleable by sending a command
    m_entryOrdersSent = false;
    m_exitOrdersSent = false;

    // Athena's API uses callbacks.  In this next section we wire up the callbacks that will fire when events happen (Book updates, Tick Updates, Orders, etc.)
    // for each instrument create a book listener
    bb::EventDistributorPtr spED = m_clientContext->getEventDistributor();

    // Process each instrument
    BOOST_FOREACH(const InstrVector::value_type& instr, m_instrs) {
        // Create a book for the instrument
        // bb::IBookPtr spBook = m_tradingContext->getInstrumentContext(instr)->getBook();
        bb::IBookPtr spBook(new SseBook(m_clientContext, instr, source, string("desc")));
        // LOG_INFO << "book: " << spBook->getInstrument() << " ok: " << spBook->isOK() << bb::endl;
        // add this object as a listener. Everytimt the book changes the onBookChanged function
        // of this object will be called with a reference to the book and the level of the book that changed
        spBook->addBookListener(this);
        m_books.push_back(spBook);
        // Keep the book for later
        m_Books.insert(BookMap::value_type(instr, spBook));

        // Create a price provider.  By default this is a midpoint price provider where the price is
        // the midpoint between the best ask and the best bid.  The program can add a listener that gets
        // called whenever the midpoint price changes, or it can query the price on demand
        bb::IPriceProviderCPtr pp = m_tradingContext->getInstrumentContext(instr)->getPriceProvider();
	
        // add the litener
        pp->addPriceListener(m_priceSub[ instr ], boost::bind(&Strategy::onPriceChanged, this, _1));

        // Create a tick provider.  for the instrument
        // bb::ITickProviderPtr tp = m_tradingContext->getInstrumentContext(instr)->getTickProvider();
        bb::ITickProviderPtr tp(new SSETickProvider(m_clientContext, instr, source, string("desc")));
        // add this object as a listener to the tick provider.  On every tick received for this instrument
        // the onTickReceived function of this object will be called. In addition the onOpenTick function
        // is called when the openning tick is received.
        tp->addTickListener(this);
        m_tps.push_back(tp);

        // Get the issued order tracker.  This object is used to track all orders for a given
        // instrument.  Add thuis object as an onStatusChange listener.  This will call the
        // onStatusChanged function everytime an order's status changes
        m_tradingContext->getIssuedOrderTracker(instr)->addStatusChangeListener(this);

        // preps PP to provide position info - Greybox functionality.
        // Fills that occur outside of the context of this strategy but on the strategy account will fire this function
        m_tradingContext->getPositionProvider(instr)->addPositionListener(m_posSub[ instr ], this);

    }

    if (!m_baseline) {
        loadBaseline();
    }
    
    // parse and build the inner strategy commands list from csv file
    gotCommandList(csvfile); 

    // create a timer that wakes the strategy up one hour before the end of market
    for (size_t i = 0; i < g_oies.size(); i++) {
        OrderInstructionPtr oi = g_oies[i];
        DateTimePtr dt = oi->dt;
        bb::Subscription sub;
        m_timer->schedule(sub,
                           boost::bind(&Strategy::oneShotTimerCB, this, oi->ins, oi->dir, oi->size, oi->price),
                           bb::timeval_t::make_time(dt->year, dt->month, dt->day, dt->hour, dt->min, dt->sec));
        m_subVec.push_back(sub);
    }
    // schedule a callback once an hour until the end of market
    bb::Subscription sub;
    m_timer->schedulePeriodic(sub,
                               boost::bind(&Strategy::hourlyTimerPeriodicCB, this),
                               bb::timeval_t::make_time(sd.year(), sd.month(), sd.day(), m_strategySettings.end_hour, m_strategySettings.end_minute, m_strategySettings.end_second),
                               boost::posix_time::hours(1),
                               boost::bind(&Strategy::hourlyTimerDoneCB, this));
    m_subVec.push_back(sub);

    // handle some common exit signals for graceful shutdown
    m_clientContext->sigAction(SIGINT, boost::bind(&alphaless::Strategy::shutdown, this));
    m_clientContext->sigAction(SIGTERM, boost::bind(&alphaless::Strategy::shutdown, this));
    m_tickPrices.clear();
}

Strategy::~Strategy()
{
    BOOST_FOREACH(BookMap::value_type bookPair, m_Books) {
        bookPair.second->removeBookListener(this);
    }

    outputInsInfo();
    LOG_INFO<<"Destructing Startegy"<<bb::endl;
}

void Strategy::subscribeUserMessage()
{
    bb::MsgHandlerPtr handler = bb::MsgHandler::createMType<bb::UserMessageMsg>(
    bb::source_t::make_auto(bb::SRC_UMSG),
    m_tradingContext->getEventDistributor(),
    boost::bind(&Strategy::handleUserMessage, this, _1),
    bb::trading::PRIORITY_TRADING_DEFAULT);
    m_msgHandlers.push_back(handler);
}

void Strategy::oneShotTimerCB(instrument_t ins, dir_t dir, int size, double price) {
    if (!m_baseline) {
        InstrumentInfoMap::iterator it = g_iies_baseline.find(ins.toString());
        if (it == g_iies_baseline.end()) {
            if (dir == bb::SELL) {
                return;
            }
        } else {
            InstrumentInfoPtr ii = it->second;
            if (dir == bb::SELL) {
                size = min(ii->position, size);
            }
        }
    }

    bb::trading::ITraderPtr trader = m_tradingContext->getTrader(ins);

    bb::trading::OrderPtr order(new bb::trading::Order);
    order->orderInfo()
            .setInstrument(ins)
            .setPrice(price)
            .setDir(dir)
            .setTimeInForce(bb::TIF_IMMEDIATE_OR_CANCEL)
            .setDesiredSize(size)
            .setMktDest(m_market);

    unsigned int orderResult = trader->sendOrder(order);
    if (orderResult == bb::trading::ITrader::SEND_ORDER_FAILED) {
        LOG_PANIC << "PLACED ORDER FAILED: " << *order << bb::endl;
    }
}

void Strategy::hourlyTimerPeriodicCB(){
    // check active orders
    BOOST_FOREACH(const InstrVector::value_type& instr, m_instrs) {
        // Issuedordertracker
        bb::trading::IssuedOrderTrackerPtr orderTracker = m_tradingContext->getIssuedOrderTracker(instr);
        bb::trading::IssuedOrderTracker::OrderMap::const_iterator it = orderTracker->getActiveOrdersBegin(BID);
        bb::trading::IssuedOrderTracker::OrderMap::const_iterator itEnd = orderTracker->getActiveOrdersEnd(BID);
        while (it != itEnd) {
            bb::trading::OrderPtr activeOrder = it->second;
            bb::trading::ITraderPtr trader = m_tradingContext->getTrader(instr);
            bool ret = trader->sendCancel(activeOrder);
            if (!ret) {
                LOG_PANIC << "Try to cancel order"/* << activeOrder->issuedInfo.getOrderid() */<< " failed" << std::endl;
            }
            it++;
        }
    }
}

void Strategy::hourlyTimerDoneCB(){
    LOG_INFO << "The periodic timer has completed at: "
             << bb::date_t(m_clientContext->getTime())
             << bb::endl;
}

void Strategy::onTickReceived(const bb::ITickProvider* tp, const bb::TradeTick& tick) {
    string curid = tp->getInstrument().toString();
    
    // update anyway
    updateMktPrice(curid, tick.getPrice());

    PriceMap::iterator it = m_tickPrices.find(curid);
    if (it == m_tickPrices.end()) {
        OpenOrderInfoByInstrumentPtr openOrder(new OpenOrderInfoByInstrument(tick.getPrice()));
        m_tickPrices.insert(make_pair(curid, openOrder));
    } else {
        it->second->price = tick.getPrice();
    }

    if (tick.getMsgTime().after(m_endTime)) {
        // after end_time stop trading
        m_trade = false;
    } else {
        if (!m_trade) {
            if (tick.getMsgTime().after(m_startTime)) {
                // after start_time, start trading
                m_trade = true;
            }
        }
    }
}

void Strategy::onPriceChanged(const bb::IPriceProvider& priceProvider) {
}

/// Prints the top of book whenever the best market changes.
/// From clientcore/IBook.h:
/// Invoked when the subscribed Book changes.
/// The levelChanged entries are negative if there is no change, or a 0-based depth.
/// This depth is a minimum -- there could be multiple deeper levels that changed
/// since the last onBookChanged.

void Strategy::onBookChanged(const bb::IBook* pBook, const bb::Msg* pMsg,
                              int32_t bidLevelChanged, int32_t askLevelChanged) {
                              return;
    if (m_trade) {
        if( bidLevelChanged == 0 || askLevelChanged == 0 )
        {
            bb::MarketLevel ml = getBestMarket( *pBook );

            std::cout << "L1 update --"
                      << " time:" << pBook->getLastChangeTime()
                      << " instr:" << pBook->getInstrument()
                      << " bid_sz:" << ml.getSize( bb::BID )
                      << " bid_px:" << ml.getPrice( bb::BID )
                      << " ask_px:" << ml.getPrice( bb::ASK )
                      << " ask_sz:" << ml.getSize( bb::ASK )
                      << " mid_px:" << pBook->getMidPrice()
                      << std::endl;
        }
    }
}

// Invoked when the subscribed Book is flushed.
void Strategy::onBookFlushed(const bb::IBook* pBook, const bb::Msg* pMsg) { }

// This function is called whenever a position changes.  Note that this will
// becalled as a result of orders issued by this object as well as orders
// issued by out-of-band traders for the same account, such as when a manual trade is done.
void Strategy::onPositionUpdated(bb::trading::IPositionProvider* pos) {
    LOG_INFO << "onPositionUpdated: "
             << "account=" << pos->getAcct()
             << ", instrument=" << pos->getInstrument()
             << ", AbsolutePosition=" << pos->getAbsolutePosition()
             << ", PendingPosition=" << pos->getPendingPosition()
             << bb::endl;

    m_posMap[ pos->getInstrument() ]  = pos->getEffectivePosition();
}

//
// This function is called whenever the status of an order changes.
// The possible values for the order status are:
//    STAT_NEW  - the order has been sent to the trade daemon
//    STAT_TRANSIT - the order has been sent to the market
//    STAT_OPEN    - the order has been confirmed as open by the govermnet
//    STAT_DONE    - The order is done.  Check the done reason to determine why it is done

void Strategy::onOrderStatusChange(const bb::trading::OrderPtr& order, const bb::trading::IOrderStatusListener::ChangeFlags& flags) {
    switch(order->issuedInfo().getOrderStatus()){
    case bb::STAT_NEW:
    case bb::STAT_TRANSIT:
    case bb::STAT_OPEN: {
            //ok now we can cancel it, since its been acknowledged as open by the market
            LOG_INFO << "onOrderStatusChange [INFO] id=" << order->issuedInfo().getOrderid()
                     << ", status=" << order->issuedInfo().getOrderStatus() << bb::endl;
        }
        break;
    case bb::STAT_DONE:
        if (order->issuedInfo().getDoneReason() != bb::R_FILL) {
            if(order->issuedInfo().getDoneReason() == bb::R_CANCEL) {
                LOG_INFO << "onOrderStatusChange [INFO] order is cancelled id=" << order->issuedInfo().getOrderid()
                         << bb::endl;
            } else {
                // R_FILL is a completed order.  IF it's STAT_DONE and not R_FILL, something went wrong...
                    LOG_WARN << "Failed Order "
                             << order->issuedInfo().getDoneReason()
                             << " oid: "
                             << order->issuedInfo().getOrderid()
                             << bb::endl; 
            }
        } else {
            // OrderInfo
            LOG_INFO << "onOrderStatusChange: [FILL ORDER] status=STAT_DONE, id=" << order->issuedInfo().getOrderid()
                     << ", instrument="<<  order->orderInfo().getInstrument()
                     << ", LastFillPrice="<<  order->issuedInfo().getLastFillPrice()
                     << ", LastFillQty="<<  order->issuedInfo().getLastFillQty()
                     << bb::endl;
        }
        break;
    default:
        LOG_ERROR << "Encountered an unexpected order status"
                  << order->issuedInfo().getOrderStatus()
                  << bb::endl;
    }
}

void Strategy::onFill(const bb::trading::FillInfo& info) {
    const bb::trading::OrderPtr& order = info.getOrder();

    // this fires when a fill occurs
    LOG_INFO << "onFill: "
             <<"instrument=" << order->orderInfo().getInstrument()
             << ", desiredSize=" << order->orderInfo().getDesiredSize()
             << ", direction=" << order->orderInfo().getDir()
             << ", lastFillQty= " << order->issuedInfo().getLastFillQty()
             << ", lastFillPrice=" << order->issuedInfo().getLastFillPrice()
             << bb::endl;
    updateInsInfo(order->orderInfo().getInstrument().toString(),
                  order->orderInfo().getDir(),
                  order->issuedInfo().getLastFillQty(),
                  order->issuedInfo().getLastFillPrice());
    // if order is complete STAT_DONE will fire via onOrderStatusChange

    // cancel the leave qty if any
    if (order->orderInfo().getDesiredSize() > order->issuedInfo().getLastFillQty())
    {
        bb::trading::ITraderPtr trader = m_tradingContext->getTrader(order->orderInfo().getInstrument());	
        bool cancelResult = trader->sendCancel(order);
        if (!cancelResult) {
            LOG_PANIC << "Cancel ORDER FAILED: " << *order << bb::endl;
        }
    }    
}

//
// This function is called when the openning tick for the market is received.
//
void Strategy::onOpenTick(const bb::ITickProvider* tp, const bb::TradeTick& tick) {
    LOG_INFO << "Received an opening Tick for "
             << tp->getInstrument()
             << " "
             << tick.getPrice()
             << bb::endl;

    // this function is useful if you're interested in knowing when a stock is OPEN by the listed exchange perspective.
    onTickReceived(tp, tick);
}

void Strategy::shutdown() {
    static const int shutdown_delay_secs = 3; // wait e.g. for TD responses to cancel requests

    if (shutdownTimerSub)
        LOG_WARN << "Alphaless got a second shutdown request; shutting down now" << bb::endl;
    else
        LOG_WARN << "Alphaless is shutting down in " << shutdown_delay_secs << " seconds" << bb::endl;

    if (shutdownTimerSub) // we're being told to shut down a second time; don't delay any longer
    {
        m_clientContext->getMStreamManager()->exit();
    }
    else // tell the MStreamManager to exit in the near future
    {
        m_clientContext->getClientTimer()->schedule(
                shutdownTimerSub,
                boost::bind(&bb::IMStreamManager::exit, m_clientContext->getMStreamManager()),
                m_clientContext->getTime() + shutdown_delay_secs);
    }
}

void Strategy::handleUserMessage(const bb::UserMessageMsg& userMsg) {
    // these are messages that can be sent into the strategy via an external program to manage the state of the strategy.
    // examples:  HALT Trading, Resume trading, Flatten all positions.
    // strategy-specific commands can be added here as well.
    if(userMsg.getAccount() == m_tradingContext->getAccount()
        || userMsg.getAccount() == bb::ACCT_ALL)
    {
        switch(userMsg.getCommand())
        {
        case bb::UMSGC_SHUTDOWN:
            m_tradingContext->exit();
            break;
        case bb::UMSGC_GET_FLAT:
            LOG_WARN << "FLATTENING ALL POSITIONS!!!" << bb::endl;
            // flattening code goes here
            break;
        case bb::UMSGC_SEND_NOTHING:
            LOG_WARN << "Received SEND_NOTHING command, initiating trade freeze" << bb::endl;
            m_trade = true;
            break;
        case bb::UMSGC_ALLOW_SENDING:
            LOG_WARN << "Received ALLOW_SENDING command, resuming trading" << bb::endl;
            m_trade = false;
            break;
        default:
            LOG_WARN << "Unhandled user message command: " << userMsg.getCommand() << bb::endl;
            break;
        }
    }

    // user message handling happens here
    LOG_INFO << "USER MESSAGE" << bb::endl;
}

}// namespace alphaless

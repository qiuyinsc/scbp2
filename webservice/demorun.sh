#!/bin/bash

cd strategy/; ./genStra.sh content.dat; cd ..
rm -rf bookChanged.py
cp strategy/bookChanged.py .

# get config info
startDate=""
endDate=""
instruments=""
capitalBase=""

userStrategy="strategy/content.dat"
while IFS='' read -r line; do
    if [[ $line == *=* ]]; then
        line=`echo ${line//[[:blank:]]/}`
        IFS='=' read -r -a arr <<< "$line"
        if [[ ${arr[0]} == "start" ]]; then
            startDate=`echo ${arr[1]}`;
            startDate=`echo "${startDate//\"}"`
        elif [[ ${arr[0]} == "end" ]]; then
            endDate=`echo ${arr[1]}`
            endDate=`echo "${endDate//\"}"`
        elif [[ ${arr[0]} == "instruments" ]]; then
            instruments=`echo ${arr[1]}`
        elif [[ ${arr[0]} == "capital_base" ]]; then
            capitalBase=`echo ${arr[1]}`
        fi
    fi
done < "$userStrategy"

#echo "instruments="$instruments
#echo "capitalBase="$capitalBase

echo "start to demorun..."

# construct config
configHead="strategyConfigHead.lua"
configTail="strategyConfigTail.lua"
strategyConfig="strategy_config.lua"
rm -rf $strategyConfig 

cat $configHead >> $strategyConfig
echo "    instruments = "$instruments"," >> $strategyConfig
cat $configTail >> $strategyConfig

./findDateList.sh ${startDate//-} ${endDate//-}

profitAll="profitAll.o"
rm -rf $profitAll

dateListOutput="dateParameterPairsList.dat"
while IFS='' read -r line; do
    IFS=' ' read -r -a arr <<< "$line"
    param1=${arr[0]}
    param2=${arr[1]}
    bsonFile="alphaless.ordertracker.TEST0-${param1//-}"".bson"
    rm -rf $bsonFile

    logOutput="output_"$param1".o"
    ./alphaless --start-date "$param1" --end-date "$param2" --id jorge_test -a TEST0 --strategy-config strategy_config.lua | tf -dg > $logOutput

    # parse result and do some merge
    ./demoshow.sh $bsonFile $param1
    cat day1Profit.dat >> $profitAll
done < "$dateListOutput"


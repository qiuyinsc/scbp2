strategy_config = { --this object is parsed into a StrategyConfig object

    market = "MKT_SHSE",
    feed_orig = "ORIG_SFIT",
    feed_dest = "DEST_SFIT",
    feed_type = "SRC_SHSE",
    trade_server = "auto",
    instruments = {"SHSE_600105"},
    -- other options can be added as needed
    start_hour = 8,
    start_minute = 0,
    start_second = 0,
    end_hour = 15,
    end_minute = 15,
    end_second = 0,

    shares = 100
}

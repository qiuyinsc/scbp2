#ifndef BB_CLIENTCORE_SSETICKPROVIDER_H
#define BB_CLIENTCORE_SSETICKPROVIDER_H

#include <boost/optional.hpp>

#include <bb/clientcore/TickProvider.h>

namespace bb {

class SseHistoricalTickMsg;

BB_FWD_DECLARE_SHARED_PTR(MsgHandler);

class SSETickProvider
    : public ITradeSplitter
    , public TickProviderImpl
{
public:
    SSETickProvider(const ClientContextPtr& context, const instrument_t& instr, source_t source, const std::string& desc);

    virtual ~SSETickProvider() {}

    virtual bool isLastTickOK() const { return m_bTickReceived; }
    double getNotionalTurnover() const { return m_notionalTurnover; }
    double getNotionalAvgPx() const { return m_notionalAvgPx; }
    int32_t getOpenInterest() const { return m_openInterest; }
    boost::optional<double> getAvgPxInLastTick(double lot_size) const;

    virtual const TradeTick* getAboveVwapTradeTick() const
    {   return &m_aboveVwap; }

    virtual const TradeTick* getBelowVwapTradeTick() const
    {   return &m_belowVwap; }

private:
    // Event handlers
    void onSSEQdMsg(const SseHistoricalTickMsg& msg);

private:
    double m_tickSize;
    double m_contractSize;
    bool m_bInitialized;
    bool m_bTickReceived;
    int64_t m_lastSerial;
    timeval_t m_lastMsgTimeval;
    MsgHandlerPtr m_subSSEQdMsg;

    double m_notionalTurnover;
    double m_notionalAvgPx;
    int32_t m_openInterest;

    double m_lastNotionalTurnover;
    uint32_t m_lastTotalVolume;
    TradeTick m_aboveVwap;
    TradeTick m_belowVwap;
};

BB_DECLARE_SHARED_PTR(SSETickProvider);
};

#endif // BB_CLIENTCORE_SSETICKPROVIDER_H

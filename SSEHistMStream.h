#ifndef BB_IO_SSE_HISTSTREAM_H
#define BB_IO_SSE_HISTSTREAM_H 

#include <fstream>
#include <queue>
#include <string>

#include <bb/core/smart_ptr.h>
#include <bb/core/MStream.h>
#include <bb/core/messages_autogen.h>
#include <bb/io/CFile.h>
#include <bb/io/ByteSource.h>

#include "SSEMsg.h"

namespace bb {

enum SSEMsgCategory
{
    SMT_TICK,
    SMT_AUCTION,
    SMT_SNAP,
    SMT_DAY_MIN,
    SMT_SNAP2,
    SMT_COUNT,
};

typedef boost::shared_ptr<SSEFileIndex> SSEFileIndexPtr;
typedef std::queue<SSEFileIndexPtr> IndexQueue;
const int  MAX_BUF_LEN = (1024 * 1024 * 16);

class SSEHistMStream : public HistMStream
{
public:
    SSEHistMStream(mtype_t&, std::string id, std::string dayStr, std::string dataPath);

public:
    virtual const Msg* next();

private:
    int initialize();
    void compactBuffer();
    void updateBuffer();
    void fillTickMsg(const SSEFileIndex& index);
    void fillAuctionMsg(const SSEFileIndex& index);
    void fillSnapMsgL1(const SSEFileIndex& index);
    void fillSnapMsgL2(const SSEFileIndex& index);
    void fillDayMinMsg(const SSEFileIndex& index);

private:
    bool m_initialized;
    SseHistoricalTickMsg m_tick;
    SseHistoricalAuctionMsg m_auction;
    SseHistoricalSnap1Msg m_snap;
    SseHistoricalSnap2Part1Msg m_snap2Part1;
    SseHistoricalSnap2Part2Msg m_snap2Part2;
    SseHistoricalDayMinMsg m_day_min;
    Msg* m_msgs[SMT_COUNT];

    size_t m_remain;
    char m_buffer[MAX_BUF_LEN];
    int m_offset;
    SSEMsgCategory m_msgCgy;
    IndexQueue m_indexes;
    int m_indexPosition;
    std::ifstream m_in;
    size_t SSE_THIS_MSG_LEN;
    std::string m_desiredID;
    uint64_t m_CodeBeginPosition;
    uint64_t m_CodeEndPosition;
};

BB_DECLARE_SHARED_PTR(SSEHistMStream);

}
#endif

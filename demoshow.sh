#!/bin/sh

# prepare raw result
tradeListFile="trade.list"
rm -rf $tradeListFile

day1Summary="day1.summary"
day2Summary="day2.summary"

rm -rf $day1Summary
rm -rf $day2Summary

/home/yqiu/ipnl alphaless.ordertracker.TEST0-20160812.bson | sed 's/\s\s*/ /g' > $day1Summary
/home/yqiu/ipnl alphaless.ordertracker.TEST0-20160813.bson | sed 's/\s\s*/ /g' > $day2Summary

echo "Day1(buy):" >> $tradeListFile
more $day1Summary | cut -d ' ' -f 1-7,8,9 | grep -i 'SHSE_' >> $tradeListFile

echo "" >> $tradeListFile
echo "Day2(sell):" >> $tradeListFile
more $day2Summary | cut -d ' ' -f 1-7,8,9 | grep -i 'SHSE_' >> $tradeListFile

echo "*** Trade info list [including two days] ***"
more $tradeListFile

echo "\n=== Summary ==="

./parse $tradeListFile

# cancel orders number
echo "\n=== order cancell info ==="
echo "Day1:"
more $day1Summary | grep -i 'Cancels Attempts:'
more $day1Summary | grep -i 'Cancels Confirms:'

echo "\nDay2:"
more $day2Summary | grep -i 'Cancels Attempts:'
more $day2Summary | grep -i 'Cancels Confirms:'

# try to plot the profit
day1BuyProfitData="day1Profit.dat"
day2SellProfitData="day2Profit.dat"
rm -rf $day1BuyProfitData
rm -rf $day2SellProfitData

more $day1Summary | grep -i 'SHSE_' | cut -d ' ' -f 3,8 > $day1BuyProfitData 
more $day2Summary | grep -i 'SHSE_' | cut -d ' ' -f 3,8 > $day2SellProfitData

# create the plot script
day1Plot="day1.plot"
day2Plot="day2.plot"
rm -rf $day1Plot
rm -rf $day2Plot

# day1
echo "set terminal png" >> $day1Plot
echo "set title \"day1 buy\"" >> $day1Plot
echo "set output \"day1Buy.png\"" >> $day1Plot
more base.plot >> $day1Plot
echo "set xrange [ \"13:00:50\":\"13:01:20\" ]" >> $day1Plot
echo "set yrange [-1:5]" >> $day1Plot
echo "plot '$day1BuyProfitData' usi 1:2 with linespoints" >> $day1Plot

# day2
echo "set terminal png" >> $day2Plot
echo "set title \"day2 sell\"" >> $day2Plot
echo "set output \"day2Sell.png\"" >> $day2Plot
more base.plot >> $day2Plot
echo "set xrange [ \"09:43:09\":\"09:43:40\" ]" >> $day2Plot
echo "set yrange [-11:3]" >> $day2Plot
echo "plot '$day2SellProfitData' usi 1:2 with linespoints" >> $day2Plot

# generate the pics
gnuplot $day1Plot &
gnuplot $day2Plot &

#!/bin/sh

# set env var
source ~/.bashrc
export SSE_HISTORY_DATA_PATH="/home/bshen/data/sse/"

rm -rf alphaless.ordertracker.TEST0-20160812.bson

./alphaless --start-date "2016-08-12" --end-date "2016-08-13" --id jorge_test -a TEST0 --strategy-config strategy_config.lua | tf -dg  > ~/tmp.o

#./demoshow.sh

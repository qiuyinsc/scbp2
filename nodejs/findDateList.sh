#!/bin/bash

dateListOutput="dateParameterPairsList.dat"
rm -rf $dateListOutput

indexFile="$SSE_HISTORY_DATA_PATH""index"
while IFS='' read -r line; do
    if (($line >= $1));then
        if (($line < $2));then
            param1=$(date +%Y-%m-%d -d "$line + 0 day")
            param2=$(date +%Y-%m-%d -d "$line + 1 day")

            echo "$param1 $param2" >> $dateListOutput
        fi
    fi
done < "$indexFile"

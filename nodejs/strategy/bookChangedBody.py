def bookChanged():
    # 行情档次
    depth = 8;

    # 下单信号, 价格窗口
    SignalWindow = 0.03;
    
    mktBidPrice = bookPrice[BID][depth];
    mktAskPrice = bookPrice[ASK][depth];
    
    if (mktAskPrice < (historyLowPrice * (1 + SignalWindow))):
        # 根据对手盘设置下单尺寸
        buyOrderSize = min(100, bookSize[ASK][depth]);
        if (buyOrderSize > 0):
            # 股票买入
            openOrder(buyOrderSize, mktAskPrice, BUY); 

    if (mktBidPrice > (historyHighPrice * (1 - SignalWindow))):
        sellOrderSize = min(100, bookSize[BID][depth]);
        if (sellOrderSize > 0):
            # 股票卖出
            openOrder(sellOrderSize, mktBidPrice, SELL);      

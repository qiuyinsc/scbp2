#include <csignal>
#include <exception>
#include <fstream>
#include <string>

#include <boost/assign/list_of.hpp>
#include <boost/foreach.hpp>
#include <boost/bind.hpp>

#include <bb/core/date.h>
#include <bb/core/env.h>
#include <bb/core/Error.h>
#include <bb/core/Log.h>
#include <bb/core/LuaState.h>
#include <bb/core/messages.h>
#include <bb/core/program_options.h>
#include <bb/core/instrument.h>
#include <bb/core/timeval.h>
#include <bb/core/smart_ptr.h>
#include <bb/core/mktdestmappings.h>

#include <bb/clientcore/clientcoreutils.h>
#include <bb/clientcore/BookBuilder.h>
#include <bb/clientcore/ClientContext.h>
#include <bb/clientcore/IClientTimer.h>
#include <bb/clientcore/SourceBooks.h>
#include <bb/clientcore/MsgHandler.h>

#include <bb/trading/priceutils.h>
#include <bb/trading/Trader.h>
#include <bb/trading/TradingContext.h>
#include <bb/trading/Trading_Scripting.h>
#include <bb/trading/PositionTrackerFactory.h>
#include <bb/trading/BbL1RefDataFactory.h>
#include <bb/trading/RefData.h>

#include <bb/trading/FillFeesDotLuaFeeProvider.h>

#include <bb/simulator/SimClientContext.h>
#include <bb/simulator/SimTradingContext.h>
#include <bb/simulator/ISimMktDest.h>
#include <bb/simulator/SimMktDelays.h>
#include <bb/simulator/OrderManager.h>
#include <bb/simulator/SimTrader.h>
#include <bb/simulator/SimTradeDemonClient.h>
#include <bb/simulator/SimTrader.h>
//#include <bb/simulator/markets/ProactiveDest.h>
#include <bb/simulator/markets/ShfeSimMktDest.h>
#include <bb/simulator/SimBbL1RefDataFactory.h>
#include <bb/simulator/Simulator_Scripting.h>

#include "Strategy.h"
#include "SSEHistMStream.h"
#include "SSESourceTickFactory.h"
#include "SSEBookBuilder.h"

using namespace bb;
using namespace bb::trading;
using namespace alphaless;
using namespace std;

namespace {
    const char* const g_usage = "usage: alphaless [options]";
    const char* const g_programName = "alphaless";
}

namespace {
class SSEMStreamManager : public HistMStreamManager {
public:
    SSEMStreamManager(const timeval_t &startTv, const timeval_t &endTv, string dayStr, string dataPath)
        : HistMStreamManager(startTv, endTv)
        , m_dayStr(dayStr)
        , m_dataPath(dataPath) {}

    virtual void addDataByMTypeInstr(const source_t& source, mtype_t mtype, const instrument_t& instr) {
        if (source.type() == SRC_SHSE) {
            SSEHistMStream* stream = new SSEHistMStream(mtype, instr.toString(), m_dayStr, m_dataPath);
            addFile(stream);
        } else {
            HistMStreamManager::addDataByMTypeInstr(source, mtype, instr);
        }
    }

    virtual void addDataByMType(const source_t& source, mtype_t mtype) {
        if (source.type() == SRC_SHSE) {
            string id("");
            SSEHistMStream* stream = new SSEHistMStream(mtype, id, m_dayStr, m_dataPath);
            addFile(stream);
        } else {
            HistMStreamManager::addDataByMType(source, mtype);
        }
    }

private:
    string m_dayStr;
    string m_dataPath;
};
BB_DECLARE_SHARED_PTR(SSEMStreamManager);

};

void initSimulator(trading::HistTradingContextPtr const& htc, timeval_t &starttv, timeval_t &endtv, const InstrVector& instrs, StrategySettings& settings)
{
    mktdest_t market = str2mktdest(settings.market.c_str());

    bb::simulator::DefaultDelaysFactoryCPtr delayFactory(new bb::simulator::DefaultDelaysFactory(HistMStreamManager::getRuntimeFeedDest()));
    bb::simulator::OrderManagerPtr om = bb::simulator::OrderManagerPtr (
            new bb::simulator::OrderManager(
                market,
                htc->getHistMStreamManager(),
                htc->getTimeProvider(),
                htc->getEventDistributor(),
                0,			//verbose
                delayFactory->getMarketTransitDelays(market)
               )
           );

    bb::simulator::SimTraderPtr simTrader = boost::dynamic_pointer_cast<bb::simulator::SimTrader>(htc->getBaseTrader());
    bb::simulator::SimTradeDemonClientPtr stdc = boost::dynamic_pointer_cast<bb::simulator::SimTradeDemonClient>(simTrader->getTradeDemonClient());

    stdc->initSimMarketDest(market, bb::dynamic_pointer_cast<bb::simulator::ISimMktDest>(om));

    source_t source;
    source.setType(str2EFeedType(settings.feed_type.c_str()));
    source.setOrig(str2EFeedOrig(settings.feed_orig.c_str()));
    source.setDest(str2EFeedDest(settings.feed_dest.c_str()));

    bb::simulator::AsiaOrderHandlerPtr aoh = bb::simulator::AsiaOrderHandlerPtr (new bb::simulator::AsiaOrderHandler(
            htc,
            market,
            source,
            1,
            om,
            delayFactory->getMarketInternalDelays(market)
           ));

    om->registerOrderHandler(aoh);

    int cnt = 0;
    BOOST_FOREACH(bb::instrument_t instr, instrs)
    {
        om->addInstrument(
            instr,
            bb::IBookSpecPtr(),
            settings.sim_order_book);

        ++cnt;

    }

    LOG_INFO << " Registered order handler for "
             << cnt
             << " tickers."
             << bb::endl;
}

int main(int argc, char** argv)
{
    std::string startDateStr;
    std::string endDateStr;
    acct_t acct;
    std::string strategyConfig;
    std::string csvfile;
    bool verbose = false;
    bool live = false;
    bool route = false;
    bool baseline = false;
    std::string id;

    bb::options_description optionsDesc("options");

    std::string ident(g_programName);
    setLogger(ident.c_str());

    default_init();
    bb::registerScripting();
    bb::simulator::registerScripting();
    SSESourceTickFactoryPtr sourceTickFactory;
    IBookBuilderPtr bp;

    {
        namespace po = boost::program_options;
        optionsDesc.add_options()
            ("help", "show the help text")
            ("start-date,s", po::value(&startDateStr),"")
            ("end-date,e", po::value(&endDateStr), "")
            ("account,a", po::value(&acct), "")
            ("verbose,v", po::bool_switch(&verbose), "")
            ("live,l", po::bool_switch(&live), "Run in live mode")
            ("route,r", po::bool_switch(&route), "Allow routing of orders")
            ("id,i", po::value(&id), "unique name for strategy")
            ("strategy-config,c", po::value(&strategyConfig), "lua config file for strategy")
            ("csv-file,f", po::value(&csvfile), "csv file which contains order instructions generated by customer strategy")
            ("baseline,b", po::bool_switch(&baseline), "whether the first trading day")
            ;
    }

    try
    {
        boost::program_options::variables_map pgo;
        parseOptionsSimple(pgo, argc, argv,
                boost::program_options::options_description()
                .add(optionsDesc));

        if(startDateStr.empty())
            throw UsageError("start-date required");
        if(!acct.isValid())
            throw UsageError("account required");
        if(strategyConfig.empty())
            throw UsageError("strategy config must be specified");
        if(id.empty())
            throw UsageError("id must be specified");

        // time relations
        timeval_t starttv = timeval_t::make_time(startDateStr);
        char* sseHistoryDataPath = getenv("SSE_HISTORY_DATA_PATH");
        if (NULL == sseHistoryDataPath) {
            throw UsageError("no 'SSE_HISTORY_DATA_PATH' environment variable");
        }

        std::string dataPath(sseHistoryDataPath);
        timeval_t endtv;
        if(endDateStr.empty())
        {
            endtv = starttv + boost::posix_time::hours(24) - boost::posix_time::seconds(1);
        }
        else
        {
            endtv = timeval_t::make_time(endDateStr);
            if(endtv < starttv)
                throw UsageError("end-date is before start-date!");
        }

        LuaStatePtr state(new LuaState);
        state->loadLibrary("core");
        state->loadLibrary("simulator");
        bb::trading::register_libtrading(*(state->getState()));
        state->load(strategyConfig);

        StrategySettings settings = StrategySettings::fromLua((*state)["strategy_config"]);
        bb::source_t::setAutoOrig(str2EFeedOrig(settings.feed_orig.c_str()));
        bb::source_t::setAutoDest(str2EFeedDest(settings.feed_dest.c_str()));

        //Construct TradingContext and ClientContext
        std::string idString = ident + "_" + id + "_" + acct.toString();

        SSEMStreamManagerPtr streamManager(new SSEMStreamManager(starttv, endtv, startDateStr, dataPath));
        HistClientContextPtr clientContext =  HistClientContext::create(bb::DefaultCoreContext::getEnvironment(),
            starttv, endtv, getLogger()->getname(), streamManager, verbose, string("worfdata"));
        sourceTickFactory.reset(new SSESourceTickFactory(clientContext));
        clientContext->set("SourceTickFactory", sourceTickFactory);

        source_t source;
        source.setType(str2EFeedType(settings.feed_type.c_str()));
        source.setOrig(str2EFeedOrig(settings.feed_orig.c_str()));
        source.setDest(str2EFeedDest(settings.feed_dest.c_str()));
        bp.reset(new SSEBookBuilder(source, clientContext));

        TradingContextFactory::Config tcCfg;
        tcCfg.setClientContextFactory(clientContext)
            .setAccount(acct)
            .createBookBuilderFromClientContext()
            .setBookBuilder(bp)
            .setOrderRoutingMode(
                route ?
                    TradingContextFactory::kRoute
                    : TradingContextFactory::kDoNotRoute);

        TradingContextPtr tradingContext = TradingContextFactory::create(tcCfg);

        InstrVector instruments;
        BOOST_FOREACH(const std::string& instr, settings.instruments) {
            instruments.push_back(bb::instrument_t::fromString(instr));
        }

        boost::optional<source_t> positionSource;
        // live default is 0
        if(live) {
            // needs to be set in live mode to get position updates
            positionSource = source_t::make_auto(SRC_INFO);
        } else { // simulation
            sourceset_t data_source;

            data_source.insert(source);

            tradingContext->setDefaultReferenceData(RefDataPtr(new RefData(data_source)));

            HistMStreamManager::setRuntimeFeedDest(str2EFeedDest(settings.feed_dest.c_str()));

            trading::HistTradingContextPtr htc =
                boost::dynamic_pointer_cast<trading::HistTradingContext>(tradingContext);
            bb::simulator::SimTradeDemonClientPtr tdc =
                bb::simulator::SimTradeDemonClient::create(htc) ;

            bb::simulator::initSimTrader(htc, tdc);

            // get the tick provider from the reference data factory
            // tdc->setUseRefTick(true);

            initSimulator(htc, starttv, endtv, instruments, settings);

            BOOST_FOREACH(const bb::instrument_t instr, instruments){
                // set the starting position for this instrument in the simulator
                tdc->initSyntheticPosition(instr, 100);
            }
        }
        IPositionProviderFactoryPtr ppf(new PositionTrackerFactory(positionSource));
        tradingContext->setPositionProviderFactory(ppf);

        const bb::trading::IFeeProviderFactoryPtr fpf = boost::make_shared<bb::trading::FillFeesDotLuaFeeProviderFactory>();
        tradingContext->setFeeProviderFactory(fpf);
        tradingContext->createTrader();

        // MUST go after "createTrader()"  or "getIssuedOrderTracker" on tradingContext won't work

        LOG_INFO << "Instantiating Strategy." << bb::endl;
        StrategyPtr strategy(new Strategy(instruments, tradingContext, settings, source, csvfile, baseline));
        LOG_INFO << "Done Instantiating Strategy." << bb::endl;

        //if it's true, then cancel all the orders if the connection between clients and TD is down
        tradingContext->getBaseTrader()->setCancelOnDisconnect(true);
        if(!tradingContext->getBaseTrader()->connectToTradeServer(settings.trade_server)) {
            BB_THROW_ERROR_SS("Failed to connect to trade server " << settings.trade_server);
        }

        // connect to listen to user messages
        // this allows message passing to the strategy
        if (clientContext->isLive())
        {
            strategy->subscribeUserMessage();
        }

        // End Strategy Launcher Items

        // start main loop
        clientContext->run();

        LOG_INFO << "end of market day - process end of day stuff"
                 << bb::endl;
    }
    catch (UsageError &e)
    {
        if(e.what()[0] != '\0')
        {
            std::cerr << e.what() << "\n";
            std::cerr << optionsDesc;
            std::cerr << bb::endl;
            return EXIT_FAILURE;
        }

        LOG_INFO << "\n\n" << g_usage << "\n";
        LOG_INFO << optionsDesc << bb::endl;
    }
    catch (std::exception& e)
    {
        std::cout << "caught exception" << std::endl;
        LOG_PANIC << "error: " << e.what() << bb::endl;
        return EXIT_FAILURE;
    }
}

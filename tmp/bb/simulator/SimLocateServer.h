#ifndef BB_SIMULATOR_SIMLOCATESERVER_H
#define BB_SIMULATOR_SIMLOCATESERVER_H

/* Contents Copyright 2016 Athena Capital Research LLC. All Rights Reserved. */

#include <bb/core/messages.h>

#include <bb/clientcore/SpinServerConnection.h>

namespace bb {
BB_FWD_DECLARE_SHARED_PTR( ClientContext );
BB_FWD_DECLARE_SHARED_PTR( HistMStreamManager );
BB_FWD_DECLARE_SHARED_PTR( MsgHandler );

namespace simulator {
class SimLocateServer : public SimSpinServer
{
public:
    //needs a histcc, but this is here for ease of use
    SimLocateServer( const ClientContextPtr& cc );
    SimLocateServer( const HistClientContextPtr& hcc );
    virtual ~SimLocateServer();

    void init( const HistClientContextPtr& hcc );

    void onLocateRequestMsg( const LocateRequestMsg& );
    void onLocateReturnMsg( const LocateReturnMsg& );

protected:
    HistMStreamManagerPtr m_histMStreamManager;
    MsgHandlerPtr  m_spLocateRequestMsgHandler;
    MsgHandlerPtr  m_spLocateReturnMsgHandler;

    uint32_t m_refNum;
};

BB_DECLARE_SHARED_PTR( SimLocateServer );

} // namespace simulator
} // namespace bb

#endif // BB_SIMULATOR_SIMLOCATESERVER_H

#ifndef BB_SIMULATOR_SIMTRADER_H
#define BB_SIMULATOR_SIMTRADER_H

/* Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved. */


#include <bb/core/smart_ptr.h>
#include <bb/core/acct.h>
#include <bb/core/instrument.h>

#include <bb/trading/Trader.h>
#include <bb/trading/ITrader.h>
#include <bb/core/Scripting.h>
#include <bb/core/source.h>

namespace bb {
BB_FWD_DECLARE_SHARED_PTR( ITradeDemonClient );
BB_FWD_DECLARE_SHARED_PTR( ClientContext );

namespace trading {
BB_FWD_DECLARE_SHARED_PTR( HistTradingContext );
BB_FWD_DECLARE_SHARED_PTR( TradingContext );
} // namespace trading

namespace simulator {

class SimMessagesRecorder;

BB_FWD_DECLARE_SHARED_PTR( SimTradeDemonClient );
BB_FWD_DECLARE_SHARED_PTR( SimTradingContext );
BB_DECLARE_SHARED_PTR( SimMessagesRecorder );

/// Trader for use in simulation.
/// Core difference:  doesn't maintain a persistent sequence number.
BB_FWD_DECLARE_SHARED_PTR( SimTrader );
class SimTrader
    : public trading::BaseTrader
    , virtual public trading::ISimTrader
    , virtual public trading::ISimMessageRecoder
{
public:
    SimTrader( trading::HistTradingContextPtr const& trading_context );

    SimTrader( trading::HistTradingContextPtr const& trading_context,
               ITradeDemonClientPtr const& spDefaultTDC );
    SimTrader( trading::TradingContextPtr const& trading_context );

    SimTrader( trading::TradingContextPtr const& trading_context,
               ITradeDemonClientPtr const& spDefaultTDC );

    // A convenience method. It constructs a SimTradeDemonClient and a
    // new SimTrader using that client, and then injects the SimTrader
    // into  the  TradingContext,  which  must actually  point  to  an
    // instance of HistClientContext.
    static SimTraderPtr create( trading::TradingContextPtr const& trading_context );

    virtual ~SimTrader();
    BB_DECLARE_SCRIPTING();
    virtual unsigned int sendOrder( const trading::OrderPtr& order, timeval_t* send_time );
    virtual bool sendCancel( const trading::OrderPtr& issued_order, timeval_t* send_time );

    virtual unsigned int modifyOrder(const trading::OrderPtr& issued_order,
                                     const trading::OrderPtr& desired_order,
                                     timeval_t* send_time = 0);

    // Helper function for modifying an order, by using original order's orderid
    virtual unsigned int modifyOrder(unsigned int orig_orderid,
                                     const trading::OrderPtr& desired_order,
                                     timeval_t* send_time = 0);

    virtual bool connectToTradeServer( const std::string& new_trdserver_name );
    virtual void disconnectFromTradeServer();
    virtual bool isConnected() const { return m_bconnected; }

    // ITrader interface is
    // Implemented in the BaseTrader

    void initInstrument( const instrument_t& instr, mktdest_t src );
    /// Adds a simulated position at startup (only used by autoflat)
    void initSyntheticPosition( const instrument_t& instr, int pos );

    /// This records all the simulated messages to a file (for traderviz)
    void recordMessages( std::string const& outFileName );

    /// Obtains the ITradeDemonClientPtr from the super class and
    /// downcasts it to a SimTradeDemonClient. This is useful if you
    /// are in lua where it would be hard or impossible to make the
    /// downcast yourself.
    SimTradeDemonClientPtr getSimTradeDemonClient() const;

private:
    void init( trading::HistTradingContextPtr const&, ITradeDemonClientPtr const& );
    /// Sends alerts to std::cout
    virtual void sendAlert( loglevel_t warn_level, const std::string& alertmsg );

    SimMessagesRecorderPtr m_simMessagesRecorder;
    ClientContextPtr m_clientContext;
    bool m_bconnected;
    source_t m_clientSource;
};

} // namespace simulator
} // namespace bb

#endif // BB_SIMULATOR_SIMTRADER_H

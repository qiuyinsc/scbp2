#ifndef BB_SIMULATOR_SIMTRADINGCONTEXT_H
#define BB_SIMULATOR_SIMTRADINGCONTEXT_H

/* Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved. */


#include <bb/trading/TradingContext.h>
#include <bb/core/Msg.h>

namespace bb {

BB_FWD_DECLARE_SHARED_PTR( IBookBuilder );
BB_FWD_DECLARE_SHARED_PTR( EventDistributor );

namespace trading {
BB_FWD_DECLARE_SHARED_PTR( BaseTrader );
}

namespace simulator {

BB_FWD_DECLARE_SHARED_PTR( SimClientContext );
BB_FWD_DECLARE_SHARED_PTR( SimTradingContext );

class SimTradingContext : public bb::trading::HistTradingContext
{
public:
    SimTradingContext( const SimClientContextPtr& scc, const bb::IBookBuilderPtr& bookbuilder, bb::acct_t acct,
                       trading::BaseTraderPtr const& spDefaultTrader = trading::BaseTraderPtr() );
    virtual ~SimTradingContext();

    EventDistributorPtr getMktEventDistributor() const;

private:
    SimClientContextPtr m_simClientContext;
};

}
}

#endif // BB_SIMULATOR_SIMTRADINGCONTEXT_H

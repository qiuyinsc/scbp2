#ifndef BB_SIMULATOR_SIMMKTDELAYS_H
#define BB_SIMULATOR_SIMMKTDELAYS_H

/* Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved. */


#include <map>
#include <iosfwd>

#include <bb/core/bbint.h>
#include <bb/core/symbol.h>
#include <bb/core/mktdest.h>
#include <bb/core/LuaConfig.h>
#include <bb/core/ptime.h>
#include <bb/core/source.h>
#include <bb/simulator/SimTypes.h>
#include <bb/core/Scripting.h>

namespace bb {
namespace simulator {

class ITDLocation //it is assumed that the TD and mkt location are the same
{
public:
    virtual bb::EFeedDest getTDDest() const = 0; //where the TD is sitting
    virtual ~ITDLocation() {}
};
BB_DECLARE_SHARED_PTR( ITDLocation );

class IClientLocation
{
public:
    virtual bb::EFeedOrig getClientOrig() const = 0; //where the client is sitting
    virtual ~IClientLocation() {}
};
BB_DECLARE_SHARED_PTR( IClientLocation );

class IMarketInternalDelays  : public virtual IMarketDestination //todo: remove IMarketDestination?
{
public:
    virtual ptime_duration_t getConfirm2Fill()   const = 0; // from ACK to marketable order
    virtual ptime_duration_t getCancel2Confirm() const = 0; // from cancel request to confirmation
    virtual ptime_duration_t getOpenDelay()      const = 0; // from recv request to open confirm
    virtual ptime_duration_t getRejectDelay()    const = 0; // from recv request to reject response
    virtual ptime_duration_t getFillDelay()      const = 0; // from recv request to reject response

    virtual ~IMarketInternalDelays() {}
};
BB_DECLARE_SHARED_PTR( IMarketInternalDelays );


class IMarketTransitDelays : public virtual ITDLocation, public virtual IMarketDestination, public virtual IMarketInternalDelays
{
public:
    virtual ptime_duration_t getTD2Mkt()    const = 0;
    virtual ptime_duration_t getMkt2TD()    const = 0;
    virtual ~IMarketTransitDelays() {}
};
BB_DECLARE_SHARED_PTR( IMarketTransitDelays );

class ITDTransitDelays : public virtual ITDLocation, public virtual IClientLocation
{
public:
    virtual ~ITDTransitDelays() {}
    virtual ptime_duration_t getTD2Client()    const = 0;
    virtual ptime_duration_t getClient2TD()    const = 0;
    virtual ptime_duration_t getTDClientRoundtrip()    const = 0;
    virtual ptime_duration_t getTDInternal()    const = 0;
    virtual bool isTDInternalDelaySet() const = 0;
};
BB_DECLARE_SHARED_PTR( ITDTransitDelays );
/*
 * order STAT_NEW ->getClient2TD() + getTD2Client()->STAT_TRANSIT
 * STAT_NEW ->getClient2TD() + getTD2Mkt() + getMkt2TD() + getTD2Client()->STAT_OPEN
 * STAT_NEW ->getClient2TD() + getTD2Mkt() + getMkt2TD() + getTD2Client() + getConfirm2Fill()->fill
 * */


class IDelaysFactory //: public virtual ITDLocation, public virtual IClientLocation
{
public:
    virtual ITDTransitDelaysCPtr getTDTransitDelays() const = 0;
    virtual IMarketTransitDelaysCPtr getMarketTransitDelays( bb::mktdest_t const& ) const = 0;
    virtual IMarketInternalDelaysCPtr getMarketInternalDelays( bb::mktdest_t const& ) const = 0;
    virtual ~IDelaysFactory() {}
};
BB_DECLARE_SHARED_PTR( IDelaysFactory );


////////////////////////////////////////////////////////////////////////////////////////
class TDLocation : public virtual ITDLocation
{
public:
    TDLocation( bb::EFeedDest const& d );
    bb::EFeedDest getTDDest() const;

private:
    bb::EFeedDest m_tdest;
};

BB_DECLARE_SHARED_PTR( TDLocation );

class ClientLocation : public virtual IClientLocation
{
public:
    ClientLocation( bb::EFeedOrig const& d );
    bb::EFeedOrig getClientOrig()    const;

private:
    bb::EFeedOrig m_orig;
};


struct MarketDestination : virtual IMarketDestination
{
    MarketDestination( bb::mktdest_t const& d );
    bb::mktdest_t getMktDest() const;

private:
    bb::mktdest_t m_orig;
};


BB_DECLARE_SHARED_PTR( ClientLocation );

class TDTransitDelays : public ITDTransitDelays, public TDLocation, public ClientLocation
{
public:
    TDTransitDelays(ptime_duration_t const& transit,
                    ptime_duration_t const& internal, bb::EFeedDest);
    void setDelay(const ptime_duration_t& transit,
                  const ptime_duration_t& internal);
    ptime_duration_t getTD2Client() const;
    ptime_duration_t getClient2TD() const;
    ptime_duration_t getTDClientRoundtrip() const { return getClient2TD() + getTD2Client(); }
    ptime_duration_t getTDInternal()   const;
    bool isTDInternalDelaySet() const;
private:
    ptime_duration_t m_triptime;
    ptime_duration_t m_internal;
};

BB_DECLARE_SHARED_PTR( TDTransitDelays );

class MarketInternalDelays : public virtual IMarketInternalDelays
{
public:
    MarketInternalDelays( ptime_duration_t const& t );
    ptime_duration_t getConfirm2Fill()    const;
    ptime_duration_t getCancel2Confirm()    const;
    ptime_duration_t getOpenDelay() const;
    ptime_duration_t getRejectDelay() const;
    ptime_duration_t getFillDelay() const;

private:
    ptime_duration_t m_delaytime;
};


class MarketTransitDelays : public virtual IMarketTransitDelays
                          , public TDLocation
                          , public MarketDestination
                          , public MarketInternalDelays
{
public:
    MarketTransitDelays( bb::mktdest_t const& j,
                         ptime_duration_t const& transitdelay,
                         ptime_duration_t const& internaldelay,
                         bb::EFeedDest const& d );

    ptime_duration_t getTD2Mkt()    const;
    ptime_duration_t getMkt2TD()    const;

private:
    ptime_duration_t m_triptime;
};


struct SimMktDelaysConfig : public bb::LuaConfig<SimMktDelaysConfig> {
    ptime_duration_t tdTransitDelays;
    ptime_duration_t tdInternalDelays;
    ptime_duration_t mktTransitDelays;
    ptime_duration_t mktInternalDelays;

    SimMktDelaysConfig();

    static void describe();
};

//not a particularly good design, just implementing something to proceed with
class DefaultDelaysFactory
    : public virtual IDelaysFactory
{
public:
    BB_DECLARE_SCRIPTING();
    DefaultDelaysFactory( bb::EFeedDest const& _tdlocation );
    virtual ~DefaultDelaysFactory();
    ITDTransitDelaysCPtr getTDTransitDelays() const;
    IMarketTransitDelaysCPtr getMarketTransitDelays( bb::mktdest_t const& ) const;
    IMarketInternalDelaysCPtr getMarketInternalDelays( bb::mktdest_t const& ) const;

protected:
    TDLocationPtr m_spTDLocation;
    ClientLocationPtr m_spClientLocation;
    TDTransitDelaysPtr m_spTDTransitDelays;
    SimMktDelaysConfig m_config;

private:
    DefaultDelaysFactory( DefaultDelaysFactory const& );
    void operator=( DefaultDelaysFactory const& );
    mutable std::map<bb::mktdest_t, IMarketDestinationCPtr> m_mktdelays;

};
BB_DECLARE_SHARED_PTR( DefaultDelaysFactory );

} // namespace simulator
} // namespace bb

#endif // BB_SIMULATOR_SIMMKTDELAYS_H

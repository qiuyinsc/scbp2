#ifndef BB_SIMULATOR_SIMRISKPOSITIONCONTROL_H
#define BB_SIMULATOR_SIMRISKPOSITIONCONTROL_H

/* Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved. */


#include <bb/core/smart_ptr.h>
#include <bb/servers/tdcore/ITdRiskControl.h>
#include <bb/servers/tdcore/ITdPositionStore.h>
#include <stdint.h>
#include <bb/core/Dispose.h>
#include <bb/core/Scripting.h>
namespace bb {

class TdOrderMsg;
class TdOrderFutureMsg;
class instrument_t;
namespace tdcore {
BB_FWD_DECLARE_SHARED_PTR( TdRiskControl );

class TdRiskLimits;
class TdPositionSaver;
}
namespace simulator {
BB_FWD_DECLARE_SHARED_PTR( SimTradeDemonClient );
struct RiskPositionControlHook
{

    virtual ~RiskPositionControlHook() {}
    virtual void setTDC( bb::IntrusiveWeakPtr<SimTradeDemonClient> const& ) = 0;
    virtual tdcore::ITdPositionStorePtr getPositionStore() const = 0;
};
BB_DECLARE_SHARED_PTR( RiskPositionControlHook );


struct PositionStoreOptions
{
    enum EInitAction { INIT_NONE, INIT_LOAD, INIT_LOAD_NO_UNLINK, INIT_LOAD_READONLY, INIT_NULL };

    ///
    /// INIT_LOAD will initialize the risk control with the positions specified in <filename>
    /// and rename the file to <filename>.bak
    /// INIT_LOAD_NO_UNLINK does the same as INIT_LOAD, but leaves <filename> in place.
    /// INIT_LOAD_READONLY  only loads, but does not save
    /// INIT_NONE will not load the file. but only saves
    /// INIT_NULL will do nothing
    /// default is INIT_NULL
    ///


    PositionStoreOptions() : m_init( INIT_NULL ) {}
    PositionStoreOptions( EInitAction e, std::string const& _fname )
        : m_init( e )
        , m_filename( _fname ) {}
    EInitAction getInitAction() const
    {
        return m_init;
    }
    std::string getFilename() const
    {
        return m_filename;
    }

private:
    EInitAction m_init;
    std::string m_filename;
};


struct RiskPositionControl
    : tdcore::ITdRiskControlExt
    , tdcore::ITdPositionStore
    , RiskPositionControlHook
{
    BB_DECLARE_SCRIPTING();
    typedef boost::function<void ( side_t, const instrument_t& )> cancel_callback_t;

    virtual ~RiskPositionControl();
    RiskPositionControl( tdcore::TdRiskLimits const& );

    RiskPositionControl( tdcore::TdRiskLimits const&, PositionStoreOptions const& s );
    // a function which can be called to cancel all orders on a side, for a given symbol (or SYM_ALL)

    void operator()( side_t, const instrument_t& );
//impl ITdPositionStore
    tdcore::SymbolData& setSymbolPriceIfUnset( const instrument_t& instr );  // use m_limits.default_price (supports stocks and options)
    tdcore::SymbolData& setSymbolPriceIfUnset( const instrument_t& instr, double price );
    void setSymbolPrice( const instrument_t& instr, double price );  // set price and adjust account values
    void setSymbolSize( const instrument_t& instr, int32_t dir_size );  // set position size and adjust account values
    void setSymbolBaseline( const instrument_t& instr, int32_t baseline );
    void clear( bool keep_baselines = false );
    const tdcore::SymbolData& getSymbolData( const instrument_t& instr ) const;

    // these can be called from a separate thread, e.g. for position broadcasting
    void forEach( boost::function<void(const instrument_t&, const tdcore::SymbolData&)> callback ) const;
    void forInstrument( const instrument_t& instr, boost::function<void(const tdcore::SymbolData&)> callback ) const;


    // impl  ITdRiskControlExt
    void close( uint32_t orderid, const tid_t& tradeid, dir_t dir, const instrument_t& instr, int32_t left );

    int32_t bust( uint32_t orderid, const tid_t& tradeid, dir_t dir,
                  const instrument_t& instr, double price, int32_t size );

    int32_t fill( uint32_t orderid, const tid_t& tradeid, dir_t dir,
                  const instrument_t& instr, double price, int32_t size );
    void open( uint32_t orderid, const tid_t& tradeid, dir_t dir, const instrument_t& instr, int32_t size );
    bool approve( const acct_t& acct, uint32_t orderid, dir_t dir, const instrument_t& instr, double price, int32_t size, tif_t tif );


    void setTDC( bb::IntrusiveWeakPtr<SimTradeDemonClient> const& );
    tdcore::ITdPositionStorePtr getPositionStore() const;

private:
    void init( tdcore::TdRiskLimits const&, PositionStoreOptions const& s );
    tdcore::TdRiskControlPtr m_impl;
    tdcore::ITdRiskControlExtPtr m_risk;
    tdcore::ITdPositionStorePtr m_store;
    std::auto_ptr<tdcore::TdRiskLimits> m_limits;
    std::auto_ptr<tdcore::TdPositionSaver> m_saver;

    bb::IntrusiveWeakPtr<SimTradeDemonClient> m_parent;

};
BB_DECLARE_SHARED_PTR( RiskPositionControl );

} // namespace simulator
} // namespace bb


#endif // BB_SIMULATOR_SIMRISKPOSITIONCONTROL_H

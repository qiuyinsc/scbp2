#ifndef BB_SIMULATOR_MESSAGES_FAKEMESSAGEGENERATOR_H
#define BB_SIMULATOR_MESSAGES_FAKEMESSAGEGENERATOR_H

/* Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved. */


#include <boost/noncopyable.hpp>
#include <bb/core/smart_ptr.h>
#include <bb/core/source.h>
#include <bb/core/fillflag.h>
#include <bb/core/symbol.h>
#include <bb/core/acct.h>
#include <bb/core/ostatus.h>
#include <bb/core/cxlstatus.h>
#include <bb/core/liquidity.h>
#include <bb/core/side.h>
#include <bb/core/mtype.h>
#include <bb/core/timeval.h>
#include <bb/simulator/SimTypes.h>

namespace bb {

BB_FWD_DECLARE_SHARED_PTR( TdCancelRejectMsg );
BB_FWD_DECLARE_SHARED_PTR( TdStatusChangeMsg );
BB_FWD_DECLARE_SHARED_PTR( TdStatusOrderMsg );
BB_FWD_DECLARE_SHARED_PTR( TdFillNewMsg );
BB_FWD_DECLARE_SHARED_PTR( MarginInstrumentInfoMsg );

struct MsgHdr;

namespace simulator {

/// Simple class used for formatting messages.

class FakeMessageGenerator : private boost::noncopyable
{
public:
    FakeMessageGenerator( acct_t account, source_t tdSource, source_t marginSource );

    source_t getSimTDSource() const { return m_tdSource; }

    TdFillNewMsgPtr createFakeFillNewMsg(
        const instrument_t& instr,
        OrderId const& order_id,
        int left,
        dir_t dir,
        double fillpx,
        int fillsz,
        timeval_t msg_tv,
        liquidity_t liquidity,
        timeval_t exch_tv,
        mktdest_t contraBroker,
        fillflag_t fflag = static_cast<fillflag_t>( 0 )
        );

    TdStatusChangeMsgPtr createFakeStatChangeMsg(
        const instrument_t& instr,
        OrderId const& order_id,
        ostatus_t prev_status,
        ostatus_t newstatus,
        cxlstatus_t cxlstatus,
        bb::oreason_t reason,
        timeval_t msg_tv,
        timeval_t exch_tv /*= timeval_t()*/
        );

    TdCancelRejectMsgPtr createFakeCancelRejectMessage(
        const instrument_t& instr,
        acct_t acct,
        OrderId const& order_id,
        timeval_t msg_tv
        );

    MarginInstrumentInfoMsgPtr createFakeInstrumentInfoMsg( const timeval_t& tv
                                                            , const instrument_t& instr, int pos, bool isUnicast =
                                                                false );

private:
    void fillHeader( MsgHdr& hdr, symbol_t sym, mtype_t mtype, size_t size, timeval_t time );
    acct_t m_account;
    source_t m_tdSource, m_marginSource;
    int m_seqnum;
};
BB_DECLARE_SHARED_PTR( FakeMessageGenerator );

} // namespace simulator
} // namespace bb

#endif // BB_SIMULATOR_MESSAGES_FAKEMESSAGEGENERATOR_H

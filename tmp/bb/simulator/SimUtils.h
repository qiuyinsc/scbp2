#ifndef BB_SIMULATOR_SIMUTILS_H
#define BB_SIMULATOR_SIMUTILS_H

/* Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved. */


#include <bb/core/instrument.h>
#include <bb/core/bbassert.h>
#include <bb/core/ptime.h>
#include <bb/core/TimeProvider.h>
#include <bb/core/smart_ptr.h>

#include <bb/trading/trading.h>
#include <bb/trading/Order.h>

#include <bb/simulator/SimTypes.h>
#include <bb/simulator/SimMktDelays.h>

namespace bb {
namespace simulator {

BB_FWD_DECLARE_SHARED_PTR( IOrderHandler );
class OrderIdHolder
{
    struct nat { int for_bool_; };

public:
    OrderIdHolder( OrderId oid, trading::OrderPtr const& _d, IOrderHandlerPtr const& d )
        : m_oid( oid ), m_ord( _d ), m_handler( d ) {}

    operator int nat::*( )  const { return m_ord ? &nat::for_bool_ : 0; }
    bool operator!() const { return !m_ord; }

    //usage
    bool operator<( OrderIdHolder const& r ) const
    {
        return m_oid < r.m_oid;
    }
    friend std::ostream& operator<<( std::ostream& os, OrderIdHolder const& y )
    {
        char const* v = y.m_ord ? "Active" : "Expired";
        os << y.m_oid << "->" << v;
        return os;
    }
    bool operator<( OrderId const& _d ) const
    {
        return m_oid < _d;
    }
    friend bool operator<( OrderId const& _d, OrderIdHolder const& s )
    {
        return _d < s.m_oid;
    }
    OrderId getId() const
    {
        return m_oid;
    }
    trading::OrderPtr const& getOrder() const
    {
        return m_ord;
    }
    IOrderHandlerPtr const& getOrderHandler() const
    {
        return m_handler;
    }
    void reset()
    {
        m_ord.reset();
    }

private:
    OrderId m_oid;
    trading::OrderPtr m_ord;
    IOrderHandlerPtr m_handler;
};

struct TimeProviderForwarder : virtual bb::ITimeProvider
{
    TimeProviderForwarder( bb::ITimeProviderCPtr const& d );
    bb::timeval_t getTime()    const { return m_tp->getTime(); }

private:
    ITimeProviderCPtr m_tp;
};

struct OrderManagerForwarder
{
    OrderManagerForwarder( IOrderManagerPtr const& d
                           , IMarketInternalDelaysCPtr const& _internaldelays );
//    OrderManagerForwarder( IOrderManagerWeakPtr const&d);
    OrderManagerForwarder( bb::IntrusiveWeakPtr<IOrderManager> const& d
                           , IMarketInternalDelaysCPtr const& _internaldelays );

    virtual ~OrderManagerForwarder() {}

    virtual bool notifyFill( trading::OrderPtr const& simord
                     , double price
                     , unsigned sz
                     , bb::fillflag_t fflag = bb::fillflag_t()
                     , const ptime_duration_t& add_delay = ptime_duration_t() )
    {
        BB_ASSERT( m_om );
        return m_om->notifyFill( simord, price, sz, fflag, m_spInternalMktDelays->getFillDelay() );
    }

    void notifyDone( trading::OrderPtr const& simord, bb::oreason_t dreason
                     , const ptime_duration_t& add_delay = ptime_duration_t() )
    {
        BB_ASSERT( m_om );
        m_om->notifyDone( simord, dreason, add_delay );
    }

private:
    bb::IntrusiveWeakPtr<IOrderManager> m_om;
    IMarketInternalDelaysCPtr m_spInternalMktDelays;
};

} // shfe
} // bb

#endif // BB_SIMULATOR_SIMUTILS_H

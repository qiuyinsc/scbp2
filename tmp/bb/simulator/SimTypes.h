#ifndef BB_SIMULATOR_SIMTYPES_H
#define BB_SIMULATOR_SIMTYPES_H

/* Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved. */

#include <bb/core/ptime.h>
#include <bb/core/safevalue.h>
#include <bb/core/mktdest.h>
#include <bb/core/oreason.h>
#include <bb/core/fillflag.h>
#include <bb/core/smart_ptr.h>
#include <bb/core/Dispose.h>

#include <bb/clientcore/BookSpec.h>

#include <bb/simulator/markets/IOrderBook.h>

#include <bb/trading/trading.h>

namespace bb {
class instrument_t;

namespace trading {
BB_FWD_DECLARE_INTRUSIVE_PTR( Order );
} // trading

namespace simulator {
BB_FWD_DECLARE_SHARED_PTR( IMarketInternalDelays );

namespace detail {
struct OrderIdPolicy : SafeFundamentalDefaultPolicy<unsigned> {};
} //detail

typedef SafeValue<detail::OrderIdPolicy> OrderId;
class IMarketDestination
{
public:
    // the market we are simulating
    virtual bb::mktdest_t getMktDest() const = 0; //the marketDest
    virtual ~IMarketDestination() {}
};
BB_DECLARE_SHARED_PTR( IMarketDestination );

class IOrderHandler
    : public virtual IMarketDestination
{
public:
    virtual ~IOrderHandler() {}
    virtual bool canCancel( trading::OrderCPtr const& ) const = 0;
    virtual bool canAccept( trading::OrderCPtr const& ) const = 0;
    virtual void activateOrder( trading::OrderPtr const& ) = 0;
    virtual void deactivateOrder( trading::OrderPtr const& ) = 0;
    virtual IMarketInternalDelaysCPtr getMarketInternalDelays() const = 0;

    virtual bool addInstrument( const bb::instrument_t& instr, const IBookSpecPtr& sim_book_spec = IBookSpecPtr(),
                                const IOrderBookSpecPtr& order_book_spec = IOrderBookSpecPtr() ) = 0;
};
BB_DECLARE_SHARED_PTR( IOrderHandler );

class IOrderManager
    : public virtual IMarketDestination
    , public virtual IntrusiveWeakPtrHookable
{
public:
    virtual ~IOrderManager() {}
    // return true if completely filled
    virtual bool notifyFill( trading::OrderPtr const& simord
                             , double price
                             , unsigned sz
                             , bb::fillflag_t fillflags
                             , const ptime_duration_t& mktdelay ) = 0;
    virtual void notifyDone( trading::OrderPtr const& simord
                             , bb::oreason_t dreason
                             , const ptime_duration_t& add_delay = ptime_duration_t() ) = 0;
    virtual void registerOrderHandler( IOrderHandlerPtr const& ) = 0;
    typedef  boost::intrusive::list_base_hook<
            boost::intrusive::link_mode<boost::intrusive::auto_unlink>,
            boost::intrusive::tag<IOrderManager> > OrderManagerHook;
};
BB_DECLARE_SHARED_PTR( IOrderManager );

} // namespace simulator
} // namespace bb
#endif // BB_SIMULATOR_SIMTYPES_H

#ifndef BB_SIMULATOR_SIMBBL1REFDATAFACTORY_H
#define BB_SIMULATOR_SIMBBL1REFDATAFACTORY_H

/* Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved. */

#include <bb/core/date.h>
#include <bb/trading/BbL1RefDataFactory.h>


namespace bb {
namespace simulator {

// The BbL1RefDataFactory builds reference data that selects among CQS
// or UQDF for quote feeds and CTS or UTDF for tick feeds, depending
// on the instrument.

class SimBbL1RefDataFactory : public trading::BbL1RefDataFactory
{
public:

    static date_t SecaucusBeginDate;
    static date_t PCWeehawkenEndDate;

    // By default we will produce RefData for each instrument that
    // includes both tick and quote sources.
    SimBbL1RefDataFactory();

    virtual ~SimBbL1RefDataFactory();

    virtual trading::RefDataPtr createRefData(
        trading::TradingContext* tradingContext,
        const instrument_t& instr );
};

BB_DECLARE_SHARED_PTR(SimBbL1RefDataFactory);

} // namespace trading
} // namespace bb

#endif // BB_SIMULATOR_SIMBBL1REFDATAFACTORY_H

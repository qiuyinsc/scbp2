#ifndef BB_SIMULATOR_ISIMMKTDEST_H
#define BB_SIMULATOR_ISIMMKTDEST_H

/* Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved. */

#include <stdint.h>

#include <bb/core/instrument.h>
#include <bb/core/acct.h>
#include <bb/core/tif.h>
#include <bb/core/side.h>
#include <bb/core/mktdest.h>
#include <bb/core/smart_ptr.h>
#include <bb/core/host_port_pair.h>

#include <bb/clientcore/BookSpec.h>

#include <bb/simulator/markets/IOrderBook.h>

namespace bb {
BB_FWD_DECLARE_SHARED_PTR( ITradeDemonClient );
BB_FWD_DECLARE_SHARED_PTR( SimSpinServer );
class Subscription;
class LuaState;
namespace trading {
BB_FWD_DECLARE_SHARED_PTR( TradingContext );
BB_FWD_DECLARE_SHARED_PTR( ITrader );

}
namespace simulator {

BB_FWD_DECLARE_SHARED_PTR( IDelaysFactory );
class ISimMktDest
{
public:
    virtual ~ISimMktDest();
    virtual void receiveOrder( const instrument_t& instr
                               , dir_t dir, mktdest_t dest
                               , double px, unsigned int size, unsigned int orderid
                               , int timeout, bool visible, tif_t tif, uint32_t oflags ) = 0;
    virtual void receiveCancelOrder( acct_t order_acct, const instrument_t& instr
                                     , const unsigned int order_id ) = 0;
    virtual void addInstrument( const instrument_t& instr, const IBookSpecPtr& sim_book_spec = IBookSpecPtr(),
                                const IOrderBookSpecPtr& order_book_spec = IOrderBookSpecPtr() ) {};
};
BB_DECLARE_SHARED_PTR( ISimMktDest );

void initSimMarketDest( trading::TradingContextPtr const&, mktdest_t dest, ISimMktDestPtr const& );
void initSimMarketDestWithSource( trading::TradingContextPtr const& _tc, mktdest_t dest, const source_t& source );
void initSimTrader( trading::TradingContextPtr const&, ITradeDemonClientPtr const& _ = ITradeDemonClientPtr() );
void initSimSpinServer( const trading::TradingContextPtr&, const SimSpinServerPtr&, const HostPortPair& );
void initSimInstrument( trading::TradingContextPtr const& _tc, mktdest_t dest, instrument_t const& instr );
void initSimDelays( trading::TradingContextPtr const& _tc, IDelaysFactoryCPtr const& delaysf );
void initSimDelays( trading::TradingContextPtr const& _tc, IDelaysFactoryCPtr const& delaysf );
void initSyntheticPosition( trading::TradingContextPtr const& _tc, const instrument_t& instr, int pos );
void initSimMessageRecorder( trading::ITraderPtr const& _tc, std::string const& _messagefile );
//creates Environment, loads program, initializes simulator
void initSimTraderLua( bb::Subscription&,
                       trading::TradingContextPtr const&,
                       std::string const& _func,
                       std::istream& _prog );
//assumes the program is already loaded into the Environment, and willonly initialize simulator
//calling function can handle luabind errors
//also assumes that   the "simulator" library is loaded into the envirment
void initSimTraderLua( LuaState&, trading::TradingContextPtr const&, std::string const& _func );

void enableTunnelDelays();


} // namespace simulator
} // namespace bb


#endif // BB_SIMULATOR_ISIMMKTDEST_H

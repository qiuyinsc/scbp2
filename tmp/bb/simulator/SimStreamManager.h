#ifndef BB_SIMULATOR_SIMSTREAMMANAGER_H
#define BB_SIMULATOR_SIMSTREAMMANAGER_H

/* Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved. */


#include <bb/core/MStreamCallback.h>
#include <bb/clientcore/HistMStreamManager.h>

namespace bb {
BB_FWD_DECLARE_SHARED_PTR( EventDistributor );
namespace simulator {

/** This is the trader facing Stream manager
 * The simulator creates this as it's stream manager so that it can
 * intercept the run method call
 *
 * We need to be able to pull data off the historic stream and send
 * it to the market for postprocessing before passing it onto the
 * rest of the system. In this way, the market can change the historic
 * data in any way it sees fit.
 */
class SimStreamManager : public HistMStreamManager, IMStreamCallback
{
public:
    virtual void addDataByMType( const source_t &, mtype_t );
    virtual void addDataByMTypeInstr( const source_t &, mtype_t, const bb::instrument_t & );

    virtual void run( IMStreamCallback* c );
    virtual void onMessage( const bb::Msg& msg );

    SimStreamManager( bb::timeval_t starttv, bb::timeval_t endtv );
    virtual ~SimStreamManager();

    /// the event distributor the market should use to get mkt data
    /// this gets data before anything else
    EventDistributorPtr getMktEventDistributor();

    /// return the history stream which feeds the mkt event dist
    /// should be used only if something wishes to inject something into
    /// the pre market stream
    HistMStreamManagerPtr getMktHistMStreamManager();

private:
    /// data stream, access to files and whatnot
    HistMStreamManagerPtr m_histManager;

    /// distribute events directly to the market
    /// this idea is to bypass the other crap... (tm)
    EventDistributorPtr m_mktEventDistributor;

    /// original callback from the main app
    IMStreamCallback* m_callback;

    /// next historic data message
    const Msg* m_nextHistMsg;

    bb::timeval_t m_rejectTime;

};
BB_DECLARE_SHARED_PTR( SimStreamManager );

}
}

#endif // BB_SIMULATOR_SIMSTREAMMANAGER_H

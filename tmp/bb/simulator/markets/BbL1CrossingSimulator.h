#ifndef BB_SIMULATOR_MARKETS_BBL1CROSSINGSIMULATOR_H
#define BB_SIMULATOR_MARKETS_BBL1CROSSINGSIMULATOR_H

/* Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved. */


#include <vector>

#include <bb/core/Subscription.h>
#include <bb/core/smart_ptr.h>
#include <bb/core/source.h>
#include <list>
#include <bb/core/timeval.h>
#include <bb/core/ostatus.h>
#include <bb/simulator/SimTypes.h>
#include <bb/simulator/SimUtils.h>
#include <memory>
#include <bb/core/Scripting.h>

namespace bb {
class instrument_t;
class BbL1TradeMsg;
BB_FWD_DECLARE_SHARED_PTR( MsgHandler );
BB_FWD_DECLARE_SHARED_PTR( ClientContext );
namespace simulator {

BB_FWD_DECLARE_SHARED_PTR( IMarketInternalDelays );
BB_FWD_DECLARE_SHARED_PTR( IOrderManager );
BB_FWD_DECLARE_SHARED_PTR( ICrossingTimes );


class BbL1CrossingSimulator
    : public virtual IOrderHandler
    , protected MarketDestination
    , protected TimeProviderForwarder
    , protected OrderManagerForwarder
{
public:
    BbL1CrossingSimulator( ClientContextPtr const& hist_cc,
                           bb::sources_t const& src,
                           int vbose
                           ,
                           IOrderManagerPtr const& om,
                           IMarketInternalDelaysCPtr const& _internal,
                           ICrossingTimesCPtr const& );
    virtual ~BbL1CrossingSimulator();
    BB_DECLARE_SCRIPTING();

private:
    //Impl IOrderHandler
    bool canAccept( trading::OrderCPtr const& ) const;
    bool canCancel( trading::OrderCPtr const& ) const;
    void activateOrder( trading::OrderPtr const& );
    void deactivateOrder( trading::OrderPtr const& ); //may be called more than once????
    IMarketInternalDelaysCPtr getMarketInternalDelays() const;
    bool addInstrument( bb::instrument_t const& instr, const IBookSpecPtr& sim_book_spec = IBookSpecPtr(),
                        const IOrderBookSpecPtr& order_book_spec = IOrderBookSpecPtr() );


    void onBbL1Tick( const BbL1TradeMsg& msg );
    ClientContextPtr m_spHistCC;
    int m_verboseLevel;
    typedef std::list<trading::OrderPtr> OrderList;
    OrderList m_active_orders;
    IMarketInternalDelaysCPtr m_internaldelays;
    std::vector<MsgHandlerPtr> m_spBbL1TickHandlers;
    bb::sources_t m_sources;
    ICrossingTimesCPtr m_crossingtimes;
    std::vector<bb::symbol_t> m_opened;
    std::vector<bb::symbol_t> m_closed;
    std::vector<bb::symbol_t> m_listening;
};
BB_DECLARE_SHARED_PTR( BbL1CrossingSimulator );
} // namespace simulator
} // namespace bb

#endif // BB_SIMULATOR_MARKETS_BBL1CROSSINGSIMULATOR_H

#ifndef BB_SIMULATOR_MARKETS_PROACTIVEDEST_H
#define BB_SIMULATOR_MARKETS_PROACTIVEDEST_H

/* Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved. */

#include <bb/core/Dispose.h>
#include <bb/core/Scripting.h>
#include <bb/core/TimeProvider.h>
#include <bb/clientcore/MsgHandler.h>
#include <bb/clientcore/IBook.h>
#include <bb/clientcore/TickProvider.h>
#include <bb/trading/Order.h>
#include <bb/simulator/ISimMktDest.h>
#include <bb/simulator/SimTypes.h>
#include <bb/simulator/OrderManager.h>
#include <bb/core/ValHolder.h>
#include <bb/core/smart_ptr.h>

namespace bb {
class Msg;
BB_FWD_DECLARE_SHARED_PTR( IBookBuilder );
BB_FWD_DECLARE_SHARED_PTR( ClientContext );
BB_FWD_DECLARE_SHARED_PTR( BbL1Book );

namespace trading {
BB_FWD_DECLARE_SHARED_PTR( HistTradingContext );
}

namespace simulator {
BB_FWD_DECLARE_SHARED_PTR( IMarketInternalDelays );
class ProactiveDest
    : public virtual IOrderHandler
    , protected IBookListener
    , protected ITickListener
    , protected MarketDestination
    , protected TimeProviderForwarder
    , protected OrderManagerForwarder
{
public:
    BB_DECLARE_SCRIPTING();

    enum CrossType { OPEN, CLOSE };

    static void initBatsProactiveSimulator( const trading::HistTradingContextPtr& htc, const std::set<instrument_t>& instrs );

    ProactiveDest(
        bb::mktdest_t _dest,
        const trading::HistTradingContextPtr& htc,
        bb::symbol_t sym,
        IBookBuilderPtr const& spBookBuilder,
        int _vbose,
        IOrderManagerPtr const& om, IMarketInternalDelaysCPtr const& _internal );

    ~ProactiveDest();
    void onBookChanged( const IBook* pBook, const Msg* pMsg, int32_t bidLevelChanged, int32_t askLevelChanged );
    void onBookFlushed( const IBook* pBook, const Msg* pMsg ) {}
    std::string getDesc() const;
    void setDesc( std::string const& );
    int getVerbose() const;
    void setVerbose( int );

    void onTickReceived( const bb::ITickProvider*, const bb::TradeTick& );
    void onOpenTick( const bb::ITickProvider* tp, const bb::TradeTick& tick );
    void onCloseTick( const bb::ITickProvider* tp, const bb::TradeTick& tick );

private:
    bool canAccept( trading::OrderCPtr const& ) const;
    bool canCancel( trading::OrderCPtr const& ) const;
    void activateOrder( trading::OrderPtr const& );
    void deactivateOrder( trading::OrderPtr const& ); //may be called more than once????
    IMarketInternalDelaysCPtr getMarketInternalDelays() const;
    bool addInstrument( bb::instrument_t const& instr, const IBookSpecPtr& sim_book_spec = IBookSpecPtr(),
                        const IOrderBookSpecPtr& order_book_spec = IOrderBookSpecPtr() );
    void checkAndHandleFills( trading::OrderPtr const& simord );
    void checkAndHandleCrossFills( CrossType cross, const bb::ITickProvider*, const bb::TradeTick& tick, trading::OrderPtr const& simord  );
    void handleMarketOrder( trading::OrderPtr const& simord, IBookPtr const& book );

    void checkOrdersAgainstBook( trading::OrderList const& order_list, const char* ba_label );

private:
    BbL1BookPtr m_l1book;

    typedef bb::ValHolder<boost::scoped_ptr<trading::OrderList>, bb::side_t, 2> order_holder_t;

    std::string m_mydesc;
    int m_verboseLevel;
    IMarketInternalDelaysCPtr m_internaldelays;
    friend std::ostream& operator<<( std::ostream& out, const ProactiveDest& mds );
    order_holder_t m_simOrders;

    bb::symbol_t m_sym;
};
BB_DECLARE_SHARED_PTR( ProactiveDest );
} // namespace simulator
} // namespace bb


#endif // BB_SIMULATOR_MARKETS_PROACTIVEDEST_H

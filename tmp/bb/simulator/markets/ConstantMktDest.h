#ifndef BB_SIMULATOR_MARKETS_CONSTANTMKTDEST_H
#define BB_SIMULATOR_MARKETS_CONSTANTMKTDEST_H

/* Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved. */


#include <vector>

#include <bb/core/smart_ptr.h>
#include <bb/core/timeval.h>
#include <bb/core/ostatus.h>
#include <bb/simulator/SimTypes.h>
#include <bb/simulator/SimUtils.h>
#include <memory>
#include <bb/core/Scripting.h>

namespace bb {
class instrument_t;

namespace simulator {
BB_FWD_DECLARE_SHARED_PTR( IMarketInternalDelays );
BB_FWD_DECLARE_SHARED_PTR( IOrderManager );

/*
 *
 * This is used to always give back the same price for every order
 * Usage:
 * 1. For things that just accumulate near the open, and then does a MOC, this
 *   can dramatically speed up the simulation, since you can configure this to always give you the closing price
 *   no matter when it is called
 *
 */
class ConstantMktDest
    : public virtual IOrderHandler
    , protected MarketDestination
    , protected OrderManagerForwarder
{
public:
    ConstantMktDest( bb::tif_t tiftype, int vbose
                     , IOrderManagerPtr const& om, IMarketInternalDelaysCPtr const& _internal );

    BB_DECLARE_SCRIPTING();
    bool addInstrument( bb::instrument_t const& instr, double px, unsigned size );

private:
    //Impl IOrderHandler
    bool canAccept( trading::OrderCPtr const& ) const;
    bool canCancel( trading::OrderCPtr const& ) const;
    void activateOrder( trading::OrderPtr const& );
    bool addInstrument( bb::instrument_t const& instr, const IBookSpecPtr& sim_book_spec = IBookSpecPtr(),
                        const IOrderBookSpecPtr& order_book_spec = IOrderBookSpecPtr() );
    void deactivateOrder( trading::OrderPtr const& );
    IMarketInternalDelaysCPtr getMarketInternalDelays() const;
    int m_verboseLevel;
    IMarketInternalDelaysCPtr m_internaldelays;
    bb::tif_t m_tiftype;

    struct PriceData
    {
        PriceData( bb::symbol_t s, double p, unsigned x ) : m_sym( s ), m_price( p ), m_size( x ) {}
        bool operator<( bb::symbol_t s ) const
        {
            return m_sym < s;
        }
        friend  bool operator<( bb::symbol_t s, PriceData const& p )
        {
            return s < p.m_sym;
        }
        bb::symbol_t m_sym;
        double m_price;
        unsigned m_size;
    };
    std::vector<PriceData> m_data;
};
BB_DECLARE_SHARED_PTR( ConstantMktDest );
} // namespace simulator
} // namespace bb

#endif // BB_SIMULATOR_MARKETS_CONSTANTMKTDEST_H

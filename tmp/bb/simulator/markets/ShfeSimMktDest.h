#ifndef BB_SIMULATOR_MARKETS_SHFESIMMKTDEST_H
#define BB_SIMULATOR_MARKETS_SHFESIMMKTDEST_H

/* Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved. */

#include <bb/core/hash_map.h>
#include <bb/core/source.h>
#include <bb/core/instrument.h>
#include <bb/core/source.h>
#include <bb/core/oreason.h>
#include <bb/core/mktdest.h>
#include <bb/core/cxlstatus.h>
#include <bb/core/EFeedDest.h>
#include <bb/core/acct.h>
#include <bb/core/smart_ptr.h>
#include <bb/core/Dispose.h>

#include <bb/clientcore/Book.h>

#include <bb/simulator/markets/AsiaInstrOrderBook.h>
#include <bb/simulator/markets/AsiaQueueInstrOrderBook.h>
#include <bb/simulator/markets/SimMktDest.h>
#include <bb/simulator/SimMktDelays.h>
#include <bb/simulator/SimTypes.h>

#include <bb/trading/Order.h>
#include <bb/trading/TradingContext.h>

namespace bb {

namespace simulator {

class ShfeDelaysFactory : public DefaultDelaysFactory
{
public:
    ShfeDelaysFactory( bb::EFeedDest const& _tdlocation, bool lateFills = false );
    IMarketTransitDelaysCPtr getMarketTransitDelays( bb::mktdest_t const& mkt ) const;
    IMarketInternalDelaysCPtr getMarketInternalDelays( bb::mktdest_t const& ) const;

private:
    bool m_latefills;
};

BB_FWD_DECLARE_SHARED_PTR( IMarketInternalDelays );

class AsiaOrderHandler
    : public virtual IOrderHandler
    , protected MarketDestination
    , protected OrderManagerForwarder
{
public:
    AsiaOrderHandler( trading::TradingContextPtr const& hist_cc
                      , bb::mktdest_t mkt
                      , bb::source_t src
                      , int _vbose, IOrderManagerPtr const& om
                      , IMarketInternalDelaysCPtr const& _internal
                      , bool check_market_hours = true );
    virtual ~AsiaOrderHandler();

    void registerInstrToSrcMapping(const instrument_t& instr, const source_t& src) { m_instr2src[instr] = src; }

protected:
    virtual bool addInstrument( const bb::instrument_t& instr, const IBookSpecPtr& sim_book_spec = IBookSpecPtr(),
                                const IOrderBookSpecPtr& order_book_spec = IOrderBookSpecPtr() );

    bool isMarketOpen( const timeval_t& tv_of_day ) const;

    bool canAccept( trading::OrderCPtr const& ) const;
    bool canCancel( trading::OrderCPtr const& ) const;
    void activateOrder( trading::OrderPtr const& );
    void deactivateOrder( trading::OrderPtr const& ); //may be called more than once????
    IMarketInternalDelaysCPtr getMarketInternalDelays() const;

    bb::source_t m_source;
    bbext::hash_map<bb::instrument_t, IOrderBookPtr> m_orderBookTable;
    bbext::hash_map<bb::instrument_t, bb::source_t> m_instr2src;   // in case we have certain instruments coming from a different source
    int m_verboseLevel;
    IMarketInternalDelaysCPtr m_internaldelays;
    bb::IntrusiveWeakPtr<trading::TradingContext> m_tc;
    bb::IntrusiveWeakPtr<IOrderManager> m_om;
    IBookSpecPtr m_simBookSpec;
    IOrderBookSpecPtr m_orderBookSpec;
    bool m_bCheckMarketHours;
};
BB_DECLARE_SHARED_PTR( AsiaOrderHandler );

class CmeOrderHandler : public AsiaOrderHandler
{
public:
    CmeOrderHandler( trading::TradingContextPtr const& hist_cc
                     , bb::mktdest_t mkt
                     , bb::source_t src
                     , int _vbose, IOrderManagerPtr const& om
                     , IMarketInternalDelaysCPtr const& _internal
                     , bool check_market_hours = true );
protected:
    virtual bool addInstrument( const bb::instrument_t& instr, const IBookSpecPtr& sim_book_spec = IBookSpecPtr(),
                                const IOrderBookSpecPtr& order_book_spec = IOrderBookSpecPtr() );

};

class AsiaSimMktDestBase
    : public ISimMktDest
{
public:
    AsiaSimMktDestBase( const trading::HistTradingContextPtr& htc, mktdest_t mkt, const source_t& src, const IDelaysFactoryCPtr& delays,
                    bool bcheck_market_hours = true )
        : m_spOrderManager( new OrderManager( mkt, htc->getHistMStreamManager(), htc->getClockMonitor(),
                                              htc->getEventDistributor(), htc->getVerbose(),
                                              delays->getMarketTransitDelays( mkt ) ) )
    {
    }

    virtual void registerInstrToSrcMapping(const instrument_t& instr, const source_t& src)
    {
        m_spAsioOh->registerInstrToSrcMapping( instr, src );
    }

    virtual void addInstrument( const instrument_t& instr, const IBookSpecPtr& sim_book_spec = IBookSpecPtr(),
                                const IOrderBookSpecPtr& order_book_spec = IOrderBookSpecPtr() )
    {
        m_spOrderManager->addInstrument( instr, sim_book_spec, order_book_spec );
    }

protected:
    virtual void receiveOrder( const instrument_t& instr, dir_t dir, mktdest_t dest, double px, unsigned int size,
                               unsigned int orderid, int timeout, bool visible, tif_t tif, uint32_t oflags )
    {
        ISimMktDestPtr( m_spOrderManager )->receiveOrder( instr, dir, dest, px, size, orderid, timeout, visible, tif,
                                                          oflags );
    }

    virtual void receiveCancelOrder( acct_t order_acct, const instrument_t& instr, const unsigned int order_id )
    {
        ISimMktDestPtr( m_spOrderManager )->receiveCancelOrder( order_acct, instr, order_id );
    }

protected:
    OrderManagerPtr m_spOrderManager;
    AsiaOrderHandlerPtr m_spAsioOh;
};

template<mktdest_t MKT_T, typename OrderHandler>
class AsiaSimMktDest
    : public AsiaSimMktDestBase
{
public:
    AsiaSimMktDest( const trading::HistTradingContextPtr& htc, const source_t& src, const IDelaysFactoryCPtr& delays,
                    bool bcheck_market_hours = true )
        : AsiaSimMktDestBase( htc, MKT_T, src, delays, bcheck_market_hours )
    {
        m_spAsioOh = boost::make_shared<OrderHandler>( htc, MKT_T, src, htc->getVerbose(), m_spOrderManager,
                                                           delays->getMarketInternalDelays( MKT_T ),
                                                           bcheck_market_hours );
        m_spOrderManager->registerOrderHandler( m_spAsioOh );
    }
};

typedef AsiaSimMktDest<MKT_SHFE, AsiaOrderHandler> ShfeSimMktDest;
typedef AsiaSimMktDest<MKT_DCE, AsiaOrderHandler> DceSimMktDest;
typedef AsiaSimMktDest<MKT_CZCE, AsiaOrderHandler> ZceSimMktDest;
typedef AsiaSimMktDest<MKT_CFFEX, AsiaOrderHandler> CffexSimMktDest;
typedef AsiaSimMktDest<MKT_HKFE, AsiaOrderHandler> HkfeSimMktDest;
typedef AsiaSimMktDest<MKT_SGX, AsiaOrderHandler> SgxSimMktDest;
typedef AsiaSimMktDest<MKT_TOCOM, AsiaOrderHandler> TocomSimMktDest;
typedef AsiaSimMktDest<MKT_CME, CmeOrderHandler> CmeSimMktDest;
typedef AsiaSimMktDest<MKT_CEC, CmeOrderHandler> CecSimMktDest;
typedef AsiaSimMktDest<MKT_CBOT, CmeOrderHandler> CbotSimMktDest;
typedef AsiaSimMktDest<MKT_NYMEX, CmeOrderHandler> NymexSimMktDest;
typedef AsiaSimMktDest<MKT_TAIFEX, AsiaOrderHandler> TaifexSimMktDest;
typedef AsiaSimMktDest<MKT_TWSE, AsiaOrderHandler> TwseSimMktDest;
typedef AsiaSimMktDest<MKT_SHSE, AsiaOrderHandler> ShseSimMktDest;
typedef AsiaSimMktDest<MKT_SZSE, AsiaOrderHandler> SzseSimMktDest;
typedef AsiaSimMktDest<MKT_HKSE, AsiaOrderHandler> HkseSimMktDest;

BB_DECLARE_SHARED_PTR( ShfeSimMktDest );
BB_DECLARE_SHARED_PTR( DceSimMktDest );
BB_DECLARE_SHARED_PTR( ZceSimMktDest );
BB_DECLARE_SHARED_PTR( CffexSimMktDest );
BB_DECLARE_SHARED_PTR( HkfeSimMktDest );
BB_DECLARE_SHARED_PTR( SgxSimMktDest );
BB_DECLARE_SHARED_PTR( TocomSimMktDest );
BB_DECLARE_SHARED_PTR( CmeSimMktDest );
BB_DECLARE_SHARED_PTR( CecSimMktDest );
BB_DECLARE_SHARED_PTR( CbotSimMktDest );
BB_DECLARE_SHARED_PTR( NymexSimMktDest );
BB_DECLARE_SHARED_PTR( TaifexSimMktDest );
BB_DECLARE_SHARED_PTR( TwseSimMktDest );
BB_DECLARE_SHARED_PTR( ShseSimMktDest );
BB_DECLARE_SHARED_PTR( SzseSimMktDest );
BB_DECLARE_SHARED_PTR( HkseSimMktDest );

} // namespace simulator
} // namespace bb

#endif // BB_SIMULATOR_MARKETS_SHFESIMMKTDEST_H

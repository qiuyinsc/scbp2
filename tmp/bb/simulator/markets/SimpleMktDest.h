#ifndef BB_SIMULATOR_MARKETS_SIMPLEMKTDEST_H
#define BB_SIMULATOR_MARKETS_SIMPLEMKTDEST_H

/* Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved. */


#include <bb/core/smart_ptr.h>
#include <bb/core/mktdest.h>

#include <bb/clientcore/MStreamManager.h>
#include <bb/clientcore/EventDist.h>
#include <bb/clientcore/BookBuilder.h>
#include <bb/clientcore/SourceBooks.h> //TODO remove

#include <bb/simulator/SimMktContext.h>
#include <bb/simulator/SimTypes.h>

namespace bb {
namespace simulator {

BB_FWD_DECLARE_SHARED_PTR( IMarketInternalDelays );

class SimpleMktDest : public IOrderHandler, IEventDistListener
{
public:
    SimpleMktDest( bb::mktdest_t dest, bb::EventDistributorPtr mktEventDistributor,
                   bb::HistMStreamManagerPtr outStream, SimMktContextPtr mktContext,
                   bb::SourceBookSpecPtr sourceBookSpec );

    virtual void activateOrder( const bb::trading::OrderPtr& );
    virtual bool addInstrument( const bb::instrument_t& instr, const IBookSpecPtr& sim_book_spec = IBookSpecPtr(),
                                const IOrderBookSpecPtr& order_book_spec = IOrderBookSpecPtr() );
    virtual bool canAccept( const bb::trading::OrderCPtr& ) const;
    virtual bool canCancel( const bb::trading::OrderCPtr& ) const;
    virtual void deactivateOrder( const bb::trading::OrderPtr& );
    virtual IMarketInternalDelaysCPtr getMarketInternalDelays() const;
    virtual mktdest_t getMktDest() const;

    virtual void onEvent( const bb::Msg& msg );

private:
    IMarketInternalDelaysCPtr m_internaldelays;

    bb::mktdest_t m_mktdest;

    bb::EventDistributorPtr m_eventDist;
    bb::HistMStreamManagerPtr m_outStream;

    bb::EventDistributorPtr m_mktEventDist;

    bb::BookBuilderPtr m_bookBuilder;

    bb::SourceBookSpecPtr m_sourceBookSpec;

    // TEMP
    Subscription m_sub;
};
BB_DECLARE_SHARED_PTR( SimpleMktDest );

}
}

#endif // BB_SIMULATOR_MARKETS_SIMPLEMKTDEST_H

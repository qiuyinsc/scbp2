#ifndef BB_SIMULATOR_MARKETS_ASIAQUEUEINSTRORDERBOOK_H
#define BB_SIMULATOR_MARKETS_ASIAQUEUEINSTRORDERBOOK_H

/* Contents Copyright 2012 Athena Capital Research LLC. All Rights Reserved. */

#include <bb/core/LuaConfig.h>
#include <bb/simulator/markets/AsiaInstrOrderBook.h>
#include <bb/clientcore/ClientContext.h>
#include <bb/clientcore/IBook.h>

namespace bb {

BB_FWD_DECLARE_SHARED_PTR( BookBuilder );
BB_FWD_DECLARE_SHARED_PTR( LookAheadHistClientContext );
BB_FWD_DECLARE_SHARED_PTR( IndeterminateHistClientContext );
BB_FWD_DECLARE_SHARED_PTR( IBook );
BB_FWD_DECLARE_SHARED_PTR( IPriceProvider );

namespace simulator {

class AsiaQueueSimulationInfo : public bb::trading::OrderInfoSlot
{
public:
    AsiaQueueSimulationInfo();
    BB_DECLARE_ORDER_INFO();

    virtual ~AsiaQueueSimulationInfo() {};

    AsiaQueueSimulationInfo& setPositionInQueue( uint32_t position ) { m_positionInQueue = position; return *this; };
    uint32_t getPositionInQueue() { return m_positionInQueue; };

    AsiaQueueSimulationInfo& setLastUpdateTime( timeval_t time ) { m_lastUpdateTime = time; return *this; };
    timeval_t getLastUpdateTime() { return m_lastUpdateTime; };

    AsiaQueueSimulationInfo& setPositionInQueue( uint32_t position, timeval_t time )
    {
        m_positionInQueue = position;
        m_lastUpdateTime = time;
        return *this;
    };

protected:
    uint32_t m_positionInQueue;
    timeval_t m_lastUpdateTime;
};

struct AdverseFillsConfig : public bb::LuaConfig<AdverseFillsConfig>
{
    static ptime_duration_t DEFAULT_LOOK_AHEAD_TIME;
    static double DEFAULT_FRONT_OF_QUEUE_RETURNS;

    ptime_duration_t look_ahead_time;
    double front_of_queue_returns;

    bool enabled() const { return front_of_queue_returns != DEFAULT_FRONT_OF_QUEUE_RETURNS; }

    AdverseFillsConfig()
        : look_ahead_time( DEFAULT_LOOK_AHEAD_TIME )
        , front_of_queue_returns( DEFAULT_FRONT_OF_QUEUE_RETURNS ){}

    static void describe()
    {
        Self::create()
            .param( "look_ahead_time"        , &Self::look_ahead_time )
            .param( "front_of_queue_returns" , &Self::front_of_queue_returns )
            ;
    }
};

struct LookAheadExchangeConfig : public bb::LuaConfig<LookAheadExchangeConfig>
{
    bool enabled, verbose, use_fixed_look_ahead_time;
    ptime_duration_t fixed_look_ahead_time, max_advance_duration;

    LookAheadExchangeConfig()
        : enabled( false )
        , verbose( false )
        , use_fixed_look_ahead_time( false )
        , fixed_look_ahead_time( boost::posix_time::milliseconds( 500 ) )
        , max_advance_duration( boost::posix_time::seconds( 5.0 ) ){}

    static void describe()
    {
        Self::create()
            .param( "enabled"                     , &Self::enabled )
            .param( "verbose"                     , &Self::verbose )
            .param( "use_fixed_look_ahead_time"   , &Self::use_fixed_look_ahead_time )
            .param( "fixed_look_ahead_time"       , &Self::fixed_look_ahead_time )
            .param( "max_advance_duration"        , &Self::max_advance_duration )
            ;
    }
};

#if !defined( SWIG )
class BookUpdateClientContextAdvancer : public bb::ClientContextAdvancer, IBookListener
{
public:
    static ptime_duration_t DEFAULT_MAX_ADVANCE_DURATION;

    BookUpdateClientContextAdvancer( const IBookPtr&, const ptime_duration_t max_advance_duration = DEFAULT_MAX_ADVANCE_DURATION );
    virtual ~BookUpdateClientContextAdvancer();

    virtual void onBookChanged( const bb::IBook* pBook, const bb::Msg* pMsg,
                                int32_t bidLevelChanged, int32_t askLevelChanged );
protected:
    const IBookPtr m_book;
};
BB_DECLARE_SHARED_PTR( BookUpdateClientContextAdvancer );
#endif // defined( SWIG )

class AsiaQueueInstrOrderBook
    : public AsiaInstrOrderBook
{
public:
    AsiaQueueInstrOrderBook( const instrument_t& instr
                             , bb::IntrusiveWeakPtr<trading::TradingContext> const& tc
                             , source_t src
                             , bb::IntrusiveWeakPtr<IOrderManager> const& om
                             , IMarketInternalDelaysCPtr const& _internaldelays
                             , const IBookSpecPtr& sim_book_spec
                             , const bool& use_split_trades
                             , const AdverseFillsConfig& adverse_fills_config
                             , const LookAheadExchangeConfig& look_ahead_book_config
                             );

    virtual ~AsiaQueueInstrOrderBook();

    virtual void activateOrder( trading::OrderPtr const& );
    virtual void applyAdverseFillAdjustment( trading::OrderPtr const& );

protected:
    virtual bool oidMatchesActiveOid( unsigned int oid );


    virtual uint32_t checkAndApplyLookAheadFills( side_t book_side,
                                                  double la_book_price,
                                                  uint32_t la_book_size,
                                                  const IOrderBook::FillMode& m );

    virtual void checkLookAheadBookExecution( IOrderBook::FillMode m );


    virtual void checkAndApplyFills( const side_t& side, const double& price, const uint32_t& size,
                                         const IOrderBook::FillMode& m );

    virtual void checkBookExecution( IOrderBook::FillMode m );

    static uint32_t getBookSizeAtPrice( const IBookPtr book,side_t side, double price );
    void addQueuePosition( bb::trading::OrderPtr const& order, const IBookPtr& book );
    void updateQueuePositions( const IBookPtr& book,
                               const side_t& side,
                               const double& price,
                               const uint32_t& size,
                               const IOrderBook::FillMode& m );

    void updateQueuePosition( const trading::OrderPtr& o,
                              const IBookPtr& book,
                              const side_t& side,
                              const double& price,
                              const uint32_t& size,
                              const IOrderBook::FillMode& m );

    bb::IntrusiveWeakPtr<trading::TradingContext> m_context;

    bool m_useLookAheadBook;

    AdverseFillsConfig m_adverseFills;
    LookAheadExchangeConfig m_lookAheadExchange;

    SimHistClientContextPtr m_lookAheadExchangeCC;
    LookAheadHistClientContextPtr m_adverseFillsCC;

    IBookPtr m_lookAheadBook;

    //map of the time an order was compared to lookAhead book
    typedef std::map<uint32_t, timeval_t> LookAheadOrderTimeMap;
    LookAheadOrderTimeMap m_lookAheadBookOrderCheckTime;

    IPriceProviderPtr m_adverseFillsPP;

    bb::trading::OrderPtr m_activeOrder;
};
BB_DECLARE_SHARED_PTR( AsiaQueueInstrOrderBook );

class CmeQueueInstrOrderBook : public AsiaQueueInstrOrderBook
{
public:
    CmeQueueInstrOrderBook( const instrument_t& instr
                            , bb::IntrusiveWeakPtr<trading::TradingContext> const& tc
                            , source_t src
                            , bb::IntrusiveWeakPtr<IOrderManager> const& om
                            , IMarketInternalDelaysCPtr const& _internaldelays
                            , const IBookSpecPtr& sim_book_spec
                            , const bool& use_split_trades
                            , const AdverseFillsConfig& adverse_fills_config
                            , const LookAheadExchangeConfig& look_ahead_book_config
        );

    virtual void onBookChanged( const IBook* pBook, const Msg* pMsg,
                                int32_t bidLevelChanged, int32_t askLevelChanged );
};

class AsiaQueueInstrOrderBookSpec
    : public AsiaInstrOrderBookSpec
{
public:
    BB_DECLARE_SCRIPTING();

    AsiaQueueInstrOrderBookSpec();

    virtual IOrderBookPtr construct( const instrument_t& instrument,
                                     const source_t& source,
                                     const IntrusiveWeakPtr<trading::TradingContext>& context,
                                     const IntrusiveWeakPtr<IOrderManager>& manager,
                                     const IMarketInternalDelaysCPtr& internal_delays,
                                     const IBookSpecPtr& book_spec );

    virtual void print( std::ostream& os, const LuaPrintSettings& ps ) const;

protected:
    ptime_duration_t m_lookAheadTime; // deprecated. use adverseFillsConfig
    double m_frontOfQueueReturns; // deprecated. use adverseFillsConfig

    luabind::object m_adverseFillsConfig;
    luabind::object m_lookAheadExchangeConfig;
};

BB_DECLARE_SHARED_PTR( AsiaQueueInstrOrderBookSpec );


} // namespace simulator
} // namespace bb

#endif // BB_SIMULATOR_MARKETS_ASIAQUEUEINSTRORDERBOOK_H

#ifndef BB_SIMULATOR_MARKETS_SIMMKTDEST_H
#define BB_SIMULATOR_MARKETS_SIMMKTDEST_H

/* Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved. */


#include <bb/core/hash_map.h>
#include <bb/core/Dispose.h>
#include <bb/core/Scripting.h>
#include <bb/core/TimeProvider.h>

#include <bb/clientcore/MsgHandler.h>
#include <bb/clientcore/IBook.h>
#include <bb/clientcore/ClockMonitor.h>
#include <bb/clientcore/TickProvider.h>

#include <bb/trading/Order.h>

#include <bb/simulator/ISimMktDest.h>
#include <bb/simulator/SimTypes.h>
#include <bb/simulator/OrderManager.h>

namespace bb {
class Msg;
BB_FWD_DECLARE_SHARED_PTR( IBookBuilder );
BB_FWD_DECLARE_SHARED_PTR( SourceTickFactory );
namespace trading {
BB_FWD_DECLARE_SHARED_PTR( TradingContext );
} //trading
namespace simulator {
BB_FWD_DECLARE_SHARED_PTR( IMarketInternalDelays );
BB_FWD_DECLARE_SHARED_PTR( SimTradingContext );

/// Note:  To simplify things, uses the orderid as a unique identifier.  The
/// SimMktDest enforces this by requiring a unique orderid for each order.
/// this will need to change if we ever want these classes to better represent a market on which
/// multiple traders can place orders at the same time.
/// Account/symbol/orderid is the complete unique identifier, but right now one SimTradeDemonClient/
/// SimMktDest pair is created per account/symbol, so only the orderid is needed.
class SimMktDest
    : public virtual IOrderHandler
    , protected IBookListener
    , protected ITickListener
    , protected MarketDestination
    , protected TimeProviderForwarder
    , protected OrderManagerForwarder
{
public:
    /// create structures to hold bid and ask orders for any instrument we are simulating
    typedef boost::scoped_ptr<trading::OrderList> order_holder_t[2];
    BB_DECLARE_SCRIPTING();
    class InstrumentData
    {
public:
        InstrumentData();
        ~InstrumentData();
        void clearSimOrders();
        void each_order( const boost::function< void(trading::OrderPtr, IBookPtr) >& op );

        order_holder_t m_simOrders;
        IBookPtr m_book;
        ITickProviderPtr m_ticks;
        ITickListener* m_ticklistener;
        IBookListener* m_booklistener;

    };
    BB_DECLARE_SHARED_PTR( InstrumentData );
    typedef bbext::hash_map<instrument_t, boost::shared_ptr<InstrumentData> > InstrumentDataTable;

public:
    SimMktDest(
        bb::mktdest_t _dest,
        trading::TradingContextPtr const& _tc,
        source_t _src,
        IBookBuilderPtr const& spBookBuilder,
        int _vbose,
        IOrderManagerPtr const& om, IMarketInternalDelaysCPtr const& _internal );

    virtual ~SimMktDest();

    std::string getDesc() const;
    void setDesc( std::string const& );
    int getVerbose() const;
    void setVerbose( int );

private:
    /// BookListener interface
    void onBookChanged( const IBook* pBook, const Msg* pMsg, int32_t bidLevelChanged, int32_t askLevelChanged );
    void onBookFlushed( const IBook* pBook, const Msg* pMsg ) {}

    /// ITickProvider interface
    void onTickReceived( const ITickProvider* tp, TradeTick const& tick );

    /// Unschedule all wakeup calls and reset for new day
    void handleEndDayWakeup();
    virtual void handleCloseWakeup( const timeval_t& close_tv );
    virtual void handleOpenWakeup( const timeval_t& open_tv );

    /// Adds support for simulating trading for instrument. Gets book for instrument and source,
    /// and adds instrument to internal orders holding structure.
    InstrumentDataPtr addInstrument_i( const instrument_t& instr );


    /// Return true if instr has already been added, false otherwise
    virtual bool isInstrumentSimulated( const instrument_t& instr ) const;


    bool canAccept( trading::OrderCPtr const& ) const;
    bool canCancel( trading::OrderCPtr const& ) const;
    void activateOrder( trading::OrderPtr const& );
    void deactivateOrder( trading::OrderPtr const& ); //may be called more than once????
    IMarketInternalDelaysCPtr getMarketInternalDelays() const;
    bool addInstrument( bb::instrument_t const& instr, const IBookSpecPtr& sim_book_spec = IBookSpecPtr(),
                        const IOrderBookSpecPtr& order_book_spec = IOrderBookSpecPtr() );

    bool addInstrumentDefault( bb::instrument_t const& instr ) { return addInstrument( instr ); }


    virtual void checkAndHandleFills( trading::OrderPtr const& simord );
    virtual void handleMarketOrder( trading::OrderPtr const& simord, IBookPtr const& book );

    void checkOrdersAgainstBook( trading::OrderList const& order_list, const char* ba_label );
    void checkOrdersAgainstTick( trading::OrderPtr const& order, const TradeTick& tick );
    void each_order( const boost::function<void(trading::OrderPtr const&, IBookPtr const& book)>& order_op );
    void processMarketOnOpenOrder( trading::OrderPtr const& order, IBookPtr const& book );

    void handleMarketOnCloseOrder( trading::OrderPtr const & order, IBookPtr const & book
                                   , int32_t counters[2], const timeval_t &close_tv );

private:
    ClockMonitorPtr m_cm;
    source_t m_src;
    bb::IntrusiveWeakPtr<trading::TradingContext> m_pTradingContext;

    SourceTickFactoryPtr m_spSourceTickFactory;
    IBookBuilderPtr m_spBookBuilder;
    std::string m_mydesc;
    InstrumentDataTable m_instrumentDataTable;
    Subscription m_endOfDayWakeupSub, m_atOpenWakeupSub, m_atCloseWakeupSub;
    int m_verboseLevel;
    IMarketInternalDelaysCPtr m_internaldelays;

    friend std::ostream& operator<<( std::ostream& out, const SimMktDest& mds );
};

BB_DECLARE_SHARED_PTR( SimMktDest );

} // namespace simulator
} // namespace bb


#endif // BB_SIMULATOR_MARKETS_SIMMKTDEST_H

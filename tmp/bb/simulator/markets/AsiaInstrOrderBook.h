#ifndef BB_SIMULATOR_MARKETS_ASIAINSTRORDERBOOK_H
#define BB_SIMULATOR_MARKETS_ASIAINSTRORDERBOOK_H

/* Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved. */


#include <bb/core/hash_map.h>
#include <bb/core/source.h>
#include <bb/core/instrument.h>
#include <bb/core/source.h>
#include <bb/core/oreason.h>
#include <bb/core/mktdest.h>
#include <bb/core/cxlstatus.h>
#include <bb/core/EFeedDest.h>
#include <bb/core/acct.h>
#include <bb/core/smart_ptr.h>
#include <bb/core/Dispose.h>

#include <bb/clientcore/IBook.h>
#include <bb/clientcore/TickProvider.h>

#include <bb/trading/Order.h>
#include <bb/simulator/SimUtils.h>
#include <bb/simulator/markets/IOrderBook.h>

namespace bb {

BB_FWD_DECLARE_SHARED_PTR( IBookBuilder );

namespace simulator {

BB_FWD_DECLARE_SHARED_PTR( OrderManager );

namespace {
class FillInfo
{
public:
    FillInfo( const trading::OrderPtr& o, double px, double sz )
        : order( o ), fill_price( px ), fill_size( sz ) {}
    trading::OrderPtr order;
    double fill_price;
    uint32_t fill_size;
};
}

class AsiaInstrOrderBook
    : public IOrderBook
    , protected ITickListener
    , protected IBookListener
    , protected OrderManagerForwarder
{
public:
    AsiaInstrOrderBook( const instrument_t& instr
                        , bb::IntrusiveWeakPtr<trading::TradingContext> const& tc
                        , source_t src
                        , bb::IntrusiveWeakPtr<IOrderManager> const& om
                        , IMarketInternalDelaysCPtr const& _internaldelays
                        , const IBookSpecPtr& sim_book_spec
                        , const bool& use_split_trades );

    virtual ~AsiaInstrOrderBook();

    virtual void activateOrder( trading::OrderPtr const& );
    virtual void deactivateOrder( trading::OrderPtr const& ); //may be called more than once????

    static IBookPtr build_book( const IBookBuilderPtr& book_builder, const instrument_t&, source_t, const IBookSpecPtr& sim_book_spec );
protected:

    static bb::fillflag_t getFillFlag( trading::OrderInfo const& o );
    virtual void checkAndApplyFills( const side_t& side, const double& price, const uint32_t& size,
                                     const IOrderBook::FillMode& m );
    virtual void onTickReceived( const ITickProvider* tp, const TradeTick& tick );

    virtual void onBookChanged( const IBook* pBook, const Msg* pMsg,
                                int32_t bidLevelChanged, int32_t askLevelChanged )
    {
        checkBookExecution( IOrderBook::FILL_FROM_BOOKUPDATE );
    }

    virtual void checkBookExecution( IOrderBook::FillMode m );

    virtual bool notifyFill( trading::OrderPtr const& simord,
                             double price,
                             unsigned sz,
                             bb::fillflag_t fflag = bb::fillflag_t(),
                             const ptime_duration_t& add_delay = ptime_duration_t() );

    virtual void labelAutoOffsetOrder( trading::OrderPtr const& o );

    trading::OrderList& orderList( dir_t dir )
    {
        if( dir == BUY )
            return m_buyOrderList;
        else
            return m_selOrderList;
    }

    instrument_t m_instr;
    IBookPtr m_spBook;
    ITickProviderPtr m_spTickProvider;
    trading::OrderList m_buyOrderList;
    trading::OrderList m_selOrderList;
    uint32_t m_tickVolMultiple;
    bool m_useSplitTrades;
    int32_t m_offsets[bb::DIR_INVALID];


};
BB_DECLARE_SHARED_PTR( AsiaInstrOrderBook );

class AsiaInstrOrderBookSpec
    : public IOrderBookSpec
{
public:
    BB_DECLARE_SCRIPTING();

    static const bool DEFAULT_USE_SPLIT_TRADES;

    AsiaInstrOrderBookSpec();

    virtual IOrderBookPtr construct( const instrument_t& instrument,
                                     const source_t& source,
                                     const IntrusiveWeakPtr<trading::TradingContext>& context,
                                     const IntrusiveWeakPtr<IOrderManager>& manager,
                                     const IMarketInternalDelaysCPtr& internal_delays,
                                     const IBookSpecPtr& book_spec );

    virtual void print( std::ostream& os, const LuaPrintSettings& ps ) const;

protected:
    bool m_useSplitTrades;
};

BB_DECLARE_SHARED_PTR( AsiaInstrOrderBookSpec );

} // namespace simulator
} // namespace bb

#endif // BB_SIMULATOR_MARKETS_ASIAINSTRORDERBOOK_H

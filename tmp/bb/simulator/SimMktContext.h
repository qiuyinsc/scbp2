#ifndef BB_SIMULATOR_SIMMKTCONTEXT_H
#define BB_SIMULATOR_SIMMKTCONTEXT_H

/* Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved. */


#include <bb/core/smart_ptr.h>

#include <bb/clientcore/ClientContext.h>
#include <bb/clientcore/MStreamManager.h>
#include <bb/clientcore/EventDist.h>

#include <bb/simulator/SimClientContext.h>

namespace bb {
namespace simulator {

/** This Client Context can be used by a market when providing a context to
 * BookBuilders and TickProviders. It creates a dummy HistStream and an event
 * distributor which is used to just receive passed messages */
class SimMktContext : public ClientContext
{
public:
    SimMktContext( SimClientContextPtr simClientContext );

    /// this should return the SimStreamManager
    /// run gets called on this
    virtual IMStreamManagerPtr getMStreamManager();

private:
    /// nothing should ever be calling methods on this stream!!
    class NoOpStream : public IMStreamManager
    {
public:
        virtual void addDataByMType( const source_t&, mtype_t ) { fail(); };
        virtual void addDataByMTypeInstr( const source_t&, mtype_t, const instrument_t& ) { fail(); };
        virtual void exit() { fail(); };
        virtual void run( IMStreamCallback* c ) { fail(); };
        virtual void sigAction( int sig, const UnixSigCallback& cb ) { fail(); };
        virtual void setNextTimeout( const ptime_duration_t& timeout) { fail(); };
private:
        void fail()
        {
            //TODO not everything should fail...
            //BB_ASSERT(false);
        }
    };
    BB_DECLARE_SHARED_PTR( NoOpStream );

    /// no op stream, calling anything will fail!
    NoOpStreamPtr m_noOpStream;
};
BB_DECLARE_SHARED_PTR( SimMktContext );

} // namespace simulator
} // namespace bb

#endif // BB_SIMULATOR_SIMMKTCONTEXT_H

#ifndef BB_SIGNALS_SIGDECAY_H
#define BB_SIGNALS_SIGDECAY_H

/* Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved. */


#include <bb/clientcore/TickProvider.h>
#include <bb/signals/Signal.h>
#include <bb/signals/SignalSpec.h>

namespace bb {
namespace signals {

class SigDecay
    : public SignalSmonImpl
{
public:
    typedef enum {
        FAST_DECAY = 0,
        SLOW_DECAY = 1
    } DecaySpeed;

    SigDecay(const instrument_t& instr, const std::string &desc, ClockMonitorPtr _cm,
             IPriceProviderCPtr spRefpp, int _numw, int _numvw, int _vbose=0, 
             ReturnMode returnMode=DIFF );
    virtual void reset();

    virtual void updateVars(double px, int sz);
    void initSlowerDecays();
    void initFasterDecays();

    void setUseDecaySpeed(DecaySpeed _decay_speed)
        { decay_speed = _decay_speed; }

    static const char* DecaySpeed2str(DecaySpeed s);

protected:
    virtual void logClockNotice(int reason);
    virtual void recomputeState() const;
    // IPriceProvider Listener
    virtual void onPriceChanged( const IPriceProvider& pp );

    std::vector<double> vars;
    std::vector<double> varws;
    std::vector<double> decays;
    bool first_update_fg;
    DecaySpeed decay_speed;
    int numw, numvw, eventcnt;
    IPriceProviderCPtr m_spRefpp;
    Subscription m_spRefPPSub;
    ReturnMode m_returnMode;

    static const int ScaleOutputVal = 1;
};


/// If multiple TickProviders are added, the signal state is updated whenever
/// any of the TickProvider's state is updated.
class SigDecayTick
    : public SigDecay
    , private ITickListener
{
public:
    SigDecayTick(const instrument_t& instr, const std::string &desc,
                 ClockMonitorPtr clockm, IPriceProviderCPtr spRefpp,
                 ITickProviderCPtr spTickProvider, int _numw, int _numvw,  int _vbose, 
                 ReturnMode returnMode);
    SigDecayTick(const instrument_t& instr, const std::string &mydesc, ClockMonitorPtr clockm,
                 IPriceProviderCPtr spRefpp,
                 const std::vector<ITickProviderCPtr>& vTickProviders, int _numw, int _numvw,  int _vbose, 
                 ReturnMode returnMode);
    virtual ~SigDecayTick();

protected:
    /// This will be called when the ITickProvider's receives a new TradeTick.
    /// That last trade tick is passed.
    /// Spurious calls are allowed.  tp will never be NULL.
    virtual void onTickReceived( const ITickProvider* tp, const TradeTick& tick );

    // we don't care about this
    virtual void onTickVolumeUpdated( const ITickProvider* tp, uint64_t totalVolume ) {}

protected:
    std::vector<ITickProviderCPtr>    m_vTickProviders;
};
BB_DECLARE_SHARED_PTR(SigDecayTick);


/// SignalSpec for SigDecayTick
class SigDecayTickSpec : public SignalSpec
{
public:
    BB_DECLARE_SCRIPTING();

    SigDecayTickSpec();

    virtual instrument_t getInstrument() const { return m_instrument; }
    virtual ISignalPtr build(SignalBuilder *builder) const;
    virtual void checkValid() const;
    virtual void hashCombine(size_t &result) const;
    virtual bool compare(const ISignalSpec *other) const;
    virtual void print(std::ostream &o, const LuaPrintSettings &ps) const;
    virtual void getDataRequirements(IDataRequirements *rqs) const;
    virtual SigDecayTickSpec *clone() const;

    instrument_t m_instrument;
    int m_numWVars;
    int m_numVWVars;

    signals::SigDecay::DecaySpeed m_decaySpeed;
    ReturnMode m_returnMode;

    sources_t m_sources;
};
BB_DECLARE_SHARED_PTR(SigDecayTickSpec);



class SigDecayQuote
    : public SigDecay
    , protected IBookListener
{
public:
    SigDecayQuote(const instrument_t& instr, const std::string &mydesc, ClockMonitorPtr clockm,
                  IPriceProviderPtr spRefpp, IBookPtr spBook, int _numw, int _numvw, int _vbose,
                  ReturnMode returnMode);
    virtual ~SigDecayQuote();

protected:
    /// Invoked when the subscribed Book changes.
    virtual void onBookChanged( const IBook* pBook, const Msg* pMsg,
                                int32_t bidLevelChanged, int32_t askLevelChanged );
    /// Invoked when the subscribed Book is flushed.
    virtual void onBookFlushed( const IBook* pBook, const Msg* pMsg );

protected:
    IBookPtr m_spBook;
};
BB_DECLARE_SHARED_PTR(SigDecayQuote);


/// SignalSpec for SigDecayQuote
class SigDecayQuoteSpec : public SignalSpec
{
public:
    BB_DECLARE_SCRIPTING();

    SigDecayQuoteSpec();
    SigDecayQuoteSpec(const SigDecayQuoteSpec &e);

    virtual instrument_t getInstrument() const { return m_book->getInstrument(); }
    virtual ISignalPtr build(SignalBuilder *builder) const;
    virtual void checkValid() const;
    virtual void hashCombine(size_t &result) const;
    virtual bool compare(const ISignalSpec *other) const;
    virtual void print(std::ostream &o, const LuaPrintSettings &ps) const;
    virtual void getDataRequirements(IDataRequirements *rqs) const;
    virtual SigDecayQuoteSpec *clone() const;

    IBookSpecCPtr m_book;
    int m_numWVars, m_numVWVars;
    signals::SigDecay::DecaySpeed m_decaySpeed;
    sources_t m_sources;
    ReturnMode m_returnMode;
};
BB_DECLARE_SHARED_PTR(SigDecayQuoteSpec);

void luaprint(std::ostream &out, bb::signals::SigDecay::DecaySpeed speed, LuaPrintSettings const &ps);

} // namespace signals
} // namespace bb

#endif // BB_SIGNALS_SIGDECAY_H

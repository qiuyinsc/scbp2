#ifndef BB_SIGNALS_PRODUCTENTRYSIGNALSLOADER_H
#define BB_SIGNALS_PRODUCTENTRYSIGNALSLOADER_H

/* Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved. */


#include <vector>
#include <bb/clientcore/ConfigFile.h>

namespace bb {

namespace statistics {
BB_FWD_DECLARE_SHARED_PTR( IDataSamplerSpec );
}

namespace signals {

BB_FWD_DECLARE_SHARED_PTR( IModelDataWriterSpec );
BB_FWD_DECLARE_SHARED_PTR( ISignalSpec );

class ProductEntrySignalsLoader
{
public:
    static void getSignals( std::vector<ISignalSpecCPtr>* signals, const ConfigFile::ProductEntry& prod, bool isTarget );
    static ISignalSpecPtr getDependentVariable(const ConfigFile::ProductEntry& prod);
    static statistics::IDataSamplerSpecPtr getDataSampler( const ConfigFile::ProductEntry& prod );
    static IModelDataWriterSpecPtr getModelDataWriter( const ConfigFile::ProductEntry& prod );
};

} // namespace signals
} // namespace bb

#endif // BB_SIGNALS_PRODUCTENTRYSIGNALSLOADER_H

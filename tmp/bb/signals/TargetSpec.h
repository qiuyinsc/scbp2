#ifndef BB_SIGNALS_TARGETSPEC_H
#define BB_SIGNALS_TARGETSPEC_H

/* Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved. */


namespace bb {
namespace signals {

BB_FWD_DECLARE_SHARED_PTR( Weights );
BB_FWD_DECLARE_SHARED_PTR( ISignalSpecList );

class TargetSpec
{
public:
    TargetSpec( ISignalSpecListCPtr signal_spec_list, WeightsCPtr weights )
        : m_spSignalSpecList( signal_spec_list )
        , m_spWeights( weights )
    {
    }

    ISignalSpecListCPtr getSignalSpecs() const { return m_spSignalSpecList; }
    WeightsCPtr getWeights() const { return m_spWeights; }

protected:
    ISignalSpecListCPtr m_spSignalSpecList;
    WeightsCPtr m_spWeights;
};

} // namespace signals
} // namespace bb

#endif // BB_SIGNALS_TARGETSPEC_H

#ifndef BB_SIGNALS_SIGNALPRINTER_H
#define BB_SIGNALS_SIGNALPRINTER_H

/* Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved. */

#include <vector>

#include <bb/core/smart_ptr.h>

#include <bb/clientcore/ClockMonitor.h>

#include <bb/statistics/DataSampler.h>

#include <bb/signals/SignalPrinterSpec.h>

namespace bb {

BB_FWD_DECLARE_SHARED_PTR( ClientContext );
BB_FWD_DECLARE_SHARED_PTR( IPriceProvider );

class Msg;
class instrument_t;
class timeval_t;

namespace statistics {

} // namespace statistics

namespace signals {

BB_FWD_DECLARE_SHARED_PTR( IModelData );
BB_FWD_DECLARE_SHARED_PTR( IModelDataWriter );
BB_FWD_DECLARE_SHARED_PTR( SignalsList );

class ISignalPrinter
{
public:
    virtual ~ISignalPrinter() {}

    virtual void startPrinting() = 0;
    virtual void stopPrinting() = 0;

    virtual void print() const = 0;

    /// All targets should be added before we start printing ( or writing the .desc file ).
    virtual void addModelData( const IModelDataPtr& modelData, const IModelDataWriterPtr& modelDataWriter ) = 0;

    virtual ClockMonitorPtr getClockMonitor() const = 0;
    virtual SignalsListCPtr getSignals( const instrument_t& target ) const = 0;
};
BB_DECLARE_SHARED_PTR( ISignalPrinter );

class SignalPrinter
    : public ISignalPrinter
    , public statistics::IDataSamplerListener
{
public:
    // We expect IDataSampler is actually not shared by other objects, so that the SignalPrinter
    // can ensure that it's at least the last to get a IDataSampler sample change callback and
    // to call startPrinting()/stopPrinting() without external consequences
    SignalPrinter( const bb::ClientContextPtr& clientContext, const statistics::IDataSamplerPtr& dataSampler );

    virtual void startPrinting();
    virtual void stopPrinting();

    virtual void print() const;

    virtual void addModelData( const IModelDataPtr& modelData, const IModelDataWriterPtr& modelDataWriter );

    virtual ClockMonitorPtr getClockMonitor() const { return m_clockMonitor; }
    virtual SignalsListCPtr getSignals( const instrument_t& targetInstrument ) const;

protected:
    BB_FWD_DECLARE_SHARED_PTR( Target );

    void onAllEvents( const Msg& msg );
    virtual void onDataSample( const statistics::IDataSampler* dataSampler );

    ClockMonitorPtr m_clockMonitor;
    const statistics::IDataSamplerPtr m_dataSampler;
    const bool m_delayPrinting;
    Subscription m_ccSub, m_dsSub;
    std::vector<TargetPtr> m_targets;
    bb::timeval_t m_lastMessageTimeval, m_lastSampleTime;
};
BB_DECLARE_SHARED_PTR( SignalPrinter );

/// Some stuff common to most signals
class SignalPrinterSpec : public SignalPrinterImplSpec
{
public:
    BB_DECLARE_SCRIPTING();

    SignalPrinterSpec();
    SignalPrinterSpec( const SignalPrinterSpec& e );

    virtual ISignalPrinterPtr build( ISignalBuilder* builder ) const;

    virtual void checkValid() const;
    virtual void hashCombine( size_t& result ) const;
    virtual bool compare( const ISignalPrinterSpec* other ) const;
    virtual void print( std::ostream& o, const LuaPrintSettings& ps ) const;
    virtual void getDataRequirements( IDataRequirements* rqs ) const;

    statistics::IDataSamplerSpecPtr m_dataSampler;
};
BB_DECLARE_SHARED_PTR( SignalPrinterSpec );

} // namespace signals
} // namespace bb

#endif // BB_SIGNALS_SIGNALPRINTER_H

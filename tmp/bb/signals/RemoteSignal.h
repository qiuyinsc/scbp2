#ifndef BB_SIGNALS_REMOTESIGNAL_H
#define BB_SIGNALS_REMOTESIGNAL_H

/* Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved. */


#include <string>
#include <iosfwd>

#include <bb/core/LuaState.h>
#include <bb/core/smart_ptr.h>
#include <bb/core/Uuid.h>

#include <bb/io/Socket.h>
#include <bb/io/LuaAdmin.h>

#include <bb/signals/SignalSpec.h>
#include <bb/signals/CachedSignal.h> // for MessageSignal

namespace bb {
namespace signals {

namespace sigserver {



/// bb::signals::sigserver::Client handles a client's connection to a sigserver.
class Client : public LuaCommandProtocol
{
public:
    Client(TCPSocket *sock);
    virtual ~Client();

    void addSignal(MessageSignal::SignalID_t *out_sigID,
            std::vector<std::string> *out_stateNames,
            ISignalSpecCPtr spec);

    const Uuid &getServerId() const { return m_serverUuid; }

protected:
    // LuaCommandProtocol overrides
    void sendCommandImpl(const std::string &chunk);
    void executeCommandImpl(const std::string &cmd);

    // config methods (called by the server from lua)
    void setServerId(const Uuid &uuid);
    void error(std::string error);
    void signalAdded(MessageSignal::SignalID_t signalID, luabind::object stateNamesObj);

    void waitForServerResponse();

    Uuid m_serverUuid;
    TCPSocket *m_socket;
    bool m_waitingForResponse;
    std::string m_errorMessage;
    MessageSignal::SignalID_t m_lastSigID;
    std::vector<std::string> m_lastStateNames;
};
BB_DECLARE_SHARED_PTR(Client);

} // namespace sigserver

/// SignalSpec for a remote signal (a MessageSignal object which connects to a sigserver).
class RemoteSignalSpec : public ISignalSpec
{
public:
    BB_DECLARE_SCRIPTING();

    RemoteSignalSpec();
    RemoteSignalSpec(const RemoteSignalSpec &e);

    virtual ISignalPtr build(SignalBuilder *builder) const;
    virtual void checkValid() const;
    virtual void hashCombine(size_t &result) const;
    virtual bool compare(const ISignalSpec *other) const;
    virtual void print(std::ostream &o, const LuaPrintSettings &ps) const;
    virtual void getDataRequirements(IDataRequirements *rqs) const;
    virtual RemoteSignalSpec *clone() const;

    virtual instrument_t getInstrument() const { return m_subSignal->getInstrument(); }
    virtual std::string getDescription() const { return m_subSignal->getDescription(); }

    ISignalSpecCPtr m_subSignal;
    std::string m_host;
    uint16_t m_port;
};
BB_DECLARE_SHARED_PTR(RemoteSignalSpec);

} // namespace signals
} // namespace bb

#endif // BB_SIGNALS_REMOTESIGNAL_H

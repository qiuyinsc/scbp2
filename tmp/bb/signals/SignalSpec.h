#ifndef BB_SIGNALS_SIGNALSPEC_H
#define BB_SIGNALS_SIGNALSPEC_H

/* Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved. */


#include <bb/core/instrument.h>
#include <bb/core/smart_ptr.h>
#include <bb/core/Scripting.h>
#include <bb/clientcore/PriceProviderSpec.h>

namespace bb {

class IDataRequirements;

namespace signals {

// Forward Declarations
class ISignal;
class ISignalSpec;
class SignalSpec;
class SignalBuilder;
BB_FWD_DECLARE_SHARED_PTR(ISignal);
BB_FWD_DECLARE_SHARED_PTR(ISignalSpec);


/// Some common signal normalization values.
enum ReturnMode { DIFF, ARITH, LOG };

/*******************************************************************************************/
//
// SignalSpec interface and a useful base implementation
//
/*******************************************************************************************/

/// An ISignalSpec is a description of a signal to be calculated.
/// The subclasses of SignalSpec each describe a different signal type,
/// and include all the data to uniquely charaterize which signal it is.
class ISignalSpec
{
public:
    BB_DECLARE_SCRIPTING();

    virtual ~ISignalSpec() {}

    virtual instrument_t getInstrument() const = 0;
    virtual std::string getDescription() const = 0;
    std::string getDesc() const { return getDescription(); }

    /// Instantiates a concrete ISignal according to the Spec
    virtual ISignalPtr build(SignalBuilder *builder) const = 0;

    /// deep copy this ISignalSpec
    virtual ISignalSpec *clone() const = 0;
    inline ISignalSpec *clone(const ISignalSpec *e) { return e ? e->clone() : NULL; }
    inline ISignalSpec *clone(ISignalSpecCPtr e) { return clone(e.get()); }

    /// Combines this ISignalSpec into the given hash value
    virtual void hashCombine(size_t &result) const = 0;

    /// Compares two signal specs.
    virtual bool compare(const ISignalSpec *other) const = 0;

    /// Prints the lua representation of this SignalSpec.
    virtual void print(std::ostream &o, const LuaPrintSettings &ps) const = 0;

    /// ensures sure that all fields have been set correctly, or throws runtime_error
    virtual void checkValid() const {}

    /// Propagates all the required data (instr/src pairs) to the IDataRequirements.
    virtual void getDataRequirements(IDataRequirements *rqs) const = 0;

    /// This variable returns true if Signal represents a dependent variable in the regression,
    /// rather than a regressor. Dependent variable signals have special requirements,
    /// which are documented in OvargenPrinter.
    virtual bool isDependentVariable() const { return false; }
};
BB_DECLARE_SHARED_PTR(ISignalSpec);


class ISignalSpecList : public std::vector<ISignalSpecCPtr>{};
BB_DECLARE_SHARED_PTR(ISignalSpecList);

/// Some stuff common to most signals
class SignalSpec : public ISignalSpec
{
public:
    BB_DECLARE_SCRIPTING();

    SignalSpec();
    SignalSpec(const SignalSpec &e);

    virtual void checkValid() const;

    virtual std::string getDescription() const { return m_description; }

    virtual void hashCombine(size_t &result) const;
    virtual bool compare(const ISignalSpec *other) const;

    virtual void getDataRequirements(IDataRequirements *rqs) const;

    std::string m_description;
    IPxProviderSpecPtr m_refPxP;
};
BB_DECLARE_SHARED_PTR(SignalSpec);

/// Some signal ideas do not have any use for a refpx provider
class NoRefPxSignalSpec : public ISignalSpec
{
public:
    BB_DECLARE_SCRIPTING();

    NoRefPxSignalSpec();
    NoRefPxSignalSpec(const NoRefPxSignalSpec &e);

    virtual void checkValid() const;

    virtual std::string getDescription() const { return m_description; }

    virtual void hashCombine(size_t &result) const;
    virtual bool compare(const ISignalSpec *other) const;

    virtual void getDataRequirements(IDataRequirements *rqs) const;

    std::string m_description;
//    IPxProviderSpecPtr m_refPxP;
};
BB_DECLARE_SHARED_PTR(NoRefPxSignalSpec);

/// Helper class for converting a string into a SignalSpec.
class SignalSpecReader : public bb::SpecReader
{
public:
    SignalSpecReader();

    // parses the given lua code (which should be lua code for a signal spec),
    // and returns the signal spec
    ISignalSpecPtr getSignalSpec(std::string txt);
};

std::size_t hash_value(const ISignalSpec &a);

// SWIG 1.3.29 doesn't like these in-namespace operators
#ifndef SWIG

bool operator==(const ISignalSpec &a, const ISignalSpec &b); // defined in SignalSpec.cc
inline bool operator!=(const ISignalSpec &a, const ISignalSpec &b) { return !(a == b); }

std::ostream &operator<<(std::ostream &out, const ISignalSpec &spec);
void luaprint(std::ostream &out, bb::signals::ISignalSpec const &bk, LuaPrintSettings const &ps);

#endif // !SWIG

// helpers for hash_map
struct SignalSpecHasher : public std::unary_function<size_t, ISignalSpecCPtr>
    { size_t operator()(const ISignalSpecCPtr &a) const { return hash_value(*a); } };
struct SignalSpecComparator : public std::binary_function<bool, ISignalSpecCPtr, ISignalSpecCPtr>
    { bool operator()(const ISignalSpecCPtr &a, const ISignalSpecCPtr &b) const { return *a == *b; } };

} // namespace signals
} // namespace bb

#endif // BB_SIGNALS_SIGNALSPEC_H

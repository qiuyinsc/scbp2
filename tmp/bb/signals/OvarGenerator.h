#ifndef BB_SIGNALS_OVARGENERATOR_H
#define BB_SIGNALS_OVARGENERATOR_H

/* Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved. */


#include <bb/core/smart_ptr.h>
#include <bb/clientcore/IBook.h>
#include <bb/clientcore/ClockMonitor.h>
#include <bb/clientcore/PeriodicWakeup.h>
#include <bb/clientcore/PriceProvider.h>

#include <deque>
#include <queue>

#include <bb/signals/Signal.h>
#include <bb/signals/SignalSpec.h>
#include <bb/signals/ITriggerPrintableTarget.h>
#include <bb/signals/TargetVariable.h>

namespace bb {

namespace statistics {

BB_FWD_DECLARE_SHARED_PTR( IDataSampler );
BB_FWD_DECLARE_SHARED_PTR( IDataSamplerSpec );

}

namespace signals {

/// Generates the dependent variable a.k.a. output variable a.k.a. the y in linear regression y = beta*X
/// for price prediction. This is an exponential moving average of the reference price, sampled once every
/// second.

/// 06-13-2013
/// Warning: This code has confused many a poor reader. Hopefully the following notes will help future poor souls
/// digest it more quickly.
///
/// Notes:
/// For a given configured "decay" this does generate an exponential moving average of the reference price, but there
/// are a couple of important details.
///
/// A conventional EMA uses a simple formula:
///     s(t) = W*v(t) + (1-W)*s(t-1)
///     W is constant
///     so when W=1, s(t) = v(t), or the last entry in the series.
///     and when W=0, s(t) = v(0), or the initial value in the series.
///
/// However, here in the OvarGenerator, the function initDecays() appears to calculate a set of w(t)'s and then this set is applied
/// in the update() function such that the W in the above formula becomes non-constant w(t) and the formula used is:
///     s(t) = w(t)*v(t) + (1-w(t))*s(t-1)
///
/// So, the question has been: why is this non-constant w(t) being used and is the calculated sum still a classic EMA?
///
/// Short version: w(t) is actually equivalent to pre-calculating factors of (1-W)^N
///
/// Long version, let us look at two characteristics of using w(t) in this manner.
///
///      First characteristic: It basically "inverts" the EMA such that for most of the range of W's from 0 to ~0.95
///      using configured decay W to generate non-constant w(W,t); s(t) = w(W,t)*v(t) + (1-w(W,t))*s(t-1) and
///      then applying w(W,t) to the prices in the order at which they are received ...
///
///      ... is equivalent to using a constant decay D = (1-W); s(t) = D*s(t-1) + (1-D)*v(t) applied to the prices in the
///      reverse order from that in which they are recieved.
///
///      I would characterize this change as making the EMA act more as an EMA "focused" on the prices closest to
///      beginning of the window. For example: Using the default 15-180 seconds window, the simple ema and a range of decays
///      from 0.1-0.9 moves the effective predicted time from 171 to 179.9 seconds, whereas after this transformation,
///      the same range moves the effective predicted time from 15.1 seconds to 24 seconds.
///
///      Second characteristic: For high values of W ~= 1,
///      the calculated sum approaches a simple average of the prices instead of the last entry of the series.
class OvarGenerator
    : public TargetVariable
    , public ITriggerPrintableTarget
    , protected PeriodicWakeup
    , protected IClockListener
{
public:
    // OvarGenerator is a special kind of signal whose state contains future information. At any time t, we do want it to
    // be updated to time t before it could be printed, hence it updates with a priority of PRIORITY_SIGNAL_PREPRINTING
    OvarGenerator(const instrument_t &instr, const std::string &mydesc, const IPriceProviderPtr& pp, const ClockMonitorPtr& cm,
                  const ptime_duration_t &sampleInterval, const ptime_duration_t &sampleOffset, int numSamplesOmit,
                  int numSamplesMax, const std::vector<double> &decayRates,
                  uint32_t wakeupPriority = PRIORITY_SIGNAL_PREPRINTING, int vbose = 0);
    virtual ~OvarGenerator() {}

    void reset();

    virtual bool isOK() const { return m_lastIsOK; }
    virtual const instrument_t& getInstrument() const { return m_instr; }
    virtual const std::string& getDesc() const { return m_desc; }
    virtual const std::vector<double, std::allocator<double> >& getSignalState() const { return m_lastState; }

    // ITriggerListPrintable
    virtual bool firstVarAsPrintRefPrice() const { return true; }
    virtual double getPrintRefPrice() const { return m_lastState[0]; }
    virtual ptime_duration_t getPrintDelay() const { return getTvMax(); }

    virtual size_t getStateSize() const { return m_stateNames.size(); }
    virtual const std::vector<std::string>& getStateNames() const { return m_stateNames; }
    virtual bb::timeval_t getLastChangeTv() const { return m_lastChangeTv; }
    virtual boost::optional<timeval_t> getLastScheduledChangeTv() const { return m_lastScheduledChangeTv; }

    size_t getNumSamplesMax() const { return m_numSamplesMax; }
    bb::ptime_duration_t getTvMax() const { return m_tvMax; }

protected:
    void update( timeval_t scheduled_wtv, timeval_t current_tv );

    // PeriodicWakeup implementation
    virtual void onPeriodicWakeup(const timeval_t &ctv, const timeval_t &swtv);
    // IClockListener implementation
    void onWakeupCall( const timeval_t& ctv, const timeval_t& swtv, int reason, void* pData );

protected:
    void initDecays(const std::vector<double> &decay_rates);

    // Class representing snapshots that are time decayed/smoothed to create the target
    // price movement
    class TimeSlice {
    public:
        TimeSlice(timeval_t otv, timeval_t actual_tv, double refpx, bool slice_valid) :
            m_otv(otv),
            m_actual_tv(actual_tv),
            m_refpx(refpx),
            m_slice_valid(slice_valid)
            { }
        ~TimeSlice() { }

        timeval_t m_otv;
        timeval_t m_actual_tv;
        std::vector<double> m_vals;
        double m_refpx;
        bool m_slice_valid;
    };

    std::string m_desc;
    instrument_t m_instr;
    IPriceProviderPtr m_pp;
    Subscription m_pp_sub;
    const size_t m_numDecays;
    const size_t m_numSamplesMax;
    const ptime_duration_t m_tvOmit, m_tvMax;
    const ptime_duration_t m_sampleInterval, m_sampleOffset;
    const uint32_t m_wakeupPriority;
    int m_vbose;

    int m_num_valid_slices;

    std::deque<TimeSlice> m_slices;
    typedef std::deque<TimeSlice>::iterator deqiter;
    std::vector< std::vector<double> > m_decays;

    bool m_lastIsOK;
    timeval_t m_lastChangeTv, m_lastScheduledChangeTv;
    std::vector<std::string> m_stateNames;
    std::vector<double> m_lastState;
};
BB_DECLARE_SHARED_PTR( OvarGenerator );


/// SignalSpec corresponding to OvarGenerator
class OvarGeneratorSignalSpec : public ISignalSpec
{
public:
    BB_DECLARE_SCRIPTING();

    OvarGeneratorSignalSpec()
        : m_wakeupPriority( PRIORITY_SIGNAL_PREPRINTING )
    {}

    OvarGeneratorSignalSpec(const OvarGeneratorSignalSpec &e);

    virtual instrument_t getInstrument() const { return m_pp->getInstrument(); }
    virtual std::string getDescription() const { return m_description; }

    virtual ISignalPtr build(SignalBuilder *builder) const;
    virtual void checkValid() const;
    virtual void hashCombine(size_t &result) const;
    virtual OvarGeneratorSignalSpec *clone() const;
    virtual bool compare(const ISignalSpec *other) const;
    virtual void print(std::ostream &o, const LuaPrintSettings &ps) const;
    virtual void getDataRequirements(IDataRequirements *rqs) const;
    virtual bool isDependentVariable() const { return true; }

    std::string m_description;
    IPxProviderSpecPtr m_pp;
    std::vector<double> m_decayRates;
    ptime_duration_t m_sampleInterval, m_sampleOffset;
    int m_numSamplesOmit, m_numSamplesMax;
    uint32_t m_wakeupPriority;
};
BB_DECLARE_SHARED_PTR(OvarGeneratorSignalSpec);




/// Logs the state of a given output variable to a file.
/// An output variable is just a special kind of signal, with the following requirements:
/// 1) notifySignalListeners must get called at regular intervals, such that
///    lcm(interval, SignalPrinter::m_printInterval) == SignalPrinter::m_printInterval.
/// 2) getLastChangeTv() should return return a time in the past, and the value of the signal should pertain
///    to the future price of the stock relative to that time.
/// 3) getLastScheduledChangeTv() must be implemented.
/// 4) The first component in the signal should be the reference price. The other components
///    should be the desired price prediction columns (relative to 0, not the reference price)
class OvargenPrinter : public IClockListener, ISignalListener
{
public:
    OvargenPrinter(ClockMonitorPtr cm, ISignalPtr outputVariable, std::string obase, int vbose,
            std::string printFormat, const ptime_duration_t &printInterval, const ptime_duration_t &printOffset);
    virtual ~OvargenPrinter();

    void update( timeval_t scheduled_wtv, timeval_t current_tv );

    void reset();

protected:
    // ClockListener implementation
    virtual void onWakeupCall( const timeval_t& ctv, const timeval_t& swtv, int reason, void* pData );

    // ISignalListener implementation
    virtual void onSignalChanged( const ISignal& signal );

protected:
    void createDataDescription();

    ClockMonitorPtr m_clockMonitor;
    ISignalPtr m_outputVariable;

    std::string m_obase;
    FILE* m_outf;
    size_t m_numStates;
    bool m_printTimeExact;
    std::string m_printFormat;
    ptime_duration_t m_printInterval, m_printOffset;
};

BB_DECLARE_SHARED_PTR( OvargenPrinter );


struct SampleInterval
{
    SampleInterval();
    SampleInterval( const unsigned int start, const unsigned int end );
    void checkValid() const;
    unsigned int m_start, m_end;
};

inline bool operator==(const SampleInterval &a, const SampleInterval &b)
    { return a.m_start == b.m_start && a.m_end == b.m_end; }

typedef std::vector<SampleInterval> SampleIntervalVec;

class DataSamplerTargetVariable
    : public SignalStateImpl
    , public statistics::IDataSamplerListener
{
public:

    DataSamplerTargetVariable( const IPriceProviderPtr& pxp, const statistics::IDataSamplerPtr& dataSampler, const SampleIntervalVec& sampleIntervals, const bool verbose = false );
    void reset();

    statistics::IDataSamplerPtr getDataSampler() const { return m_dataSampler; }

protected:
    virtual void recomputeState() const;

    virtual void onDataSample( const statistics::IDataSampler* dataSampler );

private:
    const IPriceProviderPtr m_pxp;
    const statistics::IDataSamplerPtr m_dataSampler;
    const SampleIntervalVec m_sampleIntervals;
    unsigned int m_numParams, m_largestStart, m_largestEnd;

    const bool m_verbose;
    Subscription m_sub;
    std::deque<std::pair<bb::timeval_t, double> > m_slices;
};
BB_DECLARE_SHARED_PTR( DataSamplerTargetVariable );

/// SignalSpec corresponding to DataSamplerTargetVariable
class DataSamplerTargetVariableSpec : public ISignalSpec
{
public:
    BB_DECLARE_SCRIPTING();

    DataSamplerTargetVariableSpec()
        : m_verbose( false )
    {}

    DataSamplerTargetVariableSpec( const DataSamplerTargetVariableSpec& e );

    virtual instrument_t getInstrument() const { return m_pp->getInstrument(); }
    virtual std::string getDescription() const { return m_description; }

    virtual ISignalPtr build( bb::signals::SignalBuilder* builder ) const;
    virtual void checkValid() const;
    virtual void hashCombine( size_t& result ) const;
    virtual DataSamplerTargetVariableSpec* clone() const;
    virtual bool compare( const bb::signals::ISignalSpec* other ) const;
    virtual void print( std::ostream& o, const LuaPrintSettings& ps ) const;
    virtual void getDataRequirements( IDataRequirements* rqs ) const;
    virtual bool isDependentVariable() const { return true; }

    std::string m_description;
    bb::IPxProviderSpecPtr m_pp;
    bb::statistics::IDataSamplerSpecPtr m_dataSampler;
    SampleIntervalVec m_sampleIntervals;

    bool m_verbose;
};
BB_DECLARE_SHARED_PTR(DataSamplerTargetVariableSpec);

} // namespace signals
} // namespace bb

#endif // BB_SIGNALS_OVARGENERATOR_H

#ifndef BB_SIGNALS_SIGSNAPSGNSQRT_H
#define BB_SIGNALS_SIGSNAPSGNSQRT_H

/* Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved. */

#include <bb/clientcore/ClockMonitor.h>
#include <bb/signals/Signal.h>
#include <bb/signals/SignalSpec.h>

#include <bb/signals/SigSnap.h>

/// This signal tends to cause model to perform more volatile day-to-day. Use with caution.

namespace bb {
namespace signals {

class SigSnapSgnSqrt
    : public SigSnap
{
public:
    
    /// N is the number of weights, which must be equal to the number of intervals.
    SigSnapSgnSqrt( const instrument_t& instr, const std::string &desc,
             ClockMonitorPtr cm,
             IPriceProviderPtr spRefPP, IPriceProviderPtr spInputPP,
             ptime_duration_t _interval,
             const intervals &interval_list, int vbose);

    virtual void recomputeState() const;

};
BB_DECLARE_SHARED_PTR(SigSnapSgnSqrt);


/// SignalLagSpec for SigSnapSgnSqrt
class SigSnapSgnSqrtSpec : public SignalSpec
{
public:
    BB_DECLARE_SCRIPTING();

    SigSnapSgnSqrtSpec() {}
    SigSnapSgnSqrtSpec(const SigSnapSgnSqrtSpec &e);

    virtual instrument_t getInstrument() const { return m_inputPxP->getInstrument(); }
    virtual ISignalPtr build(SignalBuilder *builder) const;
    virtual void checkValid() const;
    virtual void hashCombine(size_t &result) const;
    virtual bool compare(const ISignalSpec *other) const;
    virtual void print(std::ostream &o, const LuaPrintSettings &ps) const;
    virtual void getDataRequirements(IDataRequirements *rqs) const;
    virtual SigSnapSgnSqrtSpec *clone() const;

    IPxProviderSpecPtr m_inputPxP;
    ptime_duration_t m_interval;
    std::vector<unsigned int> m_intervals;
};
BB_DECLARE_SHARED_PTR(SigSnapSgnSqrtSpec);



} // namespace signals
} // namespace bb

#endif // BB_SIGNALS_SIGSNAPSGNSQRT_H

#ifndef BB_SIGNALS_MODELDATA_H
#define BB_SIGNALS_MODELDATA_H

/* Contents Copyright 2016 Athena Capital Research LLC. All Rights Reserved. */

#include <bb/core/smart_ptr.h>
#include <bb/signals/Signal.h>

namespace bb {

class timeval_t;

namespace signals {

BB_FWD_DECLARE_SHARED_PTR( ISignalBuilder );
BB_FWD_DECLARE_SHARED_PTR( TargetInfo );

// By ModelData, we mean all the data needed to model a target
class IModelData
{
public:
    virtual ~IModelData() {}

    virtual const bb::instrument_t& getInstrument() const = 0;
    virtual bb::timeval_t getLastChangeTv() const = 0;
    virtual bb::timeval_t getLastScheduledChangeTv() const = 0;
    virtual TargetInfoCPtr getTargetInfo() const = 0;
    virtual ISignalPtr getTarget() const = 0;
    virtual ISignalPtr getRefSignal() const = 0;
    virtual SignalsListCPtr getSignals() const = 0;
    virtual double getPrediction() const = 0;
};
BB_DECLARE_SHARED_PTR( IModelData );

class ModelData
    : public IModelData
{
public:
    ModelData( const TargetInfoCPtr& targetInfo, const ISignalBuilderPtr& signalBuilder );

    virtual const bb::instrument_t& getInstrument() const { return getTarget()->getInstrument(); }
    virtual bb::timeval_t getLastChangeTv() const { return getTarget()->getLastChangeTv(); }
    virtual bb::timeval_t getLastScheduledChangeTv() const;
    virtual TargetInfoCPtr getTargetInfo() const { return m_targetInfo; }
    virtual ISignalPtr getTarget() const { return m_target; }
    virtual ISignalPtr getRefSignal() const { return m_refSignal; }
    virtual SignalsListCPtr getSignals() const { return m_signals; }
    virtual double getPrediction() const;

protected:
    const TargetInfoCPtr m_targetInfo;
    const ISignalPtr m_target;
    ISignalPtr m_refSignal;
    SignalsListPtr m_signals;
};
BB_DECLARE_SHARED_PTR( ModelData );

} // namespace signals
} // namespace bb

#endif // BB_SIGNALS_MODELDATA_H

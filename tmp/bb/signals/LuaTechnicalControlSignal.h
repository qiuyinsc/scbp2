#ifndef BB_SIGNALS_LUATECHNICALCONTROLSIGNAL_H
#define BB_SIGNALS_LUATECHNICALCONTROLSIGNAL_H

/* Contents Copyright 2013 Athena Capital Research LLC. All Rights Reserved. */

#include <vector>

#include <bb/core/smart_ptr.h>
#include <bb/clientcore/IBook.h>
#include <bb/clientcore/TickProvider.h>
#include <bb/signals/IControlSignal.h>

namespace bb {
class instrument_t;
BB_FWD_DECLARE_SHARED_PTR( Environment );
BB_FWD_DECLARE_SHARED_PTR( ClientContext );

namespace signals {

BB_FWD_DECLARE_SHARED_PTR( ISignalBuilder );

class LuaTechnicalControlSignal : public bb::signals::IControlSignal,
                                  public bb::IBookListener,
                                  public bb::ITickListener
{
public:
    class Spec : public IControlSignal::Spec
    {
    public:
        BB_DECLARE_SCRIPTING();

        Spec() {}

        virtual bb::signals::IControlSignalPtr create( const ISignalBuilderPtr& signalBuilder );

        bb::instrument_t instr;
        std::string script_filename;
        bb::source_t source;
    };

public:
    LuaTechnicalControlSignal( const ISignalBuilderPtr& signalbuilder, const Spec& params );

    virtual ~LuaTechnicalControlSignal();

    virtual int32_t value() const;

    virtual bool isValid() const { return true; }

protected:
    virtual void onBookChanged( const bb::IBook *pBook, const bb::Msg *pMsg,
                                int32_t bidLevelChanged, int32_t askLevelChanged );

    virtual void onTickReceived( const bb::ITickProvider *tp, const bb::TradeTick& tick );

    const Spec m_params;
    bb::EnvironmentPtr m_spEnv;
    bb::ClientContextWeakPtr m_wpCC;
    double m_prediction;
    std::vector<bb::Subscription> m_subs;
    bb::IBookPtr m_spBook;
    bb::ITickProviderPtr m_spTickProvider;

    bb::PriceSize m_lastBid;
    bb::PriceSize m_lastAsk;
};

} // namespace signals
} // namespace bb

#endif // BB_SIGNALS_LUATECHNICALCONTROLSIGNAL_H


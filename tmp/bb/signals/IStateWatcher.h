#ifndef BB_SIGNALS_ISTATEWATCHER_H
#define BB_SIGNALS_ISTATEWATCHER_H

/* Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved. */

#include <bb/core/smart_ptr.h>
#include <bb/core/EventPublisher.h>

namespace bb {
namespace signals {

// The IBinaryStateWatcher
// Announces when a state is entered or exited from.
// "State" is implementation defined
// Multiple states are intended to be constructed by
// combining binary states.

class IBinaryStateWatcher {

public:
    class IListener : public bb::IEventSubscriber{
    public:
        virtual ~IListener() {}
        /// Invoked when the status changes.
        /// Guaranteed not to be called if the status has not changed.
        virtual void onStateChange( bool newState, const IBinaryStateWatcher* pBSW=NULL  ) = 0;
    };

    virtual ~IBinaryStateWatcher() {}

    virtual bool isInState()=0;

    // Listener management functions
    virtual void addListener( IListener* pListener )=0;
    virtual void removeListener( IListener* pListener )=0;
    virtual void updateListeners()=0;

};

BB_DECLARE_SHARED_PTR( IBinaryStateWatcher );


// Some parts implemented
// @todo Implement the parts.
class BinaryStateWatcher : public IBinaryStateWatcher {

public:
    BinaryStateWatcher() : m_state(false) {}

    virtual ~BinaryStateWatcher() {}

    virtual bool isInState(){ return m_state; }

    void addListener( IBinaryStateWatcher::IListener* pListener )
    {
        m_listeners.connect(pListener);
    }

    void removeListener( IBinaryStateWatcher::IListener* pListener )
    {
        m_listeners.disconnect(pListener);
    }

    void updateListeners()
    {
        m_listeners.notifyAll<onStateChange>(m_state, this);
    }

protected:
    typedef bb::EventPublisher<IBinaryStateWatcher::IListener> StateChangePub;
    struct onStateChange : public StateChangePub::INotifier
    {
        onStateChange(IBinaryStateWatcher::IListener* callback) : StateChangePub::INotifier ( callback ){}
        bool notify( const bool state, const IBinaryStateWatcher* watcher) { m_callback->onStateChange( state, watcher ); return true; }
    };
    bool m_state;
    StateChangePub m_listeners;
};

BB_DECLARE_SHARED_PTR( BinaryStateWatcher );


} // signals
} // bb

#endif // BB_SIGNALS_ISTATEWATCHER_H

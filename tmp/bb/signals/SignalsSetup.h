#ifndef BB_SIGNALS_SIGNALSSETUP_H
#define BB_SIGNALS_SIGNALSSETUP_H

/* Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved. */


#include <bb/clientcore/ClientCoreSetup.h>

#include <bb/signals/SignalManager.h>
#include <bb/signals/SignalsConfigFile.h>
#include <bb/signals/OvargenCommon.h>
#include <bb/signals/PredictionReport.h>
#include <bb/signals/TriggerListSignalPrinter.h>

namespace bb {
namespace signals {

BB_FWD_DECLARE_SHARED_PTR(ISignalBuilder);
BB_FWD_DECLARE_SHARED_PTR(TargetInfo);
BB_FWD_DECLARE_SHARED_PTR(TargetWeights);
BB_FWD_DECLARE_SHARED_PTR(ISignalSpecList);

class SignalsSetup : public virtual ClientCoreSetup
{
public:
    SignalsSetup( int argc, char** argv );
    virtual ~SignalsSetup();

    /// Makes objects that are unique on a per program basis.
    /// Calls ClientCoreSetup::setup, then additionally makes
    /// a SignalManager.
    virtual void setup();

    virtual bool setupSymbols();
    virtual void run();

    virtual void printSettings(bool endl_fg=true);

    ConfigFilePtr getConfigFile() const { return static_pointer_cast<signals::ConfigFile>(m_configFile); }
    SignalManagerPtr getSignalManager() const { return m_spSM; }

    // Exposes the "SignalManagerFactory" portion of SignalsSetup.
    // Added to help allow multiple SignalManagers
    // SignalsSetup continues to have the default SignalManager internally.
    SignalManagerPtr createSignalManager( const std::string& label );

    TargetInfoPtr initSignalTarget( const ConfigFile::ProductEntryCPtr& target, const ProductVec_t& signal_prods
        , TargetWeightsPtr tgtwts );
    virtual TargetInfoPtr initSignalTarget( const ConfigFile::ProductEntryCPtr& target, const ProductVec_t& signal_prods
        , const std::string& weights_filename );

    virtual void getAllProducts(ProductVec_t *prods) const;
    virtual void getSignalProducts(ProductVec_t *prods) const;

protected:
    boost::shared_ptr<SetupOptions> getSignalsSetupOptions(const std::string& default_output_base="", bool add_cache_options = true);
    void checkSignalsSetupOptions(const boost::program_options::variables_map& vm, ProblemList* problems);

    virtual void addOptions(boost::program_options::options_description &allOptions);
//    virtual void checkOptions(ProblemList* problems);

    virtual void onExit(bool end_time_reached);
    virtual bb::ConfigFilePtr makeConfig();

    virtual bool smonBaselineDataRequired();

    bool addDataSymbol( const instrument_t& instr, const ProductEntry& prod, bool isTarget );
    TargetInfoPtr initSignalTarget_imp( const ConfigFile::ProductEntryCPtr& target, const ProductVec_t& signal_prods
        , TargetWeightsPtr tgtwts, ISignalSpecListPtr all_spec_list );

    std::string m_strOutputFilenameBase, m_strWeightsFilename;

    SignalManagerPtr m_spSM;
    ISignalBuilderPtr m_signalBuilder;

    CacheMode m_cacheMode;
    std::string m_cacheURL;

    bool m_cacheDataForOvargen, m_printOvargen, m_printPredictionReport, m_predictionReportVbose, m_extendedHoursSignals;
    boost::scoped_ptr<ovargen::DependentVarCacher> m_depVarCacher;

    // if --ovargen is specified, this stores one OvargenPrinter per target
    std::vector<OvargenPrinterPtr> m_ovarPrinters;
    std::vector<PredictionReportPtr> m_predictionReports;

    TriggerListSignalPrinterPtr m_spTriggerPrinter;
    ISignalPrinterPtr m_signalPrinter;
    std::vector<ISignalPtr> m_targetVariables;

    bool m_bRequireWeightsFile;
    bool m_enableMessagePerf;
};
BB_DECLARE_SHARED_PTR(SignalsSetup);


} // namespace signals
} // namespace bb
#endif // BB_SIGNALS_SIGNALSSETUP_H

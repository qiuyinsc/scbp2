#ifndef BB_SIGNALS_MODELDATAWRITER_H
#define BB_SIGNALS_MODELDATAWRITER_H

/* Contents Copyright 2016 Athena Capital Research LLC. All Rights Reserved. */

#include <string>

#include <boost/shared_ptr.hpp>

#include <bb/core/smart_ptr.h>
#include <bb/signals/ModelDataWriterSpec.h>
#include <bb/signals/Signal.h>

#include "H5Cpp.h"

#ifndef H5_NO_NAMESPACE
    using namespace H5;
#endif

namespace bb {

class ClockMonitor;
class timeval_t;

namespace signals {

BB_FWD_DECLARE_SHARED_PTR( IModelData );
BB_FWD_DECLARE_SHARED_PTR( ISignalBuilder );

class IModelDataWriter
{
public:
    virtual ~IModelDataWriter() {}

    virtual void startPrinting( const IModelDataPtr& modelData ) = 0;
    virtual void stopPrinting( const IModelDataPtr& modelData ) = 0;

    virtual void print( const IModelDataPtr& modelData, const ClockMonitorPtr& clockMonitor, const bb::timeval_t& printTime ) = 0;
};
BB_DECLARE_SHARED_PTR( IModelDataWriter );

class ModelDataWriterImpl
    : public IModelDataWriter
{
public:
    ModelDataWriterImpl( bool verbose )
        : m_verbose( verbose )
    {}

    virtual void print( const IModelDataPtr& modelData, const ClockMonitorPtr& clockMonitor, const bb::timeval_t& printTime );

protected:
    virtual void printTargetData( const IModelDataPtr& modelData, const ClockMonitorPtr& clockMonitor, const bb::timeval_t& printTime ) = 0;
    virtual void printSignalData( const IModelDataPtr& modelData, const ClockMonitorPtr& clockMonitor, const bb::timeval_t& printTime ) = 0;

    const bool m_verbose;
};

class LegacyModelDataWriter
    : public ModelDataWriterImpl
{
public:
    typedef boost::shared_ptr<std::ofstream> OFStream;

    LegacyModelDataWriter( const std::string& outputFilenameBase, bb::date_t date
                           , const std::string& printFormat = "%7.05", bool verbose = false, size_t buffSize = 256
        );

    virtual void startPrinting( const IModelDataPtr& modelData );
    virtual void stopPrinting( const IModelDataPtr& modelData );

    virtual std::string getOutputFilenameBase() const { return m_outputFilenameBase; }

protected:
    void printTargetDescs( const IModelDataPtr& modelData );
    void printSignalDescs( const IModelDataPtr& modelData );
    void printTargetData( const IModelDataPtr& modelData, const ClockMonitorPtr& clockMonitor, const bb::timeval_t& printTime );
    void printSignalData( const IModelDataPtr& modelData, const ClockMonitorPtr& clockMonitor, const bb::timeval_t& printTime );

    void openOutputFile( OFStream output, const std::string& filename, const std::string& dataType );

    const std::string m_outputFilenameBase;
    const std::string m_targetDescFilename, m_targetDataFilename;
    const std::string m_signalDescFilename, m_signalDataFilename;
    const OFStream m_targetOutput, m_signalOutput;
    const char* m_printFormat;
    const size_t m_buffSize;
};
BB_DECLARE_SHARED_PTR( LegacyModelDataWriter );

class LegacyModelDataWriterSpec : public TextModelDataWriterSpec
{
public:
    BB_DECLARE_SCRIPTING();

    virtual IModelDataWriterPtr build( ISignalBuilder* builder ) const;
    virtual LegacyModelDataWriterSpec* clone() const;
    virtual void print( std::ostream& o, const LuaPrintSettings& ps ) const;
};
BB_DECLARE_SHARED_PTR( LegacyModelDataWriterSpec );

class TargetOnlyLegacyModelDataWriter
    : public LegacyModelDataWriter
{
public:
    TargetOnlyLegacyModelDataWriter( const std::string& outputFilenameBase, bb::date_t date
                                     , const std::string& printFormat = "%7.05", bool verbose = false, size_t buffSize = 256
        )
        : LegacyModelDataWriter( outputFilenameBase, date, printFormat, verbose, buffSize )
    {
    }

protected:
    void printSignalDescs( const IModelDataPtr& modelData ) {}
    void printSignalData( const IModelDataPtr& modelData, const ClockMonitorPtr& clockMonitor, const bb::timeval_t& printTime ) {}
};
BB_DECLARE_SHARED_PTR( TargetOnlyLegacyModelDataWriter );

class TargetOnlyLegacyModelDataWriterSpec : public LegacyModelDataWriterSpec
{
public:
    BB_DECLARE_SCRIPTING();

    virtual IModelDataWriterPtr build( ISignalBuilder* builder ) const;
    virtual TargetOnlyLegacyModelDataWriterSpec* clone() const;
    virtual void print( std::ostream& o, const LuaPrintSettings& ps ) const;
};
BB_DECLARE_SHARED_PTR( LegacyModelDataWriterSpec );

class SignalOnlyLegacyModelDataWriter
    : public LegacyModelDataWriter
{
public:
    SignalOnlyLegacyModelDataWriter( const std::string& outputFilenameBase, bb::date_t date
                                     , const std::string& printFormat = "%7.05", bool verbose = false
                                     , size_t buffSize = 256 )
        : LegacyModelDataWriter( outputFilenameBase, date, printFormat, verbose, buffSize )
    {
    }

protected:
    void printTargetDescs( const IModelDataPtr& modelData ) {}
    void printTargetData( const IModelDataPtr& modelData, const ClockMonitorPtr& clockMonitor, const bb::timeval_t& printTime ) {}
};
BB_DECLARE_SHARED_PTR( SignalOnlyLegacyModelDataWriter );

class SignalOnlyLegacyModelDataWriterSpec : public LegacyModelDataWriterSpec
{
public:
    BB_DECLARE_SCRIPTING();

    virtual IModelDataWriterPtr build( ISignalBuilder* builder ) const;
    virtual SignalOnlyLegacyModelDataWriterSpec* clone() const;
    virtual void print( std::ostream& o, const LuaPrintSettings& ps ) const;
};
BB_DECLARE_SHARED_PTR( LegacyModelDataWriterSpec );

class HdfModelDataWriter : public ModelDataWriterImpl
{
public:
    HdfModelDataWriter( const std::string& outputFilenameBase, const bb::date_t& date, const bool verbose = false );

    virtual void startPrinting( const IModelDataPtr& modelData );
    virtual void stopPrinting( const IModelDataPtr& modelData );

protected:
    virtual void printTargetData( const IModelDataPtr& modelData, const ClockMonitorPtr& clockMonitor, const bb::timeval_t& printTime );
    virtual void printSignalData( const IModelDataPtr& modelData, const ClockMonitorPtr& clockMonitor, const bb::timeval_t& printTime );

    virtual void printTargetDescs( const IModelDataPtr& modelData );
    virtual void printTargetDataset( const IModelDataPtr& modelData, const ClockMonitorPtr& clockMonitor, const bb::timeval_t& printTime );
    virtual void printTargetMetaDataset( const IModelDataPtr& modelData, const ClockMonitorPtr& clockMonitor, const bb::timeval_t& printTime );

    virtual void printSignalDescs( const IModelDataPtr& modelData );
    virtual void printSignalDataset( const IModelDataPtr& modelData, const ClockMonitorPtr& clockMonitor, const bb::timeval_t& printTime );
    virtual void printSignalMetaDataset( const IModelDataPtr& modelData, const ClockMonitorPtr& clockMonitor, const bb::timeval_t& printTime );

    void append( hid_t hdfDataSet, std::vector< std::vector< double > > data );

private:

    const std::string m_outputFilenameBase;
    const std::string m_hdfFilename;
    hid_t m_hdfFile;
    hid_t m_hdfSignalGroup;
    hid_t m_hdfTargetGroup;
    hid_t m_hdfSignalMetaDataSet;
    hid_t m_hdfSignalDataSet;
    hid_t m_hdfTargetMetaDataSet;
    hid_t m_hdfTargetDataSet;
};
BB_DECLARE_SHARED_PTR( HdfModelDataWriter );


class HdfModelDataWriterSpec : public ModelDataWriterSpec
{
public:
    BB_DECLARE_SCRIPTING();

    HdfModelDataWriterSpec( const std::string& outputFilenameBase = emptyString, bb::date_t date = bb::date_t::today(), bool verbose = false );
    HdfModelDataWriterSpec( const HdfModelDataWriterSpec& e );

    IModelDataWriterPtr build( ISignalBuilder* builder ) const;
    HdfModelDataWriterSpec* clone() const;
    void print( std::ostream& o, const LuaPrintSettings& ps ) const;

    virtual void hashCombine( size_t& result ) const;
    virtual bool compare( const IModelDataWriterSpec* other ) const;
    virtual void checkValid() const;

    void setOutputFilenameBase( const std::string& outputFilenameBase ){ m_outputFilenameBase = outputFilenameBase; }
    std::string getOutputFilenameBase( const std::string& outputFilenameBase ) const { return m_outputFilenameBase; }

private:
    std::string m_outputFilenameBase;
    bb::date_t m_date;
};
BB_DECLARE_SHARED_PTR( HdfModelDataWriterSpec );


} // namespace signals
} // namespace bb

#endif // BB_SIGNALS_MODELDATAWRITER_H

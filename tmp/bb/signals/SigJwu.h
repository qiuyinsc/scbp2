#ifndef BB_SIGNALS_SIGJWU_H
#define BB_SIGNALS_SIGJWU_H

/* Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved. */

#include <vector>
#include <bb/clientcore/PriceProvider.h>
#include <bb/clientcore/PriceSizeProviderSpec.h>
#include <bb/clientcore/TickProvider.h>
#include <bb/signals/Signal.h>
#include <bb/signals/SignalSpec.h>

namespace bb {
namespace signals {

// SigJwu
/// A Signal that given a vector of input PriceProvider (equity prices),
/// generates 
///     index = sum(equity*weights)
/// weights are equity weights in the index.
/// The length of the signal is the size of this weights vector.
///
class SigJwuPrice
    : public SignalStateImpl
    , private IClockListener
{
public:
    /// Constructs a SigJwuPrice with the given weights,
    /// and input equity PriceProvider.
    ///
    /// min_dt refers to the minimum amount of time must pass before a value is sampled.
    /// An empty timeval_t corresponds to no minimum delta time.

    SigJwuPrice( const instrument_t & indexInstr, const std::vector<instrument_t>& instr, const std::string &desc, ClockMonitorPtr spCM,
                   const std::vector<double>& vIndexWeights,
                   std::vector<IPriceProviderPtr> vspInputPP,
                   bool     m_include_equity,
                   ReturnMode returnMode,
                   const timeval_t min_dt_input = timeval_t() );

    /// Destructor
    virtual ~SigJwuPrice();

    /// Returns the index weights of this signal.
    const std::vector<double>& getIndexWeights() const
    {   return m_vIndexWeights; }


    /// Returns the min_dt for the input Equity PriceProvider.
    timeval_t getMinDeltaTime_Input() const
    {   return m_min_dt_input; }

private:
    // IPriceProvider Listeners
    void onInputPriceChanged( const IPriceProvider& pp,const int i);
    // void onInputPriceChanged( const int i );
    // IClockListener interface
    virtual void onWakeupCall( const timeval_t& ctv, const timeval_t& swtv, int reason, void* pData);

    void reset();
    virtual void recomputeState() const;

private:
    double                          m_index;
    ClockMonitorPtr                 m_spCM;
    const std::vector<double>       m_vIndexWeights;
    std::vector<instrument_t>       m_vspInstr; // equity instruments vector
    std::vector<IPriceProviderPtr>  m_vspInputPP;
    std::vector<Subscription>       m_vspInputPPSub;
    bool                            m_index_ready;
    bool                            m_include_equity;
    ReturnMode                      m_returnMode;
    std::vector<timeval_t>          m_lastChangeTv_input;
    std::vector<double>             m_lastPrice_input;
    const timeval_t                 m_min_dt_input;
};
BB_DECLARE_SHARED_PTR( SigJwuPrice );





/// SigJwuSpec
class SigJwuPriceSpec : public NoRefPxSignalSpec
{
public:
    BB_DECLARE_SCRIPTING();

    SigJwuPriceSpec()
        : m_min_dt_input(0.0)
        , m_returnMode(DIFF)
    {}
    SigJwuPriceSpec(const SigJwuPriceSpec &other);

    virtual instrument_t getInstrument() const { 
        instrument_t indexInstr=bb::instrument_t::acrindex("SHSE_000300");
        return indexInstr; 
    }


    virtual ISignalPtr build(SignalBuilder *builder) const;
    virtual void checkValid() const;
    virtual void hashCombine(size_t &result) const;
    virtual bool compare(const ISignalSpec *other) const;
    virtual void print(std::ostream &o, const LuaPrintSettings &ps) const;
    virtual void getDataRequirements(IDataRequirements *rqs) const;
    virtual SigJwuPriceSpec *clone() const;

    std::vector<instrument_t>       m_vInstr;
    double                          m_min_dt_input;
    std::vector<double>             m_vIndexWeights;
    bool                            m_include_equity;
    ReturnMode                      m_returnMode;
};
BB_DECLARE_SHARED_PTR( SigJwuPriceSpec );

} // namespace signals
} // namespace bb

#endif // BB_SIGNALS_SIGJWU_H

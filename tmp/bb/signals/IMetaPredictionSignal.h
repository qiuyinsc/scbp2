#ifndef BB_SIGNALS_IMETAPREDICTIONSIGNAL_H
#define BB_SIGNALS_IMETAPREDICTIONSIGNAL_H

/* Contents Copyright 2013 Athena Capital Research LLC. All Rights Reserved. */

#include <bb/core/smart_ptr.h>
#include <bb/core/Scripting.h>
#include <bb/clientcore/BookBuilder.h>
#include <bb/clientcore/ITickProviderFactory.h>

namespace bb {
namespace signals {

BB_FWD_DECLARE_SHARED_PTR( PredictionHistory );
/// Meta-prediction signal
/// Allows for signals that consider the aggregate prediction computed
/// using other signals.
///
/// Will need to give the signal access to the fully initialized prediction
class IMetaPredictionSignal
{
public:
    virtual ~IMetaPredictionSignal() {}

    virtual void setPredictionHistory( PredictionHistoryPtr pred_history ) = 0;
};

BB_DECLARE_SHARED_PTR( IMetaPredictionSignal );

} // namespace signals
} // namespace bb

#endif // BB_SIGNALS_IMETAPREDICTIONSIGNAL_H

#ifndef BB_SIGNALS_SIGFUTURESIGNAL_H
#define BB_SIGNALS_SIGFUTURESIGNAL_H

/* Contents Copyright 2013 Athena Capital Research LLC. All Rights Reserved. */

#include <bb/clientcore/ClientContext.h>
#include <bb/clientcore/PriceSizeProviderSpec.h>
#include <bb/signals/Signal.h>
#include <bb/signals/SignalSpec.h>

namespace bb {
namespace signals {

// Returns the future values of a given signal.
// Intended for use with "perfect future knowledge" simulations.
class SigFutureSignal : public SignalStateImpl
{
public:
    SigFutureSignal(const instrument_t& instr, const std::string &desc
                    , ClientContextPtr client_context
                    , LookAheadHistClientContextPtr look_ahead
                    , ISignalPtr spInputSignal, uint32_t inputIndex
                    , ptime_duration_t look_ahead_time
        );

    virtual ~SigFutureSignal();

    virtual const std::vector<double>& getSignalState() const;
protected:
    virtual void recomputeState() const;

    void reset();

    ClientContextPtr m_clientContext;
    LookAheadHistClientContextPtr m_lookAheadCC;

    ISignalPtr m_spInputSignal;
    uint32_t   m_inputIndex;
    ptime_duration_t m_lookAheadTime;

    Subscription m_spInputSignalSub;
};
BB_DECLARE_SHARED_PTR(SigFutureSignal);


/// SigFutureSignalSpec
class SigFutureSignalSpec : public NoRefPxSignalSpec
{
public:
    BB_DECLARE_SCRIPTING();

    SigFutureSignalSpec() 
    {}

    SigFutureSignalSpec(const SigFutureSignalSpec &e);

    virtual instrument_t getInstrument() const { return m_inputSignal->getInstrument(); }
    virtual std::string getDescription() const { return m_description; }

    virtual ISignalPtr build(SignalBuilder *builder) const;
    virtual void checkValid() const;
    virtual void hashCombine(size_t &result) const;
    virtual bool compare(const ISignalSpec *other) const;
    virtual void print(std::ostream &o, const LuaPrintSettings &ps) const;
    virtual void getDataRequirements(IDataRequirements *rqs) const;
    virtual SigFutureSignalSpec *clone() const;

    ISignalSpecPtr   m_inputSignal;
    uint32_t         m_inputIndex;
    ptime_duration_t m_lookAheadTime;
};
BB_DECLARE_SHARED_PTR(SigFutureSignalSpec);

} // namespace signals
} // namespace bb

#endif // BB_SIGNALS_SIGFUTURESIGNAL_H

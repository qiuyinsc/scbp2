#ifndef BB_SIGNALS_SIGNAL_H
#define BB_SIGNALS_SIGNAL_H

/* Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved. */


#include <map>
#include <bb/core/shared_vector.h>
#include <bb/clientcore/suppress_stdout.h>
#include <bb/clientcore/IBook.h>
#include <bb/clientcore/ClockMonitor.h>
#include <bb/clientcore/SourceMonitor.h>
#include <bb/clientcore/PriceProvider.h>

namespace bb {

BB_FWD_DECLARE_SHARED_PTR( IMStreamManager );

namespace signals {

BB_FWD_DECLARE_SHARED_PTR( ISignal );
BB_FWD_DECLARE_SHARED_PTR( ISignalListener );


/**
    Signal Library Priorities
**/
// The priority for signal printing, a high value outside the range of normal signal handling.
// We want the signal printer to run before any handling of the message has occurred -- we do
// this to avoid printing out signal values at timeval t that are based on a message whose
// timestamps is > t.
extern const Priority PRIORITY_SIGNAL_PREPRINTING;     // (0xD5000000);
extern const Priority PRIORITY_SIGNAL_PRINTING;        // (0xD4000000);

extern const Priority PRIORITY_SIGNALS_HIGHEST;        // (0x7FFFFFFF);
extern const Priority PRIORITY_SIGNALS_Signal;         // (0x78000000);
extern const Priority PRIORITY_SIGNALS_SignalManager;  // (0x72000000);
extern const Priority PRIORITY_SIGNALS_LOWEST;         // (0x70000000);


/// Interface for listeners of signals.
/// When a signal changes, its listeners are notified of the event.
class ISignalListener : public bb::IEventSubscriber
{
public:
    virtual ~ISignalListener() {}

    /// Invoked when a signal changes value.
    /// The reference to signal and its state vector are only valid during this method call.
    /// You must cache any relevant values.
    virtual void onSignalChanged( const ISignal& signal ) = 0;
};

/// Interface for a signal.
///
/// A Signal is simply a vector of numbers that represent some state.
/// The calculation and meaning of this state is Signal dependent.
///
/// Signals work primarily on a push model.  Clients that are interested
/// in a signal should add themselves as a listener to a signal.
class ISignal : public boost::noncopyable
{
public:
    typedef EventPublisher<ISignalListener> Listeners_t;
    typedef Subscription ListenerSubPtr;

    typedef boost::function<void(const std::vector<double>&)> Callback;
    typedef shared_vector<Callback> Callbacks_t;
    typedef Subscription CallbackSubPtr;

    virtual ~ISignal() {}

    /// Returns true if the signal is in a "valid" state.
    /// The meaning of this is signal dependent, however clients typically use it
    /// to test whether they should "trust" the signal's contents.
    virtual bool isOK() const                                        = 0;

    /// Returns the vector of numbers of this signal.
    /// The length of the vectors returned from getSignalState and getStateNames must be the same.
    /// In general, this should be a lightweight function so clients can call it repeatedly.
    /// Implementors are advised to use some sort of caching/dirty-bit strategy.
    virtual const std::vector<double>& getSignalState() const        = 0;

    /// Returns the length of the state and names vectors. This is not allowed to change
    /// at runtime.
    virtual size_t getStateSize() const                              = 0;

    /// Returns the timeval this signal last changed.
    virtual timeval_t getLastChangeTv() const                        = 0;

    /// Returns the timeval at which the last change was intended to happen,
    /// for signals which are based on a regular clock tick. This is optional,
    /// and only implemented for OvarGenerator at the moment.
    virtual boost::optional<timeval_t> getLastScheduledChangeTv() const = 0;

    /// Adds or removes a listener to this ISignal.
    /// When the Signal changes, the listener will be notified of the change.

    // New style signal listeners:
    // - They disconnect when going out of scope
    // - But otherwise you'll have to disconnect manually
    virtual bool addSignalListener( ISignalListener *listener ) = 0;
    virtual bool removeSignalListener( ISignalListener *listener ) = 0;

    // Old style signal listeners:
    // - The subscribtion disconnects the listener when going out of scope
    // - And when reset()
    virtual void addSignalListener( Subscription &callbackSub, ISignalListener *listener ) = 0;
    virtual void addSignalCallback( Subscription &callbackSub, Callback callback ) = 0;

    /// Signal Metadata accessors

    /// Returns the instrument associated with this Signal. There may be
    /// multiple instruments, but just pick a primary one.
    virtual const instrument_t& getInstrument() const                = 0;

    /// Returns the description string of this Signal. For signals which are being printed
    /// to a file, this must be unique. Otherwise it's essentially optional information.
    virtual const std::string& getDesc() const                       = 0;

    /// Returns the names of the components of the state vector.
    /// The length of the vectors returned from getSignalState and getStateNames must be the same.
    /// In general, this should be a lightweight function so clients can call it repeatedly.
    /// Implementors are advised to use some sort of caching/dirty-bit strategy.
    virtual const std::vector<std::string>& getStateNames() const    = 0;

};
BB_DECLARE_SHARED_PTR( ISignal );


/// Implements the Listener portion of the signal interface.
class SignalListenerImpl
    : public ISignal
{
public:
    // new style listeners
    virtual bool addSignalListener( ISignalListener *listener );
    virtual bool removeSignalListener( ISignalListener *listener );

    // old style listeners
    virtual void addSignalListener( Subscription &callbackSub, ISignalListener *listener );
    virtual void addSignalCallback( Subscription &callbackSub, Callback callback );

protected:
    void notifySignalListeners();
    Listeners_t m_listeners;
    Callbacks_t m_callbacks;
};

/// Common implementation of the signal state & dirty-bit strategy
class SignalStateImpl
    : public SignalListenerImpl
{
public:
    // ISignal impl
    virtual bool isOK() const;
    virtual const instrument_t& getInstrument() const { return m_instr; }
    virtual const std::string& getDesc() const { return m_desc; }
    virtual const std::vector<double>& getSignalState() const;
    virtual size_t getStateSize() const { return m_stateNames.size(); }
    virtual const std::vector<std::string>& getStateNames() const { return m_stateNames; }
    virtual timeval_t getLastChangeTv() const { return m_lastChangeTv; }
    virtual boost::optional<timeval_t> getLastScheduledChangeTv() const { return boost::none; }

protected:
    SignalStateImpl(const instrument_t &instr, const std::string &desc)
        : m_instr(instr), m_desc(desc), m_dirty(true), m_isOK(false) {}
    void allocState(std::string stateName) { m_stateNames.push_back(stateName); m_state.push_back(0.0); }

    /// This is called when the signal is dirty, and someone wants to know isOK or getSignalState.
    /// Should update m_state and m_isOK to update the state to reflect reality.
    virtual void recomputeState() const = 0;

    /// Resets the state to !ok, state == all zeros, and notifies listeners
    void reset(const timeval_t &changeTv);

    /// Updates the last change time and sets the dirty bit.
    void notifySignalListeners(const timeval_t &changeTv);

    instrument_t                    m_instr;
    std::string                     m_desc;
    std::vector<std::string>        m_stateNames;
    timeval_t                       m_lastChangeTv;
    mutable bool                    m_dirty;
    mutable bool                    m_isOK;
    mutable std::vector<double>     m_state;
};

/// Base class for signals which abstracts out the source monitor handling; the
/// variable m_bSourcesOK will be updated depending on whether the dependent sources
/// are up or down.
///
/// The virtual fn reset will be called after a sourcemonitor problem and at the open & close.
class SignalSmonImpl
    : public SignalStateImpl
    , public IClockListener
    , public SourceMonitorListener
{
public:
    virtual ~SignalSmonImpl();

    // ClockListener interface
    virtual void onWakeupCall( const timeval_t& ctv, const timeval_t& swtv, int reason, void* pData);

    // SourceMonitor hooks
    void registerWithSourceMonitors(ClientContext *cc, const sources_t &srcs);
    void registerWithSourceMonitors(ClientContextPtr cc, const sources_t &srcs)
        { registerWithSourceMonitors(cc.get(), srcs); }
    virtual void onSMonStatusChange(source_t src, smon_status_t status, smon_status_t old_status,
            const ptime_duration_t& avg_lag_tv, const ptime_duration_t& last_interval_tv);

protected:
    SignalSmonImpl(const instrument_t& instr, const std::string &desc, ClockMonitorPtr clockMon, int vbose);
    void notifySignalListeners();
    virtual void logClockNotice(int reason);
    virtual void reset();

protected:
    ClockMonitorPtr m_clockMonitor;

    bool m_bSourcesOK;
    int m_vboseLvl;
    int m_numDependentSources;
    std::map<source_t, smon_status_t> m_statusBySrc;
    std::vector<SourceMonitorPtr> m_sourceMonitors;
};

class SignalsList : public std::vector<ISignalPtr>{};
BB_DECLARE_SHARED_PTR( SignalsList );


} // namespace signals
} // namespace bb

#endif // BB_SIGNALS_SIGNAL_H

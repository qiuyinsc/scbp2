#ifndef BB_SIGNALS_BASELINEBIN_H
#define BB_SIGNALS_BASELINEBIN_H

/* Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved. */


#include <bb/core/ptime.h>
#include <bb/core/timeval.h>

namespace bb {
namespace signals {

// Object representation of a timeslice of a given product's daily ticks, for a given
// message stream.

// It is defined by the start and end of the time range and the statistics typical of the
// fills during that time range.

// It's main purpose is to take in an arbitrary window of time and return a given statistic's
// (volume, volume stdev, price stdev, and price stdev stdev) value in that window of time.
//  This value is just a base value multiplied times the fraction of the window that is in this bin.

class BaselineBin {
public:
    BaselineBin(const ptime_duration_t& _start_time_adjustment, const ptime_duration_t& _end_time_adjustment,
            double _volume, double _volume_stdev, double _price_stdev, double _price_stdev_stdev, int _vbose);

    void setTimes(timeval_t _trading_start_time);

    timeval_t getStartTime();

    bool isThisBin(timeval_t window_start_time, timeval_t window_end_time) ;

    double getWeightedVolume( timeval_t window_start_time, timeval_t window_end_time );
    double getWeightedVolumeStdev( timeval_t window_start_time, timeval_t window_end_time );
    double getWeightedPriceStdev( timeval_t window_start_time, timeval_t window_end_time );
    double getWeightedPriceStdevStdev( timeval_t window_start_time, timeval_t window_end_time );

private:

    ptime_duration_t start_time_adjustment;
    ptime_duration_t end_time_adjustment;

    timeval_t start_time ;
    timeval_t end_time ;


    double baseline_volume_mean ;
    double baseline_volume_stdev ;
    double baseline_price_stdev_mean ;
    double baseline_price_stdev_stdev ;

    int vbose;

    double timeWeight( double unweighted_value, timeval_t window_start_time, timeval_t window_end_time );

};

} // namespace signals
} // namespace bb
#endif // BB_SIGNALS_BASELINEBIN_H

#ifndef BB_SIGNALS_MODELDATAWRITERSPEC_H
#define BB_SIGNALS_MODELDATAWRITERSPEC_H

/* Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved. */


#include <bb/core/instrument.h>
#include <bb/core/smart_ptr.h>
#include <bb/core/Scripting.h>
#include <bb/clientcore/PriceProviderSpec.h>

namespace bb {

class IDataRequirements;
class date_t;

namespace signals {

// Forward Declarations
class ModelDataWriterSpec;
class ISignalBuilder;

BB_FWD_DECLARE_SHARED_PTR( ISignal );
BB_FWD_DECLARE_SHARED_PTR( IModelDataWriter );
BB_FWD_DECLARE_SHARED_PTR( IModelDataWriterSpec );

/*******************************************************************************************/
//
// ModelDataWriterSpec interface and a useful base implementation
//
/*******************************************************************************************/

class IModelDataWriterSpec
{
public:
    BB_DECLARE_SCRIPTING();

    virtual ~IModelDataWriterSpec() {}

    /// Instantiates a concrete ISignal according to the Spec
    virtual IModelDataWriterPtr build( ISignalBuilder* builder ) const = 0;

    /// deep copy this IModelDataWriterSpec
    virtual IModelDataWriterSpec* clone() const = 0;
    inline IModelDataWriterSpec* clone( const IModelDataWriterSpec* e ) { return e ? e->clone() : NULL; }
    inline IModelDataWriterSpec* clone( IModelDataWriterSpecCPtr e ) { return clone( e.get() ); }

    /// Combines this IModelDataWriterSpec into the given hash value
    virtual void hashCombine( size_t& result ) const = 0;

    /// Compares two signal specs.
    virtual bool compare( const IModelDataWriterSpec* other ) const = 0;

    /// Prints the lua representation of this ModelDataWriterSpec.
    virtual void print( std::ostream& o, const LuaPrintSettings& ps ) const = 0;

    /// ensures sure that all fields have been set correctly, or throws runtime_error
    virtual void checkValid() const = 0;

    /// Propagates all the required data ( instr/src pairs ) to the IDataRequirements.
    virtual void getDataRequirements( IDataRequirements* rqs ) const = 0;

    // Convenience function to set the output filename base, as an app as opposed to a
    // user is probably what most often should determine the output dir/base
    virtual void setOutputFilenameBase( const std::string& outputFilenameBase ) = 0;
    virtual std::string getOutputFilenameBase( const std::string& outputFilenameBase ) const = 0;
};
BB_DECLARE_SHARED_PTR( IModelDataWriterSpec );

static const std::string emptyString = "";

class ModelDataWriterSpec : public IModelDataWriterSpec
{
public:
    BB_DECLARE_SCRIPTING();

    virtual void hashCombine( size_t& result ) const;
    virtual bool compare( const IModelDataWriterSpec* other ) const;

    virtual void checkValid() const {}
    virtual void getDataRequirements( IDataRequirements* rqs ) const {}

    virtual void setOutputFilenameBase( const std::string& outputFilenameBase ) {}
    virtual std::string getOutputFilenameBase( const std::string& outputFilenameBase ) const { return emptyString; }

    bool m_verbose;

protected:
    ModelDataWriterSpec() : m_verbose( false ) {}
    ModelDataWriterSpec( bool verbose ) : m_verbose( verbose ) {}
    ModelDataWriterSpec( const ModelDataWriterSpec& e );
};
BB_DECLARE_SHARED_PTR( ModelDataWriterSpec );

class TextModelDataWriterSpec : public ModelDataWriterSpec
{
public:
    BB_DECLARE_SCRIPTING();

    virtual void hashCombine( size_t& result ) const;
    virtual bool compare( const IModelDataWriterSpec* other ) const;
    virtual void checkValid() const;
    virtual void getDataRequirements( IDataRequirements* rqs ) const {}

    virtual void setOutputFilenameBase( const std::string& outputFilenameBase );
    virtual std::string getOutputFilenameBase( const std::string& outputFilenameBase ) const { return m_outputFilenameBase; }

    std::string m_outputFilenameBase;
    bb::date_t m_date;
    std::string m_printFormat;
    size_t m_buffSize;

protected:
    TextModelDataWriterSpec( const std::string& outputFilenameBase = emptyString, bb::date_t date = bb::date_t::today(),
                             const std::string& printFormat = "%7.05f", size_t buffSize = 256, bool verbose = false );
    TextModelDataWriterSpec( const TextModelDataWriterSpec& e );
};
BB_DECLARE_SHARED_PTR( TextModelDataWriterSpec );

class IModelDataWriterSpecList : public std::vector<IModelDataWriterSpecCPtr>{};
BB_DECLARE_SHARED_PTR( IModelDataWriterSpecList );

/// Helper class for converting a string into a ModelDataWriterSpec.
class ModelDataWriterSpecReader : public bb::SpecReader
{
public:
    ModelDataWriterSpecReader();

    // parses the given lua code ( which should be lua code for a signal spec ),
    // and returns the signal spec
    IModelDataWriterSpecPtr getModelDataWriterSpec( std::string txt );
};

std::size_t hash_value( const IModelDataWriterSpec& a );

// SWIG 1.3.29 doesn't like these in-namespace operators
#ifndef SWIG

bool operator==( const IModelDataWriterSpec& a, const IModelDataWriterSpec& b ); // defined in ModelDataWriterSpec.cc
inline bool operator!=( const IModelDataWriterSpec& a, const IModelDataWriterSpec& b ) { return !( a == b ); }

std::ostream& operator<<( std::ostream& out, const IModelDataWriterSpec& spec );
void luaprint( std::ostream& out, bb::signals::IModelDataWriterSpec const& bk, LuaPrintSettings const& ps );

#endif // !SWIG

// helpers for hash_map
struct ModelDataWriterSpecHasher : public std::unary_function<size_t, IModelDataWriterSpecCPtr>
    { size_t operator()( const IModelDataWriterSpecCPtr& a ) const { return hash_value( *a ); } };
struct ModelDataWriterSpecComparator : public std::binary_function<bool, IModelDataWriterSpecCPtr, IModelDataWriterSpecCPtr>
    { bool operator()( const IModelDataWriterSpecCPtr& a, const IModelDataWriterSpecCPtr& b ) const { return *a == *b; } };

} // namespace signals
} // namespace bb

#endif // BB_SIGNALS_MODELDATAWRITERSPEC_H

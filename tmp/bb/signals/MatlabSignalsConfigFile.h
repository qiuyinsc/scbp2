#ifndef BB_SIGNALS_MATLABSIGNALSCONFIGFILE_H
#define BB_SIGNALS_MATLABSIGNALSCONFIGFILE_H

/* Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved. */


#include <bb/core/instrument.h>
#include <bb/core/LuaState.h>
#include <bb/core/LuabindScripting.h>
#include <bb/signals/SignalSpec.h>
#include <bb/signals/SignalsConfigFile.h>

namespace bb {
namespace signals {

/** Matlab-friendly signals config file format

Note: In the signal.input section, this can be a table of signal definitions,
   or a table of a table, the loader is recursive.  The result is flatten out in
   depth first order.

@code

model = {
   desc = {
      'PeriodicDiff_AL',
      'PeriodicDiff_AU',
      'PeriodicDiff_CU',
      'PeriodicDiff_FU',
      'PeriodicDiff_RU',
      'PeriodicDiff_ZN',
   }
   constantWeight = -0.0099,
   weights = {
      -0.0591,
      10.4064,
      0.2714,
      -0.1984,
      -0.0459,
      0.0336,
   }
}

signal = {
   target = { "FUT_SHFE_CU", model, RefBookFactory() },
   input = {
      def { PeriodicDiffSpec, description = 'PeriodicDiff_AL', period = 5, symbol = 'FUT_SHFE_AL' },
      def { PeriodicDiffSpec, description = 'PeriodicDiff_AU', period = 5, symbol = 'FUT_SHFE_AU' },
      def { PeriodicDiffSpec, description = 'PeriodicDiff_CU', period = 5, symbol = 'FUT_SHFE_CU' },
      def { PeriodicDiffSpec, description = 'PeriodicDiff_FU', period = 5, symbol = 'FUT_SHFE_FU' },
      def { PeriodicDiffSpec, description = 'PeriodicDiff_RU', period = 5, symbol = 'FUT_SHFE_RU' },
      def { PeriodicDiffSpec, description = 'PeriodicDiff_ZN', period = 5, symbol = 'FUT_SHFE_ZN' },
   }
}

@endcode
*/

class MatlabSignalsConfigFile : public bb::signals::ConfigFile
{
public:
    MatlabSignalsConfigFile( bool runLive
        , const date_t& startDate
        , const date_t& endDate
        , LuaStatePtr config )
        : bb::signals::ConfigFile( runLive, startDate, endDate, config )
    {
    }

    virtual void load( const std::string& symbolsFilename )
    {
        if( m_fileLoaded )
            BB_THROW_ERROR_SS("ConfigFile: tried to load a file twice");

        m_config->load( symbolsFilename.c_str() );

        loadSettings( m_config->root()["signal"] );
    }

    virtual void loadSettings( const luabind::object& root );
    virtual void getAllSignals( std::vector<ISignalSpecCPtr>* signals ) const
    {
        loadSignalSpecs( signals, m_inputs );
    }
    virtual void getSignals( std::vector<ISignalSpecCPtr>* signals
        , const ConfigFile::ProductEntry& prod
        , bool isTarget ) const
    {
        signals->clear();
    }

    const luabind::object& getModel() const { return m_model; }

protected:
    void loadSignalSpecs( std::vector<ISignalSpecCPtr>* dest, const luabind::object& specs ) const;

protected:
    luabind::object m_model;
    luabind::object m_inputs;
};

BB_DECLARE_SHARED_PTR( MatlabSignalsConfigFile );

} // signals
} // bb

#endif // BB_SIGNALS_MATLABSIGNALSCONFIGFILE_H

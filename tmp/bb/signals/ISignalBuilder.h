#ifndef BB_SIGNALS_ISIGNALBUILDER_H
#define BB_SIGNALS_ISIGNALBUILDER_H

/* Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved. */

#include <bb/core/smart_ptr.h>

namespace bb {

BB_FWD_DECLARE_SHARED_PTR( ClientContext );
BB_FWD_DECLARE_SHARED_PTR( ClockMonitor );
BB_FWD_DECLARE_SHARED_PTR( EventDistributor );
BB_FWD_DECLARE_SHARED_PTR( IBookBuilder );
BB_FWD_DECLARE_SHARED_PTR( IMStreamManager );
BB_FWD_DECLARE_SHARED_PTR( ITickProviderFactory );
BB_FWD_DECLARE_SHARED_PTR( PriceProviderBuilder );

namespace statistics {
BB_FWD_DECLARE_SHARED_PTR( IStatisticBuilder );
}

namespace signals {

BB_FWD_DECLARE_SHARED_PTR( ISignalSpec );
BB_FWD_DECLARE_SHARED_PTR( ISignal );

class ISignalBuilder
{
public:
    virtual ~ISignalBuilder() {}

    virtual bb::IBookBuilderPtr getBookBuilder() = 0;
    virtual bb::PriceProviderBuilderPtr getPxPBuilder() const = 0;
    virtual bb::ITickProviderFactoryPtr getSourceTickFactory() const = 0;
    virtual bb::statistics::IStatisticBuilderPtr getStatisticBuilder() const = 0;
    virtual ISignalPtr buildSignal(ISignalSpecCPtr spec) = 0;

    virtual bool getRunLive() const = 0;
    virtual int getVerboseLevel() const = 0;
    virtual ClientContextPtr getClientContext() const = 0;
    virtual ClockMonitorPtr getClockMonitor() const = 0;
    virtual IMStreamManagerPtr getMStreamManager() const = 0;
    virtual EventDistributorPtr getEventDistributor() const = 0;

    virtual void setVerboseLevel(int vbose_lvl) = 0;
    virtual void onEndTimeReached() {}
};
BB_DECLARE_SHARED_PTR(ISignalBuilder);

} // namespace signals
} // namespace bb

#endif // BB_SIGNALS_ISIGNALBUILDER_H

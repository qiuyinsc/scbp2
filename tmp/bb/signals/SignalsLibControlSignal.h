#ifndef BB_SIGNALS_SIGNALSLIBCONTROLSIGNAL_H
#define BB_SIGNALS_SIGNALSLIBCONTROLSIGNAL_H

/* Contents Copyright 2013 Athena Capital Research LLC. All Rights Reserved. */

#include <bb/core/Scripting.h>
#include <bb/core/LuabindScripting.h>
#include <bb/signals/IControlSignal.h>

namespace bb {

BB_FWD_DECLARE_SHARED_PTR( ClientContext );

namespace signals {

BB_FWD_DECLARE_SHARED_PTR( ISignalBuilder );
BB_FWD_DECLARE_SHARED_PTR( ISignalSpec );
BB_FWD_DECLARE_SHARED_PTR( ISignal );

class SignalsLibControlSignal : public IControlSignal
{
public:
    class Spec : public IControlSignal::Spec
    {
    public:
        BB_DECLARE_SCRIPTING();

        Spec();

        virtual IControlSignalPtr create( const ISignalBuilderPtr& signalBuilder );

        ISignalSpecPtr signal;
        double threshold;
        bool absolute_threshold;
        bool inverse_threshold;
    };

public:
    SignalsLibControlSignal( const ISignalBuilderPtr& signalBuilder, const Spec& params = Spec() );

    virtual int32_t value() const;
    virtual bool isValid() const;

    ISignalPtr getBaseSignal() { return m_spSignal;}

private:
    const Spec m_params;
    ISignalBuilderPtr m_spSignalBuilder;
    ISignalPtr m_spSignal;
};

BB_DECLARE_SHARED_PTR(SignalsLibControlSignal);

} // namespace signals
} // namespace bb

#endif // BB_SIGNALS_SIGNALSLIBCONTROLSIGNAL_H

#ifndef BB_SIGNALS_SIGDECAY2_H
#define BB_SIGNALS_SIGDECAY2_H

/* Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved. */


#include <bb/clientcore/PriceProvider.h>
#include <bb/clientcore/PriceSizeProviderSpec.h>

#include <bb/signals/Signal.h>
#include <bb/signals/SignalSpec.h>
#include <bb/signals/MovingAverage.h>


namespace bb {
namespace signals {

/// A Signal that given a reference PriceProvider and an input PriceProvider,
/// generates a signal where the signal state is:
///
///    MA(inputPx) - refPx
///
/// MA is a MovingAverageGenerator with specified vDecayWeights.
/// The length of the signal is the size of this vDecayWeights vector.
///
class SigDecayPrice
    : public SignalStateImpl
    , private IClockListener
{
public:
    /// Constructs a SigDecayPrice with the given moving average decay weights,
    /// reference PriceProvider and input PriceProvider.
    ///
    /// min_dt refers to the minimum amount of time must pass before a value is sampled.
    /// An empty timeval_t corresponds to no minimum delta time.
    /// There are separate values for the reference price and the input price, though they
    /// can be the same.
    SigDecayPrice( const instrument_t& instr, const std::string &desc, ClockMonitorPtr spCM,
                   const std::vector<double>& vDecayWeights,
                   IPriceProviderPtr spRefPP, IPriceProviderPtr spInputPP,
                   ReturnMode returnMode,
                   const timeval_t min_dt_ref = timeval_t(),
                   const timeval_t min_dt_input = timeval_t() );

    /// Constructs a SigDecayPrice with the given moving average decay weights,
    /// and the given PP is both the reference PriceProvider and input PriceProvider.
    ///
    /// min_dt refers to the minimum amount of time must pass before a value is sampled.
    /// An empty timeval_t corresponds to no minimum delta time.
    SigDecayPrice( const instrument_t& instr, const std::string &desc, ClockMonitorPtr spCM,
                   const std::vector<double>& vDecayWeights,
                   IPriceProviderPtr spPP,
                   ReturnMode returnMode,
                   const timeval_t min_dt = timeval_t() );

    /// Destructor
    virtual ~SigDecayPrice();

    /// Returns the decay weights of this signal.
    const std::vector<double>& getDecayWeights() const
    {   return m_mag_px.getDecayWeights(); }

    /// Returns the min_dt for the reference PriceProvider.
    timeval_t getMinDeltaTime_Ref() const
    {   return m_min_dt_ref; }

    /// Returns the min_dt for the input PriceProvider.
    timeval_t getMinDeltaTime_Input() const
    {   return m_min_dt_input; }

private:
    // IPriceProvider Listeners
    void onRefPriceChanged(   const IPriceProvider& pp );
    void onInputPriceChanged( const IPriceProvider& pp );
    // IClockListener interface
    virtual void onWakeupCall( const timeval_t& ctv, const timeval_t& swtv, int reason, void* pData);

    void reset();
    virtual void recomputeState() const;

private:
    MovingAverageGenerator          m_mag_px;
    ClockMonitorPtr                 m_spCM;
    IPriceProviderPtr               m_spRefPP,    m_spInputPP;
    Subscription                    m_spRefPPSub, m_spInputPPSub;
    double                          m_refpx;
    bool                            m_refpx_ready, m_ma_ready;
    ReturnMode                      m_returnMode;
    timeval_t                       m_lastChangeTv_ref, m_lastChangeTv_input;
    const timeval_t                 m_min_dt_ref, m_min_dt_input;
};
BB_DECLARE_SHARED_PTR( SigDecayPrice );


/// SignalSpec for SigDecayPrice
class SigDecayPriceSpec : public SignalSpec
{
public:
    BB_DECLARE_SCRIPTING();

    SigDecayPriceSpec()
        : m_min_dt_ref(0.0)
        , m_min_dt_input(0.0)
        , m_returnMode(DIFF)
    {}
    SigDecayPriceSpec(const SigDecayPriceSpec &other);

    virtual instrument_t getInstrument() const { return m_inputPxP->getInstrument(); }
    virtual ISignalPtr build(SignalBuilder *builder) const;
    virtual void checkValid() const;
    virtual void hashCombine(size_t &result) const;
    virtual bool compare(const ISignalSpec *other) const;
    virtual void print(std::ostream &o, const LuaPrintSettings &ps) const;
    virtual void getDataRequirements(IDataRequirements *rqs) const;
    virtual SigDecayPriceSpec *clone() const;

    IPxProviderSpecPtr  m_inputPxP;
    double              m_min_dt_ref, m_min_dt_input;
    std::vector<double> m_vDecayWeights;
    ReturnMode m_returnMode;
};
BB_DECLARE_SHARED_PTR( SigDecayPriceSpec );



/// A Signal that given a reference PriceProvider and an input PriceSizeProvider,
/// generates a signal where the signal state is:
///
///    (MA(inputPx*inputSz) / MA(inputSz)) - refPx
///
/// MA is a MovingAverageGenerator with specified vDecayWeights.
/// The length of the signal is the size of this vDecayWeights vector.
///
/// VW refers to 'volume weighted'.
///
class SigDecayVWPrice
    : public SignalStateImpl
    , private IClockListener
{
public:
    /// Constructs a SigDecayVWPrice with the given moving average decay weights,
    /// reference PriceProvider and input PriceSizeProvider.
    ///
    /// min_dt refers to the minimum amount of time must pass before a value is sampled.
    /// An empty timeval_t corresponds to no minimum delta time.
    /// There are separate values for the reference price and the input price, though they
    /// can be the same.
    SigDecayVWPrice(
            const instrument_t& instr, const std::string &desc, ClockMonitorPtr spCM,
            const std::vector<double>& vDecayWeights,
            IPriceProviderPtr spRefPP, IPriceSizeProviderPtr spInputPSP,
            ReturnMode returnMode,
            const timeval_t min_dt_ref = timeval_t(),
            const timeval_t min_dt_input = timeval_t() );

    /// Constructs a SigDecayVWPrice with the given moving average decay weights,
    /// and the given PP is both the reference PriceProvider and input PriceProvider.
    ///
    /// min_dt refers to the minimum amount of time must pass before a value is sampled.
    /// An empty timeval_t corresponds to no minimum delta time.
    SigDecayVWPrice(
            const instrument_t& instr, const std::string &desc, ClockMonitorPtr spCM,
            const std::vector<double>& vDecayWeights,
            IPriceSizeProviderPtr spInputPSP,
            ReturnMode returnMode,
            const timeval_t min_dt = timeval_t() );

    /// Destructor
    virtual ~SigDecayVWPrice();

    /// Returns the decay weights of this signal.
    const std::vector<double>& getDecayWeights() const
    {   return m_mag_pxsz.getDecayWeights(); }

    /// Returns the min_dt for the reference PriceProvider.
    timeval_t getMinDeltaTime_Ref() const
    {   return m_min_dt_ref; }

    /// Returns the min_dt for the input PriceProvider.
    timeval_t getMinDeltaTime_Input() const
    {   return m_min_dt_input; }

private:
    // IPriceProvider Listener
    void onRefPriceChanged( const IPriceProvider& pp );
    // IPriceSizeProvider Listener
    void onInputPriceSizeChanged( const IPriceSizeProvider& psp );
    // IClockListener interface
    virtual void onWakeupCall( const timeval_t& ctv, const timeval_t& swtv, int reason, void* pData);

    void reset();
    virtual void recomputeState() const;

private:
    MovingAverageGenerator              m_mag_pxsz, m_mag_sz;
    ClockMonitorPtr                     m_spCM;
    IPriceProviderPtr                   m_spRefPP;
    Subscription                        m_spRefPPSub;
    IPriceSizeProviderPtr               m_spInputPSP;
    Subscription                        m_spInputPSPSub;
    double                              m_refpx;
    bool                                m_refpx_ready, m_ma_ready;
    timeval_t                           m_lastChangeTv_ref, m_lastChangeTv_input;
    ReturnMode                          m_returnMode;
    const timeval_t                     m_min_dt_ref, m_min_dt_input;
};
BB_DECLARE_SHARED_PTR( SigDecayVWPrice );


/// SignalSpec for SigDecayVWPrice
class SigDecayVWPriceSpec : public SignalSpec
{
public:
    BB_DECLARE_SCRIPTING();

    SigDecayVWPriceSpec()
        : m_min_dt_ref(0.0)
        , m_min_dt_input(0.0)
        , m_returnMode(DIFF)
    {}
    SigDecayVWPriceSpec(const SigDecayVWPriceSpec &other);

    virtual instrument_t getInstrument() const { return m_inputPxSzP->getInstrument(); }
    virtual ISignalPtr build(SignalBuilder *builder) const;
    virtual void checkValid() const;
    virtual void hashCombine(size_t &result) const;
    virtual bool compare(const ISignalSpec *other) const;
    virtual void print(std::ostream &o, const LuaPrintSettings &ps) const;
    virtual void getDataRequirements(IDataRequirements *rqs) const;
    virtual SigDecayVWPriceSpec *clone() const;

    IPxSzProviderSpecPtr    m_inputPxSzP;
    double                  m_min_dt_ref, m_min_dt_input;
    std::vector<double>     m_vDecayWeights;
    ReturnMode   m_returnMode;
};
BB_DECLARE_SHARED_PTR( SigDecayVWPriceSpec );



/// A Signal that given two PriceProviders, generates a signal where the signal state is:
///
///    MA(px2 - px1) - (px2 - px1)
///
/// MA is a MovingAverageGenerator with specified vDecayWeights.
/// The length of the signal is the size of this vDecayWeights vector.
///
class SigDecaySpread
    : public SignalStateImpl
    , private IClockListener
{
public:
    /// Constructs a SigDecaySpread with the given decay weights and PriceProviders.
    ///
    /// min_dt refers to the minimum amount of time must pass before a value is sampled.
    /// An empty timeval_t corresponds to no minimum delta time.
    /// There are separate values for the different input prices.
    SigDecaySpread( const instrument_t& instr, const std::string &desc, ClockMonitorPtr spCM,
               const std::vector<double>& vDecayWeights,
               IPriceProviderPtr spPriceProv1, IPriceProviderPtr spPriceProv2,
               const timeval_t min_dt_input1 = timeval_t(),
               const timeval_t min_dt_input2 = timeval_t() );

    /// Destructor
    virtual ~SigDecaySpread();

    /// Returns the decay weights of this signal.
    const std::vector<double>& getDecayWeights() const
    {   return m_mag_spread.getDecayWeights(); }

    /// Returns the min_dt for PriceProvider 1.
    timeval_t getMinDeltaTime_Input1() const
    {   return m_min_dt_input1; }

    /// Returns the min_dt for PriceProvider 2.
    timeval_t getMinDeltaTime_Input2() const
    {   return m_min_dt_input2; }

private:
    // IPriceProvider Listeners
    void onPriceChanged_1( const IPriceProvider& pp );
    void onPriceChanged_2( const IPriceProvider& pp );
    // IClockListener interface
    virtual void onWakeupCall( const timeval_t& ctv, const timeval_t& swtv, int reason, void* pData);

    void reset();
    virtual void recomputeState() const;

private:
    MovingAverageGenerator              m_mag_spread;
    ClockMonitorPtr                     m_spCM;
    IPriceProviderPtr                   m_spPP1,    m_spPP2;
    Subscription                        m_spPPSub1, m_spPPSub2;
    bool                                m_px1_ready, m_px2_ready;
    double                              m_px1, m_px2;
    timeval_t                           m_lastChangeTv_input1, m_lastChangeTv_input2;
    const timeval_t                     m_min_dt_input1, m_min_dt_input2;
};
BB_DECLARE_SHARED_PTR( SigDecaySpread );


/// SignalSpec for SigDecaySpread
class SigDecaySpreadSpec : public ISignalSpec
{
public:
    BB_DECLARE_SCRIPTING();

    SigDecaySpreadSpec()
        : m_min_dt_input1(0.0)
        , m_min_dt_input2(0.0)
    {}
    SigDecaySpreadSpec(const SigDecaySpreadSpec &other);

    virtual ISignalPtr build(SignalBuilder *builder) const;
    virtual void checkValid() const;
    virtual void hashCombine(size_t &result) const;
    virtual bool compare(const ISignalSpec *other) const;
    virtual void print(std::ostream &o, const LuaPrintSettings &ps) const;
    virtual void getDataRequirements(IDataRequirements *rqs) const;
    virtual SigDecaySpreadSpec *clone() const;

    virtual instrument_t getInstrument() const { return m_instrument; }
    virtual std::string getDescription() const { return m_description; }

    instrument_t m_instrument;
    std::string m_description;

    IPxProviderSpecPtr  m_inputPxP1, m_inputPxP2;
    double              m_min_dt_input1, m_min_dt_input2;
    std::vector<double> m_vDecayWeights;
};
BB_DECLARE_SHARED_PTR( SigDecaySpreadSpec );



/// A Signal that given a reference PriceProvider and an input Signal and an index,
/// generates a signal where the signal state is:
///
///    MA(inputSig(index)) - refPx
///
/// MA is a MovingAverageGenerator with specified vDecayWeights.
/// The length of the signal is the size of this vDecayWeights vector.
///
class SigDecaySignal
    : public SignalStateImpl
    , private ISignalListener
    , private IClockListener
{
public:
    /// Constructs a SigDecayPrice with the given moving average decay weights,
    /// reference PriceProvider and input Signal with index.
    ///
    /// min_dt refers to the minimum amount of time must pass before a value is sampled.
    /// An empty timeval_t corresponds to no minimum delta time.
    /// There are separate values for the reference price and the signal input.
    SigDecaySignal( ClockMonitorPtr cm, const instrument_t& instr, const std::string &desc,
                    const std::vector<double>& vDecayWeights,
                    IPriceProviderPtr spRefPP,
                    ISignalPtr spInputSignal, uint32_t inputIndex,
                    ReturnMode returnMode,
                    const timeval_t min_dt_ref = timeval_t(),
                    const timeval_t min_dt_input = timeval_t() );

    /// Constructs a SigDecayPrice with the given moving average decay weights
    /// and input Signal with index.  refPx is always 0.
    ///
    /// min_dt refers to the minimum amount of time must pass before a value is sampled.
    /// An empty timeval_t corresponds to no minimum delta time.
    SigDecaySignal( ClockMonitorPtr cm, const instrument_t& instr, const std::string &desc,
                    const std::vector<double>& vDecayWeights,
                    ISignalPtr spInputSignal, uint32_t inputIndex,
                    ReturnMode returnMode,
                    const timeval_t min_dt_input = timeval_t() );

    /// Destructor
    virtual ~SigDecaySignal();

    void onWakeupCall(const timeval_t& ctv, const timeval_t& swtv, int reason, void* pData );

    /// Returns the decay weights of this signal.
    const std::vector<double>& getDecayWeights() const
    {   return m_mag_sig.getDecayWeights(); }

    /// Returns the min_dt for the reference PriceProvider.
    timeval_t getMinDeltaTime_Ref() const
    {   return m_min_dt_ref; }

    /// Returns the min_dt for the input Signal.
    timeval_t getMinDeltaTime_Input() const
    {   return m_min_dt_input; }

private:
    void construct( const std::vector<double>& vDecayWeights );

    // IPriceProvider Listener
    void onRefPriceChanged(   const IPriceProvider& pp );
    // ISignalListener
    virtual void onSignalChanged( const ISignal& signal );

    void reset();
    virtual void recomputeState() const;

private:
    MovingAverageGenerator          m_mag_sig;
    IPriceProviderPtr               m_spRefPP;
    Subscription                    m_spRefPPSub;
    ClockMonitorPtr                 m_spCM;
    ISignalPtr                      m_spInputSignal;
    uint32_t                        m_inputIndex;
    ReturnMode                      m_returnMode;
    double                          m_refpx, m_last_val;
    bool                            m_refpx_ready, m_ma_ready;
    timeval_t                       m_lastChangeTv_ref, m_lastChangeTv_input;
    const timeval_t                 m_min_dt_ref, m_min_dt_input;
};
BB_DECLARE_SHARED_PTR( SigDecaySignal );


/// SignalSpec for SigDecaySignal
class SigDecaySignalSpec : public NoRefPxSignalSpec
{
public:
    BB_DECLARE_SCRIPTING();

    SigDecaySignalSpec()
        : m_returnMode(DIFF)
        , m_min_dt_ref(0.0)
        , m_min_dt_input(0.0)
    {}
    SigDecaySignalSpec(const SigDecaySignalSpec &other);

    virtual instrument_t getInstrument() const { return m_inputSignal->getInstrument(); }
    virtual ISignalPtr build(SignalBuilder *builder) const;
    virtual void checkValid() const;
    virtual void hashCombine(size_t &result) const;
    virtual bool compare(const ISignalSpec *other) const;
    virtual void print(std::ostream &o, const LuaPrintSettings &ps) const;
    virtual void getDataRequirements(IDataRequirements *rqs) const;
    virtual SigDecaySignalSpec *clone() const;

    IPxProviderSpecPtr         m_refPxP;
    ISignalSpecPtr             m_inputSignal;
    uint32_t                   m_inputIndex;
    ReturnMode m_returnMode;
    double                     m_min_dt_ref, m_min_dt_input;
    std::vector<double>        m_vDecayWeights;
};
BB_DECLARE_SHARED_PTR( SigDecaySignalSpec );


} // namespace signals
} // namespace bb


#endif // BB_SIGNALS_SIGDECAY2_H

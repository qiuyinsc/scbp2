#ifndef BB_SIGNALS_SIGAGGRESSIVETRADES_H
#define BB_SIGNALS_SIGAGGRESSIVETRADES_H

/* Contents Copyright 2013 Athena Capital Research LLC. All Rights Reserved. */


#include <bb/clientcore/PriceSizeProviderSpec.h>

#include <bb/signals/Signal.h>
#include <bb/signals/SignalSpec.h>

namespace bb {
namespace signals {

/// Compares a price provider and a book and reports when the prices are occurring
/// outside the book.
class SigAggressiveTrades : public SignalStateImpl
{
public:
    SigAggressiveTrades(const instrument_t& instr, const std::string &desc
                        , IPriceProviderPtr price_provider, IBookPtr book, ReturnMode return_mode);

protected:
    void onPriceChanged( const IPriceProvider& pp );
    virtual void recomputeState() const;

    IPriceProviderPtr m_priceProvider;
    IBookPtr m_book;
    Subscription m_pxpListenerSub;

    ReturnMode m_returnMode;
};
BB_DECLARE_SHARED_PTR(SigAggressiveTrades);


/// SigAggressiveTradesSpec
class SigAggressiveTradesSpec : public NoRefPxSignalSpec
{
public:
    BB_DECLARE_SCRIPTING();

    SigAggressiveTradesSpec() 
        : m_returnMode(DIFF)
    {}

    SigAggressiveTradesSpec(const SigAggressiveTradesSpec &e);

    virtual instrument_t getInstrument() const { return m_priceProvider->getInstrument(); }
    virtual std::string getDescription() const { return m_description; }

    virtual ISignalPtr build(SignalBuilder *builder) const;
    virtual void checkValid() const;
    virtual void hashCombine(size_t &result) const;
    virtual bool compare(const ISignalSpec *other) const;
    virtual void print(std::ostream &o, const LuaPrintSettings &ps) const;
    virtual void getDataRequirements(IDataRequirements *rqs) const;
    virtual SigAggressiveTradesSpec *clone() const;

    IPxProviderSpecPtr m_priceProvider;
    IBookSpecPtr m_book;

    ReturnMode m_returnMode;
};
BB_DECLARE_SHARED_PTR(SigAggressiveTradesSpec);

} // namespace signals
} // namespace bb

#endif // BB_SIGNALS_SIGAGGRESSIVETRADES_H

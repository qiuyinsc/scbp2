#ifndef BB_SIGNALS_SIGNALMANAGER_H
#define BB_SIGNALS_SIGNALMANAGER_H

/* Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved. */


#include <vector>
#include <string>
#include <fstream>
#include <bb/core/smart_ptr.h>
#include <bb/core/hash_map.h>
#include <bb/core/ptime.h>
#include <bb/clientcore/ClockMonitor.h>
#include <bb/clientcore/IClockListener.h>

namespace bb {

BB_FWD_DECLARE_SHARED_PTR( ClientContext );
BB_FWD_DECLARE_SHARED_PTR( IBookSpec );

namespace statistics {
BB_FWD_DECLARE_SHARED_PTR( IDataSampler );
}

namespace signals {

// Forward declaration
BB_FWD_DECLARE_SHARED_PTR( ISignal );
BB_FWD_DECLARE_SHARED_PTR( ISignalSpec );
BB_FWD_DECLARE_SHARED_PTR( ISignalBuilder );
BB_FWD_DECLARE_SHARED_PTR( ISignalSpecList );
BB_FWD_DECLARE_SHARED_PTR( ISignalPrinter );
BB_FWD_DECLARE_SHARED_PTR( TargetInfo );
BB_FWD_DECLARE_SHARED_PTR( TargetWeights );

BB_FWD_DECLARE_SHARED_PTR( Target );

class TargetSpec;

typedef std::vector<double> SignalWeights_t;

class SignalManagerListener
{
public:
    virtual ~SignalManagerListener() { }
    virtual void onCalcSignal(const instrument_t& targinstr, double predpx) { }
};

class SignalManager
    : public IClockListener
{
protected:
    typedef std::vector<TargetInfoPtr> TargetInfoVec_t;

public:
    class TargetInfoEnumerator
    {
    public:
        bool hasNext() const { return m_pit != m_targetInfoVec.end(); }
        const TargetInfoPtr& next()
        {
            const TargetInfoPtr& rval = *m_pit;
            ++m_pit;
            return rval;
        }

        size_t size() const { return m_targetInfoVec.size(); }

    private:
        friend class SignalManager;
        TargetInfoEnumerator( const TargetInfoVec_t& r );
        const TargetInfoVec_t& m_targetInfoVec;
        TargetInfoVec_t::const_iterator m_pit;
    };
    BB_DECLARE_SHARED_PTR( TargetInfoEnumerator );

protected:
    /// Generates a price prediction from the signals and calls listeners based on that.
    class Prediction
    {
    public:
        Prediction(ITimeProviderPtr timeProvider, TargetInfoPtr targetInfo, TargetPtr tgt, int vboseLvl);

        double getMostRecentPredictedPrice() const { return m_currPred; }

        /// Calculates current predicted price signal and calls Listeners.
        /// Remembers this prediction in m_currPred (which is what
        /// getMostRecentPredicedPrice will return).
        void calcSignal();
	void printSignal();
        void onClockNotice(int reason);

        void addSigManListener(SignalManagerListener* sml);
        void removeSigManListener(SignalManagerListener* sml);

        bool checkSignalsOK(uint32_t signal_count_threshold) const ;

    protected:
        ITimeProviderPtr m_timeProvider;
        int m_verboseLevel;

        TargetInfoPtr m_targetInfo;

        /// Current predicted price
        double m_currPred;
        int m_calcSignalCnt;

        typedef std::vector<SignalManagerListener*> SMListeners_t;
        SMListeners_t m_listeners;

        TargetPtr m_spTarget;
    };
    BB_DECLARE_SHARED_PTR(Prediction);

public:
    SignalManager(const ClientContextPtr& clientContext, ISignalBuilderPtr signalBuilder, const std::string& _fbase, bool extendedHours);
    virtual ~SignalManager() { m_clockMonitor->unscheduleClockNotices( this ); }

    ISignalBuilderPtr getSignalBuilder() const { return m_signalBuilder; }

    /// Adds a target stock. All signals must be registered before a target can be added.
    /// weightsFile should be a weights file for this target, or NULL if no weights file should be loaded
    /// This also creates the .desc file and the .keys file.
    TargetInfoPtr addTarget( const instrument_t& target, ISignalSpecListCPtr spec_list
                             , TargetWeightsPtr tgtwts
                             , IBookSpecCPtr refBook
                             , ISignalSpecPtr depvar_spec
                             , int verbose
                             , ptime_duration_t printInterval
                             , ptime_duration_t printOffset );

    /// Returns the TargetInfo for an instrument that's already been added.
    TargetInfoPtr findTargetInfo(const instrument_t &target);
    TargetInfoEnumeratorPtr enumTargetInfos() const;

    /// Creates a meta-data file that describes the file output created by this class.
    /// All targets and all signals must be registered before calling this.
    void createDataDescription() {}

    /// Adds a listener. This must be done before the ATOPEN has been sent.
    void addSigManListener(const instrument_t &target, SignalManagerListener* sml);
    void removeSigManListener(const instrument_t &target, SignalManagerListener* sml);

    /// Returns most recent predicted price as calculated by component signals.
    /// Semantically, this value should be a delta relative to the price of target,
    /// not an absolute price. However, these semantics can not be guaranteed because
    /// the actual value depends on what the component signals produce. As long as component
    /// signals honor these semantics, it will be true.
    double getMostRecentPredictedPrice(const instrument_t &target) const;

    /// ClockListener interface
    virtual void onWakeupCall( const timeval_t& ctv, const timeval_t& swtv, int reason, void* pData );

    /// sets the printing format & interval (must be done before any targets have been added).
    void setPrintParams(std::string printFormat, const ptime_duration_t &printInterval, const ptime_duration_t &printOffset);

    Prediction &findPrediction(const instrument_t &target);
    const Prediction &findPrediction(const instrument_t &target) const;


    // new interface
    TargetPtr createTarget( const instrument_t& tgt_instr, const TargetSpec& spec );
    TargetPtr findTarget( const instrument_t& tgt_instr ) const
    {
        TargetRegistry::const_iterator it = m_targetRegistry.find( tgt_instr );
        if( it != m_targetRegistry.end() )
            return it->second;
        else
            return TargetPtr();
    }

    std::string getFilenameBase() const { return m_fbase; }
    std::string getPrintFormat() const { return m_printFormat; }
    ptime_duration_t getPrintInterval() const { return m_printInterval; }
    ptime_duration_t getPrintOffset() const { return m_printOffset; }

private:
    SignalManager(const SignalManager& sigman); 

protected:
    ISignalPrinterPtr getSignalPrinter();

protected:
    ClientContextPtr m_clientContext;
    ClockMonitorPtr m_clockMonitor;
    ISignalBuilderPtr m_signalBuilder;
    std::string m_fbase;
    int m_vbose;
    statistics::IDataSamplerPtr m_dataSampler;
    ISignalPrinterPtr m_signalPrinter;

    /// For keeping record of stocks that are targets. All symbols that have had addTarget called.
    TargetInfoVec_t m_targets;

    typedef std::vector<PredictionPtr> PredictionVec_t;
    PredictionVec_t m_predictions;
    typedef bbext::hash_map<bb::instrument_t, PredictionPtr> PredictionMap_t;
    PredictionMap_t m_predictionsByInstr;

    std::string m_printFormat;
    ptime_duration_t m_printInterval, m_printOffset;

    typedef bbext::hash_map<instrument_t,TargetPtr> TargetRegistry;
    TargetRegistry m_targetRegistry;
    bool m_extendedHours;
    std::string m_lastDataFilename;
};

BB_DECLARE_SHARED_PTR( SignalManager );

namespace detail
{
size_t instantiate_signals( std::vector<ISignalPtr>* out, ISignalBuilderPtr builder, ISignalSpecListCPtr spec_list );
}

} // namespace signals
} // namespace bb

#endif // BB_SIGNALS_SIGNALMANAGER_H

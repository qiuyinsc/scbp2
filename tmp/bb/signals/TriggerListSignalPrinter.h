#ifndef BB_SIGNALS_TRIGGERLISTSIGNALPRINTER_H
#define BB_SIGNALS_TRIGGERLISTSIGNALPRINTER_H

/* Contents Copyright 2012 Athena Capital Research LLC. All Rights Reserved. */

#include <iosfwd>

#include <boost/shared_ptr.hpp>

#include <bb/core/InstrSource.h>
#include <bb/core/smart_ptr.h>
#include <bb/core/Subscription.h>
#include <bb/core/ptime.h>
#include <bb/signals/SignalsConfigFile.h>
#include <bb/signals/TriggerListDriver.h>
#include <bb/signals/Signal.h>
#include <bb/signals/SignalManager.h>

namespace bb {

class Msg;
BB_FWD_DECLARE_SHARED_PTR( ClientContext );
BB_FWD_DECLARE_SHARED_PTR( ClockMonitor );

namespace signals {

BB_FWD_DECLARE_SHARED_PTR( TargetInfo );
BB_FWD_DECLARE_SHARED_PTR( ISignalBuilder );
BB_FWD_DECLARE_SHARED_PTR( SignalManager );
BB_FWD_DECLARE_SHARED_PTR( IControlSignal );
BB_FWD_DECLARE_SHARED_PTR( TriggerPrintableTargetList );

BB_FWD_DECLARE_SHARED_PTR( TriggerListSignalPrinter );

class TriggerListSignalPrinter
    : TriggerListDriver::Listener
{
public:
    struct Params
    {
        Params();

        bool isOK() const;

        std::vector<InstrSource> trigger_list;
        ptime_duration_t trigger_timeout;
        bool trigger_smart_fire, trigger_on_msg;
        std::string printFormat;
        ptime_duration_t printInterval;
        ptime_duration_t printOffset;
    };

protected:
    //class ContextInfo;
    struct ContextInfo
       {
    timeval_t m_swtv;
    timeval_t m_ctv;
    double m_refPrice;
    //TriggerListSignalPrinter::TargetPtr m_spTarget;
       };

    BB_DECLARE_SHARED_PTR( ContextInfo );

public:
    class Target
    {
    public:
        Target( bb::signals::TargetInfoCPtr target_info
                , const TriggerListSignalPrinter::Params& params
                , const ISignalBuilderPtr& sigbuilder );

        virtual ~Target() {}

        bb::signals::TargetInfoCPtr getTargetInfo() const { return m_spTargetInfo; }
        bb::signals::SignalsListCPtr getSignals() const { return m_spSignals; }
        bb::signals::TriggerPrintableTargetListCPtr getOvarSignals() const { return m_spOvarSignals; }
        // Non-const version intended to be uses to add subscribers to ovargen updates.
        bb::signals::TriggerPrintableTargetListPtr getSubscriptionOvarSignals() const { return m_spOvarSignals; }

        bb::ptime_duration_t getOvarDelay() const { return m_ovarDelay; }
        boost::shared_ptr<std::ostream> getOutFile() const { return m_spOutFile; }
        double getRefPrice() const { return m_spTargetRefPrice->getSignalState()[0]; }

        virtual void startPrinting( boost::shared_ptr<std::ostream> outfile, boost::shared_ptr<std::ostream> ovarOutfile, const std::string& outputBase, uint32_t ymd );

        virtual void stopPrinting()
        {
            m_spOutFile.reset();
            m_spOvarOutFile.reset();
        }

        virtual void print( const bb::timeval_t& ctv, const bb::timeval_t& swtv );
        virtual void printOvar( const bb::timeval_t& ctv, const bb::timeval_t& swtv, ContextInfoPtr ci );

    protected:
        bb::signals::TargetInfoCPtr m_spTargetInfo;
        bb::signals::SignalsListPtr m_spSignals;
        bb::signals::TriggerPrintableTargetListPtr m_spOvarSignals;
        bb::ptime_duration_t m_ovarDelay;

        const TriggerListSignalPrinter::Params m_params;
        bb::signals::ISignalPtr m_spTargetRefPrice;
        boost::shared_ptr<std::ostream> m_spOutFile;
        boost::shared_ptr<std::ostream> m_spOvarOutFile;
    };
    BB_DECLARE_SHARED_PTR( Target );

    class Targets : public std::vector<TargetPtr>{};
    BB_DECLARE_SHARED_PTR( Targets );

public:
    TriggerListSignalPrinter( const ClientContextPtr& cc
        , const bb::signals::ISignalBuilderPtr& signalBuilder
        , const TargetsPtr& targets
        , const Params& params
        , const ConfigFileCPtr& configFile );

    TriggerListSignalPrinter( const ClientContextPtr& cc
        , const SignalManagerCPtr& sm
        , const Params& params
        , const ConfigFileCPtr& configFile );

    TriggerListSignalPrinter( const ClientContextPtr& cc
        , const bb::signals::ISignalBuilderPtr& signalBuilder
        , const TargetsPtr& targets
        , const TriggerListDriverPtr& driver
        , const Params& params );

    TriggerListSignalPrinter( const ClientContextPtr& cc
        , const SignalManagerCPtr& sm
        , const TriggerListDriverPtr& driver
        , const Params& params );

    virtual void startPrinting( boost::shared_ptr<std::ostream> outf, boost::shared_ptr<std::ostream> ovarOutf, const std::string& outputBase, uint32_t ymd );

protected:
    virtual void emitOvarGen( const timeval_t& ctv, const timeval_t& swtv, ContextInfoPtr ci );
    virtual void emitRow( const timeval_t& ctv, const timeval_t& swtv );
    virtual void onFire( const timeval_t& ctv, const timeval_t& swtv );
    virtual void onInterestingMsg( const Msg& m );

    void construct( const ClientContextPtr& cc, const TargetsPtr& targets, const ConfigFileCPtr& configFile );
    void setControlSignal( const IControlSignalPtr& ctrl );

protected:
    Params m_params;
    bb::ClockMonitorPtr m_spClockMonitor;
    ISignalBuilderPtr m_spSignalBuilder;
    TriggerListDriverPtr m_spDriver;
    Subscription m_subDriver;
    Targets m_targets;
    std::vector<bb::Subscription> m_subs;
};

} // signals
} // bb

#endif // BB_SIGNALS_TRIGGERLISTSIGNALPRINTER_H

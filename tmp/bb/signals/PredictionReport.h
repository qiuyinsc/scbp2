#ifndef BB_SIGNALS_PREDICTIONREPORT_H
#define BB_SIGNALS_PREDICTIONREPORT_H

/* Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved. */
#include <iostream>
#include <fstream>

#include <boost/scoped_array.hpp>
#include <boost/range.hpp>
#include <bb/core/Stats.h>
#include <bb/clientcore/PeriodicWakeup.h>
#include <bb/clientcore/IClientTimer.h>
#include <bb/io/XmlLog.h>
#include <bb/signals/ISignalBuilder.h>
#include <bb/signals/TargetInfo.h>
#include <bb/signals/OvarGenerator.h>

/// This stuff monitors the performance of our model and writes an XML file with
/// a summary of how it performed.

namespace bb {
namespace signals {

/// Listener class for a PredictionHistory
class IPredictionListener
{
public:
    typedef boost::iterator_range<std::vector<double>::const_iterator> double_range;
    virtual ~IPredictionListener() {}
    virtual void onStartPrediction(const timeval_t &ctv, int32_t ymdDate) = 0;
    virtual void onEndPrediction(const timeval_t &ctv, int32_t ymdDate) = 0;
    virtual void onPricePrediction(const timeval_t &ctv, double predictedPx, double refpx,
            const double_range &ovars) = 0;
};

/// This class samples our price prediction every second, records it to an
/// internal buffer, and then lines it up with the ovargen output.
class PredictionHistory
    : public PeriodicWakeup
    , public IClockListener
    , public ISignalListener
{
public:
    typedef shared_vector<IPredictionListener*> Listeners_t;
    typedef Subscription ListenerSubPtr;

    PredictionHistory( ClockMonitorPtr clockMonitor, ISignalBuilderPtr signalBuilder, TargetInfoCPtr target, OvarGeneratorPtr ovargen);

    OvarGeneratorPtr getOvarGenerator() { return m_ovarGenerator; }
    TargetWeightsCPtr getWeights() { return m_weights; }

    void subscribeListener(Subscription &outListener, IPredictionListener *p) { m_listenerList.push_back(outListener, p); }

    // PeriodicWakeup impl
    virtual void onPeriodicWakeup(const timeval_t &ctv, const timeval_t &swtv);

    // ISignalListener impl
    virtual void onSignalChanged( const ISignal& signal );

    // IClockListener impl
    virtual void onWakeupCall( const timeval_t& ctv, const timeval_t& swtv, int reason, void* pData );

    void addPause( IClientTimerPtr client_timer, const bb::timeval_t& start_tv,  const bb::timeval_t& end_tv);

protected:
    std::list<std::pair<timeval_t, double> > m_predictionHistory;
    int m_vboseLvl;
    size_t m_historySize;

    instrument_t m_instr;
    SessionParams m_sessionParams;
    OvarGeneratorPtr m_ovarGenerator;
    TargetWeightsCPtr m_weights;
    std::vector<ISignalPtr> m_sampledSignals;
    Listeners_t m_listenerList;
    ptime_duration_t m_printInterval, m_printOffset;

    std::vector<bb::Subscription> m_subs;

};
BB_DECLARE_SHARED_PTR(PredictionHistory);


class PredictionReport : public IPredictionListener
{
public:
    PredictionReport(PredictionHistoryPtr p, std::string obase, bool printSamples);
    virtual ~PredictionReport();

    // IPredictionListener impl
    virtual void onStartPrediction(const timeval_t &tv, int32_t ymdDate);
    virtual void onEndPrediction(const timeval_t &tv, int32_t ymdDate);
    virtual void onPricePrediction(const timeval_t &tv, double predictedPx, double refpx,
            const double_range &ovars);

protected:
    inline size_t getStateSize() const { return m_prediction->getOvarGenerator()->getStateSize()-1; }
    inline const std::string &getStateName(size_t i) const
    {
        BB_ASSERT(i < getStateSize());
        return m_prediction->getOvarGenerator()->getStateNames()[i+1];
    }

    void finishHour(const timeval_t &tv);
    void finishReport(const timeval_t &tv);

    PredictionHistoryPtr m_prediction;

    Subscription m_listenerSub;

    std::string m_outputFileBase;
    boost::scoped_array<RSquaredCalculator> m_dayRSquared; // for the whole day
    boost::scoped_array<RSquaredCalculator> m_hourRSquared; // for the last hour

    bool m_logOpen, m_hourOpen, m_printSamples;
    timeval_t m_hourEndTime, m_lastTime;
    double m_lastRefPrice;
    std::ofstream m_outFile;

    XmlLog m_outLog;
    XmlLog::Attribute m_aNumSamples, m_aLastRefPx, m_aIndex, m_aName, m_aYMean,
        m_aYVariance, m_aRSquared, m_aTargetColumn, m_aValue, m_aPredictedPx, m_aRefPx;
    XmlLog::Element m_eEndHour, m_eOvar, m_eEndReport, m_eModelReport,
        m_eModelInfo, m_eStartReport, m_eStartHour, m_eSample;
};

BB_DECLARE_SHARED_PTR(PredictionReport);

} // namespace signals
} // namespace bb

#endif // BB_SIGNALS_PREDICTIONREPORT_H

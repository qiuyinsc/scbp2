#ifndef BB_SIGNALS_SIGNALBUILDER_H
#define BB_SIGNALS_SIGNALBUILDER_H

/* Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved. */


#include <map>

#include <bb/clientcore/PriceProviderBuilder.h>
#include <bb/clientcore/BookBuilder.h>
#include <bb/clientcore/TickFactory.h>
#include <bb/clientcore/ClientContext.h>
#include <bb/statistics/StatisticBuilder.h>
#include <bb/signals/ISignalBuilder.h>
#include <bb/signals/SignalSpec.h>
#include <bb/signals/Signal.h>
#include <bb/signals/CachedSignal.h>
#include <bb/signals/RemoteSignal.h>

namespace bb {
namespace signals {

/// SignalBuilder takes a ISignalSpec structure (i.e. a description of a
/// signal that needs to be built) and builds it.
class SignalBuilder : public ISignalBuilder
{
public:
    SignalBuilder(ClientContextPtr clientContext,
            bb::IBookBuilderPtr bookBuilder,
            int verboseLevel,
            bool runLive);

    // ISignalBuilder implementation
    virtual bb::IBookBuilderPtr getBookBuilder() { return m_bookBuilder; }
    virtual ITickProviderFactoryPtr getSourceTickFactory() const { return m_pxpBuilder->getSourceTickFactory(); }
    virtual statistics::IStatisticBuilderPtr getStatisticBuilder() const { return m_dataSamplerBuilder; }
    virtual ISignalPtr buildSignal(ISignalSpecCPtr spec);
    virtual void setVerboseLevel(int vbose_lvl) { m_verboseLevel = vbose_lvl; }
    virtual PriceProviderBuilderPtr getPxPBuilder() const { return m_pxpBuilder; }

    // Functions used by Spec subclasses in building their concrete PriceProviders
    virtual bool getRunLive() const { return m_runLive; }
    virtual int getVerboseLevel() const { return m_verboseLevel; }
    virtual ClientContextPtr getClientContext() const { return m_clientContext.lock(); }
    virtual ClockMonitorPtr getClockMonitor() const { return getClientContext()->getClockMonitor(); }
    virtual IMStreamManagerPtr getMStreamManager() const { return getClientContext()->getMStreamManager(); }
    virtual EventDistributorPtr getEventDistributor() const { return getClientContext()->getEventDistributor(); }

    sigserver::ClientPtr getSigServerConnection(const std::string& host, uint16_t port);
    CacheFilePtr getCachedSignalFile(std::string filename);
    RemoteMessageMonitorPtr getSigSourceMonitor(source_t sigSrc, Uuid sourceID);

protected:
    ClientContextWeakPtr m_clientContext;
    IBookBuilderPtr m_bookBuilder;
    PriceProviderBuilderPtr m_pxpBuilder;
    statistics::IStatisticBuilderPtr m_dataSamplerBuilder;

    int m_verboseLevel;
    bool m_runLive;

    typedef std::map<Uuid, RemoteMessageMonitorPtr> SignalSourceMonitors_t;
    SignalSourceMonitors_t m_signalSourceMonitors;

    // a list of the signals that already exist
    typedef bbext::hash_map<ISignalSpecCPtr, ISignalWeakPtr, SignalSpecHasher, SignalSpecComparator> Signals_t;
    Signals_t m_alreadyBuiltSignals;

    typedef std::map<std::string, CacheFilePtr> SignalCacheMap_t;
    SignalCacheMap_t m_cacheFiles;

    typedef std::map<std::string, sigserver::ClientPtr> ConnectionMap_t;
    ConnectionMap_t m_sigServerConnections;

    SpamFilter m_harvestedSignalSpam;
};
BB_DECLARE_SHARED_PTR(SignalBuilder);

} // namespace signals
} // namespace bb

#endif // BB_SIGNALS_SIGNALBUILDER_H

#ifndef BB_SIGNALS_SIGNALSLIBMULTICONTROLSIGNAL_H
#define BB_SIGNALS_SIGNALSLIBMULTICONTROLSIGNAL_H

/* Contents Copyright 2013 Athena Capital Research LLC. All Rights Reserved. */

#include <bb/core/Scripting.h>
#include <bb/core/LuabindScripting.h>
#include <bb/signals/IControlSignal.h>

namespace bb {

BB_FWD_DECLARE_SHARED_PTR( ClientContext );

namespace signals {

BB_FWD_DECLARE_SHARED_PTR( ISignalBuilder );
BB_FWD_DECLARE_SHARED_PTR( ISignalSpec );
BB_FWD_DECLARE_SHARED_PTR( ISignal );

class SignalsLibMultiControlSignal : public IControlSignal
{
public:
    class Spec : public IControlSignal::Spec
    {
    public:
        BB_DECLARE_SCRIPTING();

        Spec();

        virtual IControlSignalPtr create( const ISignalBuilderPtr& signalBuilder );

        std::vector<ISignalSpecPtr> signals;
        std::vector<double> thresholds;
    };

public:
    SignalsLibMultiControlSignal( const ISignalBuilderPtr& signalBuilder, const Spec& params = Spec() );

    virtual int32_t value() const;
    virtual bool isValid() const;

private:
    const Spec m_params;
    ISignalBuilderPtr m_spSignalBuilder;
    std::vector<ISignalPtr> m_vecSignals;
};

} // namespace signals
} // namespace bb

#endif // BB_SIGNALS_SIGNALSLIBMULTICONTROLSIGNAL_H

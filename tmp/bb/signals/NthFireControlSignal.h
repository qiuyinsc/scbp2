#ifndef BB_SIGNALS_NTHFIRECONTROLSIGNAL_H
#define BB_SIGNALS_NTHFIRECONTROLSIGNAL_H

/* Contents Copyright 2013 Athena Capital Research LLC. All Rights Reserved. */

#include <bb/core/bbint.h>
#include <bb/core/smart_ptr.h>
#include <bb/core/Scripting.h>
#include <bb/signals/IControlSignal.h>

namespace bb {

BB_FWD_DECLARE_SHARED_PTR( ClientContext );

namespace signals {

BB_FWD_DECLARE_SHARED_PTR( ISignalBuilder );

class NthFireControlSignal : public IControlSignal
{
public:
    class Spec : public IControlSignal::Spec
    {
    public:
        BB_DECLARE_SCRIPTING();

        Spec();
        virtual ~Spec() {}
        virtual IControlSignalPtr create( const ISignalBuilderPtr& signalBuilder );

        uint32_t m_nthFire, m_offset;
    };
    BB_DECLARE_SHARED_PTR( Spec );

public:
    NthFireControlSignal( const ISignalBuilderPtr& signalBuilder, const Spec& params = Spec() );

    virtual ~NthFireControlSignal() {}

    virtual int32_t value() const;
    virtual bool isValid() const { return true; }

protected:
    const uint32_t m_nthFire, m_offset;
    mutable uint32_t m_count;
};

BB_DECLARE_SHARED_PTR( NthFireControlSignal );

} // namespace signals
} // namespace bb

#endif // BB_SIGNALS_NTHFIRECONTROLSIGNAL_H

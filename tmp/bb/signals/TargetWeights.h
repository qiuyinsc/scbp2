#ifndef BB_SIGNALS_TARGETWEIGHTS_H
#define BB_SIGNALS_TARGETWEIGHTS_H

/* Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved. */


#include <luabind/object.hpp>
#include <bb/core/bbassert.h>
#include <bb/clientcore/BookSpec.h>
#include <bb/signals/SignalSpec.h>
#include <bb/signals/Signal.h>
#include <bb/signals/Weights.h>
#include <bb/signals/TargetSpec.h>

namespace bb {
namespace signals {

BB_FWD_DECLARE_SHARED_PTR(TargetWeights);
BB_FWD_DECLARE_SHARED_PTR(LuaTargetWeights);

typedef std::vector<double> SignalWeights_t; // a vector of weights for a single signal

/// TargetWeights encapsulates a set of weights for a given target product.
class TargetWeights
{
public:
    static TargetWeightsPtr readWeights( std::istream *wf
        , const std::vector<ISignalPtr>& signals
        , ISignalSpecListCPtr specs
        , int verbose );

    /// This copies in the set of weights. The vector "weights" will be destroyed.
    TargetWeights(double constantWeight,
            ISignalSpecListCPtr specs,
            std::vector<SignalWeights_t> const & weights);
    virtual ~TargetWeights() {} // needed for dynamic_cast only

    static double getWeightedValue(const SignalWeights_t &weights, ISignalPtr const & sig);

    /// checks to make sure our internal weights list syncs up with the given signals
    /// (that the descriptions and weights sizes match).
    void checkSignalList( const std::vector<ISignalPtr>& signals ) const;
    void checkSignalList( ISignalSpecListCPtr specs) const;

    /// calculates the total price prediction against the given signals.
    double calcSignal(const std::vector<ISignalPtr> &signals) const;

    /// print out each signal that contributes to the predicted price
    void printSignal(const std::vector<ISignalPtr> &signals) const;

    WeightsCPtr getWeights() const { return m_spWeights; }

protected:
    TargetWeights();

    WeightsPtr m_spWeights;
};
BB_DECLARE_SHARED_PTR(TargetWeights);


/// A new-style weights file is lua format. It has extra info (not just
/// the weights), so that we can monitor more info about the performance of
/// the models.
class LuaTargetWeights : public TargetWeights
{
public:
    LuaTargetWeights(LuaStatePtr config);
    LuaTargetWeights( LuaStatePtr config, const luabind::object& weight_info );

    const boost::optional<std::string> getTargetColumn() const { return m_targetColumn; }
    const luabind::object& getModelInfo() const { return m_modelInfo; }

protected:
    void init( const luabind::object& weight_info );

    LuaStatePtr m_config;
    boost::optional<std::string> m_targetColumn;
    luabind::object m_modelInfo;
};
BB_DECLARE_SHARED_PTR(LuaTargetWeights);

/// A newer-new-style weights file in lua format. It's easier to write up by hand
/// and is designed to work with models built in matlab where we can copy over
/// a list of weights and just run it.
class LuaTargetWeights2 : public TargetWeights
{
public:
    LuaTargetWeights2( LuaStatePtr config, const luabind::object& model );
};

// model = {
//     constantWeight = 3.1415,
//     weights = {
//         foo = { 3.23, 2.34, 4.67, 8.67 },
//         bar = 43,
//         baz = { 3.23, 2.34, 4.67, 8.67, 5.67 },
//     },
// },
class LuaTargetWeights3 : public TargetWeights
{
public:
    LuaTargetWeights3( const luabind::object& model );
};


namespace detail {
    /// parses the string "1 1.0" or throws
    std::pair<int, double> parseOldWeightsLine(std::string v);
    int readWeightsForSignal(bb::signals::SignalWeights_t &weights, bb::signals::ISignalCPtr signal, std::istream* wf, int cnt, int verbose);
}


} // signals
} // bb

#endif // BB_SIGNALS_TARGETWEIGHTS_H

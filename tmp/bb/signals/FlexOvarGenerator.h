#ifndef BB_SIGNALS_FLEXOVARGENERATOR_H
#define BB_SIGNALS_FLEXOVARGENERATOR_H

/* Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved. */

#include <boost/function.hpp>

#include <bb/core/smart_ptr.h>
#include <bb/clientcore/IBook.h>
#include <bb/clientcore/ClockMonitor.h>
#include <bb/clientcore/PeriodicWakeup.h>

#include <deque>
#include <bb/signals/Signal.h>
#include <bb/signals/SignalSpec.h>
#include <bb/signals/ITriggerPrintableTarget.h>

namespace bb {
namespace signals {

class FlexOvarGenerator
    : public ITriggerPrintableTarget
    , protected PeriodicWakeup
    , protected IClockListener
{
public:
    // Class representing snapshots/samples of the target prices.
    // Generators take these and output some value = F(snapshots)
    class DataSample {
    public:
        DataSample(timeval_t otv, timeval_t actual_tv, double refpx, bool slice_valid) :
            m_otv(otv),
            m_actual_tv(actual_tv),
            m_refpx(refpx),
            m_slice_valid(slice_valid)
            { }

        ~DataSample() { }

        timeval_t m_otv;
        timeval_t m_actual_tv;
        double m_refpx;
        bool m_slice_valid;
    };

    class IGenerator {
    public :
        virtual ~IGenerator() {}

        virtual void update(const DataSample& update) = 0;

        virtual bool valueChanged() = 0;

        virtual bool isReady() = 0;

        virtual double get() = 0;
        virtual double getRefpx() = 0;
        virtual timeval_t getScheduledTv() = 0;
        virtual timeval_t getActualTv() = 0;

        virtual const std::string& getDescription() = 0;

        virtual void reset() = 0;

        virtual size_t getNumSamplesMax() const = 0;

        virtual ptime_duration_t getTvMax() const = 0;

        virtual void initPrinting(const ClockMonitor* cm) = 0;
    };
    BB_DECLARE_SHARED_PTR( IGenerator );

    BB_FWD_DECLARE_SHARED_PTR(IGeneratorSpec);
    class IGeneratorSpec {
    public:
        BB_DECLARE_SCRIPTING();

        virtual ~IGeneratorSpec() {}

        virtual IGeneratorPtr build() const {  return IGeneratorPtr(); }

        /// deep copy this IGeneratorSpec
        virtual IGeneratorSpec *clone() const = 0;
        inline IGeneratorSpec *clone(const IGeneratorSpec *e) { return e ? e->clone() : NULL; }
        inline IGeneratorSpec *clone(IGeneratorSpecCPtr e) { return clone(e.get()); }

        virtual void hashCombine(size_t &result) const = 0;

        virtual bool compare(const IGeneratorSpec *other) const = 0;


    };
    //BB_DECLARE_SHARED_PTR( IGeneratorSpec );

    // Pretty much OvarGenertor with vector of mapping functions instead of decay rates.
    // Should set it up to generate identical output as old version
    FlexOvarGenerator(const instrument_t &instr, const std::string &mydesc, const IPriceProviderPtr& pp, const ClockMonitorPtr& cm,
                  const ptime_duration_t &sampleInterval, const ptime_duration_t &sampleOffset, int vbose);
    virtual ~FlexOvarGenerator() {}

    void reset();

    virtual bool isOK() const { return m_lastIsOK; }
    virtual const instrument_t& getInstrument() const { return m_instr; }
    virtual const std::string& getDesc() const { return m_desc; }
    virtual const std::vector<double, std::allocator<double> >& getSignalState() const { return m_lastState; }

    // ITriggerListPrintable 
    virtual bool firstVarAsPrintRefPrice() const { return true; }
    virtual double getPrintRefPrice() const { return m_lastState[0]; }
    virtual ptime_duration_t getPrintDelay() const { return getTvMax(); }

    virtual size_t getStateSize() const { return m_stateNames.size(); }
    virtual const std::vector<std::string>& getStateNames() const { return m_stateNames; }
    virtual bb::timeval_t getLastChangeTv() const { return m_lastChangeTv; }
    virtual boost::optional<timeval_t> getLastScheduledChangeTv() const { return m_lastScheduledChangeTv; }

    size_t getNumSamplesMax() const;
    bb::ptime_duration_t getTvMax() const;

    void addGenerator(IGeneratorPtr generator);

protected:
    void update( timeval_t scheduled_wtv, timeval_t current_tv );

    // PeriodicWakeup implementation
    virtual void onPeriodicWakeup(const timeval_t &ctv, const timeval_t &swtv);
    // IClockListener implementation
    void onWakeupCall( const timeval_t& ctv, const timeval_t& swtv, int reason, void* pData );

protected:
    std::string m_desc;
    instrument_t m_instr;
    IPriceProviderPtr m_pp;
    Subscription m_pp_sub;
    const ptime_duration_t m_sampleInterval, m_sampleOffset;
    int m_vbose;

    std::vector<IGeneratorPtr> m_generators;

    bool m_lastIsOK;
    timeval_t m_lastChangeTv, m_lastScheduledChangeTv;
    std::vector<std::string> m_stateNames;
    std::vector<double> m_lastState;
};
BB_DECLARE_SHARED_PTR( FlexOvarGenerator );

std::size_t hash_value(const FlexOvarGenerator::IGeneratorSpec &a);
bool operator==(const FlexOvarGenerator::IGeneratorSpec &a, const FlexOvarGenerator::IGeneratorSpec &b);
inline bool operator!=(const FlexOvarGenerator::IGeneratorSpec &a, const FlexOvarGenerator::IGeneratorSpec &b) { return !(a == b); }


/// SignalSpec corresponding to FlexOvarGenerator
class FlexOvarGeneratorSignalSpec : public ISignalSpec
{
public:
    BB_DECLARE_SCRIPTING();

    FlexOvarGeneratorSignalSpec() {}
    FlexOvarGeneratorSignalSpec(const FlexOvarGeneratorSignalSpec &e);

    virtual instrument_t getInstrument() const { return m_pp->getInstrument(); }
    virtual std::string getDescription() const { return m_description; }

    virtual ISignalPtr build(SignalBuilder *builder) const;
    virtual void checkValid() const;
    virtual void hashCombine(size_t &result) const;
    virtual FlexOvarGeneratorSignalSpec *clone() const;
    virtual bool compare(const ISignalSpec *other) const;
    virtual void print(std::ostream &o, const LuaPrintSettings &ps) const;
    virtual void getDataRequirements(IDataRequirements *rqs) const;
    virtual bool isDependentVariable() const { return true; }

    std::string m_description;
    IPxProviderSpecPtr m_pp;
    ptime_duration_t m_sampleInterval, m_sampleOffset;
    std::vector<FlexOvarGenerator::IGeneratorSpecPtr> m_generators;
};
BB_DECLARE_SHARED_PTR(FlexOvarGeneratorSignalSpec);

} // namespace signals
} // namespace bb

#endif // BB_SIGNALS_FLEXOVARGENERATOR_H

#ifndef BB_SIGNALS_SIGBOOKDEPTH_H
#define BB_SIGNALS_SIGBOOKDEPTH_H

/* Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved. */


#include <bb/signals/Signal.h>
#include <bb/signals/SignalSpec.h>

namespace bb {
namespace signals {

/// SigBookDepth measures what's on the book up to
/// a given distance from the best market.
/// i.e. with offsetMode == DOLLARS and offsets = { 0.10 }, 
/// SigBookDepth[ask] = sum( order.px * order.sz for all ask orders s.t. px < best ask+0.10 )
/// SigBookDepth[bid] = sum( order.px * order.sz for all bid orders s.t. px > best bid-0.10 )
///
/// Implementation is very naive & slow right now.
class SigBookDepth : public SignalStateImpl, public IBookListener
{
public:
    enum OffsetMode { DOLLARS, BASIS_POINTS };
    SigBookDepth(const std::string &desc, ITimeProviderPtr tp, IBookPtr book, const std::vector<double> &offsets, OffsetMode offsetMode);
    virtual ~SigBookDepth();

    virtual void onBookChanged( const IBook* pBook, const Msg* pMsg,
                                int32_t bidLevelChanged, int32_t askLevelChanged );
    virtual void onBookFlushed( const IBook* pBook, const Msg* pMsg );

protected:
    virtual void recomputeState() const;

    ITimeProviderPtr m_timeProvider;
    IBookPtr m_book;
    std::vector<double> m_offsets;
    OffsetMode m_offsetMode;
};
BB_DECLARE_SHARED_PTR(SigBookDepth);

/// SigBookDepthSpec
class SigBookDepthSpec : public ISignalSpec
{
public:
    BB_DECLARE_SCRIPTING();

    SigBookDepthSpec() : m_offsetMode(SigBookDepth::DOLLARS) {}
    SigBookDepthSpec(const SigBookDepthSpec &e);

    virtual instrument_t getInstrument() const { return m_book->getInstrument(); }
    virtual std::string getDescription() const { return m_description; }

    virtual ISignalPtr build(SignalBuilder *builder) const;
    virtual void checkValid() const;
    virtual void hashCombine(size_t &result) const;
    virtual bool compare(const ISignalSpec *other) const;
    virtual void print(std::ostream &o, const LuaPrintSettings &ps) const;
    virtual void getDataRequirements(IDataRequirements *rqs) const;
    virtual SigBookDepthSpec *clone() const;

    std::string m_description;
    std::vector<double> m_offsets;
    SigBookDepth::OffsetMode m_offsetMode;
    IBookSpecPtr m_book;
};
BB_DECLARE_SHARED_PTR(SigBookDepthSpec);

} // namespace signals
} // namespace bb

#endif // BB_SIGNALS_SIGBOOKDEPTH_H

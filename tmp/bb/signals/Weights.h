#ifndef BB_SIGNALS_WEIGHTS_H
#define BB_SIGNALS_WEIGHTS_H

/* Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved. */


#include <vector>
#include <string>
#include <bb/core/smart_ptr.h>

namespace bb {
namespace signals {

BB_FWD_DECLARE_SHARED_PTR( ISignal );
BB_FWD_DECLARE_SHARED_PTR( ISignalSpecList );

class Weights
{
public:
    typedef std::vector<double> SignalWeights; // a vector of weights for a single signal

public:
    Weights() : m_constantWeight( 0.0 ) {}
    Weights( const std::vector<std::string>& descs, double constant_wt, const std::vector<SignalWeights> vals )
        : m_signalDescs( descs )
        , m_signalWeights( vals )
        , m_constantWeight( constant_wt )
    {
        BB_ASSERT( descs.size() == vals.size() );
    }

    void setConstantWeight( double constant_wt ) { m_constantWeight = constant_wt; }
    void insert( const std::string& desc, const SignalWeights& wts )
    {
        m_signalDescs.push_back( desc );
        m_signalWeights.push_back( wts );
    }

    const std::vector<std::string>& getDesc() const { return m_signalDescs; }
    const std::vector<SignalWeights>& getValues() const { return m_signalWeights; }
    double getConstantWeight() const { return m_constantWeight; }

    void checkSignalList( const std::vector<ISignalPtr>& signals ) const;
    void checkSignalList( ISignalSpecListCPtr signals ) const;

protected:
    std::vector<std::string> m_signalDescs;
    std::vector<SignalWeights> m_signalWeights;
    double m_constantWeight;
};

BB_DECLARE_SHARED_PTR( Weights );

} // namespace signals
} // namespace bb

#endif // BB_SIGNALS_WEIGHTS_H

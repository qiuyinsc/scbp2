#ifndef BB_SIGNALS_SIGFROMFILE_H
#define BB_SIGNALS_SIGFROMFILE_H

/* Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved. */

#include <string>

// #include <bb/core/path_utils.h>
#include <bb/core/Subscription.h>
#include <bb/core/timeval.h>
#include <bb/clientcore/ClockMonitor.h>
#include <bb/clientcore/IClientTimer.h>
#include <bb/signals/Signal.h>
#include <bb/signals/SignalSpec.h>

#include <boost/filesystem/path.hpp>
#include <boost/tokenizer.hpp>
#include <boost/lexical_cast.hpp>

namespace bb {
namespace signals {

template <class T>
class SigFromFile;

class ClockMonitorScheduleProvider
{
public:
    ClockMonitorScheduleProvider( ClockMonitorPtr clockMonitor );
    void schedule( Subscription& sub, SigFromFile<ClockMonitorScheduleProvider>& sig, const bb::timeval_t& tv );

    typedef ClockMonitorPtr SchedulerPtr;

private:
    ClockMonitorPtr m_spClockMonitor;
};

class ClientTimerScheduleProvider
{
public:
    ClientTimerScheduleProvider( IClientTimerPtr clientTimer );
    void schedule( Subscription& sub, SigFromFile<ClientTimerScheduleProvider>& sig, const bb::timeval_t& tv );

    typedef IClientTimerPtr SchedulerPtr;

private:
    IClientTimerPtr m_spClientTimer;
};

template <class T>
class SigFromFile
    : public SignalStateImpl
{
public:
    SigFromFile( const instrument_t& instr, const std::string &desc, IPriceProviderPtr spRefpp, ClockMonitorPtr clockMonitor, typename T::SchedulerPtr scheduler, std::string filename, std::string separator = ",", int timestampIndex = 0, int signalIndex = 1 );
    virtual ~SigFromFile();

    virtual const std::vector<double>& getSignalState() const;

    void update();
    void update( const bb::timeval_t& swtv );

protected:
    void resetVars() const;
    void updateVars() const;
    virtual void recomputeState() const;

    // IPriceProvider Listener
    void onPriceChanged( const IPriceProvider& pp );

private:
    void loadFile();
    void initializeTracking();
    void scheduleUpdate( const bb::timeval_t& swtv );

    class SignalUpdate
    {
    public:
        SignalUpdate( bb::timeval_t tv, double signal )
            : m_tv( tv )
            , m_signal( signal )
        {}

        bb::timeval_t getTimeval() const { return m_tv; }
        double getSignal() const { return m_signal; }

    private:
        timeval_t m_tv;
        double m_signal;
    };

protected:
    typedef std::vector<SignalUpdate> SignalUpdateList;

    IPriceProviderPtr m_spRefpp;
    ClockMonitorPtr m_spClockMonitor;
    T m_scheduleProvider;
    boost::filesystem::path m_filename;
    std::string m_separator;
    int m_timestampIndex;
    int m_signalIndex;
    std::vector<bb::Subscription> m_subs;
    SignalUpdateList m_timeSlices;
    typename SignalUpdateList::iterator m_step;
};

template <class T>
SigFromFile<T>::SigFromFile( const instrument_t& instr, const std::string &desc, IPriceProviderPtr spRefpp, ClockMonitorPtr clockMonitor, typename T::SchedulerPtr scheduler, std::string filename, std::string separator, int timestampIndex, int signalIndex )
    : SignalStateImpl( instr, desc )
    , m_spRefpp( spRefpp )
    , m_spClockMonitor( clockMonitor )
    , m_scheduleProvider( scheduler )
    , m_filename( filename )
    , m_separator( separator )
    , m_timestampIndex( timestampIndex )
    , m_signalIndex( signalIndex )
{
    BB_THROW_EXASSERT_SSX( m_spRefpp, "SigFromFile: was passed a NULL refpp" );
    BB_THROW_EXASSERT_SSX( m_timestampIndex >= 0, "SigFromFile: timestamp index must be non-negative" );
    BB_THROW_EXASSERT_SSX( m_signalIndex >= 0, "SigFromFile: signal index must be non-negative" );

    Subscription sub;

    m_spRefpp->addPriceListener( sub, bind( &SigFromFile<T>::onPriceChanged, this, _1 ) );

    m_subs.push_back( sub );

    loadFile();

    if( isOK() )
    {
        initializeTracking();
    }
}

template <class T>
SigFromFile<T>::~SigFromFile()
{}

template <class T>
const std::vector<double> &SigFromFile<T>::getSignalState() const
{
    return SignalStateImpl::getSignalState();
}

template <class T>
void SigFromFile<T>::recomputeState() const {}

template <class T>
void SigFromFile<T>::onPriceChanged( const IPriceProvider& pp )
{}

template <class T>
void SigFromFile<T>::loadFile()
{
    std::string filename = m_spClockMonitor->getTime().strftime( m_filename.string() );

    std::ifstream file( filename.c_str() );

    BB_THROW_EXASSERT_SSX( file, "SigFromFile::loadFile() could not open file " << filename );

    bool indexChecked = false;

    std::string buf;

    while( std::getline( file, buf ) )
    {
        try
        {
            std::vector<std::string> tokens;
            boost::tokenizer< boost::char_separator<char> > tok( buf, boost::char_separator<char>( m_separator.c_str() ) );
            for( boost::tokenizer< boost::char_separator<char> >::iterator it = tok.begin() ; it != tok.end() ; ++it )
            {
                tokens.push_back( *it );
            }

            // grab first row and verify that requested indices are within the number of columns
            if( !indexChecked )
            {
                BB_THROW_EXASSERT_SSX( ( unsigned ) m_timestampIndex < tokens.size(),
                                       "SigFromFile::loadFile() timestamp index exceeds number of columns in file" );
                BB_THROW_EXASSERT_SSX( ( unsigned ) m_signalIndex < tokens.size(),
                                       "SigFromFile::loadFile() signal index exceeds number of columns in file" );

                indexChecked = true;
            }

            bb::timeval_t ts = bb::timeval_t::make_time( tokens[m_timestampIndex] );

            double signal = boost::lexical_cast<double>( tokens[m_signalIndex] );

            m_timeSlices.push_back( SignalUpdate( ts, signal ) );
        }
        catch( ... )
        {
            std::cerr << "SigFromFile::loadFile() failed to parse line: " << buf << std::endl;
            throw;
        }
    }

    m_isOK = true;
}

template <class T>
void SigFromFile<T>::initializeTracking()
{
    allocState( "signal" );

    m_step = m_timeSlices.begin();

    scheduleUpdate( m_step->getTimeval() );
}

template <class T>
void SigFromFile<T>::update()
{
    m_state[0] = m_step->getSignal();

    notifySignalListeners( m_step->getTimeval() );

    if( m_step != m_timeSlices.end() - 1 )
    {
        m_step = boost::next( m_step );

        scheduleUpdate( m_step->getTimeval() );
    }
}

template <class T>
void SigFromFile<T>::update( const timeval_t& swtv )
{
    m_state[0] = m_step->getSignal();

    notifySignalListeners( m_step->getTimeval() );

    if( m_step != m_timeSlices.end() - 1 )
    {
        m_step = boost::next( m_step );

        scheduleUpdate( m_step->getTimeval() );
    }
}

template <class T>
void SigFromFile<T>::scheduleUpdate( const timeval_t& swtv )
{
    bb::Subscription sub;

    m_scheduleProvider.schedule( sub, *this, swtv );

    m_subs.push_back( sub );
}

/// SignalSpec for SigFromFile
class SigFromFileSpec : public SignalSpec
{
public:
    BB_DECLARE_SCRIPTING();

    SigFromFileSpec();
    SigFromFileSpec( const SigFromFileSpec &e );

    virtual instrument_t getInstrument() const { return m_refPxP->getInstrument(); }
    virtual ISignalPtr build( SignalBuilder *builder ) const;
    virtual void checkValid() const;
    virtual void hashCombine( size_t &result ) const;
    virtual bool compare( const ISignalSpec *other ) const;
    virtual void print( std::ostream &o, const LuaPrintSettings &ps ) const;
    virtual void getDataRequirements( IDataRequirements *rqs ) const;
    virtual SigFromFileSpec *clone() const;

    std::string m_filename;
    std::string m_separator;
    int m_timestampIndex;
    int m_signalIndex;
    bool m_enforceExactTime;
};
BB_DECLARE_SHARED_PTR( SigFromFileSpec );

} // namespace signals
} // namespace bb

#endif // BB_SIGNALS_SIGFROMFILE_H

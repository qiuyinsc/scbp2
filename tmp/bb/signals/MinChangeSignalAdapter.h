#ifndef BB_SIGNALS_MINCHANGESIGNALADAPTER_H
#define BB_SIGNALS_MINCHANGESIGNALADAPTER_H

/* Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved. */


#include <bb/signals/Signal.h>
#include <bb/signals/SignalSpec.h>


namespace bb {
namespace signals {


/// This signal adapts an input Signal, only notifying when
/// the change in a signal component is greater than some minimum amount.
///
/// You can chose the threshold as a single value that is checked against every
/// signal state component.  Otherwise, you can set a vector of thresholds where
/// threshold[i] is compared against state[i].
///
/// The state names are the input state name, with 'mc'+i appended.
class MinChangeSignalAdapter
    : public SignalStateImpl
    , private ISignalListener
{
public:
    MinChangeSignalAdapter(
        const std::string& desc,
        ISignalPtr spInputSignal, double threshold );
    MinChangeSignalAdapter(
        const std::string& desc,
        ISignalPtr spInputSignal, const std::vector<double>& vThresholds );

    /// Returns the thresholds of this signal.
    const std::vector<double>& getThresholds() const { return m_thresholds; }

private:
    /// Invoked when a signal changes value.
    /// The reference to signal and its state vector are only valid during this method call.
    /// You must cache any relevant values.
    virtual void onSignalChanged( const ISignal& signal );
    virtual void recomputeState() const {}

private:
    ISignalPtr          m_spInputSignal;
    std::vector<double> m_thresholds;
};
BB_DECLARE_SHARED_PTR( MinChangeSignalAdapter );

/// MinChangeSignalSpec
/// uses m_threshold if m_vThresholds.empty()
class MinChangeSignalSpec : public ISignalSpec
{
public:
    BB_DECLARE_SCRIPTING();

    MinChangeSignalSpec() {}
    MinChangeSignalSpec(const MinChangeSignalSpec &e);

    virtual ISignalPtr build(SignalBuilder *builder) const;
    virtual void checkValid() const;
    virtual void hashCombine(size_t &result) const;
    virtual bool compare(const ISignalSpec *other) const;
    virtual void print(std::ostream &o, const LuaPrintSettings &ps) const;
    virtual void getDataRequirements(IDataRequirements *rqs) const;
    virtual MinChangeSignalSpec *clone() const;

    virtual instrument_t getInstrument() const { return m_inputSignal->getInstrument(); }
    virtual std::string getDescription() const { return m_description; }

    std::string         m_description;
    ISignalSpecCPtr     m_inputSignal;

    double              m_threshold;    /// uses m_threshold if m_vThresholds.empty()
    std::vector<double> m_vThresholds;
};
BB_DECLARE_SHARED_PTR(MinChangeSignalSpec);


} // namespace signals
} // namespace bb


#endif // BB_SIGNALS_MINCHANGESIGNALADAPTER_H

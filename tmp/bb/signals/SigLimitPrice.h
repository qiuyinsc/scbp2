#ifndef BB_SIGNALS_SIGLIMITPRICE_H
#define BB_SIGNALS_SIGLIMITPRICE_H

/* Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved. */


#include <bb/clientcore/IBook.h>
#include <bb/clientcore/BookPriceProvider.h>
#include <bb/signals/Signal.h>
#include <bb/signals/SignalSpec.h>

namespace bb {
namespace signals {

/// A Signal that computes the distance between the nearest book level and a limit price.
/// If no limit price or book level exists, then the signal is not OK
class SigLimitPrice
    : public SignalStateImpl
    , private IBookListener
{
public:
    SigLimitPrice( const instrument_t& instr, const std::string &desc, IBookPtr spBook, const uint32_t numLevels, bool inPercent );

    virtual ~SigLimitPrice();

protected:
    PriceSize getDeepestLevel( side_t side ) const;

private:
    virtual void onBookChanged( const IBook* pBook, const Msg* pMsg, int32_t bidLevelChanged, int32_t askLevelChanged );
    virtual void onBookFlushed( const IBook* pBook, const Msg* pMsg );

    virtual void recomputeState() const;

private:
    IBookPtr                m_spBook;
    const uint32_t          m_numLevels;
    boost::optional<double> m_limitUp, m_limitDown;
    PriceSize               m_deepestAskLevel, m_deepestBidLevel;
    const bool              m_inPercent;
};
BB_DECLARE_SHARED_PTR( SigLimitPrice );


/// SignalSpec for SigLimitPrice
class SigLimitPriceSpec : public NoRefPxSignalSpec
{
public:
    BB_DECLARE_SCRIPTING();

    SigLimitPriceSpec()
    {}
    SigLimitPriceSpec( const SigLimitPriceSpec &e );

    virtual instrument_t getInstrument() const { return m_book->getInstrument(); }
    virtual ISignalPtr build( SignalBuilder *builder ) const;
    virtual void checkValid() const;
    virtual void hashCombine( size_t &result ) const;
    virtual bool compare( const ISignalSpec *other ) const;
    virtual void print( std::ostream &o, const LuaPrintSettings &ps ) const;
    virtual void getDataRequirements( IDataRequirements *rqs ) const;
    virtual SigLimitPriceSpec *clone() const;

    IBookSpecCPtr   m_book;
    uint32_t        m_numLevels;
    bool            m_inPercent;
};
BB_DECLARE_SHARED_PTR( SigLimitPriceSpec );



} // namespace signals
} // namespace bb

#endif // BB_SIGNALS_SIGLIMITPRICE_H

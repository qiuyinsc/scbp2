#ifndef BB_SIGNALS_SIGDIFF_H
#define BB_SIGNALS_SIGDIFF_H

/* Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved. */


#include <bb/signals/Signal.h>
#include <bb/signals/SignalSpec.h>


namespace bb {
namespace signals {

class SigDiff
    : public SignalStateImpl
    , private ISignalListener
{
public:
    SigDiff( const instrument_t& instr, const std::string &desc, ISignalPtr signal1, ISignalPtr signal2 );

    /// Destructor
    virtual ~SigDiff();

private:
    // ISignalListener
    virtual void onSignalChanged( const ISignal& signal );

    void reset();
    virtual void recomputeState() const;

private:
    ISignalPtr m_signal1, m_signal2;
    timeval_t m_lastChangeTv;
};
BB_DECLARE_SHARED_PTR( SigDiff );


/// SignalSpec for SigDiff
class SigDiffSpec : public NoRefPxSignalSpec
{
public:
    BB_DECLARE_SCRIPTING();

    SigDiffSpec() {}
    SigDiffSpec( const SigDiffSpec &other );

    virtual instrument_t getInstrument() const { return m_signal1->getInstrument(); }
    virtual ISignalPtr build( SignalBuilder *builder ) const;
    virtual void checkValid() const;
    virtual void hashCombine( size_t &result ) const;
    virtual bool compare( const ISignalSpec *other ) const;
    virtual void print( std::ostream &o, const LuaPrintSettings &ps ) const;
    virtual void getDataRequirements( IDataRequirements *rqs ) const;
    virtual SigDiffSpec *clone() const;

    ISignalSpecPtr m_signal1, m_signal2;
};
BB_DECLARE_SHARED_PTR( SigDiffSpec );


} // namespace signals
} // namespace bb


#endif // BB_SIGNALS_SIGDIFF_H

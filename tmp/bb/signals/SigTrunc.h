#ifndef BB_SIGNALS_SIGTRUNC_H
#define BB_SIGNALS_SIGTRUNC_H

/* Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved. */


#include <bb/signals/Signal.h>
#include <bb/signals/SignalSpec.h>

namespace bb {
namespace signals {

struct TruncLimit;
typedef std::vector<TruncLimit> TruncLimitVec;

/// Restricts the components of a signal to lie within a given [min,max] range.
class SigTrunc : public SignalStateImpl, public ISignalListener
{
public:
    SigTrunc(const instrument_t& instr, const std::string &desc,
            ISignalPtr inputSig, const TruncLimitVec &specs);

protected:
    virtual void onSignalChanged( const ISignal& signal );
    virtual void recomputeState() const;

    ISignalPtr m_inputSig;
    std::vector<double> m_min, m_max;
};
BB_DECLARE_SHARED_PTR(SigTrunc);


struct TruncLimit
{
    TruncLimit();
    void checkValid() const;

    /// which signal state component this limit applies to
    int m_index;

    /// These default to unlimited.
    double m_min, m_max;
};
inline bool operator==(const TruncLimit &a, const TruncLimit &b)
    { return a.m_index == b.m_index && a.m_min == b.m_min && a.m_max == b.m_max; }
inline bool operator==(const TruncLimit &e, int i) { return e.m_index == i; }


/// SigTruncSpec
class SigTruncSpec : public ISignalSpec
{
public:
    BB_DECLARE_SCRIPTING();

    SigTruncSpec();
    SigTruncSpec(const SigTruncSpec &e);

    virtual instrument_t getInstrument() const { return m_inputSig->getInstrument(); }
    virtual std::string getDescription() const { return m_description; }

    virtual ISignalPtr build(SignalBuilder *builder) const;
    virtual void checkValid() const;
    virtual void hashCombine(size_t &result) const;
    virtual bool compare(const ISignalSpec *other) const;
    virtual void print(std::ostream &o, const LuaPrintSettings &ps) const;
    virtual void getDataRequirements(IDataRequirements *rqs) const;
    virtual SigTruncSpec *clone() const;

    std::string m_description;
    ISignalSpecPtr m_inputSig;
    TruncLimitVec m_limits;
};
BB_DECLARE_SHARED_PTR(SigTruncSpec);

} // namespace signals
} // namespace bb

#endif // BB_SIGNALS_SIGTRUNC_H

#ifndef BB_SIGNALS_SIGNALS_SCRIPTING_H
#define BB_SIGNALS_SIGNALS_SCRIPTING_H

/* Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved. */


namespace bb
{
    namespace signals
    {
        /// Registers the signals library with ScriptManager.
        /// The library can be loaded from C++ with ScriptManager::loadLibary( "signals" )
        /// or from Lua with loadLibary( "signals" ).
        bool registerScripting();

    } // namespace signals
} // namespace bb

#endif // BB_SIGNALS_SIGNALS_SCRIPTING_H

#ifndef BB_SIGNALS_EMAPRICEPROVIDER_H
#define BB_SIGNALS_EMAPRICEPROVIDER_H

/* Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved. */
#include <boost/optional.hpp>

#include <bb/clientcore/PriceProvider.h>
#include <bb/clientcore/PriceProviderSpec.h>
#include <bb/signals/MovingAverage.h>

namespace bb
{
namespace signals
{

class EMAPriceProvider
    : public PriceProviderImpl
{
public:
    EMAPriceProvider( IPriceProviderCPtr inputPxP, double halfLife, bool fillOnUpdate=false );

    virtual ~EMAPriceProvider() {}

    // IPriceProvider interface
    // The EMA is always returned. The isPriceOK check must be done by users
    virtual double getRefPrice( bool* pSuccess = NULL ) const;

    /// Returns true if getRefPrice would return a valid price if asked at current moment.
    virtual bool isPriceOK() const { return m_inputPxP->isPriceOK(); }

    /// Returns the instrument_t associated with this PriceProvider
    virtual instrument_t getInstrument() const { return m_inputPxP->getInstrument(); }

    /// Returns the timeval at which this PriceProvider last changed.
    virtual timeval_t getLastChangeTime() const { return m_inputPxP->getLastChangeTime(); }

private:
    void onPriceChanged( const IPriceProvider& inputPxP );

    IPriceProviderCPtr m_inputPxP;
    EMAGenerator m_emaGen;
    Subscription m_sub;
    bool m_fillOnUpdate;
    boost::optional<double> m_lastValidRefPx;
};

///////////////////////////////////////////////////////////////////////////////////////////

class EMAPxPSpec : public IPxProviderSpec
{
public:
    BB_DECLARE_SCRIPTING();

    EMAPxPSpec() : m_fillOnUpdate( false ) {}
    EMAPxPSpec( const EMAPxPSpec& a, const boost::optional<InstrSubst> &instrSubst );

    instrument_t getInstrument() const { return m_inputPxP->getInstrument(); }
    virtual IPxProviderSpec* clone( const boost::optional<InstrSubst>& instrSubst = boost::none ) const;
    virtual IPriceProviderPtr build( PriceProviderBuilder* builder ) const;

    void hashCombine( size_t& result ) const;
    bool compare( const IPxProviderSpec* other ) const;
    void print( std::ostream &o, const LuaPrintSettings &ps ) const;
    virtual void checkValid() const;
    virtual void getDataRequirements( IDataRequirements *rqs ) const { return m_inputPxP->getDataRequirements( rqs ); }

public:
    IPxProviderSpecPtr m_inputPxP;
    double m_halfLife;
    bool m_fillOnUpdate;
};

} // namespace signals
} // namespace bb

#endif // BB_SIGNALS_EMAPRICEPROVIDER_H

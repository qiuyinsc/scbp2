#ifndef BB_SIGNALS_SIGNALPRINTERSPEC_H
#define BB_SIGNALS_SIGNALPRINTERSPEC_H

/* Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved. */


#include <bb/core/instrument.h>
#include <bb/core/smart_ptr.h>
#include <bb/core/Scripting.h>
#include <bb/clientcore/PriceProviderSpec.h>

namespace bb {

class IDataRequirements;

namespace signals {

// Forward Declarations
class SignalPrinterSpec;
class ISignalBuilder;

BB_FWD_DECLARE_SHARED_PTR( ISignal );
BB_FWD_DECLARE_SHARED_PTR( ISignalPrinter );
BB_FWD_DECLARE_SHARED_PTR( ISignalPrinterSpec );

/*******************************************************************************************/
//
// SignalPrinterSpec interface and a useful base implementation
//
/*******************************************************************************************/

class ISignalPrinterSpec
{
public:
    BB_DECLARE_SCRIPTING();

    virtual ~ISignalPrinterSpec() {}

    virtual std::string getDescription() const = 0;

    /// Instantiates a concrete ISignal according to the Spec
    virtual ISignalPrinterPtr build( ISignalBuilder* builder ) const = 0;

    /// deep copy this ISignalPrinterSpec
    virtual ISignalPrinterSpec* clone() const = 0;
    inline ISignalPrinterSpec* clone( const ISignalPrinterSpec* e ) { return e ? e->clone() : NULL; }
    inline ISignalPrinterSpec* clone( ISignalPrinterSpecCPtr e ) { return clone( e.get() ); }

    /// Combines this ISignalPrinterSpec into the given hash value
    virtual void hashCombine( size_t& result ) const = 0;

    /// Compares two signal specs.
    virtual bool compare( const ISignalPrinterSpec* other ) const = 0;

    /// Prints the lua representation of this SignalPrinterSpec.
    virtual void print( std::ostream& o, const LuaPrintSettings& ps ) const = 0;

    /// ensures sure that all fields have been set correctly, or throws runtime_error
    virtual void checkValid() const {}

    /// Propagates all the required data ( instr/src pairs ) to the IDataRequirements.
    virtual void getDataRequirements( IDataRequirements* rqs ) const = 0;
};
BB_DECLARE_SHARED_PTR( ISignalPrinterSpec );

class ISignalPrinterSpecList : public std::vector<ISignalPrinterSpecCPtr>{};
BB_DECLARE_SHARED_PTR( ISignalPrinterSpecList );

/// Some stuff common to most signals
class SignalPrinterImplSpec : public ISignalPrinterSpec
{
public:
    BB_DECLARE_SCRIPTING();

    virtual void checkValid() const;

    virtual std::string getDescription() const { return m_description; }

    virtual void hashCombine( size_t& result ) const;
    virtual bool compare( const ISignalPrinterSpec* other ) const;

    virtual void getDataRequirements( IDataRequirements* rqs ) const;

    std::string m_description;

protected:
    SignalPrinterImplSpec();
    SignalPrinterImplSpec( const std::string& description );
    SignalPrinterImplSpec( const SignalPrinterImplSpec& e );
};
BB_DECLARE_SHARED_PTR( SignalPrinterImplSpec );

/// Helper class for converting a string into a SignalPrinterSpec.
class SignalPrinterSpecReader : public bb::SpecReader
{
public:
    SignalPrinterSpecReader();

    // parses the given lua code ( which should be lua code for a signal spec ),
    // and returns the signal spec
    ISignalPrinterSpecPtr getSignalPrinterSpec( std::string txt );
};

std::size_t hash_value( const ISignalPrinterSpec& a );

// SWIG 1.3.29 doesn't like these in-namespace operators
#ifndef SWIG

bool operator==( const ISignalPrinterSpec& a, const ISignalPrinterSpec& b ); // defined in SignalPrinterSpec.cc
inline bool operator!=( const ISignalPrinterSpec& a, const ISignalPrinterSpec& b ) { return !( a == b ); }

std::ostream& operator<<( std::ostream& out, const ISignalPrinterSpec& spec );
void luaprint( std::ostream& out, bb::signals::ISignalPrinterSpec const& bk, LuaPrintSettings const& ps );

#endif // !SWIG

// helpers for hash_map
struct SignalPrinterSpecHasher : public std::unary_function<size_t, ISignalPrinterSpecCPtr>
    { size_t operator()( const ISignalPrinterSpecCPtr& a ) const { return hash_value( *a ); } };
struct SignalPrinterSpecComparator : public std::binary_function<bool, ISignalPrinterSpecCPtr, ISignalPrinterSpecCPtr>
    { bool operator()( const ISignalPrinterSpecCPtr& a, const ISignalPrinterSpecCPtr& b ) const { return *a == *b; } };

} // namespace signals
} // namespace bb

#endif // BB_SIGNALS_SIGNALPRINTERSPEC_H

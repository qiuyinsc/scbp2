#ifndef BB_SIGNALS_DATASAMPLERCONTROLSIGNAL_H
#define BB_SIGNALS_DATASAMPLERCONTROLSIGNAL_H

/* Contents Copyright 2016 Athena Capital Research LLC. All Rights Reserved. */

#include <bb/core/Scripting.h>
#include <bb/core/LuabindScripting.h>
#include <bb/signals/IControlSignal.h>
#include <bb/statistics/DataSampler.h>

namespace bb {

BB_FWD_DECLARE_SHARED_PTR( ClockMonitor );

namespace signals {
BB_FWD_DECLARE_SHARED_PTR( ISignalSpec );
BB_FWD_DECLARE_SHARED_PTR( SignalBuilder );
BB_FWD_DECLARE_SHARED_PTR( ISignal );


class DataSamplerControlSignal
        : public IControlSignal
        , public bb::statistics::IDataSamplerListener
{
public:
    class Spec : public IControlSignal::Spec
    {
    public:
        BB_DECLARE_SCRIPTING();

        Spec(){}

        IControlSignalPtr create( const ISignalBuilderPtr& signalBuilder );

        bb::statistics::IDataSamplerSpecPtr data_sampler;
    };

public:
    DataSamplerControlSignal( const ISignalBuilderPtr& signalBuilder, const Spec& params = Spec() );
    virtual void onDataSample( const bb::statistics::IDataSampler* dataSampler );
    virtual int32_t value() const;
    virtual bool isValid() const;

private:
    const bb::ClockMonitorPtr m_spClockMonitor;
    bb::statistics::IDataSamplerPtr m_spDataSampler;
    bb::timeval_t m_lastSampleTv;
};
BB_DECLARE_SHARED_PTR( DataSamplerControlSignal );


} // namespace signals
} // namespace bb

#endif // BB_SIGNALS_DATASAMPLERCONTROLSIGNAL_H

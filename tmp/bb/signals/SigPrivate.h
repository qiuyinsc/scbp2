#ifndef BB_SIGNALS_SIGPRIVATE_H
#define BB_SIGNALS_SIGPRIVATE_H

/* Contents Copyright 2015 Athena Capital Research LLC. All Rights Reserved. */

// DO NOT MODIFY THIS FILE.
// The contents of this file are automatically generated.
// If you edit this file directly, your changes will be overwritten.
// DO NOT MODIFY THIS FILE.
/* ########## Do not modify the contents of this file, as it is used as a template for generated code ########## */


namespace bb {
namespace signals {

bool registerPrivateScripting() {

    return true;
}

} // namespace signals
} // namespace bb

#endif // BB_SIGNALS_SIGPRIVATE_H

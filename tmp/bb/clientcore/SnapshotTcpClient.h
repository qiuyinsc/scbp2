#ifndef BB_CLIENTCORE_SNAPSHOTTCPCLIENT_H
#define BB_CLIENTCORE_SNAPSHOTTCPCLIENT_H

/* Contents Copyright 2016 Athena Capital Research LLC. All Rights Reserved. */


#include <bb/core/messages.h>
#include <bb/clientcore/ClientContext.h>
#include <bb/io/Socket.h>
#include <bb/io/RecvTransport.h>
#include <bb/io/SendTransport.h>
#include <bb/io/TransMplexStream.h>
#include <bb/io/AsyncConnector.h>
#include <bb/io/NonBlockingByteSink.h>
#include <bb/io/feeds.h>

namespace bb {

class SnapshotTcpClientListener
{
public:
    virtual void onSnapshot( const BookSnapshotMsg& msg ) = 0;
    virtual ~SnapshotTcpClientListener() {}
};

class SnapshotTcpClient
    : private IMStreamCallback
{
public:
    SnapshotTcpClient( ClientContextPtr spCC, source_t src );

    void requestInstrFromSnapshotServer( const instrument_t& instr );

    void addListener( const instrument_t& instr, SnapshotTcpClientListener* l );
    void removeListener( const instrument_t& instr, SnapshotTcpClientListener* l );

private:
    /// Implements IMStreamCallback, for spinserver responses
    virtual void onMessage( const Msg& msg );

    void onSpinServerConnected( EFeedType feed_type, FDSet& sd, const TCPSocketPtr& socket, int the_errno );
    void onSpinServerConnectionEnd( EFeedType feed_type );

    void sendInstrRequest( const instrument_t& instr );

    HostPortPair                   m_host_port;
    TCPSocketRecvTransportPtr      m_recv_transport;
    NonBlockingTCPSendTransportPtr m_send_transport;
    Subscription                   m_subscription;
    source_t                       m_srcBook;
    LiveClientContextPtr           m_spliveClientContext;

    typedef std::set< SnapshotTcpClientListener* > ListenerSet;
    typedef bbext::hash_map<instrument_t, ListenerSet, instrument_t::hash_no_mkt_no_currency, instrument_t::equals_no_mkt_no_currency> InstrListenerMap;

    InstrListenerMap m_listeners;
};

BB_DECLARE_SHARED_PTR( SnapshotTcpClient );

// Client factory class to ensure we have a unique client per source
// This way we can reuse the same book_snapshot_server connection for different instruments
class SnapshotTcpClientFactory
{
public:
    static SnapshotTcpClientPtr getTcpClient( ClientContextPtr spCC, source_t src )
    {
        ClientMap::iterator it = m_snapshotServerTcpClients.find( src );
        if( it != m_snapshotServerTcpClients.end() )
        {
            return it->second;
        }

        m_snapshotServerTcpClients[src] = boost::make_shared<SnapshotTcpClient>( spCC, src );
        return m_snapshotServerTcpClients[src];
    }

private:
    typedef std::map< source_t, SnapshotTcpClientPtr > ClientMap;
    static ClientMap m_snapshotServerTcpClients;
};

}

#endif // BB_CLIENTCORE_SNAPSHOTTCPCLIENT_H

#ifndef BB_CLIENTCORE_PERIODICWAKEUP_H
#define BB_CLIENTCORE_PERIODICWAKEUP_H

/* Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved. */


#include <bb/core/ptime.h>

#include <bb/clientcore/ClockMonitor.h>

namespace bb {
// for unit testing
class PeriodicWakeupTest;

/// Arranges with ClockMonitor to wakeup repeatedly with a fixed interval in between.
class PeriodicWakeup
{
public:
    /// This arranges for wakeups at all times (wakeupOffset+ wakeupInterval*n), where n
    /// is an integer. startPeriodicWakeup() must be called to begin firing.
    ///
    /// fillInGaps matters when we get woken up by ClockMonitor late, and several intervals
    /// have passed. If it is true, multiple callbacks will be fired for each missed interval.
    /// If it is false, only one callback will be fired (for the original schedule wakeup time).
    PeriodicWakeup(ClockMonitor *cm, const ptime_duration_t &wakeupInterval,
            const ptime_duration_t &wakeupOffset, uint32_t priority, bool fillInGaps);
    PeriodicWakeup(); // for delayed initialization using setup()
    virtual ~PeriodicWakeup() {}

    void startPeriodicWakeup();
    void stopPeriodicWakeup();

    virtual void onPeriodicWakeup(const timeval_t &ctv, const timeval_t &swtv) = 0;

protected:
    friend class PeriodicWakeupTest;
    void setup(ClockMonitor *cm, const ptime_duration_t &wakeupInterval,
            const ptime_duration_t &wakeupOffset, uint32_t priority, bool fillInGaps);
    timeval_t getNextWakeupTime(const bb::timeval_t &currentTime) const;

    ClockMonitor *m_clockMonitor;
    uint32_t m_wakeupPriority;

    bool periodicWakeupActive() const { return m_clockRunning; }

private:
    void periodicWakeupImpl(const timeval_t& ctv, const timeval_t& swtv);

    ptime_duration_t m_wakeupInterval, m_wakeupOffset;
    Subscription m_pendingWakeup;
    bool m_clockRunning, m_fillInGaps;
};


struct PeriodicWakeupActiveSpec{
    PeriodicWakeupActiveSpec(bb::timeval_t const& start,bb::timeval_t const& endtv, ptime_duration_t const& interval);
    bb::timeval_t getStartTv()const;
    bb::timeval_t getEndTv()const;
    ptime_duration_t getInterval()const;

    ptime_duration_t getOffset()const;
    PeriodicWakeupActiveSpec& setOffset(ptime_duration_t const&);
    uint32_t getPriority()const;
    PeriodicWakeupActiveSpec& setPriority(uint32_t const&);
    bool getFillInGaps()const;
    PeriodicWakeupActiveSpec& setFillInGaps(bool);
private:
    bb::timeval_t   m_starttv;
    bb::timeval_t   m_endtv;
    ptime_duration_t m_interval;
    ptime_duration_t m_offset;
    uint32_t        m_priority;
    bool            m_fillgaps;
};
std::ostream&operator<<(std::ostream&,PeriodicWakeupActiveSpec const&);


struct PeriodicWakeupActive:protected PeriodicWakeup{
protected:
    PeriodicWakeupActive(ClockMonitor *cm,PeriodicWakeupActiveSpec const&);
    bb::PeriodicWakeupActiveSpec  const& getSpec()const;
private:
    void start( );
    void stop();
    Subscription m_nextStateSub;
    bb::PeriodicWakeupActiveSpec  m_spec;
    bb::ClockMonitor *m_cm;
};

} // namespace bb

#endif // BB_CLIENTCORE_PERIODICWAKEUP_H

#ifndef BB_CLIENTCORE_MASTERTICKFACTORY_H
#define BB_CLIENTCORE_MASTERTICKFACTORY_H

/* Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved. */


#include <set>
#include <bb/core/hash_map.h>
#include <bb/core/sourceset.h>
#include <bb/clientcore/MasterTick.h>

namespace bb {

BB_FWD_DECLARE_SHARED_PTR( SourceTickFactory );

/** Creates MasterTicks
 */
class MasterTickFactory
{
public:
    /// Constructs a MasterTickFactory for the given SourceTickFactory.
    MasterTickFactory( SourceTickFactoryPtr const& src_factory );

    MasterTickProviderPtr getMasterTickProvider( const instrument_t& instr, const sourceset_t& srcs, bool create );

private:
    SourceTickFactoryPtr m_spSourceTickFactory;

    typedef bbext::hash_map<size_t, MasterTickProviderPtr> Hash2MasterTickMap;
    Hash2MasterTickMap m_alreadyBuiltMasterTicks;
};

BB_DECLARE_SHARED_PTR( MasterTickFactory );

}

#endif // BB_CLIENTCORE_MASTERTICKFACTORY_H

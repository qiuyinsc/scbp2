#ifndef BB_CLIENTCORE_TDORDERBOOK_H
#define BB_CLIENTCORE_TDORDERBOOK_H

/* Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved. */


#include <map>

#include <bb/core/smart_ptr.h>

#include <bb/clientcore/TDOrder.h>
#include <bb/clientcore/Book.h>

namespace bb {

struct TDLogInfo;
class TdFillFutureMsg;
class TdFillMsg;
class TdFillNewMsg;
class TdOrderBaseMsg;
class TdOrderFutureMsg;
class TdOrderMsg;
class TdOrderNewMsg;
class TdStatusChangeMsg;

BB_FWD_DECLARE_SHARED_PTR(TDLogManager);

/// TDOrderBook
/// Builds a list of orders by processing td messages received.
///
/// Histo-only for the time being, talk to BMB about fixing this
class TDOrderBook
    : public BookImpl
{
public:
    /// TDOrderBook constructor
    /// requireAtMarket:
    ///    If requireAtMarket is true, an Order is only considered
    ///    part of the book once it has been confirmed on the market (STAT_OPEN).
    ///    Otherwise, it will appear in the book immediately after it has been
    ///    announced by a trade demon.
    /// requireVisible:
    ///    If requireVisible is true, an Order is only considered
    ///    part of the book if it is a visible order.
    /// acct:
    ///    Specifies the account that this order book listens to.
    ///    If it is ACCT_ALL, all accounts are considered.
    /// Note that this book's source is the source of the local ORDERS feed.
    TDOrderBook( bool requireAtMarket, bool requireVisible, acct_t acct, mktdest_t mkt,
                 EventDistributorPtr spED, TDLogManagerPtr tdLogs, 
                 const instrument_t& instr, const std::string& desc );
    virtual ~TDOrderBook();

    /// Returns the requireAtMarket flag.
    /// If requireAtMarket is true, an Order is only considered part of the book
    /// once it has been confirmed on the market (STAT_OPEN).  Otherwise, it will
    /// appear in the book immediately after it has been announced by a trade demon.
    bool getRequireAtMarket() const;

    /// Returns the requireVisible flag.
    /// If requireVisible is true, an Order is only considered
    /// part of the book if it is a visible order.
    bool getRequireVisible() const;

    /// Returns the account that this order book listens to.
    /// If it is ACCT_ALL, all accounts are considered.
    acct_t getAccount() const;

    /// Returns the market that this order book listens to.
    /// If it is MKT_UNKNOWN, all markets are considered.
    mktdest_t getMarket() const;

    /// Returns the account held by this order book.
    /// Returns true if the book is in a "valid" state.
    /// The meaning of this is book-dependent, however clients typically use it
    /// to test whether they should "trust" the book's contents.
    virtual bool isOK() const;

    /// Flushes the book.
    virtual void flushBook();

    /// Returns the mid-price of a book, or 0.0 if the book is not valid.
    /// Since TDOrderBooks can be imbalanced (only bids or only asks), this
    /// method will return the ask price when there are only sell orders and
    /// the bid price when there are only buy orders.
    virtual double getMidPrice() const;

    /// Returns the PriceSize at a specified depth and side.
    /// Returns an empty PriceSize if nothing exists at that depth or side.
    virtual PriceSize getNthSide( size_t depth, side_t side ) const;

    /// Returns an object for iterating over BookLevels of a particular side.
    /// Postcondition: The returned iterator will iterate getNumLevels objects.
    /// This abstracts the underlying containers used by a book; book implementations
    /// will implement their own iterator and pass them back.
    virtual IBookLevelCIterPtr getBookLevelIter( side_t side ) const;

    /// Returns the number of levels of a particular side of this book.
    /// Note that empty levels above the "bottom" level are counted.
    virtual size_t getNumLevels( side_t side ) const;

    // public for unit tests
    void handle_order        ( const TDLogInfo &tdlog, const TdOrderMsg& msg_order );
    void handle_order_future ( const TDLogInfo &tdlog, const TdOrderFutureMsg& msg_order );
    void handle_order_new    ( const TDLogInfo &tdlog, const TdOrderNewMsg& msg_order );
    void handle_status_change( const TDLogInfo &tdlog, const TdStatusChangeMsg& msg_sc );
    void handle_fill         ( const TDLogInfo &tdlog, const TdFillMsg& msg_fill );
    void handle_fill_future  ( const TDLogInfo &tdlog, const TdFillFutureMsg& msg_fill );
    void handle_fill_new     ( const TDLogInfo &tdlog, const TdFillNewMsg& msg_fill );

protected:
    void handle_order_base   ( const TDLogInfo &tdlog, const TdOrderBaseMsg& msg_order, const instrument_t instr );

    typedef std::map<std::pair<uint32_t, uint32_t>,TDOrder>  OrderMap;
    typedef OrderMap::value_type OrderPair;

    /// Looks in the buy/sell maps and returns a pair result.
    /// The first is 0 if it is buy-side, 1 if it is sell-side, and -1 if not found.
    /// The second is the iterator in the corresponding map, if found.
    std::pair<short,OrderMap::iterator> findOrder( uint32_t fileID, uint32_t orderid );

    int addOrderToBookLevel( const TDOrder& order );
    int dropOrderFromBookLevel( const TDOrder& order );
    int updateOrderInBookLevel( const TDOrder& order );

protected:
    bool                    m_requireAtMarket, m_requireVisible;
    acct_t                  m_acct;
    mktdest_t               m_mkt;

    OrderMap                m_orderMap[2];      // BID = 0, ASK = 1
    bookhalf_t              m_bhalf[2];         // BID = 0, ASK = 1
    EventDistributorWeakPtr m_wpED;
    Subscription            m_orderMsgSub, m_orderFutureMsgSub, m_orderNewMsgSub,
                            m_statChangeMsgSub,
                            m_fillMsgSub, m_fillFutureMsgSub, m_fillNewMsgSub;

private:
    // Drop all elements in m_bhalf.
    void clearLevels();
};
BB_DECLARE_SHARED_PTR( TDOrderBook );


/**********************************************************************************/
//
// Inline implementation
//
/**********************************************************************************/

/// Returns the requireAtMarket flag.
/// If requireAtMarket is true, an Order is only considered part of the book
/// once it has been confirmed on the market (STAT_OPEN).  Otherwise, it will
/// appear in the book immediately after it has been announced by a trade demon.
inline bool TDOrderBook::getRequireAtMarket() const
{   return m_requireAtMarket; }

/// Returns the requireVisible flag.
/// If requireVisible is true, an Order is only considered
/// part of the book if it is a visible order.
inline bool TDOrderBook::getRequireVisible() const
{   return m_requireVisible; }

/// Returns the account that this order book listens to.
/// If it is ACCT_ALL, all accounts are considered.
inline acct_t TDOrderBook::getAccount() const
{   return m_acct; }

/// Returns the market that this order book listens to.
/// If it is MKT_UNKNOWN, all markets are considered.
inline mktdest_t TDOrderBook::getMarket() const
{   return m_mkt; }


} // namespace bb

#endif // BB_CLIENTCORE_TDORDERBOOK_H

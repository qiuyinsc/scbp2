#ifndef BB_CLIENTCORE_MASTERTICK_H
#define BB_CLIENTCORE_MASTERTICK_H

/* Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved. */


#include <vector>
#include <bb/clientcore/TickProvider.h>


namespace bb {

class MasterTickProvider
    : public TickProviderImpl
    , protected ITickListener
{
public:
    MasterTickProvider( const instrument_t& instr, const std::string& desc );
    virtual ~MasterTickProvider();

    /// Adds an ITickProvider to this MasterTickProvider.
    /// Returns true if successful, or false if the ITickProvider is already monitored.
    bool addTickProvider( ITickProviderPtr spTP );

    /// Removes an ITickProvider to this MasterTickProvider.
    /// Returns true if successful, or false if the ITickProvider was not found.
    bool removeTickProvider( ITickProviderPtr spTP );

    /// Returns true if the last obtained tick is OK.
    /// Returns false if there is no last tick.
    virtual bool isLastTickOK() const
    {   return m_ok; }

protected:
    /// Update our totals and forward tick to our listeners.
    virtual void onTickReceived( const ITickProvider* tp, const TradeTick& tick );

    /// Update our totals and forward volume to our listeners.
    virtual void onTickVolumeUpdated( const ITickProvider* tp, uint64_t totalVolume );

protected:
    std::vector<ITickProviderPtr>   m_tickProviders;
    typedef std::pair<ITickProvider*, uint64_t> TotalVolPair;
    std::vector<TotalVolPair>       m_totalVolumeTracker;
    bool                            m_ok;
};
BB_DECLARE_SHARED_PTR( MasterTickProvider );


} // namespace bb

#endif // BB_CLIENTCORE_MASTERTICK_H

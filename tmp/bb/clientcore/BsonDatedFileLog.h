#ifndef BB_CLIENTCORE_BSONDATEDFILELOG_H
#define BB_CLIENTCORE_BSONDATEDFILELOG_H

/* Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved. */


#include <string>

#include <bb/core/timeval.h>
#include <bb/io/BsonLog.h>
#include <bb/clientcore/IClockListener.h>

namespace bb {

BB_FWD_DECLARE_SHARED_PTR( ClockMonitor );

class BsonDatedFileLog : public BsonLog, public IClockListener
{
public:
    static const ssize_t BUFFER_DEFAULT;
    static const ssize_t UNBUFFERED;

    static const char* DEFAULT_POSTFIX; // todo: this should be DEFAULT_FILE_EXTENSION and just 'bson'

    BsonDatedFileLog(
        const ClockMonitorPtr& clockMonitor,
        const std::string& prefix,
        const std::string& postfix = DEFAULT_POSTFIX,
        const ssize_t bufferSize = UNBUFFERED );


    virtual ~BsonDatedFileLog();

    void setAutoTimestamp( bool b );

protected:
    virtual void onWakeupCall( const bb::timeval_t& ctv, const bb::timeval_t& swtv, int32_t reason, void* );

private:
    void openNextLogFile();
    void closeLogFile();

    const bb::ClockMonitorPtr m_clockMonitor;
    const std::string m_prefix;
    const std::string m_postfix;
    const ssize_t m_bufferSize;
};
BB_DECLARE_SHARED_PTR(BsonDatedFileLog);

} // namespace bb

#endif // BB_CLIENTCORE_BSONDATEDFILELOG_H

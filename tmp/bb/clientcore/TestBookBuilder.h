#ifndef BB_CLIENTCORE_TESTBOOKBUILDER_H
#define BB_CLIENTCORE_TESTBOOKBUILDER_H

/* Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved. */


#include <bb/clientcore/BookBuilder.h>
#include <bb/clientcore/L1Book.h>
#include <bb/clientcore/SourceBooks.h>

namespace bb {

class Msg;

class TestBookBuilder : public BookBuilder
{
public:
    class Book : public L1Book
    {
    public:
        Book( const instrument_t& instr, const source_t& src, ClockMonitorPtr cm )
            : L1Book( instr, src, cm, "" ) {}

        bool setTopLevel( side_t side, double price, size_t size )
        {
            m_lastChangeTv = m_spCM->getTime();
            return L1Book::setTopLevel( side, price, size );
        }
        virtual void notifyBookChanged( const Msg* pMsg, int32_t bidLevelChanged, int32_t askLevelChanged ) const
        {
            return L1Book::notifyBookChanged( pMsg, bidLevelChanged, askLevelChanged );
        }
    };

    BB_DECLARE_SHARED_PTR( Book );

public:
    TestBookBuilder( ClientContextPtr cc )
        : BookBuilder( cc, false )
    {}

    virtual bool getUseSrcMonitors() const { return true; }

    virtual IBookPtr buildBook(IBookSpecCPtr spec)
    {
        BookSpec2InstanceMap::iterator iter = m_alreadyBuiltBooks.find( spec );
        if( iter == m_alreadyBuiltBooks.end() )
        {
            IBookPtr new_book = createNewBook( spec );
            m_alreadyBuiltBooks[spec] = new_book;
            return new_book;
        }
        return iter->second.lock();
    }

    virtual IBookPtr getBuiltBook(IBookSpecCPtr spec)
    {
        BookSpec2InstanceMap::iterator iter = m_alreadyBuiltBooks.find( spec );
        if( iter == m_alreadyBuiltBooks.end() )
            return IBookPtr();
        return iter->second.lock();
    }

    virtual IBookPtr createNewBook( IBookSpecCPtr spec )
    {
        SourceBookSpecCPtr sbspec = boost::dynamic_pointer_cast<const SourceBookSpec>( spec );
        IBookPtr new_book;
        if( sbspec )
        {
            new_book.reset( new Book( sbspec->getInstrument()
                    , sbspec->m_source
                    , getClientContext()->getClockMonitor() ) );
        } else {
            new_book = spec->build(this);
        }
        return new_book;
    }

    BookPtr getSourceBook( const instrument_t& instr, source_t src )
    {
        SourceBookSpecPtr spec( new SourceBookSpec() );
        spec->m_instrument = instr;
        spec->m_source = src;
        return boost::dynamic_pointer_cast<Book>( buildBook( spec ) );
    }
};

} // namespace bb

#endif // BB_CLIENTCORE_TESTBOOKBUILDER_H

#ifndef BB_CLIENTCORE_MASTERBOOK_H
#define BB_CLIENTCORE_MASTERBOOK_H

/* Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved. */


#include <bb/core/sourceset.h>
#include <bb/clientcore/L2Book.h>
#include <boost/foreach.hpp>
#include <bb/clientcore/MasterBookLevel.h>

namespace bb {

BB_FWD_DECLARE_SHARED_PTR( ClockMonitor );

class CompositeBook
    : protected IBookListener
{
public:
    CompositeBook();
    virtual ~CompositeBook();
    /// Adds spSubBook to MB's list as the book for spSubBook->getSource()
    /// and stores spSMon as the SourceMonitor for spSubBook->getSource()
    void addSubBook( IBookPtr spSubBook, SourceMonitorPtr spSMon );

    /// Removes spSubBook from MB's list as the book for spSubBook->getSource()
    /// and remove spSMon as the SourceMonitor for spSubBook->getSource().
    /// Removes all BookOrders derived from that Book and invokes listener's onBookChanged.
    /*
      This function flushes the book that is passed
    */
    void removeSubBook( IBookPtr spSubBook, SourceMonitorPtr spSMon );

    /// Returns the the subbook associated with src, or an invalid shared_ptr if none exists
    IBookPtr getSubBook( source_t src );
    /// Returns the the subbook associated with src, or an invalid shared_ptr if none exists
    IBookCPtr getSubBook( source_t src ) const;

    /// returns the sources of the books monitored by this MasterBook.
    const std::vector<source_t>& getSources() const;

    /// Flushes the book. Listeners are notified with onBookFlush.  Otherwise, listeners are notified onBookChange.
    void flushUnderlyingBooks();

protected:
    // BookListener interface
    /// Invoked when the subscribed Book is flushed.
    virtual void onBookFlushed( const IBook* pBook, const Msg* pMsg ) = 0 ;

    /// Invoked when the subscribed Book changes.
    /// The levelChanged entries are negative if there is no change, or a 0-based depth.
    virtual void onBookChanged( const IBook* pBook, const Msg* pMsg,
                                int32_t bidLevelChanged, int32_t askLevelChanged ) = 0;

protected:
    typedef std::map<source_t, IBookPtr>    SourceBookMap;
    // indexed by src
    SourceBookMap                        subbooks;
    std::map<source_t, SourceMonitorPtr> smons;
    std::vector<source_t>                m_sources;
    double                               m_minCrossPxAmt;
    source_t                             m_compositeSource;
};

/// A MasterBook consists of the first N BookLevels of its component subbooks.
///
/// MasterBook doesn't directly listen to any data feeds. Instead, it gets
/// notified whenever one of it's component books changes.
class MasterBook
    : public CompositeBook
    , public L2Book
    , protected IBookLevelListener
{
public:
    MasterBook( const ClientContextPtr& cc, const instrument_t& instr, const char* desc, L2BookConfig config = L2BookConfig()  );

    virtual ~MasterBook();

    /// Adds spSubBook to MB's list as the book for spSubBook->getSource()
    /// and stores spSMon as the SourceMonitor for spSubBook->getSource()
    void addSubBook( IBookPtr spSubBook, SourceMonitorPtr spSMon );

    /// Removes spSubBook from MB's list as the book for spSubBook->getSource()
    /// and remove spSMon as the SourceMonitor for spSubBook->getSource().
    /// Removes all BookOrders derived from that Book and invokes listener's onBookChanged.
    /*
      This function flushes the book that is passed
    */
    void removeSubBook( IBookPtr spSubBook, SourceMonitorPtr spSMon );

    virtual void dropOrder(BookOrder* o);

    /// Flushes the book. Listeners are notified with onBookFlush.  Otherwise, listeners are notified onBookChange.
    virtual void flushBook();

    /// print the first optional_nlevels of the book to an ostream
    /// if optional_nlevels isn't specified, either 4 or print_level_chg + 2 is used
    virtual std::ostream& print(std::ostream &out, int optional_nlevels=-2) const;

    /// ClockListener interface
    virtual void onWakeupCall( const timeval_t &ctv, const timeval_t &swtv, int reason, void* pData );

protected:
// BookListener interface
    /// Invoked when the subscribed Book is flushed.
    virtual void onBookFlushed( const IBook* pBook, const Msg* pMsg );

    /// Invoked when the subscribed Book changes.
    /// The levelChanged entries are negative if there is no change, or a 0-based depth.
    virtual void onBookChanged( const IBook* pBook, const Msg* pMsg,
                                int32_t bidLevelChanged, int32_t askLevelChanged );

// BookLevelListener interface
    /// Invoked when the subscribed Book is about to add a previously non-existant BookLevel.
    /// Normally, return true.  Return false to advise the Book to not add the level.
    /// Note that the Book does not necessarily have to heed this advice.
    virtual bool onBookLevelAdded( const IBook* pBook, const BookLevel* pLevel );
    /// Invoked when the subscribed Book drops a previously existant BookLevel.
    virtual void onBookLevelDropped( const IBook* pBook, const BookLevel* pLevel );
    /// Invoked when the subscribed Book modifies the specified BookLevel.
    virtual void onBookLevelModified( const IBook* pBook, const BookLevel* pLevel );

    // L2Book function overload
    // creates a book level from the book order
    // this method was created so that if the mb wants to use a modified book level
    // we could over-ride this function and create an object of that modified BooKLevel class
    virtual BookLevel* createNewBookLevel( BookOrder* order );
protected:
    MasterBook();

    /// Most complicated behavioral part of a MasterBook. Attempts to behave reasonably
    /// intelligently in the face of crossed subbooks. Using escalating levels of
    /// seriousness of cross:
    /// Ignores small crosses (less than m_minCrossPxAmt).
    /// For crosses that are >= small crosses, checks if it's pretty clear
    /// whether one side or the other is screwed up (mostly by looking at source).
    /// If so, fixes book if possible and/or puts new order on hold. If no specific
    /// problem can be found, allows new order to be put on the book despite crossing.
    /// If the cross is >= m_maxCrossPxAmt, order is always put on hold even if no
    /// specific problem can be found. The MasterBook will never be crossed by more
    /// than m_maxCrossPxAmt.
    virtual bool handleCrossedOrder(BookOrder* o);
    virtual std::pair<long, long> getBookCheckOffset();

    static char* makeMktIdstr( char* s, const BookLevel* bl );
    static const int MaxMktIds = 5;

    int last_cross_print_secs;
    int last_cross_cond;

    // additional crossing behavior vars, in addition to maxCross.
    double m_warnCrossPxAmt;

    MasterBookLevelFactoryPtr  m_spMasterBookLevelFactory;
    static const int CROSS_NONE  = 0;
    static const int CROSS_MINOR = 1;
    static const int CROSS_MAJOR = 3;
};

BB_DECLARE_SHARED_PTR( MasterBook );

/// spec for building a master book
class MasterBookSpec : public BookSpecCommon
{
public:
    BB_DECLARE_SCRIPTING();

    MasterBookSpec() {}
    MasterBookSpec(const instrument_t &instr, const sourceset_t &sources)
        : BookSpecCommon(instr), m_sources(sources) {}
    MasterBookSpec(const MasterBookSpec &a, const boost::optional<InstrSubst> &instrSubst);

    virtual IBookPtr build(BookBuilder *builder) const;
    virtual void checkValid() const;
    virtual void hashCombine(size_t &result) const;
    virtual bool compare(const IBookSpec *other) const;
    virtual void print(std::ostream &o, const LuaPrintSettings &ps) const;
    virtual MasterBookSpec *clone(const boost::optional<InstrSubst> &instrSubst = boost::none) const;
    virtual void getDataRequirements(IDataRequirements *rqs) const;

    sourceset_t m_sources;
};
BB_DECLARE_SHARED_PTR(MasterBookSpec);


} // namespace bb

#endif // BB_CLIENTCORE_MASTERBOOK_H

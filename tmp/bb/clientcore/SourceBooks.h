#ifndef BB_CLIENTCORE_SOURCEBOOKS_H
#define BB_CLIENTCORE_SOURCEBOOKS_H

/* Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved. */


/**
   @file SourceBooks.h

   Books specialized for various sources.

**/

#include <bb/core/consts/cqs_consts.h>
#include <bb/core/hash_map.h>
#include <bb/core/auto_vector.h>

#include <boost/assign/list_of.hpp>

#include <bb/clientcore/L1Book.h>
#include <bb/clientcore/L2Book.h>
#include <bb/core/mktdest.h>
#include <bb/core/source.h>
#include <bb/clientcore/MultiExchL1Book.h>

namespace bb {

class BbL1QuoteMsg;
class ClientContext;
class NyseQuoteMsg;

class EdgxAddMsg;
class EdgxExeMsg;
class EdgxCxlMsg;
class EdgxModMsg;

class BatsAddMsg;
class BatsExeMsg;
class BatsCxlMsg;
class BatsModMsg;

BB_FWD_DECLARE_SHARED_PTR( RandomSource );
BB_FWD_DECLARE_SHARED_PTR( SourceMonitor );


/*****************************************************************************/
//
// CQS Book
//
/*****************************************************************************/

class CqsBookOrder
    : public BookOrder
{
public:
    cqs::exchange_t exch;

    CqsBookOrder( const instrument_t& _instr,
                  const timeval_t& _tv_msg, const timeval_t& _tv_exch,
                  int _idnum, side_t _side, double _px,
                  int _sz, mktdest_t _mkt, source_t _src, cqs::exchange_t _exch );
    virtual ~CqsBookOrder() {}
    virtual void reset();
    virtual std::ostream& print( std::ostream& out ) const;
};


class CqsBook
    : public L2Book
{
public:
    CqsBook( const ClientContextPtr& context, const instrument_t& instr, source_t src,
             SourceMonitorPtr sm, RandomSourcePtr rs, const char* desc, int _vbose, double basepx );
    virtual ~CqsBook();

    virtual std::ostream& print( std::ostream &out, int optional_nlvls = -2 ) const;
    virtual void flushBook();
    virtual void dropOrder( BookOrder* o );

    void createOrder( const timeval_t& _tv_msg, const timeval_t& _tv_exch,
                      int _idnum, side_t _side, double _px, int _sz, cqs::exchange_t exch );

    /// ClockListener interface
    virtual void onWakeupCall( const timeval_t &ctv, const timeval_t &swtv, int reason, void* pData );

    /// IEventListener interface
    virtual void onEvent( const Msg& msg );

protected:
    virtual bool handleCrossedOrder( BookOrder* o );
    virtual std::pair<long, long> getBookCheckOffset();

    auto_vector<CqsBookOrder*> exch_bids;
    auto_vector<CqsBookOrder*> exch_asks;
    EventSubPtr                m_eventSub;
};



/*****************************************************************************/
//
// ISLD Book
//
/*****************************************************************************/

class IsldBook
    : public L2Book
{
public:
    IsldBook( const ClientContextPtr& context, const instrument_t& instr, source_t src,
              SourceMonitorPtr sm, const char* desc, L2BookConfig config = L2BookConfig()  );
    virtual ~IsldBook();

    virtual void flushBook();
    virtual void dropOrder( BookOrder* o );

    void createOrder( const timeval_t& _tv_msg, const timeval_t& _tv_exch,
                      uint64_t _idnum, side_t _side, double _px, int _sz );

    /// IEventListener interface
    virtual void onEvent( const Msg& msg );

protected:
    typedef bbext::hash_map<uint64_t, BookOrder*> bo_map;

    virtual bool handleCrossedOrder( BookOrder* o );
    virtual std::pair<long, long> getBookCheckOffset();
    void dropOrder( bo_map::iterator mel );

    bo_map isldorders;
    EventSubPtr m_eventSub;
};



/*****************************************************************************/
//
// BATS Book
// Both BatsQD and EdgxQD are using the same message protocol.
// There is no difference between EdgxXXXMsg and BatsXXXMsg other than the name.
// The *Traits* below are used to make BatsBook be initilized with correct message types.
//
/*****************************************************************************/

template<bb::EFeedType> struct BatsBookTraits;

template<>
struct BatsBookTraits<bb::SRC_EDGX>
{
    static bb::mktdest_t getMkt() {
        return bb::MKT_EDGX;
    };
    static bb::EFeedType getSource() {
        return SRC_EDGX;
    };
    static std::vector<mtype_t> getSubscribeMType() {
        return boost::assign::list_of
                    (bb::MSG_EDGX_ADD)
                    (bb::MSG_EDGX_EXE)
                    (bb::MSG_EDGX_CXL)
                    (bb::MSG_EDGX_MOD);
    };
    typedef bb::EdgxAddMsg AddMsg;
    typedef bb::EdgxExeMsg ExeMsg;
    typedef bb::EdgxCxlMsg CxlMsg;
    typedef bb::EdgxModMsg ModMsg;
};

template<>
struct BatsBookTraits<bb::SRC_BATS>
{
    static bb::mktdest_t getMkt() {
        return bb::MKT_BATS;
    };
    static bb::EFeedType getSource() {
        return SRC_BATS;
    };
    static std::vector<mtype_t> getSubscribeMType() {
        return boost::assign::list_of
                    (bb::MSG_BATS_ADD)
                    (bb::MSG_BATS_EXE)
                    (bb::MSG_BATS_CXL)
                    (bb::MSG_BATS_MOD);
    };
    typedef bb::BatsAddMsg AddMsg;
    typedef bb::BatsExeMsg ExeMsg;
    typedef bb::BatsCxlMsg CxlMsg;
    typedef bb::BatsModMsg ModMsg;
};

typedef BatsBookTraits<bb::SRC_BATS> BatsTraits;
typedef BatsBookTraits<bb::SRC_EDGX> EdgxTraits;

template<typename Traits>
class BatsBaseBook
    : public L2Book
{
    typedef typename Traits::AddMsg AddMsg;
    typedef typename Traits::ExeMsg ExeMsg;
    typedef typename Traits::CxlMsg CxlMsg;
    typedef typename Traits::ModMsg ModMsg;

public:
    BatsBaseBook( const ClientContextPtr& context, const instrument_t& instr, source_t src,
                  SourceMonitorPtr sm, const char* desc, L2BookConfig config = L2BookConfig()  );
    virtual ~BatsBaseBook();

    virtual void flushBook();
    virtual void dropOrder( BookOrder* o );

    void createOrder( const timeval_t& _tv_msg, const timeval_t& _tv_exch,
                      uint64_t _idnum, side_t _side, double _px, int _sz );

    /// IEventListener interface
    virtual void onEvent( const Msg& msg );

protected:
    typedef bbext::hash_map<uint64_t, BookOrder*> bo_map;

    virtual bool handleCrossedOrder( BookOrder* o );
    virtual std::pair<long, long> getBookCheckOffset();
    void dropOrder( bo_map::iterator mel );

    bo_map m_batsorders;
    EventSubPtr m_eventSub;
};


typedef BatsBaseBook<BatsTraits> BatsBook;
typedef BatsBaseBook<EdgxTraits> EdgxBook;


/*****************************************************************************/
//
// NYSE Quotes Book
//
/*****************************************************************************/

class NYSEQuotesBook
    : public BookImpl
    , protected IEventDistListener
    , protected SourceMonitorListener
{
public:
    NYSEQuotesBook( const ClientContextPtr& context, const instrument_t& instr,
            SourceMonitorPtr smon,
            source_t src,
            const std::string &desc,
            int vbose=0);
    virtual ~NYSEQuotesBook();

    // IBook implementation
    virtual bool isOK() const;
    virtual void flushBook();
    virtual double getMidPrice() const;
    virtual PriceSize getNthSide( size_t depth, side_t side ) const;
    virtual IBookLevelCIterPtr getBookLevelIter( side_t side ) const;
    virtual size_t getNumLevels( side_t side ) const;

protected:
    // an IBookLevelCIterPtr which runs through only one item.
    class SingletonLevelCIter : public IBookLevelCIter
    {
    public:
        SingletonLevelCIter(BookLevelCPtr ptr) : m_bookLevel(ptr) {}
        virtual bool hasNext() const { return m_bookLevel != NULL; }
        virtual BookLevelCPtr next() { BookLevelCPtr r = m_bookLevel; m_bookLevel.reset(); return r; }
        BookLevelCPtr m_bookLevel;
    };
    BB_DECLARE_SHARED_PTR(SingletonLevelCIter);

    /// EventListener interface
    virtual void onEvent( const Msg& msg );

    /// SourceMonitorListener interface
    virtual void onSMonStatusChange( source_t src, smon_status_t status, smon_status_t old_status,
            const ptime_duration_t& avg_lag_tv, const ptime_duration_t& last_interval_tv );

    void updateLevel(int &levelChanged, side_t side, double bestPrice, uint32_t size, const NyseQuoteMsg *msg);

    int                 m_verbose;
    EventDistributorPtr m_eventDistributor;
    SourceMonitorPtr    m_sourceMonitor;
    bool                m_sourceOK;

    BookLevelPtr        m_topOfBook[2];
    EventSubPtr m_eventSub;
};



/*****************************************************************************/
//
// SXL1 Book
//
/*****************************************************************************/

class SxL1Book
    : public MultiExchL1Book
{
public:
    SxL1Book( const ClientContextPtr& context, const instrument_t& instr, source_t src,
             SourceMonitorPtr sm, const char* desc, int _vbose );
    virtual ~SxL1Book();

    void createOrder( const timeval_t& _tv_msg, const timeval_t& _tv_exch, int _idnum,
                      side_t _side, double _px, int _sz, mktdest_t mkt, bool append );

    /// IEventListener interface
    virtual void onEvent( const Msg& msg );

protected:
    virtual bool handleCrossedOrder( BookOrder* o );
    virtual std::pair<long, long> getBookCheckOffset();
    EventSubPtr m_eventSub;
};



/*****************************************************************************/
//
// OPRA Book
//
/*****************************************************************************/

class OpraBook
    : public MultiExchL1Book
{
public:
    OpraBook( const ClientContextPtr& context, const instrument_t& instr, source_t src,
             SourceMonitorPtr sm, const char* desc, int _vbose );
    virtual ~OpraBook();

    void createOrder( const timeval_t& _tv_msg, const timeval_t& _tv_exch, int _idnum,
                      side_t _side, double _px, int _sz, mktdest_t mkt, bool append );

    /// IEventListener interface
    virtual void onEvent( const Msg& msg );

protected:
    virtual bool handleCrossedOrder( BookOrder* o );
    virtual std::pair<long, long> getBookCheckOffset();
    EventSubPtr m_eventSub;
};



/*****************************************************************************/
//
// BBL1 Book
//
/*****************************************************************************/

class BbL1Book
    : public MultiExchL1Book
{
public:
    BbL1Book( const ClientContextPtr& context, const instrument_t& instr, source_t src,
              SourceMonitorPtr sm, const char* desc, int _vbose );
    virtual ~BbL1Book();

    /// IEventListener interface
    virtual void onEvent( const Msg& msg );

protected:
    virtual bool handleCrossedOrder( BookOrder* o );
    virtual std::pair<long, long> getBookCheckOffset();
    EventSubPtr m_eventSub;

private:
    void handleQuoteMessage( const BbL1QuoteMsg& msg, bool append );

    void updateSide( side_t side, const BbL1QuoteMsg& msg, double px, int size, bool append );
};
BB_DECLARE_SHARED_PTR( BbL1Book );


/*****************************************************************************/
//
// BBO Book
//
/*****************************************************************************/

class BBOBook
    : public L1Book
{
public:
    BBOBook( const ClientContextPtr& context, const instrument_t& instr, source_t src,
              SourceMonitorPtr sm, const char* desc, int _vbose );
    virtual ~BBOBook();

    /// IEventListener interface
    virtual void onEvent( const Msg& msg );

protected:
    EventSubPtr m_eventSub;
    SourceMonitorPtr    m_spSMon;
private:
};
BB_DECLARE_SHARED_PTR( BbL1Book );


/*****************************************************************************/
//
// TSX CDF Book
//
/*****************************************************************************/

class TsxCdfBook
    : public L2Book
{
public:
    TsxCdfBook( const ClientContextPtr& context, const instrument_t& instr, source_t src,
             SourceMonitorPtr sm, RandomSourcePtr rs, const char* desc, int _vbose );
    virtual ~TsxCdfBook();

    virtual void flushBook();
    virtual void dropOrder( BookOrder* o);

    void createOrder( const timeval_t& _tv_msg, const timeval_t& _tv_exch, uint64_t _idnum,
                      side_t _side, double _px, int _sz );

    /// IEventListener interface
    virtual void onEvent( const Msg& msg );

protected:
    typedef bbext::hash_map<uint64_t, BookOrder*> BookOrderMap;

    virtual bool handleCrossedOrder( BookOrder* o );
    virtual std::pair<long, long> getBookCheckOffset();
    void dropOrder( BookOrderMap::iterator mel );
    BookOrderMap m_tsxOrders;

    EventSubPtr m_eventSub;
};



/*****************************************************************************/
//
// SourceBookSpec
//
/*****************************************************************************/

/// BookSpec corresponding to the source books
class SourceBookSpec : public BookSpecCommon
{
public:
    BB_DECLARE_SCRIPTING();

    SourceBookSpec() : m_source(SRC_UNKNOWN) {}
    SourceBookSpec(const SourceBookSpec &a, const boost::optional<InstrSubst> &instrSubst);
    SourceBookSpec(const instrument_t &instr, source_t src) : BookSpecCommon(instr), m_source(src) {}

    virtual IBookPtr build(BookBuilder *builder) const;
    virtual void checkValid() const;
    virtual void hashCombine(size_t &result) const;
    virtual bool compare(const IBookSpec *other) const;
    virtual void print(std::ostream &o, const LuaPrintSettings &ps) const;
    virtual SourceBookSpec *clone(const boost::optional<InstrSubst> &instrSubst = boost::none) const;
    virtual void getDataRequirements(IDataRequirements *rqs) const;

    source_t m_source;
    EventSubPtr m_eventSub;
};
BB_DECLARE_SHARED_PTR(SourceBookSpec);




} // namespace bb



#endif // BB_CLIENTCORE_SOURCEBOOKS_H

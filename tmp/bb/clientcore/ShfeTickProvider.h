#ifndef BB_CLIENTCORE_SHFETICKPROVIDER_H
#define BB_CLIENTCORE_SHFETICKPROVIDER_H

/* Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved. */


#include <boost/optional.hpp>

#include <bb/core/messages.h>
#include <bb/clientcore/TickProvider.h>

namespace bb {

class ShfeQdMsg;

BB_FWD_DECLARE_SHARED_PTR( MsgHandler );

class ShfeTickProvider
    : public ITradeSplitter
    , public TickProviderImpl
{
public:
    /// Constructs an ShfeTickProvider for the given instrument,
    /// receiving events from the EventDistributor.
    /// Instrument must be a stock.
    ShfeTickProvider( const ClientContextPtr& context, const instrument_t& instr, source_t source,
                      const std::string& desc );

    /// Destructor.
    virtual ~ShfeTickProvider() {}

    /// Returns true if the last obtained tick is OK.
    /// Returns false if there is no last tick.
    virtual bool isLastTickOK() const           { return m_bTickReceived; }

    double getNotionalTurnover() const { return m_notionalTurnover; }
    double getNotionalAvgPx() const { return m_notionalAvgPx; }
    int32_t getOpenInterest() const { return m_openInterest; }
    boost::optional<double> getAvgPxInLastTick( double lot_size ) const;

    virtual const TradeTick* getAboveVwapTradeTick() const
    {   return &m_aboveVwap; }

    virtual const TradeTick* getBelowVwapTradeTick() const
    {   return &m_belowVwap; }

private:
    // Event handlers
    void onShfeQdMsg( const ShfeQdMsg& msg );

private:
    double m_tickSize;
    double m_contractSize;
    bool m_bInitialized;
    bool m_bTickReceived;
    int64_t m_lastSerial;
    timeval_t m_lastMsgTimeval;
    MsgHandlerPtr m_subShfeQdMsg;

    double m_notionalTurnover;
    double m_notionalAvgPx;
    int32_t m_openInterest;

    double m_lastNotionalTurnover;
    uint32_t m_lastTotalVolume;
    TradeTick m_aboveVwap;
    TradeTick m_belowVwap;
};

BB_DECLARE_SHARED_PTR( ShfeTickProvider );

//
// An adapter which turns an ShfeTickProvider into a IPriceProvider that provides VWAP
//
class ShfeTickBasedVWAPProvider
    : public PriceProviderImpl
    , protected ITickListener
{
public:
    ShfeTickBasedVWAPProvider( ShfeTickProviderPtr spSTP, double lotSize );
    virtual ~ShfeTickBasedVWAPProvider();

    /// Returns the last VWAP price as the "reference price".
    /// Returns NaN if no VWAP price is available, and sets pSuccess to
    /// false. Otherwise returns a VWAP price and sets pSuccess true.
    virtual double getRefPrice( bool* pSuccess = NULL ) const;

    virtual bool isPriceOK() const
    {   return m_spShfeTickProvider->isLastTickOK(); }

    /// Returns the ShfeTickProvider's instrument_t.
    virtual instrument_t getInstrument() const
    {   return m_spShfeTickProvider->getInstrument(); }

    /// Returns the timeval at which this PriceProvider last changed.
    virtual timeval_t getLastChangeTime() const
    {   return m_spShfeTickProvider->getLastMsgTimestamp(); }

protected:
    virtual void onTickReceived( const ITickProvider* tp, const TradeTick& tick );
    virtual void onTickVolumeUpdated( const ITickProvider* tp, uint64_t totalVolume ) {}

protected:
    ShfeTickProviderPtr m_spShfeTickProvider;
    TradeTick           m_notifiedTick;
    double              m_lotSize;
};
BB_DECLARE_SHARED_PTR( ShfeTickBasedVWAPProvider );

//
/// VWAPPSpec corresponding to a ShfeTickBasedVWAPProvider
//
class ShfeTickVWAPPSpec : public IPxProviderSpec
{
public:
    BB_DECLARE_SCRIPTING();

    ShfeTickVWAPPSpec() {}
    ShfeTickVWAPPSpec(const instrument_t &instr, source_t src, double lotSize) : m_instrument(instr), m_source(src), m_lotSize(lotSize) {}
    ShfeTickVWAPPSpec(const ShfeTickVWAPPSpec &a, const boost::optional<InstrSubst> &instrSubst);

    virtual instrument_t getInstrument() const { return m_instrument; }
    virtual IPriceProviderPtr build(PriceProviderBuilder *builder) const;
    virtual ShfeTickVWAPPSpec *clone(const boost::optional<InstrSubst> &instrSubst = boost::none) const;
    virtual void hashCombine(size_t &result) const;
    virtual bool compare(const IPxProviderSpec *other) const;
    virtual void print(std::ostream &o, const LuaPrintSettings &pset) const;
    virtual void checkValid() const;
    virtual void getDataRequirements(IDataRequirements *rqs) const;

    instrument_t    m_instrument;
    source_t        m_source;
    double          m_lotSize;
};
BB_DECLARE_SHARED_PTR( ShfeTickVWAPPSpec );

//
// An adapter which turns a ShfeTickProvider into a IPriceSizeProvider with VWAP
//
class ShfeTickBasedVWAPSizeProvider
    : public PriceSizeProviderImpl
    , protected ITickListener
{
public:
    enum side_filter_t
    {
        BID_ONLY,
        ASK_ONLY,
        BOTH_SIDES
    };

    ShfeTickBasedVWAPSizeProvider( ShfeTickProviderPtr spSTP, side_filter_t side, double lotSize );
    virtual ~ShfeTickBasedVWAPSizeProvider();

    /// Returns the last VWAP and size as the "reference PriceSize".
    /// Returns PriceSize() if !isLastTickOK, and sets pSuccess to
    /// false. Otherwise returns a price and sets pSuccess true.
    virtual PriceSize getRefPriceSize( bool* pSuccess = NULL ) const;

    /// Returns true if getRefPriceSize would return a valid price if asked at current moment.
    virtual bool isPriceSizeOK() const
    {   return m_spShfeTickProvider->isLastTickOK(); }

    /// Returns the ShfeTickProvider's instrument_t.
    virtual instrument_t getInstrument() const
    {   return m_spShfeTickProvider->getInstrument(); }

    /// Returns the timeval at which this PriceSizeProvider last changed.
    virtual timeval_t getLastChangeTime() const
    {   return m_spShfeTickProvider->getLastMsgTimestamp(); }

protected:
    virtual void onTickReceived( const ITickProvider* tp, const TradeTick& tick );
    virtual void onTickVolumeUpdated( const ITickProvider* tp, uint64_t totalVolume ) {}

protected:
    ShfeTickProviderPtr m_spShfeTickProvider;
    TradeTick           m_notifiedTick;
    side_filter_t       m_side;
    double              m_lotSize;
};
BB_DECLARE_SHARED_PTR( ShfeTickBasedVWAPSizeProvider );

//
/// PxSzPSpec corresponding to a ShfeTickBasedVWAPSizeProvider
//
class
ShfeTickVWAPSzPSpec : public IPxSzProviderSpec
{
public:
    BB_DECLARE_SCRIPTING();

    ShfeTickVWAPSzPSpec() : m_side(ShfeTickBasedVWAPSizeProvider::BOTH_SIDES) {}

    virtual instrument_t getInstrument() const { return m_instrument; }
    virtual IPriceSizeProviderPtr build(PriceProviderBuilder *builder) const;
    virtual ShfeTickVWAPSzPSpec *clone() const;
    virtual void hashCombine(size_t &result) const;
    virtual bool compare(const IPxSzProviderSpec *other) const;
    virtual void print(std::ostream &o, const LuaPrintSettings &ps) const;
    virtual void checkValid() const;
    virtual void getDataRequirements(IDataRequirements *rqs) const;

    instrument_t    m_instrument;
    source_t        m_source;
    ShfeTickBasedVWAPSizeProvider::side_filter_t m_side;
    double          m_lotSize;
};
BB_DECLARE_SHARED_PTR( ShfeTickVWAPSzPSpec );

}

#endif // BB_CLIENTCORE_SHFETICKPROVIDER_H

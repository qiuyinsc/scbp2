#ifndef BB_CLIENTCORE_ICLIENTTIMER_H
#define BB_CLIENTCORE_ICLIENTTIMER_H

/* Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved. */


#include <boost/function.hpp>

#include <bb/core/ptimefwd.h>
#include <bb/core/smart_ptr.h>
#include <bb/core/instrument.h>
#include <bb/core/Subscription.h>
#include <bb/core/SymbolContext.h>

namespace bb {
class Subscription;
class timeval_t;

class IClientTimer
{
public:
    IClientTimer();
    virtual ~IClientTimer() {}

    typedef boost::function<void()> Callback;

    /// Schedules a callback.
    /// Note that handler is copied, so be wary of values (versus pointers or references) bound to it.
    ///
    /// @param sub        will unsubscribe the callback upon destruction
    /// @param callback   to be invoked at wtv
    /// @param wtv        the time to be woken up (absolute time)
    void schedule( Subscription& sub, const Callback& callback, const timeval_t& wtv );

    /// Schedules a callback using a relative delay from now.
    /// Note that handler is copied, so be wary of values (versus pointers or references) bound to it.
    ///
    /// @param sub        will unsubscribe the callback upon destruction
    /// @param callback   to be invoked at wtv
    /// @param duration   the relative time from now to be woken up
    void scheduleFromNow( Subscription& sub, const Callback& callback, const ptime_duration_t& duration );


    ///  Periodic Caller of a callback.
    /// Note that handler is copied, so be wary of values (versus pointers or references) bound to it.
    ///
    /// @param sub                    will unsubscribe the callback upon destruction
    /// @param callback               to be invoked at every interval
    /// @param endtime                the time to stop (absolute time)
    /// @param interval               the periodicity (relative time)
    /// @param timeout_notifier       call when the timer has expired (about one interval after the end time
    //                                , not called when unsubscribed)
    void schedulePeriodic( Subscription& sub, const Callback& callback, const timeval_t& endtime,
            const ptime_duration_t& interval, const Subscription::Callback& timeout_notifier = Subscription::Callback() );

    ///  Periodic Caller of a callback.
    /// Note that handler is copied, so be wary of values (versus pointers or references) bound to it.
    ///
    /// @param sub                    will unsubscribe the callback upon destruction
    /// @param callback               to be invoked at every interval
    /// @param starttime              the time to start (absolute time of the first callback)
    /// @param endtime                the time to stop (absolute time)
    /// @param interval               the periodicity (relative time)
    /// @param timeout_notifier       call when the timer has expired (about one interval after the end time
    //                                , not called when unsubscribed)
    void schedulePeriodic( Subscription& sub, const Callback& callback, const timeval_t& starttime, const timeval_t& endtime,
            const ptime_duration_t& interval, const Subscription::Callback& timeout_notifier = Subscription::Callback() );


protected:
    // get the next symbol to use for a subscription
    instrument_t next();

private:
    virtual void scheduleImpl( Subscription& sub, const Callback& callback, const timeval_t& wtv ) = 0;
    virtual void scheduleFromNowImpl( Subscription& sub, const Callback& callback, const ptime_duration_t& duration ) = 0;
    virtual void schedulePeriodicImpl( Subscription& sub, const Callback& callback,  const timeval_t& starttime, const timeval_t& endtime,
                                       const ptime_duration_t& interval, const Subscription::Callback&) = 0;

    const ISymbolContextPtr                        m_symbolContext;
    ISymbolContext::const_iterator                 m_symbolIter;

};
BB_DECLARE_SHARED_PTR( IClientTimer );

} // namespace bb

#endif // BB_CLIENTCORE_ICLIENTTIMER_H

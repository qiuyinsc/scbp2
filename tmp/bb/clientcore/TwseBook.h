#ifndef BB_CLIENTCORE_TWSEBOOK_H
#define BB_CLIENTCORE_TWSEBOOK_H

/* Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved. */

#include <boost/unordered_map.hpp>

#include <bb/core/messages.h>
#include <bb/clientcore/ClockMonitor.h>
#include <bb/clientcore/Book.h>
#include <bb/clientcore/SourceBooks.h>

namespace bb {

class TwseBookLevelCIter;

class TwseBook : public bb::BookImpl
               , private bb::IEventDistListener
               , private bb::SourceMonitorListener
{
public:
    TwseBook( const bb::instrument_t& instr, bb::ClientContextPtr spCC, bb::source_t src,
              const std::string& desc, bb::SourceMonitorPtr spSMon );
    virtual ~TwseBook();

public:
    // IBook interface
    /// Returns true if the book is in a "valid" state.
    /// The meaning of this is book-dependent, however clients typically use it
    /// to test whether they should "trust" the book's contents.
    /// For example, getMidPrice should return a valid price if isOK is true.
    virtual bool isOK() const;

    /// Flushes the book.
    virtual void flushBook();

    /// Returns the mid-price of a book, or 0.0 if the book is not valid or empty.
    /// If one side of the book is empty, WindBook's getMidPrice will
    /// return the price of the existing side.
    virtual double getMidPrice() const;

    /// Returns the PriceSize at a specified depth and side.
    /// Returns an empty PriceSize if nothing exists at that depth or side.
    virtual PriceSize getNthSide( size_t depth, bb::side_t side ) const;

    /// Returns an object for iterating over BookLevels of a particular side.
    /// Postcondition: The returned iterator will iterate getNumLevels objects.
    /// This abstracts the underlying containers used by a book.
    virtual IBookLevelCIterPtr getBookLevelIter( bb::side_t side ) const;

    /// Returns the number of levels of a particular side of this book.
    virtual size_t getNumLevels( bb::side_t side ) const;

private:
    /// EventListener interface
    virtual void onEvent( const bb::Msg& msg );

    /// SourceMonitorListener interface
    virtual void onSMonStatusChange( bb::source_t src, bb::smon_status_t status, bb::smon_status_t old_status,
                                     const bb::ptime_duration_t& avg_lag_tv, const bb::ptime_duration_t& last_interval_tv );

private:
    void process_message( const bb::TwseBookSnapshotMsg& );
    void triggerBookChanges( const bb::Msg& );

private:
    void print_book_status() const;

private:
    typedef std::vector<bb::BookLevelPtr> BookLevels;
    typedef std::vector<int32_t>          ChangedLevels;

private:
    ClientContextPtr m_spCC;
    SourceMonitorPtr m_spSMon;
    uint32_t         m_verbose;
    EventSubPtr      m_event_sub;
    mutable bool     m_ok;
    mutable bool     m_ok_dirty;
    bool             m_source_ok;
    BookLevels       m_bid_levels;
    BookLevels       m_ask_levels;
    size_t           m_bid_size;
    size_t           m_ask_size;
    ChangedLevels    m_bid_changes;
    ChangedLevels    m_ask_changes;
};

BB_DECLARE_SHARED_PTR( TwseBook );

/// Book spec
class TwseBookSpec : public bb::SourceBookSpec
{
public:
    BB_DECLARE_SCRIPTING();

    TwseBookSpec() : SourceBookSpec() {};
    TwseBookSpec(const TwseBookSpec& other, const boost::optional<bb::InstrSubst>& instr) : SourceBookSpec(other, instr) {}
    TwseBookSpec(const bb::instrument_t& instr, bb::source_t src) : SourceBookSpec(instr, src) {}

public:
    virtual bb::IBookPtr build(bb::BookBuilder *builder) const;
    virtual void print(std::ostream& out, const bb::LuaPrintSettings& ps) const;
    virtual TwseBookSpec* clone(const boost::optional<bb::InstrSubst>& instrSubst = boost::none) const;
};

BB_DECLARE_SHARED_PTR(TwseBookSpec);

}

#endif // BB_CLIENTCORE_TWSEBOOK_H

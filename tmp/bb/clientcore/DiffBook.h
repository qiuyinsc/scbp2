#ifndef BB_CLIENTCORE_DIFFBOOK_H
#define BB_CLIENTCORE_DIFFBOOK_H

/* Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved. */


#include <bb/clientcore/Book.h>


namespace bb {

// Forward declaration
class DiffBookLevelCIter;


/// A DiffBook provides a view of a "primary" book with a "sub" book removed.
///
/// The IBookLevelListener notifications are unsupported.
/// The IBookListener notifications are supported, but the bidLevelChanged/askLevelChanged
/// will always be zero.
class DiffBook
    : public BookImpl
{
public:
    /// DiffBook constructor
    /// Book information such as getInstrument, etc., are derived from the primary book.
    DiffBook( IBookCPtr spPrimaryBook, IBookCPtr spSubBook, const std::string& desc );
    virtual ~DiffBook();

    /// Returns the "primary" book used by this DiffBook.
    IBookCPtr getPrimaryBook() const    { return m_spPrimaryBook; }

    /// Returns the "sub" book used by this DiffBook.
    IBookCPtr getSubBook() const        { return m_spSubBook; }

    /// Sets the timeval used to determine if orders match.  The default is 20 seconds.
    /// DiffBook needs to determine if two orders map to the same order.
    /// In addition to price/size/side/market, it also uses the order's time.
    /// It is considered a match if (primaryBookOrder.tv - subBookOrder.tv) > tvMatching.
    void setTimevalForMatching( const timeval_t& tvMatching );

    /// Returns the timeval used to determine if orders match.  The default is 20 seconds.
    /// DiffBook needs to determine if two orders map to the same order.
    /// In addition to price/size/side/market, it also uses the order's time.
    /// It is considered a match if (primaryBookOrder.tv - subBookOrder.tv) > tvMatching.
    timeval_t getTimevalForMatching() const;

    /// Returns the value of spPrimaryBook->getInstrument().
    virtual instrument_t getInstrument() const      { return m_spPrimaryBook->getInstrument(); }

    /// Returns the value of spPrimaryBook->getSource().
    virtual source_t     getSource() const          { return m_spPrimaryBook->getSource(); }

    /// Returns true if the book is in a "valid" state.
    /// A DiffBook is valid if both its primary and sub books are valid.
    virtual bool isOK() const
    {   return m_spPrimaryBook->isOK(); }

    /// Since the DiffBook's internal books are held "const", this method does nothing.
    /// If you want to flush the component book, access them individually.
    virtual void flushBook()            {}

    /// Returns the mid-price of the DiffBook.
    virtual double getMidPrice() const;

    /// Returns the PriceSize at a specified depth and side.
    /// Returns an empty PriceSize if nothing exists at that depth or side.
    virtual PriceSize getNthSide( size_t depth, side_t side ) const;

    /// Returns an object for iterating over BookLevels of a particular side.
    /// Postcondition: The returned iterator will iterate getNumLevels objects.
    /// This abstracts the underlying containers used by a book; book implementations
    /// will implement their own iterator and pass them back.
    virtual IBookLevelCIterPtr getBookLevelIter( side_t side ) const;

    /// Returns the number of levels of a particular side of this book.
    /// Note that empty levels above the "bottom" level are counted.
    virtual size_t getNumLevels( side_t side ) const;

protected:
    IBookCPtr   m_spPrimaryBook, m_spSubBook;
    timeval_t   m_tvMatching;

    BB_FWD_DECLARE_SHARED_PTR(DiffBookImpl);
    friend class DiffBookImpl;
    DiffBookImplPtr m_spImpl;

    friend class DiffBookLevelCIter;
};

BB_DECLARE_SHARED_PTR( DiffBook );


/// An iterator for DiffBook.
/// Represents the book level (primaryIter \ subBookIter), where (\) is the difference operation.
/// If aggregate is true, the size at each level will be subtracted.
/// Otherwise, the order list will be searched for matching orders (where their times must match
/// to within matchTimeval). This takes time O(len(primary_order_list) * len(sub_order_list)).
class DiffBookLevelCIter : public IBookLevelCIter
{
public:
    DiffBookLevelCIter( side_t side, const IBookLevelCIterPtr &primaryIter,
            const IBookLevelCIterPtr &subBookIter, const timeval_t &matchTimeval, bool aggregate );
    virtual ~DiffBookLevelCIter() {}

    /// Returns true if there is another item in the sequence.
    /// IBookLevelCIter::next will return a valid value if this is true.
    /// IBookLevelCIter::next will return an invalid value if this is false.
    virtual bool hasNext() const;

    /// Returns the next BookLevel from the iterator and increments the iterator.
    /// Returns an empty smart-pointer if the iteration is finished.
    virtual BookLevelCPtr next();

protected:
    /// Generates the BookLevel that will be returned by next().
    /// This is a const method, but modifies the mutable m_spNextBL.
    void calcNext() const;

private:
    side_t              m_side;
    IBookLevelCIterPtr  m_primaryIter;    // the primary book's iterator we are wrapping...
    IBookLevelCIterPtr  m_subBookIter;    // the subbook's iterator we are wrapping...
    timeval_t           m_matchTimeval;
    bool                m_aggregate;

    mutable bool          m_calcNextDirty;
    mutable BookLevelCPtr m_spSubBookCur;
    mutable BookLevelCPtr m_spNextBL;
};


/// BookSpec for DiffBook
class DiffBookSpec : public IBookSpec
{
public:
    BB_DECLARE_SCRIPTING();

    DiffBookSpec() {}
    DiffBookSpec(const DiffBookSpec &e, const boost::optional<InstrSubst> &instrSubst);

    virtual IBookPtr build(BookBuilder *builder) const;
    virtual void checkValid() const;
    virtual void hashCombine(size_t &result) const;
    virtual bool compare(const IBookSpec *other) const;
    virtual void print(std::ostream &o, const LuaPrintSettings &ps) const;
    virtual DiffBookSpec *clone(const boost::optional<InstrSubst> &instrSubst = boost::none) const;
    virtual void getDataRequirements(IDataRequirements *rqs) const;

    virtual const instrument_t &getInstrument() const;

    IBookSpecCPtr m_primaryBook, m_subBook;
};
BB_DECLARE_SHARED_PTR(DiffBookSpec);

} // namespace bb

#endif // BB_CLIENTCORE_DIFFBOOK_H

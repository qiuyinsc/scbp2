#ifndef BB_CLIENTCORE_CFFEXINFOTICKPROVIDER_H
#define BB_CLIENTCORE_CFFEXINFOTICKPROVIDER_H

/* Contents Copyright 2012 Athena Capital Research LLC. All Rights Reserved. */


#include <boost/optional.hpp>

#include <bb/clientcore/TickProvider.h>

namespace bb {

class CffexInfoTradeMsg;

BB_FWD_DECLARE_SHARED_PTR( MsgHandler );

class CffexInfoTickProvider
    : public TickProviderImpl
{
public:
    /// Constructs an CffexInfoTickProvider for the given instrument,
    /// receiving events from the EventDistributor.
    /// Instrument must be a stock.
    CffexInfoTickProvider( ClientContext& context, const instrument_t& instr, source_t source,
                           const std::string& desc );

    /// Destructor.
    virtual ~CffexInfoTickProvider() {}

    /// Returns true if the last obtained tick is OK.
    /// Returns false if there is no last tick.
    virtual bool isLastTickOK() const           { return m_bTickReceived; }

private:
    // Event handlers
    void onCffexInfoTradeMsg( const CffexInfoTradeMsg& msg );

private:
    bool m_bTickReceived;
    MsgHandlerPtr m_subCffexInfoTradeMsg;
};

BB_DECLARE_SHARED_PTR( CffexInfoTickProvider );

}

#endif // BB_CLIENTCORE_CFFEXINFOTICKPROVIDER_H

#ifndef BB_CLIENTCORE_CMEBOOKSERIALIZER_H
#define BB_CLIENTCORE_CMEBOOKSERIALIZER_H

/* Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved. */
#include <boost/function.hpp>

#include <bb/core/smart_ptr.h>
#include <bb/core/instrument.h>

#include <bb/core/messages.h>
#include <bb/core/CompactMsg.h>


namespace bb
{

BB_FWD_DECLARE_SHARED_PTR( ClientContext );
BB_FWD_DECLARE_SHARED_PTR( CmeBook );
BB_FWD_DECLARE_SHARED_PTR( IBookLevelCIter );
class Msg;

}

namespace cme
{

class BookSerializer
{
public:
    typedef boost::function<void( const bb::Msg& )> OnMessageCallback;

    BookSerializer( bb::ClientContextPtr pContext, bb::instrument_t instr );
    BookSerializer( bb::CmeBookPtr book );

    void serialize( OnMessageCallback msgHandler );

private:
    void serialize( OnMessageCallback msgHandler, bb::IBookLevelCIterPtr pLevels, bool implied = false );
    void initHeader();

    bb::CmeBookPtr m_book;
    bb::CompactMsg<bb::CmeFastDepthMsg> m_msgBuff;
};

}



#endif // BB_CLIENTCORE_CMEBOOKSERIALIZER_H


#ifndef BB_CLIENTCORE_CLIENTIDLEMANAGER_H
#define BB_CLIENTCORE_CLIENTIDLEMANAGER_H

/* Contents Copyright 2016 Athena Capital Research LLC. All Rights Reserved. */

#include <bb/core/IdleManager.h>

#include <bb/clientcore/ClientContext.h>

namespace bb{

BB_FWD_DECLARE_SHARED_PTR( MsgHandler );

class ClientIdleManager
    : public IdleManager
{
public:
    ClientIdleManager( ClientContext& cc );

    // Allow the objects to perform
    // maintenance tasks when an OnIdle message is received.
    void enable();

protected:
    ClientContext&          m_cc;
    bb::MsgHandlerPtr       m_idleMsgHandler;
};

BB_DECLARE_SHARED_PTR( ClientIdleManager );

// This subclass is for idleListeners who want to be processed
// only once during once EventDist event.
// To get onIdle callbacks, the subclass should implement onIdle_impl
class OnIdleOncePerEventListener
    : public OnIdleListener
{
public:
    OnIdleOncePerEventListener( EventDistributorPtr ed )
        : m_lastEventCount( 0 )
        , m_spED( ed )
    {}

    bool onIdle()
    {
        if( m_lastEventCount != m_spED->getTotalMessageCount() )
        {
            // Update the member eventCount to the current
            // eventCount from the EventDist.
            serviced();
            return onIdle_impl();
        }
        return false;
    }

protected:
    virtual bool onIdle_impl() = 0;

    // Updates the lastEventCount to current eventCount
    // from the EventDist.
    void serviced()
    {
        m_lastEventCount = m_spED->getTotalMessageCount();
    }

    uint64_t                 m_lastEventCount;
    EventDistributorPtr      m_spED;

};

} //end namespace bb

#endif // BB_CLIENTCORE_CLIENTIDLEMANAGER_H

#ifndef BB_CLIENTCORE_CQGBOOK_H
#define BB_CLIENTCORE_CQGBOOK_H

/* Contents Copyright 2015 Athena Capital Research LLC. All Rights Reserved. */
#include <bb/core/consts/cqg_consts.h>

#include <bb/clientcore/IncrementalBook.h>

namespace bb {
template<>
struct IncrementalBookTraits<bb::SRC_CQG>
{
    static std::vector<mtype_t> getSubscribeMType() {
        return boost::assign::list_of
            (bb::MSG_CQG_BOOK_UPDATE)
            (bb::MSG_CQG_TRADE)
            (bb::MSG_CQG_STATISTICS)
            (bb::MSG_CQG_FLUSH_BOOK)
            ;
    };

    // message Types
    typedef CqgBaseMsg          BaseMsg;
    typedef CqgBookUpdateMsg    BookUpdateMsg;
    typedef CqgFlushBookMsg     BookFlushMsg;
    typedef CqgSnapshotMsg      SnapshotMsg;

    // Action Types
    static const uint32_t  AddAction;
    static const uint32_t  ModifyAction;
    static const uint32_t  DeleteAction;
};
typedef IncrementalBookTraits<bb::SRC_CQG>  CqgBookTraits;

const uint32_t CqgBookTraits::AddAction    = cqg::BA_NEW;
const uint32_t CqgBookTraits::ModifyAction = cqg::BA_MODIFY;
const uint32_t CqgBookTraits::DeleteAction = cqg::BA_DELETE;

typedef IncrementalBook<CqgBookTraits>      CQGIncBook;

class CqgBook
    : public CQGIncBook
{
public:
    enum BookView { kCombined, kImplied };

    /// Default view is merged.
    CqgBook( const instrument_t& instr, ClientContextPtr spCC, source_t src,
             const std::string& strDesc, SourceMonitorPtr spSMon, BookView view = kCombined, size_t holeCheckDepth = 2)
        : CQGIncBook( instr, spCC, src, strDesc, spSMon, getIncView( view), holeCheckDepth )
    {}
    virtual ~CqgBook(){};

    /// EventListener interface
    virtual void onEvent( const Msg& msg ) {
        CQGIncBook::onEvent( msg );
    };

    //set the view
    void setView( enum BookView view )
    {
        CQGIncBook::setView( getIncView( view ) );
    }

    BookView getView()
    {
        return getCqgView( CQGIncBook::getView( ) );
    }

protected:

    // The CQG feed only provides a merged book and an implied book
    // so merging the two book does not make sense.  So if a combined
    // book is desired, set the underlying type to OUTRIGHT this way
    // the implied levels will not be merged (again) into the combined
    // levels
    enum EView getIncView( enum BookView view ){
        return (kCombined == view) ? CQGIncBook::OUTRIGHT : CQGIncBook::IMPLIED;
    }
    enum BookView getCqgView( enum EView view ){
        return (CQGIncBook::OUTRIGHT == view) ? kCombined : kImplied ;
    }

    virtual bool handleMessage( const Msg& msg )
    {
        bool handled = true;
        BB_MESSAGE_SWITCH_BEGIN( &msg )
        BB_MESSAGE_CASE( CqgFlushBookMsg, pFlush )
            ++m_bookUpdateMsgCnt;
            m_lastChangeTv = pFlush->hdr->time_sent;
            flushBook();
        BB_MESSAGE_CASE_END()
        BB_MESSAGE_CASE_DISCARD( CqgTradeMsg )
        BB_MESSAGE_CASE_DISCARD( CqgStatisticsMsg )
        BB_MESSAGE_CASE_DEFAULT(  )
            handled = false;
        BB_MESSAGE_CASE_END()
        BB_MESSAGE_SWITCH_END()
        return handled;
    }

    virtual bool handleOtherActions( BookLevelVec& levels, uint32_t action, const Msg& msg)
    {
        const CqgBookUpdateMsg* updateMsg = static_cast<const CqgBookUpdateMsg*>(&msg);
        bool handled = false;
        if( action == cqg::BA_OVERLAY )
        {
            modifyAction(levels, action, *updateMsg, false );
            handled = true;
        }
        return handled;
    };
    virtual bool handleOtherActionNotify( BookLevel* level, uint32_t action) {
        bool handled = false;
        if( action == cqg::BA_OVERLAY )
        {
            if( m_isAdd )
            {
                notifyBookLevelAdded( level );
            }
            else
            {
                notifyBookLevelModified( level );
            }
            handled = true;
        }
        return handled;
    };
};

} // end namespace bb
#endif // BB_CLIENTCORE_CQGBOOK_H

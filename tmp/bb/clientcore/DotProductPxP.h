#ifndef BB_CLIENTCORE_DOTPRODUCTPXP_H
#define BB_CLIENTCORE_DOTPRODUCTPXP_H

/* Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved. */


#include <bb/clientcore/PriceProvider.h>
#include <bb/clientcore/PriceProviderSpec.h>

namespace bb {

/**
 * An DotProductPxP holds a set of components and a weight for each one.
 * These components are IPriceProviders. The weights are simple constants.
 *
 * The reported price of a DotProductPxP is weighted sum of all its components' prices.
 **/
class DotProductPxP : public PriceProviderImpl
{
public:
    class Component {
    public:
        Component( IPriceProviderCPtr spPP, double weight, const Subscription &spPPSub)
            : m_spPP( spPP ), m_spPPSub(spPPSub), m_weight( weight ) {}
        const IPriceProviderCPtr &getPriceProvider() const   { return m_spPP; }
        double                    getWeight() const          { return m_weight; }
        void                      setWeight( double w )      { m_weight = w; }
    private:
        IPriceProviderCPtr  m_spPP;
        Subscription  m_spPPSub;
        double              m_weight;
    };
    typedef std::vector<Component>        ComponentList;

    /// Creates an DotProductPxP with the specified identifying instrument.
    /// The DotProductPxP is constructed with no components. begins empty.
    DotProductPxP( const instrument_t& instr,bool updatealways=false );

    /// Adds a component to the DotProductPxP. The same PriceProvider can be added multiple times.
    virtual void addComponent( IPriceProviderCPtr const& spPP, double weight );

    /// Removes a component to the DotProductPxP, by specifying the added IPriceProvider.
    /// Throws if the PxP isn't found
    void removeComponent( IPriceProviderCPtr const& spPP );

    /// Returns this DotProductPxP's list of components.
    const ComponentList& getComponents() const { return m_comps; }

    double getRefPrice(bool *pSuccess = NULL) const;

    virtual bool isPriceOK() const;

    instrument_t getInstrument() const { return m_instr; }
    timeval_t getLastChangeTime() const { return m_last_change_tv; }
    friend std::ostream& operator<<(std::ostream&,DotProductPxP const&);
protected:
    // IPriceProvider Listener
    void onComponentPriceChanged( const IPriceProvider& pp );

    /// updates all the index's internal values
    /// TODO: eventually we'll do smarter caching... for now it's just a dirty bit
    /// we need to look at performance and usage first
    virtual void _update() const;

private:
    instrument_t        m_instr;
    mutable bool        m_ok;
    mutable bool        m_dirty;
    mutable double      m_value;
    timeval_t           m_last_change_tv;
    ComponentList       m_comps;
    bool                m_updatealways;
};
BB_DECLARE_SHARED_PTR( DotProductPxP );

/// PxPSpec for building a DotProductPxP
class DotProductPxPSpec : public IPxProviderSpec
{
public:
    BB_DECLARE_SCRIPTING();

    struct Component
    {
        Component() : m_weight(0.0) {}
        Component(const IPxProviderSpecPtr &pxp, double w) : m_pxp(pxp), m_weight(w) {}
        IPxProviderSpecPtr m_pxp;
        double m_weight;
    };

    DotProductPxPSpec();
    DotProductPxPSpec( const DotProductPxPSpec& a, const boost::optional<InstrSubst> &instrSubst );

    virtual instrument_t getInstrument() const { return m_instrument; }
    virtual IPriceProviderPtr build(PriceProviderBuilder *builder) const;
    virtual DotProductPxPSpec *clone(const boost::optional<InstrSubst> &instrSubst = boost::none) const;
    virtual void hashCombine(size_t &result) const;
    virtual bool compare(const IPxProviderSpec *other) const;
    virtual void print(std::ostream &o, const LuaPrintSettings &pset) const;
    virtual void checkValid() const;
    virtual void getDataRequirements(IDataRequirements *rqs) const;

    instrument_t m_instrument;
    std::vector<Component> m_components;

};
BB_DECLARE_SHARED_PTR( DotProductPxPSpec );

} // namespace bb

#endif // BB_CLIENTCORE_DOTPRODUCTPXP_H

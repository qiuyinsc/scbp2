#ifndef BB_CLIENTCORE_MULTIPATHTICK_H
#define BB_CLIENTCORE_MULTIPATHTICK_H

/* Contents Copyright 2013 Athena Capital Research LLC. All Rights Reserved. */


#include <vector>
#include <bb/clientcore/TickProvider.h>


namespace bb {

class MultipathTickProvider
    : public TickProviderImpl
    , protected ITickListener
{
public:
    MultipathTickProvider( const instrument_t& instr, const std::string& desc );
    virtual ~MultipathTickProvider();

    /// Adds an ITickProvider to this MultipathTickProvider.
    /// Returns true if successful, or false if the ITickProvider is already monitored.
    bool addTickProvider( ITickProviderPtr spTP );

    /// Removes an ITickProvider to this MultipathTickProvider.
    /// Returns true if successful, or false if the ITickProvider was not found.
    bool removeTickProvider( ITickProviderPtr spTP );

    /// Returns true if the last obtained tick is OK.
    /// Returns false if there is no last tick.
    virtual bool isLastTickOK() const
    {   return m_ok; }

protected:
    /// Update our totals and forward tick to our listeners.
    virtual void onTickReceived( const ITickProvider* tp, const TradeTick& tick );

protected:
    std::vector<ITickProviderPtr>   m_tickProviders;

    bool                            m_ok;
};
BB_DECLARE_SHARED_PTR( MultipathTickProvider );


} // namespace bb

#endif // BB_CLIENTCORE_MULTIPATHTICK_H

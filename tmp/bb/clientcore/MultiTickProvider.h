#ifndef BB_CLIENTCORE_MULTITICKPROVIDER_H
#define BB_CLIENTCORE_MULTITICKPROVIDER_H

/* Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved. */


#include <boost/scoped_ptr.hpp>
#include <boost/ptr_container/ptr_map.hpp>
#include <bb/core/smart_ptr.h>
#include <bb/core/ListenNotify.h>
#include <bb/clientcore/ClientContext.h>
#include <bb/clientcore/TickProvider.h>


namespace bb {

/// A MultiTickProvider is optimized to handle many ticks simultaneously.
/// It is useful for things like keeping track of all ticks in a market.
// TODO: this does NOT work for futures
class MultiTickProvider
{
    BB_DECLARE_LISTENER_2( MultiTickProvider, ITickProviderCPtr );

public:
    /// Constructs the MultiTickProvider with the given ClientContext.
    MultiTickProvider( ClientContextPtr spContext );
    /// Destructor
    virtual ~MultiTickProvider();

    /// Returns the ITickProvider for the given source and instrument.
    /// Returns ITickProviderPtr() if the source has not been added (see MultiTickProvider::addSource)
    ITickProviderPtr getTickProvider( source_t src, const instrument_t& instr );

    /// Returns the IPriceProvider for the given source and instrument.
    /// Returns IPriceProviderPtr() if the source has not been added (see MultiTickProvider::addSource)
    IPriceProviderPtr getPriceProvider( source_t src, const instrument_t& instr );

    /// Returns the IPriceSizeProvider for the given source and instrument.
    /// Returns IPriceSizeProviderPtr() if the source has not been added (see MultiTickProvider::addSource)
    IPriceSizeProviderPtr getPriceSizeProvider( source_t src, const instrument_t& instr );

    /// Adds the given source to be monitored.  Has no effect if the source is
    /// already added.  Sets the boolean that determines whether a tick summary
    /// will be dumped at the end.
    /// Throws if the source cannot be added.
    void addSource( source_t src );
    void addSource( source_t src, bool dumpSummary );
    /// For histo mode, addSource doesn't work great because we can't load all ticks.
    /// This variant will load the ticks for the given instruments only.
    void addSourceHisto( source_t src, const std::vector<instrument_t> &instrs );
    void addSourceHisto( source_t src, const std::vector<instrument_t> &instrs, bool dumpSummary );

    /// Removes the given source from monitoring.
    /// Returns true if successful, false if the source does not exist.
    bool removeSource( source_t src );

    /// Adds the given MultiTickProvider::Listener to this MultiTickProvider.
    /// Subscription will be initialized with a new subscription to the object.
    /// When the Subscription is released, the listener is unsubscribed.
    /// The Listener signature is void(*)(const MultiTickProvider&, ITickProviderCPtr );
    void addListener( Subscription &outListenerSub, const Listener& listener ) const
    {    m_notifier.subscribeListener( outListenerSub, listener ); }


private:
    ClientContextPtr    m_spContext;

    class SourceSlice;
    friend class SourceSlice;
    class ProviderAdapter;
    BB_DECLARE_SHARED_PTR( ProviderAdapter );
    typedef boost::ptr_map<source_t, MultiTickProvider::SourceSlice> SourceSliceMap;
    SourceSliceMap  m_sourceSliceMap;

    typedef TNotifier2<MultiTickProvider, ITickProviderCPtr> MultiTickNotifier;
    mutable MultiTickNotifier m_notifier;

public:
#ifndef SWIG
    class TickProviderGenerator
    {
    public:
        TickProviderGenerator( const TickProviderGenerator& g );
        ITickProviderPtr operator() ();
    private:
        TickProviderGenerator( const SourceSlice& slice );
        TickProviderGenerator();
        class Impl;
        friend class MultiTickProvider;
        friend class MultiTickProvider::SourceSlice;
        boost::shared_ptr<Impl> m_impl;
        bool m_bfirstTime;
    };
    TickProviderGenerator getTickProviderGenerator( source_t src );
#endif // SWIG
    void getSources( std::vector<source_t>* srcs );
};
BB_DECLARE_SHARED_PTR( MultiTickProvider );


} // namespace bb

#endif // BB_CLIENTCORE_MULTITICKPROVIDER_H

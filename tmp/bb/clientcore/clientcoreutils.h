#ifndef BB_CLIENTCORE_CLIENTCOREUTILS_H
#define BB_CLIENTCORE_CLIENTCOREUTILS_H

/* Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved. */


#include <bb/core/timeval.h>
#include <bb/core/instrument.h>
#include <bb/core/mktdest.h>
#include <bb/core/source.h>
#include <bb/core/smart_ptr.h>

struct lua_State;

namespace bb {

BB_FWD_DECLARE_SHARED_PTR( ClientContext );

/// Returns the expiry to used for sym given a ref-date
/// Uses DateProvider object to determine the appropriate future contract
/// simple use: refdate = today
/// historical use: refdate = historical date (acr date)
expiry_t get_expiry_for_date( symbol_t sym, const date_t& acr_date );

/// Returns the fully specified source_t to be used for feedtype (live version).
/// Returns the fully specified source_t to be used for feedtype (hist version).
/// Currently sets Orig and Dest for feedtypes based on
/// HistMStreamManager::runtimeFeedDest and start date.
source_t feed_to_source( EFeedType feedtype, bool brun_live, const date_t& refdate );

mktdest_t primary_mktdest( const ClientContextWeakPtr&, const instrument_t& );

source_t primary_source_from_instr( const ClientContextWeakPtr&, const instrument_t& );

/// Converts an EFeedDest to the EFeedOrig for the same site.
/// For example, DEST_CARTERET yields ORIG_CARTERET.
EFeedOrig orig_from_dest( EFeedDest feed_dest );

/// Returns the source which will be on the messages coming from the trade daemon,
/// based on the feed_dest given.
source_t get_td_source_for_dest( EFeedDest feed_dest );

/// Returns the source which will be on the position update messages.
/// based on the feed_dest given.
source_t get_margin_source_for_dest( EFeedDest feed_dest );

/// Global ClientCore configurations are set up in a "Setup" table in Lua
/// this call makes sure that happens, it may be called multiple times, but will initialize
/// only once.
void init_client_scripting_setup( lua_State* state
    , bool run_live
    , const date_t& sdate
    , const date_t& edate );


/// Multiply this value by shares and current price of contract to get value in currency of your choosing
/// Calculated by multiplying contract-size * pricing-unit, and then multilpying by the exchange rate of
/// currency specified
double instrument_value_multiplier( const ClientContextPtr& clientContext, const instrument_t& instr, currency_t currency = currency_t::USD );


} // namespace bb

#endif // BB_CLIENTCORE_CLIENTCOREUTILS_H

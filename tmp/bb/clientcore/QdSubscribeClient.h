#ifndef BB_CLIENTCORE_QDSUBSCRIBECLIENT_H
#define BB_CLIENTCORE_QDSUBSCRIBECLIENT_H

/* Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved. */


#include <bb/core/Error.h>
#include <bb/core/mtype.h>
#include <bb/core/instrument.h>
#include <bb/core/smart_ptr.h>

namespace bb {

BB_FWD_DECLARE_SHARED_PTR( ISendTransport );

/** Client side interface to send a subscription
 *  MSG_NONE and MSG_ALL not allowed.  Methods will throw InvalidMtype();
 */
class QdSubscribeClient
{
public:
    QdSubscribeClient( ISendTransportPtr sender, const std::string& id );
    ~QdSubscribeClient();

    class InvalidMtype : public Error
    {
    public:
        InvalidMtype( mtype_t mtype )
            : Error( "invalid mtype used with QdSubscribeClient: %s", mtype2str( mtype ) )
        {}
    };

    void sendSubscribe( mtype_t mtype, const instrument_t& instr );
    void sendUnsubscribe( mtype_t mtype, const instrument_t& instr );

protected:
    ISendTransportPtr m_spSendTransport;
};

BB_DECLARE_SHARED_PTR( QdSubscribeClient );

} // namespace bb

#endif // BB_CLIENTCORE_QDSUBSCRIBECLIENT_H

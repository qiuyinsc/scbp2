#ifndef BB_CLIENTCORE_SFITTDTICKPROVIDER_H
#define BB_CLIENTCORE_SFITTDTICKPROVIDER_H

/* Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved. */

#include <bb/core/messages.h>
#include <bb/clientcore/TickProvider.h>

namespace bb {

class TdFillFutureMsg;

BB_FWD_DECLARE_SHARED_PTR( MsgHandler );

class SfittdTickProvider
    : public TickProviderImpl
{
public:
    /// Constructs an SfittdTickProvider for the given instrument,
    /// receiving events from the EventDistributor.
    /// Instrument must be a stock.
    SfittdTickProvider( ClientContext& context, const instrument_t& instr, source_t source,
                      const std::string& desc );

    /// Destructor.
    virtual ~SfittdTickProvider() {}

    /// Returns true if the last obtained tick is OK.
    /// Returns false if there is no last tick.
    virtual bool isLastTickOK() const           { return m_bTickReceived; }

private:
    // Event handlers
    void onTdFillFutureMsg( const TdFillFutureMsg& msg );

private:
    bool m_bTickReceived;

    MsgHandlerPtr m_subTdFillFutureMsg;
};

BB_DECLARE_SHARED_PTR( SfittdTickProvider );

}

#endif // BB_CLIENTCORE_SFITTDTICKPROVIDER_H

#ifndef BB_CLIENTCORE_IEVENTDISTLISTENER_H
#define BB_CLIENTCORE_IEVENTDISTLISTENER_H

/* Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved. */


#include <bb/core/smart_ptr.h>
#include <bb/core/Subscription.h>


namespace bb {

BB_FWD_DECLARE_SHARED_PTR(Msg);
BB_FWD_DECLARE_SHARED_PTR(EventDistributor);

/// Inteface for EventDistributor listeners.
/// Clients implement this interface and register themselves with EventDistributor.
class IEventDistListener
{
public:
    virtual ~IEventDistListener() {}
    /// Invoked when a message the receiver is interested in arrives.
    /// The msg object is only guaranteed to be valid during the invocation of this method.
    /// Therefore any data should be copied from it.
    virtual void onEvent( const Msg& msg ) = 0;
};
BB_DECLARE_SHARED_PTR(IEventDistListener);

struct NullEventDistListener : public IEventDistListener { virtual void onEvent(const Msg&) {} };


typedef Subscription EventSubPtr;

} // namespace bb

#endif // BB_CLIENTCORE_IEVENTDISTLISTENER_H

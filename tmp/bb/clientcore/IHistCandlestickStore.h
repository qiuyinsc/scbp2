#ifndef BB_CLIENTCORE_IHISTCANDLESTICKSTORE_H
#define BB_CLIENTCORE_IHISTCANDLESTICKSTORE_H

/* Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved. */


#include <boost/shared_ptr.hpp>
#include <bb/core/instrument.h>
#include <bb/core/source.h>

namespace bb {

class ICandlestickSeries;

class IHistCandlestickStore
{
public:
    virtual ~IHistCandlestickStore() {}
    virtual boost::shared_ptr<ICandlestickSeries> getInstrument( const bb::instrument_t& instr,
                                                                 const bb::source_t& alt_src = bb::source_t() ) = 0;
};

BB_DECLARE_SHARED_PTR( IHistCandlestickStore );

} // namespace bb

#endif // BB_CLIENTCORE_IHISTCANDLESTICKSTORE_H

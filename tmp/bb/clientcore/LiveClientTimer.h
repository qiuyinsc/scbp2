#ifndef BB_CLIENTCORE_LIVECLIENTTIMER_H
#define BB_CLIENTCORE_LIVECLIENTTIMER_H

/* Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved. */

#include <bb/core/Timer.h>
#include <bb/core/Subscription.h>
#include <bb/clientcore/IClientTimer.h>
#include <bb/clientcore/IEventDistListener.h>

namespace bb {
class FDSet;
BB_FWD_DECLARE_SHARED_PTR( EventDistributor );
BB_FWD_DECLARE_SHARED_PTR( TimeoutMsg );
BB_FWD_DECLARE_SHARED_PTR( ISymbolContext );


class LiveClientTimer
    : public IClientTimer
{
public:
    LiveClientTimer( FDSet& fdSet, const EventDistributorPtr& distributor );

protected:
    typedef boost::function<void()> Callback;

private:
    virtual void scheduleImpl( Subscription& sub, const Callback& callback, const timeval_t& wtv );
    virtual void scheduleFromNowImpl( Subscription& sub, const Callback& callback, const ptime_duration_t& duration );
    virtual void schedulePeriodicImpl( Subscription& sub, const Callback& callback, const timeval_t& starttime,
                                       const timeval_t& endtime, const ptime_duration_t& interval,
                                       const Subscription::Callback& done_callback);


    FDSet&                                         m_fdSet;
    EventDistributorPtr                            m_distributor;

};
BB_DECLARE_SHARED_PTR( LiveClientTimer );

class TimerSubscriptionBase : public SubscriptionBase, public IEventDistListener
{
public:
    enum timeoutType { SINGLE_TIMER_TIMEOUT, PERIODIC_TIMER_TIMEOUT, TIMER_DONE };
    typedef boost::function<void()> Callback;


    TimerSubscriptionBase( FDSet& fdSet, const EventDistributorPtr& distributor, const instrument_t& instr, const timeval_t& wtv, const Callback& cb );
    TimerSubscriptionBase( FDSet& fdSet, const EventDistributorPtr& distributor, const instrument_t& instr, const timeval_t& starttv,
                           const timeval_t& end, const ptime_duration_t& interval,
                           const Callback& cb, const Subscription::Callback& donecb );

    // NOTE: that a use case is that the owner can unsubscribe to the timer while inside a callback
    // meaning that "this" may no longer be around after the callback and therefore invalid
    virtual void onEvent( const Msg& msg );

    virtual bool isValid() const { return (bool) m_timer; }
    unsigned int numOverruns() const
    {
        BB_ASSERT ( m_overrunCnt.get() != nullptr );
        return m_overrunCnt.get() != nullptr ? m_overrunCnt->overruns : 0;
    }

private:
    void initialize();
    void timerSingleCallback();
    void timerPeriodicCallback();

    OverrunCounterPtr         m_overrunCnt;
    TimerPtr                  m_timer;
    EventDistributorPtr       m_distributor;
    Subscription              m_eventSubs;
    TimeoutMsgPtr             m_msg;
    instrument_t              m_instr;
    timeval_t                 m_end;
    timeval_t                 m_scheduledWakeupTv;
    ptime_duration_t          m_interval;
    Callback                  m_timerPopCB;
    Callback                  m_timerDoneCB;
};


} // namespace bb

#endif // BB_CLIENTCORE_LIVECLIENTTIMER_H

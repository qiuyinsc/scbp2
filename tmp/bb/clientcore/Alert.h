#ifndef BB_CLIENTCORE_ALERT_H
#define BB_CLIENTCORE_ALERT_H

/* Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved. */


#include <string>
#include <cstdarg>
#include <bb/core/timeval.h>
#include <bb/core/hash_map.h>
#include <bb/core/loglevel.h>
#include <bb/core/smart_ptr.h>

namespace bb {

// Forward declarations
class ClockMonitor;
BB_FWD_DECLARE_SHARED_PTR( LiveAlertTransport );

/// Interface for raising alert error messages to a 3rd party.
class IAlert
{
public:
    virtual ~IAlert() {}

    /// Sends an alert. Format does not need to have a newline
    bool sendAlert(loglevel_t warn_level, const char* format, ...);
    bool sendAlert(loglevel_t warn_level, const std::string& alertmsg)
        { return sendAlert(warn_level, alertmsg.c_str()); }

protected:
    virtual bool vsendAlert(loglevel_t warn_level, const char* format, va_list &args) = 0;
};
BB_DECLARE_SHARED_PTR( IAlert );


/// Sends alert messages to the BB logger with rate limiting.
/// Msgs with the same format will be printed twice per second at the fastest.
///
/// Also overrides core/Log.h's identity string.
/// 
/// Subclasses can override sendAlertImpl, to additionally send msg to
/// other destinations (usually subject to independent rate_limits).
class Alert : public IAlert
{
public:
    Alert(ClockMonitor *cm, const std::string &idString);
    virtual ~Alert() {}

    /// sets the identity strings which will be placed on the alerts.
    /// idString identifies which program is running, and
    /// prefix identifies the instance of the program.
    /// For example, in:
    /// 13:13:10.204963 lg1 alertd :  (robotrader.dapper-err6) Warning: robotrader_ATFB_MSFT: foo bar
    /// robotrader.dapper is the ID string and robotrader_ATFB_MSFT is the prefix.
    /// Also overrides core/Log.h's identity string.
    void setIdentity(const std::string &idString, const std::string &prefix);

    const std::string& getIdentity() const { return m_identityString; }
    const std::string& getPrefixStr() const { return m_prefix; }

protected:
    virtual bool vsendAlert(loglevel_t warn_level, const char* format, va_list &args);
    virtual void sendAlertImpl(loglevel_t warn_level, const char* format, const std::string &msg) {}

    ClockMonitor* m_cm;
    std::string m_identityString, m_prefix;

    bbext::hash_map<std::string, timeval_t> m_last_localtv;
};
BB_DECLARE_SHARED_PTR( Alert );

/// Overrides sendAlertImpl to broadcast alerts to the network.
class LiveAlert : public Alert
{
public:
    LiveAlert(ClockMonitor *cm, const std::string &idString);

protected:
    virtual void sendAlertImpl(loglevel_t warn_level, const char* format, const std::string& msg);

    bbext::hash_map<std::string, timeval_t> m_last_networktv;
    LiveAlertTransportPtr m_liveTransport;
};
BB_DECLARE_SHARED_PTR( LiveAlert );

/// Alert implementation which does nothing.
class NoAlert : public IAlert
{
protected:
    virtual bool vsendAlert(loglevel_t warn_level, const char* format, va_list &args) { return true; }
};
BB_DECLARE_SHARED_PTR( NoAlert );

} // namespace bb

#endif // BB_CLIENTCORE_ALERT_H

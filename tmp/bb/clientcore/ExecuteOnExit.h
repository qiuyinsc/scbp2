#ifndef BB_CLIENTCORE_EXECUTEONEXIT_H
#define BB_CLIENTCORE_EXECUTEONEXIT_H

/* Contents Copyright 2016 Athena Capital Research LLC. All Rights Reserved. */

/*
 * Some tasks, such as re-scheduling a clock monitor are quite costly but need to happen anyway.
 * To avoid disrupting the critical path, we will schedule these tasks for after we exit the critical path.
 * To make use of this:
 * - instantiate a new 'ExecuteOnExit' object before you start the critical path
 * - keep calling 'add(Callback & )' on it for everything you want to happen at a later stage
 * - have the object go out of scope once we leave the critical path
 * At this stage, all queued up events will be fired.
 *
 */

#include <vector>

#include <boost/noncopyable.hpp>
#include <boost/function.hpp>
#include <boost/foreach.hpp>

namespace bb {

class ExecuteOnExit : private boost::noncopyable
{
    public:
        typedef boost::function<void()> Callback;
        ExecuteOnExit()
        {
        }

        void add(Callback & cb)
        {
            m_callbacks.push_back(cb);
        }

        ~ExecuteOnExit()
        {
            BOOST_FOREACH(Callback & cb, m_callbacks)
            {
                cb();
            }
        }

    private:
        typedef std::vector<Callback> CallbackVct;

        CallbackVct m_callbacks;
};

} // namespace bb

#endif // BB_CLIENTCORE_EXECUTEONEXIT_H

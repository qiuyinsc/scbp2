#ifndef BB_CLIENTCORE_ONTICKL1SNAPSHOTPROVIDER_H
#define BB_CLIENTCORE_ONTICKL1SNAPSHOTPROVIDER_H

/* Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved. */


#include <boost/ptr_container/ptr_map.hpp>

#include <bb/core/ListenNotify.h>

#include <bb/io/SendTransport.h>

#include <bb/clientcore/ClientContext.h>
#include <bb/clientcore/L1Snapshot.h>

namespace bb {

class OnTickL1SnapshotProvider
{
public:
    /// Constructs the given OnTickL1SnapshotProvider with the given ClientContextPtr
    OnTickL1SnapshotProvider(ClientContextPtr context, ISendTransportPtr trans);

    /// Destructor
    ~OnTickL1SnapshotProvider();

    void addSource(source_t src, const std::vector<instrument_t>& instrs);
    bool removeSource(source_t src);

    /// Takes in a vector of sources and appends the sources present in the
    /// current SourceSliceMap onto the vector
    void getSources(std::vector<source_t>* srcs);

private:
    ClientContextPtr m_context;
    ISendTransportPtr m_trans;

    class SourceSlice;
    friend class SourceSlice;

    typedef boost::ptr_map<source_t, OnTickL1SnapshotProvider::SourceSlice> SourceSliceMap;
    SourceSliceMap m_source_slice_map;
};

} // namespace bb

#endif // BB_CLIENTCORE_ONTICKL1SNAPSHOTPROVIDER_H

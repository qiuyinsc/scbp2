#ifndef BB_CLIENTCORE_SYMBOLSCONFIGFILE_H
#define BB_CLIENTCORE_SYMBOLSCONFIGFILE_H

/* Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved. */


#include <bb/clientcore/ConfigFile.h>

namespace bb {

/** ConfigFile with a single Target Product
    This reads a config file that has a symbols_config.target_product member
 */
class SymbolsConfigFile : public ConfigFile
{
public:
    typedef bb::ConfigFile Super;
    SymbolsConfigFile( bool runLive, const date_t& startDate, const date_t& endDate, LuaStatePtr config )
        : Super( runLive, startDate, endDate, config ) {}

    virtual void loadSettings( const luabind::object& root );

    virtual ProductEntryCPtr getTargetProduct() const;

protected:
    ProductEntryPtr m_spTargetProduct;
};

} // namespace bb

inline bb::ConfigFile::ProductEntryCPtr bb::SymbolsConfigFile::getTargetProduct() const
{
    if(!m_fileLoaded)
        throw std::runtime_error("bb::SymbolsConfigFile::getTargetProduct called before a file was loaded");
    return m_spTargetProduct;
}

#endif // BB_CLIENTCORE_SYMBOLSCONFIGFILE_H

#ifndef BB_CLIENTCORE_RANDOM_H
#define BB_CLIENTCORE_RANDOM_H

/* Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved. */


#include <bb/core/instrument.h>
#include <bb/core/source.h>

#include <boost/random/linear_congruential.hpp>
#include <boost/random/uniform_int.hpp>

namespace bb {

/// This is intended as a repeatable, order-independent source of pseudorandom values
/// (a hash function).
///
/// In other words, calling getSourceBookHash() with the same parameters
/// and the same seed always returns the same value, but calling with different parameters
/// returns different values. This way, a source book will always use the same random
/// check offset, regardless of where it's created in the program.
class RandomSource
{
public:
    RandomSource() : m_seed(1) {}
    /// sets the global seed value, to randomize the values of everything in the system.
    void setSeed(uint32_t seed) { m_seed = seed; }

    /// Returns an offset value for the source monitors status check.
    /// This'll be a random number divisible by 1000 in the range [0,1000000).
    long getSourceMonitorHash(source_t source);

    /// Returns an offset for the source book's full structure check.
    /// This'll be a pair, where the first is between 0 and checkInterval, and the second is a
    /// random number divisible by 1000 in the range [0,1000000).
    std::pair<uint32_t, uint32_t> getSourceBookHash(const bb::instrument_t &instr, source_t source, int checkInterval);

    /// Returns a seed for the master book's full structure check.
    /// This'll be a pair, where the first is between 0 and checkInterval, and the second is a
    /// random number divisible by 1000 in the range [0,1000000).
    std::pair<uint32_t, uint32_t> getMasterBookHash(const bb::instrument_t &instr,
            const std::vector<source_t> &sources, int checkInterval);



    // Low level interface:
    typedef boost::minstd_rand RandomGenerator_t;
    /// returns a seed for the RNG, based on the m_seed and a hash value.
    /// This can be used for building a RNG and generating some values yourself.
    uint32_t getRandomSeed(uint32_t hash);

protected:
    uint32_t m_seed;
};
BB_DECLARE_SHARED_PTR(RandomSource);


} // namespace bb

#endif // BB_CLIENTCORE_RANDOM_H

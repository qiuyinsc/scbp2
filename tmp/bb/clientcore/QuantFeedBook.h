#ifndef BB_CLIENTCORE_QUANTFEEDBOOK_H
#define BB_CLIENTCORE_QUANTFEEDBOOK_H

/* Contents Copyright 2015 Athena Capital Research LLC. All Rights Reserved. */

// Book for QuantFeed Market-By-Limit (MBL) data

#include <iostream>
#include <boost/optional.hpp>

#include <bb/core/instrument.h>
#include <bb/core/source.h>
#include <bb/core/messages.h>
#include <bb/clientcore/Book.h>
#include <bb/clientcore/BookLevel.h>
#include <bb/clientcore/ClientContext.h>
#include <bb/clientcore/IEventDistListener.h>
#include <bb/clientcore/SourceMonitor.h>
#include <bb/clientcore/SourceBooks.h>

namespace bb {

class QuantFeedBook : public BookImpl, public IEventDistListener,
    public SourceMonitorListener
{
public:
    QuantFeedBook(const instrument_t &instr, ClientContextPtr spCC,
            source_t src, const std::string &strDesc, SourceMonitorPtr spSMon);

    virtual ~QuantFeedBook();

    virtual bool isOK() const;

    virtual void flushBook();

    virtual double getMidPrice() const;

    virtual PriceSize getNthSide(size_t depth, side_t side) const;

    virtual IBookLevelCIterPtr getBookLevelIter(side_t side) const;

    virtual size_t getNumLevels(side_t side) const;

private:
    typedef std::vector<BookLevel> BookLevelVec;

    virtual void onEvent(const Msg &msg);

    virtual void onSMonStatusChange(source_t src, smon_status_t status,
            smon_status_t old_status, const ptime_duration_t &avg_lag_tv,
            const ptime_duration_t &last_internal_tv);

    void process(const QuantFeedMblFullRefreshMsg &msg);
    void process(const QuantFeedMblOverlapRefreshMsg &msg);
    void process(const QuantFeedMblDeltaRefreshMsg &msg);
    void process(const QuantFeedMblMaxVisibleDepthMsg &msg);

    // All the following functions return the level at which the book has
    // changed, which is to be used by notifyBookChanged(). If no level
    // changes, -1 is returned.
    int32_t overlapUpdate(bb::side_t side, int16_t indicator,
            BookLevelVec &dst, const fos_order_book_level *src, uint16_t depth);

    int32_t  clearFromLevel(BookLevelVec &depth, uint16_t lvl);
    int32_t insertAtLevel(BookLevelVec &depth, side_t side, uint16_t lvl,
            double px, uint32_t sz, uint32_t orders);
    int32_t removeLevelAndAppend(BookLevelVec &depth, uint16_t lvl, double px,
            uint32_t sz, uint32_t orders);
    int32_t removeLevel(BookLevelVec &depth, uint16_t lvl);
    int32_t updateQuantity(BookLevelVec &depth, uint16_t lvl, uint32_t sz,
            uint32_t orders);

    void applyMblDeltaRefreshUpdate(const QuantFeedMblDeltaRefreshMsg &msg,
            int32_t &bidLevel, int32_t &askLevel);

private:

    EventSubPtr m_eventSub;
    ClientContextPtr m_spCC;
    SourceMonitorPtr m_spSMon;
    mutable bool m_ok, m_okDirty;
    bool m_sourceOk;
    bool m_full_snapshot_received;
    std::vector<QuantFeedMblDeltaRefreshMsg *> m_pendingMblDeltas;
    int32_t m_bid_level_changed_by_delta;
    int32_t m_ask_level_changed_by_delta;

    uint32_t m_maxDepths;
    // TODO get rid of vector because insertion and removal are not optimal
    BookLevelVec m_bookLevels[2]; // 0: BID, 1: ASK
};

BB_DECLARE_SHARED_PTR( QuantFeedBook );

class QuantFeedBookSpec : public SourceBookSpec
{
public:
    BB_DECLARE_SCRIPTING();

    QuantFeedBookSpec() : SourceBookSpec() {}
    QuantFeedBookSpec(const bb::QuantFeedBookSpec &,
            const boost::optional<bb::InstrSubst> &);

    QuantFeedBookSpec(const instrument_t &instr, source_t src)
        : SourceBookSpec(instr, src)
    {}

    virtual IBookPtr build(BookBuilder *builder) const;

    virtual void print(std::ostream &os, const LuaPrintSettings &ps) const;
    virtual QuantFeedBookSpec *clone(
            const boost::optional<InstrSubst> &instrSubst = boost::none) const;
};

BB_DECLARE_SHARED_PTR( QuantFeedBookSpec );

}

#endif // BB_CLIENTCORE_QUANTFEEDBOOK_H


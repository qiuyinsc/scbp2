#ifndef BB_CLIENTCORE_HKFETICKPROVIDER_H
#define BB_CLIENTCORE_HKFETICKPROVIDER_H

/* Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved. */


#include <vector>

#include <boost/optional.hpp>

#include <bb/core/messages.h>
#include <bb/core/smart_ptr.h>
#include <bb/core/ListenerList.h>

#include <bb/clientcore/TickProvider.h>

namespace bb {

class HkfeTradeMsg;

BB_FWD_DECLARE_SHARED_PTR( MsgHandler );

struct HkfeOrdersInTick
{
    HkfeOrdersInTick() : remainingQty( 0 ) {}

    void reset()
    {
        remainingQty = 0;

        bidOrders.clear();
        askOrders.clear();
    }

    struct Order
    {
        double px;
        int32_t qty;
        uint16_t block_sz;
        uint16_t order_type;
    };

    int32_t remainingQty;

    std::vector<Order> bidOrders;
    std::vector<Order> askOrders;
};

class IHkfeTickWithOrdersListener
{
public:
    virtual ~IHkfeTickWithOrdersListener() {}

    virtual void onTickWithOrders( const ITickProvider *tp, const TradeTick& tick,
                                   const HkfeOrdersInTick& orders ) = 0;
};

class HkfeTickProvider
    : public TickProviderImpl
{
public:
    /// Constructs an HkfeTickProvider for the given instrument,
    /// receiving events from the EventDistributor.
    HkfeTickProvider( ClientContext& context, const instrument_t& instr, source_t source,
                       const std::string& desc );

    /// Destructor.
    virtual ~HkfeTickProvider() {}

    /// Returns true if the last obtained tick is OK.
    /// Returns false if there is no last tick.
    virtual bool isLastTickOK() const { return m_bTickReceived; }

    void addTickWithOrdersListener( IHkfeTickWithOrdersListener *l ) const
    { m_tickWithOrdersListeners.add( l ); }

    void removeTickWithOrdersListener( IHkfeTickWithOrdersListener *l ) const
    { m_tickWithOrdersListeners.remove( l ); }

    const HkfeOrdersInTick& getOrdersInTick() { return m_orders_in_tick; }

private:
    // Event handlers
    void onHkfeTradeMsg( const HkfeTradeMsg& msg );

    void notifyTickWithOrders();
    void addOrderToTick( const HkfeTradeMsg& msg );

private:
    bool m_bInitialized;
    bool m_bTickReceived;
    MsgHandlerPtr m_subHkfeTradeMsg;
    boost::optional<uint32_t> m_currentTradeSequence;

    mutable ListenerList<IHkfeTickWithOrdersListener*> m_tickWithOrdersListeners;
    HkfeOrdersInTick m_orders_in_tick;
};

BB_DECLARE_SHARED_PTR( HkfeTickProvider );

}

#endif // BB_CLIENTCORE_HKFETICKPROVIDER_H

#ifndef BB_CLIENTCORE_BOOKLEVEL_H
#define BB_CLIENTCORE_BOOKLEVEL_H

/* Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved. */


#include <boost/noncopyable.hpp>
#include <boost/foreach.hpp>

#define BOOST_MULTI_INDEX_DISABLE_SERIALIZATION // eliminate unnecessary dependency
#include <boost/multi_index_container.hpp>
#include <boost/multi_index/sequenced_index.hpp>
#include <boost/multi_index/hashed_index.hpp>
#include <boost/multi_index/identity.hpp>
#undef BOOST_MULTI_INDEX_DISABLE_SERIALIZATION

#include <bb/core/PriceSize.h>
#include <bb/core/MarketLevel.h>
#include <bb/core/PreallocatedObjectFactory.h>

#include <bb/clientcore/BookOrder.h>

namespace bb {

/// Represents a set of orders that share the same price/side/source
class BookLevel
    : public FactoryObject<BookLevel>
{
public:
    // this is like std::list<BookOrder*>, but adds a hash table for quick lookup by value
    typedef boost::multi_index_container<BookOrder*, boost::multi_index::indexed_by<
        boost::multi_index::sequenced<>, // like list
        boost::multi_index::hashed_unique<boost::multi_index::identity<BookOrder*> > // like hash_set
        > > orderlist_base;

    class orderlist
        : public orderlist_base
    {
    public:
        orderlist( BookLevel* owner )
            : m_owner(owner)
        {}

        void push_back( const value_type& value )
        {
            value->lvl = m_owner;
            orderlist_base::push_back(value);
        }
        void push_front( const value_type& value )
        {
            value->lvl = m_owner;
            orderlist_base::push_front(value);
        }

    private:
        // hide the following methods; they aren't needed but would have to set BookOrder::lvl
        void insert();
        void merge();
        void splice();
        void swap();

        BookLevel* m_owner;
    };

public:
    BookLevel();
    BookLevel( source_t _src, side_t _side, double _px, int32_t _sz,
            int32_t _numOrders = -1, bool _maintainOrderList = true );
    BookLevel( source_t _src, side_t _side, double _px );
    /// copy-constructor.  performs a deep copy of the order list
    // TODO: worry about slicing in the deep copy, for now it's just a BookOrder
    BookLevel( const BookLevel& other );
    virtual ~BookLevel();
    BookLevel &operator=(const BookLevel &other);
    BookLevel &operator+= (const BookLevel &other);
    /// Creates a BookOrder, filling it with information from this BookLevel.
    /// This BookOrder must be freed with delete.
    virtual BookOrder* toBookOrder() const;
    virtual BookOrder* toFactoryBookOrder( const BookOrderFactoryPtr& factory ) const;


    std::ostream& print(std::ostream &out) const;

    bool isOK() const { return likely ( sz > 0 && !std::isnan( px ) ); }
    bool detailedIsOK() const;

    void setMaintainOrderList( bool b ) { m_bMaintainOrderList = b; }
    bool getMaintainOrderList() const { return m_bMaintainOrderList; }

    void setUseOrderListSizes( bool b ) { m_bUseOrderListSizes = b; }
    bool getUseOrderListSizes() const { return m_bUseOrderListSizes; }


    void setLastUpdateTime( const timeval_t& local, const timeval_t& exch )
    {
        m_lastUpdateLocalTime = local;
        m_lastUpdateExchangeTime = exch;
    }
    const timeval_t& getLastUpdateLocalTime() const { return m_lastUpdateLocalTime; }
    const timeval_t& getLastUpdateExchangeTime() const { return m_lastUpdateExchangeTime; }

    /// Returns the source of this BookLevel.
    source_t getSource() const               { return src; }
    /// Returns the side of this BookLevel.
    side_t   getSide() const                 { return side; }
    /// Returns the price of this BookLevel.
    double   getPrice() const                { return px; }

    // Returns the price multiplied by the sidesign
    double getComparePrice() const           { return cmpPx; }

    /// Returns the aggregate size of this BookLevel.
    /// The BookLevel may not actually hold any orders, storing only the aggregate size.
    int32_t  getSize() const                 { return sz; }
    /// Returns the number of orders in this BookLevel.
    virtual int32_t  getNumOrders( source_t source = source_t() ) const
    {
        BB_ASSERT( source == getSource() || source.type() == bb::SRC_UNKNOWN );
        return ( m_numOrders >= 0 ) ? m_numOrders : olist.size();
    }

    /// Returns the price and aggregate size of this BookLevel in a PriceSize
    PriceSize getPriceSize() const           { return PriceSize(px, sz); }

    /// Sets the source of this BookLevel.
    void     setSource( source_t src_)       { src = src_; }
    /// Sets the side of this BookLevel.
    void     setSide( side_t side_)          { side = side_; setCmpPx(); }
    /// Sets the price of this BookLevel.
    void     setPrice( double px_)           { px = px_; setCmpPx(); }
    /// Sets the aggregate size of this BookLevel.
    /// The BookLevel may not actually hold any orders, storing only the aggregate size.
    void     setSize( int32_t sz_ )            { sz = sz_; }
    void     setPriceSize( double px_, int32_t sz_ ) { px = px_; sz = sz_;  setCmpPx(); }
    void     setPriceSize( const PriceSize& ps ) { px = ps.getPrice(); sz = ps.getSize();  setCmpPx(); }
    /// Set the number of orders in this BookLevel.
    /// If numOrders is less than 0, then getNumOrders returns olist.size().
    void     setNumOrders( int32_t numOrders ) { m_numOrders = numOrders; }

    // Fills the passed MarketLevel with information from this BookLevel.
    // Returns the modified MarketLevel.
    MarketLevel& applyTo( MarketLevel& ml ) const
    {   ml.set( side, px, sz );  return ml; }

    const orderlist& getOrders() const { return olist; }

    /// Takes ownership of the pointer
    virtual void insertOrder( BookOrder* bo )
    {
        olist.push_front( bo );

        if( m_bUseOrderListSizes )
            sz += bo->sz;

        m_lastUpdateLocalTime = bo->tv_msg;
        m_lastUpdateExchangeTime = bo->tv_exch;
    }
    /// Takes ownership of the pointer
    /// order really shouldn't matter, but this is done to maintain compatibility.
    void pushbackOrder( BookOrder* bo )
    {
        olist.push_back( bo );

        if( m_bUseOrderListSizes )
            sz += bo->sz;

        m_lastUpdateLocalTime = bo->tv_msg;
        m_lastUpdateExchangeTime = bo->tv_exch;
    }
    virtual bool removeAndDeleteOrder( BookOrder* bo );
    void clearAndDeleteOrders()
    {
        BOOST_FOREACH( BookOrder* o, olist)
        {
            o->deallocate( o );
        }
        olist.clear();
        if( m_bUseOrderListSizes )
            sz = 0;
    }
    /// relinquishes control of the passed in order.
    BookOrder* releaseOrder( BookOrder* bo )
    {
        if( olist.get<1>().erase( bo ) )
        {
            if( m_bUseOrderListSizes && sz >= bo->sz )
                sz -= bo->sz;
            bo->lvl = NULL;
            return bo;
        }
        return NULL;
    }

    bool isEmpty() const
    {
        // Sanity check both indicators for emptiness.
        BB_ASSERT( getOrders().empty() == (getSize() == 0) );
        return getOrders().empty();
    }

protected:

    void setCmpPx()
    {
        cmpPx = -side2sign( side ) * px;
    }

    orderlist olist;

    source_t src;         /// the source of this BookLevel
    side_t side;          /// the side of this BookLevel
    double px;            /// the price of this BookLevel
    int32_t sz;             /// the aggregate size of this BookLevel
    double cmpPx;         /// the price used to compare book levels
    int32_t m_numOrders;    /// the number of orders in this BookLevel; set to -1 to use olist.size()
    bool m_bMaintainOrderList;
    bool m_bUseOrderListSizes;          /// if true the sizes of the orders in olist will affect the BookLevel aggregate size
    timeval_t m_lastUpdateLocalTime;    /// last time we received an update
    timeval_t m_lastUpdateExchangeTime; /// last exchange time

};
BB_DECLARE_SHARED_PTR( BookLevel );

typedef PreallocatedObjectFactory<BookLevel>  BookLevelFactory;
BB_DECLARE_INTRUSIVE_PTR( BookLevelFactory );


template<side_t side>
struct BookLevelComparer
{
};

template<>
struct BookLevelComparer<BID>
{
    bool operator()(BookLevelPtr const & one, BookLevelPtr const & two) const
    {
        return bb::GT ( one->getPrice(), two->getPrice() );
    }
    bool operator()( double const & one, double const & two) const
    {
        return bb::GT ( one, two );
    }
};

template<>
struct BookLevelComparer<ASK>
{
    bool operator()(BookLevelPtr const & one, BookLevelPtr const & two) const
    {
        return bb::LT ( one->getPrice(), two->getPrice() );
    }
    bool operator()( double const & one, double const & two) const
    {
        return bb::LT ( one, two );
    }
};

inline std::ostream& operator <<( std::ostream &out, const BookLevel& bl )
{
    return bl.print( out );
}

/// BookLevelCmp is comparison object for ordering BookLevels in a sorted_list
class BookLevelCmp : public std::binary_function<const BookLevel *, const BookLevel *, bool>
{
public:
    /// the cmp method returns true if bl1 is "less than" bl2
    /// "less than" means closer to the inside market, ie higher px for
    /// bids is "less than" a lower px bid.
    /// with asks, "less than" means lower px
    /// don't sort on any criteria other than px for now
    bool operator()(const BookLevel* bl1, const BookLevel* bl2) const
    {
        // use exact comparision rather than bb::NE because the comparator has to
        // be transitive.
        return bl1->getComparePrice() < bl2->getComparePrice();
    }
};


} // namespace bb


#endif // BB_CLIENTCORE_BOOKLEVEL_H

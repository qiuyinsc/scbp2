#ifndef BB_CLIENTCORE_FILTEREDBOOK_H
#define BB_CLIENTCORE_FILTEREDBOOK_H

/* Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved. */

#include <bb/clientcore/IBook.h>
#include <bb/core/instrument.h>
#include <bb/core/source.h>
#include <bb/core/smart_ptr.h>
#include <bb/core/PriceSize.h>
#include <boost/compressed_pair.hpp>
#include <boost/call_traits.hpp>
#include <iosfwd>
namespace bb {

/// FilteredBook
/// Book to help scrub data
/// example usage
/// struct MyFilter{
///     MyFilter(float h,float l):m_high(h),m_low(l){}
///     bool operator()(IBookPtr const&p)const{
///         if(!(p->isOK()))return false;
///         float mpx= p->getMidPrice();
///         return mpx<m_high && mpx>m_low;
///     }
/// private:
///  float m_high;
///  float m_low;
///};


template<class Filter>
class FilteredBook:public IBook
{
public:
    FilteredBook(IBookPtr const&b,typename boost::call_traits<Filter>::param_type f):m_data(b,f){
    }
    /// Returns the instrument this book is maintained for.
    instrument_t getInstrument() const  {return m_data.first()->getInstrument();}

    /// Returns a descriptive name for the book.
    std::string  getDesc() const {return m_data.first()->getDesc();}

    /// Returns the source of this book.
    source_t     getSource() const{return m_data.first()->getSource();}

    /// Returns the timeval this book last changed.
    timeval_t    getLastChangeTime() const {return m_data.first()->getLastChangeTime();}

    /// Returns true if the book is an aggregate book.
    /// An aggregate book is one that does not keep individual orders,
    /// but rather each BookLevel is an aggregate of orders.
    bool isAggregate() const{return m_data.first()->isAggregate();}

    /// Returns true if the book is in a "valid" state.
    /// The meaning of this is book-dependent, however clients typically use it
    /// to test whether they should "trust" the book's contents.
    bool isOK() const{
        return m_data.second()(m_data.first());
    }

    /// Flushes the book.
    void flushBook(){m_data.first()->flushBook();}

    /// Tells the Book to drop the specified level.
    /// Returns true if the Book actually does it (it doesn't necessarily have to), false otherwise.
    bool dropBookLevel( side_t side, double px ) {return m_data.first()->dropBookLevel(side,px);}

    /// Returns the mid-price of a book, or 0.0 if the book is not valid (e.g. is one-sided).
    double getMidPrice() const{return m_data.first()->getMidPrice();}

    /// Returns the PriceSize at a specified depth and side.
    /// Returns an empty PriceSize if nothing exists at that depth or side.
    PriceSize getNthSide( size_t depth, side_t side ) const {return m_data.first()->getNthSide(depth,side);}

    /// Returns an object for iterating over BookLevels of a particular side.
    /// Postcondition: The returned iterator will iterate getNumLevels objects.
    /// This abstracts the underlying containers used by a book; book implementations
    /// will implement their own iterator and pass them back.
    IBookLevelCIterPtr getBookLevelIter( side_t side ) const{return m_data.first()->getBookLevelIter(side);}

    /// Returns the number of levels of a particular side of this book.
    /// Note that empty levels above the "bottom" level are counted.
    size_t getNumLevels( side_t side ) const {return m_data.first()->getNumLevels(side);}

    /// Adds a book listener to this Book.
    /// Returns true if successful, or false if the listener is already added.
    bool addBookListener( IBookListener* pListener ) const {return m_data.first()->addBookListener(pListener);}

    /// Removes a book listener from this Book.
    /// Returns true if successful, or false if the listener was never in the Book.
    bool removeBookListener( IBookListener* pListener ) const {return m_data.first()->removeBookListener(pListener);}

    /// Adds a book level listener to this Book.
    /// Returns true if successful, or false if the listener is already added.
    bool addBookLevelListener( IBookLevelListener* pListener ) const {return m_data.first()->addBookLevelListener(pListener);}

    /// Removes a book level listener from this Book.
    /// Returns true if successful, or false if the listener was never in the Book.
    bool removeBookLevelListener( IBookLevelListener* pListener ) const{return m_data.first()->removeBookLevelListener(pListener);}

    /// Print the first optional_nlevels of the book to an ostream.
    /// If optional_nlevels isn't specified, either getDefaultMaxPrintLevels() or print_level_chg + 2 is used.
    std::ostream& print( std::ostream& out, int optional_nlvls  ) const{return m_data.first()->print(out,optional_nlvls);}
    std::ostream& print( std::ostream& out ) const{return m_data.first()->print(out,-2 );}

    /// Called when this book is about to be added to a MasterBook. Since we can't architecturally
    /// handle the same book being in 2 MasterBooks, this should check to make sure it hasn't been
    /// added already.
    void addToMasterBook() { m_data.first()->addToMasterBook( );}

    ///access to the filter you passed in
    Filter const& getFilter()const{
        return m_data.second();
    }
    ///access to the filter you passed in, and allow modification, do this at your own risk
    Filter & getFilter(){
       return m_data.second();
    }
private:
    boost::compressed_pair< IBookPtr, Filter> m_data;

};


} // namespace bb

#endif // BB_CLIENTCORE_FILTEREDBOOK_H

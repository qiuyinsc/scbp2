#ifndef BB_CLIENTCORE_CHECKEDTRADEDEMONCLIENT_H
#define BB_CLIENTCORE_CHECKEDTRADEDEMONCLIENT_H

/* Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved. */


#include <bb/core/LuaState.h>
#include <bb/core/env.h>

#include <bb/clientcore/ITradeDemonClient.h>

namespace bb {

/// ITradeDemonClient implementation which wraps another one, adding order validation
/// For interactive (human) use only, not for automated trading (not high performance)
template<typename TradeDemonClientT>
class CheckedTradeDemonClient
    : public TradeDemonClientT
{
public:
    template<typename A1>
    CheckedTradeDemonClient( const A1& a1 ) : TradeDemonClientT( a1 ) { init(); }

    template<typename A1, typename A2>
    CheckedTradeDemonClient( const A1& a1, const A2& a2 ) : TradeDemonClientT( a1, a2 ) { init(); }

    template<typename A1, typename A2, typename A3>
    CheckedTradeDemonClient( const A1& a1, const A2& a2, A3& a3 ) : TradeDemonClientT( a1, a2, a3 ) { init(); }

    // timeout/visible only apply to equity orders
    virtual hc_return_t send_order( acct_t acct
                                    , const instrument_t& instr
                                    , dir_t dir
                                    , mktdest_t dest
                                    , double px
                                    , unsigned int size
                                    , unsigned int orderid
                                    , int timeout
                                    , bool visible
                                    , tif_t tif
                                    , uint32_t oflags
                                    , timeval_t* send_time = NULL
                                    , const ISpecialOrderPostProcessor* specialOrder = NULL )
    {
        hc_return_t result = luabind::call_function<hc_return_t>( m_function,
                instr, dir, dest, px, size, orderid, timeout, visible, tif, oflags );

        return result != HC_SUCCESS ? result : TradeDemonClientT::send_order
            ( acct
              , instr, dir, dest, px, size, orderid, timeout, visible, tif, oflags, send_time
              , specialOrder );
    }

private:
    void init()
    {
        m_config.loadLibrary( "core" );
        m_config.load( DefaultCoreContext::getEnvironment()->getRoot() + "/lib/lua/check_order.lua" );
        m_function = m_config.root()["check_order"];
        BB_THROW_EXASSERT( luabind::type( m_function ) == LUA_TFUNCTION, "check_order is not a function" );
    }

    LuaState m_config;
    luabind::object m_function;
};

} // namespace bb

#endif // BB_CLIENTCORE_CHECKEDTRADEDEMONCLIENT_H

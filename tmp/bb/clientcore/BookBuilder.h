#ifndef BB_CLIENTCORE_BOOKBUILDER_H
#define BB_CLIENTCORE_BOOKBUILDER_H

/* Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved. */


#include <bb/clientcore/IBook.h>
#include <bb/clientcore/L2Book.h>
#include <bb/clientcore/BookSpec.h>
#include <bb/clientcore/ClientContext.h>
#include <bb/clientcore/SpamFilter.h>

namespace bb {

BB_FWD_DECLARE_SHARED_PTR(ClientContext);
BB_FWD_DECLARE_SHARED_PTR(IBookFactory);
BB_FWD_DECLARE_SHARED_PTR(SourceBookFactory);
BB_FWD_DECLARE_SHARED_PTR(OrderFilteredBookFactory);

/// IBookBuilder takes a BookSpec structure (i.e. a description of a
/// book that needs to be built) and builds it.
class IBookBuilder
{
public:
    virtual ~IBookBuilder() {}

    /// Builds an IBook which matches the given IBookSpec; if a suitable one already
    /// exists, returns that.
    virtual IBookPtr buildBook(IBookSpecCPtr spec) = 0;

    /// If the given book already exists inside the book builder, returns it.
    /// NULL if the book hasn't been built yet.
    virtual IBookPtr getBuiltBook(IBookSpecCPtr spec) = 0;

    virtual bool getUseSrcMonitors() const = 0;
    virtual void onEndTimeReached() {}

    /// Convenience method: calls buildBook with a SourceBookSpec for the given instr/src.
    IBookPtr buildSourceBook(const instrument_t &instr, source_t src );

    /// Convenience method: calls buildBook with a ShmemBookSpec for the given instr/src.
    IBookPtr buildShMemSourceBook(const instrument_t &instr, source_t src );
};
BB_DECLARE_SHARED_PTR(IBookBuilder);

/// The default IBookBuilder implementation. Builds the book locally.
class BookBuilder : public IBookBuilder
{
public:

    enum SourceMonitorMode { kUseSrcMonitors };

    BookBuilder( ClientContextPtr const& );
    BookBuilder( ClientContextPtr const&, SourceMonitorMode, bool requestSnapshot=false );
    BookBuilder( ClientContextPtr const&, bool useSrcMonitors, bool requestSnapshot=false );

    // IBookBuilder implementation
    virtual IBookPtr buildBook(IBookSpecCPtr spec);
    virtual IBookPtr getBuiltBook(IBookSpecCPtr spec);
    virtual bool getUseSrcMonitors() const { return m_useSrcMonitors; }

    // Functions used by Spec subclasses in building their concrete IBooks
    int getVerbose() { return getClientContext()->getVerbose(); }
    virtual ClientContextPtr getClientContext()const { return m_clientContext.lock(); }
    ClockMonitorPtr getClockMonitor() { return getClientContext()->getClockMonitor(); }
    IMStreamManagerPtr getMStreamManager() { return getClientContext()->getMStreamManager(); }
    // this is duplicate -- can get from ClientContext
    RandomSourcePtr getRandomSource() { return getClientContext()->getRandomSource(); }
    EventDistributorPtr getEventDistributor() { return getClientContext()->getEventDistributor(); }
    bool getShouldRequestSnapshot() { return m_requestSnapshot; }
    void setL2BookConfig( const L2BookConfig& l2config ){ m_l2BookConfig = l2config; };
    L2BookConfig getL2BookConfig(){ return m_l2BookConfig; };
protected:
    IBookPtr getBookInternal(IBookSpecCPtr spec, bool build);

    typedef bbext::hash_map<IBookSpecCPtr, IBookWeakPtr, BookSpecHasher, BookSpecComparator> BookSpec2InstanceMap;
    BookSpec2InstanceMap m_alreadyBuiltBooks;

    IBookFactoryPtr getRawMBFactory(const instrument_t& instr, const sources_t& srcs);
    bb::ClientContextWeakPtr m_clientContext; // m_clientContext has a ref to the BookBuilder.
    bool m_useSrcMonitors;
    bool m_requestSnapshot;
    SpamFilter m_harvestedBookSpam;
    L2BookConfig m_l2BookConfig;
};
BB_DECLARE_SHARED_PTR(BookBuilder);
class BookBuilderLua : public BookBuilder
{
public:
    BookBuilderLua(ClientContextPtr const&clientContext, bool useSrcMonitors);
    ClientContextPtr getClientContext() const{ return m_clientContextRef; }
private:
    bb::ClientContextPtr m_clientContextRef;//this one is NOT held by the ClientConext
};
} // namespace bb

#endif // BB_CLIENTCORE_BOOKBUILDER_H

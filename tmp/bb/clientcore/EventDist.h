#ifndef BB_CLIENTCORE_EVENTDIST_H
#define BB_CLIENTCORE_EVENTDIST_H

/* Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved. */

#include <bitset>
#include <vector>

#include <boost/foreach.hpp>
#include <boost/shared_ptr.hpp>
#include <boost/enable_shared_from_this.hpp>
#include <boost/intrusive_ptr.hpp>
#include <boost/operators.hpp>
#include <boost/function.hpp>
#include <boost/unordered_map.hpp>

#include <bb/core/instrument.h>
#include <bb/core/Msg.h>
#include <bb/core/auto_vector.h>
#include <bb/core/Priority.h>
#include <bb/core/ptime.h>
#include <bb/core/network.h>
#include <bb/core/MStreamCallback.h>
#include <bb/core/EventPublisher.h>
#include <bb/clientcore/IEventDistListener.h>

namespace bb {
class ClockMonitor;

// forward declaration
BB_FWD_DECLARE_SHARED_PTR( Alert );
BB_FWD_DECLARE_SHARED_PTR( EventDistributor );
BB_FWD_DECLARE_SHARED_PTR( IMStreamManager );


// Clientcore Priorities
extern const Priority PRIORITY_CC_HIGHEST;       // (0xCFFFFFFF);
extern const Priority PRIORITY_CC_ClockMonitor;  // (0xCD000000); // For regression-compatibility
extern const Priority PRIORITY_CC_SourceMonitor; // (0xCC000000);
extern const Priority PRIORITY_CC_Imbalance;     // (0xCB000000);
extern const Priority PRIORITY_CC_Book;          // (0xCA000000);
extern const Priority PRIORITY_CC_Ticks;         // (0xC9000000);
extern const Priority PRIORITY_CC_MasterBook;    // (0xC8000000);
extern const Priority PRIORITY_CC_SyntheticBook; // (0xC7000000);
extern const Priority PRIORITY_CC_Misc;          // (0xC1000000);
extern const Priority PRIORITY_CC_LOWEST;        // (0xC0000000);

// ClockMonitor needs to wakeup before and after _all_ normal message processing
// is done; it uses these priorities for that.
// The only things that should happen outside of this range are handlers which need
// to sit outside the normal range of things (like message performance monitoring).
extern const Priority PRIORITY_CC_ClockMonitor_HIGHEST; // (0xFEFFFFFF);
extern const Priority PRIORITY_CC_ClockMonitor_LOWEST;  // (0x01000000);

/// MessageLogger runs before ClockMonitor.
extern const Priority PRIORITY_CC_MessageLogger; // (0xFF0000FF);

class EventDistStateListener : public IEventSubscriber
{
    public:
        virtual bool onEventDistributorStartProcessing( const Msg& msg ) = 0;
        virtual bool onEventDistributorFinishProcessing( const Msg& msg ) = 0;
};

typedef EventPublisher<EventDistStateListener> EventDistStatePublisher;
struct notifyStartProcessingImpl : public EventDistStatePublisher::INotifier
{
    notifyStartProcessingImpl(EventDistStateListener* callback) : EventDistStatePublisher::INotifier ( callback ) { }
    bool notify( const Msg& msg )
    {
       return m_callback->onEventDistributorStartProcessing( msg );
    }
};
struct notifyFinishProcessingImpl : public EventDistStatePublisher::INotifier
{
    notifyFinishProcessingImpl(EventDistStateListener* callback) : EventDistStatePublisher::INotifier ( callback ) { }
    bool notify( const Msg& msg )
    {
       return m_callback->onEventDistributorFinishProcessing( msg );
    }
};

/**
    \class EventDistributor

    The EventDistributor is responsible for notifying listeners about events with which they
    have registered interest. Listeners can choose to listen to all events, or specific message types,
    or specific mtypes on specific symbols.

    TOPIC FILTERING:

    The most specific topic filter is subscribeEvents -- you supply a source, mtype, and instrument.
    The multicast group corresponding to the source will be joined, and the ED will see all the
    messages coming in on that group. Then, any messages incoming with the same mtype and the
    same symbol will trigger your listener.

    Important points:
    *) The source doesn't do anything -- once the multicast group is joined, you'll see messages
       which match your mtype/instr, regardless of which source they actually come from.
    *) Though you must pass a full instrument, the filtering does not care about anything
       but the symbol.

    PRIORITIES:

    Listeners can assign priorities to themselves (ranging from 0-2^32).
    The HIGHEST priority listeners for a given message are called first.
**/
class EventDistributor
    : public boost::enable_shared_from_this<EventDistributor>
    , public IMStreamCallback
    , public EventDistStatePublisher
{
public:
    typedef boost::function<void(void)>       WorkHandler;
    typedef boost::function<void(const Msg&)> MsgHandler;
    class                                     MsgWorker;
    typedef boost::intrusive_ptr<MsgWorker>   MsgWorkerPtr;
    typedef std::list < MsgWorkerPtr >        MsgWorkerPtrList;

    enum StoreMessageReceiveTime
    {
        StoreMessageReceiveTimeDisabled,
        StoreMessageReceiveTimeEnabled
    };

    EventDistributor( IMStreamManagerPtr msm,
            const timeval_t& starttv,
            const timeval_t& endtv,
            StoreMessageReceiveTime store_msg_receive_tv = StoreMessageReceiveTimeEnabled );
    virtual ~EventDistributor();

    /// Subscribes to messages for the given source, mtype and instr.
    /// Subscription will be initialized so as to unsubscribe the handler upon destruction.
    /// The Subscription will be invalid if the handler is empty.
    ///
    /// It is ok to pass instrument_t::all() to these.  This will not pass all instruments!
    /// It'll pass messages whose hdr.sym == SYM_ALL. Same thing holds for none()/unknown().
    void subscribeEvents( Subscription &eventSub, const MsgHandler& handler,
            source_t src, mtype_t mtype, const instrument_t &instr,
            Priority priority ) const;
    void subscribeEvents( Subscription &eventSub, IEventDistListener* pEDL,
            source_t src, mtype_t mtype, const instrument_t &instr,
            Priority priority ) const;
    void subscribeEvents( Subscription &eventSub, IEventDistListener* pEDL,
            source_t src, const std::vector<mtype_t>& msgtypes, const instrument_t &instr,
            Priority priority ) const;

    void subscribeEventsSym( Subscription &eventSub, const MsgHandler& handler,
        source_t src, mtype_t mtype, const symbol_t &sym, Priority priority ) const;

    /// Subscribes to messages for the given source/mtype, matching all symbols.
    /// Subscription will be initialized so as to unsubscribe the handler upon destruction.
    /// The Subscription will be invalid if the handler is empty.
    void subscribeEventsMType( Subscription &eventSub, const MsgHandler& handler,
            source_t src, mtype_t mtype, Priority priority ) const;
    void subscribeEventsMType( Subscription &eventSub, IEventDistListener* pEDL,
            source_t src, mtype_t mtype, Priority priority ) const;
    void subscribeEventsMType( Subscription &eventSub, IEventDistListener* pEDL,
            source_t src, const std::vector<mtype_t>& msgtypes, Priority priority ) const;

    /// Passive listeners -- listens to messages without subscribing to any multicast groups.
    /// Typically users of this have already arranged for the MStreamManager to deliver their messages,
    /// so this forces the EventDistributor to pass them as well.
    void subscribeEventsPassive( Subscription &eventSub, IEventDistListener* pEDL,
            const std::vector<mtype_t>& msgtypes, const instrument_t &instr,
            Priority priority ) const;
    void subscribeEventsPassive( Subscription &eventSub, IEventDistListener* pEDL,
            mtype_t mtype, const instrument_t &instr,
            Priority priority ) const;
    void subscribeEventsMTypePassive( Subscription &eventSub, IEventDistListener* pEDL,
            mtype_t mtype, Priority priority ) const;

    /// don't use me - hconsole only
    /// Passive listener for ALL messages
    /// hconsole uses it to print messages it doesn't understand
    /// this keeps old hconsols compatible with new traders
    void subscribeEventsAllPassive( Subscription &eventSub, IEventDistListener* pEDL,
            Priority priority ) const;

    /// Event hooks: these don't modify the filter list or the feeds subscribed.
    /// Instead, you will get called back for all the messages that someone else
    /// in the program has asked for.

    /// Hooks into all messages (for ClockMonitor).
    void hookEventsAll( Subscription &eventSub, IEventDistListener *handler, Priority priority ) const;
    void hookEventsAll( Subscription &eventSub, const MsgHandler &handler, Priority priority ) const;
    void hookEventsAll( Subscription &eventSub, MsgWorkerPtr worker ) const;

    /// Hooks into all messages of the given mtype (for SourceMonitors).
    void hookEventsMType( Subscription &eventSub, IEventDistListener *pEDL, mtype_t mtype, Priority priority ) const;

    /// Adds a unit of work to the work queue.
    /// TODO: throw if priority is higher than 'current' priority level
    void addWork( const WorkHandler& handler, Priority priority );

    /// Adds a unit of work to the work queue.
    /// TODO: throw if priority is higher than 'current' priority level
    void addWork( MsgWorkerPtr spMsgWorker );

    /// Runs the EventDistributor with the given MStream until we are
    /// past our endtime or until the MStream has no more messages.
    void run();

    /// Run with a timeout, specified in seconds from the current time.
    /// Slightly less efficient than without timeout, so use only when needed.
    /// If the timeout is reached, the specified callback is invoked, then the distributor continues to run.
    void runWithTimeout( const ptime_duration_t& timeout, boost::function<void ()> timeoutCallback );

    /// Registers all the streams but does not start the event loop. This assume that the
    /// event loop is being driven by some other entity
    void startDispatch();

    /// Removes all streams. This assume that the event loop is being driven by some other entity
    void endDispatch();

    /// Returns the end timeval, the timeval after which the event loop will escape.
    timeval_t getEndTimeval() const                 { return m_endtv; }

    /// Sets the end timeval, the timeval after which the event loop will escape.
    void setEndTimeval( const timeval_t& endtv )    { m_endtv = endtv; }

    /// Returns the start timeval, the timeval after which events will start to be handled.
    timeval_t getStartTimeval() const               { return m_starttv; }

    /// Returns msg-related info from the last event that was received.
    const Msg *getLastMsg() const;
    timeval_t getLastEventTimeval() const;

    // This timeval is the time the last msg was received.
    // The 'getLastEventTimeval()' time is the timestamp *inside* the msg itself
    // which is usually the timestamp of when it was sent
    timeval_t getLastMsgReceiveTimeval() const;

    // This timeval is the time the last msg was received by the NIC.
    timeval_t getLastMsgReceiveNicTimeval() const;

    // Retrieves the source of the last message we received. It would typically be in the form of
    // a fqdn.
    bb::sockaddr_ipv4_t const & getLastMsgSourceAddress() const;

    /// Returns the total number of messages processed by the EventDistributor.
    uint64_t getHandledMessageCount() const                  { return m_handledMsgCount; }

    /// Returns the total number of messages processed by the EventDistributor.
    uint64_t getFilteredHandledMessageCount() const                  { return m_filteredHandledMsgCount; }

    /// Returns the total number of messages received by the EventDistributor (including rejection).
    uint64_t getTotalMessageCount() const           { return m_totalMsgCount; }

    /// Returns the total number of OnIdle messages received by the EventDistributor.
    uint64_t getTotalOnIdleMessageCount() const           { return m_totalOnIdleMsgCount; }

    // It is expensive to merge (potentially large) vectors of MsgWorkerPtrs.
    // The reason is that swapping them isn't that easy and what we're actually doing is
    // constructing and destructing loads of times, just so we end up with a sorted vector of
    // MsgWorkerPtr's. Instead we do this:
    // - Every time we process a type of message we haven't been before ( a specific sym + mtype )
    // - We figure out which callbacks are associated with this message:
    //   - Callbacks for all messages
    //   - Callbacks for this specific message type
    //   - Callbacks for this specific message type and symbol
    // - We go through the trouble of merging these vectors of callbacks into a large vector.
    // - We store references to all these callbacks in the resulting vector into yet another vector.
    // Now, every time we receive a message of the same sym+mtype, we can simply return a copy of this
    // vector of references. Copying these references is much quicker than the merge action we have to
    // take when we encounter a message for the first time.
    // We actually have to return a copy of this vector because 'addWork' can still schedule ad-hoc work to
    // be done, and it's quicker to stuff that in the same vector as well.
    virtual void onMessage( const Msg &msg );

    /// Make sure msg is valid until onMessage finishes
    virtual void onMessage( MsgCPtr msg );

    /// Get the position in the current work queue
    /// This indicates how many other callbacks got called before the current callback
    /// for the current message.  A high number indicates that many other callbacks
    /// with higher priority existed for this message
    uint32_t getWorkQueuePosition() { return m_workQueuePosition; };

    /// Get the transit time, in nanoseconds for the last message recieved
    /// for the given Feed
    timeval_nanosecond getLastTransitTime( EFeedType feed ) const
    {
        return m_feedTimes[feed].getTransitTime();
    }


public:
    // When the event distributor has to do its work ( because a message came in ), it will
    // initially figure out which callbacks are subscribed for all messages, this specific message type
    // or this specific instrument. It will ** maintain references ** to these callbacks in a vector that's
    // stored in the cache. Whenever we need to process a similar set of callbacks we make * a copy * of this
    // vector of references and process it in order of priority.
    //
    // Because we are dealing with references here, we want to make sure that they stay valid. At least while
    // we're busy processing messages. It is because of this that we want to store 'pending subscriptions' in
    // a separate list. It is only once we are done calling all callbacks that we will add or remove new or old
    // callbacks. If we do this, we will also invalidate the cache.
    mutable struct pending_subscriptions_t : private boost::noncopyable
    {
        public:
            pending_subscriptions_t() : inOnMessage ( false )
            {}

            void push_back ( const WorkHandler & cb )
            {
                BB_THROW_EXASSERT_SSX ( inOnMessage, "Only expect to add back to queue if we're processing a message" );
                m_queue.push_back ( cb );
            }

            void processQueue ()
            {
                if ( unlikely ( !m_queue.empty() ) )
                {
                    BOOST_FOREACH ( WorkHandler & f, m_queue )
                    {
                        f();
                    }
                    m_queue.clear();
                }
            }

            bool inOnMessage;

        private:
            std::vector < WorkHandler > m_queue;
    } m_pendingSubscriptions;

private:
    friend class ClockMonitor;
    template< class T >
    struct EventDistListenerWrapper
    {
        EventDistListenerWrapper( T* eventListener ) : m_EventListener( eventListener ) {}
        void operator()( const Msg & msg ) { m_EventListener->onEvent( msg); }

    private:
        T* m_EventListener;
    };

    // - Reset alpha exit time
    // - Set alpha entry time
    // - Call every queued up MessageWorker
    void process( const Msg &msg );
    void processIntl( const Msg &msg );

    uint32_t m_workQueuePosition;

    struct source_timing
    {
        source_timing()
            : sent_tv( timeval_t::earliest )
            , recv_tv( timeval_t::earliest )
        {}
        timeval_t      sent_tv;
        timeval_t      recv_tv;
        timeval_nanosecond getTransitTime() const
        {
            return timeval_diff_fast_to_nanoseconds(recv_tv, sent_tv);
        }
    };
    source_timing                  m_feedTimes[SRC_UNKNOWN+1];

public:
    /// A MsgWorker holds a MsgHandler which will handle the message.
    /// This is the object which is held internally in the work queue.
    /// Implementors can pass instances held in a MsgWorkerPtr
    /// to EventDistributor::addWork.
    class MsgWorker
    {
    public:
        template <class T>
        MsgWorker( Priority priority, T *handler )
            : m_handler( EventDistListenerWrapper<T>( handler ) ), m_priority( priority ), m_refCount(0) {}

        MsgWorker( const MsgHandler &hdlr, Priority priority )
            : m_handler( hdlr ), m_priority( priority ), m_refCount( 0 ) {}
        virtual ~MsgWorker();

        Priority getPriority() const               { return m_priority; }

        // TODO: this is a hack for ClockMonitor compatibility
        // the real /final interface wouldn't allow this
        // you definitely can't do this once this object is added to the queue
        void setPriority( Priority priority )      { m_priority = priority; }
        void operator()(const Msg &msg) const { m_handler(msg); }

    protected:
        friend void intrusive_ptr_add_ref(const EventDistributor::MsgWorker *e);
        friend void intrusive_ptr_release(const EventDistributor::MsgWorker *e);

        MsgHandler          m_handler;
        Priority            m_priority;
        mutable int         m_refCount;
    };

    static inline bool mtypeOK(bb::mtype_t mtype)
        { return mtype > MSG_ALL && mtype <= MAX_VALID_MTYPE_LEN; }
    static inline bool instrOK(const bb::instrument_t &instr)
        { return instr.sym == SYM_UNKNOWN || instr.sym == SYM_ALL || instr.sym == SYM_NONE || instr.is_valid(); }


    struct MsgWorker_higher_priority : public std::binary_function<MsgWorkerPtr, MsgWorkerPtr, bool>
    {
        bool operator()( const MsgWorkerPtr &x, const MsgWorkerPtr &y ) const
            { return x->getPriority() < y->getPriority(); }
    };

    typedef boost::reference_wrapper<MsgWorkerPtr> MsgWorkerPtrRef;
    typedef std::vector<MsgWorkerPtr> MsgWorkerVec;
    typedef std::vector<MsgWorkerPtrRef> MsgWorkerRefVec;

private:
    // We will keep merge and cache vectors of references to callbacks as that's quicker. When we do this we will want to make
    // sure these references stay valid which is why we don't want to add new subscriptions of remove existing ones when we're
    // within 'onMessage'. This code make sure we process our vector changes at the very end.

    // We're using a construction similar to 'BOOST_SCOPE_EXIT' for this - once this struct goes out of scope, the destructor
    // will add all subscriptions and clear the queues. We are adding it here rather than at the end of the function to make
    // sure that this function will be called, regardless of what happens below. That is, if we throw an exception or decide
    // on another reason to return, our subscriptions will still be added.
    struct post_event_work_manager_t
    {
        // Contains:
        // - pending_subscriptions_t
        //   We don't want to add/remove subscriptions as we're looping over vectors of callbacks. We save such actions until
        //   we're done.
        // - MsgWorkerPtrList
        //   This is a list ( so references stay valid ) of ad-hoc MsgWorkerPtr items that were added by calling the 'addWork'
        //   function within the event distributor. These callbacks are meant to stay alive until they're called once.
        // - MsgWorkerRefVec
        //   This is a vector containing references to:
        //   - All subscriptions for the current messages
        //   - All 'ad hoc' work items contained in the MsgWorkerPtrList we mentioned before.
        post_event_work_manager_t ( pending_subscriptions_t & ps, MsgWorkerPtrList & wl, MsgWorkerRefVec & mwrv )
                : m_alreadyInOnMessage ( ps.inOnMessage )
                , m_queue ( ps )
                , m_workerList ( wl )
                , m_msgWorkerRefVec ( mwrv )
        {
            m_queue.inOnMessage = true;
        }

        ~post_event_work_manager_t()
        {
            // In backtesting or live sim mode, we fake timers by sending a TIMEOUT message to the ED, which it will process.
            // Should we decide to send our own message to the ED from a timer callback, we'll be within an onMessage function twice.
            // Whoa. Mind. Blown. In this case, we *still* want to hold off on invalidating anything until the
            // topmost onMessage call is done.
            if ( likely ( !m_alreadyInOnMessage ) )
            {
                m_queue.inOnMessage = false;
                m_queue.processQueue();
            }

            // It is possible to prematurely exit from the event distributor onMessage() function - for instance, because we are before the
            // start time or after the end time. In this case, we will still have items in our 'm_mwRefVec' which we don't want pointing
            // to rubbish data. Because of this, we only clear out the m_workerList when there could be nothing pointing to it anymore.
            if ( likely ( m_msgWorkerRefVec.empty() ) )
            {
                m_workerList.clear();
            }
        }

        private:
            const bool                          m_alreadyInOnMessage;
            pending_subscriptions_t &           m_queue;
            MsgWorkerPtrList &                  m_workerList;
            MsgWorkerRefVec &                   m_msgWorkerRefVec;
    };

    struct eventdist_state_manager_t
    {
        eventdist_state_manager_t(Msg const & msg, EventDistributor& ed)
            : m_msg ( msg )
            , m_ed ( ed )
        {
            m_ed.notifyStartProcessing( m_msg );
        }

        ~eventdist_state_manager_t()
        {
            m_ed.notifyFinishProcessing( m_msg );
        }

        Msg const & m_msg;
        EventDistributor& m_ed;
    };
    friend struct eventdist_state_manager_t;

protected:
    void insert_in_priority_order_all( const MsgWorkerPtr & work ) const;
    void insert_in_priority_order_mtype( const mtype_t mtype, const MsgWorkerPtr & work ) const;
    void insert_in_priority_order_mtype_sym ( const mtype_t mtype, const symbol_t sym, const MsgWorkerPtr & work ) const;

    // The event distributor is message based. As a message comes in, we iterate over references to callbacks that
    // want to be informed when this messages comes in. These callbacks that we're referencing are stored in vectors and
    // looked up by simid or message type. This function removes the MsgWorkerPtr from such a vector but won't do this
    // yet if we are already busy operating on the references. Doing so will probably invalidate references and we want to avoid that.
    //
    // Instead, if we are already processing a message, the 'inOnMessage' variable will be set. If in this function we notice
    // that variable is indeed set, we will put off removing from the vector now. Instead, we will bind a boost function and put it
    // on a list of methods that will be called once all message callbacks have already been called.
    //
    // Once all message callbacks have been called, we will call this method again. This time, 'inOnMessage' will not be set and
    // we will actually add/remove the vector to/from the vector in question.
    void remove_from_vector_all( const MsgWorkerPtr & work ) const;
    void remove_from_vector_mtype( const mtype_t mtype, const MsgWorkerPtr & work ) const;
    void remove_from_vector_mtype_sym ( const mtype_t mtype, const symbol_t sym, const MsgWorkerPtr & work ) const;
    void remove_from_pending_callbacks ( const MsgWorkerPtr & work ) const;

    void invalidate_cache ( const mtype_t mtype, const symbol_t sym ) const;
    void invalidate_cache ( const mtype_t mtype ) const;
    void invalidate_cache ( ) const;

private:
    void notifyStartProcessing( const Msg& msg )
    {
        notifyAll<notifyStartProcessingImpl>( msg );
    }
    void notifyFinishProcessing( const Msg& msg )
    {
        notifyAll<notifyFinishProcessingImpl>( msg );
    }
#ifdef UBUNTU14
    inline void insert_in_priority_order( MsgWorkerVec & vec, const MsgWorkerPtr & work ) const;
    inline void remove_from_vector( MsgWorkerVec & vec, const MsgWorkerPtr & work ) const;
#else
    void insert_in_priority_order( MsgWorkerVec & vec, const MsgWorkerPtr & work ) const __attribute__((always_inline));
    void remove_from_vector( MsgWorkerVec & vec, const MsgWorkerPtr & work ) const __attribute__((always_inline));
#endif

    // disallow default constructor
    EventDistributor();

    BB_FWD_DECLARE_SHARED_PTR( AllEventSub );
    BB_FWD_DECLARE_SHARED_PTR( MTypeEventSub );
    BB_FWD_DECLARE_SHARED_PTR( SymMTypeEventSub );
    class MStreamTimeoutAdapter;
    friend class AllEventSub;
    friend class MTypeEventSub;
    friend class SymMTypeEventSub;

protected:
    IMStreamManagerPtr m_mstreamManager;

    timeval_t                                        m_starttv;
    timeval_t                                        m_endtv;
    const StoreMessageReceiveTime                    m_storeMsgReceiveTv;
    uint64_t                                         m_totalMsgCount, m_handledMsgCount, m_filteredHandledMsgCount;

    uint64_t                                         m_totalOnIdleMsgCount;

    mutable std::bitset<SYM_MAX>                     m_syms;
    mutable std::bitset<MAX_VALID_MTYPE_LEN>         m_mtypes;

    Msg const *                                      m_lastMsg;
    timeval_t                                        m_lastMsgReceiveTv;

    struct MessageKey : public bb::MsgHdrKey < bb::KeySymbol, bb::KeyMType >
    {
        MessageKey ( bb::MsgHdr const & hdr )
            : bb::MsgHdrKey < bb::KeySymbol, bb::KeyMType > ( hdr )
        {}

        MessageKey ( const symbol_t s, const mtype_t mt )
            : bb::MsgHdrKey < bb::KeySymbol, bb::KeyMType > ( s, mt )
        {}

        mtype_t mtype() const
        {
            return static_cast<mtype_t>(field_two);
        }
    };
    typedef boost::unordered_map < MessageKey,
        std::pair < MsgWorkerVec, MsgWorkerRefVec >,
        bb::MsgHdrKeyHasher,
        bb::MsgHdrKeyEqual
        > MsgCache;

    mutable MsgWorkerPtrList                         m_addWorkItems;
    mutable MsgCache                                 m_msgWorkerCache;
    mutable MsgWorkerRefVec                          m_mwRefVec;

    mutable MsgWorkerVec                             m_mwAll;
    mutable auto_vector<MsgWorkerVec>                m_mwMType;
    mutable auto_vector< auto_vector<MsgWorkerVec> > m_mwMTypeSym;

    // this is cleared every step, but is kept as a member to prevent
    // constant allocation/deallocation/resizing
    // it is kept in order according to MsgWorker_higher_priority (highest at the end)
    mutable MsgWorkerVec                             m_mwQueue;
    mutable MsgWorkerVec                             m_tmpMwQueue;
    // when true all messages should be passed along (aka no filter)
    mutable bool m_subscribeAllPassive;
    bool m_amRunning;

};
BB_DECLARE_SHARED_PTR( EventDistributor );

inline void intrusive_ptr_add_ref(const EventDistributor::MsgWorker *e) { ++e->m_refCount; }
inline void intrusive_ptr_release(const EventDistributor::MsgWorker *e) { --e->m_refCount; if(e->m_refCount == 0) delete e; }

inline void EventDistributor::subscribeEventsPassive( Subscription &eventSub, IEventDistListener* pEDL,
        mtype_t mtype, const instrument_t &instr,
        Priority priority ) const
{
    std::vector<mtype_t> mts;
    mts.push_back(mtype);
    subscribeEventsPassive(eventSub, pEDL, mts, instr, priority);
}

// This class will receive a message and check if it belongs to the instrument it was constructed for
// if so the handler will be invoked otherwise the message will be ignored
class InstrFilterMsgHandler
{
public:
    InstrFilterMsgHandler( const instrument_t& instr, const EventDistributor::MsgHandler& h )
        : m_instr( instr ), m_handler( h ) {}

    void operator() ( const Msg& m )
    {
        boost::optional<instrument_t> opt_instr = m.instrument();
        if( opt_instr && instrument_t::equals_no_mkt_no_currency()(opt_instr.get(), m_instr) )
        {
            m_handler( m );
        }
    }

    instrument_t m_instr;
    EventDistributor::MsgHandler m_handler;
};


} // namespace bb

#endif // BB_CLIENTCORE_EVENTDIST_H

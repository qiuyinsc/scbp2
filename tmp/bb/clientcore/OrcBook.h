#ifndef BB_CLIENTCORE_ORCBOOK_H
#define BB_CLIENTCORE_ORCBOOK_H

/* Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved. */


#include <map>
#include <set>

#include <bb/clientcore/ClockMonitor.h>
#include <bb/clientcore/Book.h>

#include <boost/array.hpp>

namespace bb {

BB_FWD_DECLARE_SHARED_PTR( ClientContext );
BB_FWD_DECLARE_SHARED_PTR( SourceMonitor );
class OrcOrderDepthMsg;
struct orc_order_depth_level;

class OrcBook
    : public BookImpl
    , private IEventDistListener
    , private SourceMonitorListener
{
public:
    OrcBook( const instrument_t& instr, ClientContextPtr spCC, source_t src,
             const std::string& strDesc, SourceMonitorPtr spSMon );
    virtual ~OrcBook();

// IBook interface
    /// Returns true if the book is in a "valid" state.
    /// The meaning of this is book-dependent, however clients typically use it
    /// to test whether they should "trust" the book's contents.
    /// For example, getMidPrice should return a valid price if isOK is true.
    virtual bool isOK() const;

    /// Flushes the book.
    virtual void flushBook();

    /// Returns the mid-price of a book, or 0.0 if the book is not valid or empty.
    /// If one side of the book is empty, OrcBook's getMidPrice will
    /// return the price of the existing side.
    virtual double getMidPrice() const;

    /// Returns the PriceSize at a specified depth and side.
    /// Returns an empty PriceSize if nothing exists at that depth or side.
    virtual PriceSize getNthSide( size_t depth, side_t side ) const;

    /// Returns an object for iterating over BookLevels of a particular side.
    /// Postcondition: The returned iterator will iterate getNumLevels objects.
    /// This abstracts the underlying containers used by a book.
    virtual IBookLevelCIterPtr getBookLevelIter( side_t side ) const;

    /// Returns the number of levels of a particular side of this book.
    virtual size_t getNumLevels( side_t side ) const;

private:
    static const size_t NUM_LEVELS;

    typedef std::vector<BookLevelPtr>   BookLevelVec;
    typedef std::map<double,BookLevelPtr> BookLevelMap;

    /// EventListener interface
    virtual void onEvent( const Msg& msg );

    void onEvent( const OrcOrderDepthMsg& msg );

    /// SourceMonitorListener interface
    virtual void onSMonStatusChange( source_t src, smon_status_t status, smon_status_t old_status,
            const ptime_duration_t& avg_lag_tv, const ptime_duration_t& last_interval_tv );

protected:
    void updateBookLevels(
        side_t side,
        const boost::array<orc_order_depth_level,5>& newLevels,
        std::set<int32_t>& levelsChanged );

    bool isBookLevelChanged( const BookLevelPtr& bookLevel, const orc_order_depth_level& newLevel );

    ClientContextPtr    m_spCC;
    SourceMonitorPtr    m_spSMon;
    uint32_t            m_verbose;

    bool                m_source_ok;

    BookLevelVec        m_bookLevels[2];    // BID = 0, ASK = 1
    BookLevelMap        m_bookLevelsMap[2];

    EventSubPtr         m_eventSub;
};

BB_DECLARE_SHARED_PTR( OrcBook );

}

#endif // BB_CLIENTCORE_ORCBOOK_H

#ifndef BB_CLIENTCORE_BOOKSPEC_H
#define BB_CLIENTCORE_BOOKSPEC_H

/* Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved. */


#include <iosfwd>
#include <stdexcept>
#include <boost/noncopyable.hpp>
#include <boost/optional.hpp>
#include <bb/core/instrument.h>
#include <bb/core/smart_ptr.h>
#include <bb/core/Scripting.h>
#include <bb/core/Error.h>
#include <bb/clientcore/DataAvailability.h>


namespace bb {

BB_FWD_DECLARE_SHARED_PTR(LuaState);
BB_FWD_DECLARE_SHARED_PTR(IBookSpec);
class SourceBookSpec;
class MasterBookSpec;
class IDataRequirements; // DataAvailability.h
class IBook;
class BookBuilder;
struct LuaPrintSettings; // core/LuaCodeGen.h
BB_FWD_DECLARE_SHARED_PTR(IBook);

/// Represents a substitution which replaces all copies of m_fromInstr with m_toInstr.
struct InstrSubst
{
    InstrSubst(const instrument_t &fromInstr, const instrument_t &toInstr)
        : m_fromInstr(fromInstr), m_toInstr(toInstr) {}
    instrument_t m_fromInstr, m_toInstr;
    static const instrument_t &exec(const instrument_t &srcInstr, const boost::optional<InstrSubst> &instrSubst)
        { return instrSubst && instrSubst->m_fromInstr == srcInstr ? instrSubst->m_toInstr : srcInstr; }
};


/// An IBookSpec is a description of a book to be built.
/// Having these classes as data means we can pass them around the network.
/// A BookBuilder is responsible for taking them and actually creating the IBook.
class IBookSpec
{
public:
    BB_DECLARE_SCRIPTING();

    virtual ~IBookSpec() {}

    /// Instantiates a concrete IBook according to the Spec
    virtual IBookPtr build(BookBuilder *builder) const = 0;

    /// deep copy this IBookSpec. If instrSubst is non-null, all copies of fromInstr in the BookSpec
    /// will be replaced with toInstr.
    virtual IBookSpec *clone(const boost::optional<InstrSubst> &instrSubst = boost::none) const = 0;
    inline static IBookSpec *clone(const IBookSpec *e, const boost::optional<InstrSubst> &instrSubst = boost::none)
        { return e ? e->clone(instrSubst) : NULL; }
    inline static IBookSpec *clone(const IBookSpecCPtr &e, const boost::optional<InstrSubst> &instrSubst = boost::none)
        { return clone(e.get(), instrSubst); }

    virtual void hashCombine(size_t &result) const = 0;

    virtual bool compare(const IBookSpec *other) const = 0;

    /// Prints the lua representation of this IPxProviderSpec.
    virtual void print(std::ostream &o, const LuaPrintSettings &ps) const = 0;

    /// Propagates all the required data (instr/src pairs) to the IDataRequirements.
    virtual void getDataRequirements(IDataRequirements *rqs) const = 0;

    /// ensures sure that all fields have been set correctly, or throws runtime_error
    virtual void checkValid() const = 0;

    virtual const instrument_t &getInstrument() const = 0;
};
BB_DECLARE_SHARED_PTR(IBookSpec);


class BookSpecCommon : public IBookSpec
{
public:
    BB_DECLARE_SCRIPTING();

    BookSpecCommon() {}
    BookSpecCommon(instrument_t instr) : m_instrument(instr) {}
    BookSpecCommon(const BookSpecCommon &a, const boost::optional<InstrSubst> &instrSubst);

    instrument_t m_instrument;

    virtual void checkValid() const;
    virtual void hashCombine(size_t &result) const;
    virtual bool compare(const IBookSpec *other) const;

    virtual const instrument_t &getInstrument() const { return m_instrument; }
};
BB_DECLARE_SHARED_PTR(BookSpecCommon);



/// Helper class for converting a string into a BookSpec.
class SpecReader : public boost::noncopyable
{
public:
    SpecReader();

    // parses the given lua code (which should be lua code for a book spec),
    // and returns the book spec
    IBookSpecPtr getBookSpec(std::string txt);

protected:
    LuaStatePtr m_config;
};




namespace util
{
    template< typename T> void checkSourcesValid(const T& srcs)
    {
        typename T::const_iterator i;
        for(i = srcs.begin(); i != srcs.end(); ++i)
        {
            if(!i->isValid())
                BB_THROW_ERROR_SS("source " << (*i) << " is invalid");
        }
    }
}


std::size_t hash_value(const IBookSpec &a);

bool operator==(const IBookSpec &a, const IBookSpec &b);
inline bool operator!=(const IBookSpec &a, const IBookSpec &b) { return !(a == b); }

// convert any IBookSpec object into a human-readable, lua-readable form
std::ostream &operator<<(std::ostream &out, const bb::IBookSpec &book);
void luaprint(std::ostream &out, bb::IBookSpec const &bk, LuaPrintSettings const &ps);

// helpers for hash_map
struct BookSpecHasher : public std::unary_function<size_t, IBookSpecCPtr>
    { size_t operator()(const IBookSpecCPtr &a) const { return hash_value(*a); } };
struct BookSpecComparator : public std::binary_function<bool, IBookSpecCPtr, IBookSpecCPtr>
    { bool operator()(const IBookSpecCPtr &a, const IBookSpecCPtr &b) const { return *a == *b; } };

} // namespace bb

#endif // BB_CLIENTCORE_BOOKSPEC_H

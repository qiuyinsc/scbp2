#ifndef BB_CLIENTCORE_WINDBOOK_H
#define BB_CLIENTCORE_WINDBOOK_H

/* Contents Copyright 2012 Athena Capital Research LLC. All Rights Reserved. */


#include <map>
#include <set>

#include <bb/core/messages.h>
#include <bb/clientcore/ClockMonitor.h>
#include <bb/clientcore/Book.h>
#include <bb/clientcore/SourceBooks.h>

#include <boost/array.hpp>

namespace bb {

BB_FWD_DECLARE_SHARED_PTR( ClientContext );
BB_FWD_DECLARE_SHARED_PTR( SourceMonitor );
class WindOrderDepthMsg;
struct wind_order_depth_level;

class WindBook
    : public BookImpl
    , private IEventDistListener
    , private SourceMonitorListener
{
public:
    WindBook( const instrument_t& instr, ClientContextPtr spCC, source_t src,
             const std::string& strDesc, SourceMonitorPtr spSMon );
    virtual ~WindBook();

// IBook interface
    /// Returns true if the book is in a "valid" state.
    /// The meaning of this is book-dependent, however clients typically use it
    /// to test whether they should "trust" the book's contents.
    /// For example, getMidPrice should return a valid price if isOK is true.
    virtual bool isOK() const;

    /// Flushes the book.
    virtual void flushBook();

    /// Returns the mid-price of a book, or 0.0 if the book is not valid or empty.
    /// If one side of the book is empty, WindBook's getMidPrice will
    /// return the price of the existing side.
    virtual double getMidPrice() const;

    /// Returns the PriceSize at a specified depth and side.
    /// Returns an empty PriceSize if nothing exists at that depth or side.
    virtual PriceSize getNthSide( size_t depth, side_t side ) const;

    /// Returns an object for iterating over BookLevels of a particular side.
    /// Postcondition: The returned iterator will iterate getNumLevels objects.
    /// This abstracts the underlying containers used by a book.
    virtual IBookLevelCIterPtr getBookLevelIter( side_t side ) const;

    /// Returns the number of levels of a particular side of this book.
    virtual size_t getNumLevels( side_t side ) const;
    boost::optional<double> getTurnover() const { return m_opTurnover; }
    boost::optional<int64_t> getTotalVolume() const { return m_opTotalVolume; }

    double getLimitUpPrice() const
    { return m_limitUpPrice ? m_limitUpPrice.get() : 0.0; }
    double getLimitDownPrice() const
    { return m_limitDownPrice ? m_limitDownPrice.get() : 0.0; }

private:

    typedef std::vector<BookLevelPtr>   BookLevelVec;
    typedef std::map<double,BookLevelPtr,bb::FPComp> BookLevelMap;

    /// EventListener interface
    virtual void onEvent( const Msg& msg );

    template <typename MsgType>
    void onEventT( const MsgType& msg );

    void onEvent( const WindOrderDepthMsg& msg );

    /// SourceMonitorListener interface
    virtual void onSMonStatusChange( source_t src, smon_status_t status, smon_status_t old_status,
            const ptime_duration_t& avg_lag_tv, const ptime_duration_t& last_interval_tv );

    typedef std::bitset<32> SeenLevelsSet;

    template<side_t side>
    std::pair<size_t,bool> tryAddLevel( const double price,
            const int32_t size );

    template<side_t side>
    size_t tryDropLevels ( std::bitset<32> seen_levels );

    template <std::size_t Depth, typename MsgType, side_t side>
    int32_t updateBookLevelsT( const MsgType& msg );

    template <side_t side>
    int32_t updateBookLevels(
        const WindOrderDepthMsg& msg );

    template <side_t side>
    int32_t updateBookLevels(
        const WindFuturesMarketDataMsg& msg );

    template <side_t side>
    int32_t updateBookLevels(
        const WindStockMarketDataMsg& msg );

    static bool isBookLevelChanged( const BookLevelPtr& bookLevel, const wind_order_depth_level& newLevel );

    void recordTickInfo( const WindStockMarketDataMsg& msg );
    void recordTickInfo( const WindFuturesMarketDataMsg& msg );

    ClientContextPtr    m_spCC;
    SourceMonitorPtr    m_spSMon;
    uint32_t            m_verbose;

    mutable bool        m_ok;
    mutable bool        m_ok_dirty;
    bool                m_source_ok;

    BookLevelVec        m_bookLevels[2];    // BID = 0, ASK = 1

    EventSubPtr         m_eventSub;

    size_t m_maxNumLevels;

    static const size_t STOCK_MAX_LEVEL = 10;

    wind_order_depth_level msg_wind_order_depth::*m_orderDepthOffsets[2][STOCK_MAX_LEVEL];

    boost::optional<double>    m_opTurnover;
    boost::optional<int64_t>   m_opTotalVolume;

    bool m_inLimitDown;
    boost::optional<double>    m_limitDownPrice;
    bool m_inLimitUp;
    boost::optional<double>    m_limitUpPrice;
};
BB_DECLARE_SHARED_PTR( WindBook );

/// BookSpec corresponding to an Wind book
class WindBookSpec : public SourceBookSpec
{
public:
    BB_DECLARE_SCRIPTING();

    WindBookSpec() : SourceBookSpec() {}
    WindBookSpec(const bb::WindBookSpec&, const boost::optional<bb::InstrSubst>&);
    WindBookSpec(const instrument_t &instr, source_t src)
        : SourceBookSpec(instr, src)
    {}

    virtual IBookPtr build(BookBuilder *builder) const;
    //virtual void checkValid() const;
    //virtual void hashCombine(size_t &result) const;
    //virtual bool compare(const IBookSpec *other) const;
    virtual void print(std::ostream &o, const LuaPrintSettings &ps) const;
    virtual WindBookSpec *clone(const boost::optional<InstrSubst> &instrSubst = boost::none) const;
    //virtual void getDataRequirements(IDataRequirements *rqs) const;
};
BB_DECLARE_SHARED_PTR(WindBookSpec);

}

#endif // BB_CLIENTCORE_WINDBOOK_H

#ifndef BB_CLIENTCORE_SASLCLIENT_H
#define BB_CLIENTCORE_SASLCLIENT_H

/* Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved. */


#include <boost/noncopyable.hpp>

#include <bb/core/MStreamCallback.h>
#include <bb/core/source.h>
#include <bb/core/Uuid.h>

struct sasl_conn;
typedef sasl_conn sasl_conn_t;

namespace bb {

BB_FWD_DECLARE_SHARED_PTR( IRecvTransport );
BB_FWD_DECLARE_SHARED_PTR( ISendTransport );

class SaslClient : public IMStreamCallback, boost::noncopyable
{
public:
    SaslClient( const source_t& source, const char* serviceName ); // caller must later call onEvent()
    virtual ~SaslClient();

    void connect( ISendTransportPtr sendTrans, const std::string& serverFQDN );
    void connect_blocking( ISendTransportPtr sendTrans, IRecvTransportPtr recvTrans, const std::string& serverFQDN );

    bool isAuthenticated() const { return m_authenticated; }
    bool isAuthStarted()   const { return m_authStarted; }

protected:
    virtual void onMessage( const Msg& msg ); // IMStreamCallback impl

private:
    void onEndOfStream();

    void saslInit( const std::string& serverFQDN );
    void saslStart();

    ISendTransportPtr   m_sendTrans;
    source_t            m_source;
    std::string         m_serviceName;
    bool                m_authenticated;
    bool                m_authStarted;
    sasl_conn_t*        m_saslConn;
    Uuid                m_sessionId; // identifies which SaslMsgs in the EventDistributor are for this instance
};
BB_DECLARE_SHARED_PTR( SaslClient );

} // namespace bb

#endif // BB_CLIENTCORE_SASLCLIENT_H

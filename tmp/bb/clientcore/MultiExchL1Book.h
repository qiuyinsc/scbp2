#ifndef BB_CLIENTCORE_MULTIEXCHL1BOOK_H
#define BB_CLIENTCORE_MULTIEXCHL1BOOK_H

/* Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved. */


#include <vector>

#include <bb/core/bbhash.h>
#include <bb/core/hash_map.h>
#include <bb/core/bbassert.h>

#include <bb/clientcore/Book.h>
#include <bb/clientcore/ClockMonitor.h>

namespace bb {

BB_FWD_DECLARE_SHARED_PTR( RandomSource );
BB_FWD_DECLARE_SHARED_PTR( SourceMonitor );

class ExchL1;
typedef bbext::hash_map<mktdest_t, ExchL1> ExchMap;
typedef bbext::hash_map<mktdest_t, int32_t> ExchBBOSizes;

class ExchL1
{
public:
    ExchL1();

    ExchL1( source_t src );

    ~ExchL1() {}

    /// Returns the mid-price of a book, or 0.0 if the book is not valid.
    double getMidPrice() const { return isOK() ? ( m_half[BID]->getPrice() + m_half[ASK]->getPrice() ) / 2.0 : 0.0; }

    /// Returns the PriceSize for the specified side.
    PriceSize getSide( side_t side ) const
    {
        BB_ASSERT( side == BID || side == ASK );
        return m_half[side]->getPriceSize();
    }

    /// Returns a pointer to the BookLevel given the side
    const BookLevelPtr& getBookLevel( side_t side ) const
    {
        BB_ASSERT( side == BID || side == ASK );
        return m_half[side];
    }

    /// Sets the price and size at a given side
    void setPriceSize( double px, int32_t sz, side_t side )
    {
        BB_ASSERT( side == BID || side == ASK );
        m_half[side]->setPriceSize( px, sz );
    }

    /// Sets the BookOrder at a given side
    void setBookOrder( const BookOrder &bo )
    {
        BB_ASSERT( bo.side == BID || bo.side == ASK );
        m_half[bo.side]->setPriceSize( bo.px, bo.sz );
    }

    /// Sets the BookLevel at a given side
    void setBookLevel( const BookLevel &bl, side_t side )
    {
        BB_ASSERT( side == BID || side == ASK );
        m_half[side]->setPriceSize( bl.getPrice(), bl.getSize() );
    }

    /// Checks two sides are valid
    bool isOK() const { return m_half[BID]->isOK() && m_half[ASK]->isOK() && !isCrossed(); }

    /// Checks two sides are crossed
    bool isCrossed() const { return m_half[BID]->getPrice() > m_half[ASK]->getPrice(); }

protected:
    BookLevelPtr m_half[2];
};

class MultiExchBookLevelCIter : public IBookLevelCIter
{
public:
    virtual bool hasNext() const;

    virtual BookLevelCPtr next();

    MultiExchBookLevelCIter( double bbo, const ExchBBOSizes &exchsAtBBO, const ExchMap &m, side_t side )
        : m_level( 0 )
        , m_bbo( bbo )
        , m_exchs( exchsAtBBO )
        , m_map( m )
        , m_side ( side ) { }

private:
    unsigned int m_level;
    double m_bbo;
    const ExchBBOSizes& m_exchs;
    const ExchMap& m_map;
    side_t m_side;
};



/// MultiExchL1Book both defines an interface for a set of L1Books indexed by
/// market destination and provides implementation for bookkeeping and
/// organization functions that MultiExchL1Books will want.
class MultiExchL1Book
    : public    BookImpl
    , protected IEventDistListener
    , protected SourceMonitorListener
{
public:
    MultiExchL1Book( const instrument_t& instr, source_t src, EventDistributorPtr spED, ClockMonitorPtr spCM,
        SourceMonitorPtr spSMon, RandomSourcePtr spRS, const std::string &desc, int _vbose = 0 );
    virtual ~MultiExchL1Book();

    /// Returns the default market used for data consumption
    mktdest_t getDefaultMkt() { return m_mktDefault; }

    /// Sets the default market used for data consumption
    void setDefaultMkt( mktdest_t mkt ) { m_mktDefault = mkt; }

    /// Returns the price/size for a given side in the default market destination
    PriceSize getSide( side_t side ) { return getSideInMkt( side, m_mktDefault ); }

    /// Returns the price/size for a given side in the given market destination
    PriceSize getSideInMkt( side_t side, mktdest_t mkt ) const;

    /// An alternative formulation of getSideInMkt. If the data for
    /// the given side and market isOK, then update 'ps' in place and
    /// return true, otherwise return 'false'.
    bool getSideInMkt( side_t side, mktdest_t mkt, PriceSize* ps) const;

    // Returns true if we have an entry for the market, and if the
    // side for that market isOK.
    bool isSideInMarketOK( side_t side, mktdest_t mkt ) const;

    /// Returns the number of markets the map is currently keeping track of
    size_t getNumMkts() const { return m_mktBooks.size(); }

    /// Returns a vector containing all of the markets for which we
    /// have quote data.
    std::vector<mktdest_t> getMarkets() const;

    /// Returns the best bid or offer of all the markets we track
    std::pair<ExchBBOSizes, double> calcBBO( side_t side ) const;

    /// Calculates the aggregate size of the exchanges at BBO
    int32_t calcAggregateSize( side_t side ) const
    {
        int32_t aggSize = 0;
        for (ExchBBOSizes::const_iterator it = m_exchsAtBBO[side].begin(); it != m_exchsAtBBO[side].end(); ++it)
            aggSize += it->second;
        return aggSize;
    }

    double getBBO( side_t side ) const { return m_bbo[side]; }

    ExchBBOSizes getExchBBOSizes( side_t side ) const { return m_exchsAtBBO[side]; }

    /// Updates market with given BookOrder and given side
    virtual void updateMkt( const BookOrder &bookOrder, bool append );

    // Remove any quote for the given side and market.
    virtual void removeSideInMkt( side_t side, mktdest_t mkt );

    /// Returns false only if book is crossed over maxCrossPxAmt.  Otherwise
    /// returns true (book could be crossed or locked as long as diff is less
    /// than mCPA).
    virtual bool checkBookCross() const { return ( m_bbo[BID] >= m_bbo[ASK] ) && ( m_bbo[ASK] > 0 ); }


    /*
     * IBook interface
     */

    /// Returns true if the book is in a "valid" state.
    /// The meaning of this is book-dependent, however clients typically use it
    /// to test whether they should "trust" the book's contents.
    /// For example, getMidPrice should return a valid price if isOK is true.
    virtual bool isOK() const;

    /// Returns the mid-price of a book, or 0.0 if the book is not valid or empty.
    /// If one side of the book is empty, CmeBook's getMidPrice will
    /// return the price of the existing side.
    virtual double getMidPrice() const;

    /// Returns the PriceSize at a specified depth and side.
    /// Returns an empty PriceSize if nothing exists at that depth or side.
    virtual PriceSize getNthSide( size_t depth, side_t side ) const;

    MarketLevel getInnerMostUncrossedMarket() const;
    PriceSize getBestPriceSizeOutsideOf( const side_t side, const PriceSize& bestPriceSize ) const;
    /// Returns an object for iterating over BookLevels of a particular side.
    /// Postcondition: The returned iterator will iterate getNumLevels objects.
    /// This abstracts the underlying containers used by a book.
    virtual IBookLevelCIterPtr getBookLevelIter( side_t side ) const;

    /// Returns the number of levels of a particular side of this book.
    virtual size_t getNumLevels( side_t side ) const { return isOK() ? 1 : 0; }

    /// Flushes the book. Listeners are notified with onBookFlush.  Otherwise,
    /// listeners are notified onBookChange.
    virtual void flushBook();


    /*
     * EventListener interface
     */

    /// Returns the EventDistributor used by this MultiExchL1Book.
    EventDistributorPtr getEventDistributor() const { return m_spED; }

    /// calls listeners if any visible changes have been made to book. calls
    /// checkIsOK anytime that book_rdy_fg is not true.
    void afterBookChg( const Msg* msg, mtype_t msg_type, source_t msgsrc, symbol_t sym, timeval_t timestamp );

    /// SourceMonitorListener handler
    virtual void onSMonStatusChange(source_t src, smon_status_t status,
                                    smon_status_t old_status, const ptime_duration_t& avg_lag_tv,
                                    const ptime_duration_t& last_interval_tv);

    //virtual void addToMasterBook();

    virtual std::ostream& print( std::ostream &out, int optional_nlvls = -2 ) const;

protected:
    virtual bool defaultPrint() const { return print_level_chg || ( bb::getDefaultMaxPrintLevels() >= 0 ); }

    /// methods that contain common code that all MultiExchL1Books do when onEvent is
    /// called
    bool beforeOnEvent( const Msg& msg );

    void removeMktFromBBO( side_t side, mktdest_t mkt );

    void recalculateBBO( side_t side );

    double calcBBOHelper( ExchBBOSizes& bestMkts, side_t side ) const;

    mutable ExchMap m_mktBooks;
    mktdest_t m_mktDefault;

    // Keeps track of best bid and offer in all messages we have seen
    double m_bbo[2];
    ExchBBOSizes m_exchsAtBBO[2];

    bool        print_level_chg;
    bool        bPrintCheckCross;
    timeval_t   RetryInsert_tv;
    double      m_maxCrossPxAmt;
    uint32_t    m_verbose;
    std::vector<mtype_t> mtypes;

    EventDistributorPtr m_spED;
    SourceMonitorPtr    m_spSMon;
    RandomSourcePtr     m_spRandomSource;

    bool m_addedToMasterbook;

    //static const int NUM_KNOWN_MARKETS = 4;
    static const int CHECK_OK_INTERVAL = 20;
    static const int CHECK_STRUCTURE_INTERVAL = 300;
    //static const int R_CHECK_OK = cm::USER_REASON + 1;
    //static const int R_CHECK_FULL_STRUCTURE = cm::USER_REASON + 2;
    //static const int R_RETRY_INSERT = cm::USER_REASON + 3;
    //static const int DefaultCheckDepth = 3;
};

BB_DECLARE_SHARED_PTR( MultiExchL1Book );

} // namespace bb


#endif // BB_CLIENTCORE_MULTIEXCHL1BOOK_H

#ifndef BB_CLIENTCORE_TAIFEXTICKPROVIDER_H
#define BB_CLIENTCORE_TAIFEXTICKPROVIDER_H

/* Contents Copyright 2013 Athena Capital Research LLC. All Rights Reserved. */


#include <bb/core/messages.h>
#include <bb/clientcore/TickProvider.h>

namespace bb {

class TaifexTradeMsg;
class TaifexSettlementMsg;

BB_FWD_DECLARE_SHARED_PTR( MsgHandler );

class TaifexTickProvider
    : public TickProviderImpl
{
public:
    /// Constructs an TaifexTickProvider for the given instrument,
    /// receiving events from the EventDistributor.
    /// Instrument must be a stock.
    TaifexTickProvider( ClientContext& context, const instrument_t& instr, source_t source,
                      const std::string& desc );

    /// Destructor.
    virtual ~TaifexTickProvider() {}

    /// Returns true if the last obtained tick is OK.
    /// Returns false if there is no last tick.
    virtual bool isLastTickOK() const           { return m_bTickReceived; }





private:
    // Event handlers
    void onTaifexTradeMsg( const TaifexTradeMsg& msg );
    void onTaifexSettlementMsg( const TaifexSettlementMsg &msg );
    void onTaifexFinalSettlementMsg( const TaifexFinalSettlementMsg &msg );

private:
    bool m_bInitialized;
    bool m_bTickReceived;
    int64_t m_lastSerial;
    MsgHandlerPtr m_subTaifexTradeMsg;
    MsgHandlerPtr m_subTaifexSettlementMsg;
    MsgHandlerPtr m_subTaifexFinalSettlementMsg;

    uint32_t m_lastTotalVolume;
    bool m_bFirstTick;
};

BB_DECLARE_SHARED_PTR( TaifexTickProvider );

} // namespace bb

#endif // BB_CLIENTCORE_TAIFEXTICKPROVIDER_H

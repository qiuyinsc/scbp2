#ifndef BB_CLIENTCORE_TICKSTATISTICSTICKPROVIDER_H
#define BB_CLIENTCORE_TICKSTATISTICSTICKPROVIDER_H

/* Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved. */


#include <bb/clientcore/IClockListener.h>
#include <bb/clientcore/ClockMonitor.h>
#include <bb/clientcore/MsgHandler.h>
#include <bb/clientcore/TickProvider.h>
#include <bb/clientcore/ClientContext.h>

namespace bb {

BB_FWD_DECLARE_SCOPED_PTR( TickStatisticsMsg );
BB_FWD_DECLARE_SCOPED_PTR( ClientContext );

/// A TickProvider that generates 'ticks' off of the tickstatistics msg
class TickStatisticsTickProvider
    : public TickProviderImpl
    , private IClockListener
{
public:
    /// Constructs a TickStatisticsTickProvider for the given instrument, receiving events
    /// from the ClientContext's EventDistributor.  Instrument must be a stock.
    TickStatisticsTickProvider( const ClientContext&, const instrument_t&, source_t, const mktdest_t, const std::string& desc);

    /// Destructor.
    virtual ~TickStatisticsTickProvider();

    /// Returns true if the last obtained tick is OK.
    /// Returns false if there is no last tick.
    virtual bool isLastTickOK() const
    {
        return m_tick_ok;
    }

    /// Returns the last TickStatisticsMsg handled by this TickProvider.
    /// This will only have valid values if: isLastTickOK()
    const TickStatisticsMsg& getTickStatisticsMsg() const
    {
        return *m_tradeMsg;
    }

private:
    // Event handlers
    void onTickStatisticsMsg( const TickStatisticsMsg& );

    // IClockListener interface
    virtual void onWakeupCall(const timeval_t& ctv, const timeval_t& swtv, int reason, void* pData);

private:
//    const static int R_ONESECAFTERCLOSE = cm::USER_REASON + 1;

    ClockMonitorPtr       m_spCM;
    MsgHandlerPtr         m_spTickHandler;
    TickStatisticsMsgScopedPtr m_tradeMsg;
    bool                  m_tick_ok;
    mktdest_t             m_primary_mkt;
};

BB_DECLARE_SHARED_PTR( TickStatisticsTickProvider );

} // namespace bb

#endif // BB_CLIENTCORE_TICKSTATISTICSTICKPROVIDER_H

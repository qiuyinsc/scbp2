#ifndef BB_CLIENTCORE_SPAMFILTER_H
#define BB_CLIENTCORE_SPAMFILTER_H

/* Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved. */


#include <bb/core/timeval.h>
#include <bb/core/loglevel.h>
#include <bb/core/smart_ptr.h>

namespace bb {

BB_FWD_DECLARE_SHARED_PTR(IAlert);
BB_FWD_DECLARE_SHARED_PTR(ITimeProvider);


class SpamFilter
{
public:
    /// SpamFilter(..., 10, timeval_t(1,0), 0) will let through at most 10 messages every second,
    /// and only let through messages with a verbosity level <= 0.
    SpamFilter(bb::ITimeProviderCPtr spTimeProvider,
        int spam_counter_limit = 10,
        timeval_t counter_interval = timeval_t(1,0),
        int verbosity_level = 0);
    ~SpamFilter() {}

    void setSpamCounterLimit( int limit )   { m_spam_counter_limit = limit; }
    int getSpamCounterLimit() const         { return m_spam_counter_limit; }

    static void enableSpamFilters( bool enable )    { ms_spam_filters_enabled = enable; }
    static bool areSpamFiltersEnabled()             { return ms_spam_filters_enabled; }

    /// Sends an spam-filtered message to cout.
    void filteredMessageOutput( const std::string& message, int message_verbosity=0 );

    /// 20080220
    /// Returns whether an unknown message passes the filter.  Updates counters
    /// as if SpamFilter were asked to print to standard out.
    /// Intended to decouple filter counting and printing a bit so
    /// this class can be used to filter messages not intended for standard out
    /// (XmlLog in this case).
    bool passMessage( int message_verbosity=0 );

    /// Sends an alert, spam filtered. The verbosity_level limit does not apply to this alert.
    /// Alerts and messages both apply to the same limits.
    void filteredAlertOutput( IAlertPtr alert, const std::string& message, loglevel_t warn_level );

protected:
    void updateCounter();
    bool messageFilter( int verbosity );

    int                   m_spam_counter_limit;
    timeval_t             m_counter_interval;
    bb::ITimeProviderCPtr m_spTimeProvider;
    int                   m_verbosity_level;

    timeval_t             m_interval_start_time;
    int                   m_spam_counter;

    static bool           ms_spam_filters_enabled;
};

BB_DECLARE_SHARED_PTR( SpamFilter );

} // namespace bb

#endif // BB_CLIENTCORE_SPAMFILTER_H

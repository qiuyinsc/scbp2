#ifndef BB_CLIENTCORE_INDEXSPECIFICATIONFACTORY_H
#define BB_CLIENTCORE_INDEXSPECIFICATIONFACTORY_H

/* Contents Copyright 2014 Athena Capital Research LLC. All Rights Reserved. */

#include <bb/core/env.h>
#include <bb/clientcore/IndexSpecification.h>

namespace bb {

class IndexSpecificationFactory
{
public:
    static IIndexSpecificationPtr getIndexSpecificationFromDB( const EnvironmentPtr& env
        , const instrument_t& index_instr
        , const date_t date
        , const std::string& db_profile
        , const std::string& db_table );
};

} // namespace bb

#endif // BB_CLIENTCORE_INDEXSPECIFICATIONFACTORY_H

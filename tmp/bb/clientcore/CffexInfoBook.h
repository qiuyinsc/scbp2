#ifndef BB_CLIENTCORE_CFFEXINFOBOOK_H
#define BB_CLIENTCORE_CFFEXINFOBOOK_H

/* Contents Copyright 2013 Athena Capital Research LLC. All Rights Reserved. */


#include <bb/core/hash_map.h>
#include <bb/core/messages.h>
#include <bb/clientcore/SourceBooks.h>
#include <bb/clientcore/L2Book.h>

namespace bb {

BB_FWD_DECLARE_SHARED_PTR( RandomSource );
BB_FWD_DECLARE_SHARED_PTR( SourceMonitor );


/*****************************************************************************/
//
// CFFEX INFO Book
//
/*****************************************************************************/

struct CffexInfoBookOrder : public BookOrder
{
    CffexInfoBookOrder(const std::string& id_, side_t side_, double px_, int sz_,
                       const instrument_t& instr_, const timeval_t& tv_, source_t src_);

    std::string     _id;
};

class CffexInfoBook
    : public L2Book
{
private:
    typedef L2Book      BaseType;

public:

    CffexInfoBook( const ClientContextPtr& context, const instrument_t& instr, source_t src,
              SourceMonitorPtr sm, const char* desc, int _vbose = 0 );
    virtual ~CffexInfoBook();

    virtual void dropOrder(BookOrder* o);

    /// IEventListener interface
    virtual void onEvent( const Msg& msg );

protected:
    std::pair<long, long> getBookCheckOffset();

private:
    void onEvent(const CffexInfoOrderMsg& msg);
    void onEvent(const CffexInfoTradeMsg& msg);

    void createOrder(const std::string& id_, side_t side_, double px_, int sz_,
                     const instrument_t& instr_, const timeval_t& tv_);

    void processQueuedFills(CffexInfoBookOrder* order);

    typedef boost::function<void (CffexInfoBookOrder*)>                 RemoveLiquidityFunctor;
    typedef bbext::hash_multimap<std::string, RemoveLiquidityFunctor>   RemoveLiquidityFunctorMap;
    typedef bbext::hash_map<std::string, CffexInfoBookOrder*>           BookOrderMap;

    RemoveLiquidityFunctorMap   m_queuedFills;
    BookOrderMap    m_orders;
    EventSubPtr     m_eventSub;
    source_t        m_src;
};

class CffexInfoBookSpec : public SourceBookSpec
{
public:
    BB_DECLARE_SCRIPTING();

    CffexInfoBookSpec() : SourceBookSpec() {}
    CffexInfoBookSpec(const bb::CffexInfoBookSpec&, const boost::optional<bb::InstrSubst>&);
    CffexInfoBookSpec(const instrument_t &instr, source_t src)
        : SourceBookSpec(instr, src)
    {}

    virtual IBookPtr build(BookBuilder *builder) const;
    virtual void print(std::ostream &o, const LuaPrintSettings &ps) const;
    virtual CffexInfoBookSpec *clone(const boost::optional<InstrSubst> &instrSubst = boost::none) const;
};

BB_DECLARE_SHARED_PTR(CffexInfoBookSpec);

} // namespace bb



#endif // BB_CLIENTCORE_CFFEXINFOBOOK_H

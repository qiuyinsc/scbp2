#ifndef BB_CLIENTCORE_DCEL2TICKPROVIDER_H
#define BB_CLIENTCORE_DCEL2TICKPROVIDER_H

/* Contents Copyright 2012 Athena Capital Research LLC. All Rights Reserved. */

#include <bb/core/messages.h>
#include <bb/clientcore/TickProvider.h>

namespace bb {

BB_FWD_DECLARE_SHARED_PTR( ClockMonitor );
BB_FWD_DECLARE_SHARED_PTR( MsgHandler );

class DceL2TickProvider
    : public ITradeSplitter, public TickProviderImpl
{
public:
    /// Constructs an DceL2TickProvider for the given instrument,
    /// receiving events from the EventDistributor.
    /// Instrument must be a stock.
    DceL2TickProvider( ClientContext& context, const instrument_t& instr, source_t source,
                      const std::string& desc );

    /// Destructor.
    virtual ~DceL2TickProvider() {}

    /// Returns true if the last obtained tick is OK.
    /// Returns false if there is no last tick.
    virtual bool isLastTickOK() const { return m_bTickReceived; }

    double getNotionalTurnover() const
    {
        return m_notionalTurnover;
    }

    // ITradeSplitter
    const TradeTick* getAboveVwapTradeTick() const
    {   return &m_aboveVwap; }
    const TradeTick* getBelowVwapTradeTick() const
    {   return &m_belowVwap; }

private:
    // Event handlers
    void onBestAndDeep( const bb::DceL2BestAndDeepMsg& );
    void onStartOfDay( const timeval_t& ctv, const timeval_t& wtv );

    void readCommoditySpec();
    void scheduleNextStartOfDay( const timeval_t& now );

private:
    ClientContext &m_clientContext;
    ClockMonitorPtr m_spClockMonitor;
    bool m_bInitialized;
    bool m_bTickReceived;
    bool m_hasCFD;
    int32_t m_lastSerial;
    uint32_t m_contractSize;
    double m_tickSize;
    double m_notionalTurnover;
    MsgHandlerPtr m_subDceL2BestAndDeepMsg;
    Subscription m_subWakeup;

    TradeTick m_aboveVwap;
    TradeTick m_belowVwap;
};

BB_DECLARE_SHARED_PTR( DceL2TickProvider );

}

#endif // BB_CLIENTCORE_DCEL2TICKPROVIDER_H

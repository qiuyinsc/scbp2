#ifndef BB_CLIENTCORE_BBL1TICKPROVIDER_H
#define BB_CLIENTCORE_BBL1TICKPROVIDER_H

/* Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved. */

#include <bb/core/messages.h>
#include <bb/clientcore/ClientContext.h>
#include <bb/clientcore/IClockListener.h>
#include <bb/clientcore/ClockMonitor.h>
#include <bb/clientcore/MsgHandler.h>
#include <bb/clientcore/TickProvider.h>

namespace bb {

BB_FWD_DECLARE_SCOPED_PTR(BbL1TradeMsg);
BB_FWD_DECLARE_SCOPED_PTR(BbL1ConsEodSummaryMsg);

/// A TickProvider that handles all the special messages from the consolidated
/// trade feeds, i.e. CTS and UTDF.
class BbL1TickProvider
    : public TickProviderImpl
    , private IClockListener
{
public:
    /// Constructs a BbL1TickProvider for the given instrument, receiving events
    /// from the ClientContext's EventDistributor.  Instrument must be a stock.
    BbL1TickProvider(ClientContext& context, const instrument_t& instr, source_t source, const std::string& desc);

    /// Destructor.
    virtual ~BbL1TickProvider();

    /// Returns true if the last obtained tick is OK.
    /// Returns false if there is no last tick.
    virtual bool isLastTickOK() const
    {
        return m_tick_ok;
    }

    /// Returns the last BbL1TradeMsg handled by this TickProvider.
    /// This will only have valid values if: isLastTickOK()
    const BbL1TradeMsg& getBbL1TickMsg() const
    {
        return *m_tradeMsg;
    }

private:
    // Event handlers
    void onBbL1Tick(const BbL1TradeMsg& msg);
    void onBbL1ConsolidatedEodSummary(const BbL1ConsEodSummaryMsg& msg );

    // IClockListener interface
    virtual void onWakeupCall(const timeval_t& ctv, const timeval_t& swtv, int reason, void* pData);

private:
    const static int R_ONESECAFTERCLOSE = cm::USER_REASON + 1;

    ClockMonitorPtr       m_spCM;
    MsgHandlerPtr         m_spBbL1TickHandler, m_spBbL1EodSummaryHandler;
    BbL1TradeMsgScopedPtr m_tradeMsg;
    bool                  m_tick_ok;
    bool                  m_received_opening_tick, m_received_closing_tick;
};

BB_DECLARE_SHARED_PTR(BbL1TickProvider);

} // namespace bb

#endif // BB_CLIENTCORE_BBL1TICKPROVIDER_H

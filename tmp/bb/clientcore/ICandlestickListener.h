#ifndef BB_CLIENTCORE_ICANDLESTICKLISTENER_H
#define BB_CLIENTCORE_ICANDLESTICKLISTENER_H

/* Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved. */


namespace bb {

class Candlestick;
class ICandlestickSeries;

class ICandlestickListener
{
public:
    virtual ~ICandlestickListener() {}
    virtual void onUpdate( const ICandlestickSeries* series, const Candlestick& entry ) = 0;
};

} // namespace bb

#endif // BB_CLIENTCORE_ICANDLESTICKLISTENER_H

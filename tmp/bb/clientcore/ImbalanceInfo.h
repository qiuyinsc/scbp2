#ifndef BB_CLIENTCORE_IMBALANCEINFO_H
#define BB_CLIENTCORE_IMBALANCEINFO_H

/* Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved. */


#include <bb/core/ListenNotify.h>
#include <bb/core/consts/arca_consts.h>
#include <bb/core/side.h>

#include <bb/clientcore/MsgHandler.h>
#include <bb/clientcore/ClientContext.h>

namespace bb {

class ArcaImbalanceMsg;
class IsldNoiiMsg;
class IsldNoiiOldMsg;
class NyseAlertsImbalanceMsg;
class NyseImbalanceMsg;
class TsxTl1MocImbalanceMsg;
class TsxTl2MocImbalanceMsg;
class UserMessageMsg;

/// ImbalanceInfo just holds the most basic information contained in an imbalance message.
class ImbalanceInfo {
public:
    /// Default constructor
    ImbalanceInfo() {}

    ImbalanceInfo( source_t source, const instrument_t& instr,
                   const timeval_t& msg_timestamp, const timeval_t& exch_timestamp );

    virtual ~ImbalanceInfo() {}

    const instrument_t& getInstrument() const   { return m_instr; }
    void setInstrument( const instrument_t& i ) { m_instr = i; }

    source_t getSource() const                  { return m_source; }
    void setSource( source_t src )              { m_source = src; }

    const timeval_t& getMsgTimestamp() const    { return m_msg_timestamp; }
    void setMsgTimestamp( const timeval_t& tv ) { m_msg_timestamp = tv; }

    const timeval_t& getExchTimestamp() const   { return m_exch_timestamp; }
    void setExchTimestamp( const timeval_t& tv ){ m_exch_timestamp = tv; }

    uint32_t getNumImbalanceShares() const         { return m_imbalance_shares; }
    void setNumImbalanceShares(uint32_t num_shares){ m_imbalance_shares = num_shares; }

    side_t getSide() const                      { return m_imbalance_side; }
    void setSide(side_t side)                   { m_imbalance_side = side; }

    virtual std::ostream& print( std::ostream& out ) const;

protected:
    instrument_t    m_instr;
    source_t        m_source;

    timeval_t       m_msg_timestamp;
    timeval_t       m_exch_timestamp;

    uint32_t        m_imbalance_shares;
    side_t          m_imbalance_side;
};
BB_DECLARE_SHARED_PTR( ImbalanceInfo );


/// NyseAlertsImbalanceMsg holds only absolute most basic info.
class NyseAlertsImbalanceInfo
    : public ImbalanceInfo
{
public:
    /// Default Constructor
    NyseAlertsImbalanceInfo() {}

    /// Extract the info from the imbalance msg that we want to remember, and
    NyseAlertsImbalanceInfo( const NyseAlertsImbalanceMsg& msg );
};
BB_DECLARE_SHARED_PTR( NyseAlertsImbalanceInfo );


/// NyseImbalanceMsg holds only absolute most basic info.
class NyseImbalanceInfo
    : public ImbalanceInfo
{
public:
    /// Default Constructor
    NyseImbalanceInfo()
        : m_paired_shares( 0 )
        , m_ref_px( 0 )
        , m_is_closing( true )
        , m_is_open_now( true )
        , m_is_rule123c( true )
    {}

    /// Extract the info from the imbalance msg that we want to remember, and
    NyseImbalanceInfo( const NyseImbalanceMsg& msg );

    /// Construct a NyseImbalanceInfo from a UserMessage
    /// Throws if command is not UMSGC_IMBALANCE_NYSE
    NyseImbalanceInfo( const UserMessageMsg& umsg );

    double getRefPrice() const           { return m_ref_px; }
    void   setRefPrice( double px )      { m_ref_px = px; }
    uint32_t getNumPairedShares() const    { return m_paired_shares; }
    void setNumPairedShares(uint32_t sz)   { m_paired_shares = sz; }
    bool isClosing() const               { return m_is_closing; }
    void setClosing( bool is_closing )   { m_is_closing = is_closing; }
    bool isOpenNow() const               { return m_is_open_now; }
    void setIsOpenNow( bool is_open_now ){ m_is_open_now = is_open_now; }
    bool isRule123c() const              { return m_is_rule123c; }
    void setRule123c( bool is_rule123c ) { m_is_rule123c = is_rule123c; }
    const timeval_t& getNyseTime() const { return m_nyse_timestamp; }
    void setNyseTime(const timeval_t& tv){ m_nyse_timestamp = tv; }
    double getClearingPrice() const      { return m_clearing_px; }

    virtual std::ostream& print(std::ostream& out) const;

protected:
    uint32_t m_paired_shares;
    double m_ref_px, m_clearing_px;
    bool m_is_closing, m_is_open_now, m_is_rule123c;
    timeval_t m_nyse_timestamp;
};
BB_DECLARE_SHARED_PTR( NyseImbalanceInfo );


/// IsldImbalanceInfo holds the more detailed information contained in an noii message.
class IsldImbalanceInfo
    : public ImbalanceInfo
{
public:
    enum ECrossType {
        UNKNOWN   = 0,
        OPENING   = 'O',
        CLOSING   = 'C',
        HALTED    = 'H',
        INTRADAY  = 'I',
        EMERGENCY = 'E'
    };

public:
    /// Default constructor
    IsldImbalanceInfo()
        : m_far_px( 0.0 )
        , m_near_px( 0.0 )
        , m_ref_px( 0.0 )
        , m_px_variation( 0.0 )
        , m_paired_shares( 0 )
        , m_cross_type( UNKNOWN )
    {}

    /// Extract the info from the imbalance msg that we want to remember, and
    /// calculate a couple of useful values (like the price variation)
    template<typename IsldNoiiMsgT>
    IsldImbalanceInfo( const IsldNoiiMsgT& msg );

    /// Construct an IsldImbalanceInfo from a UserMessage
    /// Throws if command is not UMSGC_IMBALANCE_ISLD
    IsldImbalanceInfo( const UserMessageMsg& umsg );

    double getRefPrice() const         { return m_ref_px; }
    void   setRefPrice( double px )    { m_ref_px = px; }
    double getNearPrice() const        { return m_near_px; }
    void   setNearPrice( double px )   { m_near_px = px; }
    double getFarPrice() const         { return m_far_px; }
    void   setFarPrice( double px )    { m_far_px = px; }
    uint32_t getNumPairedShares() const  { return m_paired_shares; }
    void setNumPairedShares(uint32_t sz) { m_paired_shares = sz; }
    double getPriceVariation() const   { return m_px_variation; }
    void setPriceVariation(double var) { m_px_variation = var; }
    ECrossType getCrossType() const    { return m_cross_type; }
    void setCrossType(ECrossType ct)   { m_cross_type = ct; }

    virtual std::ostream& print(std::ostream& out) const;

protected:
    double      m_far_px;
    double      m_near_px;
    double      m_ref_px;
    double      m_px_variation;
    uint32_t    m_paired_shares;
    ECrossType  m_cross_type;
};
BB_DECLARE_SHARED_PTR( IsldImbalanceInfo );


/// ArcaImbalanceInfo holds the more detailed information contained in an arca_imbalance message.
class ArcaImbalanceInfo
    : public ImbalanceInfo
{
public:
    /// Default constructor
    ArcaImbalanceInfo()
        : m_near_px( 0.0 )
        , m_total_imbalance( 0 )
        , m_market_imbalance( 0 )
        , m_paired_shares( 0 )
        , m_cross_type( arca::AT_INVALID )
        , m_auction_timestamp()
    {}

    /// Extract the info from the imbalance msg that we want to remember, and
    /// calculate a couple of useful values (like the price variation)
    ArcaImbalanceInfo( const ArcaImbalanceMsg& msg );

    double getNearPrice() const        { return m_near_px; }
    void   setNearPrice( double px )   { m_near_px = px; }

    /// This is a signed value;  it is negative if the total imbalance is a sell imbalance.
    int32_t getTotalImbalance() const               { return m_total_imbalance; }
    void setTotalImbalance( int32_t sz )            { m_total_imbalance = sz; }

    /// This is always positive.  See getSide() to find out the direction.
    uint32_t getMarketOrderImbalance() const         { return m_market_imbalance; }
    void setMarketOrderImbalance( int32_t market_imb_sz )
    {
        m_imbalance_side = (market_imb_sz < 0) ? ASK : BID;
        m_market_imbalance = m_imbalance_shares = abs(market_imb_sz);
    }

    uint32_t getNumPairedShares() const  { return m_paired_shares; }
    void setNumPairedShares(uint32_t sz) { m_paired_shares = sz; }

    arca::auction_t getCrossType() const    { return m_cross_type; }
    void setCrossType(arca::auction_t ct)   { m_cross_type = ct; }

    virtual std::ostream& print(std::ostream& out) const;

    const timeval_t& getAuctionTimestamp() const   { return m_auction_timestamp; }
    void setAuctionTimestamp( const timeval_t& tv ){ m_auction_timestamp = tv; }

protected:
    double           m_near_px;
    int32_t          m_total_imbalance;
    uint32_t         m_market_imbalance;
    uint32_t         m_paired_shares;
    arca::auction_t  m_cross_type;
    timeval_t        m_auction_timestamp;
};
BB_DECLARE_SHARED_PTR( ArcaImbalanceInfo );


/// TsxImbalanceInfo only holds the absolute most basic info... plus a mkt dest
class TsxImbalanceInfo
    : public ImbalanceInfo
{
public:
    /// Default constructor
    TsxImbalanceInfo() { }

    /// Extract the info from the imbalance msg that we want to remember
    TsxImbalanceInfo( const TsxTl1MocImbalanceMsg& msg );

    /// Extract the info from the imbalance msg that we want to remember
    TsxImbalanceInfo( const TsxTl2MocImbalanceMsg& msg );

    mktdest_t getOrigMkt() const          { return m_orig_mkt; }
    void      setOrigMkt( mktdest_t mkt ) { m_orig_mkt = mkt; }

protected:
    mktdest_t m_orig_mkt;
};
BB_DECLARE_SHARED_PTR( TsxImbalanceInfo );


// TODO: maybe it would be better to have distinct imbalance providers?
/// Provides notifications of Imbalances.
class ImbalanceProvider
{
    BB_DECLARE_LISTENER_2_PREFIX( Isld, ImbalanceProvider, IsldImbalanceInfoPtr );
    BB_DECLARE_LISTENER_2_PREFIX( NyseAlerts, ImbalanceProvider, NyseAlertsImbalanceInfoPtr );
    BB_DECLARE_LISTENER_2_PREFIX( Nyse, ImbalanceProvider, NyseImbalanceInfoPtr );
    BB_DECLARE_LISTENER_2_PREFIX( Arca, ImbalanceProvider, ArcaImbalanceInfoPtr );
    BB_DECLARE_LISTENER_2_PREFIX( Tsx, ImbalanceProvider, TsxImbalanceInfoPtr );

public:
    ImbalanceProvider( ClientContextPtr spCC, const std::vector<source_t> &srcs );

    /// Notifies listeners of fake Imbalance from a user message.
    /// Returns true if a valid Imbalance message was injected, false otherwise.
    bool injectFromUserMessage( const UserMessageMsg& msg );

    /// Adds the given IsldListener to this ImbalanceProvider.
    /// IsldListenerSubPtr will be initialized with a new subscription to the object.
    /// When the IsldListenerSubPtr is released, the listener is unsubscribed.
    void addIsldListener( Subscription &outSub, const IsldListener& listener )
    {   m_notifier_isld.subscribeListener( outSub, listener ); }

    /// Adds the given NyseAlertsListener to this ImbalanceProvider.
    /// Subscription will be initialized with a new subscription to the object.
    /// When the Subscription is released, the listener is unsubscribed.
    void addNyseAlertsListener( Subscription &outSub, const NyseAlertsListener& listener )
    {   m_notifier_nyse_alerts.subscribeListener( outSub, listener ); }

    /// Adds the given NyseListener to this ImbalanceProvider.
    /// Subscription will be initialized with a new subscription to the object.
    /// When the Subscription is released, the listener is unsubscribed.
    void addNyseListener( Subscription &outSub, const NyseListener& listener )
    {   m_notifier_nyse.subscribeListener( outSub, listener ); }

    /// Adds the given ArcaListener to this ImbalanceProvider.
    /// Subscription will be initialized with a new subscription to the object.
    /// When the Subscription is released, the listener is unsubscribed.
    void addArcaListener( Subscription &outSub, const ArcaListener& listener )
    {   m_notifier_arca.subscribeListener( outSub, listener ); }

    /// Adds the given TsxListener to this ImbalanceProvider.
    /// Subscription will be initialized with a new subscription to the object.
    /// When the Subscription is released, the listener is unsubscribed.
    void addTsxListener( Subscription &outSub, const TsxListener& listener )
    {   m_notifier_tsx.subscribeListener( outSub, listener ); }

protected:
    void onIsldImbalance( const IsldNoiiMsg& msg );
    void onIsldImbalanceOld( const IsldNoiiOldMsg& msg );
    void onNyseAlertsImbalance( const NyseAlertsImbalanceMsg& msg );
    void onNyseImbalance( const NyseImbalanceMsg& msg );
    void onArcaImbalance( const ArcaImbalanceMsg& msg );
    void onTsxImbalance( const TsxTl1MocImbalanceMsg& msg );
    void onTsxCdfImbalance( const TsxTl2MocImbalanceMsg& msg );

protected:
    ClientContextPtr    m_spCC;
    MsgHandlerPtr       m_spIsldHandler, m_spIsldOldHandler, m_spNyseHandler, m_spNyseAlertsHandler,
                        m_spArcaHandler, m_spTsxTl1Handler, m_spTsxTl2Handler;

    TNotifier2<ImbalanceProvider, IsldImbalanceInfoPtr> m_notifier_isld;
    TNotifier2<ImbalanceProvider, NyseAlertsImbalanceInfoPtr> m_notifier_nyse_alerts;
    TNotifier2<ImbalanceProvider, NyseImbalanceInfoPtr> m_notifier_nyse;
    TNotifier2<ImbalanceProvider, ArcaImbalanceInfoPtr> m_notifier_arca;
    TNotifier2<ImbalanceProvider, TsxImbalanceInfoPtr> m_notifier_tsx;
};
BB_DECLARE_SHARED_PTR( ImbalanceProvider );


template<typename IsldNoiiMsgT>
IsldImbalanceInfo::IsldImbalanceInfo( const IsldNoiiMsgT& msg )
    : ImbalanceInfo( msg.hdr->source, instrument_t::stock(msg.hdr->symbol),
                     msg.hdr->time_sent, msg.getIsldTime() )
    , m_far_px( msg.getFarPx() )
    , m_near_px( msg.getNearPx() )
    , m_ref_px( msg.getRefPx() )
    , m_px_variation( 0.0 )
    , m_paired_shares( msg.getPairedShares() )
    , m_cross_type( static_cast<ECrossType>(msg.getCrossType()) )
{
    m_imbalance_shares = msg.getImbalShares();

    switch ( msg.getImbalDir() ) {
    case 'B':
        m_imbalance_side = BID;
        break;

    case 'S':
        m_imbalance_side = ASK;
        break;

    case 'N':
    case 'O':
    default:
        m_imbalance_side = SIDE_INVALID;
        // ### throw something or log warning
        break;
    }

    if ( m_near_px > m_ref_px )
        m_px_variation = (m_near_px - m_ref_px) / m_ref_px;
    else
        m_px_variation = (m_ref_px - m_near_px) / m_ref_px;
}


/// Stream operator to convert ImbalanceInfo into human readable form.
/// Calls print virtual method of ImbalanceInfo object.
std::ostream &operator <<( std::ostream &out, const bb::ImbalanceInfo &i );


} // namespace bb


#endif // BB_CLIENTCORE_IMBALANCEINFO_H

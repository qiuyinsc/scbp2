#ifndef BB_CLIENTCORE_MARKETLEVELPROVIDER_H
#define BB_CLIENTCORE_MARKETLEVELPROVIDER_H

/* Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved. */


#include <bb/core/ListenNotify.h>
#include <bb/core/mktdest.h>
#include <bb/core/smart_ptr.h>
namespace bb {

// Forward declarations
class MarketLevel;
class timeval_t;
class instrument_t;


/// Interface for classes which provide market levels for a given symbol from a
/// given market destination.
class IMarketLevelProvider
{
    BB_DECLARE_LISTENER(IMarketLevelProvider);

public:
    /// Destructor
    virtual ~IMarketLevelProvider() { }

    /// @return the mktdest of this market level provider
    virtual mktdest_t getMktDest() const = 0;

    /// @return the current MarketLevel
    virtual const MarketLevel& getMarketLevel() const = 0;

    /// @return the instrument_t associated with this PriceProvider
    virtual symbol_t getSymbol() const = 0;

    /// @return the instrument_t associated with this PriceProvider
    virtual const instrument_t& getInstrument() const = 0;

    /// @return the timeval at which this PriceProvider last updated.
    virtual const timeval_t& getLastUpdateTime() const = 0;

    /// @return if getMarketLevel would return a valid MarketLevel at this time
    virtual bool isMarketLevelOk() const = 0;

    /// Adds a listener to be notified whenever the market level might have
    /// changed.  Subscription is an output parameter.  When the returned
    /// Subscription is released, the listener is unsubscribed.
    virtual void addMarketLevelListener(Subscription &sub, const Listener& listener) const = 0;
};
BB_DECLARE_SHARED_PTR(IMarketLevelProvider);
/// IMarketLevelProvider implements the common addListener support.
class MarketLevelProviderImpl : public IMarketLevelProvider
{
public:
    virtual void addMarketLevelListener(Subscription &out_listener_sub, const Listener& listener) const
    {
        m_notifier.subscribeListener(out_listener_sub, listener);
    }

protected:
    void notifyMarketLevelChanged()
    {
        m_notifier.notifyListeners(*this);
    }

    mutable TNotifier<IMarketLevelProvider> m_notifier;
};

} // namespace bb

#endif // BB_CLIENTCORE_MARKETLEVELPROVIDER_H

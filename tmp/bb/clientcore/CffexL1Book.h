#ifndef BB_CLIENTCORE_CFFEXL1BOOK_H
#define BB_CLIENTCORE_CFFEXL1BOOK_H

/* Contents Copyright 2012 Athena Capital Research LLC. All Rights Reserved. */


#include <boost/optional.hpp>
#include <bb/core/timeval.h>
#include <bb/clientcore/L1Book.h>
#include <bb/clientcore/SourceBooks.h>

namespace bb {

class ClientContext;
class CffexQdMsg;

BB_FWD_DECLARE_SHARED_PTR( MsgHandler );
BB_FWD_DECLARE_SHARED_PTR( SourceMonitor );
BB_FWD_DECLARE_SHARED_PTR( RandomSource );
BB_FWD_DECLARE_SHARED_PTR( ClientContext );

class CffexL1Book : public L1Book
{
public:
    CffexL1Book( const ClientContextPtr& context, const instrument_t& instr, source_t src
        , SourceMonitorPtr sm, const std::string& desc, int _vbose=0);
    CffexL1Book( const ClientContextPtr& context, const instrument_t& instr, source_t src
        , const std::string& desc, int _vbose = 0 );
    virtual ~CffexL1Book() {}

    boost::optional<double> getLimitUpPrice() const
    { return m_limitUpPrice; }
    boost::optional<double> getLimitDownPrice() const
    { return m_limitDownPrice; }
    timeval_t getExchangeTime() const
    { return m_exchangeTv; }

    virtual bool isOK() const;
    virtual double getMidPrice() const;

protected:
    void onCffexQdMsg( const CffexQdMsg& msg );

protected:
    MsgHandlerPtr m_subCffexQdMsg;
    boost::optional<double> m_limitUpPrice;
    boost::optional<double> m_limitDownPrice;
    timeval_t m_exchangeTv;
};

BB_DECLARE_SHARED_PTR( CffexL1Book );

/// BookSpec corresponding to an Shfe book
class CffexL1BookSpec : public SourceBookSpec
{
public:
    BB_DECLARE_SCRIPTING();

    CffexL1BookSpec() : SourceBookSpec() {}
    CffexL1BookSpec(const bb::CffexL1BookSpec&, const boost::optional<bb::InstrSubst>&);
    CffexL1BookSpec(const instrument_t &instr, source_t src)
        : SourceBookSpec(instr, src)
    {}

    virtual IBookPtr build(BookBuilder *builder) const;
    virtual void print(std::ostream &o, const LuaPrintSettings &ps) const;
    virtual CffexL1BookSpec *clone(const boost::optional<InstrSubst> &instrSubst = boost::none) const;
};
BB_DECLARE_SHARED_PTR(CffexL1BookSpec);

} // namespace bb

#endif // BB_CLIENTCORE_CFFEXL1BOOK_H

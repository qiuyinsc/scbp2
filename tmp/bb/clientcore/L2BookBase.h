#ifndef BB_CLIENTCORE_L2BOOKBASE_H
#define BB_CLIENTCORE_L2BOOKBASE_H

/* Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved. */


#include <map>
#include <bb/clientcore/Book.h>
#include <bb/clientcore/ClockMonitor.h>

namespace bb {

BB_FWD_DECLARE_SHARED_PTR( RandomSource );
BB_FWD_DECLARE_SHARED_PTR( SourceMonitor );
BB_FWD_DECLARE_SHARED_PTR( ClientContext );

/// 2011-11-22: L2BookBase was built by taking all code from L2Book that did not involve orders.
/// As such, it is hoped it will be a base class for L2 books whether or not they use orders
/// or are built using one or more aggregate data feeds.
class L2BookBase
    : public Book
{
public:
    L2BookBase( const ClientContextPtr& cc, const instrument_t& instr,
                SourceMonitorPtr spSMon, const char* desc, int _vbose = 0 );

    virtual ~L2BookBase();

    /// Have the book print levels whenever the book changes within
    /// print_level_chg from the front -1 turns off printing of levels,
    /// 0 prints when best mkt changes, and so on
    void setPrintLevelChg( int _print_level_chg ) { print_level_chg = _print_level_chg; }
    int getPrintLevelChg() const { return print_level_chg;  };

    /// Returns the mid-price of a book, or 0.0 if the book is not valid.
    virtual double getMidPrice() const;

    /// Returns the PriceSize at a specified depth and side.
    /// Returns an empty PriceSize if nothing exists at that depth or side.
    virtual PriceSize getNthSide( size_t depth, side_t side ) const;

    /// Returns an object for iterating over BookLevels of a particular side.
    /// Postcondition: The returned iterator will iterate getNumLevels objects.
    /// This abstracts the underlying containers used by a book; book implementations
    /// will implement their own iterator and pass them back.
    virtual IBookLevelCIterPtr getBookLevelIter( side_t side ) const;

    /// Returns the number of levels of a particular side of this book.
    /// Note that empty levels above the "bottom" level are counted.
    virtual size_t getNumLevels( side_t side ) const;

public:
    /// Returns the EventDistributor used by this L2BookBase.
    EventDistributorPtr getEventDistributor() const { return m_spED; }

    /// Checks book, sets book_rdy_fg accordingly, and returns book_rdy_fg value.
    bool checkIsOK() const;

    /// Returns false only if book is crossed over maxCrossPxAmt.
    /// Otherwise returns true (book could be crossed or locked as long as
    /// diff is less than mCPA).
    virtual bool checkBookCross() const;
    /// calls listeners if any visible changes have been made to book. calls
    /// checkIsOK anytime that book_rdy_fg is not true.
    void afterBookChg( const Msg* msg, mtype_t msg_type, source_t msgsrc, symbol_t sym, timeval_t timestamp );

    /// IMPORTANT: any pointers or iterators to BookOrders or BookLevels will
    /// be invalidated when flushBook is called, and might be invalidated by
    /// dropBookLevel
    /// Returns true if it drops the level.
    virtual bool dropBookLevel( side_t side, double px );
    virtual bool dropBookLevelImpl( side_t side, double px );

    /// Flushes the book. Listeners are notified with onBookFlush.  Otherwise, listeners are notified onBookChange.
    virtual void flushBook();

    /// ClockListener interface
    virtual void onWakeupCall( const timeval_t& ctv, const timeval_t& swtv, int reason, void* pData );

    /// SourceMonitorListener handler
    virtual void onSMonStatusChange( source_t src, smon_status_t status,
                                     smon_status_t old_status, const ptime_duration_t& avg_lag_tv,
                                     const ptime_duration_t& last_interval_tv );

    /// Returns the BookLevel for the specified side and price.
    /// If pDepth is passed, fills it with the depth at which the price was found.
    /// If it is not found, returns an empty BookLevelCPtr and sets *pDepth to -1.
    ///
    /// This more performant than the generic bb::getBookLevelAtPrice, since it directly
    /// accesses the levels, rather than using polymorphic iterators.
    BookLevelCPtr getBookLevelAtPrice( side_t side, double px, int32_t* pDepth = NULL ) const;
    BookLevel*    getBookLevelPointerAtPrice( side_t side, double px, int32_t* pDepth = NULL ) const;

    virtual void addToMasterBook();

    const bookhalf_t& getBookHalf( side_t side ) const
    {
        BB_ASSERT( side == BID || side == ASK );
        return bhalf[side];
    }

protected:
    virtual int32_t defaultMaxPrintLevels() const
    {
        return print_level_chg == -1 ? bb::getDefaultMaxPrintLevels() : print_level_chg + 2;
    }

    /// methods that contain common code that all L2BookBases do when onEvent is
    /// called
    bool beforeOnEvent( const Msg& msg );

    // Flushes a side of the book.
    // Does not call any listeners
    void flushSide( side_t side, int& level_cnt );

    /// returns a (deterministic) random offset to use for the book checks
    virtual std::pair<long, long> getBookCheckOffset() = 0;

    bookhalf_t bhalf[2];
    int print_level_chg;
    double m_maxCrossPxAmt;
    bool m_bPrintCheckCross;
    ptime_duration_t m_retryInsertTime;

    EventDistributorPtr m_spED;
    SourceMonitorPtr m_spSMon;
    RandomSourcePtr m_spRandomSource;

    mutable BookLevel m_tempBL; // used for searching, kept here to avoid reallocation

    bool m_addedToMasterbook;

    static const int CHECK_OK_INTERVAL = 20;
    static const int CHECK_STRUCTURE_INTERVAL = 300;
    static const int R_CHECK_OK = cm::USER_REASON + 1;
    static const int R_CHECK_FULL_STRUCTURE = cm::USER_REASON + 2;
    static const int DefaultCheckDepth = 3;

    static const timeval_t CHECK_OK_INTERVAL_TV;
    static const timeval_t CHECK_STRUCTURE_INTERVAL_TV;
};

BB_DECLARE_SHARED_PTR( L2BookBase );


/// Returns the BookLevel for the specified side and price.
/// If pDepth is passed, fills it with the depth at which the price was found.
/// If it is not found, returns an empty BookLevelCPtr and sets *pDepth to -1.
/// Specialization for L2BookBase.
template<>
inline BookLevelCPtr getBookLevelAtPrice( const L2BookBase& l2book, side_t side, double px, int32_t* pDepth )
{
    return l2book.getBookLevelAtPrice( side, px, pDepth );
}


} // namespace bb


#endif // BB_CLIENTCORE_L2BOOKBASE_H

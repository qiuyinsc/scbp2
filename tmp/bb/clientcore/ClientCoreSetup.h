#ifndef BB_CLIENTCORE_CLIENTCORESETUP_H
#define BB_CLIENTCORE_CLIENTCORESETUP_H

/* Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved. */


#include <vector>
#include <boost/function.hpp>
#include <boost/program_options/options_description.hpp>
#include <boost/program_options/positional_options.hpp>
#include <boost/program_options/variables_map.hpp>
#include <bb/core/Log.h>
#include <bb/core/instrument.h>
#include <bb/core/InstrSource.h>
#include <bb/clientcore/BookBuilder.h>
#include <bb/clientcore/ClientContext.h>
#include <bb/clientcore/ConfigFile.h>


namespace bb {
// Forward declarations
class ClientCoreSetup;

BB_FWD_DECLARE_SHARED_PTR(LuaState);
BB_FWD_DECLARE_SHARED_PTR(Environment);

class ProblemList
{
public:
    enum SEVERITY { WARNING = 1, ERROR };
    class Problem
    {
    public:
        Problem( SEVERITY s, const std::string& desc ) : m_severity( s ), m_desc( desc ) {}

        SEVERITY m_severity;
        std::string m_desc;
    };

public:
    ProblemList() : m_numFatalErrors( 0 ) {}

    void add( SEVERITY s, const std::string& msg )
    {
        boost::shared_ptr<Problem> problem( new Problem( s, msg ) );
        m_problemList.push_back( problem );
        if( s == ERROR ) m_numFatalErrors++;
    }

    uint32_t getNumFatalErrors() const { return m_numFatalErrors; }
    void print( std::ostream& os ) const
    {
        std::vector< boost::shared_ptr<Problem> >::const_iterator it = m_problemList.begin();
        for(; it != m_problemList.end(); ++it )
        {
            switch( (*it)->m_severity )
            {
            case WARNING: os << "WARNING: "; break;
            case ERROR:
            default:      os << "ERROR: "; break;
            }
            os << (*it)->m_desc << std::endl;
        }
    }

protected:
    std::vector< boost::shared_ptr<Problem> > m_problemList;
    uint32_t m_numFatalErrors;
};

class SetupOptions
{
public:
    typedef
    boost::function< void (const boost::program_options::variables_map& vm, ProblemList* problems) >
    CheckOptionsFunc;

    SetupOptions( const std::string& section_id, const CheckOptionsFunc& f )
        : m_optionsDesc( section_id ), m_checkOptionsFunc( f ) {}

    const boost::program_options::options_description& getOptionsDesc() const { return m_optionsDesc; }
    CheckOptionsFunc getCheckOptionsFunc() const { return m_checkOptionsFunc; }
    boost::program_options::options_description_easy_init add_options() { return m_optionsDesc.add_options(); }

protected:
    boost::program_options::options_description m_optionsDesc;
    CheckOptionsFunc m_checkOptionsFunc;
};
BB_DECLARE_SHARED_PTR(SetupOptions);

/// most client programs will use this object (or inheritors from this object) to set
/// up basically everything.
/// typically, client should call setup, setupSymbols, and then run, in that order and
/// with only those calls
///
/// \bParsing commandline options\b
///
/// ClientCoreSetup defines a basic set of command line options it recognizes.  Command line options are
/// registered in the ClientCoreSetup::addOptions(). Derived classes can extend default options by
/// overriding addOptions() and creating SetupOptions objects to define other options and a handler
/// function to process them.  The new options can be registered by calling ClientCoreSetup::registerOptions()
/// with the new SetupOptions.
///
/// ClientCoreSetup has a getClientCoreSetupOptions() that can return reconfigured Client Core commandline
/// options to be used instead of the default options.

class ClientCoreSetup
{
public:
    typedef ConfigFile::ProductEntry ProductEntry;
    typedef ConfigFile::ProductVec_t ProductVec_t;

    ClientCoreSetup( int argc, char** argv );
    virtual ~ClientCoreSetup();

    /// parses command line settings and does any global setup.
    /// if the cmdline is invalid, will throw bb::UsageError with an error message.
    virtual void setup();

    virtual bool setupSymbols();
    virtual void loadLuaState( const std::string& config_file );

    /// this will enter the run loop of the EventDistributor
    virtual void run();
    virtual void exit(); // exits the main loop prematurely
    /// installs signal handler that'll exit the main loop. call setup() before this.
    void sigExit(int sig);

    virtual void printSettings(bool endl_fg=true);

    ClientContextPtr getClientContext() const { return m_spClientContext; }
    bool isRunLive() const { return m_runLive; }
    bool shouldLoadProductFile() const { return m_bLoadProductFile; }
    int32_t getVerboseLevel() const { return m_verboseLevel; }
    ConfigFilePtr getConfigFile() const { return m_configFile; }
    IBookBuilderPtr getBookBuilder() const { return m_bookBuilder; }
    int32_t getRunLiveValue() const { return m_runLiveValue; }

    ConfigFile::ProductEntryCPtr getTargetProduct() const { return m_configFile->getTargetProduct(); }
    virtual void getTargetProducts(ProductVec_t *prods) const;

    /// returns a list of all the instruments which are being included in this clientcore run.
    virtual void getAllProducts(ProductVec_t *prods) const;

    /// if the given instrument is known by CCS, returns the info CCS knows about that instrument.
    ConfigFile::ProductEntryCPtr findInstrument( const instrument_t& instr ) const;

    /// returns the reference book for the given instrument, creating it if it doesn't exist yet.
    /// The RefBook is a book which includes all the sources for the given instrument
    virtual IBookPtr getRefBook(const instrument_t &instr) const { return getRefBook( *(findInstrument(instr).get()) ); }

    /// Workaround for caching/smon limitations:
    /// We build all the signals/books, and then see if anything actually needs
    /// to be built from scratch (vs. read from the cache). If we are building stuff
    /// from scratch, add all the instr/srcs from smon_baseline_data in the config file
    /// to ensure that the source monitors won't spuriously complain.
    /// Histo only/cache only for now.
    void setupSmonBaselineData();
    /// helper for setupSmonBaselineData. Returns true if anything is being built from scratch.
    /// Default is always do the smon baseline, but signals caching overrides that.
    virtual bool smonBaselineDataRequired() { return true; }
    virtual const std::vector<InstrSource>& getSmonBaselineData() const;

    const date_t& getStartDate() const { return startdate; }
    const date_t& getEndDate() const { return enddate; }

    virtual void setIdentity( const std::string& id );
    virtual const std::string& getIdentity() const;

    std::string getProgramName();

protected:
    SetupOptionsPtr getClientCoreSetupOptions(bool bstocksfile_options = true);
    void checkClientCoreSetupOptions(const boost::program_options::variables_map& vm
        , ProblemList* problems, bool bcheck_stocksfile);

    /// Adds all cmdline options to the options_description.
    virtual void addOptions(boost::program_options::options_description &allOptions);

    /// This is called once the cmdline options have been parsed into m_programOptions.
    /// It invokes handlers who should copy those settings out into local variables or whatever,
    /// and add any problems with the settings to the ProblemsList. setup() will throw if anything
    /// with an ERROR severity is added.
    void checkOptionsImp(const boost::program_options::variables_map& vm, ProblemList* problems);

    /// Creates a bb::ConfigFile instance for this setup run. You can override this to use a
    /// different subclass of bb::ConfigFile.
    virtual ConfigFilePtr makeConfig();
    /// if end_time_reached is false, we're exiting prematurely
    /// because of a ctrl-c or user message or something like that.
    virtual void onExit(bool end_time_reached);

    virtual ClientContextPtr makeClientContext( const EnvironmentPtr& env, const timeval_t& starttv, const timeval_t& endtv );

    bool addDataSymbol(const ConfigFile::ProductEntry &prod);

    IBookPtr getRefBook(const ConfigFile::ProductEntry &prod) const;

    /// Registers the options and handler func described in the options param for commandline
    /// parsing with this setup object.
    void registerOptions(boost::program_options::options_description &allOptions
        , boost::shared_ptr<SetupOptions> options);
    
    boost::program_options::positional_options_description& positionalOptions() { return m_positionalOptions; }

    /// returns true if option is loaded.
    template <typename T> bool assignOption( T* dest
        , const boost::program_options::variables_map& vm
        , const std::string& id, bool brequired = false )
    {
        if( vm.count( id ) )
        {
            (*dest) = vm[id].as<T>();
            return true;
        }
        else if( brequired )
            throw id + std::string( " is required, but was not specified." );
        return false;
    }

    template <typename T> void assignOption( boost::optional<T>* dest
        , const boost::program_options::variables_map& vm, const std::string& id, bool brequired = false )
    {
        if( vm.count( id ) )
            (*dest) = vm[id].as<T>();
        else if( brequired )
            throw id + std::string( " is required, but was not specified." );
    }

protected:
    int m_argc;
    char** m_argv;
    LuaStatePtr m_config;

    ClientContextPtr m_spClientContext;

    std::string m_strSymbolsFilename;

    bool m_runLive;
    int m_runLiveValue;
    int m_verboseLevel;
    std::string m_startDateStr, m_endDateStr;
    date_t startdate;
    date_t enddate;
    timeval_t starttv;
    timeval_t endtv;
    bool m_prematureExit;
    bool m_bLoadProductFile;

    ConfigFilePtr m_configFile;
    IBookBuilderPtr m_bookBuilder;

    std::vector<SetupOptions::CheckOptionsFunc> m_checkOptionsFuncList;
    boost::optional<std::string> m_identity;

    boost::program_options::positional_options_description m_positionalOptions;
};
BB_DECLARE_SHARED_PTR(ClientCoreSetup);


} // namespace bb


#endif // BB_CLIENTCORE_CLIENTCORESETUP_H

#ifndef BB_CLIENTCORE_WINDTICKPROVIDER_H
#define BB_CLIENTCORE_WINDTICKPROVIDER_H

/* Contents Copyright 2012 Athena Capital Research LLC. All Rights Reserved. */


#include <bb/core/messages.h>
#include <bb/clientcore/TickProvider.h>

namespace bb {

class WindPriceFeedMsg;

BB_FWD_DECLARE_SHARED_PTR( ClockMonitor );
BB_FWD_DECLARE_SHARED_PTR( MsgHandler );

class WindTickProvider
    : public ITradeSplitter
    , public TickProviderImpl
{
public:
    /// Constructs an WindTickProvider for the given instrument,
    /// receiving events from the EventDistributor.
    /// Instrument must be a stock.
    WindTickProvider( ClientContext& context, const instrument_t& instr, source_t source,
                      const std::string& desc );

    /// Destructor.
    virtual ~WindTickProvider() {}

    /// Returns true if the last obtained tick is OK.
    /// Returns false if there is no last tick.
    virtual bool isLastTickOK() const { return m_bTickReceived; }

    uint64_t getNotionalTurnover() const { return m_notionalTurnover; }
    uint64_t getOpenInterest() const { return m_openInterest; }

    // ITradeSplitter
    virtual const TradeTick* getAboveVwapTradeTick() const
    {   return &m_aboveVwap; }
    virtual const TradeTick* getBelowVwapTradeTick() const
    {   return &m_belowVwap; }

private:
    // Event handlers
    void onWindTradeMsg( const WindTradeMsg& msg );
    void onWindOrderDepthMsg( const WindOrderDepthMsg& msg );
    void onWindIndexDataMsg( const WindIndexDataMsg& msg );
    void onWindFuturesMarketDataMsg( const WindFuturesMarketDataMsg& msg );
    void onWindStockTransactionMsg( const WindStockTransactionMsg& msg );
    void onStartOfDay( const timeval_t& ctv, const timeval_t& wtv );

    void scheduleNextStartOfDay( const timeval_t& now );

private:
    bool m_hasCommSpec;
    double m_tickSize;
    double m_contractSize;
    ClockMonitorPtr m_spClockMonitor;
    bool m_bInitialized;
    bool m_bTickReceived;
    int32_t m_lastSerial;
    uint64_t m_notionalTurnover;
    MsgHandlerPtr m_subWindTradeMsg;
    MsgHandlerPtr m_subWindOrderDepthMsg;
    MsgHandlerPtr m_subWindIndexDataMsg;
    MsgHandlerPtr m_subWindFuturesMarketDataMsg;
    MsgHandlerPtr m_subWindStockTransactionMsg;
    Subscription m_subWakeup;

    double m_lastTurnover;
    uint64_t m_openInterest;

    TradeTick m_aboveVwap;
    TradeTick m_belowVwap;

};

BB_DECLARE_SHARED_PTR( WindTickProvider );

}

#endif // BB_CLIENTCORE_WINDTICKPROVIDER_H

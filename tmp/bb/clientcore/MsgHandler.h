#ifndef BB_CLIENTCORE_MSGHANDLER_H
#define BB_CLIENTCORE_MSGHANDLER_H

/* Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved. */


#include <boost/function.hpp>
#include <bb/core/bbassert.h>
#include <bb/clientcore/EventDist.h>


namespace bb {

class MsgHandler;
BB_DECLARE_SHARED_PTR( MsgHandler );

// helpful macro for readability
// substitute for lack of template typedef
// we #undef it at end of this file
#define BB_TMESSAGE_FN     boost::function<void(const TMessage&)>


class MsgHandler
{
public:
    virtual ~MsgHandler() {}

    /// Creates a MsgHandler that subscribes to specific message for a specific symbol.
    /// Your handler must have the signature:
    ///     void handler( const TMessage& )
    /// Calls EventDistributor::subscribeEvents.
    template <class TMessage>
    static MsgHandlerPtr create(
        source_t src, const instrument_t &instr, const EventDistributorPtr& spED,
        const BB_TMESSAGE_FN& handler,
        uint32_t priority );

    /// Creates a MsgHandler that subscribes to specific message, all symbols.
    /// Your handler must have the signature:
    ///     void handler( const TMessage& )
    /// Calls EventDistributor::subscribeEventsMType.
    template <class TMessage>
    static MsgHandlerPtr createMType(
        source_t src,
        const EventDistributorPtr& spED,
        const BB_TMESSAGE_FN& handler,
        uint32_t priorty );

    /// Variant of create() which calls EventDistributor::subscribeEventsPassive.
    template <class TMessage>
    static MsgHandlerPtr createPassive(
        const instrument_t &instr, const EventDistributorPtr& spED,
        const BB_TMESSAGE_FN& handler,
        uint32_t priority );

    /// Variant of create() which calls EventDistributor::subscribeEventsMTypePassive.
    template <class TMessage>
    static MsgHandlerPtr createMTypePassive(
        const EventDistributorPtr& spED,
        const BB_TMESSAGE_FN& handler,
        uint32_t priorty );


protected:
    Subscription         m_spSub;
};


// MsgHandler implementation for handling a specific message for a specific symbol.
template <class TMessage>
struct TSymMsgHandlerImpl
    : public IEventDistListener, public MsgHandler
{
    TSymMsgHandlerImpl(
        source_t src, const instrument_t &instr, const EventDistributorPtr& spED,
        const BB_TMESSAGE_FN& handler,
        uint32_t priority,
        bool passive)
        : m_instr( instr )
        , m_fnHandler( handler )
    {
        if ( spED && !m_fnHandler.empty() )
        {
            if(passive)
                spED->subscribeEventsPassive( m_spSub, this, TMessage::kMType, instr, priority );
            else
                spED->subscribeEvents( m_spSub, this, src, TMessage::kMType, instr, priority );
        }
    }

    const BB_TMESSAGE_FN& getHandler() const
    {   return m_fnHandler; }

protected:
    virtual void onEvent( const Msg &msg )
    {
        BB_ASSERT( m_fnHandler );
        m_fnHandler(static_cast<const TMessage&>(msg));
    }

private:
    const instrument_t   m_instr;
    const BB_TMESSAGE_FN m_fnHandler;
};


// MsgHandler implementation for handling a specific message.
template <class TMessage>
class TMTypeMsgHandlerImpl
    : public IEventDistListener, public MsgHandler
{
public:
    TMTypeMsgHandlerImpl(
        source_t src,
        const EventDistributorPtr& spED,
        const BB_TMESSAGE_FN& handler,
        uint32_t priority,
        bool passive
    )
        : m_fnHandler( handler )
    {
        if ( spED && !m_fnHandler.empty() )
        {
            if(passive)
                spED->subscribeEventsMTypePassive( m_spSub, this, TMessage::kMType, priority );
            else
                spED->subscribeEventsMType( m_spSub, this, src, TMessage::kMType, priority );
        }
    }

    const BB_TMESSAGE_FN& getHandler() const
    {   return m_fnHandler; }

protected:
    virtual void onEvent( const Msg &msg )
    {
        BB_ASSERT( m_fnHandler );
        m_fnHandler(static_cast<const TMessage&>(msg));
    }

private:
    const BB_TMESSAGE_FN m_fnHandler;
};




//
// MsgHandler factory functions
//

template <class TMessage>
inline MsgHandlerPtr MsgHandler::createMType(
    source_t src,
    const EventDistributorPtr& spED,
    const BB_TMESSAGE_FN& handler,
    uint32_t priority )
{
    return MsgHandlerPtr( new TMTypeMsgHandlerImpl<TMessage>( src, spED, handler, priority, false ) );
}

template <class TMessage>
inline MsgHandlerPtr MsgHandler::createMTypePassive(
    const EventDistributorPtr& spED,
    const BB_TMESSAGE_FN& handler,
    uint32_t priority )
{
    return MsgHandlerPtr( new TMTypeMsgHandlerImpl<TMessage>( source_t(), spED, handler, priority, true ) );
}



template <class TMessage>
inline MsgHandlerPtr MsgHandler::create(
    source_t src,
    const instrument_t &instr, const EventDistributorPtr& spED,
    const BB_TMESSAGE_FN& handler,
    uint32_t priority )
{
    return MsgHandlerPtr ( new TSymMsgHandlerImpl<TMessage>( src, instr, spED, handler, priority, false ) );
}

template <class TMessage>
inline MsgHandlerPtr MsgHandler::createPassive(
    const instrument_t &instr, const EventDistributorPtr& spED,
    const BB_TMESSAGE_FN& handler,
    uint32_t priority )
{
    return MsgHandlerPtr ( new TSymMsgHandlerImpl<TMessage>( source_t(), instr, spED, handler, priority, true ) );
}



#undef BB_TMESSAGE_FN


} // namespace bb

#endif // BB_CLIENTCORE_MSGHANDLER_H

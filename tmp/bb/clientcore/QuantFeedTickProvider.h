#ifndef BB_CLIENTCORE_QUANTFEEDTICKPROVIDER_H
#define BB_CLIENTCORE_QUANTFEEDTICKPROVIDER_H

/* Contents Copyright 2015 Athena Capital Research LLC. All Rights Reserved. */

#include <bb/core/messages.h>
#include <bb/clientcore/TickProvider.h>
#include <bb/clientcore/MsgHandler.h>

namespace bb {

class QuantFeedTickProvider : public TickProviderImpl
{
public:
    QuantFeedTickProvider(const ClientContextPtr &ctx,
            const instrument_t &instr, source_t source,
            const std::string &desc);

    ~QuantFeedTickProvider()
    {}

    virtual bool isLastTickOK() const
    {
        return m_tick_ok;
    }

private:
    void onTradeMsg(const QuantFeedTradeMsg &msg);
    void onIndexMsg(const QuantFeedIndexMsg &msg);
    void onL1SnapshotMsg(const QuantFeedL1SnapshotMsg &msg);

    bool m_tick_ok;
    MsgHandlerPtr m_subTradeMsg;
    MsgHandlerPtr m_subIndexMsg;
    MsgHandlerPtr m_subL1SnapshotMsg;
};

}

#endif // BB_CLIENTCORE_QUANTFEEDTICKPROVIDER_H

#ifndef BB_CLIENTCORE_TICKPROVIDER_H
#define BB_CLIENTCORE_TICKPROVIDER_H
/* Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved. */


#include <set>

#include <boost/optional.hpp>
#include <boost/logic/tribool.hpp>

#include <bb/core/side.h>
#include <bb/core/smart_ptr.h>
#include <bb/core/EventPublisher.h>
#include <bb/core/ListenerList.h>
#include <bb/core/Log.h>
#include <bb/core/ptimefwd.h>
#include <bb/clientcore/PriceProvider.h>
#include <bb/clientcore/PriceProviderSpec.h>
#include <bb/clientcore/PriceSizeProviderSpec.h>
#include <bb/clientcore/IClientTimer.h>

namespace bb {

// Forward declarations
BB_FWD_DECLARE_SHARED_PTR( ITickProvider );


/// TradeTick
class TradeTick
{
protected:
    enum EFlags {
        HIDDEN = 1,
        VISIBILITY_KNOWN = 2,
        SIDE_IS_BID = 4,
        SIDE_KNOWN = 8,
        IS_FINRA = 16,
    };
public:
    // Reports the type of the trade. This is sometimes an educated guess.
    enum EHoursType {
        HOURS_UNKNOWN,
        HOURS_EXTENDED,
        OPENING_PRINT,
        HOURS_NORMAL,
        CLOSING_PRINT,
        CROSS_TRADE, // miscellaneous cross trade; for ISLD the opening print comes across twice
                     // for some reason, once as an opening print and once as a cross trade,
                     // so if you're tallying up volume, you're supposed to exclude OPENING_PRINT & CLOSING_PRINT.
    };

    TradeTick( const PriceSize& pxsz, source_t source,
               const timeval_t& tv_exch = timeval_t(), const timeval_t& tv_msg = timeval_t(),
               uint64_t refnum = 0, boost::logic::tribool hidden = false, mktdest_t mktdest = mktdest_t(),
               const boost::optional<side_t> &side = boost::none,
               EHoursType = HOURS_UNKNOWN,
               bool isFinra = false,
               bool isImplied = false
            );

    TradeTick( double px, uint32_t sz, source_t source,
               const timeval_t& tv_exch = timeval_t(), const timeval_t& tv_msg = timeval_t(),
               uint64_t refnum = 0, boost::logic::tribool hidden = false, mktdest_t mktdest = mktdest_t(),
               const boost::optional<side_t> &side = boost::none,
               EHoursType = HOURS_UNKNOWN,
               bool isFinra = false,
               bool isImplied = false
             );

    ~TradeTick() {}

    PriceSize getPriceSize() const              { return PriceSize( m_px, m_sz ); }
    void setPriceSize( const PriceSize& pxsz )  { pxsz.get( m_px, m_sz ); }

    double px() const                           { return m_px; }
    double getPrice() const                     { return m_px; }
    void setPrice( double px )                  { m_px = px; }

    int32_t sz() const                            { return m_sz; }
    int32_t getSize() const                       { return m_sz; }
    void setSize( int32_t sz )                    { m_sz = sz; }

    source_t getSource() const                  { return m_source; }
    void setSource( source_t source )           { m_source = source; }

    boost::logic::tribool isHidden() const;
    void setHidden( boost::logic::tribool hidden );

    boost::optional<side_t> getSide() const;
    void setSide( const boost::optional<side_t> &side );

    timeval_t getExchangeTime() const           { return m_tv_exch; }
    void setExchangeTime( const timeval_t& tv ) { m_tv_exch = tv; }

    timeval_t getMsgTime() const                { return m_tv_msg; }
    void setMsgTime( const timeval_t& tv )      { m_tv_msg = tv; }

    uint64_t getRefnum() const                    { return m_refnum; }
    void setRefnum( uint64_t refnum )             { m_refnum = refnum; }

    mktdest_t getMktDest() const                { return m_mktdest; }
    void setMktDest(mktdest_t mktdest)          { m_mktdest = mktdest; }

    EHoursType getHours() const                 { return m_hours; }
    void setHours(EHoursType hrs)               { m_hours = hrs; }

    /// Returns true if this is a trade was reported through FINRA.
    /// For details, see the UTDF spec page 49.
    bool isFinra() const                        { return m_flags & IS_FINRA; }
    void setIsFinra(bool v);

    /// Return true if the tick is implied
    bool isImplied() const                      { return m_isImplied; }
    void setIsImplied( bool isImplied )         { m_isImplied = isImplied; }

    /// Only equal if all fields are equal.
    bool operator ==( const TradeTick& rhs ) const;

    /// Only equal if all fields are equal.
    bool operator !=( const TradeTick& rhs ) const;

protected:
    double      m_px;
    timeval_t   m_tv_exch;
    timeval_t   m_tv_msg;
    int32_t     m_sz;
    uint32_t    m_flags;
    uint64_t    m_refnum;
    source_t    m_source;
    mktdest_t   m_mktdest;
    EHoursType  m_hours;
    bool        m_isImplied;
};


/// Interface for ITickProvider listeners
class ITickListener : public bb::IEventSubscriber
{
public:
    virtual ~ITickListener() {}

    /// This will be called when the ITickProvider's receives a new TradeTick.
    /// That last trade tick is passed.
    /// Spurious calls are allowed.  tp will never be NULL.
    virtual void onTickReceived( const ITickProvider* tp, const TradeTick& tick ) {};

    /// Called when a ITickProvider's total volume is updated.
    /// Note: this is might only be called by ITickProviders who get explicitly published volume.
    /// Spurious calls are allowed.  tp will never be NULL.
    virtual void onTickVolumeUpdated( const ITickProvider* tp, uint64_t totalVolume ) {};

    virtual void onOpenTick( const ITickProvider* tp, const TradeTick& tick ){
        onTickReceived( tp, tick );
    }

    virtual void onCloseTick( const ITickProvider* tp, const TradeTick& tick ){
        onTickReceived( tp, tick );
    }

    virtual void onSettlement( const ITickProvider *tp, double settlementPx,
            uint32_t openInterest)
    {}
};

/// A ITickProvider provides Tick information.
/// Listeners can subscribe to be notified of tick changes.
///
/// Implementors can expose more information, including the base message that they process.
///
class ITickProvider
{
public:
    typedef EventPublisher<ITickListener> Listeners_t;

    enum TickTypes {
        kAll        =   0,
        kImplied    =   1,
        kOutright   =   2,
    };

    virtual ~ITickProvider() {}

    /// Returns a descriptive name of the ITickProvider.
    virtual const std::string& getDesc() const = 0;

    /// Returns the source of this ITickProvider.
    virtual source_t getSource() const = 0;

    /// Returns the instrument this tick provider monitors.
    virtual instrument_t getInstrument() const = 0;

    /// Returns true if the last obtained tick is OK.
    /// Returns false if there is no last tick.
    virtual bool isLastTickOK() const = 0;

    /// Returns the last trade tick, or NULL if there is none.
    virtual const TradeTick* getLastTradeTick() const = 0;

    /// Returns the volume accumlated at the current tick price.
    /// A change in tick price will reset this to zero.
    /// This specifically is never published by an exchange, but
    /// calculated internally by the tick provider.
    virtual uint32_t getVolumeAtCurrentPrice() const = 0;

    /// Returns the number of ticks received at the current tick price.
    /// A change in tick price will reset this to one.
    /// This specifically is never published by an exchange, but
    /// calculated internally by the tick provider.
    /// NOTE: Some exchanges aggregate ticks, so this number may not be accurate.
    virtual uint32_t getNumTicksAtCurrentPrice() const = 0;

    /// Returns the last exchange timestamp of any update on the ITickProvider.
    /// Returns timeval_t() if there has never been an exchange timestamp.
    virtual timeval_t getLastExchangeTimestamp() const = 0;

    /// Returns the last message timestamp of any update on the ITickProvider.
    /// Returns timeval_t() if there has never been a message timestamp.
    virtual timeval_t getLastMsgTimestamp() const = 0;

    /// Returns the total accumulated volume.
    /// Not all ITickProvider support this, in which case 0 is returned.
    virtual uint32_t getTotalVolume() const = 0;

    /// Returns the total accumulated hidden volume.
    /// Not all ITickProvider support this, in which case 0 is returned.
    virtual uint32_t getTotalHiddenVolume() const = 0;

    /// Returns the total accumulated extended volume.
    /// Not all ITickProvider support this, in which case total volume is returned.
    virtual uint32_t getTotalExtendedVolume() const = 0;

    /// Returns true if the total accumulated volume is internally estimated,
    /// rather than provided by an authority (exchange, etc.)
    virtual bool isTotalVolumeEstimated() const  = 0;

    /// Returns the opening price.
    /// Returns NaN if no tick has been seen yet.
    /// Some exchanges publish this.  For others, it will be based on the
    /// first price the ITickProvider sees.
    virtual double getOpenPrice() const = 0;

    /// Returns the closing price.
    /// Returns NaN if no tick has been seen yet.
    /// Some exchanges publish this.  For others, it will be based on the
    /// last price the ITickProvider sees before 4PM.
    virtual double getClosePrice() const = 0;

    /// Returns the last price the ITickProvider sees.
    /// Returns NaN if no tick has been seen yet.
    virtual double getLastPrice() const = 0;

    /// Returns the highest price the ITickProvider sees.
    /// Returns NaN if no tick has been seen yet.
    virtual double getHighPrice() const = 0;

    /// Returns the lowest price the ITickProvider sees.
    /// Returns NaN if no tick has been seen yet.
    virtual double getLowPrice() const = 0;

    /// Adds a listener to be notified whenever the tick might have changed.
    virtual void addTickListener( ITickListener *l ) const = 0;

    /// Removes a listener to be notified whenever the tick might have changed.
    /// Throws an error if that listener doesn't exist.
    virtual void removeTickListener( ITickListener *l ) const = 0;

    virtual void subscribe( bb::Subscription& sub, ITickListener* l ) const = 0;

    /// This function is used to set the tick types that would be processed by the tick provider.
    /// Types of ticks are:
    /// @param tickType   the desired types of ticks to be processed
    /// kAll       -  All types of ticks will be processed
    /// kImplied   -  Only implied ticks will be processed
    /// kOutright  -  Only outright ticks will be processed
    virtual void setTickTypesProcessed( TickTypes tickType )
    {
        if( !isAllowedType( tickType ) ){
            LOG_WARN << "The ticktype: " << tickType << " is not supported. Setting to defailt kAll" << bb::endl;
            return;
        }
        m_tickTypesProcessed = tickType;
    }

    /// This function tells the tick provider if the received tick is to be processed
    /// @param isImplied - true if tick is implied
    virtual bool shouldProcess( bool isImplied ){
        bool process = true;
        if( unlikely( m_tickTypesProcessed == ITickProvider::kImplied ) )
        {
            process = isImplied;
        }
        else
        {
            process = !isImplied || m_tickTypesProcessed != kOutright;
        }
        return process;
    };

    /// Since most of the feeds do not have implied ticks
    /// by default we do not allow setTickTypesProcessed to be set to implied
    /// Feeds that support implied ticks should over-ride this function
    /// to allow setTickTypesProcessed to be set to implied
    virtual bool isAllowedType( TickTypes tickType )
    {
        if( ITickProvider::kImplied == tickType )
        {
            return false;
        }
        return true;
    }

protected:
    TickTypes       m_tickTypesProcessed;
};
BB_DECLARE_SHARED_PTR( ITickProvider );

/// A ITradeSplitter splits combined trade messages into our best guess as to
/// the buy and sell trades that best represent the trading activity that has been
/// lumped into the combined trade message.
/// Listeners can subscribe to be notified of tick changes.
class ITradeSplitter
{
public:
    virtual ~ITradeSplitter() {}
    virtual const TradeTick* getAboveVwapTradeTick() const = 0;
    virtual const TradeTick* getBelowVwapTradeTick() const = 0;
};
BB_DECLARE_SHARED_PTR( ITradeSplitter );

/// Implements the common addListener/removeListener stuff.
class TickProviderImpl
    : public ITickProvider
{
public:
    /// Destructor
    virtual ~TickProviderImpl() {}

    // IMPLEMENT ME!
    // Returns true if the last obtained tick is OK.
    // Returns false if there is no last tick.
    // virtual bool isLastTickOK() const = 0;

    /// Returns the source of this ITickProvider.
    virtual source_t getSource() const
    {   return m_source; }

    /// Returns a descriptive name of the ITickProvider.
    virtual const std::string& getDesc() const
    {   return m_desc; }

    // Returns the last trade tick, or NULL if there is none.
    virtual const TradeTick* getLastTradeTick() const
    {   return isLastTickOK() ? &m_last_trade_tick : NULL; }

    /// Returns the instrument this tick provider monitors.
    virtual instrument_t getInstrument() const
    {   return m_instr; }

    /// Returns the volume accumlated at the current tick price.
    /// A change in tick price will reset this to zero.
    /// This specifically is never published by an exchange, but
    /// calculated internally by the tick provider.
    virtual uint32_t getVolumeAtCurrentPrice() const
    {   return m_volume_at_current_px; }

    /// Returns the number of ticks received at the current tick price.
    /// A change in tick price will reset this to one.
    /// This specifically is never published by an exchange, but
    /// calculated internally by the tick provider.
    /// NOTE: Some exchanges aggregate ticks, so this number may not be accurate.
    virtual uint32_t getNumTicksAtCurrentPrice() const
    {   return m_num_at_current_px; }

    /// Returns the last exchange timestamp of any update on the ITickProvider.
    /// Returns timeval_t() if there has never been an exchange timestamp.
    virtual timeval_t getLastExchangeTimestamp() const
    {   return m_last_tv_exch; }

    /// Returns the last message timestamp of any update on the ITickProvider.
    /// Returns timeval_t() if there has never been a message timestamp.
    virtual timeval_t getLastMsgTimestamp() const
    {   return m_last_tv_msg; }

    /// Returns the total accumulated volume.
    /// Not all ITickProvider support this, in which case 0 is returned.
    virtual uint32_t getTotalVolume() const
    {   return m_total_volume; }

    /// Returns the total accumulated hidden volume.
    /// Not all ITickProvider support this, in which case 0 is returned.
    virtual uint32_t getTotalHiddenVolume() const
    {   return m_total_hidden_volume; }

    /// Returns the total accumulated extended volume.
    /// Not all ITickProvider support this, in which case total volume is returned.
    virtual uint32_t getTotalExtendedVolume() const
    {   return m_total_extended_volume; }

    /// Returns true if the total accumulated volume is internally estimated,
    /// rather than provided by an authority (exchange, etc.)
    virtual bool isTotalVolumeEstimated() const
    {   return m_total_volume_estimated; }

    /// Returns the opening price.
    /// Some exchanges publish this.  For others, it will be based on the
    /// first price the ITickProvider sees.
    virtual double getOpenPrice() const
    {   return m_open_px; }

    /// Returns the closing price.
    /// Some exchanges publish this.  For others, it will be based on the
    /// last price the ITickProvider sees before 4PM.
    virtual double getClosePrice() const
    {   return m_close_px; }

    /// Returns the last price the ITickProvider sees.
    virtual double getLastPrice() const
    {   return m_last_px; }

    /// Returns the highest price the ITickProvider sees.
    virtual double getHighPrice() const
    {   return m_high_px; }

    /// Returns the lowest price the ITickProvider sees.
    virtual double getLowPrice() const
    {   return m_low_px; }

    /// Adds a listener to be notified whenever the tick might have changed
    virtual void addTickListener( ITickListener *l ) const { m_listeners.connect( l ); }

    /// Removes a listener to be notified whenever the tick might have changed
    virtual void removeTickListener( ITickListener *l ) const { m_listeners.disconnect( l ); }

    virtual void subscribe( bb::Subscription& sub, ITickListener* l ) const { m_listeners.connect( sub, l ); }
protected:
    /// Only implementors can inherit.
    TickProviderImpl( source_t src, const instrument_t& instr, const std::string& desc, bool totalVolumeEstimated );

    /// Notifies listeners of a new tick received.
    /// Before invoking this, you probably want to call:
    ///     setLastTimestamp, setLastTradeTick
    void notifyTickReceived();
    void notifyOpenTick();
    void notifyCloseTick();

    /// Notifies listeners of a new tick received.
    /// Before invoking this, you probably want to call:
    ///     setLastTimestamp,
    ///     setTotalVolume (or incrementTotalVolume),
    ///     setTotalHiddenVolume (or incrementTotalHiddenVolume)
    void notifyTickVolumeUpdated();

    void notifySettlement(double settlementPx, uint32_t openInterest);

    void incrementTotalVolume( int32_t deltaVolume )
    {   m_total_volume += deltaVolume; }

    void setTotalVolume( uint32_t totalVolume )
    {   m_total_volume = totalVolume; }

    void incrementTotalHiddenVolume( int32_t deltaVolume )
    {   m_total_hidden_volume += deltaVolume; }

    void setTotalHiddenVolume( uint32_t totalVolume )
    {   m_total_hidden_volume = totalVolume; }

    void incrementTotalExtendedVolume( int32_t deltaVolume )
    {   m_total_extended_volume += deltaVolume; }

    void setTotalExtendedVolume( uint32_t totalVolume )
    {   m_total_extended_volume = totalVolume; }

    void setLastTimestamps( const timeval_t& last_tv_exch, const timeval_t& last_tv_msg )
    {
        m_last_tv_exch = last_tv_exch;
        m_last_tv_msg  = last_tv_msg;
    }

    // sets the internally stored last TradeTick,
    // as well as updates the volume_at_current_px.
    void setLastTradeTick( const TradeTick& tick )
    {
        double px = tick.px();

        if ( std::isnan( m_open_px ) )
            m_open_px = m_last_px = m_high_px = m_low_px = px;
        else
        {
            m_last_px = px;
            m_low_px  = std::min( px, m_low_px );
            m_high_px = std::max( px, m_high_px );
        }

        if ( std::isnan( px ) != std::isnan( m_last_trade_tick.px() ) || bb::NE( m_last_trade_tick.px(), px ) )
        {
            m_volume_at_current_px = 0;
            m_num_at_current_px = 0;
        }

        m_num_at_current_px++;
        m_volume_at_current_px += tick.sz();
        m_last_trade_tick = tick;
    }

    void reset()
    {
        m_total_volume = 0;
        m_total_hidden_volume = 0;
        m_total_extended_volume = 0;
        m_volume_at_current_px = 0;
        m_num_at_current_px = 0;
        m_low_px = m_high_px = m_open_px = m_last_px = m_close_px = 0;
        m_last_tv_msg = m_last_tv_exch = timeval_t(0);
        m_last_trade_tick = TradeTick( 0.0, 0, source_t() );
    }

protected:
    instrument_t    m_instr;
    source_t        m_source;
    bool            m_total_volume_estimated;

    timeval_t       m_last_tv_exch;
    timeval_t       m_last_tv_msg;
    uint32_t        m_total_volume;
    uint32_t        m_total_hidden_volume;
    uint32_t        m_total_extended_volume;
    TradeTick       m_last_trade_tick;

    uint32_t        m_volume_at_current_px;
    uint32_t        m_num_at_current_px;

    double          m_low_px, m_high_px;
    double          m_open_px, m_last_px, m_close_px;

    std::string     m_desc;
    mutable Listeners_t m_listeners;

    static const int32_t       s_maxTimerJitter = 100;
};


/// An adapter which turns an ITickProvider into a IPriceProvider.
class TickBasedPriceProvider
    : public PriceProviderImpl
    , protected ITickListener
{
public:
    TickBasedPriceProvider( ITickProviderPtr spTP );
    virtual ~TickBasedPriceProvider();

    /// Returns the last TradeTick price as the "reference price".
    /// Returns NaN if no price is available (!isLastTickOK), and sets pSuccess to
    /// false. Otherwise returns a price and sets pSuccess true.
    virtual double getRefPrice( bool* pSuccess = NULL ) const;

    /// Returns true if getRefPrice would return a valid price if asked at current moment.
    virtual bool isPriceOK() const
    {   return m_spTickProvider->isLastTickOK(); }

    /// Returns the ITickProvider's instrument_t.
    virtual instrument_t getInstrument() const
    {   return m_spTickProvider->getInstrument(); }

    /// Returns the timeval at which this PriceProvider last changed.
    virtual timeval_t getLastChangeTime() const
    {   return m_spTickProvider->getLastMsgTimestamp(); }

protected:
    virtual void onTickReceived( const ITickProvider* tp, const TradeTick& tick );
    virtual void onTickVolumeUpdated( const ITickProvider* tp, uint64_t totalVolume ) {}

protected:
    ITickProviderPtr    m_spTickProvider;
    TradeTick           m_notifiedTick;
};
BB_DECLARE_SHARED_PTR( TickBasedPriceProvider );

/// Provide Vwap and Cumulative Volume
class VolumeClockVwapPriceProvider: public TickBasedPriceProvider
{
public:
    VolumeClockVwapPriceProvider( const IClientTimerPtr& clientTimer, ITickProviderPtr spTP, double nominalInterval, bool atLeastNominal = false, double multiplier = 1.0, const ptime_duration_t &triggerTimeout = ptime_duration_t() );
    double getRefPrice( bool* pSuccess = NULL ) const;

protected:
    void notifyPriceChanged();
    void onTickReceived( const ITickProvider* tp, const TradeTick& tick );
    void scheduleNextTimeout( const timeval_t& tv );
    void onTimeout( const timeval_t& ctv, const timeval_t& swtv );
    bool isPriceOK() const;
    bool isTickOK( const TradeTick & tick ) const;

protected:
    double m_vwap;
    double m_cumulativeNominal;
    const double m_nominalInterval;
    const bool m_atLeastNominal;
    const double m_multiplier;
    bb::Subscription m_subTimeout;
    const ptime_duration_t m_timeout;
    const bool m_useTimeout;
    const IClientTimerPtr m_spClientTimer;
};
BB_DECLARE_SHARED_PTR( VolumeClockVwapPriceProvider );

/// PxPSpec corresponding to a TickBasedPriceProvider (one source)
class SourceTickPxPSpec : public IPxProviderSpec
{
public:
    BB_DECLARE_SCRIPTING();

    SourceTickPxPSpec() {}
    SourceTickPxPSpec(const instrument_t &instr, source_t src) : m_instrument(instr), m_source(src) {}
    SourceTickPxPSpec(const SourceTickPxPSpec &a, const boost::optional<InstrSubst> &instrSubst);

    virtual instrument_t getInstrument() const { return m_instrument; }
    virtual IPriceProviderPtr build(PriceProviderBuilder *builder) const;
    virtual SourceTickPxPSpec *clone(const boost::optional<InstrSubst> &instrSubst = boost::none) const;
    virtual void hashCombine(size_t &result) const;
    virtual bool compare(const IPxProviderSpec *other) const;
    virtual void print(std::ostream &o, const LuaPrintSettings &pset) const;
    virtual void checkValid() const;
    virtual void getDataRequirements(IDataRequirements *rqs) const;

    instrument_t    m_instrument;
    source_t        m_source;
};
BB_DECLARE_SHARED_PTR( SourceTickPxPSpec );

class VolumeClockVwapPxPSpec: public IPxProviderSpec
{
public:
    BB_DECLARE_SCRIPTING();
    VolumeClockVwapPxPSpec();
    VolumeClockVwapPxPSpec( const instrument_t &instr, source_t src, double nominalInterval, bool atLeastNominal = false, const ptime_duration_t &m_timeout = ptime_duration_t() );
    VolumeClockVwapPxPSpec( const VolumeClockVwapPxPSpec &a, const boost::optional<InstrSubst> &instrSubst );
    virtual instrument_t getInstrument() const { return m_instrument; }
    virtual IPriceProviderPtr build( PriceProviderBuilder *builder ) const;
    virtual VolumeClockVwapPxPSpec *clone( const boost::optional<InstrSubst> &instrSubst = boost::none ) const;
    void hashCombine( size_t &result ) const;
    virtual bool compare( const IPxProviderSpec *other ) const;
    virtual void print( std::ostream &o, const LuaPrintSettings &pset ) const;
    virtual void checkValid() const;
    virtual void getDataRequirements( IDataRequirements *rqs ) const;

    instrument_t m_instrument;
    source_t m_source;
    double m_nominalInterval;
    bool m_atLeastNominal;
    ptime_duration_t m_timeout;
};
BB_DECLARE_SHARED_PTR( VolumeClockVwapPxPSpec );


/// PxPSpec corresponding to a TickBasedPriceProvider (multiple sources)
class MasterTickPxPSpec : public IPxProviderSpec
{
public:
    BB_DECLARE_SCRIPTING();

    MasterTickPxPSpec() {}
    MasterTickPxPSpec(const instrument_t &instr, const std::set<source_t> &srcs) : m_instrument(instr), m_sources(srcs) {}
    MasterTickPxPSpec(const MasterTickPxPSpec &a, const boost::optional<InstrSubst> &instrSubst);

    virtual instrument_t getInstrument() const { return m_instrument; }
    virtual MasterTickPxPSpec *clone(const boost::optional<InstrSubst> &instrSubst = boost::none) const;
    virtual IPriceProviderPtr build(PriceProviderBuilder *builder) const;
    virtual void hashCombine(size_t &result) const;
    virtual bool compare(const IPxProviderSpec *other) const;
    virtual void print(std::ostream &o, const LuaPrintSettings &pset) const;
    virtual void checkValid() const;
    virtual void getDataRequirements(IDataRequirements *rqs) const;

    instrument_t    m_instrument;
    std::set<source_t> m_sources;
};
BB_DECLARE_SHARED_PTR( MasterTickPxPSpec );


class MultipathTickPxPSpec : public IPxProviderSpec
{
public:
    BB_DECLARE_SCRIPTING();

    MultipathTickPxPSpec() {}
    MultipathTickPxPSpec(const instrument_t &instr, const std::set<source_t> &srcs) : m_instrument(instr), m_sources(srcs) {}
    MultipathTickPxPSpec(const MultipathTickPxPSpec &a, const boost::optional<InstrSubst> &instrSubst);

    virtual instrument_t getInstrument() const { return m_instrument; }
    virtual MultipathTickPxPSpec *clone(const boost::optional<InstrSubst> &instrSubst = boost::none) const;
    virtual IPriceProviderPtr build(PriceProviderBuilder *builder) const;
    virtual void hashCombine(size_t &result) const;
    virtual bool compare(const IPxProviderSpec *other) const;
    virtual void print(std::ostream &o, const LuaPrintSettings &pset) const;
    virtual void checkValid() const;
    virtual void getDataRequirements(IDataRequirements *rqs) const;

    instrument_t    m_instrument;
    std::set<source_t> m_sources;
};
BB_DECLARE_SHARED_PTR( MultipathTickPxPSpec );



/// An adapter which turns an ITickProvider into a IPriceSizeProvider.
class TickBasedPriceSizeProvider
    : public PriceSizeProviderImpl
    , protected ITickListener
{
public:
    enum side_filter_t
    {
        BID_ONLY,
        ASK_ONLY,
        BOTH_SIDES
    };

    TickBasedPriceSizeProvider( ITickProviderPtr spTP, side_filter_t side );
    virtual ~TickBasedPriceSizeProvider();

    /// Returns the last TradeTick as the "reference PriceSize".
    /// Returns PriceSize() if !isLastTickOK, and sets pSuccess to
    /// false. Otherwise returns a price and sets pSuccess true.
    virtual PriceSize getRefPriceSize( bool* pSuccess = NULL ) const;

    /// Returns true if getRefPriceSize would return a valid price if asked at current moment.
    virtual bool isPriceSizeOK() const
    {   return m_spTickProvider->isLastTickOK(); }

    /// Returns the ITickProvider's instrument_t.
    virtual instrument_t getInstrument() const
    {   return m_spTickProvider->getInstrument(); }

    /// Returns the timeval at which this PriceSizeProvider last changed.
    virtual timeval_t getLastChangeTime() const
    {   return m_spTickProvider->getLastMsgTimestamp(); }

protected:
    virtual void onTickReceived( const ITickProvider* tp, const TradeTick& tick );
    virtual void onTickVolumeUpdated( const ITickProvider* tp, uint64_t totalVolume ) {}

protected:
    ITickProviderPtr    m_spTickProvider;
    TradeTick           m_notifiedTick;
    side_filter_t       m_side;
};
BB_DECLARE_SHARED_PTR( TickBasedPriceSizeProvider );



/// PxSzPSpec corresponding to a TickBasedPriceSizeProvider (one source)
class SourceTickPxSzPSpec : public IPxSzProviderSpec
{
public:
    BB_DECLARE_SCRIPTING();

    SourceTickPxSzPSpec() : m_side(TickBasedPriceSizeProvider::BOTH_SIDES) {}

    virtual instrument_t getInstrument() const { return m_instrument; }
    virtual IPriceSizeProviderPtr build(PriceProviderBuilder *builder) const;
    virtual SourceTickPxSzPSpec *clone() const;
    virtual void hashCombine(size_t &result) const;
    virtual bool compare(const IPxSzProviderSpec *other) const;
    virtual void print(std::ostream &o, const LuaPrintSettings &ps) const;
    virtual void checkValid() const;
    virtual void getDataRequirements(IDataRequirements *rqs) const;

    instrument_t    m_instrument;
    source_t        m_source;
    TickBasedPriceSizeProvider::side_filter_t m_side;
};
BB_DECLARE_SHARED_PTR( SourceTickPxSzPSpec );

/// PxSzPSpec corresponding to a TickBasedPriceSizeProvider (multiple sources)
class MasterTickPxSzPSpec : public IPxSzProviderSpec
{
public:
    BB_DECLARE_SCRIPTING();

    MasterTickPxSzPSpec() : m_side(TickBasedPriceSizeProvider::BOTH_SIDES) {}

    virtual instrument_t getInstrument() const { return m_instrument; }
    virtual IPriceSizeProviderPtr build(PriceProviderBuilder *builder) const;
    virtual MasterTickPxSzPSpec *clone() const;
    virtual void hashCombine(size_t &result) const;
    virtual bool compare(const IPxSzProviderSpec *other) const;
    virtual void print(std::ostream &o, const LuaPrintSettings &ps) const;
    virtual void checkValid() const;
    virtual void getDataRequirements(IDataRequirements *rqs) const;

    instrument_t    m_instrument;
    std::set<source_t> m_sources;
    TickBasedPriceSizeProvider::side_filter_t m_side;
};
BB_DECLARE_SHARED_PTR( MasterTickPxSzPSpec );

class MultipathTickPxSzPSpec : public IPxSzProviderSpec
{
public:
    BB_DECLARE_SCRIPTING();

    MultipathTickPxSzPSpec() : m_side(TickBasedPriceSizeProvider::BOTH_SIDES) {}

    virtual instrument_t getInstrument() const { return m_instrument; }
    virtual IPriceSizeProviderPtr build(PriceProviderBuilder *builder) const;
    virtual MultipathTickPxSzPSpec *clone() const;
    virtual void hashCombine(size_t &result) const;
    virtual bool compare(const IPxSzProviderSpec *other) const;
    virtual void print(std::ostream &o, const LuaPrintSettings &ps) const;
    virtual void checkValid() const;
    virtual void getDataRequirements(IDataRequirements *rqs) const;

    instrument_t    m_instrument;
    std::set<source_t> m_sources;
    TickBasedPriceSizeProvider::side_filter_t m_side;
};
BB_DECLARE_SHARED_PTR( MultipathTickPxSzPSpec );

std::ostream &operator<<(std::ostream &out, TradeTick::EHoursType hrs);
std::ostream &operator<<(std::ostream &out, ITickProvider::TickTypes tickType);

} // namespace bb

inline void bb::TradeTick::setIsFinra(bool v)
{
    if(v) m_flags |= IS_FINRA;
    else  m_flags &= ~IS_FINRA;
}

inline boost::logic::tribool bb::TradeTick::isHidden() const
{
    if(m_flags & VISIBILITY_KNOWN) return (m_flags & HIDDEN);
    else                           return boost::indeterminate;
}

inline void bb::TradeTick::setHidden( boost::logic::tribool hidden )
{
    if(hidden)
        m_flags |= (HIDDEN | VISIBILITY_KNOWN);
    else if(!hidden)
    {
        m_flags |= VISIBILITY_KNOWN;
        m_flags &= ~HIDDEN;
    }
    else
        m_flags &= ~(HIDDEN | VISIBILITY_KNOWN);
}

inline boost::optional<bb::side_t> bb::TradeTick::getSide() const
{
    if(m_flags & SIDE_KNOWN) return (m_flags & SIDE_IS_BID) ? BID : ASK;
    else                     return boost::none;
}

inline void bb::TradeTick::setSide( const boost::optional<bb::side_t> &side )
{
    if(side)
    {
        if(*side == BID)
            m_flags |= (SIDE_KNOWN | SIDE_IS_BID);
        else
        {
            m_flags |= SIDE_KNOWN;
            m_flags &= ~SIDE_IS_BID;
        }
    }
    else
        m_flags &= ~(SIDE_IS_BID | SIDE_KNOWN);
}

inline bool bb::TradeTick::operator ==( const TradeTick& rhs ) const
{
    return (m_sz == rhs.m_sz) && bb::EQ(m_px, rhs.m_px) && (m_source == rhs.m_source) &&
           (m_tv_exch == rhs.m_tv_exch) && (m_tv_msg == rhs.m_tv_msg) &&
           (m_flags == rhs.m_flags) && (m_refnum == rhs.m_refnum) && (m_mktdest == rhs.m_mktdest);
}

inline bool bb::TradeTick::operator !=( const TradeTick& rhs ) const
{
    return !(*this == rhs);
}

// defined down here where setHidden and setSide have already been defined so they can be inlined
inline bb::TradeTick::TradeTick( const PriceSize& pxsz, source_t source,
           const timeval_t& tv_exch, const timeval_t& tv_msg,
           uint64_t refnum, boost::logic::tribool hidden, mktdest_t mktdest,
           const boost::optional<side_t> &side,
           EHoursType hours,
           bool isFinra, bool isImplied )
    : m_px( pxsz.px() )
    , m_tv_exch( tv_exch )
    , m_tv_msg( tv_msg )
    , m_sz( pxsz.sz() )
    , m_flags( 0 )
    , m_refnum( refnum )
    , m_source( source )
    , m_mktdest( mktdest )
    , m_hours( hours )
{
    setHidden( hidden );
    setSide( side );
    setIsFinra( isFinra );
    setIsImplied( isImplied );
}

inline bb::TradeTick::TradeTick( double px, uint32_t sz, source_t source,
           const timeval_t& tv_exch, const timeval_t& tv_msg,
           uint64_t refnum, boost::logic::tribool hidden, mktdest_t mktdest,
           const boost::optional<side_t> &side,
           EHoursType hours,
           bool isFinra, bool isImplied )
    : m_px( px )
    , m_tv_exch( tv_exch )
    , m_tv_msg( tv_msg )
    , m_sz( sz )
    , m_flags( 0 )
    , m_refnum( refnum )
    , m_source( source )
    , m_mktdest( mktdest )
    , m_hours( hours )
{
    setHidden( hidden );
    setSide( side );
    setIsFinra( isFinra );
    setIsImplied( isImplied );
}

#endif // BB_CLIENTCORE_TICKPROVIDER_H

#ifndef BB_CLIENTCORE_CANDLESTICKSERIESBUILDER_H
#define BB_CLIENTCORE_CANDLESTICKSERIESBUILDER_H

/* Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved. */


#include <boost/bind.hpp>

#include <bb/core/smart_ptr.h>
#include <bb/core/MaxMin.h>
#include <bb/core/timeval.h>
#include <bb/core/ptime.h>
#include <bb/core/Subscription.h>
#include <bb/core/ListenerList.h>

#include <bb/clientcore/ClockMonitor.h>
#include <bb/clientcore/TickProvider.h>
#include <bb/clientcore/ICandlestickSeries.h>
#include <bb/clientcore/ICandlestickListener.h>

namespace bb {

BB_FWD_DECLARE_SHARED_PTR( ICandlestickSeries );

class CandlestickSeriesBuilder
    : public bb::ITickListener
    , public bb::ListenerList<ICandlestickListener*>
{
public:
    CandlestickSeriesBuilder( bb::ClockMonitorPtr cm
        , double period
        , ICandlestickSeriesPtr series
        , bb::ITickProviderPtr tp );

    CandlestickSeriesBuilder& setPeriod( double p ) { m_period = p; return (*this); }
    CandlestickSeriesBuilder& setKeepZeros( bool b ) { m_bKeepZeros = b; return (*this); }
    CandlestickSeriesBuilder& setPreserveLastPrice( bool b )
    { m_bPreserveLastPrice = b; return (*this); }

    ptime_duration_t getPeriod() const;
    Candlestick getCurrent() const;

protected:
    virtual void onTickReceived( const bb::ITickProvider* tp, const bb::TradeTick& tick );
    void onWakeup( const bb::timeval_t& ctv, const bb::timeval_t& swtv );
    virtual void addEntry( const Candlestick& entry );
    void notifyUpdate( const Candlestick& entry )
    {
        notify( boost::bind( &ICandlestickListener::onUpdate, _1
                , m_spSeries.get(), entry ) );
    }

protected:
    class State
    {
    public:
        State();

        void init( const bb::timeval_t& t ) { m_startTime = t; }
        void reset();
        void update( double price, size_t size );

        bb::timeval_t m_startTime;
        bb::MaxMin<double> m_bounds;
        boost::optional<double> m_first;
        double m_last;
        size_t m_volume;
    };

protected:
    double m_period;
    bb::ClockMonitorPtr m_spClockMonitor;
    ICandlestickSeriesPtr m_spSeries;
    bb::ClockMonitor::WakeupHandler m_wakeupHandler;
    bb::Subscription m_wakeupSubscription;
    bb::Priority m_wakeupPriority;
    bb::Subscription m_sub;
    State m_curState;
    bool m_bKeepZeros;
    bool m_bPreserveLastPrice;
};

BB_DECLARE_SHARED_PTR( CandlestickSeriesBuilder );

} // namespace bb

#endif // BB_CLIENTCORE_CANDLESTICKSERIESBUILDER_H

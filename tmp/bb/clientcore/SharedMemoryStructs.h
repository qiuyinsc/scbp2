#ifndef BB_CLIENTCORE_SHAREDMEMORYSTRUCTS_H
#define BB_CLIENTCORE_SHAREDMEMORYSTRUCTS_H

/* Contents Copyright 2016 Athena Capital Research LLC. All Rights Reserved. */
#include <bb/threading/LockFree.h>
#include <bb/core/messages.h>

namespace bb {


struct MemAcrStruct
    : public bb::threading::LockFreeStruct
{
    MemAcrStruct()
    {
        initialize();
    }

    void resetStruct()
    {
        Seqnum = 0;
        FlushSeqnum = 0;
    }

    void initialize()
    {
        resetStruct();
        init();
    }

    LOCK_FREE_FIELD( uint64_t, Seqnum );
    LOCK_FREE_FIELD( uint64_t, FlushSeqnum );
};


// This is the structure we use to write Book to sharedMemory
struct MemAcrBook
    : public MemAcrStruct
{
public:
    MemAcrBook()
    {
        initialize();
    }

    void resetBook()
    {
        memset( &SharedMemoryBook, 0x00, sizeof(bb::msg_shared_memory_book_10) );
    }

    void initialize()
    {
        MemAcrStruct::initialize();
        resetBook();
    }

    const MemAcrBook& operator=( const MemAcrBook& other )
    {
        memcpy( this, &other, sizeof( MemAcrBook ) );
        return *this;
    }

    const static uint8_t SnapshotArraySize = 8;

    LOCK_FREE_ARRAY_MEMCPY( bb::msg_shared_memory_book_10, SharedMemoryBook, SnapshotArraySize );
};

} // namespace bb
#endif // BB_CLIENTCORE_SHAREDMEMORYSTRUCTS_H

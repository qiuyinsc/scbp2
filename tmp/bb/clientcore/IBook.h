#ifndef BB_CLIENTCORE_IBOOK_H
#define BB_CLIENTCORE_IBOOK_H

/* Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved. */


#include <bb/core/instrument.h>
#include <bb/core/source.h>
#include <bb/core/smart_ptr.h>
#include <bb/core/PriceSize.h>
#include <bb/core/MarketLevel.h>
#include <bb/core/EventPublisher.h>
#include <boost/optional.hpp>

namespace bb {

// Forward declarations
class Msg;
class IBook;
class IBookListener;
class IBookLevelListener;
BB_FWD_DECLARE_SHARED_PTR( IBookLevelCIter );
BB_FWD_DECLARE_SHARED_PTR( BookLevel ); // include BookLevel.h if you need BookLevel

/// Abstract base class for Books.
class IBook
{
public:
    virtual ~IBook() {}

    /// Returns the instrument this book is maintained for.
    virtual instrument_t getInstrument() const                          = 0;

    /// Returns a descriptive name for the book.
    virtual std::string  getDesc() const                                = 0;

    /// Returns the source of this book.
    virtual source_t     getSource() const                              = 0;

    /// Returns the timeval this book last changed.
    virtual timeval_t    getLastChangeTime() const                      = 0;

    /// Returns true if the book is an aggregate book.
    /// An aggregate book is one that does not keep individual orders,
    /// but rather each BookLevel is an aggregate of orders.
    virtual bool isAggregate() const                                    = 0;

    /// Returns true if the book is in a "valid" state.
    /// The meaning of this is book-dependent, however clients typically use it
    /// to test whether they should "trust" the book's contents.
    virtual bool isOK() const                                           = 0;

    /// Flushes the book.
    virtual void flushBook()                                            = 0;

    /// Tells the Book to drop the specified level.
    /// Returns true if the Book actually does it (it doesn't necessarily have to), false otherwise.
    virtual bool dropBookLevel( side_t side, double px )                = 0;

    /// Returns the mid-price of a book, or 0.0 if the book is not valid (e.g. is one-sided).
    virtual double getMidPrice() const                                  = 0;

    /// Returns the PriceSize at a specified depth and side.
    /// Returns an empty PriceSize if nothing exists at that depth or side.
    virtual PriceSize getNthSide( size_t depth, side_t side ) const     = 0;

    /// Returns an object for iterating over BookLevels of a particular side.
    /// Postcondition: The returned iterator will iterate getNumLevels objects.
    /// This abstracts the underlying containers used by a book; book implementations
    /// will implement their own iterator and pass them back.
    virtual IBookLevelCIterPtr getBookLevelIter( side_t side ) const    = 0;

    /// Returns the number of levels of a particular side of this book.
    /// Note that empty levels above the "bottom" level are counted.
    virtual size_t getNumLevels( side_t side ) const                    = 0;

    virtual PriceSize getInnerMostSide( side_t side ) const
    {
        return this->getNthSide( 0, side );
    }

    virtual PriceSize getOuterMostSide( side_t side ) const
    {
        const size_t numLevels = this->getNumLevels( side );
        return this->getNthSide( numLevels - 1, side );
    }

    /// Adds a book listener to this Book.
    /// Returns true if successful, or false if the listener is already added.
    virtual bool addBookListener( IBookListener* pListener ) const     = 0;

    /// Removes a book listener from this Book.
    /// Returns true if successful, or false if the listener was never in the Book.
    virtual bool removeBookListener( IBookListener* pListener ) const  = 0;

    /// Adds a book level listener to this Book.
    /// Returns true if successful, or false if the listener is already added.
    virtual bool addBookLevelListener( IBookLevelListener* pListener ) const     = 0;

    /// Removes a book level listener from this Book.
    /// Returns true if successful, or false if the listener was never in the Book.
    virtual bool removeBookLevelListener( IBookLevelListener* pListener ) const  = 0;

    /// Print the first optional_nlevels of the book to an ostream.
    /// If optional_nlevels isn't specified, either getDefaultMaxPrintLevels() or print_level_chg + 2 is used.
    virtual std::ostream& print( std::ostream& out, int optional_nlvls = -2 ) const = 0;

    /// Called when this book is about to be added to a MasterBook. Since we can't architecturally
    /// handle the same book being in 2 MasterBooks, this should check to make sure it hasn't been
    /// added already.
    virtual void addToMasterBook() = 0;

    /// Called to get the last per instrument sequence number received for the book. If the book
    /// does not support per instrument sequence number the boost::None is returned
    virtual boost::optional<uint32_t> getLastProcessedInstrSeqnum() const { return boost::none; };
};
BB_DECLARE_SHARED_PTR(IBook);


/** Iterates over BookLevels in a book.
    NOTE: This is not an STL-iterator.
    It is modeled after java.util.Iterator

    example usage:
    for ( IBookLevelCIter iter = book.getBookLevelIter(BID); iter->hasNext(); )
    {
        BookLevelCPtr spbl = iter->next();
        // do stuff with the spbl
    }
**/
class IBookLevelCIter
{
public:
    virtual ~IBookLevelCIter() {}

    /// Returns true if there is another item in the sequence.
    /// IBookLevelCIter::next will return a valid value if this is true.
    /// IBookLevelCIter::next will return an invalid value if this is false.
    virtual bool hasNext() const = 0;

    /// Returns the next BookLevel from the iterator and increments the iterator.
    /// Returns an empty smart-pointer if the iteration is finished.
    virtual BookLevelCPtr next() = 0;
};
BB_DECLARE_SHARED_PTR( IBookLevelCIter );

class IBookLevelRawIter
    : public IBookLevelCIter
{
public:
    virtual ~IBookLevelRawIter() {}

    /// Initialize the iterator using the container provided at construction
    virtual void initialize() = 0;

    /// Returns the raw poitner to a  BookLevel from the iterator and increments the iterator.
    /// Returns a nullptr if the iteration is finished.
    virtual BookLevel* nextRaw() = 0;

};
BB_DECLARE_SHARED_PTR( IBookLevelRawIter );

/// IBookListener interface
/// IBookListeners are notified when the book changes or is flushed.
/// This interface class provides empty implementations rather than pure virtual functions.
class IBookListener : public bb::IEventSubscriber
{
public:
    virtual ~IBookListener() {}
    /// Invoked when the subscribed Book changes.
    /// The levelChanged entries are negative if there is no change, or a 0-based depth.
    /// This depth is a minimum -- there could be multiple deeper levels that changed
    /// since the last onBookChanged.
    virtual void onBookChanged( const IBook* pBook, const Msg* pMsg,
                                int32_t bidLevelChanged, int32_t askLevelChanged ) {}
    /// Invoked when the subscribed Book is flushed.
    virtual void onBookFlushed( const IBook* pBook, const Msg* pMsg ) {}
    virtual bool shouldNotify(const int32_t bidLevel, const int32_t askLevel){ return true; }
};
BB_DECLARE_SHARED_PTR( IBookListener );


/// ILimitedBookListener interface
/// ILimitedBookListeners are a subclass of IBookListener that filters out
/// book changes for levels above the provided threshold
class ILimitedBookListener : public bb::IBookListener
{
public:
    ILimitedBookListener( int32_t notifyLevel )
        : m_minNotifyLevel( notifyLevel )
    {}
    virtual bool shouldNotify(const int32_t bidLevel, const int32_t askLevel)
    {
        return ( (bidLevel >= 0) && (bidLevel <= m_minNotifyLevel) ) || ( (askLevel >= 0) && (askLevel <= m_minNotifyLevel) );
    }
private:
    int32_t m_minNotifyLevel;
};

/// IBookLevelListener interface
/// BookLevelListeners are notified when a Book's levels are added, removed or modified.
/// This interface class provides empty implementations rather than pure virtual functions.
class IBookLevelListener : public bb::IEventSubscriber
{
public:
    virtual ~IBookLevelListener() {}
    /// Invoked when the subscribed Book is about to add a previously nonexistent BookLevel.
    /// Normally, return true.  Return false to advise the Book to not add the level.
    /// Note that the Book does not necessarily have to heed this advice.
    virtual bool onBookLevelAdded( const IBook* pBook, const BookLevel* pLevel ) { return true; }
    /// Invoked when the subscribed Book drops a previously existant BookLevel.
    virtual void onBookLevelDropped( const IBook* pBook, const BookLevel* pLevel ) {}
    /// Invoked when the subscribed Book modifies the specified BookLevel.
    virtual void onBookLevelModified( const IBook* pBook, const BookLevel* pLevel ) {}
};
BB_DECLARE_SHARED_PTR( IBookLevelListener );



/*****************************************************************************/
//
// Book "Algorithms"
//
/*****************************************************************************/

/// Returns the MarketLevel at a specified depth.
/// Depth of 0 would represent the best market.
/// Returns an empty MarketLevel if nothing exists at that depth.
template <class T>
MarketLevel getNthMarket( const T& tbook, size_t depth )
{
    const MarketLevel ml ( tbook.getNthSide(depth, BID), tbook.getNthSide(depth, ASK) );
    return ml;
}


/// Fills in the values for a market level at a specified depth.
/// Depth of 0 would represent the best market.
/// Returns false if nothing exists on both sides at that depth.
/// HINT: The argument order is the same as you'd read it in a Book view.
template <class T>
bool getNthMarket(
    const T& tbook, size_t depth,
    int& bidsz, double& bidpx, double& askpx, int& asksz )
{
    PriceSize pxsz_bid = tbook.getNthSide( depth, BID );
    pxsz_bid.get( bidpx, bidsz );
    PriceSize pxsz_ask = tbook.getNthSide( depth, ASK );
    pxsz_ask.get( askpx, asksz );
    return ( !pxsz_bid.empty() || !pxsz_ask.empty() );
}

/// Returns the BookLevel for the specified side and depth.
template <class T>
BookLevelCPtr getBookLevel( const T& tbook, side_t side, uint32_t depth )
{
    BB_ASSERT( side == BID || side == ASK );
    IBookLevelCIterPtr iter = tbook.getBookLevelIter( side );
    for ( size_t count = 0; count <= depth && iter->hasNext(); ++count )
    {
        BookLevelCPtr spBL = iter->next();
        if ( count == depth )
            return spBL;
    }
    // if we got here, we didn't find anything
    return BookLevelCPtr();
}

/// Returns the MarketLevel for the best market.
/// Returns an empty MarketLevel if nothing exists at that depth.
template <class T>
MarketLevel getBestMarket( const T& tbook )
{
    MarketLevel ml;
    ml.set( BID, tbook.getNthSide(0, BID) );
    ml.set( ASK, tbook.getNthSide(0, ASK) );
    return ml;
}

/// Fills the passed vector the top N BookLevels on a side of a book.  Clears the vector first.
/// Returns false (but still does the operation) if there are less than N levels on that side.
/// Returns true otherwise.
template <class T>
bool getNBookLevels( const T& tbook, side_t side, uint32_t depth, std::vector<BookLevelCPtr>& vLevels )
{
    BB_ASSERT( side == BID || side == ASK );
    vLevels.clear();

    size_t count;
    IBookLevelCIterPtr iter = tbook.getBookLevelIter( side );
    for ( count = 0; count < depth && iter->hasNext(); ++count )
        vLevels.push_back( iter->next() );

    return (count >= depth);
}

/// Checks each BookLevel on both sides (up to check_depth levels) making sure they OK (detailedIsOK).
/// Returns true if it is all OK, or false otherwise.
/// If bad_if_side_empty is true, checkBookStructure will return false if either side of the book is empty.
///
/// This version uses the pure IBook interface and exposed for the template instantiation.
bool IBook_checkBookStructure( const IBook& book, int32_t check_depth = -1, bool bad_if_side_empty = true );

/// Checks each BookLevel on both sides (up to check_depth levels) making sure they OK (detailedIsOK).
/// Returns true if it is all OK, or false otherwise.
/// If bad_if_side_empty is true, checkBookStructure will return false if either side of the book is empty.
///
/// The default implementation assumes that TBook is an IBook and will use the pure IBook interface method.
template <class TBook>
inline bool checkBookStructure( const TBook& book, int32_t check_depth = -1, bool bad_if_side_empty = true )
{
    return IBook_checkBookStructure( book, check_depth, bad_if_side_empty );
}

/// Returns the 'pressure'-weighted mid-price of the MarketLevel.
/// Returns 0 if either side has a 0 size; the call is considered unsuccessful.
/// Returns ml.getMidPrice() if the side's sizes sum to zero; the call is considered unsuccessful.
/// There is no further validation of the MarketLevel.
///
/// If pSuccess is non-NULL, sets it to true on success, or false otherwise.
///
///
///          Sb0*Pa0 + Sa0*Pb0
/// PWMP() = -----------------
///              Sb0 + Sa0
///
inline double getPressureWeightedMidPrice( const MarketLevel& ml, bool* pSuccess = NULL )
{
    const PriceSize& bid = ml.getPriceSize( BID );
    const PriceSize& ask = ml.getPriceSize( ASK );

    if ( bid.sz() == 0 || ask.sz() == 0 )
    {
        if ( pSuccess )
            *pSuccess = false;
        return 0.0;
    }

    int32_t total_sz = bid.sz() + ask.sz();
    if ( total_sz == 0 )  // don't blow up because of denominator!
    {
        if ( pSuccess )
            *pSuccess = false;
        return ml.getMidPrice();
    }

    if ( pSuccess )
        *pSuccess = true;
    return ((bid.sz()*ask.px()) + (ask.sz()*bid.px()))
           / static_cast<double>(total_sz);
}


/// Returns the volume-weighted mid-price of the MarketLevel.
/// Returns ml.getMidPrice() if the side's sizes sum to zero; there is no further validation of the MarketLevel.
/// If pSuccess is non-NULL, sets it to true on success, or false otherwise.
//
///          Sb0*Pb0 + Sa0*Pa0
/// VWMP() = -----------------
///              Sb0 + Sa0
///
inline double getVolumeWeightedMidPrice( const MarketLevel& ml, bool* pSuccess = NULL )
{
    const PriceSize& bid = ml.getPriceSize( BID );
    const PriceSize& ask = ml.getPriceSize( ASK );

    int32_t total_sz = bid.sz() + ask.sz();
    if ( total_sz == 0 )  // don't blow up because of denominator!
    {
        if ( pSuccess )
            *pSuccess = false;
        return ml.getMidPrice();
    }

    if ( pSuccess )
        *pSuccess = true;
    return ((bid.sz()*bid.px()) + (ask.sz()*ask.px()))
           / static_cast<double>(total_sz);
}

/***************************************************************************************************************************/
//
// Module Functions
//
/***************************************************************************************************************************/

/// Print the entire side of a book one bl at a time.
void printBookhalf( const IBook& book, std::ostream& out, side_t side );


inline std::ostream& operator <<( std::ostream &out, const IBook& b )
{
    return b.print( out );
}

} // namespace bb

#endif // BB_CLIENTCORE_IBOOK_H

#ifndef BB_CLIENTCORE_HKFEPRSTICKPROVIDER_H
#define BB_CLIENTCORE_HKFEPRSTICKPROVIDER_H

/* Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved. */

#include <bb/clientcore/TickProvider.h>
#include <bb/core/messages.h>

namespace bb {

class HkfeQuoteMsg;

BB_FWD_DECLARE_SHARED_PTR( ClockMonitor );
BB_FWD_DECLARE_SHARED_PTR( MsgHandler );

class HkfePrsTickProvider
    : public TickProviderImpl
{
public:
    HkfePrsTickProvider( ClientContext& context, const instrument_t& instr, source_t source,
                      const std::string& desc );

    /// Destructor.
    virtual ~HkfePrsTickProvider() {}

    /// Returns true if the last obtained tick is OK.
    /// Returns false if there is no last tick.
    virtual bool isLastTickOK() const           
    { 
        return m_tickReceived; 
    }

private:
    // Event handlers
    void onHkfeTradeInfoMsg(const HkfeTradeInfoMsg& msg);
    void onStartOfDay(const timeval_t& ctv, const timeval_t& wtv);

    void scheduleNextStartOfDay(const timeval_t& now);

private:
    ClockMonitorPtr m_spClockMonitor;
    bool m_tickReceived;
    MsgHandlerPtr m_msgHandler;
    Subscription m_subWakeup;
};

BB_DECLARE_SHARED_PTR( HkfePrsTickProvider );

}

#endif // BB_CLIENTCORE_HKFEPRSTICKPROVIDER_H

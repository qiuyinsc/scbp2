#ifndef BB_CLIENTCORE_CZCEL2TICKPROVIDER_H
#define BB_CLIENTCORE_CZCEL2TICKPROVIDER_H

/* Contents Copyright 2012 Athena Capital Research LLC. All Rights Reserved. */

#include <bb/core/messages.h>
#include <bb/clientcore/TickProvider.h>

namespace bb {


class CzceL2QdMsg;
BB_FWD_DECLARE_SHARED_PTR( ClockMonitor );
BB_FWD_DECLARE_SHARED_PTR( MsgHandler );


class CzceL2TickProvider
    : public TickProviderImpl
{
public:
    /// Constructs an CzceTickProvider for the given instrument,
    /// receiving events from the EventDistributor.
    /// Instrument must be a stock.
    CzceL2TickProvider( const ClientContext& context, const instrument_t& instr, source_t source,
                             const std::string& desc );

    /// Destructor.
    virtual ~CzceL2TickProvider() {}

    /// Returns true if the last obtained tick is OK.
    /// Returns false if there is no last tick.
    virtual bool isLastTickOK() const           { return m_bTickReceived; }


private:
    // Event handlers
    void onCzceL2QdMsg( const CzceL2QdMsg& msg );


private:
    ClockMonitorPtr m_spClockMonitor;
    bool m_bTickReceived;
    int64_t m_lastSerial;
    MsgHandlerPtr m_subCzceL2QdMsg;
    Subscription m_subWakeup;
};

BB_DECLARE_SHARED_PTR( CzceL2TickProvider );

}

#endif // BB_CLIENTCORE_CZCEL2TICKPROVIDER_H

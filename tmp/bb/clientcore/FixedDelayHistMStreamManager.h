#ifndef BB_CLIENTCORE_FIXEDDELAYHISTMSTREAMMANAGER_H
#define BB_CLIENTCORE_FIXEDDELAYHISTMSTREAMMANAGER_H

/* Contents Copyright 2012 Athena Capital Research LLC. All Rights Reserved. */

#include <bb/core/ptime.h>
#include <bb/clientcore/HistMStreamManager.h>

namespace bb {

class FixedDelayHistMStreamManager : public HistMStreamManager
{
public:
    FixedDelayHistMStreamManager( const timeval_t& start_tv,
                                  const timeval_t& end_tv,
                                  const ptime_duration_t& delay )
        : HistMStreamManager( start_tv, end_tv ),
          m_delay( delay )
    {}

    virtual boost::optional<double> getFeedDelaySecsFromScript( const source_t& feed, mktdest_t /* UNUSED */ )
    {
        return bb::ptime_duration_to_double( m_delay );
    }

private:
    ptime_duration_t m_delay;
};

}

#endif // BB_CLIENTCORE_FIXEDDELAYHISTMSTREAMMANAGER_H

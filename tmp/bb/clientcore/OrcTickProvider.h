#ifndef BB_CLIENTCORE_ORCTICKPROVIDER_H
#define BB_CLIENTCORE_ORCTICKPROVIDER_H

/* Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved. */


#include <bb/clientcore/TickProvider.h>

namespace bb {

class OrcPriceFeedMsg;

BB_FWD_DECLARE_SHARED_PTR( ClockMonitor );
BB_FWD_DECLARE_SHARED_PTR( MsgHandler );

class OrcTickProvider
    : public TickProviderImpl
{
public:
    /// Constructs an OrcTickProvider for the given instrument,
    /// receiving events from the EventDistributor.
    /// Instrument must be a stock.
    OrcTickProvider( ClientContext& context, const instrument_t& instr, source_t source,
                      const std::string& desc );

    /// Destructor.
    virtual ~OrcTickProvider() {}

    /// Returns true if the last obtained tick is OK.
    /// Returns false if there is no last tick.
    virtual bool isLastTickOK() const           { return m_bTickReceived; }

private:
    // Event handlers
    void onOrcPriceFeedMsg( const OrcPriceFeedMsg& msg );
    void onStartOfDay( const timeval_t& ctv, const timeval_t& wtv );

    void scheduleNextStartOfDay( const timeval_t& now );

private:
    ClockMonitorPtr m_spClockMonitor;
    bool m_bInitialized;
    bool m_bTickReceived;
    int64_t m_lastSerial;
    MsgHandlerPtr m_subOrcPriceFeedMsg;
    Subscription m_subWakeup;
};

BB_DECLARE_SHARED_PTR( OrcTickProvider );

}

#endif // BB_CLIENTCORE_ORCTICKPROVIDER_H

#ifndef BB_CLIENTCORE_BIGSIZEL1BOOK_H
#define BB_CLIENTCORE_BIGSIZEL1BOOK_H

/* Contents Copyright 2012 Athena Capital Research LLC. All Rights Reserved. */

#include <bb/clientcore/Book.h>
#include <bb/clientcore/ClockMonitor.h>
#include <bb/clientcore/ShfeDepthBook.h>

namespace bb {

/// BigSizeL1Book is a special single-level book that returns the PriceSize of
/// the BookLevel in the underlying L2 book that has the largest size on each
/// side.
class BigSizeL1Book
    : public BookImpl
    , public IBookListener
    , public IBookLevelListener
{
protected:
    class BookLevelCIter
        : public IBookLevelCIter
    {
    public:
        BookLevelCIter( const BookLevel* bl )
            : m_next( bl->isOK() ? bl : 0 )
        {
        }

        virtual bool hasNext() const
        {
            return m_next != 0;
        }

        virtual BookLevelCPtr next()
        {
            const BookLevel* rval = m_next;
            if( rval )
            {
                m_next = 0;
                return makeNoopSharedPtr( rval );
            }
            else
                return BookLevelCPtr();
        }

    protected:
        const BookLevel* m_next;
    };

public:
    BigSizeL1Book( ClientContext& context, const instrument_t& instrument, const source_t& source,
                       const SourceMonitorPtr& sm, const RandomSourcePtr& rs, const char* description, int verbose,
                       const IBookPtr& underlying_book, int32_t max_levels = 5 );

    virtual ~BigSizeL1Book();

    //
    // IBook interface
    //

    virtual PriceSize getSide( side_t side ) const
    {
        return m_levels[side]->getPriceSize();
    }

    virtual bool isOK() const
    {
        return m_levels[BID] && m_levels[BID]->isOK() && m_levels[ASK] && m_levels[ASK]->isOK();
    }

    virtual void flushBook()
    {
        m_levels[BID] = NULL;
        m_levels[ASK] = NULL;
    }

    virtual double getMidPrice() const;

    virtual PriceSize getNthSide( size_t depth, side_t side ) const;

    virtual IBookLevelCIterPtr getBookLevelIter( side_t side ) const;

    virtual size_t getNumLevels( side_t side ) const;

    //
    // IBookListener interface
    //

    virtual void onBookFlushed( const IBook* book, const Msg* message );

    //
    // IBookLevelListener interface
    //

    virtual bool onBookLevelAdded( const IBook* book, const BookLevel* level );

    virtual void onBookLevelModified( const IBook* book, const BookLevel* level );

    virtual void onBookLevelDropped( const IBook* book, const BookLevel* level );

private:
    template<side_t side>
    bool setBigSizeBookLevel( const IBook* book, const BookLevel* level );

    void rescanBook( const IBook* book, const BookLevel* level );

protected:
    /// BigSizeL1Book determines the big size levels from an internal Book.
    IBookPtr m_internal_book;

    /// Store the biggest size book levels
    const BookLevel* m_levels[2];

    /// The maximum # of levels to check for the biggest size, beyond
    /// m_max_levels will be ignored (too far out to be useful to trade).
    const int32_t m_max_levels;

private:
    /// Caching the old PriceSize to reduce the # of notifications that are sent
    PriceSize m_old_ps[2];
};

class BigSizeL1BookSpec
    : public IBookSpec
{
public:
    BB_DECLARE_SCRIPTING();

    BigSizeL1BookSpec();

    BigSizeL1BookSpec( const BigSizeL1BookSpec& other, const boost::optional<InstrSubst>& substitution );

    virtual ~BigSizeL1BookSpec();

    virtual IBookPtr build( BookBuilder* builder ) const;

    virtual BigSizeL1BookSpec* clone( const boost::optional<InstrSubst>& substitution = boost::none ) const;

    virtual void hashCombine( size_t& result ) const;

    virtual bool compare( const IBookSpec* other ) const;

    virtual void print( std::ostream& os, const LuaPrintSettings& ps ) const;

    virtual void getDataRequirements( IDataRequirements* requirements ) const;

    virtual void checkValid() const;

    virtual const instrument_t& getInstrument() const;

public:
    /// Instrument that the BigSizeL1Book will be constructed for
    instrument_t m_instrument;

    /// Underlying book for the BigSizeL1Book
    IBookSpecPtr m_underlying_spec;

    /// The maximum # of levels to check for the biggest size, beyond
    /// m_max_levels will be ignored (too far out to be useful to trade).
    int32_t m_max_levels;
};

template<side_t side>
bool BigSizeL1Book::setBigSizeBookLevel( const IBook* book, const BookLevel* level  )
{
    IBookLevelCIterPtr iterator = book->getBookLevelIter( side );

    m_levels[side] = NULL;

    int32_t current = 0;
    while( iterator->hasNext() && current < m_max_levels )
    {
        BookLevelCPtr book_level = iterator->next();
        if( !m_levels[side] || book_level->getSize() > m_levels[side]->getSize() )
            m_levels[side] = book_level.get();
        ++current;
    }

    if( !m_old_ps[side].empty() )
    {
        if( m_levels[side] && m_levels[side]->getPriceSize() != m_old_ps[side] )
        {
            m_old_ps[side] = m_levels[side]->getPriceSize();
            notifyBookLevelModified( m_levels[side] );
        }
        else if( !m_levels[side] )
        {
            m_old_ps[side].reset();
            notifyBookLevelDropped( level );
        }
        return true;
    }
    else if( m_old_ps[side].empty() && m_levels[side] )
    {
        m_old_ps[side] = m_levels[side]->getPriceSize();
        notifyBookLevelAdded( m_levels[side] );
        return true;
    }
    else
        return false;
}

} // namespace bb

#endif // BB_CLIENTCORE_BIGSIZEL1BOOK_H

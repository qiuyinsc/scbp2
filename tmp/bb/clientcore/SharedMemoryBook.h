#ifndef BB_CLIENTCORE_SHAREDMEMORYBOOK_H
#define BB_CLIENTCORE_SHAREDMEMORYBOOK_H

/* Contents Copyright 2016 Athena Capital Research LLC. All Rights Reserved. */

#include <bb/io/SharedMemoryContainer.h>
#include <bb/clientcore/Book.h>
#include <bb/clientcore/ClientIdleManager.h>
#include <bb/core/Error.h>
#include <bb/clientcore/SourceBooks.h>

namespace {
const uint32_t INITIAL_ATTEMPT = 100;
const uint32_t RETRY_ATTEMPTS = 5;
}

namespace bb {

class SharedMemoryBook
    : public Book
    , public OnIdleOncePerEventListener
{
public:
    enum BookChangeFlag { kNoChange, kBboChange, kNonBboChange };
    enum BookUpdateType { kBbo, kAny };
    enum NotifyFlag{ kNotify, kNoNotify };

    SharedMemoryBook( const ClientContextPtr& clientContext, const MemAcrBook& sharedMemoryBook
                      , const instrument_t& instr, source_t src, const char* desc, int _vbose=0 );

    virtual ~SharedMemoryBook();

    bool onIdle_impl();

    void flushBook( )
    {
        m_lastSeqnum = 0;
        m_lastBBOSeqnum = 0;
        book_rdy_fg = false;
        eventcnt = 0;
        m_lastChangeTv = 0;
        notifyBookFlushed( nullptr );
    }

    virtual double getMidPrice() const
    {
        if( !book_rdy_fg )
        {
            if( unlikely( vbose > 0 ) )
            {
                LOG_WARN << "::getMidPx: book isn't ready yet " << std::endl;
            }
            return 0.0;
        }
        else if( unlikely( m_pShmemBookClean->snapshot_header.num_level_bid == 0
                           || m_pShmemBookClean->snapshot_header.num_level_ask == 0 ) )
        {
            if( unlikely( vbose > 0 ) )
            {
                LOG_WARN << "::getMidPx: book is empty or one-sided " << std::endl;
            }
            return 0.0;
        }

        double ask = m_pShmemBookClean->book_snapshot[ 0 ].ask_level.price;
        double bid = m_pShmemBookClean->book_snapshot[ 0 ].bid_level.price;

        return ( bid + ask ) / 2;
    }

    PriceSize getNthSide( size_t level, side_t side ) const;

    IBookLevelCIterPtr getBookLevelIter( side_t side ) const
    {
        BB_THROW_ERROR_SSX( " not supported" );
    }

    size_t getNumLevels( side_t side ) const
    {
        if( !book_rdy_fg )
        {
            if( vbose > 0 )
            {
                LOG_WARN << " getNumLevels: book isn't ready yet" << bb::endl;
            }
            return 0;
        }

        switch( side )
        {
        case BID:
            return m_pShmemBookClean->snapshot_header.num_level_bid;
        case ASK:
            return m_pShmemBookClean->snapshot_header.num_level_ask;
        default:
            BB_THROW_ERROR_SS( "Invalid side" );
        }
        return 0;
    }

    ///
    /// Function to update the book from shared memory. If the notify variable is true
    /// the function generates a onBookChanged callback for listeners.
    /// The function also returns a BookChangeFlag{kBboChange, kNoChange, kNonBboChange}
    ///
    BookChangeFlag updateBook( BookUpdateType type, NotifyFlag notify=kNoNotify );

    // getter for various fields
    // add transit delay to msg
    const uint64_t   getTransitDelay()   const { return m_pShmemBookClean->snapshot_header.transit_delay; }
    const timeval_t& getLastUpdateTime() const { return m_pShmemBookClean->snapshot_header.update_time; }
    const timeval_t& getBookReadTime()   const { return m_bookReadTime; }
    const timeval_t& getQdSendTime()     const { return m_pShmemBookClean->snapshot_header.qd_send_time; }
    const timeval_t& getQdNicTime()      const { return m_pShmemBookClean->snapshot_header.qd_nic_time; }

protected:
    const MemAcrBook&                                m_sharedMemoryBook;

    typedef boost::array<msg_shared_memory_book_10, 2> SharedMemoryBookArray;
    SharedMemoryBookArray                            m_shmemBookArr;
    msg_shared_memory_book_10*                       m_pShmemBookClean;
    uint64_t                                         m_localShmemArrIndex;
    size_t                                           m_copySize;

    uint32_t                                         m_lastSeqnum;
    uint32_t                                         m_lastBBOSeqnum;
    uint64_t                                         m_currentTransitDelay;
    uint8_t                                          m_maxLevel;
    timeval_t                                        m_bookReadTime;

    EventDistributorPtr                              m_spED;

    uint64_t                                         m_totalBookUpdateTried;
    uint64_t                                         m_totalBookUpdateFailed;
    uint64_t                                         m_totalBookUpdateNoSeqnumChange;

    bool                                             m_notify;

    bool getBookAndSwapPointer( const uint32_t attempts );
private:
    void notify( BookUpdateType type, BookChangeFlag changeFlag );
    void issueCallback( int bidLvl, int askLvl );

    // This structure is used to ensure that we do not notify the listeners if we are inside
    // a notifyCallback.
    // Example: A book changes and we issue a callback to a book listener. The book listener in turn
    // calls our updateBook function. If the book changed, we would want to notify the same book listener
    // again and would get stuck in a loop. This struct would prevent us from getting in a loop.
    struct notify_struct
    {
        notify_struct( bool& notifyFlag )
            : notify( notifyFlag )
        {
            notify = false;
        }

        ~notify_struct()
        {
            notify = true;
        }
    private:
        bool& notify;
    };
};
BB_DECLARE_SHARED_PTR( SharedMemoryBook );

class ISnapshotWriter
{
public:
    ISnapshotWriter( MemAcrBook& sharedMemoryBook, uint8_t maxLevel, msg_shared_memory_book_10& msg )
        : m_memBook( sharedMemoryBook )
        , m_maxLevel( maxLevel )
        , m_memBookMsg( msg )
    {}

    virtual void writeBookLevels( bool processAsk, bool processBid
                                  , int32_t bidLevelChanged, int32_t askLevelChanged ) = 0;

    template<side_t SIDE, typename LevelT>
    void updateLevel(LevelT lvl, uint32_t idx)
    {
        if( BID == SIDE )
        {
            m_memBookMsg.book_snapshot[ idx ].bid_level.price = lvl->getPrice();
            m_memBookMsg.book_snapshot[ idx ].bid_level.size  = lvl->getSize();
        }
        else
        {
            m_memBookMsg.book_snapshot[ idx ].ask_level.price = lvl->getPrice();
            m_memBookMsg.book_snapshot[ idx ].ask_level.size  = lvl->getSize();
        }
    }

    virtual ~ISnapshotWriter()
    {}

    MemAcrBook&                                  m_memBook;
    uint8_t                                      m_maxLevel;
    msg_shared_memory_book_10&                   m_memBookMsg;
};
BB_DECLARE_SHARED_PTR( ISnapshotWriter );

class RawSnapshotWriter
    : public ISnapshotWriter
{
public:
    RawSnapshotWriter( MemAcrBook& sharedMemoryBook, IBookCPtr spBook, uint8_t maxLevel
                       , msg_shared_memory_book_10& msg);

    void writeBookLevels( bool processAsk, bool processBid
                          , int32_t bidLevelChanged, int32_t askLevelChanged );

protected:
    IBookLevelRawIterPtr  m_askIter;
    IBookLevelRawIterPtr  m_bidIter;
};

class SnapshotWriter
    : public ISnapshotWriter
{
public:
    SnapshotWriter( MemAcrBook& sharedMemoryBook, IBookCPtr spBook, uint8_t maxLevel
                    , msg_shared_memory_book_10& msg);

    void writeBookLevels( bool processAsk, bool processBid
                          , int32_t bidLevelChanged, int32_t askLevelChanged );

protected:
    IBookCPtr   m_spBook;
};


class SharedMemoryBookWriter
    : public ILimitedBookListener
{
public:
    SharedMemoryBookWriter( const ClientContextPtr& clientContext, MemAcrBook& sharedMemoryBook, IBookCPtr spBook, uint8_t maxLevel );

    void onBookChanged( const bb::IBook* pBook, const Msg* pMsg
                        , int32_t bidLevelChanged, int32_t askLevelChanged );

    void onBookFlushed( const IBook* pBook, const Msg* pMsg );

protected:
    MemAcrBook&                                  m_memBook;
    uint32_t                                     m_seqnum;
    uint32_t                                     m_bboSeqnum;
    ISnapshotWriterPtr                           m_snapshotWriter;
    uint8_t                                      m_maxLevel;
    size_t                                       m_copySize;
    EventDistributorPtr                          m_spEventDist;
    msg_shared_memory_book_10                    m_shmemBook;
};
BB_DECLARE_SHARED_PTR( SharedMemoryBookWriter );

/*****************************************************************************/
//
// SharedMemoryBookSpec
//
/*****************************************************************************/

/// BookSpec corresponding to the shared memory books
class SharedMemoryBookSpec : public SourceBookSpec
{
public:
    BB_DECLARE_SCRIPTING();

    SharedMemoryBookSpec(const instrument_t &instr, source_t src )
        : SourceBookSpec(instr, src)
    {}

    virtual IBookPtr build( BookBuilder* builder ) const;

};
BB_DECLARE_SHARED_PTR(SharedMemoryBookSpec);



} // namespace bb

#endif // BB_CLIENTCORE_SHAREDMEMORYBOOK_H

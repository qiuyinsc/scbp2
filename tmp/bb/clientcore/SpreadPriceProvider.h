#ifndef BB_CLIENTCORE_SPREADPRICEPROVIDER_H
#define BB_CLIENTCORE_SPREADPRICEPROVIDER_H

/* Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved. */


#include <boost/function.hpp>
#include <bb/clientcore/PriceProvider.h>
#include <bb/clientcore/PriceProviderSpec.h>

namespace bb
{

BB_FWD_DECLARE_SHARED_PTR( SpreadPriceProvider );

class SpreadPriceProvider
    : public PriceProviderImpl
{
public:
    SpreadPriceProvider( bb::IPriceProviderPtr pxp_b, bb::IPriceProviderPtr pxp_a );
    SpreadPriceProvider( double mb, const bb::IPriceProviderCPtr& pxp_b
        , double ma, const bb::IPriceProviderCPtr& pxp_a );

    virtual ~SpreadPriceProvider();

    // IPriceProvider interface
    virtual double getRefPrice( bool* pSuccess = NULL ) const;

    /// Returns true if getRefPrice would return a valid price if asked at current moment.
    virtual bool isPriceOK() const { return m_spPxP_A->isPriceOK() && m_spPxP_B->isPriceOK(); }

    /// Returns the instrument_t associated with this PriceProvider
    virtual instrument_t getInstrument() const { return m_spPxP_A->getInstrument(); }

    /// Returns the timeval at which this PriceProvider last changed.
    virtual timeval_t getLastChangeTime() const;

protected:
    double getSpreadPrice() const;
    void onPriceChanged( const IPriceProvider& inputPxP );

    double m_multB;
    double m_multA;
    bb::IPriceProviderCPtr m_spPxP_B;
    bb::IPriceProviderCPtr m_spPxP_A;
    Subscription m_subB;
    Subscription m_subA;
};

BB_DECLARE_SHARED_PTR( SpreadPriceProvider );

/// PxPSpec corresponding to a SpreadPriceProvider
class SpreadPxPSpec : public IPxProviderSpec
{
public:
    BB_DECLARE_SCRIPTING();

    SpreadPxPSpec()
        : m_multB( 1 )
        , m_multA( -1 )
    {}
    SpreadPxPSpec( const SpreadPxPSpec &a, const boost::optional<InstrSubst> &instrSubst );

    virtual instrument_t getInstrument() const;
    virtual IPriceProviderPtr build( PriceProviderBuilder *builder ) const;
    virtual SpreadPxPSpec *clone( const boost::optional<InstrSubst> &instrSubst = boost::none ) const;
    virtual void hashCombine( size_t &result ) const;
    virtual bool compare( const IPxProviderSpec *other ) const;
    virtual void print( std::ostream &o, const LuaPrintSettings &pset ) const;
    virtual void checkValid() const;
    virtual void getDataRequirements( IDataRequirements *rqs ) const {}

    double             m_multB;
    double             m_multA;
    IPxProviderSpecPtr m_pxpB;
    IPxProviderSpecPtr m_pxpA;
};
BB_DECLARE_SHARED_PTR( SpreadPxPSpec );

} // namespace bb

#endif // BB_CLIENTCORE_SPREADPRICEPROVIDER_H

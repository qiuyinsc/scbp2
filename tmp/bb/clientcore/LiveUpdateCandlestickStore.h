#ifndef BB_CLIENTCORE_LIVEUPDATECANDLESTICKSTORE_H
#define BB_CLIENTCORE_LIVEUPDATECANDLESTICKSTORE_H

/* Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved. */


#include <string>
#include <utility>
#include <bb/core/smart_ptr.h>
#include <bb/core/instrument.h>
#include <bb/core/source.h>
#include <bb/core/Subscription.h>
#include <bb/core/hash_map.h>
#include <bb/core/timeval.h>
#include <bb/clientcore/HistCandlestickFileStore.h>

namespace bb {

BB_FWD_DECLARE_SHARED_PTR( ClockMonitor );
BB_FWD_DECLARE_SHARED_PTR( ITickProviderFactory );

class ICandlestickListener;
BB_FWD_DECLARE_SHARED_PTR( ICandlestickSeries );
BB_FWD_DECLARE_SHARED_PTR( CandlestickSeriesBuilder );

class LiveUpdateCandlestickStore : public HistCandlestickFileStore
{
public:
    LiveUpdateCandlestickStore( double period
        , const source_t& src
        , const std::string& store_path
        , const ClockMonitorPtr& cm
        , const ITickProviderFactoryPtr& tpf
        , const timeval_t& start_time
        , bool keep_zeros = false );

    void setSource( const source_t& src ) { m_source = src; }

    virtual ICandlestickSeriesPtr getInstrument( const instrument_t& instr, const source_t& alt_src = source_t() );

    void subscribeSeriesUpdate( const instrument_t& instr
        , Subscription& sub, ICandlestickListener* l, const source_t& alt_src = source_t() );

    double getPeriod() const { return m_period; }

protected:
    typedef bbext::hash_map< instrument_t, CandlestickSeriesBuilderPtr > InstrBuilderTable;
    typedef std::pair<CandlestickSeriesBuilderPtr,ICandlestickSeriesPtr> BuilderSeriesPair;

    BuilderSeriesPair initInstrument( const instrument_t& instr, const source_t& alt_src );

protected:
    double m_period;
    InstrBuilderTable m_builders;
    ClockMonitorPtr m_spClockMonitor;
    ITickProviderFactoryPtr m_spTickProviderFactory;
    source_t m_source;
    bool m_bKeepZeros;
};

BB_DECLARE_SHARED_PTR( LiveUpdateCandlestickStore );

} // namespace bb

#endif // BB_CLIENTCORE_LIVEUPDATECANDLESTICKSTORE_H

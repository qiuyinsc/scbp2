#ifndef BB_CLIENTCORE_SOURCEMONITOR_H
#define BB_CLIENTCORE_SOURCEMONITOR_H

/* Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved. */


#include <deque>
#include <list>
#include <vector>
#include <boost/shared_ptr.hpp>
#include <bb/core/auto_vector.h>
#include <bb/clientcore/EventDist.h>
#include <bb/clientcore/SpamFilter.h>
#include <bb/clientcore/ISourceMonitorListener.h>
#include <bb/clientcore/IClockListener.h>
#include <bb/clientcore/ClockMonitor.h>
#include <bb/clientcore/SourceBooks.h>

namespace bb {

class SourceMonitorListener;

BB_FWD_DECLARE_SHARED_PTR(Alert);
BB_FWD_DECLARE_SHARED_PTR(ClientContext);
BB_FWD_DECLARE_SHARED_PTR(RandomSource);
BB_FWD_DECLARE_SHARED_PTR(ClockMonitor);

const char *SMonStatus2str(smon_status_t stat);


/// Listens to a source and records various statistics about it.
class SourceMonitor
    : protected IEventDistListener
    , public IClockListener
{
public:
    class CInfo {
    public:
        source_t src;
        EventDistributorWeakCPtr wpED;
        ClockMonitorPtr spCM;
        AlertPtr spAlert;
        RandomSourcePtr spRandom;
        std::string desc;
        int vbose;
        std::vector<instrument_t> baselineData;

        CInfo(source_t _src, EventDistributorCPtr _spED, ClockMonitorPtr _spCM,
              AlertPtr _spAlert, RandomSourcePtr _spRandom, const std::string& _description, int _vbose,
              const std::vector<instrument_t>& _baselineData);

        CInfo();
    };

    class ParamInfo {
    public:
        ParamInfo();

        ptime_duration_t minInterval;
        ptime_duration_t minAvgInterval;
        double longIntervalMult;
        double maxIntervalMult;
        ptime_duration_t checkStatusInterval;
        ptime_duration_t typicalLag;
        bool bCheckSeqNum;
        bool bLowPrecision;
    };
    static const ptime_duration_t kDefMinInterval;
    static const ptime_duration_t kDefMinAvgInterval;
    static double kDefLongIntervalMultiplier;
    static double kDefMaxIntervalMultiplier;
    static const ptime_duration_t kDefCheckStatusInterval;

public:
    SourceMonitor( const CInfo& _cinfo, const ParamInfo& pi, const std::vector<mtype_t> &mtypes );
    virtual ~SourceMonitor();

    source_t    getSrc() const                  { return cinfo.src; }
    const char* getDesc() const                 { return cinfo.desc.c_str(); }

    ptime_duration_t   getLastLagTv() const            { return last_lag_tv; }
    ptime_duration_t   getAvgIntervalTv() const        { return avg_interval_tv; }
    timeval_t          getLastUpdateTv() const         { return last_update_tv; }
    timeval_t          getLastReportedTv() const       { return last_reported_tv; }
    ptime_duration_t   getTypicalLag() const           { return typical_lag; }

    void        setCheckSeqnum( bool check )    { check_seqnum_fg = check; }
    bool        getCheckSeqnum() const          { return check_seqnum_fg; }

    ptime_duration_t getMinInterval() const { return min_interval; }
    ptime_duration_t getMinAvgInterval() const { return min_avg_interval_tv; }
    double getMaxIntervalMult() const { return max_interval_multiplier; }
    double getLongIntervalMult() const { return long_interval_multiplier; }
    bool getLowPrecision() const { return low_precision_fg; }

    void setMinInterval( ptime_duration_t interval )   { min_interval = interval; }
    void setLongIntervalMultiplier( double mult )  { long_interval_multiplier = mult; }
    void setMaxIntervalMultiplier( double mult )   { max_interval_multiplier = mult; }

    void addSMonListener( SourceMonitorListener* l );

    /// Remove a listener that was added with addListener.
    /// Returns true if removed or false if the listener was not found.
    bool removeSMonListener( SourceMonitorListener* l );

    bool scheduleReportedTvWakeup( SourceMonitorListener* l, timeval_t wakeup_reported_tv,
                                   int reason, void* pData=NULL );
    bool unscheduleWakeupCall( SourceMonitorListener* l, timeval_t wakeup_reported_tv );
    void unscheduleWakeupCalls( SourceMonitorListener* l );

    smon_status_t checkStatus( timeval_t current_tv );

    /// same as other checkStatus call, but also returns an estimate of lag in est_lag_tv
    /// arg and the interval between ctv and the last update from this source in interval_tv
    smon_status_t checkStatus( timeval_t ctv, ptime_duration_t& est_lag_tv, ptime_duration_t& interval_tv );

    void update( timeval_t reported_tv, const Msg& msg );
    void reset( bool bFullReset = false );

    void print() const;
    std::ostream& print( std::ostream& out ) const;

    void setVbose( int _vbose )                 { cinfo.vbose = _vbose; }
    void setupBaselineData( const std::vector<instrument_t> &baselineData )
        { cinfo.baselineData = baselineData; doSetupBaselineData(); }

protected:
    // ClockListener interface
    virtual void onWakeupCall( const timeval_t& ctv, const timeval_t& swtv, int reason, void* pData );

    // IEventDistListener interface
    virtual void onEvent( const Msg& msg );

protected:
    void doSetupBaselineData();

protected:
    CInfo cinfo;

    std::vector<EventSubPtr> m_registeredListeners;
    NullEventDistListener m_eventIgnorer;

    static const int kUSEquityOpen = 9*3600 + 30*60;
    static const int kUSEquityClose = 16*3600;
    static const int kUSFuturesOpen = 9*3600 + 30*60;
    static const int kUSFuturesClose = 16*3600;

    int atopen_sec;
    int atclose_sec;

    smon_status_t               status;

    unsigned int                ttl_packet_cnt;
    auto_vector<unsigned int>   mtype_cnts;

    timeval_t                   last_update_tv;
    timeval_t                   last_reported_tv;

    static const unsigned int   kLagBufSize = 50;
    ptime_duration_t            last_lag_tv;
    ptime_duration_t            min_lag_tv;
    ptime_duration_t            max_lag_tv;
    std::deque<ptime_duration_t>lag_buf;
    ptime_duration_t            ttl_lag_buf_tv;
    ptime_duration_t            avg_lag_tv;

    static const unsigned int   kIntervalBufSize = 100;
    ptime_duration_t            last_interval_tv;
    ptime_duration_t            min_interval_tv;
    ptime_duration_t            max_interval_tv;
    std::deque<ptime_duration_t>interval_buf;
    ptime_duration_t            ttl_interval_buf_tv;
    ptime_duration_t            avg_interval_tv;
    ptime_duration_t            min_avg_interval_tv; // the minimum allowed value of avg_internal_tv

    bool                        first_update_fg;
    bool                        market_hours_fg;
    bool                        low_precision_fg;
    typedef std::vector<SourceMonitorListener*> listeners_t;
    listeners_t listeners;

    ptime_duration_t            typical_lag;
    ptime_duration_t            max_avg_lag;
    ptime_duration_t            max_instant_lag;
    ptime_duration_t            min_interval;
    double                      long_interval_multiplier;
    double                      max_interval_multiplier;

    unsigned int                last_seqnum;
    bool                        check_seqnum_fg;

    ptime_duration_t            check_status_interval;
    static const int            R_CHECK_STATUS = cm::USER_REASON + 1;

    struct wakeup_call {
        timeval_t              wtv;
        SourceMonitorListener* l;
        int                    reason;
        void*                  pData;
        bool operator ==(const wakeup_call &rhs) const {
            return wtv==rhs.wtv && l==rhs.l && reason==rhs.reason && pData==rhs.pData;
        }
    };

    typedef std::list<wakeup_call> wakeup_list;
    wakeup_list                wakeups;
    // 20080201 - Allows max alarm spam of 10 alarms per minute
    SpamFilter m_alarmSpamFilter;
    // 20080201 - Allows max log spam of 10 messages per 6 seconds
    SpamFilter m_messageSpamFilter;

    const std::vector<mtype_t> m_mtypes;
};

BB_DECLARE_SHARED_PTR( SourceMonitor );

/// Factory for SourceMonitors. Creates a new SourceMonitor of the appropriate
/// child class depending on the feedtype of src.
///
/// @attention Looks in the ClientContext for a "SourceMonitor_SRC_XXX" property, which should be
/// @attention a SourceMonitor::ParamInfo object, and if the ParamInfo exists, will use it to
/// @attention create the SourceMonitor.
///
/// Returns: Pointer to the created smon, or NULL if the feedtype of src
/// isn't recognized.
SourceMonitor* createSourceMonitor( source_t src, ClientContext *ccontext, const std::vector<instrument_t> &baselineData );

/// Factory for SourceMonitors. If a source monitor for the src has already been created then it returns
/// the previous monitor, otherwise it creates a new SourceMonitor of the appropriate
/// child class depending on the feedtype of src.
///
/// @attention Looks in the ClientContext for a "SourceMonitor_SRC_XXX" property, which should be
/// @attention a SourceMonitor::ParamInfo object, and if the ParamInfo exists, will use it to
/// @attention create the SourceMonitor.
///
/// Returns: Shared Pointer to the created smon, or SourceMonitorPtr() if the feedtype of src
/// isn't recognized.
SourceMonitorPtr getSourceMonitorPtr( source_t src, ClientContext *ccontext, const std::vector<instrument_t> &baselineData );


/// SourceMonitor specialized for the ARCA data feed.
class SourceMonitor_ARCA
    : public SourceMonitor
{
public:
    SourceMonitor_ARCA( const CInfo& _cinfo, const ParamInfo& pi );

    virtual void onWakeupCall( const timeval_t& ctv, const timeval_t& swtv, int reason, void* pData );
};


/// SourceMonitor specialized for the BRUT data feed.
class SourceMonitor_BRUT
    : public SourceMonitor
{
public:
    SourceMonitor_BRUT( const CInfo& _cinfo, const ParamInfo& pi );

    virtual void onWakeupCall( const timeval_t& ctv, const timeval_t& swtv, int reason, void* pData );
};


/// SourceMonitor specialized for the CQS data feed.
class SourceMonitor_CQS
    : public SourceMonitor
{
public:
    SourceMonitor_CQS( const CInfo& _cinfo, const ParamInfo& pi );

    virtual void onWakeupCall( const timeval_t& ctv, const timeval_t& swtv, int reason, void* pData );
};


/// SourceMonitor specialized for the CTS data feed.
class SourceMonitor_CTS
    : public SourceMonitor
{
public:
    SourceMonitor_CTS( const CInfo& _cinfo, const ParamInfo& pi );

    virtual void onWakeupCall( const timeval_t& ctv, const timeval_t& swtv, int reason, void* pData );
};


/// SourceMonitor specialized for the Island data feed.
class SourceMonitor_ISLD
    : public SourceMonitor
{
public:
    SourceMonitor_ISLD( const CInfo& _cinfo, const ParamInfo& params );

    virtual void onWakeupCall( const timeval_t& ctv, const timeval_t& swtv, int reason, void* pData );
};


/// SourceMonitor specialized for the BATS and EDGX feed
template<typename Traits>
class SourceMonitor_BatsBase
    : public SourceMonitor
{
public:
    SourceMonitor_BatsBase( const CInfo& _cinfo, const ParamInfo& pi );

    virtual void onWakeupCall( const timeval_t& ctv, const timeval_t& swtv, int reason, void* pData );
};

typedef SourceMonitor_BatsBase<BatsTraits> SourceMonitor_BATS;
typedef SourceMonitor_BatsBase<EdgxTraits> SourceMonitor_EDGX;

/// SourceMonitor specialized for the NQDS data feed.
class SourceMonitor_NQDS
    : public SourceMonitor
{
public:
    SourceMonitor_NQDS( const CInfo& _cinfo, const ParamInfo& pi );

    virtual void onWakeupCall( const timeval_t& ctv, const timeval_t& swtv, int reason, void* pData );
};


/// SourceMonitor specialized for the TotalView data feed.
class SourceMonitor_TV
    : public SourceMonitor
{
public:
    SourceMonitor_TV( const CInfo& _cinfo, const ParamInfo& pi );

    virtual void onWakeupCall( const timeval_t& ctv, const timeval_t& swtv, int reason, void* pData );
};


/// SourceMonitor specialized for the UQDF data feed.
class SourceMonitor_UQDF
    : public SourceMonitor
{
public:
    SourceMonitor_UQDF( const CInfo& _cinfo, const ParamInfo& pi );

    virtual void onWakeupCall( const timeval_t& ctv, const timeval_t& swtv, int reason, void* pData );
};


/// SourceMonitor specialized for the UTDF data feed.
class SourceMonitor_UTDF
    : public SourceMonitor
{
public:
    SourceMonitor_UTDF( const CInfo& _cinfo, const ParamInfo& pi );

    virtual void onWakeupCall( const timeval_t& ctv, const timeval_t& swtv, int reason, void* pData );
};


/// SourceMonitor specialized for the TT data feed.
class SourceMonitor_TT
    : public SourceMonitor
{
public:
    SourceMonitor_TT( const CInfo& _cinfo, const ParamInfo& pi );

    virtual void onWakeupCall( const timeval_t& ctv, const timeval_t& swtv, int reason, void* pData );
};


/// SourceMonitor specialized for the IB data feed.
class SourceMonitor_IB
    : public SourceMonitor
{
public:
    SourceMonitor_IB( const CInfo& _cinfo, const ParamInfo& pi );

    virtual void onWakeupCall( const timeval_t& ctv, const timeval_t& swtv, int reason, void* pData );
};


/// SourceMonitor specialized for the acrindex data feed.
class SourceMonitor_ADX
    : public SourceMonitor
{
public:
    SourceMonitor_ADX( const CInfo& _cinfo, const ParamInfo& pi );

    virtual void onWakeupCall( const timeval_t& ctv, const timeval_t& swtv, int reason, void* pData );
    virtual void onEvent( const Msg& msg );
};


/// SourceMonitor specialized for the LIFFE (+CBOT) data feed.
class SourceMonitor_LIFFE
    : public SourceMonitor
{
public:
    SourceMonitor_LIFFE( const CInfo& _cinfo, const ParamInfo& pi );

    virtual void onWakeupCall( const timeval_t& ctv, const timeval_t& swtv, int reason, void* pData );
};


/// SourceMonitor specialized for the CME data feed.
class SourceMonitor_CME
    : public SourceMonitor
{
public:
    SourceMonitor_CME( const CInfo& _cinfo, const ParamInfo& pi );

    virtual void onWakeupCall( const timeval_t& ctv, const timeval_t& swtv, int reason, void* pData );
    virtual void onEvent( const Msg& msg );
};


/// SourceMonitor specialized for the NYSE OpenBook data feed.
class SourceMonitor_OpenBook
    : public SourceMonitor
{
public:
    SourceMonitor_OpenBook( const CInfo& _cinfo, const ParamInfo& pi );

    virtual void onWakeupCall( const timeval_t& ctv, const timeval_t& swtv, int reason, void* pData );
};

/// SourceMonitor specialized for the NYSE OpenBook data feed.
class SourceMonitor_OpenBookUltra
    : public SourceMonitor
{
public:
    SourceMonitor_OpenBookUltra( const CInfo& _cinfo, const ParamInfo& pi );

    virtual void onWakeupCall( const timeval_t& ctv, const timeval_t& swtv, int reason, void* pData );
};

/// SourceMonitor specialized for the NYSE Quotes data feed.
class SourceMonitor_NYSEQuotes
    : public SourceMonitor
{
public:
    SourceMonitor_NYSEQuotes( const CInfo& _cinfo, const ParamInfo& pi );

    virtual void onWakeupCall( const timeval_t& ctv, const timeval_t& swtv, int reason, void* pData );
};

/// SourceMonitor specialized for the SxL1 data feed
class SourceMonitor_SxL1
    : public SourceMonitor
{
public:
    SourceMonitor_SxL1( const CInfo& _cinfo, const ParamInfo& pi );

    virtual void onWakeupCall( const timeval_t& ctv, const timeval_t& swtv, int reason, void* pData );
};

/// SourceMonitor specialized for the SHFEQD data feed.
class SourceMonitor_ShfeQd
    : public SourceMonitor
{
public:
    SourceMonitor_ShfeQd( const CInfo& _cinfo, const ParamInfo& pi );

    virtual void onWakeupCall( const timeval_t& ctv, const timeval_t& swtv, int reason, void* pData );
    virtual void onEvent( const Msg& msg );
};

/// SourceMonitor specialized for the CZCEQD data feed.
class SourceMonitor_CzceQd
    : public SourceMonitor
{
public:
    SourceMonitor_CzceQd( const CInfo& _cinfo, const ParamInfo& pi );

    virtual void onWakeupCall( const timeval_t& ctv, const timeval_t& swtv, int reason, void* pData );
    virtual void onEvent( const Msg& msg );
};

/// SourceMonitor specialized for the CFFEXQD data feed.
class SourceMonitor_CffexQd
    : public SourceMonitor
{
public:
    SourceMonitor_CffexQd( const CInfo& _cinfo, const ParamInfo& pi );

    virtual void onWakeupCall( const timeval_t& ctv, const timeval_t& swtv, int reason, void* pData );
    virtual void onEvent( const Msg& msg );
};

/// SourceMonitor specialized for the WindFutures data feed.
class SourceMonitor_WindFuturesQd
    : public SourceMonitor
{
public:
    SourceMonitor_WindFuturesQd( const CInfo& _cinfo, const ParamInfo& pi );

    virtual void onWakeupCall( const timeval_t& ctv, const timeval_t& swtv, int reason, void* pData );
    virtual void onEvent( const Msg& msg );
};

/// SourceMonitor specialized for the WindIndex data feed.
class SourceMonitor_WindIndexQd
    : public SourceMonitor
{
public:
    SourceMonitor_WindIndexQd( const CInfo& _cinfo, const ParamInfo& pi );

    virtual void onWakeupCall( const timeval_t& ctv, const timeval_t& swtv, int reason, void* pData );
    virtual void onEvent( const Msg& msg );
};

/// SourceMonitor specialized for the WindStock data feed.
class SourceMonitor_WindStockQd
    : public SourceMonitor
{
public:
    SourceMonitor_WindStockQd( const CInfo& _cinfo, const ParamInfo& pi );

    virtual void onWakeupCall( const timeval_t& ctv, const timeval_t& swtv, int reason, void* pData );
    virtual void onEvent( const Msg& msg );
};

/// SourceMonitor specialized for the CME_TAQ data feed.
class SourceMonitor_CmeTaqQd
    : public SourceMonitor
{
public:
    SourceMonitor_CmeTaqQd( const CInfo& _cinfo, const ParamInfo& pi );

    virtual void onWakeupCall( const timeval_t& ctv, const timeval_t& swtv, int reason, void* pData );
    virtual void onEvent( const Msg& msg );
};

/// SourceMonitor specialized for the BookSnapshot data feed.
class SourceMonitor_BookSnapshot
    : public SourceMonitor
{
public:
    SourceMonitor_BookSnapshot( const CInfo& _cinfo, const ParamInfo& pi );

    virtual void onWakeupCall( const timeval_t& ctv, const timeval_t& swtv, int reason, void* pData );
    virtual void onEvent( const Msg& msg );
};


/// SourceMonitor specialized for the OrcQd data feed.
class SourceMonitor_OrcQd
    : public SourceMonitor
{
public:
    SourceMonitor_OrcQd( const CInfo& _cinfo, const ParamInfo& pi );

    virtual void onWakeupCall( const timeval_t& ctv, const timeval_t& swtv, int reason, void* pData );
};

/// SourceMonitor specialized for the ICE data feed.
class SourceMonitor_ICE
    : public SourceMonitor
{
public:
    SourceMonitor_ICE( const CInfo& _cinfo, const ParamInfo& pi );

    virtual void onWakeupCall( const timeval_t& ctv, const timeval_t& swtv, int reason, void* pData );
};

/// SourceMonitor specialized for the TOCOM data feed.
class SourceMonitor_TOCOM
    : public SourceMonitor
{
public:
    SourceMonitor_TOCOM( const CInfo& _cinfo, const ParamInfo& pi );

    virtual void onWakeupCall( const timeval_t& ctv, const timeval_t& swtv, int reason, void* pData );
};

/// SourceMonitor specialized for the HKFE PRS+ data feed.
class SourceMonitor_HKFE
    : public SourceMonitor
{
public:
    SourceMonitor_HKFE( const CInfo& _cinfo, const ParamInfo& pi );

    virtual void onWakeupCall( const timeval_t& ctv, const timeval_t& swtv, int reason, void* pData );
};

/// SourceMonitor specialized for the HKSE data feed.
class SourceMonitor_HKSE
    : public SourceMonitor
{
public:
    SourceMonitor_HKSE( const CInfo& _cinfo, const ParamInfo& pi );

    virtual void onWakeupCall( const timeval_t& ctv, const timeval_t& swtv, int reason, void* pData );
};

/// SourceMonitor specialized for the TAIFEX data feed.
class SourceMonitor_TAIFEX
    : public SourceMonitor
{
public:
    static const int TAIFEX_OPEN_SEC = 8 * 3600 + 45 * 60;
    static const int TAIFEX_CLOSE_SEC = 13 * 3600 + 45 * 60;

    SourceMonitor_TAIFEX( const CInfo& _cinfo, const ParamInfo& pi );

    virtual void onWakeupCall( const timeval_t& ctv, const timeval_t& swtv, int reason, void* pData );
};

inline std::ostream& operator <<( std::ostream &out, const bb::SourceMonitor& monitor )
{
    return monitor.print( out );
}

/// SourceMonitor specialized for the TWSE data feed.
class SourceMonitor_TWSE
    : public SourceMonitor
{
public:
    static const int TWSE_OPEN_SEC = 9 * 3600 + 0 * 60;
    static const int TWSE_CLOSE_SEC = 13 * 3600 + 30 * 60;

    SourceMonitor_TWSE( const CInfo& _cinfo, const ParamInfo& pi );

    virtual void onWakeupCall( const timeval_t& ctv, const timeval_t& swtv, int reason, void* pData );
};

/// SourceMonitor specialized for the DCE L2 data feed.
class SourceMonitor_DceL2
    : public SourceMonitor
{
public:
    SourceMonitor_DceL2( const CInfo& _cinfo, const ParamInfo& pi );

    virtual void onWakeupCall( const timeval_t& ctv, const timeval_t& swtv, int reason, void* pData );
    virtual void onEvent( const Msg& msg );
};

/// SourceMonitor specialized for the GTA_STOCK data feed.
class SourceMonitor_GtaStock
    : public SourceMonitor
{
public:
    SourceMonitor_GtaStock( const CInfo& _cinfo, const ParamInfo& pi );

    virtual void onWakeupCall( const timeval_t& ctv, const timeval_t& swtv, int reason, void* pData );
    virtual void onEvent( const Msg& msg );
};

/// SourceMonitor specialized for the GTA_INDEX data feed.
class SourceMonitor_GtaIndex
    : public SourceMonitor
{
public:
    SourceMonitor_GtaIndex( const CInfo& _cinfo, const ParamInfo& pi );

    virtual void onWakeupCall( const timeval_t& ctv, const timeval_t& swtv, int reason, void* pData );
    virtual void onEvent( const Msg& msg );
};

/// SourceMonitor specialized for the QUANTFEED data feed.
class SourceMonitor_QF
    : public SourceMonitor
{
public:
    SourceMonitor_QF( const CInfo& _cinfo, const ParamInfo& pi );

    virtual void onWakeupCall( const timeval_t& ctv, const timeval_t& swtv, int reason, void* pData );
};


} // namespace bb

#endif // BB_CLIENTCORE_SOURCEMONITOR_H

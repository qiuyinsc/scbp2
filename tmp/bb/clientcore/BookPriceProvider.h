#ifndef BB_CLIENTCORE_BOOKPRICEPROVIDER_H
#define BB_CLIENTCORE_BOOKPRICEPROVIDER_H

/* Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved. */


/**
   @file PriceProvider.h

   PriceProvider interface and several implementations.

**/

#include <bb/clientcore/IBook.h>
#include <bb/clientcore/PriceProvider.h>
#include <bb/clientcore/PriceProviderSpec.h>
#include <bb/clientcore/PriceSizeProviderSpec.h>

namespace bb {

struct LuaPrintSettings;

// Forward declarations
BB_FWD_DECLARE_SHARED_PTR( BookLevelPriceProvider );
BB_FWD_DECLARE_SHARED_PTR( LimitPriceFriendlyBookLevelPriceProvider );
BB_FWD_DECLARE_SHARED_PTR( CompleteBookLevelPriceProvider );
BB_FWD_DECLARE_SHARED_PTR( BookLevelPriceSizeProvider );
BB_FWD_DECLARE_SHARED_PTR( CompleteBookLevelPriceSizeProvider );

/// Different ways to look at the price of a Book.
enum EBookPriceView {
    BPV_UNKNOWN     = 0,
    BPV_BEST_MIDPX,     /// Mid-price of the best market
    BPV_BEST_BID,       /// Price of the best market's bid side
    BPV_BEST_ASK,       /// Price of the best market's bid side
    BPV_BEST_VWMP,      /// Volume-weighted mid-price of the best market
    BPV_BEST_PWMP       /// Pressure-weighted mid-price of the best market
};


/// Returns a price based on a 'view' of the MarketLevel.
/// Returns 0.0 if that view of the price is not available, and sets pSuccess to
/// false. Otherwise returns a price and sets pSuccess true.
/// Only supports "best market" views.
double getPriceFromView( const MarketLevel& ml, EBookPriceView view, bool* pSuccess = NULL );

/// Returns a price based on a 'view' of the Book.
/// Returns 0.0 if that view of the price is not available, and sets pSuccess to
/// false. Otherwise returns a price and sets pSuccess true.
double getPriceFromView( const IBook& book, EBookPriceView view, bool* pSuccess = NULL );

/// Returns a PriceSize based on a 'view' of the MarketLevel
/// Returns PriceSize() if that view of the price is not available, and sets pSuccess to
/// false. Otherwise returns a price and size and sets pSuccess true.
/// Only supports "best market" views.
PriceSize getPriceSizeFromView( const MarketLevel& ml, EBookPriceView view, bool* pSuccess = NULL );

/// Returns a PriceSize based on a 'view' of the Book.
/// Returns PriceSize() if that view of the price is not available, and sets pSuccess to
/// false. Otherwise returns a price and size and sets pSuccess true.
/// Only supports "best market" views.
PriceSize getPriceSizeFromView( const IBook& book, EBookPriceView view, bool* pSuccess = NULL );




/// PriceProvider that derives its price from a particular view of a book.
/// The view controls which information is being extracted from the market level
/// (bid,ask,midpx, etc), and level controls which level (0 being the best market).
class BookLevelPriceProvider
    : public PriceProviderImpl
    , protected IBookListener
{
public:
    /// Returns a BookLevelPriceProvider for the given IBook ).
    /// Returns an invalid smart pointer if the IBook is invalid.
    static BookLevelPriceProviderPtr create( IBookCPtr const&spBook, EBookPriceView view = BPV_BEST_MIDPX, int level = 0 )
    {
        if ( !spBook )
            return BookLevelPriceProviderPtr();
        return BookLevelPriceProviderPtr( new BookLevelPriceProvider( spBook, view, level ) );
    }
    static BookLevelPriceProviderPtr createBestMidpx( IBookCPtr const&spBook ) { return create( spBook ); }

    /// Constructor
    BookLevelPriceProvider( IBookCPtr const&spBook, EBookPriceView view = BPV_BEST_MIDPX, int level = 0 );

    /// Destructor
    virtual ~BookLevelPriceProvider()
    {   m_spBook->removeBookListener(this); }

    /// Returns the symbol associated with this PriceProvider
    virtual instrument_t getInstrument() const
    {   return m_spBook->getInstrument();   }

    /// Returns the timeval at which this PriceProvider last changed.
    virtual timeval_t getLastChangeTime() const
    {   return m_spBook->getLastChangeTime(); }

    /// Returns a price based on the specified 'view' of the Book.
    /// Returns 0.0 if that view of the price is not available, and sets pSuccess to
    /// false. Otherwise returns a price and sets pSuccess true.
    virtual double getRefPrice( bool* pSuccess = NULL ) const
    {   return getPriceFromView( getNthMarket(*m_spBook, m_level), m_view, pSuccess ); }

    /// Returns true if getRefPrice would return a valid price if asked at current moment.
    virtual bool isPriceOK() const;

protected:
    // IBookListener interface
    /// Triggers a notification of a price change
    virtual void onBookChanged( const IBook* pBook, const Msg* pMsg,
                                int32_t bidLevelChanged, int32_t askLevelChanged );
    virtual void onBookFlushed( const IBook* pBook, const Msg* pMsg )
    {   m_prevPx = std::numeric_limits<double>::quiet_NaN(); notifyPriceChanged(); }

protected:
    IBookCPtr       m_spBook;
    EBookPriceView  m_view;
    int             m_level;
    double          m_prevPx;
};


/// PxPSpec corresponding to BookLevelPriceProvider
/// Requires a BookSpec and a view descriptior (default = BPV_BEST_MIDPX).
class BookPxPSpec : public IPxProviderSpec
{
public:
    BB_DECLARE_SCRIPTING();

    BookPxPSpec();
    BookPxPSpec( const BookPxPSpec& other, const boost::optional<InstrSubst> &instrSubst );

    virtual instrument_t getInstrument() const { return m_bookSpec->getInstrument(); }
    virtual IPriceProviderPtr build(PriceProviderBuilder *builder) const;
    virtual BookPxPSpec *clone(const boost::optional<InstrSubst> &instrSubst = boost::none) const;
    virtual void hashCombine(size_t &result) const;
    virtual bool compare(const IPxProviderSpec *other) const;
    virtual void print(std::ostream &o, const LuaPrintSettings &pset) const;
    virtual void checkValid() const;
    virtual void getDataRequirements(IDataRequirements *rqs) const;

    EBookPriceView  m_view;
    IBookSpecCPtr   m_bookSpec;
    int             m_level;
};
BB_DECLARE_SHARED_PTR( BookPxPSpec );

// Created to better handle (Asian commodities markets) Limit up/down events,
// wherein the book will only have one pricesize,
// either the bid (limit up) or the ask (limit down).
// This class gives a more useful price than the 0.0 returned by the parent
// class in this situation.

class LimitPriceFriendlyBookLevelPriceProvider
    : public BookLevelPriceProvider
{
public:
    static LimitPriceFriendlyBookLevelPriceProviderPtr create( IBookCPtr spBook, EBookPriceView view = BPV_BEST_MIDPX, int level = 0 )
    {
        if ( !spBook )
            return LimitPriceFriendlyBookLevelPriceProviderPtr();
        return LimitPriceFriendlyBookLevelPriceProviderPtr( new LimitPriceFriendlyBookLevelPriceProvider( spBook, view, level ) );
    }
    static LimitPriceFriendlyBookLevelPriceProviderPtr createBestMidpx( IBookCPtr spBook ) { return create( spBook ); }

    /// Constructor
    LimitPriceFriendlyBookLevelPriceProvider( IBookCPtr spBook, EBookPriceView view = BPV_BEST_MIDPX, int level = 0 );

    /// Destructor
    virtual ~LimitPriceFriendlyBookLevelPriceProvider(){};

    virtual double getRefPrice( bool* pSuccess = NULL ) const;
};


/// PxPSpec corresponding to LimitPriceFriendlyBookLevelPriceProvider
class LimitPriceFriendlyBookPxPSpec : public BookPxPSpec
{
public:
    BB_DECLARE_SCRIPTING();

    LimitPriceFriendlyBookPxPSpec();
    LimitPriceFriendlyBookPxPSpec( const LimitPriceFriendlyBookPxPSpec& other, const boost::optional<InstrSubst> &instrSubst );

    virtual IPriceProviderPtr build(PriceProviderBuilder *builder) const;
    virtual LimitPriceFriendlyBookPxPSpec *clone(const boost::optional<InstrSubst> &instrSubst = boost::none) const;
    virtual bool compare(const IPxProviderSpec *other) const;
    virtual void print(std::ostream &o, const LuaPrintSettings &ps) const;
};
BB_DECLARE_SHARED_PTR( LimitPriceFriendlyBookPxPSpec );

// Created to allow level price providers for possibly incomplete, but often complete, levels.
// Only returns a price if the book level in question is complete at that moment.
class CompleteBookLevelPriceProvider
    : public BookLevelPriceProvider
{
public:
    static CompleteBookLevelPriceProviderPtr create( IBookCPtr spBook, EBookPriceView view = BPV_BEST_MIDPX, int level = 0 )
    {
        if ( !spBook )
            return CompleteBookLevelPriceProviderPtr();
        return CompleteBookLevelPriceProviderPtr( new CompleteBookLevelPriceProvider( spBook, view, level ) );
    }
    static CompleteBookLevelPriceProviderPtr createBestMidpx( IBookCPtr spBook ) { return create( spBook ); }

    /// Constructor
    CompleteBookLevelPriceProvider( IBookCPtr spBook, EBookPriceView view = BPV_BEST_MIDPX, int level = 0 );

    /// Destructor
    virtual ~CompleteBookLevelPriceProvider(){};

    virtual double getRefPrice( bool* pSuccess = NULL ) const;

    /// Returns true if getRefPrice would return a valid price if asked at current moment.
    virtual bool isPriceOK() const;
};


/// PxPSpec corresponding to CompleteBookLevelPriceProvider
class CompleteBookPxPSpec : public BookPxPSpec
{
public:
    BB_DECLARE_SCRIPTING();

    CompleteBookPxPSpec();
    CompleteBookPxPSpec( const CompleteBookPxPSpec& other, const boost::optional<InstrSubst> &instrSubst );

    virtual IPriceProviderPtr build(PriceProviderBuilder *builder) const;
    virtual CompleteBookPxPSpec *clone(const boost::optional<InstrSubst> &instrSubst = boost::none) const;
    virtual bool compare(const IPxProviderSpec *other) const;
};
BB_DECLARE_SHARED_PTR( CompleteBookPxPSpec );


/// PriceSizeProvider that derives its PriceSize from a particular view of a book.
/// The view controls which information is being extracted from the market level
/// (bid,ask,midpx, etc), and level controls which level (0 being the best market).
class BookLevelPriceSizeProvider
    : public PriceSizeProviderImpl
    , protected IBookListener
{
public:
    /// Returns a BookLevelPriceSizeProvider for the given IBook.
    /// Returns an invalid smart pointer if the IBook is invalid.
    static BookLevelPriceSizeProviderPtr create( IBookCPtr spBook, EBookPriceView view = BPV_BEST_MIDPX, int level = 0 )
    {
        if ( !spBook )
            return BookLevelPriceSizeProviderPtr();
        return BookLevelPriceSizeProviderPtr( new BookLevelPriceSizeProvider( spBook, view, level ) );
    }

    /// Constructor
    BookLevelPriceSizeProvider( IBookCPtr spBook, EBookPriceView view = BPV_BEST_MIDPX, int level = 0 );

    /// Destructor
    virtual ~BookLevelPriceSizeProvider()
    {   m_spBook->removeBookListener(this); }

    /// Returns the symbol associated with this PriceProvider
    virtual instrument_t getInstrument() const
    {   return m_spBook->getInstrument(); }

    /// Returns a price based on the specified 'view' of the Book.
    /// and whose size is the total size at the best market.
    /// Returns PriceSize() if that price view is not available, and sets pSuccess to
    /// false. Otherwise returns a price and sets pSuccess true.
    virtual PriceSize getRefPriceSize( bool* pSuccess = NULL ) const
    {   return getPriceSizeFromView( getNthMarket(*m_spBook, m_level), m_view, pSuccess ); }
    /// Returns the timeval at which this PriceSizeProvider last changed.

    virtual timeval_t getLastChangeTime() const
    {   return m_spBook->getLastChangeTime(); }

    /// Returns true if getRefPriceSize would return a valid PriceSize if asked at current moment.
    virtual bool isPriceSizeOK() const;

protected:
    // IBookListener interface
    // Triggers a notification of a price change
    virtual void onBookChanged( const IBook* pBook, const Msg* pMsg,
                                int32_t bidLevelChanged, int32_t askLevelChanged );
    virtual void onBookFlushed( const IBook* pBook, const Msg* pMsg )
    {   notifyPriceSizeChanged(); }

protected:
    IBookCPtr       m_spBook;
    EBookPriceView  m_view;
    int             m_level;
};


/// PxSzPSpec corresponding to BookLevelPriceSizeProvider
/// Requires a BookSpec and a view descriptior (default = BPV_BEST_MIDPX).
class BookPxSzPSpec : public IPxSzProviderSpec
{
public:
    BB_DECLARE_SCRIPTING();

    BookPxSzPSpec();
    BookPxSzPSpec( const BookPxSzPSpec& other );

    virtual instrument_t getInstrument() const { return m_bookSpec->getInstrument(); }
    virtual IPriceSizeProviderPtr build(PriceProviderBuilder *builder) const;
    virtual BookPxSzPSpec *clone() const;
    virtual void hashCombine(size_t &result) const;
    virtual bool compare(const IPxSzProviderSpec *other) const;
    virtual void print(std::ostream &o, const LuaPrintSettings &ps) const;
    virtual void checkValid() const;
    virtual void getDataRequirements(IDataRequirements *rqs) const;

    EBookPriceView  m_view;
    IBookSpecPtr    m_bookSpec;
    int             m_level;
};
BB_DECLARE_SHARED_PTR( BookPxSzPSpec );


// Created to allow level price providers for possibly incomplete, but often complete, levels.
// Only returns a price if the book level in question is complete at that moment.
class CompleteBookLevelPriceSizeProvider
    : public BookLevelPriceSizeProvider
{
public:
    static CompleteBookLevelPriceSizeProviderPtr create( IBookCPtr spBook, EBookPriceView view = BPV_BEST_MIDPX, int level = 0 )
    {
        if ( !spBook )
            return CompleteBookLevelPriceSizeProviderPtr();
        return CompleteBookLevelPriceSizeProviderPtr( new CompleteBookLevelPriceSizeProvider( spBook, view, level ) );
    }

    /// Constructor
    CompleteBookLevelPriceSizeProvider( IBookCPtr spBook, EBookPriceView view = BPV_BEST_MIDPX, int level = 0 );

    /// Destructor
    virtual ~CompleteBookLevelPriceSizeProvider()
    {}

    /// Returns a price based on the specified 'view' of the Book.
    /// and whose size is the total size at the best market.
    /// Returns PriceSize() if that price view is not available, and sets pSuccess to
    /// false. Otherwise returns a price and sets pSuccess true.
    virtual PriceSize getRefPriceSize( bool* pSuccess = NULL ) const;
    /// Returns true if getRefPriceSize would return a valid PriceSize if asked at current moment.
    virtual bool isPriceSizeOK() const;
};

class CompleteBookPxSzPSpec : public BookPxSzPSpec
{
public:
    BB_DECLARE_SCRIPTING();

    CompleteBookPxSzPSpec();
    CompleteBookPxSzPSpec( const CompleteBookPxSzPSpec& other );

    virtual IPriceSizeProviderPtr build(PriceProviderBuilder *builder) const;
    virtual CompleteBookPxSzPSpec *clone() const;
    virtual bool compare(const IPxSzProviderSpec *other) const;
};
BB_DECLARE_SHARED_PTR( CompleteBookPxSzPSpec );

/// Stream operator to convert EBookPriceView into human readable form.
std::ostream &operator <<(std::ostream &out, const EBookPriceView &bpv);
/// Stream operator to convert EBookPriceView into Lua form.
void luaprint(std::ostream &out, EBookPriceView bpv, LuaPrintSettings const &ps);




} // namespace bb


#endif // BB_CLIENTCORE_BOOKPRICEPROVIDER_H

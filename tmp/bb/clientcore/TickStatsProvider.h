#ifndef BB_CLIENTCORE_TICKSTATSPROVIDER_H
#define BB_CLIENTCORE_TICKSTATSPROVIDER_H

/* Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved. */


#include <string>

#include <boost/function.hpp>
#include <boost/optional.hpp>

#include <bb/core/source.h>
#include <bb/core/instrument.h>
#include <bb/core/hash_map.h>
#include <bb/core/ptime.h>

namespace bb {

class TickStatisticsMsg;
BB_FWD_DECLARE_SHARED_PTR(ISendTransport);
BB_FWD_DECLARE_SHARED_PTR(ClientContext);
BB_FWD_DECLARE_SHARED_PTR(MsgHandler);

class TickStatsProvider
{
public:
    TickStatsProvider( const ClientContextPtr& cc );
    MsgHandlerPtr subscribe( boost::function<void(const TickStatisticsMsg&)> func );
    void requestSnapshot( const instrument_t& instr = instrument_t::all() ) const;

private:
    const ClientContextPtr m_context;
    const ISendTransportPtr m_requestTrans;
    const source_t m_tickStatsSource;
};

class TickStatsCache
{
public:
    TickStatsCache( const std::string& id )
        : m_client_id( id ) {}

    // returns true on success
    // must not be called concurrently with itself nor with getTotalVolume()
    bool load( ptime_duration_t timeout = boost::posix_time::seconds(3) );
    boost::optional<uint32_t> getTotalVolume( const instrument_t& instr ) const;

private:
    void handle( const TickStatisticsMsg& msg );

    const std::string m_client_id;

    struct Stats
    {
        uint32_t totalVolume;
    };
    typedef bbext::hash_map<instrument_t, Stats> stats_map_t;
    stats_map_t m_stats_map;
};

} // namespace bb

#endif // BB_CLIENTCORE_TICKSTATSPROVIDER_H

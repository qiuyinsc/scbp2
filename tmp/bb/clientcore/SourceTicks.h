#ifndef BB_CLIENTCORE_SOURCETICKS_H
#define BB_CLIENTCORE_SOURCETICKS_H

/* Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved. */

#include <bb/core/acct.h>
#include <bb/core/smart_ptr.h>
#include <bb/core/CompactMsg.h>
#include <bb/core/messages.h>

#include <boost/bind.hpp>

#include <bb/clientcore/MsgHandler.h>
#include <bb/clientcore/TickProvider.h>
#include <bb/clientcore/IClockListener.h>
#include <bb/clientcore/SessionParams.h>
#include <bb/clientcore/SourceBooks.h>
#include <bb/clientcore/ClientContext.h>
#include <bb/clientcore/ArcaUtils.h>

namespace bb {

class IsldExeOldMsg;
class IsldHexOldMsg;
class CmeBestPriceMsg;
class TdFillMsg;
class TdFillNoCounterpartyMsg;
class TdFillFutureMsg;
class CmeTradeMsg;
class CmeRecoveryBookTradeMsg;
class CmeRecoveryBookVolumeMsg;
class CmeRecoveryBookDailyStatMsg;
class CqgTradeMsg;
class CqgStatisticsMsg;

BB_FWD_DECLARE_SCOPED_PTR(IsldExeMsg);
BB_FWD_DECLARE_SCOPED_PTR(IsldHexMsg);
BB_FWD_DECLARE_SCOPED_PTR(BatsExeMsg);
BB_FWD_DECLARE_SCOPED_PTR(BatsHexMsg);
BB_FWD_DECLARE_SCOPED_PTR(EdgxExeMsg);
BB_FWD_DECLARE_SCOPED_PTR(EdgxHexMsg);
BB_FWD_DECLARE_SCOPED_PTR(Msg);
BB_FWD_DECLARE_SCOPED_PTR(LiffeTickMsg);
BB_FWD_DECLARE_SCOPED_PTR(TdFillBaseMsg);
BB_FWD_DECLARE_SCOPED_PTR(NyseOpenbookUltraExeMsg);
BB_FWD_DECLARE_SCOPED_PTR(OpraTradeMsg);
BB_FWD_DECLARE_SCOPED_PTR(TsxTl1TradeMsg);
BB_FWD_DECLARE_SCOPED_PTR(TsxTl2TradeReportMsg);
BB_FWD_DECLARE_SCOPED_PTR(ArcaTradeMsg);

BB_FWD_DECLARE_SHARED_PTR( ClockMonitor );

/// Ticks from the ISLD datafeed
class IsldTickProvider
    : public TickProviderImpl
    , private IClockListener
{
public:
    /// Constructs an IsldTickProvider for the given instrument,
    /// receiving events from the EventDistributor.
    /// Instrument must be a stock.
    IsldTickProvider( const ClientContextPtr& context, const instrument_t& instr, source_t source,
                      const std::string& desc );

    /// Destructor.
    virtual ~IsldTickProvider();

    /// Returns true if the last obtained tick is OK.
    /// Returns false if there is no last tick.
    virtual bool isLastTickOK() const           { return m_tick_ok; }

    /// Returns the last IsldExeMsg handled by this TickProvider.
    /// This will only have valid values if: isLastTickOK() && !getLastTradeTick()->isHidden()
    const IsldExeMsg& getIsldExeMsg() const    { return *m_pIsldExeMsg; }

    /// Returns the last IsldHexMsg handled by this TickProvider.
    /// This will only have valid values if: isLastTickOK() && getLastTradeTick()->isHidden()
    const IsldHexMsg& getIsldHexMsg() const    { return *m_pIsldHexMsg; }

private:
    // Event handlers
    void onIsldExe( const IsldExeMsg& msg );
    void onIsldHex( const IsldHexMsg& msg );
    // Trac #1430: backward compatibility with 32-bit ISLD messages
    void onIsldExeOld( const IsldExeOldMsg& msg );
    void onIsldHexOld( const IsldHexOldMsg& msg );

    template<typename IsldMsgT>
    void onIsldMsg( const IsldMsgT& msg, bool hidden );

    // IClockListener interface
    virtual void onWakeupCall( const timeval_t& ctv, const timeval_t& swtv, int reason, void* pData);

private:
    ClockMonitorPtr           m_spCM;
    SessionParams             m_sessionParams;
    MsgHandlerPtr             m_spIsldExeHandler, m_spIsldHexHandler, m_spIsldExeOldHandler, m_spIsldHexOldHandler;
    const IsldExeMsgScopedPtr m_pIsldExeMsg;
    const IsldHexMsgScopedPtr m_pIsldHexMsg;
    bool                      m_tick_ok;
};

BB_DECLARE_SHARED_PTR( IsldTickProvider );


template<bb::EFeedType> struct BatsEdgeTickTraits;

template<>
struct BatsEdgeTickTraits<bb::SRC_EDGX>
    : public BatsBookTraits<bb::SRC_EDGX>
{
    typedef bb::EdgxExeMsgScopedPtr ExeMsgScopedPtr;
    typedef bb::EdgxHexMsgScopedPtr HexMsgScopedPtr;
    typedef bb::EdgxHexMsg          HexMsg;

    static std::string getClassName()
    {
        return "Edgx";
    }
};

template<>
struct BatsEdgeTickTraits<bb::SRC_BATS>
    : public BatsBookTraits<bb::SRC_BATS>
{
    typedef bb::BatsExeMsgScopedPtr ExeMsgScopedPtr;
    typedef bb::BatsHexMsgScopedPtr HexMsgScopedPtr;
    typedef bb::BatsHexMsg          HexMsg;

    static std::string getClassName()
    {
        return "Bats";
    }
};

typedef BatsEdgeTickTraits<bb::SRC_BATS> BatsTickTraits;
typedef BatsEdgeTickTraits<bb::SRC_EDGX> EdgxTickTraits;

/// Ticks from the BATS or EDGX datafeed
/// The decalration and definition of the class has been done in .h file
/// because when a file includes the header file and tries to create an object
/// of BatsEdgeTickProvider it leads to undefined reference error, since it does
/// not link the .cc file at the time.
template<typename Traits>
class BatsEdgeTickProvider
    : public TickProviderImpl
    , private IClockListener
{
    typedef typename Traits::AddMsg AddMsg;
    typedef typename Traits::ExeMsg ExeMsg;
    typedef typename Traits::CxlMsg CxlMsg;
    typedef typename Traits::ModMsg ModMsg;
    typedef typename Traits::HexMsg HexMsg;
    typedef typename Traits::ExeMsgScopedPtr ExeMsgScopedPtr;
    typedef typename Traits::HexMsgScopedPtr HexMsgScopedPtr;
private:
    // Event handlers
    void onExe( const typename Traits::ExeMsg& msg )
    {
        m_tick_ok = true;
        msg.copyTo( m_pExeMsg.get() );

        setLastTradeTick( TradeTick( msg.getPx(), msg.getSz(), msg.hdr->source, // false => not hidden
                                     msg.getBatsTime(), msg.hdr->time_sent, msg.getBatsRef(), false, Traits::getMkt(),
                                     msg.getSide() ) );
        incrementTotalVolume( msg.getSz() );
        setLastTimestamps( msg.getBatsTime(), msg.hdr->time_sent );

        notifyTickReceived();
    }
    void onHex( const typename Traits::HexMsg& msg )
    {
        m_tick_ok = true;
        msg.copyTo( m_pHexMsg.get() );

        setLastTradeTick( TradeTick(
                              msg.getPx()
                              , msg.getSz()
                              , msg.hdr->source
                              , msg.getBatsTime()
                              , msg.hdr->time_sent
                              , msg.getBatsRef()
                              , true // true => hidden
                              , Traits::getMkt()
                              , boost::none // starting 20091207, BATS removed the side indicator
                              ) );
        incrementTotalVolume( msg.getSz() );
        incrementTotalHiddenVolume( msg.getSz() );
        setLastTimestamps( msg.getBatsTime(), msg.hdr->time_sent );

        notifyTickReceived();
    }

    // IClockListener interface
    virtual void onWakeupCall( const timeval_t& ctv, const timeval_t& swtv, int reason, void* pData)
    {
        // at open, reset the total volume count
        if ( reason == cm::ATOPEN )
        {
            setTotalVolume( 0 );
        }
        // at endofday, schedule ATOPEN and next ENDOFDAY
        else if ( reason == cm::ENDOFDAY )
        {
            // Jitter is an offset it milliseconds for wakeup calls
            const int jitter = (std::rand() % s_maxTimerJitter) - (s_maxTimerJitter/2); // get a jitter in range -max/2 to max/2

            std::vector<cm::clock_notice> cns = cm::getClockNotice( m_sessionParams, m_instr, m_spCM->getYMDDate(), cm::ATOPEN, jitter );
            m_spCM->scheduleClockNotices( this, cns, PRIORITY_CC_ClockMonitor.offset(PRIORITY_CC_Ticks) );
            cns = cm::getClockNotice( m_sessionParams, m_instr, m_spCM->getYMDDate(), cm::ENDOFDAY, jitter );
            m_spCM->scheduleClockNotices( this, cns, PRIORITY_CC_ClockMonitor.offset(PRIORITY_CC_Ticks) );
        }
    }

private:
    ClockMonitorPtr           m_spCM;
    SessionParams             m_sessionParams;
    MsgHandlerPtr             m_spExeHandler, m_spHexHandler;
    const ExeMsgScopedPtr     m_pExeMsg;
    const HexMsgScopedPtr     m_pHexMsg;
    bool                      m_tick_ok;

public:
    /// Constructs a BatsTickProvider for the given instrument,
    /// receiving events from the EventDistributor.
    /// Instrument must be a stock.
    BatsEdgeTickProvider( const ClientContextPtr& context, const instrument_t& instr, source_t source,
                          const std::string& desc )
        : TickProviderImpl( source, instr, desc, true )  // true => estimated total volume
        , m_spCM( context->getClockMonitor() )
        , m_sessionParams( context->getSessionParams() )
        , m_pExeMsg( new CompactMsg<ExeMsg> )
        , m_pHexMsg( new CompactMsg<HexMsg> )
        , m_tick_ok( false )
    {
        if ( m_instr.prod != PROD_STOCK )
            throw std::invalid_argument( Traits::getClassName() + "TickProvider: instr is not a stock" );
        if ( m_source.type() != Traits::getSource() )
            throw std::invalid_argument( Traits::getClassName() + "TickProvider: invalid source received: " + source.toString() );
        EventDistributorPtr spED = context->getEventDistributor();
        if ( !spED )
            throw std::invalid_argument( Traits::getClassName() + "TickProvider: bad EventDist" );
        if ( !m_spCM )
            throw std::invalid_argument( Traits::getClassName() + "TickProvider: bad ClockMonitor" );

        m_spExeHandler = MsgHandler::create<ExeMsg>(
            source, m_instr, spED, boost::bind(&BatsEdgeTickProvider::onExe, this, _1), PRIORITY_CC_Ticks );
        m_spHexHandler = MsgHandler::create<HexMsg>(
            source, m_instr, spED, boost::bind(&BatsEdgeTickProvider::onHex, this, _1), PRIORITY_CC_Ticks );

        m_spCM->scheduleClockNotice( this, cm::clock_notice(cm::ENDOFDAY,0), PRIORITY_CC_ClockMonitor.offset(PRIORITY_CC_Ticks) );
    }

    /// Destructor.
    virtual ~BatsEdgeTickProvider()
    {
        m_spCM->unscheduleClockNotices( this );
    }

    /// Returns true if the last obtained tick is OK.
    /// Returns false if there is no last tick.
    virtual bool isLastTickOK() const           { return m_tick_ok; }

    /// Returns the last ExeMsg handled by this TickProvider.
    /// This will only have valid values if: isLastTickOK() && !getLastTradeTick()->isHidden()
    const ExeMsg& getExeMsg() const      { return *m_pExeMsg; }

    /// Returns the last HexMsg handled by this TickProvider.
    /// This will only have valid values if: isLastTickOK() && getLastTradeTick()->isHidden()
    const HexMsg& getHexMsg() const      { return *m_pHexMsg; }
};

typedef BatsEdgeTickProvider<BatsTickTraits> BatsTickProvider;
typedef BatsEdgeTickProvider<EdgxTickTraits> EdgxTickProvider;

BB_DECLARE_SHARED_PTR( BatsTickProvider );
BB_DECLARE_SHARED_PTR( EdgxTickProvider );

class ArcaTickProvider
    : public TickProviderImpl
{
public:
    /// Constructs a ArcaTickProvider for the given instrument,
    /// receiving events from the EventDistributor.
    /// Instrument must be a stock
    ArcaTickProvider( const ClientContextPtr& context, const instrument_t& intr, source_t source,
                      const std::string& desc );

    virtual ~ArcaTickProvider();

    /// Returns true if the last obtained tick is OK
    /// Returns false if there is no last obtained tick
    virtual bool isLastTickOK() const { return m_tick_ok; }

    /// Returns hte last ArcaTradeMsg handled by this TickProvider
    /// This will only have valid values if : isLastTickOK() && !getLastTradeTick()->isHidden()
    const ArcaTradeMsg& getArcaTradeMsg() const { return *m_pArcaTradeMsg; }

private:
    // Event handlers
    void onArcaTrade( const ArcaTradeMsg& msg );

private:

    ClockMonitorPtr m_spCM;
    SessionParams m_sessionParams;
    MsgHandlerPtr m_spArcaTradeHandler;
    const ArcaTradeMsgScopedPtr m_pArcaTradeMsg;
    bool m_tick_ok;

    PriceScaler<ArcaPriceScalerTraits> m_price_converter;

    static const uint8_t OPEN_PRICE_COND = 'Q';
    static const uint8_t CLOSE_PRICE_COND = 'M';
};
BB_DECLARE_SHARED_PTR( ArcaTickProvider );

/// Ticks from the CME datafeed
class CmeTickProvider
    : public TickProviderImpl
{
public:
    /// Constructs a CmeTickProvider for the given instrument,
    /// receiving events from the EventDistributor.
    /// Instrument must be a future or option.
    CmeTickProvider( const ClientContextPtr& context, const instrument_t& instr, source_t source,
                     const std::string& desc );

    /// Destructor.
    virtual ~CmeTickProvider();

    /// Returns true if the last obtained tick is OK.
    /// Returns false if there is no last tick.
    virtual bool isLastTickOK() const                       { return m_tick_ok; }

    /// Overriding isAllowedType to allow all types of ticks
    /// to be allowed to be processed by CmeTickProvider
    virtual bool isAllowedType( TickTypes tickType );

    /// Returns the last TradeMsg handled by this TickProvider.
    /// This can be of type CmeTradeMsg of CmeBookRecoveryTradeMsg
    /// This will only have valid values if: getTotalVolume() > 0
    const Msg& getCmeTradeMsg() const;
private:
    // Event handlers
    void onCmeTrade( const CmeTradeMsg& msg );
    void onCmeRecoveryTrade( const CmeRecoveryBookTradeMsg& msg );
    void onCmeRecoveryVolume( const CmeRecoveryBookVolumeMsg& msg );
    void onCmeRecoveryStat( const CmeRecoveryBookDailyStatMsg& msg );
private:
    // enum MsgIndex{TRADE_MSG, RECOVERY_TRADE_MSG};

    std::vector<MsgHandlerPtr> m_spCmeHandlerVec;
    MsgScopedPtr               m_pCmeTradeMsg;
    bool                       m_tick_ok;
    uint32_t                   m_openInterest;
};

BB_DECLARE_SHARED_PTR( CmeTickProvider );

/// Ticks from the CQG datafeed
class CqgTickProvider
    : public TickProviderImpl
{
public:
    /// Constructs a CqgTickProvider for the given instrument,
    /// receiving events from the EventDistributor.
    /// Instrument must be a future or option.
    CqgTickProvider( const ClientContextPtr& context, const instrument_t& instr, source_t source,
                     const std::string& desc );

    /// Destructor.
    virtual ~CqgTickProvider();

    /// Returns true if the last obtained tick is OK.
    /// Returns false if there is no last tick.
    virtual bool isLastTickOK() const                       { return m_tick_ok; }

    /// Overriding isAllowedType to allow all types of ticks
    /// to be allowed to be processed by CqgTickProvider
    virtual bool isAllowedType( TickTypes tickType );

    /// Returns the last TradeMsg handled by this TickProvider.
    /// This will only have valid values if: getTotalVolume() > 0
    const Msg& getCqgTradeMsg() const;
private:
    // Event handlers
    void onCqgTrade( const CqgTradeMsg& msg );
    void onCqgStat( const CqgStatisticsMsg& msg );
private:

    std::vector<MsgHandlerPtr> m_spCqgHandlerVec;
    MsgScopedPtr               m_pCqgTradeMsg;
    bool                       m_tick_ok;
    uint32_t                   m_openInterest;
};

BB_DECLARE_SHARED_PTR( CqgTickProvider );



/// Ticks from the LIFFE datafeed
class LiffeTickProvider
    : public TickProviderImpl
{
public:
    /// Constructs a LiffeTickProvider for the given instrument,
    /// receiving events from the EventDistributor.
    /// Instrument must be a future or option.
    /// The market must be either CBOT or LIFFE.
    LiffeTickProvider( const ClientContextPtr& context, const instrument_t& instr, source_t source,
                       mktdest_t mkt, const std::string& desc );

    /// Destructor.
    virtual ~LiffeTickProvider();

    /// Returns true if the last obtained tick is OK.
    /// Returns false if there is no last tick.
    virtual bool isLastTickOK() const               { return m_tick_ok; }

    /// Returns the last LiffeTickMsg handled by this TickProvider.
    /// This will only have valid values if: isLastTickOK()
    const LiffeTickMsg& getLiffeTickMsg() const    { return *m_pLiffeTickMsg; }

private:
    // Event handler
    void onLiffeTick( const LiffeTickMsg& msg );

private:
    MsgHandlerPtr               m_spLiffeTickHandler;
    const LiffeTickMsgScopedPtr m_pLiffeTickMsg;
    bool                        m_tick_ok;
    mktdest_t                   m_mkt;
};

BB_DECLARE_SHARED_PTR( LiffeTickProvider );


/// Ticks from the ACR ORDERS datafeed
///
/// The TradeTick reference number is:  tid.account.id() << 32 + tid.orderid
///
/// Note that our td_fill feed throws away the "hidden" property of an
/// order, so the reported TradeTicks are always marked as visible,
/// even though they originally may not have been.
/// A workaround for this could be to track td_status, but until people even use this
/// class, it's not clear if it is worth it.
class AcrOrderTickProvider
    : public TickProviderImpl
    , private IClockListener
{
public:
    /// Constructs an AcrOrderTickProvider for the given instrument and account,
    /// receiving events from the EventDistributor.
    /// If account is ACCT_UNKNOWN, then all accounts are considered.
    AcrOrderTickProvider( const ClientContextPtr& context, const instrument_t& instr, source_t source,
                          const std::string& desc, acct_t acct = ACCT_UNKNOWN );

    /// Destructor.
    virtual ~AcrOrderTickProvider();

    /// Returns true if the last obtained tick is OK.
    /// Returns false if there is no last tick.
    virtual bool isLastTickOK() const       { return m_tick_ok; }

    /// Returns the last TdFill message handled handled by this TickProvider.
    /// This will only have valid values if: isLastTickOK()
    /// Note that a TdFillBase message is returned, but it may be dynamic_cast to the
    /// various other messages.
    const TdFillBaseMsg& getTdFillBaseMsg() const  { return *m_pTdFillBaseMsg; }

private:
    // Event handler
    void onTdFill( const TdFillMsg& msg );
    void onTdFillNoCounterparty( const TdFillNoCounterpartyMsg& msg );
    void onTdFillFuture( const TdFillFutureMsg& msg );

    // IClockListener interface
    void onWakeupCall( const timeval_t& ctv, const timeval_t& swtv, int reason, void* pData);

private:
    ClockMonitorPtr              m_spCM;
    SessionParams                m_sessionParams;
    MsgHandlerPtr                m_spTdFillHandler, m_spTdFillNoCounterHandler, m_spTdFillFutureHandler;
    TdFillBaseMsgScopedPtr       m_pTdFillBaseMsg;
    acct_t                       m_acct;
    bool                         m_tick_ok;
};

BB_DECLARE_SHARED_PTR( AcrOrderTickProvider );


/// Ticks from the ISLD datafeed
class NyseOpenBookUltraTickProvider
    : public TickProviderImpl
    , private IClockListener
{
public:
    /// Constructs an NyseOpenBookUltraTickProvider for the given instrument,
    /// receiving events from the EventDistributor.
    /// Instrument must be a stock.
    NyseOpenBookUltraTickProvider( const ClientContextPtr& context, const instrument_t& instr, source_t source,
                      const std::string& desc );

    /// Destructor.
    virtual ~NyseOpenBookUltraTickProvider();

    /// Returns true if the last obtained tick is OK.
    /// Returns false if there is no last tick.
    virtual bool isLastTickOK() const           { return m_tick_ok; }

    /// Returns the last NyseOpenbookUltraExeMsg handled by this TickProvider.
    /// This will only have valid values if: isLastTickOK() && !getLastTradeTick()->isHidden()
    const NyseOpenbookUltraExeMsg& getNyseOpenbookUltraExeMsg() const    { return *m_pNyseOpenbookUltraExeMsg; }

private:
    // Event handlers
    void onNyseOpenbookUltraExe( const NyseOpenbookUltraExeMsg& msg );

    // IClockListener interface
    virtual void onWakeupCall( const timeval_t& ctv, const timeval_t& swtv, int reason, void* pData);

private:
    ClockMonitorPtr                        m_spCM;
    SessionParams                          m_sessionParams;
    MsgHandlerPtr                          m_spNyseOpenbookUltraExeHandler;
    const NyseOpenbookUltraExeMsgScopedPtr m_pNyseOpenbookUltraExeMsg;
    bool                                   m_tick_ok;
};

BB_DECLARE_SHARED_PTR( NyseOpenBookUltraTickProvider );


/// Ticks from the OPRA datafeed
class OpraTickProvider
    : public TickProviderImpl
    , private IClockListener
{
public:
    /// Constructs a OpraTickProvider for the given instrument, receiving
    /// events from the EventDistributor.  Instrument must be an option.
    OpraTickProvider( const ClientContextPtr& context, const instrument_t& instr,
        source_t source, const std::string& desc );

    virtual ~OpraTickProvider();

    /// Returns true if the last obtained tick is OK.
    /// Returns false if there is no last tick.
    virtual bool isLastTickOK() const           { return m_tick_ok; }

    /// Returns the last OpraTradeMsg handled by this TickProvider.
    /// This will only have a valid value if: isLastTickOK()
    const OpraTradeMsg& getOpraTradeMsg() const  { return *m_tradeMsg; }

private:
    // Event handlers
    void onOpraTrade( const OpraTradeMsg& msg );

    // IClockListener interface
    virtual void onWakeupCall( const timeval_t& ctv, const timeval_t& swtv, int reason, void* pData );

private:
    ClockMonitorPtr             m_spCM;
    MsgHandlerPtr               m_spTickHandler;
    const OpraTradeMsgScopedPtr m_tradeMsg;
    bool                        m_tick_ok;
};


/// Ticks from the TSX TL1 datafeed
class TsxTl1TickProvider
    : public TickProviderImpl
{
public:
    /// Constructs a TsxTickProvider for the given instrument based on the SRC_TSX (TL1) data feed.
    TsxTl1TickProvider( const ClientContextPtr& context, const instrument_t& instr,
        source_t source, const std::string& desc );

    /// Returns true if the last obtained tick is OK.
    /// Returns false if there is no last tick.
    virtual bool isLastTickOK() const           { return m_tick_ok; }

    /// Returns the last TsxTl1TradeMsg handled by this TickProvider.
    /// This will only have a valid value if: isLevelOne() && isLastTickOK()
    const TsxTl1TradeMsg& getTsxTl1TradeMsg() const  { return *m_tl1TradeMsg; }

private:
    // Event handlers
    void onTsxTl1Trade( const TsxTl1TradeMsg& msg );

private:
    MsgHandlerPtr                 m_spTickHandler;
    const TsxTl1TradeMsgScopedPtr m_tl1TradeMsg;
    bool                          m_tick_ok;
};

/// Ticks from the TSX CDF datafeed
class TsxTl2TickProvider
    : public TickProviderImpl
{
public:
    /// Constructs a TsxTickProvider for the given instrument based on the SRC_TSX_CDF data feed.
    TsxTl2TickProvider( const ClientContextPtr& context, const instrument_t& instr,
        source_t source, const std::string& desc );

    /// Returns true if the last obtained tick is OK.
    /// Returns false if there is no last tick.
    virtual bool isLastTickOK() const           { return m_tick_ok; }

    /// Returns the last TsxTl2TradeReportMsg handled by this TickProvider.
    /// This will only have a valid value if: isLevelTwo() && isLastTickOK()
    const TsxTl2TradeReportMsg& getTsxTl2TradeReportMsg() const  { return *m_tl2TradeMsg; }

private:
    // Event handlers
    void onTsxTl2Trade( const TsxTl2TradeReportMsg& msg );

private:
    MsgHandlerPtr                       m_spTickHandler;
    const TsxTl2TradeReportMsgScopedPtr m_tl2TradeMsg;
    bool                                m_tick_ok;
};

} // namespace bb

#endif // BB_CLIENTCORE_SOURCETICKS_H

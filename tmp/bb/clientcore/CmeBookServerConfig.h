#ifndef BB_CLIENTCORE_CMEBOOKSERVERCONFIG_H
#define BB_CLIENTCORE_CMEBOOKSERVERCONFIG_H

/* Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved. */

#include <map>

#include <bb/core/EFeedType.h>
#include <bb/core/hash_set.h>
#include <bb/core/LuaConfig.h>
#include <bb/core/instrument.h>

class CmeBookServerConfigTestFixture;

namespace cme {

class CmeBookServerHostConfig : public bb::LuaConfig<CmeBookServerHostConfig>
{
public:
    typedef std::set<bb::instrument_t, bb::instrument_t::less_no_mkt_no_currency> InstrumentList;
    const InstrumentList& getInstrumentList() const;
    uint32_t getPort() const { return m_port; }
    boost::optional<uint32_t> getThrottleRate() const { return m_throttleRate; }
    static void describe();
private:
    InstrumentList m_instrumentList;
    uint32_t m_port;
    boost::optional<uint32_t> m_throttleRate;
};

struct CmeBookServerConfig
{
public:
    CmeBookServerConfig( const luabind::table<>& );

    typedef std::map<std::string, CmeBookServerHostConfig> HostConfigMap;
    HostConfigMap mapFQDNConfig;
    std::string seqnum_file;
    bb::EFeedType feed;
    std::string output_file;
    bool        perform_recovery;

    std::vector<std::string> getHostsNamesForInstrument( const bb::instrument_t& instr ) const;

    static bool registerConfigLua( lua_State& lua );
};



}

#endif // BB_CLIENTCORE_CMEBOOKSERVERCONFIG_H

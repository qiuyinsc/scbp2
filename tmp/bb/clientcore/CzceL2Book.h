#ifndef BB_CLIENTCORE_CZCEL2BOOK_H
#define BB_CLIENTCORE_CZCEL2BOOK_H

/* Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved. */

#include <boost/unordered_map.hpp>
#include <bb/core/messages.h>
#include <bb/clientcore/ClockMonitor.h>
#include <bb/clientcore/Book.h>
#include <bb/clientcore/SourceBooks.h>

namespace bb {

class CzceL2BookLevelCIter;

class CzceL2Book : public bb::BookImpl
                 , private bb::IEventDistListener
                 , private bb::SourceMonitorListener

{

public:
    CzceL2Book( const bb::instrument_t& instr, const bb::ClientContextPtr& spCC, bb::source_t src,
                const std::string& desc,  const bb::SourceMonitorPtr& spSMon);
    virtual ~CzceL2Book();

public:

    // IBook interface
    /// Returns true if the book is in a "valid" state.
    /// The meaning of this is book-dependent, however clients typically use it
    /// to test whether they should "trust" the book's contents.
    /// For example, getMidPrice should return a valid price if isOK is true.
    virtual bool isOK() const;

    /// Flushes the book.
    virtual void flushBook();

    /// Returns the mid-price of a book, or 0.0 if the book is not valid or empty.
    /// If one side of the book is empty, WindBook's getMidPrice will
    /// return the price of the existing side.
    virtual double getMidPrice() const;

    /// Returns the PriceSize at a specified depth and side.
    /// Returns an empty PriceSize if nothing exists at that depth or side.
    virtual PriceSize getNthSide( size_t depth, bb::side_t side ) const;

    /// Returns an object for iterating over BookLevels of a particular side.
    /// Postcondition: The returned iterator will iterate getNumLevels objects.
    /// This abstracts the underlying containers used by a book.
    virtual IBookLevelCIterPtr getBookLevelIter( bb::side_t side ) const;

    /// Returns the number of levels of a particular side of this book.
    virtual size_t getNumLevels( bb::side_t side ) const;

    double getLimitUpPrice() const
    {
        return m_limitUpPrice;
    }

    double getLimitDownPrice() const
    {
        return m_limitDownPrice;
    }

    int64_t getTotalVolume() const { return m_totalVolume; };
    double getAveragePrice() const { return m_avgPrice; }

    uint32_t getTotalAskLot() const {return m_totalAskLot;}
    uint32_t getTotalBidLot() const {return m_totalBidLot;}
    boost::optional<uint32_t> getOpenInterest() const { return m_openInterest; }

private:
    /// EventListener interface
    virtual void onEvent( const bb::Msg& msg );

    void processMsg( const CzceL2QdMsg& msg );

    template<bb::side_t side>
    int32_t updateBookLevels( const CzceL2QdMsg& msg );

    template<bb::side_t side>
    std::pair<size_t, bool> tryAddLevel( const double price, const int32_t size );

    template<bb::side_t side>
    size_t tryDropLevels( std::bitset<32> seenLevels );

    void recordTickInfo( const CzceL2QdMsg& msg );


    /// SourceMonitorListener interface
    virtual void onSMonStatusChange( bb::source_t src, bb::smon_status_t status, bb::smon_status_t old_status,
                                     const bb::ptime_duration_t& avg_lag_tv, const bb::ptime_duration_t& last_interval_tv );


private:
    void process_message( const bb::CzceL2QdMsg& msg );
    void triggerBookChanges( const bb::Msg& msg );


private:
    typedef std::vector<bb::BookLevelPtr> BookLevelVec;
    typedef std::map<double,BookLevelPtr,bb::FPComp> BookLevelMap;
    typedef std::bitset<32> SeenLevelsSet;

    ClientContextPtr m_spCC;
    SourceMonitorPtr m_spSMon;
    const uint32_t   m_verbose;

    mutable bool     m_ok;
    mutable bool     m_ok_dirty;
    bool             m_source_ok;

    BookLevelVec     m_bookLevels[2]; //BID = 0, ASK = 1
    EventSubPtr      m_event_sub;

    bool             m_isInLimitDown;
    double           m_limitDownPrice;
    bool             m_isInLimitUp;
    double           m_limitUpPrice;

    double           m_avgPrice;
    uint32_t         m_totalVolume;

    uint32_t         m_totalBidLot;
    uint32_t         m_totalAskLot;
    boost::optional<uint32_t> m_openInterest;
};

BB_DECLARE_SHARED_PTR( CzceL2Book );

// Book spec
class CzceL2BookSpec : public bb::SourceBookSpec
{
public:
    BB_DECLARE_SCRIPTING();

    CzceL2BookSpec() : SourceBookSpec() {};
    CzceL2BookSpec(const CzceL2BookSpec& other, const boost::optional<bb::InstrSubst>& instr) : SourceBookSpec(other, instr) {}
    CzceL2BookSpec(const bb::instrument_t& instr, bb::source_t src) : SourceBookSpec(instr, src) {}

public:
    virtual bb::IBookPtr build(bb::BookBuilder *builder) const;
    virtual void print(std::ostream& out, const bb::LuaPrintSettings& ps) const;
    virtual CzceL2BookSpec* clone(const boost::optional<bb::InstrSubst>& instrSubst = boost::none) const;
};

BB_DECLARE_SHARED_PTR(CzceL2BookSpec);


}


#endif // BB_CLIENTCORE_CZCEL2BOOK_H

#ifndef BB_CLIENTCORE_CFFEXSFITCOMBOTICK_H
#define BB_CLIENTCORE_CFFEXSFITCOMBOTICK_H

/* Contents Copyright 2012 Athena Capital Research LLC. All Rights Reserved. */


#include <vector>
#include <bb/clientcore/TickProvider.h>

namespace bb {

class CffexSfitComboTickProvider
    : public TickProviderImpl
    , protected ITickListener
{
public:
    CffexSfitComboTickProvider( const ClientContextPtr& context, const instrument_t& instr,
                                source_t source, const std::string& desc );
    virtual ~CffexSfitComboTickProvider();

    virtual bool isLastTickOK() const
    { return m_ok; }

protected:
    /// Update our totals and forward tick to our listeners.
    virtual void onTickReceived( const ITickProvider* tp, const TradeTick& tick );

protected:
    ITickProviderPtr m_spCffexTick;
    ITickProviderPtr m_spSfitTick;
    ITickProviderPtr m_spCffexNhProxyTick;
    bool m_ok;
};
BB_DECLARE_SHARED_PTR( CffexSfitComboTickProvider );


} // namespace bb

#endif // BB_CLIENTCORE_CFFEXSFITCOMBOTICK_H

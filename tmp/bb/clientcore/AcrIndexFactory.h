#ifndef BB_CLIENTCORE_ACRINDEXFACTORY_H
#define BB_CLIENTCORE_ACRINDEXFACTORY_H

/* Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved. */


#include <bb/core/smart_ptr.h>
#include <bb/clientcore/AcrIndex.h>
#include <bb/clientcore/TickFactory.h>
#include <bb/clientcore/BookBuilder.h>
#include <bb/clientcore/PriceProviderSpec.h>

namespace bb {

// Forward declarations
BB_FWD_DECLARE_SHARED_PTR( IBookSpec );

BB_FWD_DECLARE_SHARED_PTR( IFilteredBookFactory );
namespace AcrIndex{
    BB_FWD_DECLARE_SHARED_PTR( Holdings );

}
class BookBasedAcrIndexFactory
{
public:
    /// The search checks the dates from tv back to the next earlier published date.  If none are found,
    /// it is considered failed
    BookBasedAcrIndexFactory( IBookBuilderPtr const&spBookBuilder,std::string const&dbprofile ="production");
    virtual ~BookBasedAcrIndexFactory();

    /// Creates the given BookBasedAcrIndex. The index you're interested in should be in bookSpec->getInstrument().
    /// This will be replaced with each underlying instrument to build the underlying books.
    BookBasedAcrIndexPtr getAcrIndex(  bb::timeval_t const& tv,  IBookSpec const& bookSpec);
    BookBasedAcrIndexPtr getAcrIndex( BookAcrIndexPxPSpecCPtr const&spBookAdxSpec );
    BookBasedAcrIndexPtr makeAcrIndex( BookAcrIndexPxPSpecCPtr const&spBookAdxSpec,AcrIndex::HoldingsCPtr const&  spHoldings  );
    BookBasedAcrIndexPtr makeAcrIndex(bb::timeval_t const& tv,  IBookSpec const& bookSpec,AcrIndex::HoldingsCPtr const&  spHoldings  );

private:
    IBookBuilderPtr m_spBookBuilder;
    typedef bbext::hash_map<BookAcrIndexPxPSpecCPtr, BookBasedAcrIndexPtr, PxPSpecHasher, PxPSpecComparator> Spec2AcrIndexMap;
    Spec2AcrIndexMap  m_adx_cache;
    std::string m_dbprofile;

    virtual IBookPtr  massagebook(IBookPtr const&);

};
BB_DECLARE_SHARED_PTR( BookBasedAcrIndexFactory );

class BookBasedAcrIndexFactory2 : private BookBasedAcrIndexFactory
{
public:
    /// The search checks the dates from tv back to the next earlier published date.  If none are found,
    /// it is considered failed
    BookBasedAcrIndexFactory2(IBookBuilderPtr const&spBookBuilder, IFilteredBookFactoryCPtr const&filt, std::string const&dbprofile );


    /// Creates the given BookBasedAcrIndex. The index you're interested in should be in bookSpec->getInstrument().
    /// This will be replaced with each underlying instrument to build the underlying books.
    BookBasedAcrIndexPtr getAcrIndex( IBookSpec const& bookSpec);
    BookBasedAcrIndexPtr makeAcrIndex(  IBookSpec const& bookSpec,AcrIndex::HoldingsCPtr const&  spHoldings  );
private:
    BookBasedAcrIndexFactory2(bb::timeval_t const& _tradedate, IBookBuilderPtr const&spBookBuilder,  std::string const&dbprofile );

    bb::timeval_t m_tradedate;
    IFilteredBookFactoryCPtr m_filter;
    IBookPtr  massagebook(IBookPtr const&);
};

BB_DECLARE_SHARED_PTR( BookBasedAcrIndexFactory );


} // namespace bb


#endif // BB_CLIENTCORE_ACRINDEXFACTORY_H

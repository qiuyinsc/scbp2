#ifndef BB_CLIENTCORE_PRICESIZEPROVIDERSPEC_H
#define BB_CLIENTCORE_PRICESIZEPROVIDERSPEC_H

/* Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved. */


#include <bb/core/instrument.h>
#include <bb/core/smart_ptr.h>
#include <bb/clientcore/BookSpec.h>

namespace bb {

// Forward declarations
class IPxSzProviderSpec;
class BookPxSzPSpec;
class SourceTickPxSzPSpec;
class MasterTickPxSzPSpec;
class IPriceSizeProvider;
class PriceProviderBuilder;
BB_FWD_DECLARE_SHARED_PTR( IPriceSizeProvider );
BB_FWD_DECLARE_SHARED_PTR( IPxSzProviderSpec );


/// An IPxSzProviderSpec is a description of a IPxSzProvider to be built.
/// Having these classes as data means we can pass them around the network.
/// A PxSzProviderBuilder is responsible for taking them and actually creating the IPxSzProvider.
class IPxSzProviderSpec
{
public:
    BB_DECLARE_SCRIPTING();

    virtual ~IPxSzProviderSpec() {}

    virtual instrument_t getInstrument() const = 0;

    /// Instantiates a PriceSizeProviderSpec according to the Spec
    virtual IPriceSizeProviderPtr build(PriceProviderBuilder *builder) const = 0;

    /// deep copy this IPxSzProviderSpec
    virtual IPxSzProviderSpec *clone() const = 0;
    // helper fn for cloning
    static inline IPxSzProviderSpec *clone(const IPxSzProviderSpec* e) { return e ? e->clone() : NULL; }
    static inline IPxSzProviderSpec *clone(IPxSzProviderSpecCPtr e) { return clone(e.get()); }

    /// Combines this IPxSzProviderSpec into the given hash value
    virtual void hashCombine(size_t &result) const = 0;

    /// Compares two signal specs.
    virtual bool compare(const IPxSzProviderSpec *other) const = 0;

    /// Prints the lua representation of this SignalSpec.
    virtual void print(std::ostream &o, const LuaPrintSettings &ps) const = 0;

    /// ensures sure that all fields have been set correctly, or throws runtime_error
    virtual void checkValid() const = 0;

    /// Propagates all the required data (instr/src pairs) to the IDataRequirements.
    virtual void getDataRequirements(IDataRequirements *rqs) const = 0;
};

bool operator==(const IPxSzProviderSpec &a, const IPxSzProviderSpec &b);
inline bool operator!=(const IPxSzProviderSpec &a, const IPxSzProviderSpec &b) { return !(a == b); }

std::ostream &operator<<(std::ostream &out, const IPxSzProviderSpec &spec);
void luaprint(std::ostream &out, IPxSzProviderSpec const &spec, LuaPrintSettings const &ps);

std::size_t hash_value(const IPxSzProviderSpec &a);

// helpers for hash_map
struct PxSzPSpecHasher : public std::unary_function<size_t, IPxSzProviderSpecCPtr>
    { size_t operator()(const IPxSzProviderSpecCPtr &a) const { return hash_value(*a); } };
struct PxSzPSpecComparator : public std::binary_function<bool, IPxSzProviderSpecCPtr, IPxSzProviderSpecCPtr>
    { bool operator()(const IPxSzProviderSpecCPtr &a, const IPxSzProviderSpecCPtr &b) const { return *a == *b; } };


} // namespace bb


#endif // BB_CLIENTCORE_PRICESIZEPROVIDERSPEC_H

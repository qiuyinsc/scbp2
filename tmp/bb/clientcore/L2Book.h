#ifndef BB_CLIENTCORE_L2BOOK_H
#define BB_CLIENTCORE_L2BOOK_H

/* Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved. */


#include <bb/clientcore/L2BookBase.h>

#include <bb/core/LuaConfig.h>
#include <bb/core/PreallocatedObjectFactory.h>

namespace bb {

class L2BookConfig
    : public LuaConfig<L2BookConfig>
{
public:
    L2BookConfig(int32_t vbose = 0): verbose( vbose ){}
    static void describe()
    {
        Self::create()
            .param("book_order_factory_config",        &Self::bookOrderConfig)
            .param("book_level_factory_config",        &Self::bookLevelConfig)
            .param("verbose",                          &Self::verbose)
            ;
    }

    ObjectFactoryConfig         bookOrderConfig;
    ObjectFactoryConfig         bookLevelConfig;
    int32_t                     verbose;
};


/// L2Book both defines an interface for all L2Books and also provides an
/// implementation for doing common bookkeeping and organization that all
/// L2Books will want.  Depths and Levels should be numbered counting from 0
/// is the best mkt.
class L2Book
    : public L2BookBase
{
public:
    L2Book( const ClientContextPtr& cc, const instrument_t& instr, SourceMonitorPtr spSMon,
            const char* desc, L2BookConfig config = L2BookConfig() );
virtual ~L2Book();

public:

    /// methods for adding or dropping orders from book child classes usually
    /// provide their own wrappers around these functions called createOrder
    /// and dropOrder
    virtual bool addOrder(BookOrder* o);

    /// after dropOrder is called, o is deleted, and will no longer be valid.
    virtual void dropOrder(BookOrder* o);

    /// IMPORTANT: any pointers or iterators to BookOrders or BookLevels will
    /// be invalidated when flushBook is called, and might be invalidated by
    /// dropBookLevel
    /// Returns true if it drops the level.
    virtual bool dropBookLevelImpl( side_t side, double px );

    /// Flushes the book. Listeners are notified with onBookFlush.  Otherwise, listeners are notified onBookChange.
    virtual void flushBook();

    /// ClockListener interface
    virtual void onWakeupCall( const timeval_t &ctv, const timeval_t &swtv, int reason, void* pData );

protected:
    virtual void removeLiquidity(BookOrder* o, int sz_chg);

    // Given an existing order 'o' in this book, change the order to
    // have the size 'revised_size' at price 'revised_price'. The
    // revised values must be non-zero. Note that if the revised price
    // is different from the current price, then the book order will
    // move to a new book level via a call to addOrder(o).
    virtual void reviseOrderSizeAndPrice( BookOrder* o, int revised_size, double revised_price );

    /// Default behavior for a Crossing/Locking order is to add it, but
    /// prevent it from crossing/locking the book by dropping orders off the
    /// other side of the book.  However, default behavior is used by none of
    /// the existing books. All existing inheriting books implement their own
    /// versions of hCO.
    /// Returns true if the order should be added to the Book.
    virtual bool handleCrossedOrder(BookOrder* o);
    /// drops all orders on the other side that lock or cross Order o
    /// Order o is not modified, and will presumably be added after a call
    /// to this method. Primarily used by handleCrossedOrder for SourceBooks
    /// where we trust the new info to be correct, implying that we dropped
    /// or missed a message to delete the offending order(s).
    void dropCrossedOrders(BookOrder* o);
    /// a bail out option to use during addOrder: places the order into a
    /// holding container where it is not visible in any way to outside world,
    /// schedules a wakeup call to try reinserting it again every
    /// m_retryInsertTime.
    void putNewOrderOnHold(BookOrder* o);

    // creates a book level from the book order
    // this method was created so that if the MasterBook wants to use a modified book level
    // we could over-ride this function and create an object of that modified BooKLevel class
    virtual BookLevel* createNewBookLevel(BookOrder* order);

    // Flushes a side of the book.
    // Does not call any listeners
    virtual void flushSide( side_t side, int& level_cnt, int& ttl_ord_cnt );

    typedef std::vector<BookOrder*> onhold_vec;
    onhold_vec onhold_orders;
    typedef std::map<BookOrder*, int> onhold_order_cnts_t;
    onhold_order_cnts_t onhold_order_cnts;

    BookOrderFactoryPtr     m_spBookOrderFactory;
    BookLevelFactoryPtr     m_spBookLevelFactory;

    static const int R_RETRY_INSERT = cm::USER_REASON + 3;
};

BB_DECLARE_SHARED_PTR( L2Book );

} // namespace bb


#endif // BB_CLIENTCORE_L2BOOK_H

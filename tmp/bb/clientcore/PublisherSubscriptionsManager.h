#ifndef BB_CLIENTCORE_PUBLISHERSUBSCRIPTIONSMANAGER_H
#define BB_CLIENTCORE_PUBLISHERSUBSCRIPTIONSMANAGER_H

/* Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved. */


#include <bb/core/hash_map.h>
#include <bb/core/instrument.h>
#include <bb/core/mtype.h>
#include <bb/core/source.h>
#include <bb/core/QuitMonitor.h>
#include <bb/core/SelectDispatcher.h>
#include <bb/core/queue/waitable_queue.h>
#include <bb/core/queue/ThreadPipeNotifier.h>

#include <bb/io/feeds.h>

namespace bb {
BB_FWD_DECLARE_SHARED_PTR( PublisherManager );

class PublisherSubscriptionsManager
{
public:
    // id is sent in the ClientIdentifyMsg to the publisher (QD)
    // this is typically the same id string as given to LiveClientContext
    PublisherSubscriptionsManager( const std::string& id );

    ~PublisherSubscriptionsManager();

    // set the list of hosts and ports for subscriptions to the given source
    // thread-safe, passes request to the subscription thread
    // do not call twice for the same source
    // does blocking DNS lookup
    void setTarget( source_t source, const HostPortPairList& hosts );

    // send a subscription request for one or all instruments on the given source
    // mtype must be a specific message type, not MSG_ALL
    // instr can be instrument_t::all()
    // thread-safe, passes request to the subscription thread
    void   subscribe( source_t source, mtype_t mtype, const instrument_t& instr );
    void unsubscribe( source_t source, mtype_t mtype, const instrument_t& instr );

protected:
    // entry point for the subscription thread
    void run();

    // processes queued requests in the subscription thread
    void onSubscriptionEvent();

    // the result of setTarget() or subscribe(), marshalled to the subscription thread
    void onSetTarget  ( source_t source, const HostPortPairList& hosts );
    void onSubscribe  ( source_t source, mtype_t mtype, const instrument_t& instr );
    void onUnsubscribe( source_t source, mtype_t mtype, const instrument_t& instr );

    ThreadQuitHook m_quitHook; // to clean up thread in destructor
    boost::shared_ptr<boost::thread> m_spPublisherSubscriptionThread;

    // queue for moving requests from the main thread to our subscription processing thread
    typedef queue::waitable_queue<boost::function<void()>, queue::ThreadPipeNotifier>
        PublisherSubscriptionQueue;
    PublisherSubscriptionQueue m_publisherQueue; // funnels setTarget()/subscribe() calls to one thread

    SelectDispatcher m_dispatch; // services requests and periodic reconnection attempts

    typedef bbext::hash_map<source_t, PublisherManagerPtr> PublishersTable;
    PublishersTable m_publishersTable; // populated by setTarget()

    std::string m_id; // client identification string

    timeval_t m_nextReconnectCheckTime;
};
BB_DECLARE_SHARED_PTR( PublisherSubscriptionsManager );

} // namespace bb

#endif // BB_CLIENTCORE_PUBLISHERSUBSCRIPTIONSMANAGER_H

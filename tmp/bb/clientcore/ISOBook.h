#ifndef BB_CLIENTCORE_ISOBOOK_H
#define BB_CLIENTCORE_ISOBOOK_H

/* Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved. */


#include <map>
#include <deque>

#include <boost/foreach.hpp>
#include <boost/format.hpp>
#include <boost/regex.hpp>

#include <bb/core/consts/bbl1_consts.h>
#include <bb/core/hash_set.h>
#include <bb/core/MStreamCallback.h>
#include <bb/io/feeds.h>
#include <bb/io/InProcessTransport.h>
#include <bb/clientcore/SourceBooks.h>

namespace bb {

class BbL1QuoteMsg;
class UserMessageMsg;

BB_FWD_DECLARE_SHARED_PTR( LiveMStreamManager );
BB_FWD_DECLARE_SHARED_PTR( NonBlockingTCPSendTransport );
BB_FWD_DECLARE_SHARED_PTR( TCPSocket );
BB_FWD_DECLARE_SHARED_PTR( TCPSocketRecvTransport );
BB_FWD_DECLARE_SHARED_PTR( UserMessageDispatcher );
BB_FWD_DECLARE_SHARED_PTR( EventFD );

class TransportConfig
{
    public:
        TransportConfig ( std::string const & cfg ) : m_cfg ( cfg ) {}

        TransportConfig ( const char * cfg ) : m_cfg ( cfg ) {}

        TransportConfig ( HostPortPair const & hpp ) : m_cfg ( boost::str ( boost::format("tcp://%1%:%2%") % hpp.first % hpp.second ) ) {}

        boost::optional<HostPortPair> getPair() const
        {
            const boost::regex tcp_regex("tcp://(.*)\\:(\\d+)");
            boost::smatch what;
            if ( boost::regex_match ( m_cfg, what, tcp_regex ) )
            {
                const std::string host(what[1]);
                const int port(atoi(what[2].str().c_str()));
                return boost::optional<HostPortPair>(std::make_pair(host, port));
            } else
            {
                return boost::optional<HostPortPair>();
            }
        }

        boost::optional<std::string> getInProcessName() const
        {
            const boost::regex ipt_regex("ipt://(.*)");
            boost::smatch what;
            if ( boost::regex_match ( m_cfg, what, ipt_regex ) )
            {
                return boost::optional<std::string>(what[1].str());
            } else
            {
                return boost::optional<std::string>();
            }
        }

        std::string const & str() const
        {
            return m_cfg;
        }

    private:
        std::string m_cfg;
};

class TransportConfigList : public std::deque < TransportConfig >
{
    public:
        TransportConfigList ( HostPortPairList const & hp )
        {
            BOOST_FOREACH ( HostPortPair const & hpp, hp )
            {
                push_back ( hpp );
            }
        }

        TransportConfigList()
        {}
};

// A ISOBook is just like a BbL1Book, except that it only forwards
// messages about quotes that are protected under the RegNMS order
// protection rule. It also tracks the current mmid associated with
// the MKT_FINRA_ADF quote stream.
class ISOBook : public BbL1Book
{
public:
    // Create a new ISOBook for the given instrument and source. If
    // enableGapDetection is 'true', then the ISOBook will attempt to
    // detect gaps in the feed.
    ISOBook( const ClientContextPtr& context,
             const instrument_t& instr,
             source_t src,
             bool enableGapDetection,
             const TransportConfigList& spinServers = TransportConfigList() );

    virtual ~ISOBook();

    // For the ISOBook, we don't really care about locked/crossed
    // since we are only interested in one side at a time, nor do we
    // care about the sanity of bid/ask bbo's calculated by
    // MultiExchL1Book. For now, this always returns true, unless the
    // book is completely empty.
    virtual bool isOK() const;

    virtual void flushBook();

    /// IEventListener interface
    virtual void onEvent( const Msg& msg );

    // Returns 'true' if this book has authoritative quote state for
    // all markets.
    bool isAuthoritativeForAllMarkets() const;

    /// Returns 'true' iff this book has authoritative quote state for
    /// the given market.
    bool isAuthoritativeForMarket( mktdest_t market ) const;

    /// Return the current market destination for the FINRA ADF
    /// participant on the given side.
    mktdest_t getFinraAdfDestinationMarket( side_t side ) const;

    /// We override this so we can remove any FINRA ADF participant,
    /// after which we delegate to BbL1ISOBook's implementation.
    virtual void removeSideInMkt( side_t side, mktdest_t mkt );

protected:
    const UserMessageDispatcherPtr& getUserMessageDispatcher() const;

    virtual void onSpinServerConnected( const TCPSocketPtr& socket, int error );

    virtual void onInProcessSpinServerConnected ( const InProcessTransportPtr& transport, const int error );

    void startProcessing();

    virtual void onSpinServerEOF();

    virtual bool onSpinServerSendError( const std::exception& xcp );

    virtual bool onSpinServerRecvError( const std::exception& xcp );

    virtual void onSpinServerDisconnected();

    virtual void onDNSLookupSuccess( const sockaddr_ipv4_t& sockaddr );

    virtual void onDNSLookupFailure();

    void syncWithSpinServer();

private:
    typedef bbext::hash_set<mktdest_t> MarketSet;
    typedef std::map< uint32_t, MsgPtr > SpinCache;

    static bool isProtectedQuoteCondition( bbl1::quote_condition_t condition );

    void handleSpinServerMessage( const Msg& msg );

    void handleMessage( const Msg& msg );

    void handleQuoteMessage( const BbL1QuoteMsg& msg );

    void doSpinServerLookup();

    void disconnectFromSpinServer();

    void handleUmsg( const timeval_t& when, symbol_t symbol, const UserMessageMsg& message );

    void onMessageLoss();

    // Basic state
    bool m_authoritative;
    bool m_isoProhibited;
    bool m_isoSuppressed;
    mktdest_t m_finraAdfMarket[2];
    EventSubPtr m_additionalEventsSub;

    // Stream manager and feed stream, for gap detection
    LiveMStreamManagerPtr m_manager;
    Subscription m_messageLossCallbackSub;

    // Spin server related
    EventFDPtr m_spinKickoffEvent;
    TransportConfigList m_spinServers;
    TCPSocketPtr m_spinSocket;
    InProcessTransportPtr m_inProcessSpinSocket;
    IRecvTransportPtr m_spinRecv;
    ISendTransportPtr m_spinSend;
    Subscription m_spinSub;
    MarketSet m_marketSet;
    SpinCache m_spinCache;
    size_t m_spinCompleteMessagesExpected;

    // User messages
    UserMessageDispatcherPtr m_umsgDispatcher;

    //
    class Trampoline : public IMStreamCallback
    {
    public:
        Trampoline( IEventDistListener* listener )
            : m_listener( listener ) {}

        virtual void onMessage(const Msg &msg);

    private:
        IEventDistListener* const m_listener;
    };
    Trampoline m_messageTrampoline;

};
BB_DECLARE_SHARED_PTR( ISOBook );

} // namespace bb

#endif // BB_CLIENTCORE_ISOBOOK_H

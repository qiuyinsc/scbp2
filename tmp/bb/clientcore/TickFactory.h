#ifndef BB_CLIENTCORE_TICKFACTORY_H
#define BB_CLIENTCORE_TICKFACTORY_H

/* Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved. */

#include <boost/function.hpp>
#include <boost/enable_shared_from_this.hpp>

#include <bb/core/hash_map.h>
#include <bb/core/InstrSource.h>
#include <bb/core/Scripting.h>
#include <bb/core/Dispose.h>
#include <bb/clientcore/ITickProviderFactory.h>
#include <bb/clientcore/TickProvider.h>

namespace bb {

// Forward declarations
class ITickProviderFactory;
class SourceTickFactory;
class MultipathTickFactory;
BB_DECLARE_SHARED_PTR( ITickProviderFactory );
BB_DECLARE_SHARED_PTR( SourceTickFactory );
BB_DECLARE_SHARED_PTR( MultipathTickFactory );

// A SourceTick factory generating ITickProviders for particular source feeds (Isld, Bats, Cme, etc.).
class SourceTickFactory
    : public ITickProviderFactory,
      public boost::enable_shared_from_this<SourceTickFactory>
{
    BB_DECLARE_SCRIPTING();
public:
    /// Creates a SourceTickFactory for the given ClientContext.
    static SourceTickFactoryPtr create( ClientContextPtr const& spClientContext );

    /// Constructs a SourceTickFactory for the given ClientContext.
    SourceTickFactory( ClientContextPtr  const&  spClientContext );



    /// Returns an ITickProvider for a given instrument and source.
    /// If the ITickProvider already exists in the factory, then it retrieves that one.
    /// Otherwise, if the create flag is true, it creates the TickProvider and returns it.
    /// Throws if the proper settings do not exist in the factory.
    virtual ITickProviderPtr getTickProvider( const instrument_t& instr, source_t src, bool create );

    /// Returns all ITickProvider already existing in this factory.
    /// Note that the return shared_ptr is const.
    virtual const std::vector<ITickProviderPtr>& getTickProviders() const;

private:
    typedef bbext::hash_map<InstrSource, ITickProviderPtr>  InstrSource2TickMap;
//    bb::IntrusiveWeakPtr<ClientContext> m_spContext;     // <-- Lance's comment in Dispose.h says we do this
//    because shared/weak_ptr doesn't play well with Lua, but this looks dangerous.

    ClientContextWeakPtr                m_wpContext;
    InstrSource2TickMap                 m_instrsrc2TickMap;
    std::vector<ITickProviderPtr>       m_vTicks;
    boost::function<mktdest_t ( instrument_t )> m_primary_mkt;
    MultipathTickFactoryPtr m_spMultipathTickFactory;
};

BB_DECLARE_SHARED_PTR( SourceTickFactory );




} // namespace bb

#endif // BB_CLIENTCORE_TICKFACTORY_H

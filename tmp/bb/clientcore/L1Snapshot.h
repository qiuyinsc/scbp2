#ifndef BB_CLIENTCORE_L1SNAPSHOT_H
#define BB_CLIENTCORE_L1SNAPSHOT_H

/* Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved. */


#include <map>

#include <bb/core/mktdest.h>
#include <bb/core/smart_ptr.h>

#include <bb/clientcore/TickProvider.h>

namespace bb {

BB_FWD_DECLARE_SHARED_PTR(Msg);
struct TickSummary;

/// Map of market centers to last message seen
typedef std::map<mktdest_t, MsgPtr> L1MsgCache;

/// Map of market centers to tick summaries so we can do things like
/// per-exchange volume, open, close, etc.
typedef std::map<mktdest_t, TickSummary> L1Ticks;

/// A generic tick summary class that aggregates information about a series of
/// ticks.  Holds data like open, open time, high, low, close, different kinds
/// of volumes, etc.
// TODO: I know this is going to end up being very similar to
// MultiTickProvider::Ticker, so I should be able to merge the two into some
// sort of a unified class, though the logic present here is much simpler than
// what MTP::Ticker needs.
struct TickSummary
{
    TickSummary();

    void updateTickSummary(const TradeTick& trade_tick);

    uint32_t m_total_volume;
    uint32_t m_volume_at_current_px;
    uint32_t m_num_at_current_px;

    double m_open_px;
    double m_low_px;
    double m_high_px;
    timeval_t m_open_time;

    /// The most recent message's exchange timestamp (if available)
    timeval_t m_last_exch_time;

    /// The most recent message's sent timestamp (our timestamp)
    timeval_t m_last_msg_sent_time;

    /// The most recent trade tick information
    TradeTick m_last_trade_tick;
};

class L1Snapshot
{
public:
    /// Constructor.
    L1Snapshot();

    /// Updates our snapshot by overwriting the message.  Only updates if the
    /// incoming mtype is the same as the cached mtype, or else we can't
    /// overwrite the old messages.
    void updateSnapshot(mktdest_t mkt, const Msg& msg);

    /// Returns the number of exchanges this cache holds.
    inline size_t numMarkets()
    {
        return m_message_cache.size();
    }

    /// Returns the current snapshot of quotes it has cached.
    L1MsgCache& getCurrentSnapshot();

    /// Flush our cache.
    void flushSnapshot();

private:
    /// Keeps track of the last message seen for each mktdest.  When a trade
    /// comes in, flushes this cache and outputs to a logfile.
    L1MsgCache m_message_cache;
};

class L1Tickstats
{
public:
    /// Constructor
    L1Tickstats();

    /// Update our ticks with the latest trade information
    void updateTicks(mktdest_t mkt, const TradeTick& msg);

private:

    /// Aggregated-tick summaries for each market center
    L1Ticks m_tick_summaries;
};

}

#endif // BB_CLIENTCORE_L1SNAPSHOT_H

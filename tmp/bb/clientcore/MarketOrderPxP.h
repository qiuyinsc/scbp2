#ifndef BB_CLIENTCORE_MARKETORDERPXP_H
#define BB_CLIENTCORE_MARKETORDERPXP_H

/* Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved. */


#include <bb/clientcore/PriceProvider.h>
#include <bb/clientcore/PriceProviderSpec.h>
#include <bb/clientcore/IBook.h>

namespace bb {

/// MarketOrderPxP represents the price at which we would get filled if
/// we sent a market order for a given size (ignoring invisible orders,
/// RegNMS & routing concerns).
///
/// Potential gotcha: a MarketOrderPxP for side BID means it'll get its
/// price by looking at the BID side of the book ... so it'd be simulating
/// a market order to sell!
///
/// Performance notes:
/// This might be slow for large order sizes, since it needs to walk the
/// whole book. Fixing this might mean enhancing/replacing IBook.
/// If you have multiple MarketOrderPxPs on the same book with different
/// sizes, they won't share work. Talk to BMB about how to fix this.
class MarketOrderPxP
    : public PriceProviderImpl
    , protected IBookListener
{
public:
    // Controls which price is output by the IPriceProvider impl.
    enum Mode {
        BEST_PRICE,  // returns getBestPrice
        VWAP_PRICE,  // returns getVWAPPrice
        LIMIT_PRICE, // returns getLimitPrice
    };

    MarketOrderPxP( IBookCPtr spBook, side_t side, int size, Mode mode=VWAP_PRICE );
    virtual ~MarketOrderPxP();

    virtual instrument_t getInstrument() const { return m_book->getInstrument();   }
    virtual timeval_t getLastChangeTime() const { return m_book->getLastChangeTime(); }

    virtual double getRefPrice( bool* pSuccess = NULL ) const;
    virtual bool isPriceOK() const;

    /// Returns the best price we would get filled at.
    double getBestPrice() const { checkUpdate(); return m_bestValue; }
    /// Returns the volume weighted average price we would get filled at.
    double getVWAPPrice() const { checkUpdate(); return m_vwapValue; }
    /// Returns the worst price we would get filled at.
    double getLimitPrice() const { checkUpdate(); return m_limitValue; }

    /// Resets this PxP to output values for a different size.
    /// Don't do this with PxPs you got from the PxPBuilder, or everyone sharing that PxP will be affected!
    /// Calls listeners, so watch out for reentrancy.
    void setSize(int sz);

protected:
    inline void checkUpdate() const { if(m_dirty) doUpdate(); }
    void doUpdate() const;
    // IBookListener impl
    virtual void onBookChanged( const IBook* pBook, const Msg* pMsg, int32_t bidLevelChanged, int32_t askLevelChanged );
    virtual void onBookFlushed( const IBook* pBook, const Msg* pMsg );

    IBookCPtr m_book;
    side_t m_side;
    int m_size;
    Mode m_mode;
    mutable bool m_dirty, m_ok;
    mutable double m_bestValue, m_vwapValue, m_limitValue;

    // the last time we calculated it, how many levels it took to fill the whole order.
    // for optimization in onBookFlushed.
    mutable int32_t m_lastMaxLevel;
};
BB_DECLARE_SHARED_PTR(MarketOrderPxP);

/// PxPSpec corresponding to MarketOrderPxP
class MarketOrderPxPSpec : public IPxProviderSpec
{
public:
    BB_DECLARE_SCRIPTING();

    MarketOrderPxPSpec() : m_side(SIDE_INVALID), m_size(0), m_mode(MarketOrderPxP::VWAP_PRICE) {}
    MarketOrderPxPSpec( const MarketOrderPxPSpec& a, const boost::optional<InstrSubst> &instrSubst );

    virtual instrument_t getInstrument() const { return m_bookSpec->getInstrument(); }
    virtual IPriceProviderPtr build(PriceProviderBuilder *builder) const;
    virtual MarketOrderPxPSpec *clone(const boost::optional<InstrSubst> &instrSubst = boost::none) const;
    virtual void hashCombine(size_t &result) const;
    virtual bool compare(const IPxProviderSpec *other) const;
    virtual void print(std::ostream &o, const LuaPrintSettings &pset) const;
    virtual void checkValid() const;
    virtual void getDataRequirements(IDataRequirements *rqs) const;

    IBookSpecPtr         m_bookSpec;
    side_t               m_side;
    int                  m_size;
    MarketOrderPxP::Mode m_mode;
};
BB_DECLARE_SHARED_PTR( MarketOrderPxPSpec );

} // namespace bb

#endif // BB_CLIENTCORE_MARKETORDERPXP_H

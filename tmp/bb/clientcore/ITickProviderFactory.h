#ifndef BB_CLIENTCORE_ITICKPROVIDERFACTORY_H
#define BB_CLIENTCORE_ITICKPROVIDERFACTORY_H

/* Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved. */

#include <vector>
#include <bb/core/source.h>
#include <bb/core/Scripting.h>

namespace bb {

class instrument_t;
BB_FWD_DECLARE_SHARED_PTR( ITickProvider );

// An abstract factory for generating ITickProviders.
class ITickProviderFactory
{
    BB_DECLARE_SCRIPTING();
public:
    virtual ~ITickProviderFactory()  {}

    /// Returns an ITickProvider for a given instrument and source.
    /// If the ITickProvider already exists in the factory, then it retrieves that one.
    /// Otherwise, if the create flag is true, it creates the TickProvider and returns it.
    /// Throws if the proper settings do not exist in the factory.
    virtual ITickProviderPtr getTickProvider( const instrument_t& instr, source_t src, bool create = false ) = 0;

    /// Returns all ITickProvider already existing in this factory.
    /// Note that the return shared_ptr is const.
    virtual const std::vector<ITickProviderPtr>& getTickProviders() const = 0;
};

BB_DECLARE_SHARED_PTR( ITickProviderFactory );

} // namespace bb

#endif // BB_CLIENTCORE_ITICKPROVIDERFACTORY_H

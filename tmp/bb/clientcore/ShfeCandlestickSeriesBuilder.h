#ifndef BB_CLIENTCORE_SHFECANDLESTICKSERIESBUILDER_H
#define BB_CLIENTCORE_SHFECANDLESTICKSERIESBUILDER_H

/* Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved. */


#include <bb/core/instrument.h>
#include <bb/core/Subscription.h>
#include <bb/clientcore/CandlestickSeriesBuilder.h>

namespace bb {

BB_FWD_DECLARE_SHARED_PTR( MsgHandler );
BB_FWD_DECLARE_SHARED_PTR( ClientContext );
BB_FWD_DECLARE_SHARED_PTR( ShfeTickProvider );
BB_FWD_DECLARE_SCOPED_PTR( ShfeQdMsg );

class ShfeCandlestickData
{
public:
    ShfeCandlestickData() : m_openInterest( 0 ), m_turnOver(0.0) {}
    //ShfeCandlestickData() : m_turnOver( 0.0 ) {}

    void setOpenInterest( int32_t v ) { m_openInterest = v; }
    void setTurnOver( double v ) { m_turnOver = v; }
    
    int32_t getOpenInterest() const { return m_openInterest; }
	double getTurnOver() const { return m_turnOver;}
	
protected:
    int32_t m_openInterest;
    double m_turnOver;
    
};

class ShfeCandlestickSeriesBuilder : public CandlestickSeriesBuilder
{
public:
    ShfeCandlestickSeriesBuilder( ClientContextPtr cm
        , double period
        , ICandlestickSeriesPtr series
        , ShfeTickProviderPtr tp );

    virtual ~ShfeCandlestickSeriesBuilder();
    


protected:
    virtual void addEntry( const Candlestick& entry );
    void onShfeQdMsg( const ShfeQdMsg& msg );
    
protected:
    bb::instrument_t m_instr;
    MsgHandlerPtr m_subShfeQdMsg;
    ShfeQdMsgScopedPtr m_spLastShfeQdMsg;
    
    
};

} // namespace bb

#endif // BB_CLIENTCORE_SHFECANDLESTICKSERIESBUILDER_H

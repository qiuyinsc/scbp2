#ifndef BB_CLIENTCORE_HISTCANDLESTICKFILESTORE_H
#define BB_CLIENTCORE_HISTCANDLESTICKFILESTORE_H

/* Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved. */


#include <boost/filesystem.hpp>
#include <boost/optional.hpp>
#include <bb/core/hash_map.h>
#include <bb/core/source.h>
#include <bb/core/timeval.h>
#include <bb/clientcore/IHistCandlestickStore.h>

namespace bb {

class HistCandlestickFileStore : public IHistCandlestickStore
{
public:
    HistCandlestickFileStore( const std::string& store_path );
    HistCandlestickFileStore( const std::string& store_path, const bb::timeval_t& start_time );

    boost::shared_ptr<ICandlestickSeries> getInstrument( const bb::instrument_t& instr,
                                                         const bb::source_t& alt_src = bb::source_t() );

protected:
    boost::filesystem::path m_storePath;
    typedef bbext::hash_map< bb::instrument_t, boost::shared_ptr<ICandlestickSeries> > CandlestickSeriesTable;
    CandlestickSeriesTable m_candlestickSeriesTable;
    boost::optional<bb::timeval_t> m_startTime;
};

BB_DECLARE_SHARED_PTR( HistCandlestickFileStore );

} // namespace bb

#endif // BB_CLIENTCORE_HISTCANDLESTICKFILESTORE_H

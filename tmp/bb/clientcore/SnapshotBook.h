#ifndef BB_CLIENTCORE_SNAPSHOTBOOK_H
#define BB_CLIENTCORE_SNAPSHOTBOOK_H

/* Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved. */


#include <boost/optional.hpp>
#include <bb/core/timeval.h>
#include <bb/core/smart_ptr.h>
#include <bb/clientcore/L1Book.h>
#include <bb/clientcore/SourceBooks.h>

namespace bb {

class ClientContext;
class BookSnapshotMsg;

BB_FWD_DECLARE_SHARED_PTR( MsgHandler );
BB_FWD_DECLARE_SHARED_PTR( SourceMonitor );
BB_FWD_DECLARE_SHARED_PTR( ClientContext );

class SnapshotBook : public L1Book
{
public:
    SnapshotBook( const ClientContextPtr& context, const instrument_t& instr, source_t src
        , SourceMonitorPtr sm, const std::string& desc, int _vbose=0);
    SnapshotBook( const ClientContextPtr& context, const instrument_t& instr, source_t src
        , const std::string& desc, int _vbose = 0 );
    virtual ~SnapshotBook() {}

    timeval_t getExchangeTime() const
    { return m_exchangeTv; }

    virtual bool isOK() const;
    virtual double getMidPrice() const;

protected:
    void onBookSnapshotMsg( const BookSnapshotMsg& msg );

//    void handleCross( side_t side, double price );

protected:
    MsgHandlerPtr m_subBookSnapshotMsg;
    timeval_t m_exchangeTv;
};

BB_DECLARE_SHARED_PTR( SnapshotBook );

/// BookSpec corresponding to an CmeLevelSnapshot book
class SnapshotBookSpec : public SourceBookSpec
{
public:
    BB_DECLARE_SCRIPTING();

    SnapshotBookSpec() : SourceBookSpec() {}
    SnapshotBookSpec(const bb::SnapshotBookSpec&, const boost::optional<bb::InstrSubst>&);
    SnapshotBookSpec(const instrument_t &instr, source_t src)
        : SourceBookSpec(instr, src)
    {}

    virtual IBookPtr build(BookBuilder *builder) const;
    //virtual void checkValid() const;
    //virtual void hashCombine(size_t &result) const;
    //virtual bool compare(const IBookSpec *other) const;
    virtual void print(std::ostream &o, const LuaPrintSettings &ps) const;
    virtual SnapshotBookSpec *clone(const boost::optional<InstrSubst> &instrSubst = boost::none) const;
    //virtual void getDataRequirements(IDataRequirements *rqs) const;
};
BB_DECLARE_SHARED_PTR( SnapshotBookSpec );

} // namespace bb

#endif // BB_CLIENTCORE_SNAPSHOTBOOK_H

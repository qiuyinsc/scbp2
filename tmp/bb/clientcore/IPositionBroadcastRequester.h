#ifndef BB_CLIENTCORE_IPOSITIONBROADCASTREQUESTER_H
#define BB_CLIENTCORE_IPOSITIONBROADCASTREQUESTER_H

/* Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved. */


#include <bb/core/acct.h>
#include <bb/core/instrument.h>
#include <bb/core/smart_ptr.h>

namespace bb {

/// Interface class for something that can ask a TD to begin regular MarginInstrumentInfoMsg broadcasts for
/// a given acct/sym. The trade daemon clients can do this (ITradeDemonClient inherits from this).
class IPositionBroadcastRequester
{
public:
    virtual ~IPositionBroadcastRequester() {}

    // Sends a message to the TD to begin MarginInstrumentInfoMsg broadcasts for the given account/instr.
    virtual bool setup_position_broadcasts(bb::acct_t acct, const bb::instrument_t& instr) = 0;

    // Sends a message to the TD to begin/end MarginInstrumentInfoMsg unicasts for the given account/instr.
    virtual bool margin_subscribe  (bb::acct_t acct, const bb::instrument_t& instr) = 0;
    virtual bool margin_unsubscribe(bb::acct_t acct, const bb::instrument_t& instr) = 0;
};
BB_DECLARE_SHARED_PTR( IPositionBroadcastRequester );

} // namespace bb

#endif // BB_CLIENTCORE_IPOSITIONBROADCASTREQUESTER_H

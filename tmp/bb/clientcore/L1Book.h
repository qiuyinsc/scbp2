#ifndef BB_CLIENTCORE_L1BOOK_H
#define BB_CLIENTCORE_L1BOOK_H

/* Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved. */


#include <bb/clientcore/Book.h>

namespace bb {

BB_FWD_DECLARE_SHARED_PTR( ClientContext );

class L1Book : public Book
{
public:
    virtual PriceSize getSide( side_t side ) const
    {
        return m_topOfBook[side].getPriceSize();
    }

    /// Returns true if the book is in a "valid" state.
    /// The meaning of this is book-dependent, however clients typically use it
    /// to test whether they should "trust" the book's contents.
    virtual bool isOK() const
    {
        if( m_topOfBook[BID].isOK() && m_topOfBook[ASK].isOK() )
        {
            book_rdy_fg = true;
            return Book::isOK();
        }
        else
            return false;
    }

protected:
    // BookImpl must be derived from
    L1Book( const instrument_t& instr, source_t src, const ClockMonitorPtr& cc, const std::string& desc, int _vbose=0 );
    L1Book( const instrument_t& instr, source_t src, const ClientContextPtr& cc, const std::string& desc, int _vbose=0 );

    /// Returns the mid-price of a book, or 0.0 if the book is not valid (e.g. is one-sided).
    virtual double getMidPrice() const
    {
        if( !m_topOfBook[BID].isOK() )
            return 0;
        if( !m_topOfBook[ASK].isOK() )
            return 0;
        return (m_topOfBook[BID].getPrice() + m_topOfBook[ASK].getPrice()) / 2.0;
    }

    /// Returns the PriceSize at a specified depth and side.
    /// Returns an empty PriceSize if nothing exists at that depth or side.
    virtual PriceSize getNthSide( size_t depth, side_t side ) const
    {
        if( depth == 0 && m_topOfBook[side].isOK() )
            return m_topOfBook[side].getPriceSize();
        return PriceSize();
    }

    class BookLevelCIter : public IBookLevelCIter
    {
    public:
        BookLevelCIter( const BookLevel* bl )
            : m_next( (bl->isOK()) ? bl : 0 ) {}
        virtual bool hasNext() const { return m_next != 0; }
        virtual BookLevelCPtr next()
        {
            const BookLevel* rval = m_next;
            if( rval )
            {
                m_next = 0;
                return makeNoopSharedPtr( rval );
            }
            else return BookLevelCPtr();
        }
    protected:
        const BookLevel* m_next;
    };

    /// Returns an object for iterating over BookLevels of a particular side.
    /// Postcondition: The returned iterator will iterate getNumLevels objects.
    /// This abstracts the underlying containers used by a book; book implementations
    /// will implement their own iterator and pass them back.
    virtual IBookLevelCIterPtr getBookLevelIter( side_t side ) const
    {
        return IBookLevelCIterPtr( new BookLevelCIter( &m_topOfBook[side] ) );
    }

    /// Returns the number of levels of a particular side of this book.
    /// Note that empty levels above the "bottom" level are counted.
    virtual size_t getNumLevels( side_t side ) const { return m_topOfBook[side].isOK() ? 1 : 0; }

    /// Flushes the book.
    virtual void flushBook()
    {
        setTopLevel( BID, 0, 0 );
        setTopLevel( ASK, 0, 0 );
    }

    // a price of 0.0 will "invalidate" the level.
    bool setTopLevel( side_t side, double price, size_t size )
    {
        BookLevel& bl = m_topOfBook[side];
        if( !bl.isOK() )
        {
            if( size > 0 )
            {
                bl.setPriceSize( price, size );
                notifyBookLevelAdded( &bl );
                return true;
            }
        }
        else
        {
            if( (size == 0) || EQZ( price ) )
            {
                notifyBookLevelDropped( &bl );
                bl.setPriceSize( std::numeric_limits<double>::signaling_NaN(), 0 );
                return true;
            }

            // assume non-zero price and size
            if( !EQ( bl.getPrice(), price ) )
            {
                // drop and add new book level
                notifyBookLevelDropped( &bl );
                bl.setPriceSize( price, size );
                notifyBookLevelAdded( &bl );
                return true;
            }

            if( !EQ( bl.getSize(), size ) )
            {
                // modify level size
                bl.setPriceSize( price, size );
                notifyBookLevelModified( &bl );
                return true;
            }
        }
        return false;
    }

protected:
    BookLevel m_topOfBook[2];
};

BB_DECLARE_SHARED_PTR( L1Book );

} // namespace bb

#endif // BB_CLIENTCORE_L1BOOK_H

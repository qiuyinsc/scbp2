#ifndef BB_CLIENTCORE_FILTEREDBOOKFACTORY_H
#define BB_CLIENTCORE_FILTEREDBOOKFACTORY_H

/* Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved. */


#include <bb/core/timeval.h>
#include <bb/core/instrument.h>
#include <bb/core/smart_ptr.h>
#include <bb/clientcore/FilteredBook.h>
namespace bb {
BB_FWD_DECLARE_SHARED_PTR(DayInfoStats);
BB_FWD_DECLARE_SHARED_PTR(DayInfoStatsFactory);

class IFilteredBookFactory
{
public:
    virtual ~IFilteredBookFactory(){}
    virtual IBookPtr buildBook(IBookPtr const&)const=0;
    virtual bb::date_t getDate()const=0;
};
BB_DECLARE_SHARED_PTR( IFilteredBookFactory );


//reject anything where the midprice is not between high and low
class ObviousOutlierFilter
{
public:
    ObviousOutlierFilter(float h,float l);
     bool operator()(IBookPtr const&p)const;
     friend std::ostream& operator<<(std::ostream&,ObviousOutlierFilter const&);
private:
    float m_high;
    float m_low;
};
typedef FilteredBook<ObviousOutlierFilter> ObviousOutlierFilteredBook;

BB_DECLARE_SHARED_PTR( ObviousOutlierFilteredBook );

///This creates filter books that rejects prices that are outside the specified stddev range
///from the close, calculated N trade days back and outside +/- stddevs
class ObviousOutlierFilteredBookFactory:public IFilteredBookFactory
{
public:
    ObviousOutlierFilteredBookFactory(DayInfoStatsFactoryCPtr const&f,unsigned stddevs_high,unsigned stddevs_low);
    IBookPtr buildBook(IBookPtr const&)const;
    bb::date_t getDate()const;

private:
    unsigned m_stddevs_high;
    unsigned m_stddevs_low;
    DayInfoStatsFactoryCPtr m_statsptr;
};
BB_DECLARE_SHARED_PTR( ObviousOutlierFilteredBookFactory );
} // namespace bb

#endif // BB_CLIENTCORE_FILTEREDBOOKFACTORY_H

#ifndef BB_CLIENTCORE_PERFMONEVENTDIST_H
#define BB_CLIENTCORE_PERFMONEVENTDIST_H

/* Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved. */


#include <bb/core/timeval.h>
#include <bb/io/MessagePerfMon.h>
#include <bb/clientcore/EventDist.h>
#include <bb/clientcore/ClockMonitor.h>
#include <bb/clientcore/ClientContext.h>

namespace bb {
namespace message_perf {

BB_FWD_DECLARE_SHARED_PTR(EventDistMonitor);
/// This adapts message_perf::Monitor to hook into the EventDistributor.
/// It will create an XML file on exit with stats on the message handling times.
class EventDistMonitor : public Monitor
{
public:
    static EventDistMonitorPtr create( std::string outFile, ClientContextPtr cc, bool runLive )
        { return create(outFile, cc->getEventDistributor(), cc->getClockMonitor(), runLive); }
    static EventDistMonitorPtr create( std::string outFile, EventDistributorPtr eventDist,
            ClockMonitorPtr cm, bool runLive )
        { return EventDistMonitorPtr(new EventDistMonitor(outFile, eventDist, cm, runLive)); }

    EventDistMonitor( std::string outFile, EventDistributorPtr eventDist, ClockMonitorPtr cm, bool runLive )
        : Monitor(outFile), m_eventDist(eventDist), m_clockMonitor(cm), m_cmWakeupsAtStart(0)
    {
        if(runLive)
            m_machineTimestamp = timeval_t();
        if(!eventDist)
            BB_THROW_ERROR( "MessagePerfMon: bad EventDist" );
        EventDistributor::MsgWorkerPtr highestW(new HighestEventWorker(this)),
                                       lowestW(new LowestEventWorker(this));
        eventDist->hookEventsAll( m_spEventSubHighest, highestW );
        eventDist->hookEventsAll( m_spEventSubLowest, lowestW );
    }
    ~EventDistMonitor() { shutdown(m_eventDist->getTotalMessageCount()); }

protected:
    void onEventHighest( const Msg& msg )
    {
        if(m_machineTimestamp)
            m_machineTimestamp->set_current(CLOCK_REALTIME);
        m_processingStartTime.set_current(CLOCK_MONOTONIC);
        m_cmWakeupsAtStart = m_clockMonitor->getNumEventsFiringWakeups();
    }
    void onEventLowest( const Msg& msg )
    {
        bool cmFired = m_clockMonitor->getNumEventsFiringWakeups() != m_cmWakeupsAtStart;
        timeval_t now;
        now.set_current(CLOCK_MONOTONIC);
        ptime_duration_t diff = timeval_diff(now, m_processingStartTime);
        addEvent(msg, m_machineTimestamp, diff, cmFired);
    }

    struct HighestEventWorker : public IEventDistListener, public EventDistributor::MsgWorker
    {
        HighestEventWorker(EventDistMonitor *m) : MsgWorker(Priority::HIGHEST, this), m_mon(m) {}
        virtual void onEvent(const Msg &msg) { m_mon->onEventHighest(msg); }
        EventDistMonitor *m_mon;
    };
    struct LowestEventWorker : public IEventDistListener, public EventDistributor::MsgWorker
    {
        LowestEventWorker(EventDistMonitor *m) : MsgWorker(Priority::LOWEST, this), m_mon(m) {}
        virtual void onEvent(const Msg &msg) { m_mon->onEventLowest(msg); }
        EventDistMonitor *m_mon;
    };

    boost::optional<timeval_t> m_machineTimestamp;
    timeval_t m_processingStartTime;
    EventDistributorPtr m_eventDist;
    ClockMonitorPtr m_clockMonitor;
    int64_t m_cmWakeupsAtStart;
    EventSubPtr m_spEventSubLowest, m_spEventSubHighest;
};
BB_DECLARE_SHARED_PTR(EventDistMonitor);

} // namespace message_perf
} // namespace bb

#endif // BB_CLIENTCORE_PERFMONEVENTDIST_H

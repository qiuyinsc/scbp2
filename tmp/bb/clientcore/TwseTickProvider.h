#ifndef BB_CLIENTCORE_TWSETICKPROVIDER_H
#define BB_CLIENTCORE_TWSETICKPROVIDER_H

/* Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved. */

#include <bb/core/messages.h>
#include <bb/clientcore/TickProvider.h>

namespace bb {

BB_FWD_DECLARE_SHARED_PTR( ClockMonitor );
BB_FWD_DECLARE_SHARED_PTR( MsgHandler );

class TwseTickProvider
    : public TickProviderImpl
{
public:
    /// Constructs an TwseTickProvider for the given instrument,
    /// receiving events from the EventDistributor.
    /// Instrument must be a stock.
    TwseTickProvider( ClientContext& context, const instrument_t& instr, source_t source,
                      const std::string& desc );

    /// Destructor.
    virtual ~TwseTickProvider() {}

    /// Returns true if the last obtained tick is OK.
    /// Returns false if there is no last tick.
    virtual bool isLastTickOK() const { return m_bTickReceived; }

private:
    // Event handlers
    void onBookSnapshot( const bb::TwseBookSnapshotMsg& );
    void onIndex( const bb::TwseIndexMsg &);
    void onStartOfDay( const timeval_t& ctv, const timeval_t& wtv );

    void scheduleNextStartOfDay( const timeval_t& now );

private:
    ClockMonitorPtr m_spClockMonitor;
    bool m_bInitialized;
    bool m_bTickReceived;
    int32_t m_lastSerial;
    MsgHandlerPtr m_subTwseBookSnapshotMsg;
    MsgHandlerPtr m_subTwseIndexMsg;
    Subscription m_subWakeup;
};

BB_DECLARE_SHARED_PTR( TwseTickProvider );

}

#endif // BB_CLIENTCORE_TWSETICKPROVIDER_H

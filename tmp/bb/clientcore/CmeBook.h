#ifndef BB_CLIENTCORE_CMEBOOK_H
#define BB_CLIENTCORE_CMEBOOK_H

/* Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved. */

#include <bb/core/compat.h>
#include <bb/core/consts/cme_consts.h>

#include <bb/clientcore/ClockMonitor.h>
#include <bb/clientcore/Book.h>
#include <bb/clientcore/SourceBooks.h>

namespace cme {
BB_FWD_DECLARE_SCOPED_PTR( BookServerClient );
BB_FWD_DECLARE_SHARED_PTR( CmeSnapshotRecoveryManager );
}

namespace bb {

BB_FWD_DECLARE_SHARED_PTR( ClientContext );
BB_FWD_DECLARE_SHARED_PTR( SourceMonitor );
class CmeFastDepthMsg;
class CmeRecoveryBookSnapshotMsg;
class CmeRecoveryBookBaseMsg;
/*****************************************************************************/
//
// CME Book
//
// CME books have three "views".  There is the regular "outright" book, the
// "implied" book (derived from spreads, etc.), and the merged book (what the
// CME actually sends as its book.  The default is the merged book.  You can
// change the view with CmeBook::setView().
//
/*****************************************************************************/

class CmeBook
    : public BookImpl
    , public IEventDistListener
    , private SourceMonitorListener
{
public:
    // Tack on an implied flag so we can break apart the merged book if our view
    // says we should.
    class Level
        : public BookLevel
    {
    public:
        Level()
            : BookLevel()
        {

        }

        Level( source_t _src, side_t _side, double _px, int32_t _sz, int32_t numOrders = -1, bool p_is_implied = false  )
            : BookLevel( _src, _side, _px, _sz, numOrders ), is_implied( p_is_implied )
        {

        }

        Level( source_t _src, side_t _side, double _px, bool p_is_implied = false )
            : BookLevel( _src, _side, _px ), is_implied( p_is_implied )
        {

        }

        bool is_implied;
    };

    BB_DECLARE_SHARED_PTR( Level );

public:
    typedef boost::function<bool(bb::CmeBook&, bb::timeval_t, uint32_t)> AllowRecoveryCallback;
    typedef boost::function<void(bb::CmeBook&)> RecoveryCompleteCallback;

    enum EView { MERGED = 0, OUTRIGHT = 1, IMPLIED = 2 };

    enum ENotify { NOTIFY, NO_NOTIFY};

    enum ENotificationFlag
    {
        NF_SKIP_CROSSED_BOOK = 1
    };

    /// Default view is merged.
    CmeBook( const instrument_t& instr, ClientContextPtr spCC, source_t src,
             const std::string& strDesc, SourceMonitorPtr spSMon, EView view = MERGED, size_t holeCheckDepth = 2, uint32_t notificationFlags = 0);
    virtual ~CmeBook();

    /// Returns the current book view (either MERGED, OUTRIGHT, or IMPLIED).
    EView getView() const { return m_view; }
    /// Sets the current book view (either MERGED, OUTRIGHT, or IMPLIED).
    void setView( EView view );

    /// Returns true if the implied book is OK.
    /// Note that a completely empty implied book is OK.
    bool isImpliedOK() const;

    //
    // IBook interface
    //

    /// Returns true if the book is in a "valid" state.
    /// The meaning of this is book-dependent, however clients typically use it
    /// to test whether they should "trust" the book's contents.
    /// For example, getMidPrice should return a valid price if isOK is true.
    virtual bool isOK() const;

    /// Flushes the book.
    virtual void flushBook();

    /// Returns the mid-price of a book, or 0.0 if the book is not valid or empty.
    /// If one side of the book is empty, CmeBook's getMidPrice will
    /// return the price of the existing side.
    virtual double getMidPrice() const;

    /// Returns the PriceSize at a specified depth and side.
    /// Returns an empty PriceSize if nothing exists at that depth or side.
    virtual PriceSize getNthSide( size_t depth, side_t side ) const;

    /// Returns an object for iterating over BookLevels of a particular side.
    /// Postcondition: The returned iterator will iterate getNumLevels objects.
    /// This abstracts the underlying containers used by a book.
    virtual IBookLevelCIterPtr getBookLevelIter( side_t side ) const;

    /// Returns the number of levels of a particular side of this book.
    virtual size_t getNumLevels( side_t side ) const;

    size_t getExpectedLevels( side_t side, bool bImplied ) const;

    void setHoleCheckDepth( size_t depth );
    size_t getHoleCheckDepth() const;

    boost::optional<uint32_t> getLastProcessedInstrSeqnum() const;

    void subscribeToMarketData();
    void unsubscribeToMarketData();

    void setAllowRecoveryCallback( AllowRecoveryCallback cb);

    void setRecoveryCompleteCallback( RecoveryCompleteCallback cb);

    /// EventListener interface
    virtual void onEvent( const Msg& msg );

    void onEvent( const CmeRecoveryBookSnapshotMsg& msg );

    void onRecoveryEvent( const CmeRecoveryBookBaseMsg& msg );
protected:

    template<typename CmeDepthMsgT>
    void onEvent( const CmeDepthMsgT& msg, EView view );

    template<typename CmeFastDepthMsgT>
    void onEvent( const CmeFastDepthMsgT& msg, ENotify notify );

    /// Finds the specified order in the book and updates its value.
    /// If size <= 0, then it is removed from the book.
    /// If it is not already in the book, then it is added.
    /// If seqnum < 0, then this is from an aggregate update; a seqnum is automatically generated.
    /// Returns true if the book is modified, false otherwise.
    bool applyOrderToBook( uint32_t level, side_t side, const timeval_t& tv_msg, const timeval_t& tv_exch,
                           int32_t sz, double px, int32_t numOrders, bool implied );

    /// Compares the current viewed book to m_prevLevels, generating the appropriate
    /// BookLevel notifications. If only does this if there are visitors.
    /// Updates m_prevLevels to be the same as the current view.
    void dispatchLevelNotifications();

    // Children should call this when they want to notify their listeners that they have changed.
    virtual void notifyBookChanged_View( EView view, const Msg* pMsg, int32_t bidLevelChanged,
                                         int32_t askLevelChanged ) const;

    void updateMergedView( side_t side );

private:
    static const int NO_SEQNUM = -1;
    static const size_t NUM_LEVELS;
    static const size_t NUM_IMPLIED_LEVELS;

    typedef std::vector<LevelPtr> BookLevelVec;
    class LevelChange;

    // initialize recovery process
    void initRecovery();
    void startRecovery( uint32_t msg_dropped );
    void onRecoveryComplete();

    /// SourceMonitorListener interface
    virtual void onSMonStatusChange( source_t src, smon_status_t status, smon_status_t old_status,
                                     const ptime_duration_t& avg_lag_tv, const ptime_duration_t& last_interval_tv );


    /// Flushes one side of the book.
    /// Does NOT call listeners.
    void flushSide( EView view, side_t side );

    /// Generates the LevelChanges for the given side.  Helper for generateNotifications.
    void generateChangesForSide( side_t side, std::vector<LevelChange>& vChanges ) const;

    /// Checks for holes in a side of the passed BookLevelVec (book half)
    /// Returns true if there are holes, false otherwise.
    /// If the whole half is invalid, false will be returned.
    bool hasHoles( const BookLevelVec& blvec ) const;
    inline bool isCrossed() const;

    // Children should call this when they want to notify their listeners that they have flushed.
    virtual void notifyBookFlushed_View( EView view, const Msg* pMsg ) const;

    bool fixBooks( side_t trusted_side, double trusted_px, bool implied );

protected:
    ClientContextWeakPtr m_wpCC;
    SourceMonitorPtr m_spSMon;
    uint32_t m_verbose;

    EView m_view;
    BookLevelVec* m_viewedLevels[2];           // BID = 0, ASK = 1
    BookLevelVec m_prevViewedLevels[2];        // BID = 0, ASK = 1

    uint32_t m_next_bo_seqnum;

    mutable bool m_ok, m_ok_dirty;
    bool m_source_ok;
    uint32_t m_notificationFlags;

    size_t m_holeCheckDepth;

    // Book levels are kept in a std::vector.  Most CME books are only 5 levels
    // deep.  An empty level has a null BookLevelPtr.  Level 0 is top of book.

    /// The merged levels is the book that CME uses as their canonical
    /// representation of the market.  It will include both implied and non-
    /// implied quotes.
    BookLevelVec m_mergedLevels[2];

    /// Only contains outright quotes.  Expected to have holes.  Each level is
    /// mutually exclusive with the corresponding implied level.
    BookLevelVec m_outrightLevels[2];

    /// Only contains implied quotes.  Expected to have holes.  Each level is
    /// mutually exclusive with the corresponding outright level.
    BookLevelVec m_impliedLevels[2];

    EventSubPtr m_eventSub;

    uint32_t                                  m_lastInstrSeqnum;
    ::cme::CmeSnapshotRecoveryManagerPtr      m_recoveryMgr;
    AllowRecoveryCallback                     m_allowRecoveryCB;
    RecoveryCompleteCallback                  m_recoveryCompleteCB;
    uint32_t                                  m_msgRecvCnt;
    uint32_t                                  m_updatesMissed;
    uint32_t                                  m_updatesMissedSinceLast;
    uint32_t                                  m_recoveryNum;
    uint32_t                                  m_recoverySkipped;
    uint32_t                                  m_bookUpdateMsgCnt;
    timeval_t                                 m_lastRecoveryTime;
};

bool CmeBook::isCrossed() const
{
    bool bid_empty = m_viewedLevels[BID]->empty() || !( *m_viewedLevels[BID] )[0];
    bool ask_empty = m_viewedLevels[ASK]->empty() || !( *m_viewedLevels[ASK] )[0];
    if( likely( !bid_empty && !ask_empty ) )
    {
        double bidpx = ( *m_viewedLevels[BID] )[0]->getPrice();
        double askpx = ( *m_viewedLevels[ASK] )[0]->getPrice();
        return bb::GE( bidpx, askpx );
    }
    // a completely empty book is not considered crossed: we still want to
    // notify the listener when the book is cleared
    return false;
}

BB_DECLARE_SHARED_PTR( CmeBook );

class CmeBookSpec : public SourceBookSpec
{
public:
    BB_DECLARE_SCRIPTING();

    CmeBookSpec()
        : SourceBookSpec(),
          m_notificationFlags(0),
          m_view(CmeBook::MERGED),
          m_holeCheckDepth(2)
    {}
    CmeBookSpec(const CmeBookSpec &, const boost::optional<bb::InstrSubst> &);
    CmeBookSpec(const instrument_t &instr, source_t src)
        : SourceBookSpec(instr, src),
          m_notificationFlags(0),
          m_view(CmeBook::MERGED),
          m_holeCheckDepth(2)
    {}

    IBookPtr build(BookBuilder *builder) const;
    void print(std::ostream &os, const LuaPrintSettings &ps) const;
    CmeBookSpec *clone(const boost::optional<InstrSubst> &instrSubst =
            boost::none) const;

private:
    // if this flag is false, notifyBookChanged() will not be called on a
    // crossed book
    uint32_t m_notificationFlags;
    CmeBook::EView m_view;
    size_t m_holeCheckDepth;
};

BB_DECLARE_SHARED_PTR( CmeBookSpec );

} // Namespace bb

#endif // BB_CLIENTCORE_CMEBOOK_H

#ifndef BB_CLIENTCORE_ICLOCKLISTENER_H
#define BB_CLIENTCORE_ICLOCKLISTENER_H

/* Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved. */


#include <bb/core/timeval.h>
#include <bb/core/smart_ptr.h>

namespace bb {

/// Interface for clients that need to be "woken up" at a specified time
class IClockListener
{
public:
    virtual ~IClockListener() {}

    /// Invoked by ClockMonitor when a subscribed "wakeup" time has arrived.
    /// @param ctv    "current timeval", according ClockMonitor's last message
    /// @param swtv   "scheduled wakeup timeval", the time at which the client wished to be awaken
    /// @param reason the reason code assigned for this wakeup call
    /// @param pData  user defined data assigned to the wakeup call
    virtual void onWakeupCall( const timeval_t& ctv, const timeval_t& swtv, int32_t reason, void* pData ) = 0;
};

BB_DECLARE_SHARED_PTR( IClockListener );

} // namespace bb

#endif // BB_CLIENTCORE_ICLOCKLISTENER_H

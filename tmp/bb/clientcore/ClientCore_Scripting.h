#ifndef BB_CLIENTCORE_CLIENTCORE_SCRIPTING_H
#define BB_CLIENTCORE_CLIENTCORE_SCRIPTING_H

/* Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved. */


namespace bb {

/// Registers the clientcore library with ScriptManager.
/// The library can be loaded from C++ with ScriptManager::loadLibary( "clientcore" )
/// or from Lua with loadLibary( "clientcore" ).
bool clientcore_registerScripting();

} // namespace bb


#endif // BB_CLIENTCORE_CLIENTCORE_SCRIPTING_H

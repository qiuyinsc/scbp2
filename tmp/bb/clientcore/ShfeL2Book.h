#ifndef BB_CLIENTCORE_SHFEL2BOOK_H
#define BB_CLIENTCORE_SHFEL2BOOK_H

/* Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved. */

#include <bb/core/LuabindScripting.h>
#include <bb/core/LuaCodeGen.h>

#include <bb/clientcore/BookSpec.h>
#include <bb/clientcore/BookBuilder.h>
#include <bb/clientcore/ClientContext.h>
#include <bb/clientcore/L2BookBase.h>
#include <bb/clientcore/Random.h>
#include <bb/clientcore/ShfeBook.h>

namespace bb {

class ShfeL2Book
    : public L2BookBase
    , public IBookListener
{
public:
    ShfeL2Book( const ClientContextPtr& context, const instrument_t& instr, source_t src
        , SourceMonitorPtr sm, const std::string& desc, int _vbose=0);
//    ShfeL2Book( const ClientContextPtr& context, const instrument_t& instr, source_t src
//        , const std::string& desc, int _vbose = 0 );

    virtual ~ShfeL2Book();

    /// Invoked when the subscribed Book changes.
    /// The levelChanged entries are negative if there is no change, or a 0-based depth.
    /// This depth is a minimum -- there could be multiple deeper levels that changed
    /// since the last onBookChanged.
    virtual void onBookChanged( const IBook* pBook, const Msg* pMsg,
                                int32_t bidLevelChanged, int32_t askLevelChanged );
    /// Invoked when the subscribed Book is flushed.
    virtual void onBookFlushed( const IBook* pBook, const Msg* pMsg );

    virtual bool dropBookLevel( side_t side, double px );

    /// ClockListener interface
    virtual void onWakeupCall( const timeval_t& ctv, const timeval_t& swtv, int reason, void* pData );

public:
protected:
    virtual std::pair<long, long> getBookCheckOffset();

    void dropBookLevelsWithinSpread( side_t side, double best_price );
    void updateBookLevel( side_t side, double price, int32_t size, const timeval_t sent_time );
    ShfeBook m_shfeBook;
};

BB_DECLARE_SHARED_PTR( ShfeL2Book );


/// BookSpec corresponding to an Shfe book
class ShfeL2BookSpec : public BookSpecCommon
{
public:
    BB_DECLARE_SCRIPTING();

    ShfeL2BookSpec() : m_source(SRC_UNKNOWN) {}
    ShfeL2BookSpec(const bb::ShfeL2BookSpec&, const boost::optional<bb::InstrSubst>&);
    ShfeL2BookSpec(const instrument_t &instr, source_t src)
        :  BookSpecCommon(instr), m_source(src)
    {}

    virtual IBookPtr build(BookBuilder *builder) const;
    virtual void checkValid() const;
    virtual void hashCombine(size_t &result) const;
    virtual bool compare(const IBookSpec *other) const;
    virtual void print(std::ostream &o, const LuaPrintSettings &ps) const;
    virtual ShfeL2BookSpec *clone(const boost::optional<InstrSubst> &instrSubst = boost::none) const;
    virtual void getDataRequirements(IDataRequirements *rqs) const;

    source_t m_source;
};
BB_DECLARE_SHARED_PTR(ShfeL2BookSpec);


} // namespace bb


#endif // BB_CLIENTCORE_SHFEL2BOOK_H

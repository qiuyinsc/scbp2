#ifndef BB_CLIENTCORE_MARKETLEVELPRICEPROVIDER_H
#define BB_CLIENTCORE_MARKETLEVELPRICEPROVIDER_H

/* Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved. */


#include <bb/clientcore/BookPriceProvider.h>
#include <bb/clientcore/L1MarketLevelProvider.h>
#include <bb/clientcore/PriceProvider.h>

namespace bb {

BB_FWD_DECLARE_SHARED_PTR(MarketLevelBasedPriceProvider);
BB_FWD_DECLARE_SHARED_PTR(MarketLevelBasedPriceSizeProvider);

/// An adapter that turns an IMarketLevelProvider into an IPriceProvider
class MarketLevelBasedPriceProvider
    : public PriceProviderImpl
{
public:
    /// Constructor
    MarketLevelBasedPriceProvider(IMarketLevelProviderPtr const & market_level_provider, EBookPriceView view);
    MarketLevelBasedPriceProvider(IMarketLevelProviderPtr const & market_level_provider);// = BPV_BEST_MIDPX

    /// Destructor
    virtual ~MarketLevelBasedPriceProvider() { }

    virtual instrument_t getInstrument() const
    {
        return m_market_level_provider->getInstrument();
    }

    virtual timeval_t getLastChangeTime() const
    {
        return m_market_level_provider->getLastUpdateTime();
    }

    /// Returns a price based on the specified 'view' of the Book.
    /// Returns 0.0 if that view of the price is not available, and sets
    /// success to false. Otherwise returns a price and sets success true.
    virtual double getRefPrice(bool* success = NULL) const;

    /// Returns true if getRefPrice would return a valid price if asked at current moment.
    virtual bool isPriceOK() const;

private:
    /// MarketLevelProvider that we are keeping track of
    IMarketLevelProviderPtr m_market_level_provider;

    /// "View" of what kind of price to return for this MarketLevelProvider
    EBookPriceView m_view;
};

/// An adapter that turns an IMarketLevelProvider into an IPriceSizeProvider
class MarketLevelBasedPriceSizeProvider
    : public PriceSizeProviderImpl
{
public:
    /// Constructor
    MarketLevelBasedPriceSizeProvider(IMarketLevelProviderPtr market_level_provider, EBookPriceView view =
        BPV_BEST_MIDPX);

    /// Destructor
    virtual ~MarketLevelBasedPriceSizeProvider() { }

    virtual instrument_t getInstrument() const
    {
        return m_market_level_provider->getInstrument();
    }

    virtual timeval_t getLastChangeTime() const
    {
        return m_market_level_provider->getLastUpdateTime();
    }

    /// Returns a price and size based on the specified 'view' of the Book.
    /// Returns 0.0 if that view of the price and size are not available, and
    /// sets success to false. Otherwise returns a price and sets success true.
    virtual PriceSize getRefPriceSize(bool* success = NULL) const;

    /// Returns true if getRefPrice would return a valid price if asked at current moment.
    virtual bool isPriceSizeOK() const;

private:
    /// MarketLevelProvider that we are keeping track of
    IMarketLevelProviderPtr m_market_level_provider;

    /// "View" of what kind of price to return for this MarketLevelProvider
    EBookPriceView m_view;
};

} // namespace bb

#endif // BB_CLIENTCORE_MARKETLEVELPRICEPROVIDER_H

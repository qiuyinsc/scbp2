#ifndef BB_CLIENTCORE_CFFEXSFITCOMBOL1BOOK_H
#define BB_CLIENTCORE_CFFEXSFITCOMBOL1BOOK_H

/* Contents Copyright 2012 Athena Capital Research LLC. All Rights Reserved. */

#include <bb/core/hash_map.h>
#include <bb/core/timeval.h>
#include <bb/clientcore/CffexL1Book.h>
#include <bb/clientcore/ShfeBook.h>

namespace bb {

class ClientContext;

BB_FWD_DECLARE_SHARED_PTR( MsgHandler );
BB_FWD_DECLARE_SHARED_PTR( SourceMonitor );
BB_FWD_DECLARE_SHARED_PTR( RandomSource );
BB_FWD_DECLARE_SHARED_PTR( ClientContext );

class CffexSfitComboL1Book
    : public L1Book
    , protected IBookListener
{
public:
    CffexSfitComboL1Book( const ClientContextPtr& context, const instrument_t& instr, source_t src
        , SourceMonitorPtr sm, const std::string& desc, int _vbose=0);
    CffexSfitComboL1Book( const ClientContextPtr& context, const instrument_t& instr, source_t src
        , const std::string& desc, int _vbose = 0 );
    virtual ~CffexSfitComboL1Book();

protected:
    virtual void onBookChanged( const IBook *book, const Msg *msg,
                                int32_t bidLevelChanged, int32_t askLevelChanged );

    virtual void onBookFlushed( const IBook *book, const Msg *msg );

    virtual bool isOK() const
    { return m_spCffexBook->isOK() && m_spShfeBook->isOK(); }

    virtual double getMidPrice() const
    { return getMoreUpdatedBook()->getMidPrice(); }

    virtual timeval_t getLastChangeTime() const
    { return getMoreUpdatedBook()->getLastChangeTime(); }

    virtual PriceSize getNthSide( size_t depth, side_t side ) const
    { return getMoreUpdatedBook()->getNthSide( depth, side ); }

    virtual IBookLevelCIterPtr getBookLevelIter( side_t side ) const
    { return getMoreUpdatedBook()->getBookLevelIter( side ); }

    virtual size_t getNumLevels( side_t side ) const
    { return getMoreUpdatedBook()->getNumLevels( side ); }

private:
    IBookPtr getMoreUpdatedBook() const
    {
        if( m_spCffexBook->getLastChangeTime() >= m_spShfeBook->getLastChangeTime() )
        {
            if( m_spCffexNhProxyBook->getLastChangeTime() >= m_spCffexBook->getLastChangeTime() )
            {
                return m_spCffexNhProxyBook;
            }
            else
            {
                return m_spCffexBook;
            }
        }
        else
        {
            if( m_spCffexNhProxyBook->getLastChangeTime() >= m_spShfeBook->getLastChangeTime() )
            {
                return m_spCffexNhProxyBook;
            }
            else
            {
                return m_spShfeBook;
            }
        }
    }

    void pegSourceCounter( const bb::source_t& source );
    void printSourceCounter() const;

protected:
    CffexL1BookPtr m_spCffexBook;
    CffexL1BookPtr m_spCffexNhProxyBook;
    ShfeBookPtr m_spShfeBook;
    timeval_t m_lastExchangeTv;
    typedef bbext::hash_map<source_t, uint32_t> SourceCounterMap;
    SourceCounterMap m_sourceCounter;
};

BB_DECLARE_SHARED_PTR( CffexSfitComboL1Book );

/// BookSpec corresponding to an CffexSfitComboL1Book
class CffexSfitComboL1BookSpec : public SourceBookSpec
{
public:
    BB_DECLARE_SCRIPTING();

    CffexSfitComboL1BookSpec() : SourceBookSpec() {}
    CffexSfitComboL1BookSpec(const bb::CffexSfitComboL1BookSpec&, const boost::optional<bb::InstrSubst>&);
    CffexSfitComboL1BookSpec(const instrument_t &instr, source_t src)
        : SourceBookSpec(instr, src)
    {}

    virtual IBookPtr build(BookBuilder *builder) const;
    virtual void print(std::ostream &o, const LuaPrintSettings &ps) const;
    virtual CffexSfitComboL1BookSpec *clone(const boost::optional<InstrSubst> &instrSubst = boost::none) const;
};
BB_DECLARE_SHARED_PTR(CffexSfitComboL1BookSpec);

} // namespace bb

#endif // BB_CLIENTCORE_CFFEXSFITCOMBOL1BOOK_H

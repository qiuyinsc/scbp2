#ifndef BB_CLIENTCORE_MARKET_H
#define BB_CLIENTCORE_MARKET_H

/* Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved. */


#include <ostream>
#include <boost/optional.hpp>
#include <bb/core/side.h>
#include <bb/core/PriceSize.h>

namespace bb {

class Market
{
public:
    void clear()
    {
        m_bid.reset();
        m_ask.reset();
    }
    const boost::optional<bb::PriceSize>& getBid() const
    { return m_bid; }
    const boost::optional<bb::PriceSize>& getAsk() const
    { return m_ask; }
    const boost::optional<bb::PriceSize>& getSide( bb::side_t side ) const
    {
        switch( side )
        {
        case bb::BID:
            return m_bid;
        case bb::ASK:
        default:
            return m_ask;
        }
    }

    boost::optional<bb::PriceSize>& getSide( bb::side_t side )
    {
        switch( side )
        {
        case bb::BID:
            return m_bid;
        case bb::ASK:
        default:
            return m_ask;
        }
    }

    double getMidPrice() const
    {
        return (m_bid->px() + m_ask->px()) / 2.0;
    }

    void setBid( const bb::PriceSize& bid )
    { m_bid = bid; }
    void setAsk( const bb::PriceSize& ask )
    { m_ask = ask; }

    void clearBid() { m_bid.reset(); }
    void clearAsk() { m_ask.reset(); }

    int32_t getBidSize() const { return (getBid() ? getBid()->getSize() : 0); }
    int32_t getAskSize() const { return (getAsk() ? getAsk()->getSize() : 0); }
    int32_t getSize( bb::side_t side ) const
    {
        const boost::optional<bb::PriceSize>& pxsz = getSide( side );
        return (pxsz ? pxsz->getSize() :0);
    }

    /// MarketLevel compatibility
    /// Returns the total size of the bid and ask sides.
    int32_t getTotalSize() const
    { return getBidSize() + getAskSize(); }

    /// Returns true if all values are 0.
    bool empty() const
    {
        return !getBid() && !getAsk();
    }
    bool crossed() const
    {
        return getBid() && getAsk() && (getBid()->getPrice() >= getAsk()->getPrice());
    }

    bool operator == ( const Market& m ) const
    { return (m_bid == m.m_bid) && (m_ask == m.m_ask); }

protected:
    boost::optional<bb::PriceSize> m_bid;
    boost::optional<bb::PriceSize> m_ask;
};

inline std::ostream& operator << ( std::ostream& os, const Market& mkt )
{
    if( mkt.getBid() ) os << mkt.getBid().get();
    else               os << "0@0";
    os << "x";
    if( mkt.getAsk() ) os << mkt.getAsk().get();
    else               os << "0@0";
    return os;
}

} // namespace bb

#endif // BB_CLIENTCORE_MARKET_H

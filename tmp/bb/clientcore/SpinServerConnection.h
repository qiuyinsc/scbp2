#ifndef BB_CLIENTCORE_SPINSERVERCONNECTION_H
#define BB_CLIENTCORE_SPINSERVERCONNECTION_H

/* Contents Copyright 2016 Athena Capital Research LLC. All Rights Reserved. */
#include <bb/clientcore/ClientContext.h>

namespace bb {
BB_FWD_DECLARE_SHARED_PTR( TCPSocketSendTransport );

class ISpinServerConnection
{
public:
    ISpinServerConnection( const HostPortPair& );

    virtual void send( const Msg * ) = 0;

    virtual ~ISpinServerConnection(){}

protected:
    HostPortPair m_hostPort;
};

BB_DECLARE_SHARED_PTR( ISpinServerConnection );

class SpinServerConnection : public ISpinServerConnection
{
public:
    SpinServerConnection( const LiveMStreamManagerPtr& lmsm, const HostPortPair& hpp );
    virtual ~SpinServerConnection();

    void send( const Msg * );

protected:
    TCPSocketSendTransportPtr   m_sendTransport;

};
BB_DECLARE_SHARED_PTR( SpinServerConnection );

class SimSpinServer
{
public:
    SimSpinServer(){}
    virtual ~SimSpinServer(){}
};
BB_DECLARE_SHARED_PTR( SimSpinServer );


class SimSpinServerConnection : public ISpinServerConnection
{
public:
    SimSpinServerConnection( const HistMStreamManagerPtr& lmsm, const HostPortPair& hpp );
    virtual ~SimSpinServerConnection();

    void send( const Msg * );

    //simulates starting up the Spinserver the connection will be connecting to
    virtual void setSimSpinServer( const SimSpinServerPtr& simSpinServer );
protected:
    HistMStreamManagerPtr m_histMStreamManager;
    SimSpinServerPtr m_simSpinServer;
};

BB_DECLARE_SHARED_PTR( SimSpinServerConnection );

} // namespace bb

#endif // BB_CLIENTCORE_SPINSERVERCONNECTION_H

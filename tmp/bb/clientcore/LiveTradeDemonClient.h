#ifndef BB_CLIENTCORE_LIVETRADEDEMONCLIENT_H
#define BB_CLIENTCORE_LIVETRADEDEMONCLIENT_H

/* Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved. */


#include <boost/optional.hpp>

#include <bb/core/Subscription.h>
#include <bb/core/network.h>
#include <bb/core/source.h>

#include <bb/clientcore/ITradeDemonClient.h>

namespace bb {

class FDSet;
class ICancelableDNSResolver;
class MarginSubscriptionBaseMsg;
class Msg;
class msg_td_client_config;

BB_FWD_DECLARE_SHARED_PTR( ISendTransport );
BB_FWD_DECLARE_SHARED_PTR( IRecvTransport );
BB_FWD_DECLARE_SHARED_PTR( SaslClient );
BB_FWD_DECLARE_SHARED_PTR( NetSocket );
BB_FWD_DECLARE_SHARED_PTR( TCPSocket );
BB_FWD_DECLARE_SHARED_PTR( TCPSocketSendTransport );
BB_FWD_DECLARE_SHARED_PTR( SCTPSocket );
BB_FWD_DECLARE_SHARED_PTR( SCTPSocketSendTransport );
BB_FWD_DECLARE_SHARED_PTR( InProcessTransport );

BB_FWD_DECLARE_SCOPED_PTR(TdOrderMsg);
BB_FWD_DECLARE_SCOPED_PTR(TdOrderFutureMsg);
BB_FWD_DECLARE_SCOPED_PTR(TdOrderNewMsg);
BB_FWD_DECLARE_SCOPED_PTR(TdCancelMsg);
BB_FWD_DECLARE_SCOPED_PTR(TdOrderModifyMsg);
BB_FWD_DECLARE_SCOPED_PTR(TdCancelBySideMsg);
BB_FWD_DECLARE_SCOPED_PTR(MarginBroadcastRequestMsg);
BB_FWD_DECLARE_SCOPED_PTR(MarginSubscribeMsg);
BB_FWD_DECLARE_SCOPED_PTR(MarginUnsubscribeMsg);

/// ITradeDemonClient implementation connecting to
/// a BB message-based Trade Demon (batstd, ord, etc.)
class LiveTradeDemonClient
    : public ITradeDemonClient
{
public:
    virtual bool is_valid() const { return m_sendTrans && m_recvTrans; }

    // timeout/visible only apply to equity orders
    virtual hc_return_t send_order( acct_t acct
                                    , const instrument_t& instr
                                    , dir_t dir
                                    , mktdest_t dest
                                    , double px
                                    , unsigned int size
                                    , unsigned int orderid
                                    , int timeout
                                    , bool visible
                                    , tif_t tif
                                    , uint32_t oflags
                                    , timeval_t* send_time = NULL
                                    , const ISpecialOrderPostProcessor* specialOrder = NULL );

    virtual hc_return_t send_cancel( acct_t acct, const instrument_t& instr,
            unsigned int orderid, timeval_t* send_time = NULL );

    virtual hc_return_t send_cancels_for_side( acct_t acct, const instrument_t& instr,
                side_t side, timeval_t* send_time = NULL );

    // timeout/visible only apply to equity orders
    virtual hc_return_t modify_order( acct_t acct
                                     , unsigned int orig_orderid
                                     , const instrument_t& instr
                                     , dir_t dir
                                     , mktdest_t dest
                                     , double px
                                     , unsigned int size
                                     , unsigned int orderid
                                     , int timeout
                                     , bool visible
                                     , tif_t tif
                                     , uint32_t oflags
                                     , timeval_t* send_time = NULL
                                     , const ISpecialOrderPostProcessor* specialOrder = NULL );

    virtual bool setup_position_broadcasts( acct_t, const instrument_t& );
    virtual bool margin_subscribe         ( acct_t, const instrument_t& );
    virtual bool margin_unsubscribe       ( acct_t, const instrument_t& );

    /// sends an identify message to the server
    /// returns true if successful
    virtual bool identify( const char* id, timeval_t* send_time = NULL );

    /// sends a configuration request to the server (see message definition for the parameters)
    /// returns true if successful
    virtual bool send_config( const msg_td_client_config& );

    /// sends a test message to the server with the given text
    /// trickle makes it send one byte at a time (for TCP congestion window tuning)
    /// returns true if successful
    virtual bool send_test_request( const std::string& text, bool trickle = false );

    /// request position offset snapshot from TD
    /// it is only applicable to a number of markets where traders need to know about position offsets
    /// returns true if successful
    virtual bool send_td_position_offset_request();

    IRecvTransportPtr    getRecvTransport()  { return m_recvTrans; }
    ISendTransportPtr    getSendTransport()  { return m_sendTrans; }
    source_t getSource() const { return m_source; }

protected:
    LiveTradeDemonClient( source_t source_type = source_t::make_auto( SRC_CLIENT ) );
    virtual ~LiveTradeDemonClient();

    /// sends a disconnect message to the server
    void disconnect();

    /// implementation for margin_subscribe/margin_unsubscribe
    bool margin_subscribe_unsubscribe( acct_t, const instrument_t&, MarginSubscriptionBaseMsg& );

    /// sends a message to the server (not for direct use), returning true on success
    bool send( const Msg& );
    bool send_one_byte_at_a_time( const Msg& );

    ISendTransportPtr m_sendTrans;
    IRecvTransportPtr m_recvTrans;
    InProcessTransportPtr m_spInProcessTransport;
    source_t m_source;

    const TdOrderMsgScopedPtr m_stockOrderMsg;
    const TdOrderFutureMsgScopedPtr m_futureOrderMsg;
    const TdOrderNewMsgScopedPtr m_orderMsg;
    const TdCancelMsgScopedPtr m_cancelMsg;
    const TdOrderModifyMsgScopedPtr m_modMsg;
    const TdCancelBySideMsgScopedPtr m_cancelSideMsg;
    const MarginBroadcastRequestMsgScopedPtr m_marginBroadcastRequestMsg;
    const MarginSubscribeMsgScopedPtr m_marginSubscribeMsg;
    const MarginUnsubscribeMsgScopedPtr m_marginUnsubscribeMsg;
};
BB_DECLARE_SHARED_PTR( LiveTradeDemonClient );


/// ITradeDemonClient implementation connecting to a BB message-based Trade Demon via TCP, MQ or Unix sockets
class TradeDemonClient
    : public LiveTradeDemonClient
{
public:

    // host can be "mqueue" to use MQ, "unix" for Unix domain sockets, a hostname for TCP,
    // "tcp" to look up hostname automatically, or "auto" to choose the best available transport
    // in the case of TCP (only), this function can block on DNS resolution
    // uses serviceName for Kerberos authentication
    TradeDemonClient( const char* host, acct_t acct, const char* serviceName = "bb.td" );

    TransportType getTransportType() const override { return m_transportType; }

    virtual bool is_valid() const; // we are valid only if authenticated

protected:
    // for use by "ord" only, as it uses SRC_ORD rather than SRC_CLIENT
    TradeDemonClient( const char* host, acct_t acct, source_t source, const char* serviceName = "bb.td" );
    void handleSaslMessage( const bb::Msg& msg );

    // for use by SaslAdminTradeDemonClient
    bool isAuthenticated() const;

private:
    // returns the fully-qualified domain name of the given TD host (boost::none on failure)
    boost::optional<std::string> connect( const char* host, acct_t acct );

protected:
    // if fdSet is not NULL, use non-blocking send transports
    // a TCP socket is returned only if the connection could not be immediately established so must be continued asynchronously
    std::pair<TransportType, NetSocketPtr> connect( const char* host, const std::string& td_fqdn,
            const ipv4_addr_t&, int port, FDSet* fdSet = NULL );

    void completeConnection( FDSet*, const TCPSocketPtr& );
    void completeConnection( FDSet*, const SCTPSocketPtr& );

    TransportType m_transportType;
    SaslClientPtr m_saslClient;
};
BB_DECLARE_SHARED_PTR( TradeDemonClient );


/// Like TradeDemonClient but with non-blocking sending to the TD
class NonBlockingTradeDemonClient
    : public TradeDemonClient
{
protected:
    // to be called back when the transports are set up, so beginDispatch() can be called etc.
    typedef boost::function<void ()> ConnectCB;

    // for use by "ord" only, as it uses SRC_ORD rather than SRC_CLIENT
    // the ConnectCB will be called as soon as the TD connection has either succeeded or failed
    // this will probably be before authentication has completed
    // in the current implementation it is only permissible to delete this (that is, this actual
    // NonBlockingTradeDemonClient) in the provided callback if getTransportType() == NONE (i.e. failure)
    NonBlockingTradeDemonClient( FDSet&, ICancelableDNSResolver&, const ConnectCB&,
            const char* host, acct_t, source_t, const char* serviceName = "bb.td" );

private:
    void connectNonBlocking( FDSet&, ICancelableDNSResolver&, const char* host, acct_t );
    void onNameResolution( FDSet&, const char* host, const std::string& td_fqdn, const ipv4_addr_t&, int port );
    void onSocketConnected( FDSet&, const NetSocketPtr&, TransportType transport, const std::string& td_fqdn );

    Subscription m_connectSub;
    ConnectCB m_connectCB;
};
BB_DECLARE_SHARED_PTR( NonBlockingTradeDemonClient );


/// For use only by utility programs that need to send special administrative messages to a TD
class AdminTradeDemonClient
    : public TradeDemonClient
{
public:
    AdminTradeDemonClient( const char* host, acct_t acct );
    using LiveTradeDemonClient::send;

private:
    using LiveTradeDemonClient::send_order;
    using LiveTradeDemonClient::send_cancel;
    using LiveTradeDemonClient::modify_order;
};
BB_DECLARE_SHARED_PTR( AdminTradeDemonClient );

} // namespace bb

std::ostream& operator << ( std::ostream& os, bb::ITradeDemonClient::TransportType t );

#endif // BB_CLIENTCORE_LIVETRADEDEMONCLIENT_H

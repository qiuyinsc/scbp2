#ifndef BB_CLIENTCORE_TDLOGMANAGER_H
#define BB_CLIENTCORE_TDLOGMANAGER_H

/* Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved. */


#include <bb/core/Msg.h>
#include <bb/core/acct.h>
#include <bb/core/gdatefwd.h>

#include <bb/core/MessageMacros.h>
#include <bb/clientcore/ClientContext.h>

namespace bb {

namespace io {
BB_FWD_DECLARE_SHARED_PTR(DFStreamMplex);
}

class TdFillNewMsg;
class TdFillFutureMsg;

BB_FWD_DECLARE_SHARED_PTR(TDLogManager);

/// Metadata about each message in a TD log.
struct TDLogInfo
{
    TDLogInfo() : m_fileID() { }
    uint32_t m_fileID; // a unique key that identifies the filename. cheaper than keying of m_filename.
    std::string m_filename;
    boost::optional<std::string> m_ident;
};

/// Histo helper for loading TD log files.
/// Loads TD log files into the HistMStreamManager & allows you to subscribe.
///
/// Added value over EventDistributor: gives you TDLogInfo in each listener callback.
class TDLogManager : public IEventDistListener, public boost::enable_shared_from_this<TDLogManager>
{
public:
    static TDLogManagerPtr create(HistClientContextPtr cc);

    /// Returns all filenames matching the date/acct in the historical archive.
    static std::vector<std::string> scanLogs(const date_t& date, acct_t acct);
    static std::vector<std::string> scanLogs(const gdate_t& date, acct_t acct);

    void addLog(std::string filename);

    template<typename TMessage>
    void subscribeTDEventsMType(Subscription &sub,
            boost::function<void (const TDLogInfo &, const TMessage &)> const &handler,
            Priority priority);

    template<typename TMessage>
    void subscribeTDEvents(Subscription &sub,
            boost::function<void (const TDLogInfo &, const TMessage &)> const &handler,
            const instrument_t &instr, Priority priority);

protected:
    TDLogManager(HistClientContextPtr cc);

    virtual void onEvent( const Msg& msg );

    struct DispatcherBase : public IEventDistListener, public SubscriptionBase
    {
        DispatcherBase(TDLogManagerCPtr const &mgr, EventDistributorPtr const &ed, mtype_t mtype, Priority priority);
        DispatcherBase(TDLogManagerCPtr const &mgr, EventDistributorPtr const &ed,
                mtype_t mtype, instrument_t const &instr, Priority priority);
        virtual bool isValid() const;
        void lookupInfo(TDLogInfo &info);

        template<typename T> bool isDispatcherInstrument( const T& msgT ) const { return true; }

        Subscription m_selfSub;
        TDLogManagerWeakCPtr m_mgr;
        instrument_t m_instr;
    };

    template<typename TMessage>
    struct Dispatcher : public DispatcherBase
    {
        typedef boost::function<void (const TDLogInfo &, const TMessage &)> Handler_t;

        Dispatcher(TDLogManagerCPtr const &mgr, Handler_t const &hdlr,
                EventDistributorPtr const &ed, Priority priority)
            : DispatcherBase(mgr, ed, TMessage::kMType, priority), m_handler(hdlr) {}
        Dispatcher(TDLogManagerCPtr const &mgr, Handler_t const &hdlr,
                EventDistributorPtr const &ed, instrument_t const &instr, Priority priority)
            : DispatcherBase(mgr, ed, TMessage::kMType, instr, priority), m_handler(hdlr) {}
        virtual void onEvent( const Msg& msg )
        {
            BB_MESSAGE_SWITCH_BEGIN(&msg)
            BB_MESSAGE_CASE(TMessage, msgT)
                if( isDispatcherInstrument( *msgT ) )
                {
                    TDLogInfo info;
                    lookupInfo(info);
                    m_handler(info, *msgT);
                }
            BB_MESSAGE_CASE_END()
            BB_MESSAGE_SWITCH_END()
        }

        Handler_t m_handler;
    };

    HistClientContextPtr m_cc;
    DFStreamMplexPtr m_dfMplex;
    Subscription m_identifySub;
    typedef bbext::hash_map<const IHistMStream *, std::pair<std::string, int> > DFNameIDHash;
    typedef bbext::hash_map<const IHistMStream *, std::string> DFNameHash;
    DFNameIDHash m_datafileNames;
    DFNameHash m_idents;
    int m_nextDFID;
    bool m_logFeeds;
};

template<> bool TDLogManager::DispatcherBase::isDispatcherInstrument( const TdFillNewMsg& msg ) const;
template<> bool TDLogManager::DispatcherBase::isDispatcherInstrument( const TdFillFutureMsg& msg ) const;

template<typename TMessage>
inline void TDLogManager::subscribeTDEventsMType(Subscription &sub, boost::function<void (const TDLogInfo &, const TMessage &)> const &handler, Priority priority)
{
    sub = Subscription(new Dispatcher<TMessage>(shared_from_this(), handler, m_cc->getEventDistributor(), priority));
}

template<typename TMessage>
inline void TDLogManager::subscribeTDEvents(Subscription &sub, boost::function<void (const TDLogInfo &, const TMessage &)> const &handler, const instrument_t &instr, Priority priority)
{
    sub = Subscription(new Dispatcher<TMessage>(shared_from_this(), handler, m_cc->getEventDistributor(), instr, priority));
}

} // namespace bb

#endif // BB_CLIENTCORE_TDLOGMANAGER_H

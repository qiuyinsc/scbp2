#ifndef BB_CLIENTCORE_TESTTICKFACTORY_H
#define BB_CLIENTCORE_TESTTICKFACTORY_H

/* Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved. */


#include <bb/core/smart_ptr.h>
#include <bb/clientcore/TickFactory.h>

namespace bb
{

class TestTickFactory : public bb::SourceTickFactory
{
public:
    TestTickFactory( ClientContextPtr cc ) : SourceTickFactory( cc ) {}
    class TestTickProvider : public TickProviderImpl
    {
    public:
        TestTickProvider( const instrument_t& instr, source_t& src )
            : TickProviderImpl( src, instr, "", true ) {}
        void setTotalVolume( uint64_t totalVolume ) { TickProviderImpl::setTotalVolume( totalVolume ); }
        virtual bool isLastTickOK() const { return true; }
        void publishTick( const TradeTick& tt )
        {
            setLastTimestamps( tt.getExchangeTime(), tt.getMsgTime() );
            setLastTradeTick( tt );
            notifyTickReceived();
        }
    };
    BB_DECLARE_SHARED_PTR( TestTickProvider );

    virtual ITickProviderPtr getTickProvider( const instrument_t& instr, source_t src, bool create = false )
    {
        return getTestTickProvider( instr, src, create );
    }

    TestTickProviderPtr getTestTickProvider( const instrument_t& instr, source_t src, bool create = false )
    {
        InstrSource key( instr, src );
        if( !m_objectCache[ key ] )
        {
            m_objectCache[ key ] = TestTickProviderPtr( new TestTickProvider( instr, src ) );
        }
        return m_objectCache[ key ];
    }

protected:
    /// Returns all ITickProvider already existing in this factory.
    /// Note that the return shared_ptr is const.
    virtual const std::vector<ITickProviderPtr>& getTickProviders() const
    {
        static const std::vector<ITickProviderPtr> rval;
        return rval;
    }

protected:
    typedef bbext::hash_map<InstrSource, TestTickProviderPtr> InstrSource2TickMap;
    InstrSource2TickMap m_objectCache;
};

BB_DECLARE_SHARED_PTR( TestTickFactory );

} // namespace bb

#endif // BB_CLIENTCORE_TESTTICKFACTORY_H

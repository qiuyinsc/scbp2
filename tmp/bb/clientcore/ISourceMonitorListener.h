#ifndef BB_CLIENTCORE_ISOURCEMONITORLISTENER_H
#define BB_CLIENTCORE_ISOURCEMONITORLISTENER_H

/* Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved. */


#include <bb/core/ptime.h>
#include <bb/core/timeval.h>
#include <bb/core/source.h>

namespace bb {

// TODO: i want to make this a bitset or something like that
typedef enum {
    Stat_SMon_OK            = 0,
    Stat_SMon_Lagged        = 1,
    Stat_SMon_LongInterval  = 2,
    Stat_SMon_SourceDown    = 4
} smon_status_t;

/// Subclass this to listen to SourceMonitor events.
class SourceMonitorListener
{
public:
    virtual ~SourceMonitorListener() {}

    /// Issued when a SourceMonitor has a status change
    virtual void onSMonStatusChange( source_t src, smon_status_t status, smon_status_t old_status,
                                     const ptime_duration_t& avg_lag_tv,
                                     const ptime_duration_t& last_interval_tv) {}

    /// Allows SMonListeners to schedule wake up calls that are based on the timestamps
    /// of the data source (eg Isld, Arca, and other data feeds that include their own
    /// timestamps on the data they send), as opposed to wake up calls based on
    /// ClockMonitor's global timestamp which is based on when our own internal servers
    /// received the data.
    virtual void onReportedTvWakeup( timeval_t current_reported_tv, timeval_t swreported_tv,
                                     int reason, void* pData ) {}
};

} // namespace bb

#endif // BB_CLIENTCORE_ISOURCEMONITORLISTENER_H

#ifndef BB_CLIENTCORE_HKSETICKPROVIDER_H
#define BB_CLIENTCORE_HKSETICKPROVIDER_H

/* Contents Copyright 2014 Athena Capital Research LLC. All Rights Reserved. */

#include <bb/clientcore/TickProvider.h>

namespace bb {

BB_FWD_DECLARE_SHARED_PTR( ClockMonitor );
BB_FWD_DECLARE_SHARED_PTR( MsgHandler );

class HKSETickProvider : public TickProviderImpl
{
public:
    HKSETickProvider( ClientContext& context, const instrument_t& instr, source_t source, const std::string& desc );
    virtual ~HKSETickProvider() {}

    virtual bool isLastTickOK() const { return m_bTickReceived; }

private:
    void onHkseTradeMsg( const HkseTradeMsg& );
    //void onHkseTradeCancelMsg( const HkseTradeCancelMsg& );

private:
    ClockMonitorPtr m_spClockMonitor;
    bool m_bTickReceived;
    MsgHandlerPtr m_subHkseTradeMsg;
    //MsgHandlerPtr m_subHkseTradeCancelMsg;
};

BB_DECLARE_SHARED_PTR( HKSETickProvider );

} // end bb

#endif // BB_CLIENTCORE_HKSETICKPROVIDER_H

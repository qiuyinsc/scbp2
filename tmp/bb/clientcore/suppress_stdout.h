#ifndef BB_CLIENTCORE_SUPPRESS_STDOUT_H
#define BB_CLIENTCORE_SUPPRESS_STDOUT_H

/* Contents Copyright 2016 Athena Capital Research LLC. All Rights Reserved. */

#include <ostream>
#include <iostream>

#include <bb/core/logger.h>

#ifdef SUPPRESS_STDOUT

/*
 * Logging to std::cout is expensive and we do it everywhere. Strategists know this
 * and have taken steps to comment std::cout logging out or remove it completely.
 *
 * Often, we want to do speed measurements on the baseline, where we know that such
 * logging to std::cout will massively skew results. This class lets us turn off whole
 * sections of logging in on quick sweep - just:
 * - #include <bb/clientcore/suppress_stdout.h> where you (might) log to std::cout and
 * - scons .. whatever you want .. --suppress-stdout to make this code active.
 *
 * By default, of course, logging to std::cout will do what you expect it to do, and the
 * code in this file will not be in effect.
 */
namespace std {
    static struct suppressed_stdout_t : public ostream
    {
        suppressed_stdout_t() {}
        suppressed_stdout_t ( suppressed_stdout_t const & other ) {}
    } suppressed_stdout;

    suppressed_stdout_t& endl (suppressed_stdout_t& os);

    template <typename T>
    suppressed_stdout_t & operator << ( suppressed_stdout_t & suppressed_stdout, T const & t )
    {
        return suppressed_stdout;
    }

    #define cout suppressed_stdout
};
#endif // SUPPRESS_STDOUT

#endif // BB_CLIENTCORE_SUPPRESS_STDOUT_H

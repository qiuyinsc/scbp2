#ifndef BB_CLIENTCORE_DATAAVAILABILITY_H
#define BB_CLIENTCORE_DATAAVAILABILITY_H

/* Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved. */


#include <set>
#include <bb/core/instrument.h>
#include <bb/core/source.h>

/// These are some classes for answering the question (for example)
/// I've got this BookSpec, do all the datafiles exist for it on a given day?

namespace bb
{

/// Abstract interface class for computing the set of data that will be required to do some
/// computation; stuff that requires data will report their requirements through this intf.
class IDataRequirements
{
public:
    virtual ~IDataRequirements() {}

    virtual void requireData(source_t src, const instrument_t &instr) = 0;
    /// calls requireData on each src in srcs.
    virtual void requireDataMulti(const sources_t &srcs, const instrument_t &instr)
    {
        for(sources_t::const_iterator i = srcs.begin(); i != srcs.end(); ++i)
            requireData(*i, instr);
    }
    virtual void requireDataMulti(const std::set<source_t>& srcs, const instrument_t &instr)
    {
        for(std::set<source_t>::const_iterator i = srcs.begin(); i != srcs.end(); ++i)
            requireData(*i, instr);
    }
};

} // namespace bb

#endif // BB_CLIENTCORE_DATAAVAILABILITY_H

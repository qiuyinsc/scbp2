#ifndef BB_CLIENTCORE_CLOCKNOTICE_H
#define BB_CLIENTCORE_CLOCKNOTICE_H

/* Contents Copyright 2013 Athena Capital Research LLC. All Rights Reserved. */


#include <iosfwd>
#include <bb/core/LuaCodeGen.h>
#include <boost/date_time/local_time/local_time.hpp>

namespace bb {

class timezone_t;
class SessionParams;

namespace cm {

/// User Reasons should be >= USER_REASON
static const int32_t USER_REASON = 1000;

typedef enum {

    NOREASON=0, PREOPEN, TEN_BOPEN, THREE_BOPEN, ONE_BOPEN, ATOPEN,
    TWENTYSECS_AOPEN, TWO_AOPEN, FIVE_AOPEN, FOURTEEN_AOPEN,
    ONE_BTEN, ONE_BTENFIFTEEN, ONE_BTENTHIRTY, ONE_BELEVEN, ONE_BTWELVE,
    ONE_BTHIRTEEN, ONE_BFOURTEEN, ONE_BFIFTEEN, THIRTY_BCLOSE,
    FIFTEEN_BCLOSE, FIVE_BCLOSE, TWO_BCLOSE, TWENTYSECS_BCLOSE,
    ATCLOSE, ONE_ACLOSE, TEN_ACLOSE, AFTERCLOSE, ENDOFDAY,
    DAYROLLOVER, DO_NOT_TRADE_START, DO_NOT_TRADE_END,
    NUM_CNOTICES
    // USER_REASON = 1000, all clock notices must be less than that
} reason_t;

std::ostream &operator<<(std::ostream&, reason_t);
void luaprint(std::ostream &out, reason_t reason, LuaPrintSettings const &ps);

/// invalid_reason exception
class invalid_reason
    : public std::invalid_argument
{
public:
    invalid_reason( int32_t reason, const std::string& throwee_msg );
    int32_t getReason() const { return m_reason; }

private:
    int32_t m_reason;
};

/// An interesting time, and the reason it might be interesting
/// all tv's understood as UTC
struct clock_notice {
    clock_notice() : reason(NOREASON) {}
    clock_notice( reason_t r, const timeval_t& t ) : reason(r), tv(t) {}
    clock_notice( reason_t r, const boost::local_time::local_date_time& lt );
    clock_notice( reason_t r, const date_t& d, int32_t secs, const timezone_t& tz );

    /// Returns true if the clock_notice is valid.
    /// An invalid clock_notice has NOREASON and an empty timeval_t.
    bool is_valid() const
    {   return !(reason == NOREASON && tv == timeval_t()); }

    reason_t  reason;
    timeval_t tv;
};


/// Returns the clock_notice for the given instrument for the given reason on a given date.
/// Returns an invalid clock_notice if it cannot be determined. Optionally specify a `jitter`
/// value representing a time shift in milliseconds (to avoid all Book invoking
/// wake up call at the same time).
std::vector<clock_notice> getClockNotice(const SessionParams& params,
                                         const instrument_t& instr,
                                         int32_t ymd_date, reason_t reason,
                                         int jitter = 0);

/// Return all relevant clock_notices for a particular instrument on a
/// particular date. These clock_notices will include, among others, the
/// ATOPEN and ATCLOSE times for that symbol on that date. For example,
/// invoking this with GOOG will return a series of clock_notices, one of
/// which would be a clock_notice with reason_t ATOPEN and timeval
/// corresponding to 9:30am EST for that datei. Optionally specifiy a `jitter`
/// value representing a time shift in milliseconds (to avoid all Book invoking
/// wake up call at the same time).
std::vector<clock_notice> getClockNotices(const SessionParams& params,
                                          const instrument_t& instr,
                                          int32_t ymd_date,
                                          int jitter = 0);

std::vector<clock_notice> getClockNotices( int32_t atOpen, int32_t atClose, int32_t ymd_date, int jitter = 0);
std::vector<clock_notice> getClockNotices( int32_t atOpen, int32_t atClose
    , int32_t ymd_date
    , const bb::timezone_t& tz);

/// Searches the clock notices for the given notice, and returns it's timeval. Throws on error.
timeval_t findClockNotice( const std::vector<clock_notice> &notices, reason_t reason );

} // namespace cm
} // namespace bb


#endif // BB_CLIENTCORE_CLOCKNOTICE_H

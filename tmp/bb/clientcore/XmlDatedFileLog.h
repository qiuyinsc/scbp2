#ifndef BB_CLIENTCORE_XMLDATEDFILELOG_H
#define BB_CLIENTCORE_XMLDATEDFILELOG_H

/* Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved. */


#include <fstream>
#include <string>
#include <bb/io/XmlLog.h>
#include <bb/clientcore/IClockListener.h>
#include <bb/clientcore/ClockMonitor.h>

namespace bb {

/** XML log file object with automatic day rollover
 *
 * This is a log file with each log entry being an XML element.
 *
 * Logs to a file with filename like: <prefix>-<date>.<postfix>, automatically
 * rolls over to a new file on the next day.  Has the ability to self-timestamp
 * each log entry.
 *
 * <pre>
 * <Log File> ::= <log-entry 0>...<log-entry n>
 * <log-entry> ::= <XML-tag><cr>
 * </pre>
 *
 */
class XmlDatedFileLog : public bb::XmlLog, public bb::IClockListener
{
public:
    // Create an XmlDatedFileLog that logs to a formatted name based
    // on the prefix and postfix, and rolls over to a new file on day
    // boundaries.
    XmlDatedFileLog( const bb::ClockMonitorPtr& cm
        , const std::string& _prefix, const std::string& _postfix=".xml" );

    // Create an XmlDatedFileLog that always logs to the same file,
    // given by filename. This is primarily useful for testing. The
    // third argument is (unfortunately) simply to disambiguate this
    // constructor from the other constructor when called with two
    // arguments. The third argument is not evaluated.
    struct ExplicitFilename {};
    XmlDatedFileLog( const bb::ClockMonitorPtr& cm
        , const std::string& _filename, const ExplicitFilename& );

    virtual ~XmlDatedFileLog();

    /// Sets whether log will automatically insert a timestamp using the assigned
    /// ClockMonitor (default is false)
    void setAutoTimestamp( bool b ) { bautoTimestamp = b; }

protected:
    virtual void startXml()
    {
        if( bautoTimestamp && (getDepth() == 1) )
            (*this) << this->time;
    }
    virtual void endXml()
    {
        XmlLog::endXml();
    }
    virtual const bb::timeval_t getSelfTime() const
    {
        return spClockMonitor->getTime();
    }
    virtual void onWakeupCall( const bb::timeval_t& ctv, const bb::timeval_t& swtv, int32_t reason, void* );

private:
    static const int32_t WAKEUP_TOMORROW = 0x12345;
    std::string prefix;
    std::string postfix;
    std::string filename;
    std::ofstream file;
    bool bautoTimestamp;
    bb::ClockMonitorPtr spClockMonitor;
};

} // namespace bb

#endif // BB_CLIENTCORE_XMLDATEDFILELOG_H

#ifndef BB_CLIENTCORE_ARCAUTILS_H
#define BB_CLIENTCORE_ARCAUTILS_H

/* Contents Copyright 2015 Athena Capital Research LLC. All Rights Reserved. */
#include <stdint.h>
#include <bb/core/PriceScaler.h>

namespace bb {

// Since Arca, ICE and other exchanges do not send prices in decimal points,
// we create a Traits struct to create a Price Scaler object which converts the price to double format using additional
// information sent in the messages.

// For Ex: In Arca Feed, we get integer price and integer priceScale. The way to get a double price is
// price / (10^priceScale). ICE feed also has something similar.
template<>
struct PriceScalerTraits<bb::SRC_ARCA>
{
    typedef double     ReturnType;
    typedef uint8_t    ScalerType;
    typedef uint32_t   PriceType;
    static  const uint8_t defaultScaler = 0;
    static  double getConversionScaler( uint8_t priceScale )
    {
        return std::pow( 0.1, priceScale );
    }
};

typedef PriceScalerTraits<bb::SRC_ARCA> ArcaPriceScalerTraits;

} // namespace bb


#endif // BB_CLIENTCORE_ARCAUTILS_H

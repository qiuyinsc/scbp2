#ifndef BB_THIRDPARTY_SBE_ADMINHEARTBEAT12_HPP
#define BB_THIRDPARTY_SBE_ADMINHEARTBEAT12_HPP

/* Generated SBE (Simple Binary Encoding) message codec */

#if defined(SBE_HAVE_CMATH)
/* cmath needed for std::numeric_limits<double>::quiet_NaN() */
#  include <cmath>
#  define SBE_FLOAT_NAN std::numeric_limits<float>::quiet_NaN()
#  define SBE_DOUBLE_NAN std::numeric_limits<double>::quiet_NaN()
#else
/* math.h needed for NAN */
#  include <math.h>
#  define SBE_FLOAT_NAN NAN
#  define SBE_DOUBLE_NAN NAN
#endif

#include <sbe/sbe.hpp>

#include <sbe/LegSide.hpp>
#include <sbe/DecimalQty.hpp>
#include <sbe/MDUpdateAction.hpp>
#include <sbe/SecurityTradingStatus.hpp>
#include <sbe/SecurityTradingEvent.hpp>
#include <sbe/MatchEventIndicator.hpp>
#include <sbe/PRICE.hpp>
#include <sbe/MaturityMonthYear.hpp>
#include <sbe/HaltReason.hpp>
#include <sbe/EventType.hpp>
#include <sbe/PRICENULL.hpp>
#include <sbe/OpenCloseSettlFlag.hpp>
#include <sbe/MDEntryType.hpp>
#include <sbe/FLOAT.hpp>
#include <sbe/SecurityUpdateAction.hpp>
#include <sbe/PutOrCall.hpp>
#include <sbe/GroupSize.hpp>
#include <sbe/MDEntryTypeBook.hpp>
#include <sbe/GroupSize8Byte.hpp>
#include <sbe/MDEntryTypeStatistics.hpp>
#include <sbe/InstAttribValue.hpp>
#include <sbe/MDEntryTypeDailyStatistics.hpp>
#include <sbe/AggressorSide.hpp>
#include <sbe/SettlPriceType.hpp>

using namespace sbe;

namespace sbe {

class AdminHeartbeat12
{
private:
    char *buffer_;
    int bufferLength_;
    int *positionPtr_;
    int offset_;
    int position_;
    int actingBlockLength_;
    int actingVersion_;

    AdminHeartbeat12(const AdminHeartbeat12&) {}

public:

    static const sbe_uint16_t TemplateId = (sbe_uint16_t)12;

    AdminHeartbeat12(void) : buffer_(NULL), bufferLength_(0), offset_(0) {}

    static const sbe_uint16_t sbeBlockLength(void)
    {
        return (sbe_uint16_t)0;
    }

    static const sbe_uint16_t sbeTemplateId(void)
    {
        return (sbe_uint16_t)12;
    }

    static const sbe_uint16_t sbeSchemaId(void)
    {
        return (sbe_uint16_t)1;
    }

    static const sbe_uint16_t sbeSchemaVersion(void)
    {
        return (sbe_uint16_t)5;
    }

    static const char *sbeSemanticType(void)
    {
        return "0";
    }

    sbe_uint64_t offset(void) const
    {
        return offset_;
    }

    AdminHeartbeat12 &wrapForEncode(char *buffer, const int offset, const int bufferLength)
    {
        buffer_ = buffer;
        offset_ = offset;
        bufferLength_ = bufferLength;
        actingBlockLength_ = sbeBlockLength();
        actingVersion_ = sbeSchemaVersion();
        position(offset + actingBlockLength_);
        positionPtr_ = &position_;
        return *this;
    }

    AdminHeartbeat12 &wrapForDecode(char *buffer, const int offset, const int actingBlockLength, const int actingVersion,
                         const int bufferLength)
    {
        buffer_ = buffer;
        offset_ = offset;
        bufferLength_ = bufferLength;
        actingBlockLength_ = actingBlockLength;
        actingVersion_ = actingVersion;
        positionPtr_ = &position_;
        position(offset + actingBlockLength_);
        return *this;
    }

    sbe_uint64_t position(void) const
    {
        return position_;
    }

    void position(const sbe_uint64_t position)
    {
        if (SBE_BOUNDS_CHECK_EXPECT((position > bufferLength_), 0))
        {
            throw std::runtime_error("buffer too short [E100]");
        }
        position_ = position;
    }

    int size(void) const
    {
        return position() - offset_;
    }

    char *buffer(void)
    {
        return buffer_;
    }

    int actingVersion(void) const
    {
        return actingVersion_;
    }
};
}
#endif // BB_THIRDPARTY_SBE_ADMINHEARTBEAT12_HPP

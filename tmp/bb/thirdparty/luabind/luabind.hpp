#ifndef BB_THIRDPARTY_LUABIND_LUABIND_HPP
#define BB_THIRDPARTY_LUABIND_LUABIND_HPP

// Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved.


// ACR's version of luabind.hpp. We've set things up so that saying
//
// #include <luabind/luabind.hpp>
//
// will pick up this header, rather than the one from
// /usr/include/luabind. We do this because we want to globally
// enforce the inclusion of shared_ptr_converter.hpp whenver
// luabind.hpp is included.
//
// We also take the opportunity to enable the same sort of smart
// pointer conversions for boost::intrusive pointer.
//
// All the 'original' luabind.hpp file did was include the four other
// luabind headers grouped below, so this isn't particularly fragile.

// For the intrusive_ptr variant of get_const_holder below.
#include <boost/intrusive_ptr.hpp>
#include <boost/function.hpp>
#include <boost/bind.hpp>
#include <boost/mpl/vector.hpp>

// Define deduce_signature for boost::function so that luabind can use boost::function.
// For now it supports up to 3 arguments but it can easily be extended to support more.
// They have to be defined before including the luabind headers.
namespace luabind {
namespace detail {
template<class R, class A0>
boost::mpl::vector2<R, A0> deduce_signature(const boost::function<R (A0)>&, ...)
{
    return boost::mpl::vector2<R, A0>();
}

template<class R, class A0, class A1>
boost::mpl::vector3<R, A0, A1> deduce_signature(const boost::function<R (A0, A1)>&, ...)
{
    return boost::mpl::vector3<R, A0, A1>();
}

template<class R, class A0, class A1, class A2>
boost::mpl::vector4<R, A0, A1, A2> deduce_signature(const boost::function<R (A0, A1, A2)>&, ...)
{
    return boost::mpl::vector4<R, A0, A1, A2>();
}

}
}

// The headers included by the standard luabind.hpp
#include <luabind/class.hpp>
#include <luabind/config.hpp>
#include <luabind/function.hpp>
#include <luabind/open.hpp>

// The magic we really want to enforce on all includers of
// luabind/luabind.hpp is contained in this luabind file.
#include <luabind/shared_ptr_converter.hpp>

// Provide const-conversion correctness for boost::intrusive_ptr.
namespace luabind {
    template<class A>
    boost::intrusive_ptr<const A>*
    get_const_holder(boost::intrusive_ptr<A>*)
    { return 0; }
}

#endif // BB_THIRDPARTY_LUABIND_LUABIND_HPP

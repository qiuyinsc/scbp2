#ifndef BB_TRADING_IORDERVALIDATOR_H
#define BB_TRADING_IORDERVALIDATOR_H

/* Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved. */


#include <bb/trading/Order.h>

namespace bb {
namespace trading {

class IOrderValidator
{
public:
    virtual ~IOrderValidator() {}

    /// Returns true if desired_order_list as a whole is a valid order set under limits.
    /// Returns false if not.
    virtual bool validateDesiredOrderList(side_t side, const OrderList* desired_order_list) const = 0;

    /// Returns true if, given the actual issued orders we have out
    /// (from oissued_tracker), issuing this desired_order is allowed
    /// under limits.
    /// Returns false otherwise.
    virtual bool validateOrderBeforeIssue(side_t side, OrderCPtr desired_order) const = 0;

    /// If/when an order gets rejected, this is the human-readable reason.
    virtual const std::string& getLastReason() const = 0;
};
BB_DECLARE_SHARED_PTR( IOrderValidator );

} // trading
} // bb

#endif // BB_TRADING_IORDERVALIDATOR_H

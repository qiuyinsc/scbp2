#ifndef BB_TRADING_LATTERLADDERPOSITIONACCUMULATOR_H
#define BB_TRADING_LATTERLADDERPOSITIONACCUMULATOR_H

/* Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved. */


#include <bb/clientcore/L1MarketLevelProvider.h>
#include <bb/trading/PeriodicPositionAccumulator.h>

namespace bb {
BB_FWD_DECLARE_SHARED_PTR( IPriceProvider );
namespace trading {

/// This algorithm will place a set of orders repeatedly at fixed intervals
/// according to its configuration.
class LatterLadderPositionAccumulator : public PeriodicPositionAccumulator
{
public:
    BB_DECLARE_SCRIPTING();
    struct ParamsBase : PeriodicPositionAccumulator::ParamsBase
    {
        ParamsBase( const std::string& name )
            : PeriodicPositionAccumulator::ParamsBase( name )
            , m_stale_quote_tv( timeval_t::latest )
            , m_blast_is_enabled( true )
            , m_blast_max_spread_abs( std::numeric_limits<double>::max() )
            , m_blast_max_spread_perc( 1.0 )
            , m_max_order_sz( 1000 )
            , m_min_order_sz( 1 )
            , m_order_px_offset_abs( 0.0 )
            , m_order_px_offset_perc( std::numeric_limits<double>::infinity() )
            , m_ladder_accum_position_perc( 1.0 )
            , m_ladder_max_order_sz( 1000 )
            , m_ladder_min_order_sz( 1 )
            , m_ladder_mktdest( MKT_ISLD )
            , m_ladder_order_tif( TIF_IMMEDIATE_OR_CANCEL )
            , m_ladder_order_tif_timeout( 0 )
            , m_ladder_order_flags( 0 )
            , m_ladder_order_px_offset( 0.0 )
            , m_ladder_num_steps( 0 )
            , m_ladder_steppx_perc( 0.001 )
            , m_ladder_steppx_abs( 0.01 )
            , m_ladder_do_narrow_spread ( true )
            , m_ladder_back_off_px_offset( 0.0 )
            , m_cleanup_accum_position_sz( 0 )
            , m_favorable_by_abs_min(0.01)
            , m_favorable_by_abs_max(0.01)
            , m_favorable_by_perc_min(0.0)
            , m_favorable_by_perc_max(0.02)
            , m_unless_isld_nbbo_mkt(MKT_UNKNOWN)
        {}

        timeval_t   m_stale_quote_tv;
        bool        m_blast_is_enabled;
        double      m_blast_max_spread_abs;
        double      m_blast_max_spread_perc;
        uint32_t    m_max_order_sz;
        uint32_t    m_min_order_sz;
        double      m_order_px_offset_abs;
        double      m_order_px_offset_perc;
        double      m_ladder_accum_position_perc;
        uint32_t    m_ladder_max_order_sz;
        uint32_t    m_ladder_min_order_sz;
        mktdest_t   m_ladder_mktdest;
        tif_t       m_ladder_order_tif;
        uint32_t    m_ladder_order_tif_timeout;
        uint32_t    m_ladder_order_flags;
        double      m_ladder_order_px_offset;
        uint32_t    m_ladder_num_steps;
        double      m_ladder_steppx_perc;
        double      m_ladder_steppx_abs;
        bool        m_ladder_do_narrow_spread;

        /// How much to adjust your price level when you hit m_min_sell_px or
        /// m_max_buy_price.
        double      m_ladder_back_off_px_offset;

        // for cleanup of small positions
        uint32_t    m_cleanup_accum_position_sz; // if abs(pos) <= m_cleanup_accum_position_sz just blast it 100%

        // how favorable to mark price
        double      m_favorable_by_abs_min;
        double      m_favorable_by_abs_max;
        double      m_favorable_by_perc_min;
        double      m_favorable_by_perc_max;

        // alternate market dest unless nbbo is on isld
        mktdest_t   m_unless_isld_nbbo_mkt;
        IPriceProviderCPtr m_closing_price;

        virtual void printFields( std::ostream &out ) const;
    };

    struct Params : ParamsBase, IPositionAccumulatorParamsImpl<Params, LatterLadderPositionAccumulator>
    {
        Params() : ParamsBase( "LatterLadderPositionAccumulator" ) {}
    };

public:
    LatterLadderPositionAccumulator( const ParamsBase& params, const std::string& desc,
            const instrument_t& instr, const trading::TradingContextPtr& spTradingContext,
            const IPriceProviderCPtr& spPhase1PriceProvider, const IPriceProviderCPtr& spPhaseXPriceProvider,
            int32_t desired_size, double px_offset, bool htb = false );

private:
    virtual void generateOrders();
    ParamsBase                      m_latterLadderParams;
    L1MarketLevelProviderPtr        m_spMarketLevelProvider;
};
BB_DECLARE_SHARED_PTR( LatterLadderPositionAccumulator );

/// Stream operator to convert LatterLadderPositionAccumulator::Params into human readable form.
std::ostream &operator <<( std::ostream &out, const LatterLadderPositionAccumulator::Params &p );

} // namespace trading
} // namespace bb

#endif // BB_TRADING_LATTERLADDERPOSITIONACCUMULATOR_H

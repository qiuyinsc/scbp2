#ifndef BB_TRADING_CONSTANTREFDATAFACTORY_H
#define BB_TRADING_CONSTANTREFDATAFACTORY_H

/* Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved. */


#include <bb/trading/IRefDataFactory.h>

namespace bb {
namespace trading {

// The ConstantRefDataFactory implements IRefDataFactory by cloning
// the prototype RefData with which it is constructed. In essence, it
// is a substitute for the old setDefaultReferenceData mechanism in
// TradingContext. If you look at the current implementation of
// TradingContext::setDefaultReferenceData, you can see that it simply
// constructs a ConstantRefDataFactory with the provided RefData.

class ConstantRefDataFactory : public IRefDataFactory
{
public:
    // The factory will return clones of refData. Please note that
    // this call also clones the provided refData, so any subsequent
    // modifications to the refData provided as an argument here will
    // not be reflected in the values returned by createRefData below.
    ConstantRefDataFactory( const RefDataCPtr& refData );

    virtual ~ConstantRefDataFactory();

    virtual RefDataPtr createRefData(
        TradingContext* tradingContext,
        const instrument_t& instr );

private:
    const RefDataCPtr m_refData;
};
BB_DECLARE_SHARED_PTR(ConstantRefDataFactory);

} // namespace trading
} // namespace bb

#endif // BB_TRADING_CONSTANTREFDATAFACTORY_H

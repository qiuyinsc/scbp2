#ifndef BB_TRADING_SIMPLEPOSITIONPROVIDER_H
#define BB_TRADING_SIMPLEPOSITIONPROVIDER_H

/* Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved. */


#include <bb/core/hash_map.h>
#include <bb/trading/trading.h>
#include <bb/trading/PositionTrackerFactory.h>
#include <bb/trading/IPositionProvider.h>

namespace bb {
namespace trading {

class SimplePositionProvider : public bb::trading::IOTPositionProvider
{
public:
    SimplePositionProvider()
        : IOTPositionProvider( bb::instrument_t::none() )
        , m_baselinePosition( 0 )
        , m_absolutePosition( 0 )
        , m_effectivePosition( 0 ) {}

    SimplePositionProvider( int32_t abs_pos, int32_t baseline = 0 )
        : IOTPositionProvider( bb::instrument_t::none() )
        , m_baselinePosition( baseline )
        , m_absolutePosition( abs_pos )
        , m_effectivePosition( abs_pos - baseline ) {}

    SimplePositionProvider( int32_t abs_pos, int32_t baseline, int32_t eff_pos )
        : IOTPositionProvider( bb::instrument_t::none() )
        , m_baselinePosition( baseline )
        , m_absolutePosition( abs_pos )
        , m_effectivePosition( eff_pos ) {}

    SimplePositionProvider( const instrument_t& instr )
        : IOTPositionProvider( instr )
        , m_baselinePosition( 0 )
        , m_absolutePosition( 0 )
        , m_effectivePosition( 0 ) {}

    virtual bool isReady() const { return true; }
    virtual const instrument_t& getInstrument() const
    {
        if( IOTPositionProvider::getInstrument() == instrument_t::none() )
            throw std::logic_error( "SimplePositionProvider::getInstrument: not implemented" );
        else
            return IOTPositionProvider::getInstrument();
    }
    virtual int32_t getEffectivePosition() const { return m_effectivePosition; }
    virtual int32_t getAbsolutePosition() const { return m_absolutePosition; }

    virtual acct_t getAcct() const { return ACCT_UNKNOWN; }

    // sets the absolute position, and adjusts everything accordingly
    void setPosition( int32_t abs_pos, int32_t baseline = 0 )
    {
        setPosition( abs_pos, baseline, abs_pos - baseline );
    }

    virtual void setPosition( int32_t abs_pos, int32_t baseline, int32_t eff_pos )
    {
        m_baselinePosition = baseline;
        m_absolutePosition = abs_pos;
        m_effectivePosition = eff_pos;
        notifyListeners();
    }

    virtual void handleFill( const timeval_t& tv, side_t side, double fill_px, uint32_t fill_qty,
        const boost::optional<int32_t>& posFromMsg)
    {
        int32_t s = side2sign( side );
        m_absolutePosition += s * fill_qty;
        m_effectivePosition += s * fill_qty;
        notifyListeners();
    }

protected:
    int32_t m_baselinePosition, m_absolutePosition, m_effectivePosition;
};

BB_DECLARE_SHARED_PTR( SimplePositionProvider );

class SimplePositionProviderFactory : public IPositionProviderFactory
{
public:
    virtual void init( bb::trading::TradingContext* tc ) {}
    virtual void exit() {}

    /// Obtains a IPositionProvider for instr.
    /// Please do not call directly, TradingContext will handle this!
    /// Will not be called twice for the same instr.
    virtual IOTPositionProviderPtr createInternal( ITraderPtr baseTrader, TradingContext *tradingContext,
            const bb::instrument_t& instr )
    {
        return getSimplePositionProvider( instr );
    }

    virtual IPositionProviderPtr getPositionProvider( const bb::instrument_t& instr )
    {
        return getSimplePositionProvider( instr );
    }

    virtual SimplePositionProviderPtr getSimplePositionProvider( const bb::instrument_t& instr )
    {
        if( !m_positionProviders[instr] )
            m_positionProviders[instr].reset( new SimplePositionProvider() );
        return m_positionProviders[instr];
    }

protected:
    bbext::hash_map<instrument_t,SimplePositionProviderPtr> m_positionProviders;
};

BB_DECLARE_SHARED_PTR( SimplePositionProviderFactory );

} // trading
} // bb

#endif // BB_TRADING_SIMPLEPOSITIONPROVIDER_H

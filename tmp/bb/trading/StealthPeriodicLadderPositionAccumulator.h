#ifndef BB_TRADING_STEALTHPERIODICLADDERPOSITIONACCUMULATOR_H
#define BB_TRADING_STEALTHPERIODICLADDERPOSITIONACCUMULATOR_H

/* Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved. */


#include <bb/trading/PeriodicLadderPositionAccumulator.h>

namespace bb {
namespace trading {

/// This algorithm will place a set of orders repeatedly at fixed intervals
/// according to its configuration.
class StealthPeriodicLadderPositionAccumulator : public PeriodicLadderPositionAccumulator
{
public:
    BB_DECLARE_SCRIPTING();

    struct ParamsBase : PeriodicLadderPositionAccumulator::ParamsBase
    {
        ParamsBase( const std::string& name )
            : PeriodicLadderPositionAccumulator::ParamsBase( name )
        {
        }

        struct LadderOrderParams
        {
            LadderOrderParams()
                : m_mktdest( MKT_ISLD )
                , m_tif( TIF_SPECIFIED )
                , m_timeout( 0 )
                , m_flags( 0 )
            {}

            mktdest_t   m_mktdest;
            tif_t       m_tif;
            uint32_t    m_timeout;
            uint32_t    m_flags;
        };

        std::vector<LadderOrderParams> m_ladder_order_params;

        virtual void printFields( std::ostream &out ) const;
    };

    struct Params : ParamsBase, IPositionAccumulatorParamsImpl<Params, StealthPeriodicLadderPositionAccumulator>
    {
        Params() : ParamsBase( "StealthPeriodicLadderPositionAccumulator" ) {}
    };

    StealthPeriodicLadderPositionAccumulator( const ParamsBase& params, const std::string& desc,
            const instrument_t& instr, const trading::TradingContextPtr& spTradingContext,
            const IPriceProviderCPtr& spPhase1PriceProvider, const IPriceProviderCPtr& spPhaseXPriceProvider,
            int32_t desired_size, double px_offset, bool htb = false );

protected:
    virtual void generateOrders();

    ParamsBase        m_params; // base class has a sliced copy of this, which is a bit inefficient
};
BB_DECLARE_SHARED_PTR( StealthPeriodicLadderPositionAccumulator );

/// Stream operator to convert StealthPeriodicLadderPositionAccumulator::Params into human readable form.
std::ostream &operator <<( std::ostream &out, const StealthPeriodicLadderPositionAccumulator::Params &p );

} // namespace trading
} // namespace bb

#endif // BB_TRADING_STEALTHPERIODICLADDERPOSITIONACCUMULATOR_H

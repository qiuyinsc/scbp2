#ifndef BB_TRADING_MANUALBIASADJUSTER_H
#define BB_TRADING_MANUALBIASADJUSTER_H

/* Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved. */


#include <bb/core/smart_ptr.h>
#include <bb/trading/IBiasAdjuster.h>

namespace bb {
namespace trading {

class ManualBiasAdjuster : public IBiasAdjuster
{
public:
    ManualBiasAdjuster() : m_bias( 0 ) {}
    void set( double v )
    {
        if( m_bias != v )
        {
            m_bias = v;
            notifyChange();
        }
    }
    virtual double getBiasAdjustment() const { return m_bias; }

protected:
    double m_bias;
};
BB_DECLARE_SHARED_PTR(ManualBiasAdjuster);

} // namespace trading
} // namespace bb

#endif // BB_TRADING_MANUALBIASADJUSTER_H

#ifndef BB_TRADING_POSITIONTRACKER_H
#define BB_TRADING_POSITIONTRACKER_H

/* Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved. */

#include <string>
#include <utility>

#include <boost/optional.hpp>

#include <bb/core/source.h>
#include <bb/core/instrument.h>
#include <bb/core/hash_map.h>

#include <bb/trading/IFillListener.h>
#include <bb/trading/IPositionProvider.h>
#include <bb/trading/ITrader.h>
#include <bb/trading/trading.h>
#include <bb/trading/TradingContext.h>

namespace bb {

class MarginInstrumentInfoMsg;
class TdTradeLimitsMsg;
BB_FWD_DECLARE_SHARED_PTR( EventDistributor );
BB_FWD_DECLARE_SHARED_PTR( IAlert );
BB_FWD_DECLARE_SHARED_PTR( ITimeProvider );
BB_FWD_DECLARE_SHARED_PTR( MsgHandler );

namespace trading {

/// PositionTracker keeps track of the current position for a given instrument and account
///
/// The PositionTracker needs to send a request to the trade server, so it implements
/// ITDConnectionListener to know when the trade server is connected. It does not add itself
/// as a listener directly, the caller must do that and pass in the subscription using
/// setListenerSub.
class PositionTracker
    : public IOTPositionProvider
    , public ITDConnectionListener
{
public:
    enum TradingContextMode
    {
        TRADING_LIVE = 0,
        TRADING_HIST = 1,
        TRADING_UNKNOWN = 2
    };
    /// The source parameter must be valid for market data to be requested from the MStreamManager.
    /// It should only be boost::none for historical simulations.
    PositionTracker(
        boost::optional<source_t> infoSource,
        const instrument_t& instrument,
        acct_t account,
        const EventDistributorPtr &eventDist,
        const IAlertPtr& alert,
        const TradingContextMode mode);
    virtual ~PositionTracker();
    void setListenerSub( const Subscription& l ) { m_listenerSub = l; }

    acct_t getAcct() const { return m_acct; }

    virtual bool isReady() const;

    virtual int32_t getAbsolutePosition() const { return m_baseline_pos + m_relative_pos + m_pending_pos; }

    /// returns current relative position for instrument, where relative means our
    /// position after subtracting off baseline position
    virtual int32_t getEffectivePosition() const { return m_relative_pos + m_pending_pos; }

    /// By default, returns the timeval of the last position change (either from fill
    /// or margin position difference). If include_margin_update_changes is false, will return
    /// timeval of last time onFill was called, ignoring any changes that came from
    /// broadcast position differences
    timeval_t lastPositionChangeTv(bool include_margin_update_changes = true) const;

    /// IOTPositionProvider impl
    /// When SHFE sends DONE message before the final FILL message, IssuedOrderTracker will call this function.
    virtual void setPendingPosition( int32_t p, double fill_px ) { m_pending_pos = p; m_pending_fill_px = fill_px; }
    virtual int32_t getPendingPosition() const { return m_pending_pos; }
    virtual double getPendingFillPrice() const { return m_pending_fill_px; }

    virtual TradingLimitPair getTradingLimit(const std::string& limit_id) const;

    // IOTPositionProvider impl
    virtual void handleFill( const timeval_t& tv, side_t side, double fill_px, uint32_t fill_qty,
            const boost::optional<int32_t>& posFromMsg);

    /// updates our position based on broadcast info. this is public for unit-testing.
    bool handlePositionBroadcast( const timeval_t& time_sent, int32_t broadcast_pos, int32_t broadcast_baseline );

    // Returns whether we auto-activate position broadcasts upon trade-server connection.
    bool getActivatePositionBroadcasts() const
    {   return m_activate_position_broadcasts; }
    // Sets whether we auto-activate position broadcasts upon trade-server connection.
    void setActivatePositionBroadcasts( bool activate )
    {   m_activate_position_broadcasts = activate; }

    /// ITDConnectionListener impl
    virtual void onTradeServerConnected( IPositionBroadcastRequesterPtr requester );
    virtual void onTradeServerDisconnected() {}
    int getVerbose() const;
    void setVerbose(int) ;

protected:
    void handleMarginInstrumentInfoMsg( const MarginInstrumentInfoMsg& msg );
    void handleTdTradeLimitsMsg(const TdTradeLimitsMsg& msg);

    /// Alters our position to match broadcast position when they don't match.
    /// might need to be undone if a Fill quickly follows.
    /// Returns the amount of the position adjustment.
    virtual int32_t handlePositionDiscrepancy( const timeval_t& time_sent, int32_t broadcast_pos );

    /// saves undo info in case a fill or another MarginInstrumentInfoMsg
    /// comes in within a short period of when position was changed
    /// because of broadcast position discrepancy.
    virtual void rememberPositionDiscrepancyUndoInfo( const timeval_t& time_sent, int32_t diff );

    acct_t m_acct;
    IAlertPtr m_alert;
    const TradingContextMode m_tradingMode;
    MsgHandlerPtr m_marginHandlerRef;
    MsgHandlerPtr m_tdLimitHandlerRef;

    bool m_positionBroadcastReceived;
    int32_t m_baseline_pos;
    int32_t m_relative_pos;

    /// support IOT handling of SHFE DONE message
    int32_t m_pending_pos;
    double m_pending_fill_px;

    timeval_t m_last_fill_tv[2];

    // vars for keeping position synced with broadcasts
    timeval_t m_margin_poschange_tv;
    int32_t m_margin_deltapos;
    bool m_activate_position_broadcasts;

    Subscription m_listenerSub;
    Subscription m_fillListenerSub;

    static const int32_t MARGIN_SYNC_DELAY_SEC;

    typedef bbext::hash_map<std::string, TradingLimitPair> TradingLimitsMap;
    TradingLimitsMap m_tradingLimitsMap;

    int m_verbose;

    friend class PositionTrackerTest;
};
BB_DECLARE_SHARED_PTR( PositionTracker );

} // namespace trading
} // namespace bb

#endif // BB_TRADING_POSITIONTRACKER_H

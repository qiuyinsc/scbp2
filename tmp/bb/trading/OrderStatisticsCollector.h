#ifndef BB_TRADING_ORDERSTATISTICSCOLLECTOR_H
#define BB_TRADING_ORDERSTATISTICSCOLLECTOR_H

/* Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved. */


#include <iosfwd>
#include <string>

#include <bb/core/instrument.h>
#include <bb/core/timeval.h>

#include <bb/io/XmlLog.h>

#include <bb/trading/IssuedOrderTracker.h>
#include <bb/trading/IOrderStatusListener.h>
#include <bb/trading/trading.h>

namespace bb {
namespace trading {

class OrderInfo;
class OrderStatisticsCollector : public IOrderStatusListener
{
public:
    OrderStatisticsCollector( const TradingContextPtr& tc
        , const bb::instrument_t& _instr
        , const bb::date_t& date = bb::date_t::today() );
    OrderStatisticsCollector( const TradingContextPtr& tc
        , const bb::instrument_t& _instr
        , std::ostream &os
        , ITimeProviderPtr tp );
    virtual ~OrderStatisticsCollector();

    /// connects status listener and fill listener
    void init();

    /// returns the log filename
    const std::string& getFilename() const { return filename; }

    class OrderInfo
    {
    public:
        OrderInfo( uint32_t id );

        uint32_t orderid;
        bb::timeval_t sent_time;
        bb::timeval_t confirm_time;
        bb::timeval_t cancel_time;
        bb::timeval_t done_time;
        bb::timeval_t first_fill_time;
        uint32_t num_fills;
        uint32_t fill_size;
        uint32_t order_size;
    };

protected:
    virtual void onFill( const bb::trading::FillInfo& info );
    virtual void onOrderStatusChange( const OrderPtr& order, const ChangeFlags &flags );

    void write( const OrderInfo* info );
    const bb::timeval_t now();

private:
    bb::instrument_t instr;
    bb::trading::TradingContextPtr tradingContext;
    typedef bbext::hash_map<uint32_t,OrderInfo*> OrderInfoTable;
    OrderInfoTable orderTable;
    std::string filename;
    boost::scoped_ptr<std::ostream> outfile;
    bb::XmlLog log;
    Subscription m_sub;
    ITimeProviderPtr m_timeProvider;
};

} // trading
} // bb

#endif // BB_TRADING_ORDERSTATISTICSCOLLECTOR_H

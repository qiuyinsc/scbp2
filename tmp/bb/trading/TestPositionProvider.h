#ifndef BB_TRADING_TESTPOSITIONPROVIDER_H
#define BB_TRADING_TESTPOSITIONPROVIDER_H

/* Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved. */


#include <bb/trading/SimplePositionProvider.h>

namespace bb {
namespace trading {

class TestPositionProvider : public bb::trading::SimplePositionProvider
{
public:
    TestPositionProvider() {}
    TestPositionProvider( const instrument_t& instr )
        : SimplePositionProvider( instr ) {}
    TestPositionProvider( int32_t abs_pos, int32_t baseline = 0 )
        : SimplePositionProvider( abs_pos, baseline ) {}
    TestPositionProvider( int32_t abs_pos, int32_t baseline, int32_t eff_pos )
        : SimplePositionProvider( abs_pos, baseline, eff_pos ) {}
};

BB_DECLARE_SHARED_PTR( TestPositionProvider );

class TestPositionProviderFactory : public IPositionProviderFactory
{
public:
    virtual void init( bb::trading::TradingContext* tc ) {}
    virtual void exit() {}

    /// Obtains a IPositionProvider for instr.
    /// Please do not call directly, TradingContext will handle this!
    /// Will not be called twice for the same instr.
    virtual IOTPositionProviderPtr createInternal( ITraderPtr baseTrader, TradingContext* tradingContext,
            const bb::instrument_t& instr )
    {
        return getTestPositionProvider( instr );
    }

    virtual IPositionProviderPtr getPositionProvider( const bb::instrument_t& instr )
    {
        return getTestPositionProvider( instr );
    }

    virtual TestPositionProviderPtr getTestPositionProvider( const bb::instrument_t& instr )
    {
        if( !m_positionProviders[instr] )
            m_positionProviders[instr].reset( new TestPositionProvider( instr ) );
        return m_positionProviders[instr];
    }

protected:
    bbext::hash_map<instrument_t,TestPositionProviderPtr> m_positionProviders;
};

BB_DECLARE_SHARED_PTR( TestPositionProviderFactory );

} // trading
} // bb

#endif // BB_TRADING_TESTPOSITIONPROVIDER_H

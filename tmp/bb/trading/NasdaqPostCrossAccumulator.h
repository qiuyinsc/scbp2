#ifndef BB_TRADING_NASDAQPOSTCROSSACCUMULATOR_H
#define BB_TRADING_NASDAQPOSTCROSSACCUMULATOR_H

/* Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved. */


#include <bb/core/Log.h>
#include <bb/core/Scripting.h>

#include <bb/clientcore/ConstantPriceProvider.h>
#include <bb/clientcore/MsgHandler.h>

#include <bb/trading/IPositionProvider.h>
#include <bb/trading/IPositionAccumulator.h>

namespace bb {
namespace trading {

/// This algorithm is triggered by the cross trade, and uses the
/// cross price to drive another accumulator.
class NasdaqPostCrossAccumulator : public PositionAccumulatorImpl
{
public:
    BB_DECLARE_SCRIPTING();

    struct Params : public IPositionAccumulatorParamsImpl<Params, NasdaqPostCrossAccumulator>
    {
        Params()
            : m_cross_trade_px_abs_limit(999999.99)
            , m_cross_type(0)
            , m_algo()
            , m_name( "NasdaqPostCrossAccumulator" )
        {}

        double                          m_cross_trade_px_abs_limit;
        char                            m_cross_type;
        IPositionAccumulatorParamsPtr   m_algo;
        std::string                     m_name;
    };

public:
    NasdaqPostCrossAccumulator( const Params& params, const std::string& desc, const instrument_t& instr,
                                const trading::TradingContextPtr& spTradingContext,
                                const IPriceProviderCPtr& spPhase1PriceProvider,
                                const IPriceProviderCPtr& spPhaseXPriceProvider,
                        int32_t desired_size, double px_offset, bool htb = false );

    virtual ~NasdaqPostCrossAccumulator();

    const instrument_t& getInstrument() const   { return m_instr; }

    void setDesiredPosition( int32_t desired_sz ) { m_spAlgo->setDesiredPosition(desired_sz); }
    int32_t getDesiredPosition() const            { return m_spAlgo->getDesiredPosition(); }

    int32_t getActualPosition() const             { return m_spAlgo->getActualPosition(); }

    void setActive( bool active );
    bool isActive() const                       { return m_active; }

    bool isCompleted() const                    { return false; }

    void setPriceOffset( double px_offset ) { m_spAlgo->setPriceOffset(px_offset); }
    double getPriceOffset() const           { return m_spAlgo->getPriceOffset(); }

    void setMaxBuyPrice( double px )        { m_spAlgo->setMaxBuyPrice(px); }
    double getMaxBuyPrice() const           { return m_spAlgo->getMaxBuyPrice(); }
    void setMinSellPrice( double px )       { m_spAlgo->setMinSellPrice(px); }
    double getMinSellPrice() const          { return m_spAlgo->getMinSellPrice(); }

protected:
    template<typename IsldCrossTradeMsgT>
    void onIsldCrossTrade( const IsldCrossTradeMsgT& cross_msg );

private:
    Params                    m_params;
    std::string               m_desc;
    instrument_t              m_instr;
    TradingContextPtr         m_spTradingContext;

    bool                      m_active;
    bool                      m_cross_occured;

    ConstantPriceProviderPtr  m_spCrossPriceProvider;
    IPositionAccumulatorPtr   m_spAlgo;

    MsgHandlerPtr             m_spCrossTradeMsgHandler, m_spCrossTradeOldMsgHandler;
};
BB_DECLARE_SHARED_PTR( NasdaqPostCrossAccumulator );


template<typename IsldCrossTradeMsgT>
void NasdaqPostCrossAccumulator::onIsldCrossTrade( const IsldCrossTradeMsgT& cross_msg )
{
    if ( m_params.m_cross_type && m_params.m_cross_type != cross_msg.getCtype() )
        return;

    double cross_trade_px = cross_msg.getPx();
    if ( bb::EQZ(cross_trade_px) )
    {
        TRD_LOG_WARN << m_desc << ' ' << "cannot trade because cross_trade.px == 0" << bb::endl;
        return;
    }

    m_spCrossPriceProvider->setPrice(cross_trade_px, cross_msg.getIsldTime() );
    m_cross_occured = true;
    if ( m_active )
        m_spAlgo->setActive(true);
}


/// Stream operator to convert NasdaqPostCrossAccumulator::Params into human readable form.
std::ostream &operator <<( std::ostream &out, const NasdaqPostCrossAccumulator::Params &p );

} // namespace trading
} // namespace bb

#endif // BB_TRADING_NASDAQPOSTCROSSACCUMULATOR_H

#ifndef BB_TRADING_PORTFOLIOPOSITIONTRACKERFACTORY_H
#define BB_TRADING_PORTFOLIOPOSITIONTRACKERFACTORY_H

/* Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved. */


#include <bb/core/hash.h>
#include <bb/trading/IPositionProviderFactory.h>

namespace bb {
namespace trading {

BB_FWD_DECLARE_SHARED_PTR(PortfolioPositionTracker);

/** creates PortfolioPositionTrackers */
class PortfolioPositionTrackerFactory : public IPositionProviderFactory
{
public:
    /// @param( _src )  SRC_PORTFOLIO_SERVER.ORIG_XXX.DEST_XXX
    PortfolioPositionTrackerFactory( bb::source_t _src, boost::optional<source_t> infoSource,
            bool activeByDefault, int maxPosition )
        : m_effectivePositionSource( _src )
        , m_infoSource( infoSource )
        , m_activeByDefault( activeByDefault )
        , m_maxPosition( maxPosition )
    {
    }

    IOTPositionProviderPtr createInternal( ITraderPtr baseTrader, TradingContext *tradingContext,
            const bb::instrument_t& instr );

private:
    bb::source_t m_effectivePositionSource;
    boost::optional<bb::source_t> m_infoSource;
    bool m_activeByDefault;
    int m_maxPosition;
};
BB_DECLARE_SHARED_PTR(PortfolioPositionTrackerFactory);

} // namespace bb
} // namespace trading

#endif // BB_TRADING_PORTFOLIOPOSITIONTRACKERFACTORY_H

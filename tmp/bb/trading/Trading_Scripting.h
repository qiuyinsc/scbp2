#ifndef BB_TRADING_TRADING_SCRIPTING_H
#define BB_TRADING_TRADING_SCRIPTING_H

/* Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved. */


struct lua_State;

namespace bb {
namespace trading {

/// Registers the clientcore library with ScriptManager.
/// The library can be loaded from C++ with ScriptManager::loadLibary( "trading" )
/// or from Lua with loadLibary( "trading" ).
void registerScripting();

// Injects the libtrading bindings directly into the state (bypassing ScriptManager).
bool register_libtrading( lua_State& L );

} // trading
} // bb

#endif // BB_TRADING_TRADING_SCRIPTING_H

#ifndef BB_TRADING_FILLPARTICIPATIONCOLLECTOR_H
#define BB_TRADING_FILLPARTICIPATIONCOLLECTOR_H

/* Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved. */


#include <iosfwd>
#include <bb/trading/trading.h>
#include <bb/trading/IOrderStatusListener.h>
#include <bb/clientcore/TickProvider.h>
#include <bb/clientcore/IBook.h>
#include <bb/core/instrument.h>
#include <bb/core/side.h>
#include <bb/core/bbint.h>
#include <bb/core/smart_ptr.h>
#include <boost/static_assert.hpp>
#include <bb/core/Subscription.h>
#include <tr1/array>
#include <bb/clientcore/PeriodicWakeup.h>
#include <bb/trading/IVolumeProvider.h>
namespace bb {
namespace trading {
struct MarketVolumeProvider:IVolumeProvider
,protected bb::IBookListener
,protected bb::ITickListener
{

    MarketVolumeProvider(bb::ITickProviderPtr const&,IBookCPtr const&);
    bb::instrument_t          getInstrument() const;
    virtual ~MarketVolumeProvider();

    TradingVolumeShares       getShares(bb::side_t )const;
    TradingVolumeDollars      getDollars(bb::side_t )const;
    void print(std::ostream & )const;
private:
    void onTickReceived(  bb::ITickProvider const* tp,  bb::TradeTick const& tick );
    void onBookChanged(  IBook const* pBook,  Msg const* pMsg,int32_t bidLevelChanged, int32_t askLevelChanged );
    bb::ITickProviderPtr m_ticks;
    IBookCPtr m_book;
    bb::Subscription m_ticksub;
    bool     m_lastguess;
    bool     m_priceOK;
    double     m_lastmidpx;
    typedef std::tr1::array<TradingVolumeShares,2>    tradingvol_arrtype;
    typedef std::tr1::array<TradingVolumeDollars,2>   dollarvol_arrtype;
    tradingvol_arrtype m_shares;
    dollarvol_arrtype  m_dollars;
    BOOST_STATIC_ASSERT(bb::ASK <sizeof(tradingvol_arrtype)/sizeof(tradingvol_arrtype::value_type));
    BOOST_STATIC_ASSERT(bb::BID <sizeof(tradingvol_arrtype)/sizeof(tradingvol_arrtype::value_type));
    BOOST_STATIC_ASSERT(bb::ASK <sizeof(dollarvol_arrtype)/sizeof(dollarvol_arrtype::value_type));
    BOOST_STATIC_ASSERT(bb::BID <sizeof(dollarvol_arrtype)/sizeof(dollarvol_arrtype::value_type));
};
BB_DECLARE_SHARED_PTR(MarketVolumeProvider);
struct FillVolumeProvider:IVolumeProvider
,trading::IOrderStatusListener
, bb::SubscriptionKeeper
{

    FillVolumeProvider(bb::instrument_t const&);

    bb::instrument_t          getInstrument() const;
    virtual ~FillVolumeProvider();

    TradingVolumeShares       getShares(bb::side_t )const;
    TradingVolumeDollars      getDollars(bb::side_t )const;
    void print(std::ostream & )const;
private:
    virtual void onFill( const FillInfo & info );
    void onOrderStatusChange(trading::OrderPtr const& order,ChangeFlags const&flags);

    bb::instrument_t m_instr;
    typedef std::tr1::array<TradingVolumeShares,2>    tradingvol_arrtype;
    typedef std::tr1::array<TradingVolumeDollars,2>   dollarvol_arrtype;
    tradingvol_arrtype m_shares;
    dollarvol_arrtype  m_dollars;
    BOOST_STATIC_ASSERT(bb::ASK <sizeof(tradingvol_arrtype)/sizeof(tradingvol_arrtype::value_type));
    BOOST_STATIC_ASSERT(bb::BID <sizeof(tradingvol_arrtype)/sizeof(tradingvol_arrtype::value_type));
    BOOST_STATIC_ASSERT(bb::ASK <sizeof(dollarvol_arrtype)/sizeof(dollarvol_arrtype::value_type));
    BOOST_STATIC_ASSERT(bb::BID <sizeof(dollarvol_arrtype)/sizeof(dollarvol_arrtype::value_type));

};
BB_DECLARE_SHARED_PTR(FillVolumeProvider);

struct FillParticipationStatsCollector:protected PeriodicWakeupActive{

    FillParticipationStatsCollector(IVolumeProviderCPtr const&fills,IVolumeProviderCPtr const&mkt,ClockMonitor *cm,PeriodicWakeupActiveSpec const&);

private:
    void onPeriodicWakeup( bb::timeval_t const&ctv,  bb::timeval_t const&swtv);
    IVolumeProviderCPtr m_fills;
    IVolumeProviderCPtr m_mkt;

};
BB_DECLARE_SHARED_PTR(FillParticipationStatsCollector);

} // trading
} // bb

#endif // BB_TRADING_FILLPARTICIPATIONCOLLECTOR_H

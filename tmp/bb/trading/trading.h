#ifndef BB_TRADING_TRADING_H
#define BB_TRADING_TRADING_H

/* Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved. */


#include <bb/core/Priority.h>

namespace bb {
namespace trading {

// Event priorities.
// Beware: IssuedOrderTracker assumes in TDMessageAcker that all trading code
// (even strategy-specific stuff) runs before PRIORITY_TRADING_LOWEST.
extern const Priority PRIORITY_TRADING_HIGHEST;
extern const Priority PRIORITY_TRADING_DEFAULT;
extern const Priority PRIORITY_TRADING_LOWEST;

/// The maximum size of a single order going to the trade demon.
/// The current default is 250,000.
static const uint32_t MAX_TD_ORDER_SIZE = 250000;

} // namespace trading
} // namespace bb

#endif // BB_TRADING_TRADING_H

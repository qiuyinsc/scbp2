#ifndef BB_TRADING_IREFDATAFACTORY_H
#define BB_TRADING_IREFDATAFACTORY_H

/* Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved. */


#include <boost/noncopyable.hpp>
#include <bb/core/smart_ptr.h>

namespace bb {

class instrument_t;

namespace trading {

BB_FWD_DECLARE_SHARED_PTR( RefData );
class TradingContext;

// IRefDataFactory is an abstract base class that abstracts away the
// details of constructing the appropriate RefData for a given
// instrument in a TradingContext.
//
// See TradingContext::setRefDataFactory for how to inject an
// implementation of IRefDataFactory into a trading context.
//
// You do not need to use an IRefDataFactory in your
// TradingContext. If you do not provide one, you must explicitly call
// TradingContext::setRefData for each instrument in the trading
// context. You may set an IRefDataFactory, and also explicitly set
// RefData for specific instruments. The IRefDataFactory
// implementation will only be used to construct RefData when non has
// already been constructed for the instrument.
//
// There are several existing implementations of IRefDataFactory,
// specifically the 'ConstantRefDataFactory', which will always return
// a copy of a particular instance of RefData, and
// LegacyRefDataFactory, which will always return a RefData that
// provides the same defaults as the old default reference data
// implementation in TradingContext.

class IRefDataFactory : boost::noncopyable
{
public:
    virtual ~IRefDataFactory();

    // Override this in subclasses to construct the appropriate
    // RefData for the given instrument and trading context.
    virtual RefDataPtr createRefData(
        TradingContext* tradingContext,
        const instrument_t& instr ) = 0;
};
BB_DECLARE_SHARED_PTR(IRefDataFactory);

} // namespace trading
} // namespace bb

#endif // BB_TRADING_IREFDATAFACTORY_H

#ifndef BB_TRADING_PERIODICPOSITIONACCUMULATOR_H
#define BB_TRADING_PERIODICPOSITIONACCUMULATOR_H

/* Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved. */


#include <bb/trading/OneShotPositionAccumulator.h>

namespace bb {
namespace trading {

/// This algorithm will place a set of orders repeatedly at fixed intervals
/// according to its configuration.
class PeriodicPositionAccumulator : public OneShotPositionAccumulator
{
public:
    BB_DECLARE_SCRIPTING();

    struct ParamsBase : OneShotPositionAccumulator::ParamsBase
    {
        ParamsBase( const std::string& name )
            : OneShotPositionAccumulator::ParamsBase( name )
            , m_period_secs( std::numeric_limits<double>::max() )
            , m_period_secs_random_offset( 0.0 )
            , m_max_periods( 1 )
        {}

        double m_period_secs; // how often to place orders
        double m_period_secs_random_offset;
        uint32_t m_max_periods;

        virtual void printFields( std::ostream &out ) const;
    };

    struct Params : ParamsBase, IPositionAccumulatorParamsImpl<Params, PeriodicPositionAccumulator>
    {
        Params() : ParamsBase( "PeriodicPositionAccumulator" ) {}
    };

    BB_DECLARE_SHARED_PTR( Params );
public:
    PeriodicPositionAccumulator( const ParamsBase& params, const std::string& desc,
            const instrument_t& instr, const trading::TradingContextPtr& spTradingContext,
            const IPriceProviderCPtr& spPhase1PriceProvider, const IPriceProviderCPtr& spPhaseXPriceProvider,
            int32_t desired_size, double px_offset, bool htb = false );

    void setActive( bool active );
    bool isCompleted() const                     { return m_bCompleted; }
    virtual int32_t getDesiredPosition() const;
    virtual void setDesiredPosition(int32_t);

private:
    void setNextWakeup();
    void wakeup();

    ParamsBase      m_params; // base class has a sliced copy of this, which is a bit inefficient

    int32_t         m_orig_desired_pos;
    Subscription    m_wakeupSub;
    timeval_t       m_nextWakeupTime;
    uint32_t        m_periods;
    bool            m_bCompleted;
    dir_t           m_targetDir;
};
BB_DECLARE_SHARED_PTR( PeriodicPositionAccumulator );

/// Stream operator to convert PeriodicPositionAccumulator::Params into human readable form.
std::ostream &operator <<( std::ostream &out, const PeriodicPositionAccumulator::Params &p );

} // namespace trading
} // namespace bb

#endif // BB_TRADING_PERIODICPOSITIONACCUMULATOR_H

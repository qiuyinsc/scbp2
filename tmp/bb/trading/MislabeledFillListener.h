#ifndef BB_TRADING_MISLABELEDFILLLISTENER_H
#define BB_TRADING_MISLABELEDFILLLISTENER_H

/* Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved. */

#include <bb/trading/FillInfo.h>
#include <bb/trading/IssuedOrderTracker.h>
#include <bb/trading/IPositionProvider.h>

namespace bb {
namespace trading {

///
/// The onMislabeledFill() method is invoked whenever a fill not matching the
/// order's sell/short designation arrives.
///
class MislabeledFillListener : protected IFillListener
{
public:
    MislabeledFillListener( const IPositionProviderPtr &posProvider, const IssuedOrderTrackerPtr& iot )
        : m_posProvider(posProvider), m_iot( iot )
    {
        if(m_iot)
            m_iot->subscribeFillListener(m_fillSub, this);
    }

    /// Called whenever an order receives a fill that's mislabeled such that the
    /// client can do appropriate handling of the situation
    virtual void onMislabeledFill( const OrderPtr& order, uint32_t fill_qty, int32_t absPosition ) = 0;

    virtual void onFill( const bb::trading::FillInfo& info )
    {
        const bb::trading::OrderPtr & order ( info.getOrder() );
        const uint32_t fill_qty ( info.getQty() );
        switch( order->orderInfo().getDir() )
        {
        case bb::SELL:
        {
            int32_t absPos = m_posProvider->getAbsolutePosition();
            if( absPos < 0 )
                onMislabeledFill( order, fill_qty, absPos );
            break;
        }
        case bb::SHORT:
        {
            int32_t absPos = m_posProvider->getAbsolutePosition();
            if( absPos >= 0 )
                onMislabeledFill( order, fill_qty, absPos );
            break;
        }
        default:
            break;
        }
    }

protected:
    IPositionProviderPtr m_posProvider;
    IssuedOrderTrackerPtr m_iot;
    Subscription m_fillSub;
};
BB_DECLARE_SHARED_PTR(MislabeledFillListener);

class MislabeledFillLogger : public MislabeledFillListener
{
public:
    MislabeledFillLogger( const IPositionProviderPtr &posProvider, const IssuedOrderTrackerPtr& iot )
        : MislabeledFillListener( posProvider, iot) {}

    virtual void onMislabeledFill( const OrderPtr& order, uint32_t fill_qty, int32_t absPosition )
    {
        // don't care about sells that we labeled short
        if(order->orderInfo().getDir() == SHORT)
            return;

        OrderWarningRecord(m_iot->log(), order)
            << bson_pair_literal("msg", "MislabeledFillLogger: mislabelled fill")
            << bson_pair_literal("pos", absPosition)
            << bson_pair_literal("fill_qty", fill_qty)
            ;
    }
};
BB_DECLARE_SHARED_PTR(MislabeledFillLogger);

} // namespace trading
} // namespace bb

#endif // BB_TRADING_MISLABELEDFILLLISTENER_H

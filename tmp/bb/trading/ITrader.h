#ifndef BB_TRADING_ITRADER_H
#define BB_TRADING_ITRADER_H

/* Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved. */


#include <string>
#include <stdexcept>

#include <bb/core/acct.h>
#include <bb/core/timeval.h>
#include <bb/core/Subscription.h>
#include <bb/core/smart_ptr.h>
#include <bb/core/side.h>
#include <bb/core/instrument.h>
#include <bb/trading/trading.h>

namespace bb {

BB_FWD_DECLARE_SHARED_PTR(IPositionBroadcastRequester);

namespace trading {

BB_FWD_DECLARE_INTRUSIVE_PTR(Order);

/// Listener class for connect/disconnect from something that can execute trades.
struct ITDConnectionListener
{
    virtual ~ITDConnectionListener() {}
    virtual void onTradeServerConnected(IPositionBroadcastRequesterPtr mngSetup) = 0;
    virtual void onTradeServerDisconnected() = 0;
};

class ITrader
{
public:
    typedef Subscription ListenerSubPtr;
    static const unsigned int SEND_ORDER_FAILED = 0;

    // a function which can be called to add additional bson statistics when issuing an order
    typedef boost::function<void ( const OrderPtr &o )> enrich_stats_callback_t;

protected:
    enrich_stats_callback_t m_enrich_stats_callback;

public:
    virtual ~ITrader() { }

    /* const methods */
    /// Returns whether this Trader is connected to an order receiving agent.
    virtual bool isConnected() const = 0;

    /// Returns the Account of this Trader.
    virtual bb::acct_t getAcct() const = 0;

    /// Returns the IDString of this Trader.
    virtual const std::string& getIDString() const = 0;

    /// Returns the user defined name of the other side (TD) of this trader.
    virtual const std::string& getServerName() const = 0;
    /// Returns the actual hostname of the other side (TD) of this trader.
    virtual const std::string& getServerHostName() const = 0;
    /// Returns the transport mechanism used to connec to the other side of this trader.
    virtual const std::string& getTdTransportType() const
    {
        static std::string empty;
        return empty;
    }

    /// get current count of total number of orders issued by this Trader
    virtual unsigned int getOrderCnt() const = 0;

    /* non const methods */
    /// Requests issuing of desired_order from the trade daemon that this Trader
    /// is connected to. Returns a positive, non-zero orderid on success, or
    /// SEND_ORDER_FAILED(0) if the order could not be sent to the TD.
    /// If this function succeeds, ownership of the owner will be taken over
    /// by the IssuedOrderTracker.
    ///
    /// Any subsequent trade daemon messages for this order will use this orderid,
    /// which is guaranteed to be unique for this trader.
    /// Throws std::invalid_argument if desired_order is NULL.
    /// Throws std::runtime_error if an exceptional problem occurs. Being disconnected
    /// is explicitly NOT an exceptional event.
    /// if send_time is not NULL, then the timestamp when the order is sent is returned.
    virtual unsigned int sendOrder( const OrderPtr& desired_order, timeval_t* send_time = 0 ) = 0;

    /// Requests cancelling of issued_order from the trade daemon that this Trader
    /// is connected to.
    /// Returns true if cancel is sent to trade daemon for order.
    /// Returns false if any problem sending the cancel request.
    /// Returns false if order has already been marked as cancelled.
    /// Throws std::invalid_argument if issued_order is NULL.
    virtual bool sendCancel( const OrderPtr &issued_order, timeval_t *send_time = 0 ) = 0;

    /// Requests modifying an order from the trade daemon that this Trader is connected to
    /// Returns a positive, non-zero orderid on success or SEND_ORDER_FAILED(0) if the order
    /// could not be sent to the trade daemon. If this function succeeds than the original order
    /// will be closed and the ownership of the new order will be taken by the IssuedOrderTracker
    ///
    /// Any subsequent trade daemon messages should reference the new orderid, which is guaranteed to be
    /// unique for this trader
    /// Throws std::invalid_argument if the desired_order or issued_order is null
    /// Throws std::runtime_error if and exceptional problem occurs. Being disconnected is explicitly
    /// NOT and exceptional event
    /// If send_time is not NULL, then the timestamp when the order is sent is returned
    virtual unsigned int modifyOrder( const OrderPtr& issued_order, const OrderPtr& desired_order, timeval_t* send_time = 0 ) = 0;
    // Helper function for modifying an order, by using original order's orderid
    virtual unsigned int modifyOrder( unsigned int orig_orderid, const OrderPtr& desired_order, timeval_t* send_time = 0 ) = 0;

    virtual bool sendCancelsForSide( side_t side, const instrument_t& instr, timeval_t *send_time = 0 ) {
        throw std::logic_error("sendCancelsForSide is not implemented");
    }

    /// Adds a listener which will get notified when the trader is connected/disconnected
    /// from whatever places orders.
    virtual void addTDConnectionListener( Subscription &outListener, ITDConnectionListener* l ) = 0;

    virtual void setEnrichStatsCallback( enrich_stats_callback_t cb ) { m_enrich_stats_callback = cb; }

    virtual enrich_stats_callback_t getEnrichStatsCallback() { return m_enrich_stats_callback; }
};
BB_DECLARE_SHARED_PTR( ITrader );

//this is used to check that ITraders passed as arguments to context are simulated traders (not live!!!)
struct ISimTrader{
    virtual ~ISimTrader(){}
};
BB_DECLARE_SHARED_PTR( ISimTrader );
struct ISimMessageRecoder: virtual ISimTrader{
    /// This records all the simulated messages to a file (for traderviz)
    virtual void recordMessages(std::string const &outFileName)=0;
};
BB_DECLARE_SHARED_PTR( ISimMessageRecoder );
} // namespace trading
} // namespace bb


#endif // BB_TRADING_ITRADER_H

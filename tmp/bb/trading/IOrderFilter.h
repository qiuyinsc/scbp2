#ifndef BB_TRADING_IORDERFILTER_H
#define BB_TRADING_IORDERFILTER_H

/* Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved. */

#include <bb/core/smart_ptr.h>

namespace bb {
namespace trading {

BB_FWD_DECLARE_INTRUSIVE_PTR( Order );

namespace OrderFilter {
enum eFilterCode {
    REJECT   = -1, // reject the order
    ABORT    =  0, // abort the pipeline
    OK       =  1, // continue
    MODIFIED =  2  // order modified
};
}

class IOrderFilter
{
public:
    virtual ~IOrderFilter() {}

    // return false to terminate the filter chain
    virtual OrderFilter::eFilterCode process( const OrderPtr& order ) = 0;
};

BB_DECLARE_SHARED_PTR( IOrderFilter );

} // namespace trading
} // namespace bb

#endif // BB_TRADING_IORDERFILTER_H

#ifndef BB_TRADING_POSITIONTRACKERFACTORY_H
#define BB_TRADING_POSITIONTRACKERFACTORY_H

/* Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved. */


#include <bb/core/hash.h>
#include <bb/trading/IPositionProviderFactory.h>
#include <bb/trading/PositionTracker.h>

namespace bb {
namespace trading {

BB_FWD_DECLARE_SHARED_PTR(PositionTrackerFactory);

// IPositionProviderFactory which builds a PositionTracker.
//
// infoSource is the feed which'll contain the margin_instrument_info messages (position updates).
// In live mode, this should be a SRC_INFO source (i.e. source_t::make_auto(SRC_INFO)).
// In histo, it'll probably be boost::none, because the simulation code will be
// feeding us the position updates.
class PositionTrackerFactory : public IPositionProviderFactory
{
public:
    static PositionTrackerFactoryPtr create(boost::optional<source_t> infoSource, bool bactivate_position_broadcasts = true );
    PositionTrackerFactory(boost::optional<source_t> infoSource, bool bactivate_position_broadcasts = true );

    virtual IOTPositionProviderPtr createInternal( ITraderPtr baseTrader,
            TradingContext *tradingContext, const bb::instrument_t& instr );

    // Returns whether we want to auto-activate position broadcasts for created PositionTrackers.
    bool getActivatePositionBroadcasts() const
    {   return m_activate_position_broadcasts; }
    // Sets whether we want to auto-activate position broadcasts for created PositionTrackers.
    void setActivatePositionBroadcasts( bool activate )
    {   m_activate_position_broadcasts = activate; }

    int getVerbose() const;
    void setVerbose(int) ;
private:
    boost::optional<source_t> m_infoSource;
    bool m_activate_position_broadcasts;
    int m_verbose;
};

} // namespace trading
} // namespace bb

#endif // BB_TRADING_POSITIONTRACKERFACTORY_H

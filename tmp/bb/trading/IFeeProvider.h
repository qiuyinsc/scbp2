#ifndef BB_TRADING_IFEEPROVIDER_H
#define BB_TRADING_IFEEPROVIDER_H

/* Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved. */


#include <bb/trading/Order.h>
#include <bb/trading/IFillListener.h>

namespace bb {

class Msg;
class TdFillMsg;
class TdFillFutureMsg;
class TdFillNewMsg;

namespace trading {

class IFeeProvider
{
public:
    virtual ~IFeeProvider();

    // generic entry point: msg must be a fill msg of some sort,
    // and this will switch on the mtype and dispatch to the
    // appropriate per message handler.
    IFillListener::FeeInfo calculateFees( const OrderPtr& order, const Msg& msg );

    // Eventually, TdFillMsg and TdFillFutureMsg will go away. For
    // now, there isn't much way around this. If your actual
    // implementation doesn't care, feel free to write a templated
    // implementation and dispatch to that in your subclass for each
    // of these.
    virtual IFillListener::FeeInfo calculateFees( const OrderPtr& order, const TdFillMsg& msg ) = 0;
    virtual IFillListener::FeeInfo calculateFees( const OrderPtr& order, const TdFillFutureMsg& msg ) = 0;
    virtual IFillListener::FeeInfo calculateFees( const OrderPtr& order, const TdFillNewMsg& msg ) = 0;
};
BB_DECLARE_SHARED_PTR(IFeeProvider);

class EmptyFeeProvider : public IFeeProvider
{
    public:
        virtual IFillListener::FeeInfo calculateFees( const OrderPtr& order, const TdFillMsg& msg ) { return m_feeInfo; }
        virtual IFillListener::FeeInfo calculateFees( const OrderPtr& order, const TdFillFutureMsg& msg ) { return m_feeInfo; }
        virtual IFillListener::FeeInfo calculateFees( const OrderPtr& order, const TdFillNewMsg& msg ) { return m_feeInfo; }
        static IFeeProviderPtr& instance()
        {
            static IFeeProviderPtr provider ( boost::dynamic_pointer_cast<IFeeProvider>(boost::make_shared<EmptyFeeProvider>()) );
            return provider;
        }
    private:
        IFillListener::FeeInfo m_feeInfo;
};

} // namespace trading
} // namespace bb

#endif // BB_TRADING_IFEEPROVIDER_H

#ifndef BB_TRADING_ONESHOTPOSITIONACCUMULATOR_H
#define BB_TRADING_ONESHOTPOSITIONACCUMULATOR_H

/* Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved. */


#include <bb/core/tif.h>
#include <bb/core/Scripting.h>
#include <bb/trading/IPositionProvider.h>
#include <bb/trading/IPositionAccumulator.h>

namespace bb {
namespace trading {

BB_FWD_DECLARE_SHARED_PTR(ITrader);
BB_FWD_DECLARE_SHARED_PTR(ISOGenerator);
BB_FWD_DECLARE_SHARED_PTR(IPositionProvider);
BB_FWD_DECLARE_SHARED_PTR(IssuedOrderTracker);

/// This algorithm will place a set of orders a single time (hence "one-shot")
/// according to its configuration.
class OneShotPositionAccumulator : public PositionAccumulatorImpl
{
public:
    BB_DECLARE_SCRIPTING();

    struct ParamsBase
    {
        ParamsBase( const std::string& name )
            : m_mktdest( MKT_ISLD )
            , m_market_order( false )
            , m_accum_position_perc( 1.0 )
            , m_outside_ref_px_abs_limit( 999999.00 )
            , m_outside_ref_px_perc_limit( 999999.00 )
            , m_invert_offset( false )
            , m_order_tif( TIF_IMMEDIATE_OR_CANCEL )
            , m_order_tif_timeout( 0 )
            , m_order_flags( 0 )
            , m_round_orders( true )
            , m_name( name )
            , m_enable_iso( false )
            , m_iso_price_at_top( false )
            , m_min_price_variation( 0.01 )
            , m_short_label_policy( IPositionAccumulatorParams::SHORT_LABEL_BY_POSITION )
            , m_order_size_policy( IPositionAccumulatorParams::ORDER_SIZE_DECAY )
            , m_cancel_orders_policy( IPositionAccumulatorParams::CANCEL_ORDERS_EVERY_INTERVAL )
            , m_verbose( true )

        {}

        virtual ~ParamsBase() {}

        mktdest_t   m_mktdest;
        bool        m_market_order;
        double      m_accum_position_perc;
        double      m_outside_ref_px_abs_limit;   // cannot price this $ outside ref_px
        double      m_outside_ref_px_perc_limit;  // cannot price this % outside ref_px
        bool        m_invert_offset;
        tif_t       m_order_tif;
        uint32_t    m_order_tif_timeout;
        uint32_t    m_order_flags;
        bool        m_round_orders;
        std::string m_name;
        bool        m_enable_iso;
        bool        m_iso_price_at_top;
        double      m_min_price_variation;

        IPositionAccumulatorParams::ShortLabelPolicy   m_short_label_policy;
        IPositionAccumulatorParams::OrderSizePolicy    m_order_size_policy;
        IPositionAccumulatorParams::CancelOrdersPolicy m_cancel_orders_policy;
        bool        m_verbose;
        virtual void printFields( std::ostream &out ) const;
    };

    struct Params : ParamsBase, IPositionAccumulatorParamsImpl<Params, OneShotPositionAccumulator>
    {
        Params() : ParamsBase( "OneShotPositionAccumulator" ) {}
        virtual std::string getName() const { return m_name; }
    };
    BB_DECLARE_SHARED_PTR( Params );

public:
    OneShotPositionAccumulator( const ParamsBase& params, const std::string& desc, const instrument_t& instr,
                        const trading::TradingContextPtr& spTradingContext,
                        const IPriceProviderCPtr& spPhase1PriceProvider,
                        const IPriceProviderCPtr& spPhaseXPriceProvider,
                        int32_t desired_pos, double px_offset, bool htb = false );

    virtual ~OneShotPositionAccumulator();

    const instrument_t& getInstrument() const    { return m_instr; }

    virtual void setDesiredPosition( int32_t desired_pos ) { m_desired_pos = desired_pos; }
    int32_t getDesiredPosition() const                     { return m_desired_pos; }

    int32_t getActualPosition() const              { return m_spPosProvider->getAbsolutePosition(); }

    void setActive( bool active );
    bool isActive() const                        { return m_active; }

    bool isCompleted() const                     { return false; }

    void setPriceOffset( double px_offset ) { m_px_offset = px_offset; }
    double getPriceOffset() const           { return m_px_offset; }

    void setMaxBuyPrice( double px )        { m_max_buy_px = px; }
    double getMaxBuyPrice() const           { return m_max_buy_px; }
    void setMinSellPrice( double px )       { m_min_sell_px = px; }
    double getMinSellPrice() const          { return m_min_sell_px; }

protected:
    void cancelAllOrders();
    virtual void generateOrders();

    ParamsBase                      m_params;
    std::string                     m_desc;
    trading::TradingContextPtr      m_spTradingContext;

    trading::IssuedOrderTrackerPtr  m_spIOT;
    trading::ITraderPtr             m_spTrader;
    trading::IPositionProviderPtr   m_spPosProvider;

    typedef std::vector<uint32_t>   IssuedOrderIDList;
    IssuedOrderIDList               m_issued_order_ids;

    bool                            m_htb;

private:
    double                          m_px_offset;
    IPriceProviderCPtr              m_spPriceProvider;

    instrument_t                    m_instr;
    bool                            m_active;

    int32_t                         m_desired_pos;

    /// Our rounding algorithm operates on a truncation policy, and over time
    /// can accumulate a large error size.  We employ a Bresenham-type fix here
    /// where if m_accum_error_size > a round lot size, then send out as many
    /// round lot errors as we can and decrease m_accum_error_size accordingly.
    int32_t                         m_accum_error_size;

    ISOGeneratorPtr                 m_iso_generator;
    double                          m_max_buy_px, m_min_sell_px;
};
BB_DECLARE_SHARED_PTR( OneShotPositionAccumulator );


/// Stream operator to convert OneShotPositionAccumulator::Params into human readable form.
std::ostream &operator <<( std::ostream &out, const OneShotPositionAccumulator::Params &p );
void registerOneShotPositionAccumulatorScripting();
} // namespace trading
} // namespace bb

#endif // BB_TRADING_ONESHOTPOSITIONACCUMULATOR_H

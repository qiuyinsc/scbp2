#ifndef BB_TRADING_ORDERPOSITIONTRACKER_H
#define BB_TRADING_ORDERPOSITIONTRACKER_H

/* Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved. */


#include <vector>

#include <bb/core/acct.h>
#include <bb/trading/FillInfo.h>
#include <bb/trading/IPositionProvider.h>
#include <bb/trading/IOrderStatusListener.h>

namespace bb {
namespace trading {

/** Helper class to track positions filled by registered orders
 * This object is used to track positions due to selected orders.  Register this
 * object as an OrderStatusListener of the orders you care about, and it
 * will maintain total positions as a result of those orders.
 *
 **/
class OrderPositionTracker
    : public bb::trading::PositionProvider
    , public bb::trading::IOrderStatusListener
{
public:
    OrderPositionTracker( const instrument_t& instr )
        : PositionProvider( instr )
        , m_position( 0 )
    { m_longshortShares[BID] = m_longshortShares[ASK] = 0; }
    virtual bool isReady() const { return true; }
    virtual int32_t getAbsolutePosition() const { return m_position; }
    virtual int32_t getEffectivePosition() const { return m_position; }
    bb::acct_t getAcct() const { return ACCT_UNKNOWN; }

    void registerOrder( Order* order, const Priority& priority = PRIORITY_TRADING_DEFAULT );
    int32_t getLongShares() const { return m_longshortShares[BID]; }
    int32_t getShortShares() const { return m_longshortShares[ASK]; }
    int32_t getFilledShares() const { return getLongShares() + getShortShares(); }

protected:
    virtual void handleFill( const timeval_t &tv, side_t side, double fill_px
        , uint32_t fill_qty
        , const boost::optional<int32_t> &posFromMsg) {}

    virtual void onFill( const bb::trading::FillInfo & info );

private:
    int32_t m_position;
    int32_t m_longshortShares[2];
    std::vector<Subscription> m_subscriptionList;
};

BB_DECLARE_SHARED_PTR( OrderPositionTracker );

} // trading
} // bb

#endif // BB_TRADING_ORDERPOSITIONTRACKER_H

#ifndef BB_TRADING_ISOGENERATOR_H
#define BB_TRADING_ISOGENERATOR_H

/* Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved. */


#include <string>
#include <vector>
#include <boost/noncopyable.hpp>
#include <bb/core/PriceSize.h>
#include <bb/core/bbhash.h>
#include <bb/core/hash_set.h>
#include <bb/core/mktdest.h>
#include <bb/core/side.h>
#include <bb/trading/Order.h>

namespace bb {

BB_FWD_DECLARE_SHARED_PTR(ISOBook);
BB_FWD_DECLARE_SHARED_PTR(ClientContext);

namespace trading {

BB_FWD_DECLARE_SHARED_PTR(ISOGenerator);

class ISOGenerator : boost::noncopyable
{
public:
    typedef std::vector<OrderPtr> order_vec_t;
    typedef bbext::hash_set<mktdest_t> mktdest_set_t;

    // Return the default set of protected market centers.
    static mktdest_set_t getProtectedMarkets( const ClientContextPtr& context, const instrument_t& instrument );

    // Construct a new ISO order generator, using the provided book.
    static ISOGeneratorPtr create( const ClientContextPtr& context, const ISOBookPtr& book );

    // As above, but overrides the set of markets that must be
    // checked. This is here only to facilitate testing.
    //
    // WARNING: If requiredMarkets does not contain all of
    // WARNING: the markets you are obligated to monitor for quotes,
    // WARNING: then generated orders may be in
    // WARNING: violation of RegNMS.
    // WARNING: Don't do that. Really.
    static ISOGeneratorPtr create(
        const ISOBookCPtr& book,
        const mktdest_set_t& requiredMarkets );

    // Return values for generateOrders.
    enum GenerateStatus
    {
        kGenerateSuccess = 0,
        kGenerateErrorBookNotOK,
        kGenerateErrorBookNotAuthoritative,
        kGenerateErrorNoRemainderAfterProtected,
        kGenerateErrorZeroOrdersGenerated
    };

    // Populates 'orders' with a sequence of orders for an intermarket
    // sweep. The caller provides a request object describing the
    // ISO. The returned sequence of orders will consist of RegNMS
    // compliant ISO tagged orders to take out all protected quotes
    // w.r.t. the limit price, and an ISO tagged order to the
    // preferred market destination for any remaining size after
    // dealing with protected quotes.
    //
    // The non-null pointer 'orders' points to a vector which will be
    // cleared on entry to this method, any data held there will be
    // lost, unconditionally. On a successful return, 'orders' will be
    // populated with orders to be sent.
    //
    // The method will return success if results has been populated
    // with a non-empty sequence of orders to be executed.
    //
    // The method will return an error code if the generator was unable
    // to create a sequence of orders for any reason, in which case
    // 'orders' will be empty.
    //
    // Details ISO generation are documented below at the definition
    // of Request.

    class Request;
    GenerateStatus generateOrders( const Request& request, order_vec_t* orders );

    // Iterates over and order_vec_t and totals up the size of all of
    // the orders in the vector.
    static uint32_t getTotalSize( const order_vec_t& orders );

    // Get a string describing the given error.
    static std::string statusToString( GenerateStatus status );

    // Return a constant view of the book.
    inline const ISOBookCPtr& book() const
    {
        return m_book;
    }

    // Return the markets we consider.
    inline const mktdest_set_t& markets() const
    {
        return m_markets;
    }

    ~ISOGenerator();

private:
    ISOGenerator( const ISOBookCPtr&, const mktdest_set_t& );

    const mktdest_set_t m_markets;
    const ISOBookCPtr m_book;
};


class ISOGenerator::Request
{
    friend class ISOGenerator;

public:

    enum PricingModel
    {
        // Default behavior: every order to a protected quote will be
        // priced to match the protected quote.
        kPriceOrdersToMatchQuotes,

        // A more aggressive pricing model: every order will be priced
        // at the limit, ignoring the prices of the protected quotes.
        kPriceAllOrdersAtLimit
    };

    // TODO(acm): JZ suggests making this a fraction, rather than a
    // switch, so you could say: "require that at least 30% of my
    // requested size remains after hitting protected liquidity".
    enum SweepStyle
    {
        // Default behavior: the sweep is OK no matter how deep
        // into the book the requested size eats.
        kBestEffort,

        // Require that remaining size exists after exhausting the
        // protected liquidity w.r.t. the limit price. The remainder
        // (plus any protected size) will then be issued to the
        // preferred market at the limit price. The generator will
        // return an error if no remainder exists after exhausting the
        // requested size.
        kMustHitThroughProtectedLiquidity
    };

    // Create a new ISO request with the required parameters.
    // - direction: Buying or Selling?
    // - priceSize: Maximum number of shares to act on, and the limit price.
    // - preferred: Market to which to direct residual size after sweeping protected quotes.
    //
    // The object will default to the following values. These
    // selections may be overridden by calling the appropriate
    // member function.
    // - pricing: Defaults to kPriceAllOrdersAtLimit
    // - style:   Defaults to kBestEffort
    inline Request( dir_t direction, const PriceSize& priceSize, mktdest_t preferredMarket )
        : m_direction( direction )
        , m_priceSize( priceSize )
        , m_preferredMarket( preferredMarket )
        , m_pricing( kPriceAllOrdersAtLimit )
        , m_style( kBestEffort ) {}

    inline ~Request() {}

    // Override or change the pricing model.
    inline Request& setPricingModel( PricingModel pricing )
    {
        m_pricing = pricing;
        return *this;
    }

    inline Request& setSweepStyle( SweepStyle style )
    {
        m_style = style;
        return *this;
    }

private:
    const dir_t     m_direction;
    const PriceSize m_priceSize;
    const mktdest_t m_preferredMarket;
    PricingModel    m_pricing;
    SweepStyle      m_style;
};



} // namespace trading
} // namespace bb

#endif // BB_TRADING_ISOGENERATOR_H

#ifndef BB_TRADING_ALGO_H
#define BB_TRADING_ALGO_H

/* Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved. */


#include <stdint.h>
#include <bb/core/smart_ptr.h>
#include <bb/core/ListenerList.h>
#include <bb/trading/IOrderStatusListener.h>

namespace bb {
namespace trading {

class IAlgoRequestListener;

class Algo
{
public:
    BB_FWD_DECLARE_SHARED_PTR(Request);
    class Request
        : public ListenerList<IAlgoRequestListener*>
        , public boost::enable_shared_from_this<Request>
    {
    public:
        virtual ~Request()
        {
            // Leaf subclasses are required to have called
            // notifyListenersOfClose, so if that hasn't happened by now, its
            // too late.
            BB_ASSERT( m_closeNotifyCalled );
        }

        virtual bool isActive() const = 0;
        virtual uint32_t getOutstandingShares() const = 0;
        virtual uint32_t getTargetShares() const = 0;
        virtual uint32_t getFilledShares() const = 0;

    protected:
        typedef RequestPtr Keepalive;

        /// When you're calling IAlgoRequestListener callbacks from within your
        /// Request's functions, you need to be careful that the listeners don't
        /// destroy your class. Avoid this with a Keepalive: it'll temporarily
        /// addref you while it's in scope.
        Keepalive makeKeepalive() { return shared_from_this(); }

        // Leaf subclasses of Request must call this from within their
        // destructor. This condition is checked in Request's
        // destructor.
        virtual void notifyListenersOfClose();

        Request()
            : m_closeNotifyCalled( false ) {}

    private:
        bool m_closeNotifyCalled;
    };

public:
    virtual ~Algo() {}
};

class IAlgoRequestListener
{
public:
    virtual ~IAlgoRequestListener() {}

    virtual void onStatusUpdate( Algo::Request* req, const OrderPtr& order, IOrderStatusListener::ChangeFlags const& ) =0;
    virtual void onFill( Algo::Request* req, const bb::trading::FillInfo& ) = 0;
    virtual void onFinish( Algo::Request* req )=0;

    /// Request is closed, whether because it's canceled or finished
    virtual void onClose( Algo::Request* req )=0;
};

} // trading
} // bb

#endif // BB_TRADING_ALGO_H

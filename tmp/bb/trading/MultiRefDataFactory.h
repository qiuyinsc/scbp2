#ifndef BB_TRADING_MULTIREFDATAFACTORY_H
#define BB_TRADING_MULTIREFDATAFACTORY_H

/* Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved. */


#include <bb/trading/IRefDataFactory.h>

#include <map>
#include <bb/core/mktdest.h>

namespace bb {

namespace trading {


class MultiRefDataFactory : public IRefDataFactory
{
public:

    // IRefDataFactoryPtr bbl1RefDataFactory = boost::make_shared<BbL1RefDataFactory>();
    // IRefDataFactoryPtr futuresRefDataFactory = boost::make_shared<FuturesRefDataFactory>();
    //
    // MultiRefDataFactory::FeedMap feedMap;
    // feedMap[ MKT_NYSE ] = bbl1RefDataFactory;
    // feedMap[ MKT_ARCA ] = bbl1RefDataFactory; // <-- note, you can use the same object more than once!
    // feedMap[ MKT_ISLD ] = bbl1RefDataFactory;
    // feedMap[ MKT_CME ]  = futuresRefDataFactory;
    //
    // IRefDataFactoryPtr multiRefDataFactory = boost::make_shared<MultiRefDataFactory>( feedMap );,

    typedef std::map<mktdest_t, IRefDataFactoryPtr> FeedMap;

    MultiRefDataFactory(const FeedMap& feedMap );

    virtual ~MultiRefDataFactory();

    virtual RefDataPtr createRefData(
        TradingContext* tradingContext,
        const instrument_t& instr );

protected:
    virtual RefDataPtr createRefDataFromMktDest(
        TradingContext* tradingContext, mktdest_t mkt,
        const instrument_t& instr );

    const FeedMap& m_feedMap;
};

BB_DECLARE_SHARED_PTR(MultiRefDataFactory);

} // namespace trading
} // namespace bb

#endif // BB_TRADING_MULTIREFDATAFACTORY_H

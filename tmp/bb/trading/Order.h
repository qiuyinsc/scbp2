#ifndef BB_TRADING_ORDER_H
#define BB_TRADING_ORDER_H

/* Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved. */


#include <iosfwd>
#include <json_spirit.h>
#include <stdexcept>
#include <typeinfo>

#include <boost/type_traits/is_const.hpp>
#include <boost/utility/in_place_factory.hpp>
#include <boost/utility/enable_if.hpp>
#include <boost/enable_shared_from_this.hpp>
#include <boost/static_assert.hpp>

#include <bb/core/compat.h>
#include <bb/core/Countable.h>
#include <bb/core/cxlstatus.h>
#include <bb/core/fillflag.h>
#include <bb/core/instrument.h>
#include <bb/core/liquidity.h>
#include <bb/core/oflag.h>
#include <bb/core/oreason.h>
#include <bb/core/oreason_utils.h>
#include <bb/core/ostatus.h>
#include <bb/core/side.h>
#include <bb/core/sorted_list.h>
#include <bb/core/tid.h>
#include <bb/core/tif.h>
#include <bb/core/timeval.h>
#include <bb/core/PreallocatedObjectFactory.h>

#include <bb/io/BsonLog.h>

#include <bb/trading/trading.h> //PRIORITY_TRADING_DEFAULT
#include <bb/trading/FillInfo.h>
#include <bb/trading/IOrderStatusListener.h>
#include <bb/trading/add_liquidity.h>

namespace bb {
namespace trading {

static const size_t ORDER_MAX_INFO_COUNT = 16;

BB_FWD_DECLARE_INTRUSIVE_PTR( Order );

class OrderInfo;
class OrderListenerInfo;
class IssuedOrderInfo;

/// Base class for all Info structures which will be attached to the order.
/// The idiom is:
/// class MyInfo : public OrderInfoSlot
/// {
///    MyInfo() // can have 0 parameters (for use with add()), or 1 param (for use with construct().
///    BB_DECLARE_ORDER_INFO();
/// };
/// inside .cpp, at file scope:
///    BB_DEFINE_ORDER_INFO(MyInfo);
class OrderInfoSlot : public boost::noncopyable
{
public:

    typedef const OrderInfoSlot* const_iterator;

    virtual ~OrderInfoSlot() {}

    /// Override this to add your own information to the IOT logs.
    virtual void logIssue(BsonLog::Record &parent, Order const *order) const {}
    virtual void logStatusChange(BsonLog::Record &parent, Order const *order, IOrderStatusListener::ChangeFlags const &flags) const {}
    virtual void logFill(BsonLog::Record &parent, Order const *order, double fill_px, uint32_t fill_qty) const {}
    // Called when the client is sending a cancel request to the TD.
    virtual void logCancel(BsonLog::Record &parent, Order const *order) const {}
    virtual void logReprice(BsonLog::Record &parent, Order const *order, double newpx) const {}
    /// Override this to add your own information to the IOT logs
    virtual void logModifyIssue( BsonLog::Record &parent, Order const *order, Order const *origOrder ) const {}
    virtual void logModifyCancel( BsonLog::Record &parent, Order const *order, Order const *newOrder ) const {}
    virtual json_spirit::mValue toJsonValue() const;
    struct Decl;
};



/// Encapsulates all the information about an order that must be set
/// before the order can be sent to the market.
class OrderInfo
{
public:
    OrderInfo()
        : m_frozen(false)
        , m_dest(MKT_UNKNOWN)
        , m_dir(DIR_INVALID)
        , m_px(std::numeric_limits<double>::infinity())
        , m_sz(std::numeric_limits<uint32_t>::max())
        , m_tif(TIF_IMMEDIATE_OR_CANCEL)
        , m_timeout(0)
        , m_oflags(OFLAG_NONE) {}

    // Copy constructor. We set m_frozen to false, since we want to allow the user to change
    // values for this copied OrderInfo.
    OrderInfo& operator =( const OrderInfo& other )
    {
        // If we add a new_field to OrderInfo, this would remind us to
        // implement it in the copy constructor also
        BOOST_STATIC_ASSERT( sizeof(OrderInfo) == 96 );
        if(unlikely(m_frozen)) throwFrozenException();
        m_instr = other.m_instr;
        m_dest = other.m_dest;
        m_dir  = other.m_dir;
        m_px = other.m_px;
        m_sz = other.m_sz;
        m_tif = other.m_tif;
        m_timeout = other.m_timeout;
        m_oflags = other.m_oflags;
        return *this;
    }

    // getters
    const instrument_t& getInstrument()    const { return m_instr; }
    symbol_t            getSymbol()        const { return m_instr.sym; }
    double              getPrice()         const { return m_px; }
    uint32_t            getDesiredSize()   const { return m_sz; }
    mktdest_t           getMktDest()       const { return m_dest; }
    dir_t               getDir()           const { return m_dir; }
    side_t              getSide()          const { return dir2side(getDir()); }
    uint32_t            getTimeout()       const { return m_timeout; }
    tif_t               getTimeInForce()   const { return m_tif; }
    bool                getVisible()       const { return !(m_oflags & OFLAG_INVISIBLE); }
    bool                getProactive()     const { return m_oflags & OFLAG_PROACTIVE; }
    bool                getStabilize()     const { return m_oflags & OFLAG_STABILIZE; }
    bool                getDarkPool()      const { return m_oflags & OFLAG_DARK_POOL; }
    bool                getISO()           const { return m_oflags & OFLAG_ISO; }
    bool                getCancelOrigOnReject()  const { return m_oflags & OFLAG_CANCEL_ORIG_ON_REJECT; }
    uint32_t            getFlags()         const { return m_oflags; }

    const boost::optional<uint32_t>& getReserveSize() const { return m_reserver_sz; }

    // setters: required fields
    OrderInfo &setInstrument(const instrument_t &instr);
    OrderInfo &setPrice(double px); // 0.0 means market order
    OrderInfo &setDesiredSize(int sz);
    OrderInfo &setMktDest(mktdest_t mkt);
    OrderInfo &setDir(dir_t d);

    // setters: optional fields (initialized to default values)
    OrderInfo &setTimeInForce(tif_t tif);
    OrderInfo &setTimeout(uint32_t tout); // in seconds
    OrderInfo &setVisible(bool vis);
    OrderInfo &setProactive(bool pro);
    OrderInfo &setStabilize(bool stab);
    OrderInfo &setDarkPool(bool dark);
    OrderInfo &setISO(bool iso); // for use only by ISOGenerator, do not call in application code
    // only applicable to modify orders. If a modify is rejected than whether the orig order
    // should be canceled is decided by this flag
    OrderInfo &setCancelOrigOnReject( bool CancelOrigOnReject );
    OrderInfo &setFlags(uint32_t flags);
    OrderInfo &addFlag(uint32_t flag);


    OrderInfo& setReserveSize( uint32_t size );

    // allows use of special tokens to set a group of flags
    template<typename P> OrderInfo &enableFlagGrp( const P& x ); // { BOOST_STATIC_ASSERT( sizeof(P) == 0 ); return *this; }

    /// Called once the order is issued: checks that all non-optional
    /// fields have been set in the order. Throws BadInfoException on error.
    void checkInitialized() const;
    /// Disallows modification of this Info.
    void freeze() { m_frozen = true; }

    void logAttrs(BsonLog::Record &record) const;
protected:
    void throwFrozenException();

    bool m_frozen;
    instrument_t m_instr;
    mktdest_t m_dest;
    dir_t m_dir;
    double m_px;
    uint32_t m_sz;
    tif_t m_tif;
    uint32_t m_timeout;
    uint32_t m_oflags;

    boost::optional<uint32_t> m_reserver_sz;
};



/// Stores all status information about an order that's been issued to the market.
class IssuedOrderInfo
{
public:
    IssuedOrderInfo()
        : m_parent(NULL)
        , m_oid()
        , m_issuedTv()
        , m_lastLastModTv()
        , m_lastModTv()
        , m_orderStatus(STAT_NEW)
        , m_cancelStatus(CXLSTATUS_NONE)
        , m_remaining_size()
        , m_last_fill_px(0.0)
        , m_last_fill_qty(0)
        , m_total_filled_qty(0)
        , m_lastFillFlags(FILLFLAG_NONE)
        , m_lastReportedRemainingSize(0)
        , m_done_reason(R_NONE)
        , m_lastFillLiquidity(LIQ_UNKNOWN)
        , m_lastFillContraBroker(MKT_UNKNOWN)
        , m_lastFillExchTime()
        , m_priceAtMarket() {}

    // two-stage initialization (for efficiency)
    bool initialized() const { return m_parent; }
    void initialize(Order *parent, uint32_t orderid, timeval_t const &issuedTv, uint32_t remaining_size,
            double priceAtMarket);

    /// Return the price of last fill on this order.
    double getLastFillPrice() const { return m_last_fill_px; }

    /// Return the quantity of the last fill on this order
    uint32_t getLastFillQty() const { return m_last_fill_qty; }

    /// Return the quantity of the last fill on this order
    fillflag_t getLastFillFlags() const { return m_lastFillFlags; }

    /// Returns the number of shares in this order which have not been filled.
    uint32_t getRemainingSize() const { return m_remaining_size; }

    /// Returns the remaining shares as reported by fill message
    uint32_t getLastReportedRemainingSize() const { return m_lastReportedRemainingSize; }

    /// Return the total qty filled on this order since being issued.
    uint32_t getTotalFilledQty() const { return m_total_filled_qty; }

    oreason_t getDoneReason() const { return m_done_reason; }
    unsigned int getOrderid() const { return m_oid; }
    tid_t getTradeId() const { return m_tradeId; }

    /// Returns the price of the order on the market. This equals
    /// the desired price, except if we got a td_reprice message.
    double getPriceAtMarket() const { return m_priceAtMarket; }

    // Order status flags.
    /// getOrderStatus() tracks the state of the issued order.
    /// It will never return STAT_CXLSENT, that is a legacy value.
    /// getCancelStatus() tracks the state of a cancel request on
    /// the order; it gets tracked separately from the state of the issue.
    ostatus_t getOrderStatus() const { return m_orderStatus.cur(); }
    cxlstatus_t getCancelStatus() const { return m_cancelStatus.cur(); }
    ostatus_t getLastOrderStatus() const { return m_orderStatus.prev(); }
    cxlstatus_t getLastCancelStatus() const { return m_cancelStatus.prev(); }

    bool done() const { return getOrderStatus() == STAT_DONE; }
    bool mktConfirmed() const { return getOrderStatus() >= STAT_OPEN; }
    bool atMarket() const { return getOrderStatus() == STAT_OPEN; } // mktConfirmed() && !done()
    bool cancelSent() const { return getCancelStatus() != CXLSTATUS_NONE && getCancelStatus() != CXLSTATUS_REJECT; }

    bool rejected() const { return done() && is_reject(getDoneReason()); }
    bool canceled() const { return done() && (getDoneReason() == R_CANCEL); }

    // Timestamps
    /// Return the timeval of the last fill on this order
    timeval_t getLastFillTv() const { return m_last_fill_tv; }
    const timeval_t &getIssuedTv() const { return m_issuedTv; }
    const timeval_t& getLastOrderStatusModifiedTv() const { return m_orderStatus.lastModifyTime(); }
    const timeval_t& getLastCancelStatusModifiedTv() const { return m_cancelStatus.lastModifyTime(); }

    liquidity_t getLastFillLiquidity() const { return m_lastFillLiquidity; }
    mktdest_t getLastFillContraBroker() const { return m_lastFillContraBroker; }
    const timeval_t &getLastFillExchTime() const { return m_lastFillExchTime; }

    // Functions called by the IssuedOrderTracker

    // changes the given status field. Caller must insure that the new status is
    // different from the old status, and that the transition is allowed.
    // Also updates the ChangeFlags to reflect what was changed.
    // Does not perform listener notifications.
    void updateOrderStatus( const tid_t& tid, IOrderStatusListener::ChangeFlags &flags
        , ostatus_t stat );
    void updateCancelStatus(IOrderStatusListener::ChangeFlags &flags, cxlstatus_t stat);
    void updateDoneReason(oreason_t r) { m_done_reason = r; }
    /// Updates last_fill_px, total_filled_qty, last_mod_tv. Does not call listeners.
    void applyFill( BsonLog &log
        , double fill_px, uint32_t fill_qty, uint32_t remainder
        , liquidity_t liquidity, mktdest_t contra
        , const timeval_t &timestamp, const timeval_t &exch_time
        , fillflag_t fill_flags );

    /// Call listeners for a STAT_NEW transition.
    void notifyNew(const IOrderStatusListener::ChangeFlags &newFlags);
    /// Updates last_mod_tv && calls listeners for a status change.
    void notifyStatChange(BsonLog &log, const IOrderStatusListener::ChangeFlags &changeFlags);
    /// Updated the last_mod_tv && calls the listeners for modify status change
    void notifyModifyStatChange( BsonLog& log, const IOrderStatusListener::ChangeFlags &new_changeFlags
                                 , const OrderPtr& orig_order, const IOrderStatusListener::ChangeFlags &orig_changeFlags );
    /// Calls listeners for a fill.
    void notifyFill(const bb::trading::FillInfo& fill_info);
    void applyAndNotifyReprice(BsonLog &log, const timeval_t &timestamp, double newpx);

    void logAttrs(BsonLog::Record &record) const;

protected:
    inline void notify(const boost::function<void (IOrderStatusListener*)> &fn);
    bool hasListeners();
    void touchLastModifiedTv(BsonLog &log, const timeval_t &chg_tv);
    template<typename T> class ValueTracker
    {
    public:
        ValueTracker() {}
        ValueTracker( const T& initial_val )
            : m_curVal( initial_val )
            , m_prevVal( initial_val ) {}
        void update( const T& v, timeval_t cur_time )
        {
            BB_ASSERT( v != m_curVal );
            m_prevVal = m_curVal;
            m_curVal = v;
            m_updateTime = cur_time;
        }
        const T& cur() const { return m_curVal; }
        const T& prev() const { return m_prevVal; }
        const timeval_t& lastModifyTime() const { return m_updateTime; }
        void touch( const timeval_t& tv ) { m_updateTime = tv; }
    protected:
        timeval_t m_updateTime;
        T m_curVal, m_prevVal;
    };

    Order *m_parent;
    unsigned int m_oid;
    tid_t m_tradeId;

    timeval_t m_issuedTv;
    timeval_t m_lastLastModTv;
    timeval_t m_lastModTv;
    ValueTracker<ostatus_t> m_orderStatus;
    ValueTracker<cxlstatus_t> m_cancelStatus;

    uint32_t m_remaining_size;
    double m_last_fill_px;
    uint32_t m_last_fill_qty;
    timeval_t m_last_fill_tv;
    uint32_t m_total_filled_qty;
    fillflag_t m_lastFillFlags;
    uint32_t m_lastReportedRemainingSize;

    oreason_t m_done_reason;
    liquidity_t m_lastFillLiquidity;
    mktdest_t m_lastFillContraBroker;
    timeval_t m_lastFillExchTime;
    double m_priceAtMarket;
};



/// Stores the list of listeners on an order.
/// This can be created at any time using add<OrderListenerInfo>().
/// This is used by the IssuedOrderInfo, but that's not created until the order is issued,
/// and this can be created sooner.
class OrderListenerInfo
{
public:
    OrderListenerInfo()
        : m_created( false ) {}

    bool initialized() const { return m_created; }
    void setInitialized() { m_created = true; }

    typedef IOrderStatusListener::Listeners_t Listeners_t;
    typedef Subscription SubPtr;

    /// Adds listener for all IssuedOrder changes and returns SubPtr
    void subscribe( Subscription &outSub, IOrderStatusListener *listener
        , const Priority& p = PRIORITY_TRADING_DEFAULT ) const
    {
        get().subscribe( outSub, listener, p );
    }

    template<typename C> void addSubscription( C* list
        , IOrderStatusListener* listener
        , const Priority& p = PRIORITY_TRADING_DEFAULT ) const
    {
        get().addSubscription( list, listener, p );
    }

    void add( IOrderStatusListener* listener
        , const Priority& p = PRIORITY_TRADING_DEFAULT ) const
    {
        get().add( listener, p );
    }

    Listeners_t& get() const
    {
        if( !m_listeners )
            m_listeners.reset( new Listeners_t );
        return *m_listeners;
    }

protected:
    bool m_created;
    mutable scoped_ptr<Listeners_t> m_listeners;
};



/// Order is an extensible structure representing all the information we wish to store about
/// an Order. Orders are typically held in an OrderPtr (which is a boost::intrusive_ptr).
///
/// Anyone can attach info to an Order by declaring their own OrderInfoSlot subclass.
/// The important ones are:
///
/// OrderInfo: created when the Order is created. This encapsulates all the information about
/// an order that must be set before the order can be sent to the market.
///
/// IssuedOrderInfo: created when the Order is issued. Used by IssuedOrderTracker to track
/// the order lifetime once it's sent to market.
class Order
    : public Countable<>
    , public FactoryObject<Order>
{
public:
    /// Constructs a new Order. This automatically attaches an OrderInfo to the Order as well.
    Order();
    virtual ~Order();

    OrderInfo       &orderInfo();
    OrderInfo const &orderInfo() const;
    IssuedOrderInfo       &issuedInfo();
    IssuedOrderInfo const &issuedInfo() const;
    OrderListenerInfo &listeners();

    /// Returns true if the order has been issued.
    bool isIssued() const;

    virtual void destructObject();

    /// Returns a previously attached piece of info about the order, or throws BadInfoException.
    template<typename InfoClass>
        typename boost::enable_if<boost::is_const<InfoClass>, InfoClass>::type &
            get() const;
    template<typename InfoClass>
        typename boost::disable_if<boost::is_const<InfoClass>, InfoClass>::type &
            get();

    /// Creates an object of type InfoClass (default constructed) and attaches it to the order.
    /// If an object of this type has already been added to the order, returns that.
    template<typename InfoClass> InfoClass &add();

    /// Creates an object of type InfoClass with the given arguments and attaches it to the order.
    /// If an object of this type has already been added to the order, throws BadInfoException
    /// The Info's constructor can accept 0 or 1 parameters; to pass multiple, use some kind of tuple/CInfo.
    template<typename InfoClass> InfoClass &construct();
    template<typename InfoClass, typename CInfo> InfoClass &construct(const CInfo &c);
    template<typename InfoClass, typename CInfo1, typename CInfo2> InfoClass &construct(const CInfo1 &c1, const CInfo2 &c2 );

    /// Destroys the given Info. If an object of this type doesn't exist, throws BadInfoException
    template<typename InfoClass> void destroy();

    /// Returns true if the object has info of the given type attached
    template<typename InfoClass> bool hasInfo() const;

    /// Thrown when the constraints on get/add/construct/destroy are violated.
    struct BadInfoException : public std::runtime_error
    {
        BadInfoException(std::string msg) : std::runtime_error(msg) {}
    };

    void print(std::ostream &s) const;
    void logIssue(BsonLog::Record &record) const;
    void logStatusChange(BsonLog::Record &record, IOrderStatusListener::ChangeFlags const &flags) const;
    void logCancel(BsonLog::Record &record) const;
    void logFill(BsonLog::Record &record, double fill_px, uint32_t fill_qty) const;
    void logReprice(BsonLog::Record &record, double newpx) const;
    void logModifyIssue( BsonLog::Record& record, Order const *origOrder ) const;
    void logModifyCancel( BsonLog::Record& record, Order const *newOrder ) const;

    IssuedOrderInfo &constructIssuedOrderInfo(Order *parent, uint32_t orderid,
            timeval_t const &issuedTv, uint32_t remaining_size, double priceAtMarket);

public:
    size_t getNumSlots() const;
    OrderInfoSlot::const_iterator order_info_begin() const { return m_infos[0]; }
    OrderInfoSlot::const_iterator order_info_end() const { return m_infos[ getNumSlots() ]; }
    const OrderInfoSlot*  getOrderInfoSlot( size_t i ) const { return m_infos[i]; }

protected:
    void logCommon(BsonLog::Record &record) const;
    void throwBadOrderInfoException(const char *msg, const std::type_info &tid) const;
    OrderInfo m_orderInfo;
    IssuedOrderInfo m_issuedInfo;
    OrderListenerInfo m_listenerInfo;
    OrderInfoSlot *m_infos[ORDER_MAX_INFO_COUNT];
};
BB_DECLARE_INTRUSIVE_PTR(Order);

inline std::ostream &operator<<(std::ostream &os, const Order &order) { order.print(os); return os; }


/// Order aggressiveness comparator, analogous to strcmp in that has a tri-valued result.
class AggressionComp
{
public:
    AggressionComp(side_t side) : m_sideSign(-side2sign(side)) {}

    /// returns -1 if o1 is "more aggressive" than o2.
    /// returns  1 if o1 is "less aggressive" than o2.
    /// returns  0 if o1 is "equally aggressive" as o2.
    /// note that the compared orders must both be on the side given in the constructor.
    int cmp(const OrderCPtr &o1, const OrderCPtr &o2) const;

private:
    int m_sideSign;
};

/// Predicate for sorted_list to put the most aggressive orders first.
class MoreAggressiveOrderComp : private AggressionComp
{
public:
    MoreAggressiveOrderComp(side_t side) : AggressionComp(side) {}

    // returns true if o1 is more aggressive than o2
    bool operator()(const OrderCPtr &o1, const OrderCPtr &o2) const { return cmp(o1, o2) < 0; }
};

/// A linked-list of Orders, sorted by OrderCmp.
/// Once the orders are inserted, nothing fundamental about the order should be changed
/// (price, size, or anything which OrderCmp uses).
typedef sorted_list<OrderPtr, MoreAggressiveOrderComp> OrderList;
BB_DECLARE_SHARED_PTR(OrderList);


/// RAII class which should be declared static inside each Info subclass.
/// The static initializer will allocate space for the Info inside Order.
struct OrderInfoSlot::Decl: public boost::noncopyable
{
    Decl();
    //needed only when declaring OrderInfoSlots in shared libraries
    //that are loaded in a multiday fashion
    static void resetAll();
protected:
    friend class Order;
    const size_t m_fieldOffset;
    static bool ms_frozen;
    static size_t ms_numFields;
};
#define BB_DECLARE_ORDER_INFO()     static bb::trading::OrderInfoSlot::Decl ms_orderInfoDecl;
#define BB_DEFINE_ORDER_INFO(klass) bb::trading::OrderInfoSlot::Decl klass::ms_orderInfoDecl;


/// Info about the intent of the order when it gets issued.
/// Optional, but required by Limits.
class AddLiquidityInfo : public OrderInfoSlot
{
public:
    AddLiquidityInfo(add_liquidity_t alq=CrossRemLiq) : m_addLiquidity(alq) {}
    BB_DECLARE_ORDER_INFO();
    virtual void logIssue(BsonLog::Record &parent, Order const *order) const;
    add_liquidity_t m_addLiquidity;
};



/// A warning entry for the IOT log; auto-attaches the oid & instr.
struct OrderWarningRecord : public BsonLog::Record
{
    OrderWarningRecord(BsonLog &log, OrderCPtr const &o)
        : BsonLog::Record(log, "warning")
    {
        doLog(o->issuedInfo().getOrderid(), o->orderInfo().getInstrument());
    }

    OrderWarningRecord(BsonLog &log, uint32_t orderid, instrument_t const &instr)
        : BsonLog::Record(log, "warning")
    {
        doLog(orderid, instr);
    }

    void doLog(uint32_t orderid, instrument_t const &instr)
    {
        *this << bson_pair_literal("oid", orderid)
              << bson_pair_literal("instr", instr);
    }
};

template<typename InfoClass>
    typename boost::enable_if<boost::is_const<InfoClass>, InfoClass>::type &
        Order::get() const
{
    const OrderInfoSlot *info = m_infos[InfoClass::ms_orderInfoDecl.m_fieldOffset];
    if(unlikely(!info))
        throwBadOrderInfoException("Order::get: no info of type: ", typeid(InfoClass));
    return static_cast<const InfoClass&>(*info);
}

template<typename InfoClass>
    typename boost::disable_if<boost::is_const<InfoClass>, InfoClass>::type &
        Order::get()
{
    OrderInfoSlot *info = m_infos[InfoClass::ms_orderInfoDecl.m_fieldOffset];
    if(unlikely(!info))
        throwBadOrderInfoException("Order::get: no info of type: ", typeid(InfoClass));
    return static_cast<InfoClass&>(*info);
}

template<typename InfoClass>
    InfoClass &Order::add()
{
    OrderInfoSlot *&info = m_infos[InfoClass::ms_orderInfoDecl.m_fieldOffset];
    if(!info)
    {
        InfoClass *c = new InfoClass();
        info = c;
        return *c;
    }
    else
        return static_cast<InfoClass&>(*info);
}

template<typename InfoClass>
    InfoClass &Order::construct()
{
    OrderInfoSlot *&info = m_infos[InfoClass::ms_orderInfoDecl.m_fieldOffset];
    if(unlikely(info))
        throwBadOrderInfoException("Order::construct: info of given type already exists: ", typeid(InfoClass));
    InfoClass *c = new InfoClass();
    info = c;
    return *c;
}

template<typename InfoClass, typename CInfo>
    InfoClass &Order::construct(const CInfo &cinfo)
{
    OrderInfoSlot *&info = m_infos[InfoClass::ms_orderInfoDecl.m_fieldOffset];
    if(unlikely(info))
        throwBadOrderInfoException("Order::construct: info of given type already exists: ", typeid(InfoClass));

    InfoClass *c = new InfoClass(cinfo);
    info = c;
    return *c;
}

template<typename InfoClass, typename CInfo1, typename CInfo2>
InfoClass &Order::construct(const CInfo1 &cinfo1, const CInfo2 &cinfo2)
{
    OrderInfoSlot *&info = m_infos[InfoClass::ms_orderInfoDecl.m_fieldOffset];
    if(unlikely(info))
        throwBadOrderInfoException("Order::construct: info of given type already exists: ", typeid(InfoClass));

    InfoClass *c = new InfoClass(cinfo1, cinfo2);
    info = c;
    return *c;
}


template<typename InfoClass>
bool Order::hasInfo() const
    { return m_infos[InfoClass::ms_orderInfoDecl.m_fieldOffset] != 0; }

template<typename InfoClass>
    void Order::destroy()
{
    OrderInfoSlot *&info = m_infos[InfoClass::ms_orderInfoDecl.m_fieldOffset];
    if(unlikely(!info))
        throwBadOrderInfoException("Order::destroy: info of given type does not exist: ", typeid(InfoClass));

    delete info;
    info = NULL;
}

// specialize for the inline-allocated Infos
template<>
    inline boost::enable_if<boost::is_const<const OrderInfo>, const OrderInfo>::type &
        Order::get<const OrderInfo>() const
            { return m_orderInfo; }
template<>
    inline boost::disable_if<boost::is_const<OrderInfo>, OrderInfo>::type &
        Order::get<OrderInfo>()
            { return m_orderInfo; }
template<>
    inline boost::enable_if<boost::is_const<const IssuedOrderInfo>, const IssuedOrderInfo>::type &
        Order::get<const IssuedOrderInfo>() const
            { return m_issuedInfo; }
template<>
    inline boost::disable_if<boost::is_const<IssuedOrderInfo>, IssuedOrderInfo>::type &
        Order::get<IssuedOrderInfo>()
            { return m_issuedInfo; }
template<>
    inline boost::enable_if<boost::is_const<const OrderListenerInfo>, const OrderListenerInfo>::type &
        Order::get<const OrderListenerInfo>() const
            { return m_listenerInfo; }
template<>
    inline boost::disable_if<boost::is_const<OrderListenerInfo>, OrderListenerInfo>::type &
        Order::get<OrderListenerInfo>()
            { return m_listenerInfo; }
template<>
    inline bool Order::hasInfo<OrderInfo>() const
        { return true; }
template<>
    inline bool Order::hasInfo<IssuedOrderInfo>() const
        { return m_issuedInfo.initialized(); }
template<>
    inline bool Order::hasInfo<OrderListenerInfo>() const
        { return m_listenerInfo.initialized(); }
template<>
    inline OrderListenerInfo &Order::add<OrderListenerInfo>()
{
    m_listenerInfo.setInitialized();
    return m_listenerInfo;
}

inline OrderInfo &Order::orderInfo()
    { return get<OrderInfo>(); }
inline const OrderInfo &Order::orderInfo() const
    { return get<const OrderInfo>(); }
inline IssuedOrderInfo &Order::issuedInfo()
    { return get<IssuedOrderInfo>(); }
inline const IssuedOrderInfo &Order::issuedInfo() const
    { return get<const IssuedOrderInfo>(); }
inline OrderListenerInfo &Order::listeners()
    { return add<OrderListenerInfo>(); }

inline bool Order::isIssued() const { return hasInfo<IssuedOrderInfo>(); }

inline IssuedOrderInfo &Order::constructIssuedOrderInfo(Order *parent, uint32_t orderid, timeval_t const &issuedTv, uint32_t remaining_size, double priceAtMarket)
{
    if(unlikely(m_issuedInfo.initialized()))
        throwBadOrderInfoException("Order::construct: info of given type already exists: ", typeid(IssuedOrderInfo));
    m_issuedInfo.initialize(parent, orderid, issuedTv, remaining_size, priceAtMarket);
    return m_issuedInfo;
}

inline OrderInfo &OrderInfo::setInstrument( const instrument_t &instr )
{
    if(unlikely(m_frozen)) throwFrozenException();
    m_instr = instr;
    return *this;
}

inline OrderInfo &OrderInfo::setPrice( double px )
{
    if(unlikely(m_frozen)) throwFrozenException();
    m_px = px;
    return *this;
}

inline OrderInfo &OrderInfo::setDesiredSize( int sz )
{
    if(unlikely(m_frozen)) throwFrozenException();
    m_sz = sz;
    return *this;
}

inline OrderInfo &OrderInfo::setMktDest( mktdest_t mkt )
{
    if(unlikely(m_frozen)) throwFrozenException();
    m_dest = mkt;
    return *this;
}

inline OrderInfo &OrderInfo::setDir( dir_t d )
{
    if(unlikely(m_frozen)) throwFrozenException();
    m_dir = d;
    return *this;
}

inline OrderInfo &OrderInfo::setTimeInForce( tif_t tif )
{
    if(unlikely(m_frozen)) throwFrozenException();
    m_tif = tif;
    return *this;
}

inline OrderInfo &OrderInfo::setTimeout( uint32_t tout )
{
    if(unlikely(m_frozen)) throwFrozenException();
    m_timeout = tout;
    return *this;
}

inline OrderInfo &OrderInfo::setVisible( bool vis )
{
    if(unlikely(m_frozen)) throwFrozenException();
    m_oflags = ( m_oflags & ~OFLAG_INVISIBLE ) | ( !vis ? OFLAG_INVISIBLE : OFLAG_NONE );
    return *this;
}

inline OrderInfo &OrderInfo::setProactive( bool pro )
{
    if(unlikely(m_frozen)) throwFrozenException();
    m_oflags = ( m_oflags & ~OFLAG_PROACTIVE ) | ( pro ? OFLAG_PROACTIVE : OFLAG_NONE );
    return *this;
}

inline OrderInfo &OrderInfo::setStabilize( bool stab )
{
    if(unlikely(m_frozen)) throwFrozenException();
    m_oflags = ( m_oflags & ~OFLAG_STABILIZE ) | ( stab ? OFLAG_STABILIZE : OFLAG_NONE );
    return *this;
}

inline OrderInfo &OrderInfo::setDarkPool( bool dark )
{
    if(unlikely(m_frozen)) throwFrozenException();
    m_oflags = ( m_oflags & ~OFLAG_DARK_POOL ) | ( dark ? OFLAG_DARK_POOL : OFLAG_NONE );
    return *this;
}

inline OrderInfo &OrderInfo::setISO( bool iso )
{
    if(unlikely(m_frozen)) throwFrozenException();
    m_oflags = ( m_oflags & ~OFLAG_ISO ) | ( iso ? OFLAG_ISO : OFLAG_NONE );
    return *this;
}

inline OrderInfo &OrderInfo::setCancelOrigOnReject( bool cancelOrigOnReject )
{
    if(unlikely(m_frozen)) throwFrozenException();
    m_oflags = ( m_oflags & ~OFLAG_CANCEL_ORIG_ON_REJECT ) |
        ( cancelOrigOnReject ? OFLAG_CANCEL_ORIG_ON_REJECT : OFLAG_NONE );
    return *this;
}

inline OrderInfo &OrderInfo::setFlags( uint32_t flags )
{
    if(unlikely(m_frozen)) throwFrozenException();
    m_oflags = flags;
    return *this;
}

inline OrderInfo &OrderInfo::addFlag( uint32_t flag )
{
    if(unlikely(m_frozen)) throwFrozenException();
    m_oflags |= flag;
    return *this;
}


inline OrderInfo &OrderInfo::setReserveSize( uint32_t size )
{
    if(unlikely(m_frozen)) throwFrozenException();
    m_reserver_sz = size;
    return *this;
}

inline int AggressionComp::cmp(const OrderCPtr &o1, const OrderCPtr &o2) const
{
    BB_ASSERT(-dir2sign(o1->orderInfo().getDir()) == m_sideSign);
    BB_ASSERT(-dir2sign(o2->orderInfo().getDir()) == m_sideSign);

    double p1 = o1->orderInfo().getPrice(),
           p2 = o2->orderInfo().getPrice();
    if(NE(p1, p2))
    {
        // since we know they aren't equal within epsilon, can use the builtin gt operator
        if (p1 > p2)
            return m_sideSign; // buy cmp: more aggressive, sell cmp: less aggressive
        else
            return -m_sideSign;
    }

    mktdest_t dest1 = o1->orderInfo().getMktDest(),
              dest2 = o2->orderInfo().getMktDest();
    if(dest1 != dest2)
        return (dest1 > dest2) ? 1 : -1;

    if( o1->isIssued() && o2->isIssued() )
    {
        // if the order oid1 is lower than oid2 it is assumed
        // oid1 was placed first and thus has time priority
        const unsigned int oid1 = o1->issuedInfo().getOrderid();
        const unsigned int oid2 = o2->issuedInfo().getOrderid();

        if( oid1 != oid2 )
        {
            return ( oid1 < oid2 ) ? -1 : 1; // -1 is more aggressive. see AggressionComp documentation above
        }
    }
    return 0;
}

typedef PreallocatedObjectFactory<Order>      OrderFactory;
BB_DECLARE_INTRUSIVE_PTR( OrderFactory );

} // namespace bb
} // namespace trading

#endif // BB_TRADING_ORDER_H

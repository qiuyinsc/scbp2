#ifndef BB_TRADING_PNLTRACKER_H
#define BB_TRADING_PNLTRACKER_H

/* Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved. */

#include <boost/noncopyable.hpp>
#include <bb/clientcore/Book.h>
#include <bb/trading/FillDequeLedger.h>
#include <bb/trading/PnLRecordset.h>

namespace bb {
namespace trading {

/// forward decl
class PnLTracker;
BB_DECLARE_SHARED_PTR( PnLTracker );

/// A PnLTracker tracks the realized and unrealized PnL for a single asset. Internally there is a ledger
/// of fillrecords which the addFill() method accesses. On every addFill() a new fillrecord is compared against
/// the ledger and if PnL is realized a PnLRecord is generated and the ledgers updated (by updating/removing
/// fillrecords). Unrealized PnL is the current m2m of the positions on the ledger.
class PnLTracker : private boost::noncopyable
{
public:
    friend class PnLProvider;
    static PnLTrackerPtr create(const instrument_t& instr, double InitPnL, double PxIncrement, double ValuePerIncrement, currency_t reporting_cur = currency_t::UNKNOWN);
    static PnLTrackerPtr create(const instrument_t& instr
        , double delta_underlying
        , double ValuePerIncrement
		, currency_t reporting_cur = currency_t::UNKNOWN);
    virtual ~PnLTracker() {}

    /// addFill determines if PnL is realized and updates the fill record history as nec'y
    bool addFill( const FillRecord& fillrecord );

    /// getters off the ctor
    instrument_t getInstrument() const { return m_instr; }
    double getPxIncrement() const { return m_pxinc * m_fxRate; }
    double getValuePerIncrement() const { return m_valperinc * m_fxRate; }
    double getPxChangeCoeff() const { return m_dpxcoeff; }

    double getFxRate() const { return m_fxRate; }

    /// Returns PnLTrack's est of the pos
    int32_t getPosition() const;

    /// Returns the latest value of cummulative realized pnl
    double getRealizedPnL() const { return m_rpnl * m_fxRate; }

    /// Returns unrealized pnl using last seen bid/offer levels (useful only after a fill, otherwise unreliable results)
    double getUnrealizedPnL() const;

    /// Returns unrealized pnl assuming all positions are closed at the mid
    double getUnrealizedPnL( double mid ) const;

    /// Returns unrealized pnl assuming LONGs is closed on the bid and SHORTs are closed on the offer (worst case)
    double getUnrealizedPnL( double bid, double offer) const;
    double getUnrealizedPnL( const IBookPtr& book) const
    {
        return getUnrealizedPnL( book->getNthSide( 0, BID ).getPrice(), book->getNthSide( 0, ASK ).getPrice() );
    }

    double getTotalPnL( const IBookPtr& book ) const
    {
        return getUnrealizedPnL( book ) + getRealizedPnL();
    }
    double getTotalPnL( double mid ) const
    {
        return getUnrealizedPnL( mid ) + getRealizedPnL();
    }

    /// Return total fees by adding fees from FillRecords
    double getFees() const { BB_THROW_EXASSERT_SSX( !bb::EQZ(m_fxRate), "fxrate is 0"); return m_fees * m_fxRate; }

    /// Returns the underlying ledger
    IFillLedger const & getLedger() const { return m_ledger; }

    /// Returns the full PnLRecord set
    PnLRecords const & getPnLRecords() const { return m_pnlRecords; }

    /// Reads a saved state of the PNL Tracker from disk.  Does not restore previous bid/offer markets, etc.
    /// Restores ledger and pnl records.
    bool load( const char* filename );

    /// Writes the current state of the PNL Tracker to disk.  Doesn't store quotes, only FillRecords and
    /// PNLRecords.
    bool save( const char* filename ) const;

protected:
    PnLTracker(const instrument_t& instr, double InitPnL, double PxIncrement, double ValuePerIncrement, currency_t reporting_cur = currency_t::UNKNOWN);

private:

    // from ctor
    const instrument_t m_instr;
    const double m_initpnl;
    const double m_pxinc;
    const double m_valperinc;
    const double m_dpxcoeff;

    // running values
    double m_rpnl;
    mutable double m_lastbid;
    mutable double m_lastoffer;
    double m_fees;
    const double m_fxRate;

    mutable FillDequeLedger m_ledger;
    mutable PnLRecords m_pnlRecords;

    double incrementRealizedPnL(double fillsz, double fillpx, double opptpx, dir_t dir);

};

BB_DECLARE_SHARED_PTR( PnLTracker );


}    // trading
}    // bb

#endif // BB_TRADING_PNLTRACKER_H

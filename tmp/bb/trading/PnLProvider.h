#ifndef BB_TRADING_PNLPROVIDER_H
#define BB_TRADING_PNLPROVIDER_H

/* Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved. */


#include "PnLTracker.h"

#include <iostream>
#include <set>
#include <sstream>

#include <boost/foreach.hpp>

#include <bb/trading/TradingContext.h>
#include <bb/trading/IssuedOrderTracker.h>
#include <bb/trading/FillInfo.h>
#include <bb/trading/IFillListener.h>
#include <bb/trading/IPnLListener.h>
#include <bb/trading/PnLTracker.h>
#include <bb/clientcore/ClientContext.h>
#include <bb/db/CurrencyInfo.h>


namespace bb {
namespace trading {

class PnLProvider;
BB_FWD_DECLARE_SHARED_PTR(PnLProvider);
// This class is adaptation on top of PnLTracker class with IFillListener Implementation so that it can keep track of the arriving fill messages.

class PnLProvider
    : protected IFillListener
{
public:
    static PnLProviderPtr create( TradingContext* trading_context, const instrument_t& instr );
    virtual ~PnLProvider();

    virtual double getFees() const;
    virtual double getUnrealizedPnL() const;
    virtual double getBookUnrealizedPnL() const;
    virtual double getRealizedPnL() const;
    virtual double getTotalPnL() const;
    virtual double getBookTotalPnL() const;
    virtual double getNetPnL() const;
    virtual double getBookNetPnL() const;

    const instrument_t getInstrument() const { return m_instr; }

    // This is exposed to support some of the functions in portfolioTracker that need access to PnLtracker
    PnLTracker const & getPnLTracker() const { return m_pnlTracker; }

    /// add PnLListener
    void addPnLListener( IPnLListener* listener, const Priority& priority = PRIORITY_TRADING_DEFAULT ) const;

    /// remove PnLListener
    void removePnLListener( IPnLListener* listener ) const;

    // notify Listeners
    void notifyPnLListeners( const OrderPtr& order ) const;

    // load PnL from a file
    virtual void load();

    // Save the pnl to a file
    virtual void save() const;

protected:
    //if forceSaveOnDtor is false, we will save to disk only if PnlTracker->getPnLRecords return non empty set
    PnLProvider( TradingContext* tradingContext, const instrument_t& instr );

    virtual void onFill( const bb::trading::FillInfo & info );


     // This function generates name of the file
    const std::string getFilename( const bb::instrument_t& instr ) const;

    IntrusiveWeakPtr<TradingContext> m_tradingContext;
    const instrument_t m_instr;
    IBookPtr m_book;

    typedef PrioritizedListenerList<IPnLListener*> PnLListeners;
    mutable PnLListeners m_pnlListeners;

    const std::string m_id;
    const bb::date_t m_today;
    const double m_lotsize;
    PnLTracker m_pnlTracker;
};


BB_DECLARE_SHARED_PTR( PnLProvider );

} // namespace trading
} // namespace bb

#endif // BB_TRADING_PNLPROVIDER_H

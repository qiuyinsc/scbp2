#ifndef BB_TRADING_PORTFOLIOPOSITIONTRACKER_H
#define BB_TRADING_PORTFOLIOPOSITIONTRACKER_H

/* Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved. */


#include <boost/optional.hpp>
#include <bb/core/ListenerList.h>
#include <bb/core/source.h>
#include <bb/trading/PositionTracker.h>

namespace bb {

class PortfolioEffectivePositionUpdateMsg;
class PortfolioSetTraderModeMsg;
BB_FWD_DECLARE_SHARED_PTR(MsgHandler);

namespace trading {

class PortfolioPositionTrackerFactory;

/** Tracks a portfolio effective position in addition to standard position tracking
 *
 * assumes message stream comes from event distributor
 *
 * Example:
 * @code
 * class EffectivePositionListener : public IPositionListener
 * {
 * public:
 *     virtual void onPositionUpdated(IPositionProvider *provider)
 *     {
 *         int32_t pos = tracker->getEffectivePosition();
 *     }
 * };
 *
 * source_t src = m_configFile.feedToSource( SRC_PORTFOLIO_SERVER );
 * PortfolioPositionTrackerFactoryPtr factory( new PortfolioPositionProviderFactory( src ) );
 * PortfolioPositionTrackerPtr tracker = boost::dynamic_pointer_cast<PortfolioPositionTracker>(
 *     factory->getPositionProvider( instr ) );
 *
 * EffectivePositionListener* listener = new EffectivePositionListener;
 * Subscription sub;
 * tracker->addListener( sub, listener );
 *
 * @endcode
 *
 */

class PortfolioPositionTracker : public PositionTracker
{
public:
    PortfolioPositionTracker(
            boost::optional<source_t> infoSource,
            boost::optional<source_t> effectivePositionSource,
            const bb::instrument_t& instr,
            acct_t account,
            EventDistributorPtr eventDist,
            ITimeProviderCPtr spTimeProv,
            IAlertPtr alert,
            const PositionTracker::TradingContextMode mode,
            bool activeByDefault,
            int32_t maxPosition,
            int verboseLevel );
    virtual ~PortfolioPositionTracker();

    virtual int32_t getEffectivePosition() const;

    void setPositionServerActive( bool position_server_active );

protected:
    void onEffectivePositionUpdate( const PortfolioEffectivePositionUpdateMsg& msg );
    void onSetTraderMode( const PortfolioSetTraderModeMsg& msg );

    void clampEffectivePosition() const;

private:
    ITimeProviderCPtr m_spTimeProv;
    int m_verboseLevel;
    boost::optional<timeval_t> m_lastTimeval;
    mutable boost::optional<timeval_t> m_lastAlertTimeval;
    bool m_positionServerActive;
    MsgHandlerPtr m_portfolioHandlerRef, m_modeHandlerRef, m_modeHandlerRef2;
    mutable int32_t m_effectivePosition;
    int32_t m_maxPosition;
};

BB_DECLARE_SHARED_PTR( PortfolioPositionTracker );

} // namespace bb
} // namespace trading

#endif // BB_TRADING_PORTFOLIOPOSITIONTRACKER_H

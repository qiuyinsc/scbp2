#ifndef BB_TRADING_SIMPLEORDERVALIDATOR_H
#define BB_TRADING_SIMPLEORDERVALIDATOR_H

/* Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved. */

#include <bb/core/smart_ptr.h>
#include <bb/trading/IOrderValidator.h>


namespace bb {
namespace trading {

class SimpleOrderValidator : public IOrderValidator, public boost::noncopyable
{
public:
    SimpleOrderValidator();
    virtual ~SimpleOrderValidator() {}

    /// Returns true if desired_order_list as a whole is a valid order set under limits.
    /// Returns false if not.
    virtual bool validateDesiredOrderList(side_t side, const OrderList* desired_order_list) const;

    /// Returns true if, given the actual issued orders we have out
    /// (from oissued_tracker), issuing this desired_order is allowed
    /// under limits.
    /// Returns false otherwise.
    virtual bool validateOrderBeforeIssue(side_t side, OrderCPtr desired_order) const;

    /// If/when an order gets rejected, this is the human-readable reason.
    virtual const std::string& getLastReason() const;

    void setMaxPx(double _max_px);

    /// sets m_min_px. no error checking
    void setMinPx(double _min_px);

protected:
    bool fail( const std::string& reason ) const
    {
        m_reason = reason;
        return false;
    }

    mutable std::string m_reason;
    double m_min_px, m_max_px;
};
BB_DECLARE_SHARED_PTR( SimpleOrderValidator );

} // namespace trading
} // namespace bb



#endif // BB_TRADING_SIMPLEORDERVALIDATOR_H

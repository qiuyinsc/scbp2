#ifndef BB_TRADING_ITHRESHOLDADJUSTER_H
#define BB_TRADING_ITHRESHOLDADJUSTER_H

/* Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved. */


#include <boost/bind.hpp>

#include <bb/core/side.h>
#include <bb/core/smart_ptr.h>
#include <bb/core/ListenerList.h>

namespace bb {
namespace trading {

class IThresholdAdjuster;

class IThresholdAdjusterListener
{
public:
    virtual ~IThresholdAdjusterListener() {}

    virtual void onThresholdUpdate( const IThresholdAdjuster* ) = 0;
};

/// @interface IThresholdAdjuster
/// Interface for objects used to adjust
/// thresholds for maximum buy price or minimum sell price. Thresholds
/// are typically compared to a price prediction in deciding whether
/// orders should be placed/cancelled.
class IThresholdAdjuster : public ListenerList<IThresholdAdjusterListener*>
{
public:
    virtual ~IThresholdAdjuster() {}

    /// Returns an adjustment factor typically applied to a threshold
    /// for determining max buy price or min sell price.
    /// Semantics: For either side, a positive result should be interpreted as making it
    ///   tougher to do a trade in that direction, and a negative result as making it
    ///   easier to trade in that dir.
    virtual double getThresholdAdjustment(side_t side) const = 0;

protected:
    void notifyChange()
    {
        notify( boost::bind( &IThresholdAdjusterListener::onThresholdUpdate, _1, this ) );
    }
};

BB_DECLARE_SHARED_PTR( IThresholdAdjuster );

} // trading
} // bb


#endif // BB_TRADING_ITHRESHOLDADJUSTER_H

#ifndef BB_TRADING_JSONTYPES_H
#define BB_TRADING_JSONTYPES_H

/* Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved. */


#include <json_spirit.h>
#include <bb/core/smart_ptr.h>
#include <bb/core/JsonTypes.h>

namespace bb {

namespace trading {
BB_FWD_DECLARE_INTRUSIVE_PTR( Order );
BB_FWD_DECLARE_SHARED_PTR( BaseTrader );
BB_FWD_DECLARE_SHARED_PTR( IssuedOrderTracker );
BB_FWD_DECLARE_SHARED_PTR( InstrumentContext );
BB_FWD_DECLARE_SHARED_PTR( TradingContext );

} // namespace trading

// most of the time users are working with OrderPtrs, but I included this
// specialized case for those times when you have the actual Order obj
template<>
json_spirit::mValue toJsonValue( const bb::trading::Order& );

template<>
json_spirit::mValue toJsonValue( const bb::trading::OrderPtr& );

template<>
json_spirit::mValue toJsonValue( const bb::trading::BaseTraderPtr& );

template<>
json_spirit::mValue toJsonValue( const bb::trading::IssuedOrderTrackerPtr& );

template<>
json_spirit::mValue toJsonValue( const bb::trading::InstrumentContextPtr& );

template<>
json_spirit::mValue toJsonValue( const bb::trading::TradingContextPtr& );

template<>
bb::trading::OrderPtr fromJsonValue<bb::trading::OrderPtr>( const json_spirit::mValue& );

bb::trading::OrderPtr orderPtrFromJsonValue( const json_spirit::mValue& );
json_spirit::mValue orderToJsonValue( const bb::trading::OrderPtr& );
json_spirit::mValue tradingContextToJsonValue( const bb::trading::TradingContextPtr& context );


} // namespace bb

#endif // BB_TRADING_JSONTYPES_H

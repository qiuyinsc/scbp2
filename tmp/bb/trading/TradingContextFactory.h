#ifndef BB_TRADING_TRADINGCONTEXTFACTORY_H
#define BB_TRADING_TRADINGCONTEXTFACTORY_H

/* Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved. */


#include <bb/core/smart_ptr.h>

namespace bb {
namespace trading {

class TradingSetup;
BB_FWD_DECLARE_SHARED_PTR(TradingContext);

class ITradingContextFactory
{
public:
    virtual ~ITradingContextFactory() {}
    virtual TradingContextPtr create( const TradingSetup* setup ) = 0;
};

BB_DECLARE_SHARED_PTR( ITradingContextFactory );

} // bb
} // trading

#endif // BB_TRADING_TRADINGCONTEXTFACTORY_H

#ifndef BB_TRADING_FILLFEESDOTLUAFEEPROVIDER_H
#define BB_TRADING_FILLFEESDOTLUAFEEPROVIDER_H

/* Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved. */


#include <luabind/function.hpp>
#include <bb/trading/IFeeProvider.h>
#include <bb/trading/IFeeProviderFactory.h>

namespace bb {

BB_FWD_DECLARE_SHARED_PTR( LuaState );
BB_FWD_DECLARE_SHARED_PTR( SpamFilter );

namespace trading {

class FillFeesDotLuaFeeProvider : public IFeeProvider
{

public:
    FillFeesDotLuaFeeProvider(
        TradingContext* tradingContext,
        const std::string& fillFeesLua,
        const std::string& fillFeesEntryPoint );

    virtual ~FillFeesDotLuaFeeProvider();

    virtual IFillListener::FeeInfo calculateFees( const OrderPtr& order, const TdFillMsg& msg );
    virtual IFillListener::FeeInfo calculateFees( const OrderPtr& order, const TdFillFutureMsg& msg );
    virtual IFillListener::FeeInfo calculateFees( const OrderPtr& order, const TdFillNewMsg& msg );

private:
    template<typename T>
    IFillListener::FeeInfo calculateFeesT( const OrderPtr& order, const T& msg );

    const LuaStatePtr m_luaState;
    luabind::object m_feesFunction;
    const SpamFilterPtr m_spamFilter;
};
BB_DECLARE_SHARED_PTR(FillFeesDotLuaFeeProvider);

class FillFeesDotLuaFeeProviderFactory : public IFeeProviderFactory
{
    static const std::string kDefaultFillFeesDotLua;
    static const std::string kDefaultEntryPoint;

public:
    inline FillFeesDotLuaFeeProviderFactory()
        : m_fileName( kDefaultFillFeesDotLua )
        , m_entryPoint( kDefaultEntryPoint ) {}

    virtual ~FillFeesDotLuaFeeProviderFactory();

    inline void setFillFeesFile( const std::string& fileName )
    {
        m_fileName = fileName;
    }

    inline void setEntryPoint( const std::string& entryPoint )
    {
        m_entryPoint = entryPoint;
    }

    virtual IFeeProviderPtr create(
        TradingContext* tradingContext,
        const instrument_t& );

private:
    std::string m_fileName;
    std::string m_entryPoint;

    // fillfees.lua should be able to handle any instrument, so we can
    // just return the same provider for every instrument.
    IFeeProviderPtr m_cachedResult;
};
BB_DECLARE_SHARED_PTR(FillFeesDotLuaFeeProviderFactory);

} // namespace trading
} // namespace bb

#endif // BB_TRADING_FILLFEESDOTLUAFEEPROVIDER_H

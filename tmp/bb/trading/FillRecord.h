#ifndef BB_TRADING_FILLRECORD_H
#define BB_TRADING_FILLRECORD_H

/* Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved. */


#include <cstring>

#include <bb/core/timeval.h>
#include <bb/core/side.h>
#include <bb/core/smart_ptr.h>
#include <bb/trading/IFillListener.h>

namespace bb {
namespace trading {

class Order;

/// FillRecord is a semi-persistent record of a fill. A FillRecord is many to one in that many fills
/// can arrive for a single order id. Currently PositionTracker retains the lastest FillRecord,
/// and the PnLProvider keeps a FillRecord until it is matched by a another FillRecord when booking
/// realized PnL.
class FillRecord
{
public:
    /// Use of the default ctor is discouraged but left for legacy
    FillRecord();
    FillRecord(const FillRecord&);

    /// Best ctor of FillRecord
    FillRecord(const Order& isso);

    /// Best ctor of FillRecord
    FillRecord(const Order& isso, const IFillListener::FeeInfo& fees);

    /// ctor arg list reflects current object members, but may change
    FillRecord(double fill_px, uint32_t fill_sz, timeval_t fill_tv, int oid, dir_t dir);

    /// Price of last fill
    double getFillPx() const { return m_fillpx; }

    /// Size of last fill
    uint32_t getFillSz() const { return m_fillsz; }

    void setFillSz( uint32_t fill_sz ) { m_fillsz = fill_sz; }

    /// This method is needed by the FillLedger when order matching for realized pnl
    bool setUnmatchedSz(uint32_t new_unmatched_sz);

    /// Size of remaining matched fill (used for realized PnL matching)
    uint32_t getUnmatchedSz() const { return m_unmatchedsz; }

    /// Get Fee object
    const IFillListener::FeeInfo& getFees() const { return boost::cref(m_fees); }

    /// Fill tv. What this tv represents is implementation dependent (bb time-stamp, ecn time-stamp, etc)
    timeval_t getFillTv() const { return m_filltv; }

    /// oid of IssuedOrder
    int getOID() const { return m_oid; }

    /// Order direction or type dir_t
    dir_t getDir() const { return m_dir; }

    /// For pretty-print
    std::ostream& print(std::ostream& out) const;

    ~FillRecord() {}

    void unserialize( const char* src, size_t size ) { memcpy( this, src, size ); }
    void serialize( void* dest ) const { memcpy( dest, this, sizeof(FillRecord) ); }

private:
    double m_fillpx;
    uint32_t m_fillsz;
    uint32_t m_unmatchedsz;
    timeval_t m_filltv;
    int m_oid;
    dir_t m_dir;
    IFillListener::FeeInfo m_fees;
};

BB_DECLARE_SHARED_PTR( FillRecord );

/// Overload << oper for pretty-print
inline std::ostream &operator <<(std::ostream &out, const FillRecord &o) { return o.print( out ); }

}    // trading
}    // bb

#endif // BB_TRADING_FILLRECORD_H

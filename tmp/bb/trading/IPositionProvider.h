#ifndef BB_TRADING_IPOSITIONPROVIDER_H
#define BB_TRADING_IPOSITIONPROVIDER_H

/* Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved. */

#include <string>
#include <utility>

#include <boost/noncopyable.hpp>
#include <boost/optional.hpp>

#include <bb/core/acct.h>
#include <bb/core/timeval.h>
#include <bb/core/smart_ptr.h>
#include <bb/core/shared_vector.h>
#include <bb/core/side.h>
#include <bb/core/Subscription.h>
#include <bb/core/instrument.h>

#include <bb/trading/IFillListener.h>

namespace bb {

namespace trading {

class IPositionListener;
class Order;

/// @interface
/// Interface for getting a position
class IPositionProvider : public boost::noncopyable
{
public:
    typedef Subscription ListenerSubPtr;

    virtual ~IPositionProvider() {}

    /// Returns true if the position is known.
    virtual bool isReady() const = 0;

    /// Returns the instrument whose position is tracked by this.
    virtual const instrument_t& getInstrument() const = 0;

    virtual acct_t getAcct() const = 0;

    /// Returns the absolute position, which is the position we are actually holding.
    /// Most trading programs should use getEffectivePosition() instead.
    virtual int32_t getAbsolutePosition() const = 0;

    /// Returns the effective position; in general, the meaning of this number is:
    /// the trader should behave as if we have this many shares of this stock.
    /// If the TD "baseline" position is nonzero (i.e. for hedging or a discretionary position,
    /// this might not equal getAbsolutePosition()).
    virtual int32_t getEffectivePosition() const = 0;

    virtual void addPositionListener( Subscription &outListenerSub, IPositionListener* listener ) = 0;

    /// When SHFE sends DONE message before the final FILL message, IssuedOrderTracker will call this function.
    virtual void setPendingPosition( int32_t p, double fill_px ) {}
    virtual int32_t getPendingPosition() const { return 0; }
    virtual double getPendingFillPrice() const { return 0.0; }

    /// returns current usage and the max for the trading limit related to this position
    /// -1 means this limit is not applicable
    typedef std::pair<int32_t,int32_t> TradingLimitPair;
    virtual TradingLimitPair getTradingLimit(const std::string& limit_id) const { return std::make_pair(-1, -1); }
};
BB_DECLARE_SHARED_PTR( IPositionProvider );

// Listener class for IPositionProvider
class IPositionListener
{
public:
    virtual ~IPositionListener() {}

    /// Called any time the IPositionProvider's position may have changed. Because there
    /// are different aspects of the position that can change (readyness, baseline pos,
    /// effective pos), you should make sure that what you care about has actually changed.
    virtual void onPositionUpdated( IPositionProvider* provider ) = 0;

    /// Called any time the trading limits related to this account and this instrument may have changed.
    /// This callback is optional as the trading limits are not relevant in all markets.
    virtual void onTradingLimitUpdated( IPositionProvider* provider ) {}
};

class PositionProvider : public IPositionProvider
{
public:
    PositionProvider( const instrument_t& instr )
        : m_instrument( instr ) {}
    virtual const instrument_t& getInstrument() const { return m_instrument; }
    virtual void addPositionListener( Subscription &outListenerSub, IPositionListener* listener )
        { m_listeners.push_back(outListenerSub, listener); }

    void notifyListeners()
    {
        Listeners_t::safe_jterator iter(m_listeners);
        while(iter.hasNext())
            iter.next()->onPositionUpdated( this );
    }

    void notifyTradingLimitUpdated()
    {
        Listeners_t::safe_jterator iter(m_listeners);
        while(iter.hasNext())
            iter.next()->onTradingLimitUpdated( this );
    }

protected:
    bb::instrument_t m_instrument;
    typedef shared_vector<IPositionListener*> Listeners_t;
    Listeners_t m_listeners;
};

/// The PositionTracker requires raw fill notifications from the IssuedOrderTracker.
/// This is the broader interface for PositionProviders which need that information;
/// any PositionProvider to be used through IssuedOrderTracker must implement this.
class IOTPositionProvider : public PositionProvider
{
public:
    IOTPositionProvider( const instrument_t& instr )
        : PositionProvider( instr ) {}

    /// Called by IssuedOrderTracker after a fill message
    virtual void handleFill( const timeval_t &tv, side_t side, double fill_px, uint32_t fill_qty,
                        const boost::optional<int32_t> &posFromMsg ) = 0;


};
BB_DECLARE_SHARED_PTR(IOTPositionProvider);

} // trading
} // bb


#endif // BB_TRADING_IPOSITIONPROVIDER_H

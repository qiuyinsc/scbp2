#ifndef BB_TRADING_ONESHOTBLINKPOSITIONACCUMULATOR_H
#define BB_TRADING_ONESHOTBLINKPOSITIONACCUMULATOR_H

/* Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved. */


#include <bb/trading/OneShotPositionAccumulator.h>

namespace bb {
namespace trading {

/// A OneShotBlinkPositionAccumulator is primarily used for the ORDER_SIZE_BLIND
/// policy.  It will take a snapshot of the desired_size - absolute_size at a
/// specified "blink" time, and use that as the desired order size at ORDER_SIZE
class OneShotBlinkPositionAccumulator : public OneShotPositionAccumulator
{
public:
    BB_DECLARE_SCRIPTING();

    /*
     * Params
     */

    struct ParamsBase : OneShotPositionAccumulator::ParamsBase
    {
        /// Constructor
        ParamsBase(const std::string& name);

        /// Prints the fields of this parameters struct
        virtual void printFields(std::ostream& out) const;

        /// Time to take a snapshot of the desired order - absolute size.
        timeval_t m_blink_tv;
    };

    struct Params : ParamsBase, IPositionAccumulatorParamsImpl<Params, OneShotBlinkPositionAccumulator>
    {
        Params();
    };

    BB_DECLARE_SHARED_PTR(Params);

public:
    /*
     * OneShotBlinkPositionAccumulator
     */

    OneShotBlinkPositionAccumulator(const ParamsBase& params, const std::string& desc, const instrument_t& instr,
        const trading::TradingContextPtr& spTradingContext,
        const IPriceProviderCPtr& spPhase1PriceProvider, const IPriceProviderCPtr& spPhaseXPriceProvider,
        int32_t desired_size, double px_offset, bool htb = false);

    virtual ~OneShotBlinkPositionAccumulator();

private:
    /// Hooks in the wakeup call for setting the target size
    void setBlinkWakeup();

    /// Sets the target size (desired size - absolute sz) at blink time
    void blinkWakeup();

private:
    /// Subscription token for blink wakeup
    Subscription m_blink_sub;

    /// Configurable parameters for this accumulator
    ParamsBase m_params; // base class has a sliced copy of this, which is a bit inefficient

    /// Target size at blink time for this accumulator
    uint32_t m_target_sz_at_blink;
};
BB_DECLARE_SHARED_PTR(OneShotBlinkPositionAccumulator);

/// Stream operator to convert OneShotBlinkPositionAccumulator::Params into
/// human readable form
std::ostream &operator<<(std::ostream& out, const OneShotBlinkPositionAccumulator::Params& p);

} // namespace trading
} // namespace bb

#endif // BB_TRADING_ONESHOTBLINKPOSITIONACCUMULATOR_H

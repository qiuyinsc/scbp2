#ifndef BB_TRADING_PRICEUTILS_H
#define BB_TRADING_PRICEUTILS_H

/* Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved. */


#include <bb/core/fpmath.h>
#include <bb/core/side.h>

namespace bb {
namespace trading {

inline double improve_price( side_t side, double price, double amt, double precision = 0.01 )
{
    int s = side2sign(side);

    return round_to_precision( price + ( s * amt ), precision );
}

inline double improve_price( dir_t dir, double price, double amt, double precision = 0.01 )
{
    int s = dir2sign(dir);

    return round_to_precision( price + ( s * amt ), precision );
}

inline double fade_price( side_t side, double price, double amt, double precision = 0.01 )
{
    int s = -side2sign(side);

    return round_to_precision( price + ( s * amt ), precision );
}

inline double fade_price( dir_t dir, double price, double amt, double precision = 0.01 )
{
    int s = -dir2sign(dir);

    return round_to_precision( price + ( s * amt ), precision );
}

///
/// returns the more inside of the two prices.
///
/// for example:
/// inner_price( ASK, 10, 20 ) will return 10, the more inside of the two prices.
///
inline double inner_price( side_t side, double price_0, double price_1 )
{
    return inside( side, price_0, price_1 ) ? price_0 : price_1;
}

///
/// returns the better price of the two prices given direction dir 'dir'
///
/// for example:
/// better_price( BUY, 10, 20 ) will return 10, the better buy price.
///
/// note the difference with inner_price(), which is the inside price given the side.
inline double better_price( dir_t dir, double p1, double p2 )
{
    if( dir == bb::BUY )
        return (p1 < p2) ? p1 : p2;
    else
        return (p1 > p2) ? p1 : p2;
}

///
/// syntatic sugar for rounding to nearest tick
///
inline double round_to_tick( double price, double tick )
{
    return round_to_precision( price, tick );
}


/// rounds the price to two decimal places.
inline double roundPrice( double px )
{
    return round( px * 100.0 ) / 100.0;
}

/// rounds the price to two decimal places.
inline double roundPriceUp( double px )
{
    return ceil( px * 100.0 ) / 100.0;
}

/// rounds the price to two decimal places.
inline double roundPriceDown( double px )
{
    return floor( px * 100.0 ) / 100.0;
}

/// rounds the size to the nearest parcel_sz (default = 100).
inline int32_t roundSize( int32_t sz, int32_t parcel_sz = 100 )
{
    return ( sz / parcel_sz ) * parcel_sz;
}

} // trading
} // bb

#endif // BB_TRADING_PRICEUTILS_H

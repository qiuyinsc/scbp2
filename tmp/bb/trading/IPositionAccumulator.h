#ifndef BB_TRADING_IPOSITIONACCUMULATOR_H
#define BB_TRADING_IPOSITIONACCUMULATOR_H

/* Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved. */


#include <boost/bind.hpp>
#include <bb/core/instrument.h>
#include <bb/core/Scripting.h>
#include <bb/core/ListenerList.h>
#include <bb/trading/trading.h>
#include <bb/trading/IFillListener.h>
#include <bb/trading/TradingContext.h>

/// macros to simplify logging in derived classes
#define TRD_LOG_PREFIX m_spTradingContext->getClockMonitor()->getTime() << "  " << m_desc << "  "
#define TRD_LOG_INFO   LOG_INFO  << TRD_LOG_PREFIX
#define TRD_LOG_WARN   LOG_WARN  << TRD_LOG_PREFIX
#define TRD_LOG_PANIC  LOG_PANIC << TRD_LOG_PREFIX

namespace bb {

BB_FWD_DECLARE_SHARED_PTR(IPriceProvider);

namespace trading {

class IPositionAccumulator;

class IPositionAccumulatorListener : public IFillListener
{
public:
    virtual void onPreSendOrder( const IPositionAccumulator*
        , const OrderPtr& order ) {}
    virtual void onPanic( const IPositionAccumulator* ) {}
};

class IPositionAccumulator
{
public:
    BB_DECLARE_SCRIPTING();

    virtual ~IPositionAccumulator() {}

    // "instrument" in Lua
    virtual const instrument_t &getInstrument() const = 0;

    // "desired_pos" in Lua
    virtual void setDesiredPosition( int32_t desired_pos ) = 0;
    virtual int32_t getDesiredPosition() const = 0;

    // "actual_pos" in Lua
    virtual int32_t getActualPosition() const = 0;

    // "active" in Lua
    virtual void setActive( bool active ) = 0;
    virtual bool isActive() const = 0;

    // "completed" in Lua
    virtual bool isCompleted() const = 0;

    // "px_offset" in Lua
    virtual void setPriceOffset( double px_offset ) = 0;
    virtual double getPriceOffset() const = 0;

    // max_buy_px in Lua
    // not necessarily honored by every accumulator
    // for example, a market-price accumulator cannot
    virtual void setMaxBuyPrice( double px ) = 0;
    virtual double getMaxBuyPrice() const = 0;

    // min_sell_px in Lua
    // not necessarily honored by every accumulator
    // for example, a market-price accumulator cannot
    virtual void setMinSellPrice( double px ) = 0;
    virtual double getMinSellPrice() const = 0;

    // Subscribes to get notified on the accmulator's fills.
    // WARNING: most accumulators don't implement this right now.
    virtual void subscribe( bb::Subscription& outListenerSub, IPositionAccumulatorListener* listener ) = 0;
};
BB_DECLARE_SHARED_PTR( IPositionAccumulator );


class PositionAccumulatorImpl : public IPositionAccumulator, public ListenerList<IPositionAccumulatorListener*>
{
public:
    virtual void subscribe( bb::Subscription& outListenerSub, IPositionAccumulatorListener* listener )
    {
        ListenerList<IPositionAccumulatorListener*>::subscribe( outListenerSub, listener );
    }
protected:
    void notifyFill( const bb::trading::FillInfo & info )
    {
        if(!empty())
        {
            notify( boost::bind( &IPositionAccumulatorListener::onFill, _1, boost::cref(info) ) );
        }
    }

    void notifyPreSendOrder( const OrderPtr& order )
    {
        if(!empty())
        {
            notify( boost::bind( &IPositionAccumulatorListener::onPreSendOrder
                                 , _1, this, boost::ref( order ) ) );
        }
    }

    void notifyPanic()
    {
        if(!empty())
        {
            notify( boost::bind( &IPositionAccumulatorListener::onPanic, _1, this ) );
        }
    }
};

class IPositionAccumulatorParams
{
public:
    enum ShortLabelPolicy {
        SHORT_LABEL_BY_POSITION,
        SHORT_LABEL_ALWAYS_SELL,
        SHORT_LABEL_ALWAYS_SHORT,
        SHORT_LABEL_UNLESS_HTB
    };

    enum OrderSizePolicy {
        ORDER_SIZE_DECAY, // size by position each time
        ORDER_SIZE_EQUAL, // size by position at activation
        ORDER_SIZE_BLIND, // totally ignore existing position
    };

    enum CancelOrdersPolicy {
        CANCEL_ORDERS_EVERY_INTERVAL,
        CANCEL_ORDERS_AFTER_PHASE,
        CANCEL_ORDERS_NEVER_EVER, // not during destruction, not ever
    };

    // for luabind
    struct ShortLabelPolicy_placeholder {};
    struct OrderSizePolicy_placeholder {};
    struct CancelOrdersPolicy_placeholder {};

    virtual ~IPositionAccumulatorParams() {}
    // factory method to create an accumulator using these params
    // The caller must guarantee that the params lives as long as the
    // accumulators do!
    virtual IPositionAccumulatorPtr create(
            const std::string& desc, const instrument_t& instr,
            const trading::TradingContextPtr& spTradingContext,
            const IPriceProviderCPtr &spPhase1PriceProvider,
            const IPriceProviderCPtr &spPhaseXPriceProvider,
            int32_t desired_pos, double px_offset, bool htb = false ) const = 0;

    virtual std::ostream& print( std::ostream& out ) const = 0;
    virtual bool check() const { return true; }
    virtual std::string getName() const = 0;
};
BB_DECLARE_SHARED_PTR( IPositionAccumulatorParams );


// class which implements Param's polymorphic functions using template arguments from derived classes
// first template parameter uses CRTP to make operator<< work; second is the strategy type to construct
template <typename Derived, typename Accumulator>
class IPositionAccumulatorParamsImpl : public IPositionAccumulatorParams
{
public:
    IPositionAccumulatorPtr create(
            const std::string& desc, const instrument_t& instr,
            const trading::TradingContextPtr &spTradingContext,
            const IPriceProviderCPtr &spPhase1PriceProvider,
            const IPriceProviderCPtr &spPhaseXPriceProvider,
            int32_t desired_size, double px_offset, bool htb = false ) const
            { return IPositionAccumulatorPtr( new Accumulator( derived(),
                    desc, instr, spTradingContext,
                    spPhase1PriceProvider, spPhaseXPriceProvider,
                    desired_size, px_offset, htb ) ); }
    std::ostream& print( std::ostream& out ) const { return out << derived(); }
    virtual std::string getName() const { return derived().m_name; }
private:
    const Derived& derived() const { return *static_cast<const Derived*>(this); }
};

inline std::ostream& operator <<( std::ostream& out, const IPositionAccumulatorParams& params )
{ return params.print( out ); }

std::ostream &operator <<( std::ostream &out, const IPositionAccumulatorParams::ShortLabelPolicy &p );
std::ostream &operator <<( std::ostream &out, const IPositionAccumulatorParams::OrderSizePolicy &p );
std::ostream &operator <<( std::ostream &out, const IPositionAccumulatorParams::CancelOrdersPolicy &p );

} // namespace trading
} // namespace bb

#endif // BB_TRADING_IPOSITIONACCUMULATOR_H

#ifndef BB_TRADING_PNLRECORDSET_H
#define BB_TRADING_PNLRECORDSET_H

/* Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved. */

#include <boost/container/vector.hpp>

#include <bb/core/timeval.h>
#include <bb/core/instrument.h>

namespace bb {
namespace trading {

class PnLTracker;

/// When PnL is realized a PnLRecord is created and two offsetting FillRecords (a buy against a sell)
/// are annihilated.
class PnLRecord
{
public:
    /// principal ctor
    PnLRecord(const timeval_t& tv, int oid, double fillpx, uint32_t fillsz, int pos, double rpnl, double cpnl, double upnl);
    PnLRecord(const PnLRecord& other);

    /// dtor
    ~PnLRecord() {}

    /// getters
    timeval_t getTv()            const { return m_tv; }
    int       getOID()           const { return m_oid; }
    double    getFillPx()        const { return m_fillpx; }
    uint32_t  getFillSz()        const { return m_fillsz; }
    int       getPos()           const { return m_pos; }
    double    getRealizedPnL()   const { return m_rpnl; }
    double    getCumulativePnL() const { return m_cpnl; }
    double    getUnrealizedPnL() const { return m_upnl; }

    void unserialize( const char* src, uint32_t size ) { memcpy( this, src, size ); }
    void serialize( void* dest ) const { memcpy(dest, this, sizeof(PnLRecord) ); }

    /// For pretty-print
    std::ostream& print(std::ostream& out) const;

protected:
    friend class PnLTracker;

private:
    inline PnLRecord()
    {
        std::memset(this, 0, sizeof(*this));
    }

    timeval_t m_tv;
    int       m_oid;
    double    m_fillpx;
    uint32_t  m_fillsz;
    int       m_pos;
    double    m_rpnl;
    double    m_cpnl;
    double    m_upnl;
};

BB_DECLARE_SHARED_PTR( PnLRecord );

typedef boost::container::vector<PnLRecord> PnLRecordset;
typedef PnLRecordset::iterator iPnLRecordset;
typedef PnLRecordset::const_iterator c_iPnLRecordset;

/// forward decl
class PnLRecords;
BB_DECLARE_SHARED_PTR( PnLRecords );

/// PnLRecords is a collection of PnLRecord(s) with some method decoration.
class PnLRecords
{
public:
    friend class PnLTracker;
    typedef PnLRecordset::iterator iterator;
    typedef PnLRecordset::const_iterator const_iterator;

    static PnLRecordsPtr create(const instrument_t& instr);
    virtual ~PnLRecords() {}

    /// getter
    instrument_t const & getInstrument() const { return m_instr; };

    /// Adds a PnLRecord object to the set
    void addPnLRecord(const PnLRecord& pnlrecord);
    void addPnLRecord(const timeval_t& tv, int oid, double fillpx, uint32_t fillsz, int pos, double rpnl, double cpnl, double upnl);

    iterator begin() { return m_pnlrecordset.begin(); }
    iterator end() { return m_pnlrecordset.end(); }

    const_iterator begin() const { return m_pnlrecordset.begin(); }
    const_iterator end() const { return m_pnlrecordset.end(); }

    /// T/F on internal rs being empty
    bool empty() const { return m_pnlrecordset.empty(); }

    /// Returns size of internal record set
    int size() const { return m_pnlrecordset.size(); }

    /// Returns a vector of pnl records
    PnLRecordset const & getPnLRecords() const;

    /// Returns the latest pnl record
    PnLRecord const & getTopRecord() const;

    /// For pretty-print
    virtual std::ostream& print(std::ostream& out) const;

protected:
    PnLRecords(const instrument_t& instr);

private:
    /// the recordset internal collection
    PnLRecordset m_pnlrecordset;

    const instrument_t m_instr;
};


/// Overload << oper for pretty-print
inline std::ostream &operator <<(std::ostream &out, const PnLRecord &o) { return o.print(out); }
inline std::ostream &operator <<(std::ostream &out, const PnLRecords &o) { return o.print(out); }

/// Overload equality for convenience
bool operator ==(const PnLRecord& pnla, const PnLRecord& pnlb);

}   // trading
}   // bb

#endif // BB_TRADING_PNLRECORDSET_H

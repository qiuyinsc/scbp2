#ifndef BB_TRADING_LEGACYREFDATAFACTORY_H
#define BB_TRADING_LEGACYREFDATAFACTORY_H

/* Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved. */


#include <bb/trading/ConstantRefDataFactory.h>

namespace bb {
namespace trading {

// The LegacyRefDataFactory is a ConstantRefDataFactory which always
// returns a RefData initialized with make_auto'ed sources
// cooresponding to SRC_ISLD, SRC_ARCA, and SRC_BATS. These were the
// original defaults for the defaultReferenceData in the trading
// context. Some old strategies may still rely on having those sources
// defaulted, and this factory provides an easy way to recover that
// setup.

class LegacyRefDataFactory : public ConstantRefDataFactory
{
public:
    LegacyRefDataFactory();
    virtual ~LegacyRefDataFactory();
};
BB_DECLARE_SHARED_PTR(LegacyRefDataFactory);

} // namespace trading
} // namespace bb

#endif // BB_TRADING_LEGACYREFDATAFACTORY_H

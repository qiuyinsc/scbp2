#ifndef BB_TRADING_IVOLUMEPROVIDER_H
#define BB_TRADING_IVOLUMEPROVIDER_H

/* Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved. */


#include <iosfwd>
#include <bb/trading/trading.h>
#include <bb/trading/IOrderStatusListener.h>
#include <bb/clientcore/TickProvider.h>
#include <bb/clientcore/IBook.h>
#include <bb/core/instrument.h>
#include <bb/core/side.h>
#include <bb/core/smart_ptr.h>
#include <bb/core/safevalue.h>
#include <boost/static_assert.hpp>
#include <bb/core/Subscription.h>
#include <tr1/array>
namespace bb {


namespace trading {


namespace detail{
    template<typename _T> struct DefaultFPPolicy{
        typedef  _T value_type;
         static  value_type get_default(){return value_type();}
         static  bool less_than(value_type const& l,value_type const& r){return bb::LT(l,r,static_cast<value_type>(0.000001));}
         static  bool equal_to(value_type const& l,value_type const& r){return bb::EQ(l,r,static_cast<value_type>(0.000001));}
    };
    template<typename _T> struct DefaultRestrictedPercentagePolicy:DefaultFPPolicy<_T>{
        typedef  typename DefaultFPPolicy<_T>::value_type value_type;
        static  value_type narrow(value_type const& _v){
          BB_ASSERT(_v>=0 && _v<=100);
          return _v;
        }
    };
    struct DollarVolumePolicy:DefaultFPPolicy<double>{
        static  value_type narrow(value_type const& _v){
            BB_ASSERT(_v>=0);
            return _v;
        }

    };
    struct TradingVolumePolicy:bb::SafeFundamentalDefaultPolicy<unsigned long long>{};
    struct FillPercentagePolicy:DefaultRestrictedPercentagePolicy<float>{
    };


}//detail

typedef SafeValue<detail::DollarVolumePolicy> TradingVolumeDollars;
typedef SafeValue<detail::TradingVolumePolicy> TradingVolumeShares;
typedef SafeValue<detail::FillPercentagePolicy> FillPercentage;

struct IVolumeProvider{
    virtual  TradingVolumeShares       getShares(bb::side_t )const=0;
    virtual  TradingVolumeDollars      getDollars(bb::side_t )const=0;
    virtual  bb::instrument_t          getInstrument() const=0;
    virtual ~IVolumeProvider();
    virtual void print(std::ostream & )const=0;
};
std::ostream& operator<<(std::ostream&os,IVolumeProvider const&);
BB_DECLARE_SHARED_PTR(IVolumeProvider);





} // trading
} // bb

#endif // BB_TRADING_IVOLUMEPROVIDER_H

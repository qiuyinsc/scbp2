#ifndef BB_TRADING_MSGINFO_H
#define BB_TRADING_MSGINFO_H

/* Contents Copyright 2012 Athena Capital Research LLC. All Rights Reserved. */

#include <bb/core/network.h>
#include <bb/trading/Order.h>

namespace bb {

class Msg;
struct MsgHdr;
BB_FWD_DECLARE_SHARED_PTR( EventDistributor );

namespace trading {

//adorns an order with a given message
class MsgInfo : public OrderInfoSlot
{
public:
    MsgInfo( const Msg&, timeval_t recv_time, const bb::sockaddr_ipv4_t & message_source_addr = g_emptySourceAddress );
    MsgInfo( const Msg&, const EventDistributorPtr & ed );
    MsgInfo( const EventDistributorPtr & ed );
    BB_DECLARE_ORDER_INFO();
    virtual void logIssue(BsonLog::Record&, Order const *) const;

protected:
    virtual void logMsg(BsonLog::Record& ) const;
    virtual void logMsgHdr(BsonLog::Record&, const MsgHdr& ) const;

    const Msg&                       m_msg;
    const timeval_t                  m_recvTv;
    const timeval_t                  m_nicTv;
    const boost::optional<uint32_t>  m_eventDistQueuePos;
    const bb::sockaddr_ipv4_t &      m_messageSourceAddress;
    static bb::sockaddr_ipv4_t       g_emptySourceAddress;
};

} // namespace trading
} // namespace bb


#endif // BB_TRADING_MSGINFO_H

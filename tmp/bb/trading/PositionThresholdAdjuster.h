#ifndef BB_TRADING_POSITIONTHRESHOLDADJUSTER_H
#define BB_TRADING_POSITIONTHRESHOLDADJUSTER_H

/* Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved. */


#include <bb/trading/IThresholdAdjuster.h>

namespace bb {
namespace trading {

class IPositionProvider;


/// position dependent threshold adjuster, gives bias towards 0 position.
class PositionThresholdAdjuster : public IThresholdAdjuster {
public:
    PositionThresholdAdjuster(const IPositionProvider& _position_provider, int32_t stepSize);

    /// Sets zero center factor. Larger values make it harder to increase position
    /// away from flat. Value of 0 means no bias towards flat.
    /// Throws std::invalid_argument if _zcf is less than 0.0.
    virtual void setZeroCenterFactor(double _zcf);

    /// Returns an adjustment factor based on current position to increase thresholds for
    /// increasing position away from 0 and decrease thold for decreasing position towards 0.
    /// Adjustments are symmetric.
    /// Ie, if position is positive, will return a positive value x for BID and a negative
    /// value -x for SELL or SHORT, making it tougher to buy and easier to sell.
    virtual double getThresholdAdjustment(side_t side) const;

protected:
    const IPositionProvider& position_provider;
    double m_zero_center_factor;
    int32_t m_stepSize;
};


} // trading
} // bb

#endif // BB_TRADING_POSITIONTHRESHOLDADJUSTER_H

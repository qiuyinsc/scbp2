#ifndef BB_TRADING_FILLPRICECHECKER_H
#define BB_TRADING_FILLPRICECHECKER_H

/* Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved. */


#include <bb/core/instrument.h>
#include <bb/core/ListenerList.h>
#include <bb/clientcore/Alert.h>

#include <bb/trading/trading.h>
#include <bb/trading/FillInfo.h>
#include <bb/trading/IFillListener.h>

namespace bb {
namespace trading {

BB_FWD_DECLARE_SHARED_PTR(IssuedOrderTracker);

class IFillPriceCheckerListener
{
public:
    virtual ~IFillPriceCheckerListener() {}

    virtual void onPanic() = 0;
    virtual void onWarning() = 0;
};

///
/// Monitors fills and notifies listeners of exceptional conditions
/// Supports a "warning" level and a "panic" level.  Panic level
/// must be greater than or equal to the warning level.
///
/// Note: thresholds are set as a fractional difference, i.e. 10% = 0.10
///
class FillPriceChecker : public IFillListener
                       , private ListenerList<IFillPriceCheckerListener*>
{
public:
    FillPriceChecker( const IAlertPtr &alert, const IssuedOrderTrackerPtr &spIOT, const instrument_t& instr );

    virtual void onFill( const bb::trading::FillInfo& info );

    /// Sets the level for triggering a warning, fractional threshold.  Default is
    /// 0.0025 (equal to .25%)
    void setWarningThreshold( double thresh ) { m_warningThreshold = thresh; }

    /// Sets the level for triggering a panic, fractional threshold.  Default is
    /// 0.005 (equal to 0.5%)
    void setPanicThreshold( double thresh ) { m_panicThreshold = thresh; }

    void addListenerSub( Subscription &outSub, IFillPriceCheckerListener* l ) { subscribe( outSub, l ); };
    void addListener( IFillPriceCheckerListener* l ) { add( l ); }

private:
    Subscription m_sub;
    IAlertPtr m_alert;
    double m_warningThreshold;
    double m_panicThreshold;
};

} // namespace bb
} // namespace trading

#endif // BB_TRADING_FILLPRICECHECKER_H

#ifndef BB_TRADING_BBL1REFDATAFACTORY_H
#define BB_TRADING_BBL1REFDATAFACTORY_H

/* Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved. */


#include <bb/trading/IRefDataFactory.h>

namespace bb {
namespace trading {

// The BbL1RefDataFactory builds reference data that selects among CQS
// or UQDF for quote feeds and CTS or UTDF for tick feeds, depending
// on the instrument.

class BbL1RefDataFactory : public IRefDataFactory
{
public:

    enum DataMode
    {
        kUseTicksAndQuotes,
        kUseTicksOnly,
        kUseQuotesOnly,
    };

    enum QuoteSource
    {
        kBBOFeed,
        kConsolidatedFeed
    };

    // By default we will produce RefData for each instrument that
    // includes both tick and quote sources. To suppress one of these
    // types of sources, provide a DataMode argument other than
    // kTicksAndQuotes.
    BbL1RefDataFactory();
    BbL1RefDataFactory( DataMode mode, QuoteSource source );

    virtual ~BbL1RefDataFactory();

    const DataMode& dataMode() const;

    // Returns true if quotes will be included in RefData objects
    // constructed by this factory.
    bool quotesEnabled() const;

    // Returns true if ticks will be included in RefData objects
    // constructed by this factory.
    bool ticksEnabled() const;

    virtual RefDataPtr createRefData(
        TradingContext* tradingContext,
        const instrument_t& instr );

private:
    const DataMode m_mode;
    const QuoteSource m_quoteSource;
};

BB_DECLARE_SHARED_PTR(BbL1RefDataFactory);

} // namespace trading
} // namespace bb

#endif // BB_TRADING_BBL1REFDATAFACTORY_H

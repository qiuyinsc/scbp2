#ifndef BB_TRADING_REFDATA_H
#define BB_TRADING_REFDATA_H

/* Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved. */


#include <algorithm>
#include <bb/core/instrument.h>
#include <bb/core/sourceset.h>
#include <bb/core/smart_ptr.h>
#include <bb/trading/trading.h>

namespace bb {

class instrument_t;
BB_FWD_DECLARE_SHARED_PTR(IBook);
BB_FWD_DECLARE_SHARED_PTR(IBookBuilder);
BB_FWD_DECLARE_SHARED_PTR(ITickProvider);
BB_FWD_DECLARE_SHARED_PTR(IBookSpec);
BB_FWD_DECLARE_SHARED_PTR(SourceTickFactory);
BB_FWD_DECLARE_SHARED_PTR(MasterTickFactory);

namespace trading {

BB_FWD_DECLARE_SHARED_PTR( RefData );
BB_FWD_DECLARE_SHARED_PTR( TradingContext );

/** Contains reference data about a single instrument.
 * This would be defined by the trading program the reference information for a
 * certain instrument.  These are held by the TradingContext per instrument,
 * the reference sources could be different for each instrument.
 *
 * The RefData must be initialized with a TradingContext and instrument before
 * it can be used to get reference information.
 */
class RefData
{
public:
    RefData();
    RefData( const RefData &t); // copies only the sources from the other RefData
    RefData( const sources_t& sources );
    RefData( const sourceset_t& sources );
    RefData( const sources_t& book_sources, const sources_t& tick_sources );
    RefData( const sourceset_t& book_sources, const sourceset_t& tick_sources );
    virtual ~RefData() {}

    void setRefBookSources( const sources_t& s )
    {
        m_referenceBookSources.clear();
        m_referenceBookSources.insert( s.begin(), s.end() );
    }
    const sourceset_t& getRefBookSources() const { return m_referenceBookSources; }
    IBookPtr getRefBook();
    virtual IBookSpecCPtr getRefBookSpec();
    /// Removes all sources matching Predicate from m_referenceBookSources/m_referenceTickSources
    template<typename Predicate> void removeSources(const Predicate& pred);

    void setRefTickSources( const sources_t& s )
    {
        m_referenceTickSources.clear();
        m_referenceTickSources.insert( s.begin(), s.end() );
    }
    const sourceset_t& getRefTickSources() const { return m_referenceTickSources; }
    ITickProviderPtr getRefTicks();

    void init( TradingContextPtr tc, const instrument_t& instr );
    void init( IBookBuilderPtr bb, SourceTickFactoryPtr stf, MasterTickFactoryPtr mtf, const instrument_t& instr );
    RefDataPtr clone() const;

    IBookBuilderPtr getBookBuilder() { return m_spBookBuilder; }

protected:
    sourceset_t m_referenceBookSources;
    sourceset_t m_referenceTickSources;
    bool m_bInitialized;
    instrument_t m_instr;
    IBookBuilderPtr m_spBookBuilder;
    SourceTickFactoryPtr m_spSourceTickFactory;
    MasterTickFactoryPtr m_spMasterTickFactory;
    IBookPtr m_spBook;
    IBookSpecPtr m_spBookSpec;
    ITickProviderPtr m_spTickProvider;
};

template<typename Predicate> void RefData::removeSources(const Predicate& pred)
{
    for(sourceset_t::iterator i = m_referenceBookSources.begin(); i != m_referenceBookSources.end();)
    {
        if(pred(*i))
        {
            sourceset_t::iterator erase = i;
            ++i;
            m_referenceBookSources.erase(erase);
        }
        else
            ++i;
    }
    for(sourceset_t::iterator i = m_referenceTickSources.begin(); i != m_referenceTickSources.end();)
    {
        if(pred(*i))
        {
            sourceset_t::iterator erase = i;
            ++i;
            m_referenceTickSources.erase(erase);
        }
        else
            ++i;
    }
}

/// Useful with removeSources for filtering the sources which are only valid for NYSE stocks from the ref data.
struct NyseOnlySourcesPred : public std::unary_function<source_t, bool>
{
    bool operator()(const source_t &s) const
    {
        switch(s.type())
        {
            case SRC_CQS:
            case SRC_CTS:
            case SRC_NYSE_QUOTES:
            case SRC_NYSE_TRADES:
            case SRC_NYSE_IMBALANCES:
            case SRC_OPENBOOK:
            case SRC_OPENBOOKULTRA:
            case SRC_NYSE_ALERTS:
                return true;
            default:
                return false;
        }
    }
};

/// Returns true if the source contains aggregated tick information for all exchanges.
struct UnifiedTickSourcePred : public std::unary_function<source_t, bool>
{
    bool operator()(const source_t &s) const
    {
        switch(s.type())
        {
            case SRC_CTS:
            case SRC_UTDF:
            case SRC_SXL1_TRADES:
            case SRC_SXL1:
                return true;
            default:
                return false;
        }
    }
};

/// Returns true if the source only exists for Nasdaq stocks.
struct NasdaqOnlySourcesPred : public std::unary_function<source_t, bool>
{
    bool operator()(const source_t &s) const
    {
        switch(s.type())
        {
            case SRC_UTDF:
                return true;
            default:
                return false;
        }
    }
};

} // namespace trading
} // namespace bb

#endif // BB_TRADING_REFDATA_H

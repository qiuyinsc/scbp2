#ifndef BB_TRADING_UNICASTPOSITIONTRACKER_H
#define BB_TRADING_UNICASTPOSITIONTRACKER_H

/* Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved. */


#include <bb/core/acct.h>
#include <bb/core/instrument.h>

#include <bb/trading/IPositionProvider.h>
#include <bb/trading/ITrader.h>

namespace bb {

class MarginInstrumentInfoMsg;
BB_FWD_DECLARE_SHARED_PTR( EventDistributor );
BB_FWD_DECLARE_SHARED_PTR( MsgHandler );
BB_FWD_DECLARE_SHARED_PTR( IAlert );

namespace trading {

/// UnicastPositionTracker keeps track of the current position for a given instrument and account.
/// It replaces PositionTracker, and receives positions via unicast rather than multicast.
///
/// The UnicastPositionTracker needs to send a request to the trade server, so it implements
/// ITDConnectionListener to know when the trade server is connected. It does not add itself
/// as a listener directly, the caller must do that and pass in the subscription using
/// setListenerSub.
class UnicastPositionTracker
    : public IOTPositionProvider
    , public ITDConnectionListener
{
public:
    UnicastPositionTracker( const instrument_t& instrument, acct_t account,
            EventDistributorPtr eventDist, IAlertPtr alert );
    virtual ~UnicastPositionTracker();

    void setListenerSub( const Subscription &sub ) { m_listenerSub = sub; }

    virtual acct_t getAcct() const { return m_acct; }

    // IPositionProvider (via IOTPositionProvider) impl
    virtual bool isReady() const { return m_positionMessageReceived; }
    virtual int32_t getAbsolutePosition() const { return m_baseline_pos + m_relative_pos; }
    virtual int32_t getEffectivePosition() const { return m_relative_pos; }

    // IOTPositionProvider impl
    virtual void handleFill( const timeval_t &tv, side_t side, double fill_px,
            uint32_t fill_qty, const boost::optional<int32_t> &posFromMsg );

    /// ITDConnectionListener impl
    virtual void onTradeServerConnected( IPositionBroadcastRequesterPtr requester );
    virtual void onTradeServerDisconnected();

    /// updates our position based on a MarginInstrumentInfoMsg (public for unit-testing)
    void handlePositionUpdate( int32_t new_absolute_pos, int32_t new_baseline_pos );

protected:
    void handleMarginInstrumentInfoMsg( const MarginInstrumentInfoMsg &msg );

    acct_t m_acct;
    IAlertPtr m_alert;

    bool m_positionMessageReceived;
    int32_t m_baseline_pos;
    int32_t m_relative_pos;

    MsgHandlerPtr m_msgHandlerSub;
    Subscription m_listenerSub;

    IPositionBroadcastRequesterPtr m_positionRequester;
};
BB_DECLARE_SHARED_PTR( UnicastPositionTracker );

} // namespace trading
} // namespace bb

#endif // BB_TRADING_UNICASTPOSITIONTRACKER_H

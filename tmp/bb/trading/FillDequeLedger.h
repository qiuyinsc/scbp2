#ifndef BB_TRADING_FILLDEQUELEDGER_H
#define BB_TRADING_FILLDEQUELEDGER_H

/* Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved. */

#include <bb/trading/FillRecord.h>
#include <bb/trading/IFillLedger.h>

namespace bb {
namespace trading {

typedef IFillLedger::FillRecords FillRecords;
typedef IFillLedger::iFillRecord iFillRecord;
typedef IFillLedger::c_iFillRecord c_iFillRecord;

// forward decl
class FillDequeLedger;
BB_DECLARE_SHARED_PTR( FillDequeLedger );

// TODO - rename the class and file to reflect the fact that it's no longer backed by a deque

/// A circular buffer implementation of IFillLedger. The ledgers are stored as circular buffers, one for LONG
/// and the other for SHORT. A FillRecord is pushed on a circular buffer front if no fills are
/// available on the circular buffer back on the other side of the ledger. PnL is realized when a
/// long and short FR are matched from the back of the circular buffers.
/// NOTE: use of circular buffer is an implicit assumption that the
/// timestamps of the FillRecord are not from the ecn but rather from bb. In this case
/// FillRecords are time-ordered by construction. An alternative approach is to insert a
/// new FillRecord and sort by timestamp, but that's better done w/ a sorted_list.
class FillDequeLedger
    : public IFillLedger
{
public:
    friend class PnLTracker;
    static FillDequeLedgerPtr create(const instrument_t& instr);
    virtual ~FillDequeLedger();

    /// return the instr for which the ledger is assoc'd
    virtual instrument_t getInstrument() const { return m_instr; }

    /// Circular buffer-like methods but implementation independent.
    virtual void push_front(const FillRecord& fillrecord);
    virtual void push_back(const FillRecord& fillrecord);

    virtual void pop_front(const side_t lside);
    virtual void pop_back(const side_t lside);

    FillRecord& front(const side_t side);
    FillRecord& back(const side_t side);

    /// Clears one side of the ledger
    virtual void clear(const side_t lside);

    /// Returns true if ledger on lside is empty
    virtual bool empty(const side_t lside) const { return m_ledger[lside].empty(); }

    /// Returns size of ledger on lside
    virtual size_t size(const side_t lside) const { return static_cast<size_t>(m_ledger[lside].size()); }

    /// Get the ledger side assoc'd w/ the dir_t in the fillrecord
    virtual side_t getSide(const FillRecord& fillrecord) const { return dir2side(fillrecord.getDir()); }

    /// Const iterator so the collection of interal fillrecords can be inspected
    FillRecords const& getRecords(const side_t lside) const;

    /// For pretty-print
    virtual std::ostream& print(std::ostream& out) const;

protected:
    FillDequeLedger(const instrument_t& instr);

private:
    /// Ledger[LONG, SHORT], each a circular buffer of FillRecords
    FillRecords m_ledger[2];

    /// the instrument for which this ledger is associated
    const instrument_t m_instr;
};



}    // trading
}    // bb


#endif // BB_TRADING_FILLDEQUELEDGER_H

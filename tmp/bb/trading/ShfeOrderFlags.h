#ifndef BB_TRADING_SHFEORDERFLAGS_H
#define BB_TRADING_SHFEORDERFLAGS_H

/* Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved. */


#include <ostream>
#include <bb/trading/Order.h>

namespace bb {
namespace trading {

class shfe_oflag_t
{
public:
    static const shfe_oflag_t& td_auto() { static const shfe_oflag_t f( 0 ); return f; }
    static const shfe_oflag_t& manual_open()
    {
        static const shfe_oflag_t f( bb::OFLAG_SHFE_MANUAL_OPENCLOSE ); return f;
    }
    static const shfe_oflag_t& manual_close() {
        static const shfe_oflag_t f( bb::OFLAG_SHFE_MANUAL_OPENCLOSE | bb::OFLAG_SHFE_CLOSEPOSITION );
        return f;
    }
    static const shfe_oflag_t& manual_closetoday()
    {
        static const shfe_oflag_t f( bb::OFLAG_SHFE_MANUAL_OPENCLOSE
            | bb::OFLAG_SHFE_CLOSEPOSITION
            | bb::OFLAG_SHFE_CLOSEPOSITIONTODAY );
        return f;
    }
    static const uint32_t mask()
    {
        return OFLAG_SHFE_MANUAL_OPENCLOSE
            | OFLAG_SHFE_CLOSEPOSITION
            | OFLAG_SHFE_CLOSEPOSITIONTODAY;
    }

public:
    uint32_t value() const { return m_flags; }
    std::ostream& print ( std::ostream& os ) const
    {
        if( m_flags == td_auto().m_flags ) os << "AUTO";
        else if( m_flags == manual_open().m_flags ) os << "MANUAL_OPEN";
        else if( m_flags == manual_close().m_flags ) os << "MANUAL_CLOSE";
        else if( m_flags == manual_closetoday().m_flags ) os << "MANUAL_CLOSETODAY";
        else os << "UNKNOWN";
        return os;
    }
    bool operator== ( const shfe_oflag_t& f ) const { return m_flags == f.m_flags; }
    bool operator!= ( const shfe_oflag_t& f ) const { return m_flags != f.m_flags; }

private:
    shfe_oflag_t( uint32_t f ) : m_flags(f) {}
    uint32_t m_flags;
};

inline std::ostream& operator<< ( std::ostream& os, const shfe_oflag_t& of )
{
    return of.print( os );
}

// handle SHFE flag tokens for open and close position offset markings
template<> inline OrderInfo& OrderInfo::enableFlagGrp( const shfe_oflag_t& f )
{
    static const uint32_t not_mask = ~(OFLAG_SHFE_MANUAL_OPENCLOSE
        | OFLAG_SHFE_CLOSEPOSITION
        | OFLAG_SHFE_CLOSEPOSITIONTODAY);
    m_oflags = ((m_oflags & not_mask) | f.value());
    return (*this);
}

} // namespace trading
} // namespace bb


// TODO(acm): Anonymous namespaces in headers should be
// disallowed. Fix this.
namespace {
using namespace bb::trading;

const bb::trading::shfe_oflag_t& shfe_auto = shfe_oflag_t::td_auto();
const bb::trading::shfe_oflag_t& shfe_manual_open = shfe_oflag_t::manual_open();
const bb::trading::shfe_oflag_t& shfe_manual_close = shfe_oflag_t::manual_close();
const bb::trading::shfe_oflag_t& shfe_manual_closetoday = shfe_oflag_t::manual_closetoday();
}

#endif // BB_TRADING_SHFEORDERFLAGS_H

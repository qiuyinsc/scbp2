#ifndef BB_TRADING_TIMEOFDAYTHRESHOLDADJUSTER_H
#define BB_TRADING_TIMEOFDAYTHRESHOLDADJUSTER_H

/* Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved. */


#include <bb/core/instrument.h>

#include <bb/clientcore/IClockListener.h>

#include <bb/trading/IThresholdAdjuster.h>
#include <bb/clientcore/SessionParams.h>

namespace bb {

class IPriceProvider;
BB_FWD_DECLARE_SHARED_PTR( ClockMonitor );

namespace trading {


/// allows threshold to be adjusted for time of day.
class TimeOfDayThresholdAdjuster
    : public IThresholdAdjuster
    , public IClockListener
{
public:
    TimeOfDayThresholdAdjuster(const instrument_t& _instr, ClockMonitor& _cm,
        const IPriceProvider& _price_provider, int _vbose=0);

    /// ClockListener interface. Used solely to get time of day notices based on instr
    virtual void onWakeupCall(const timeval_t& ctv, const timeval_t& swtv, int reason, void* pData);

    /// IThresholdAdjuster interface
    /// Calculate and return an adjustment based on time of day.
    /// See base class for how to use result
    virtual double getThresholdAdjustment(side_t side) const;

protected:
    instrument_t instr;
    ClockMonitor& cm;
    const IPriceProvider& price_provider;
    double m_base_timeofday_adj;
    double m_timeofday_factor;
    int vbose;
};

} // namespace trading
} // namespace bb

#endif // BB_TRADING_TIMEOFDAYTHRESHOLDADJUSTER_H

#ifndef BB_TRADING_TRADER_H
#define BB_TRADING_TRADER_H

/* Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved. */


#include <string>
#include <bb/core/loglevel.h>
#include <bb/core/smart_ptr.h>
#include <bb/core/shared_vector.h>
#include <bb/clientcore/IPositionBroadcastRequester.h>
#include <bb/trading/ITrader.h>
#include <bb/core/hc.h>

namespace bb {

BB_FWD_DECLARE_SHARED_PTR(Alert);
BB_FWD_DECLARE_SHARED_PTR(EventDistributor);
BB_FWD_DECLARE_SHARED_PTR(ITradeDemonClient);
BB_FWD_DECLARE_SHARED_PTR(LiveMStreamManager);
BB_FWD_DECLARE_SHARED_PTR(LiveTradeDemonClient);

namespace trading {

class Order;

class BaseTrader : public ITrader
{
public:
    typedef shared_vector<ITDConnectionListener*> TDListeners_t;

    BaseTrader(acct_t acct, int vbose=1);
    virtual ~BaseTrader() { }

public:
    virtual bb::acct_t getAcct() const { return m_acct; }
    virtual const std::string& getIDString() const { return m_idstring; }
    virtual const std::string& getTdTransportType() const;
    virtual const std::string& getServerName() const;
    virtual const std::string& getServerHostName() const;
    virtual unsigned int getOrderCnt() const { return m_orderCount; }
    virtual bool isConnected() const;

    /// Sets the IDString of this Trader.
    virtual void setIDString( const std::string& idstring );

    /// Request that the trade server cancel our orders on disconnection.
    /// Must be called before connectToTradeServer().
    virtual void setCancelOnDisconnect( bool enable = true );

    /// These two methods work with any ITradeDemonClient
    virtual unsigned int sendOrder( const OrderPtr& order, timeval_t* send_time );

    virtual bool sendCancel( const OrderPtr &order, timeval_t *send_time );
    virtual bool sendCancelsForSide( side_t side, const instrument_t& instr, timeval_t *send_time = 0);

    virtual unsigned int modifyOrder( const OrderPtr& issued_order, const OrderPtr& desired_order, timeval_t* send_time );
    virtual unsigned int modifyOrder( unsigned int orig_orderid, const OrderPtr& desired_order, timeval_t* send_time = 0 )
    {
        BB_THROW_ERROR_SSX("Trader::modifyOrder: This helper function is not supported");
    }

    /// Details of the specific ITradeDemonClient
    /// implementation are handled in the connection and
    /// disconnection methods.

    /// Disconnects from a trdserver.
    /// Does nothing if we are not connected.
    virtual void disconnectFromTradeServer()=0;

    /// Connects to trdserver.
    /// Requires that isConnected is false. If we are connected, will sound LogPanic and
    /// not disconnect from the current trdserver.
    virtual bool connectToTradeServer(const std::string& new_trdserver_name)=0;

    virtual void addTDConnectionListener( Subscription &outListenerSub, ITDConnectionListener* l );

    ITradeDemonClientPtr getTradeDemonClient() const { return m_spTDClient; }

protected:
    /// Simple validations that may not change much in children classes.
    virtual bool preConnectCheck(const std::string& new_trdserver_name);

    virtual bool preDisconnectCheck();

    virtual bool checkTDClient( const std::string& new_trdserver_name );
    unsigned int nextSequenceNumber() { return ++m_sequenceNumber; }

    /// Abstract, as live and simulation traders will probably implement this
    /// very differently.
    virtual void sendAlert(loglevel_t warn_level, const std::string& alertmsg)=0;

    acct_t m_acct;
    std::string m_idstring;
    bool m_cancelOnDisconnect;
    int m_vbose;
    unsigned int m_sequenceNumber;
    int32_t m_orderCount;

    std::string m_trdServerName;
    mutable std::string m_transportType;

    ITradeDemonClientPtr m_spTDClient;
    TDListeners_t m_listeners;
private:
    bool checkReturnStatus( std::string function_name, const bb::hc_return_t& return_status, const bb::symbol_t& sym
                            , const unsigned int orderid, const unsigned int orig_orderid = 0 );
};

BB_DECLARE_SHARED_PTR( BaseTrader );

class LiveTrader : public BaseTrader
{
public:
    LiveTrader( LiveMStreamManagerPtr spLiveMStreamMgr, acct_t acct, EventDistributorPtr spEventDist
        , AlertPtr spAlert, int vbose );

    virtual void disconnectFromTradeServer();
    virtual bool connectToTradeServer(const std::string& new_trdserver_name);

protected:
    enum PanicOnDisconnect{ kPanic, kNoPanic};
    void onTradeServerDisconnected( const enum PanicOnDisconnect panicOnDisconnect );
    virtual void sendAlert(loglevel_t warn_level, const std::string& alertmsg);

    LiveMStreamManagerPtr    m_spLiveMStreamMgr;
    EventDistributorPtr      m_spEventDist;
    AlertPtr                 m_spAlert;
};

BB_DECLARE_SHARED_PTR( LiveTrader );

class StubTrader : public BaseTrader
{
public:
    StubTrader( acct_t acct, int vbose );

    virtual bool isConnected() const { return m_connected; }

    virtual unsigned int sendOrder( const OrderPtr& desired_order, timeval_t* send_time )
    {
        if( send_time )
            *send_time = 0;
        return nextSequenceNumber();
    }
    virtual bool sendCancel(const OrderPtr &issued_order, timeval_t* send_time)
    {
        if( send_time )
            *send_time = 0;
        return true;
    }
    virtual bool sendCancelsForSide( side_t side, const instrument_t& instr, timeval_t *send_time = 0)
    {
        if( send_time )
            *send_time = 0;
        return true;
    }

    virtual unsigned int modifyOrder(const trading::OrderPtr& issued_order,
                                     const trading::OrderPtr& desired_order,
                                     timeval_t* send_time = 0) {
        if( send_time ) {
            *send_time = 0;
        }
        return nextSequenceNumber();
    }

    // Helper function for modifying an order, by using original order's orderid
    virtual unsigned int modifyOrder(unsigned int orig_orderid,
                                     const trading::OrderPtr& desired_order,
                                     timeval_t* send_time = 0) {
        if( send_time ) {
            *send_time = 0;
        }
        return nextSequenceNumber();
    }

    virtual bool connectToTradeServer(const std::string& new_trdserver_name);
    virtual void disconnectFromTradeServer();
    virtual void addTDConnectionListener(Subscription &outListenerSub, ITDConnectionListener *l);

protected:
    class StubPositionBroadcastRequester : public IPositionBroadcastRequester
    {
    public:
        // Sends a message to the TD to begin MarginInstrumentInfoMsg broadcasts for the given account/instr.
        virtual bool setup_position_broadcasts(bb::acct_t acct, const bb::instrument_t& instr) { return true; }

        // Sends a message to the TD to begin/end MarginInstrumentInfoMsg unicasts for the given account/instr.
        virtual bool margin_subscribe  (bb::acct_t acct, const bb::instrument_t& instr) { return true; }
        virtual bool margin_unsubscribe(bb::acct_t acct, const bb::instrument_t& instr) { return true; }
    };

protected:
    virtual void sendAlert(loglevel_t warn_level, const std::string& alertmsg) {}

    IPositionBroadcastRequesterPtr m_stubPositionBroadcastRequester;

private:
    bool m_connected;
};

} // namespace trading
} // namespace bb

#endif // BB_TRADING_TRADER_H

#ifndef BB_TRADING_ORDERHARMONIZER_H
#define BB_TRADING_ORDERHARMONIZER_H

/* Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved. */


#include <bb/core/ListenerRelay.h>

#include <bb/trading/trading.h> //PRIORITY_TRADING_DEFAULT
#include <bb/trading/Order.h>
#include <bb/trading/IOrderValidator.h>

namespace bb {
namespace trading {

BB_FWD_DECLARE_SHARED_PTR( ITrader );
BB_FWD_DECLARE_SHARED_PTR( IOrderValidator );

///
/// OrderHarmonizer fills the "match-and-issue" role in libtrading. Allows
/// desired orders to be compared to actual issued orders, and makes issue and
/// cancel calls to a Trader to bring the actual orders into harmony with
/// desired orders. All issues are subject to being validated by Limits before
/// being issued.
///
/// Each OrderHarmonizer will maintain its own orders and not interfere with
/// other orders present in the IssuedOrderTracker
///
/// max-side-orders, if set, will determine how many orders the order harmonizer will
/// allow to be in the market at one time.  An input <= 0 will disable the
/// max side orders check.  This functionality is disabled by default.
///
class OrderHarmonizer
    : public boost::noncopyable
    , public IOrderStatusListener
{
public:
    /// Construct an OrderHarmonizer
    OrderHarmonizer( ITraderPtr spTrader, IOrderValidatorCPtr spLimits );
    virtual ~OrderHarmonizer() {}

    /// Compares issued and desired orders for side, issuing and cancelling orders as needed
    /// to bring the actual issued orders into harmony with desired orders. Issuing of new
    /// orders is subjects to checks on limits.
    /// Returns false if desired_orders doesn't conform to limits.
    ///
    /// Order lifetime management is sorta tricky! If the OH issues an order, you cannot
    /// use that Order any more in your desired lists. To avoid this issue, you can use
    /// a listener so you know when orders get issued.
    virtual bool harmonizeActualAndDesiredOrders( side_t side, OrderList* desired_orders );

    void getIssuedOrders( std::vector<const Order*>* buy_orders
        , std::vector<const Order*>* sell_orders );

    virtual void onOrderStatusChange( const OrderPtr &order, const ChangeFlags &flags );

    void setMaxSideOrders( int32_t num_orders ) { m_maxSideOrders = num_orders; }
    const std::string& getLastReason() const { return m_spLimits->getLastReason(); }

    /// Adds a subscription that'll get called with status updates for any order
    /// that the OH issues.
    void subscribeStatusChangeListener( Subscription &outSub, IOrderStatusListener* l
        , const Priority& p = PRIORITY_TRADING_DEFAULT ) const;

public:
    /// allow test class for OH to call the protected methods
    /// @todo move this out of here to test file, by having test file have a class
    /// that inherits from this one and makes friend declaration there.
    friend class OrderHarmonizerTest;

protected:
    /// Checks with Limits that order is ok to issue. If it is,
    /// calls m_spTrader->sendOrder(). Responsibility for updating
    /// order and tracking the order is up to the trader.
    /// If not ok to issue, does nothing.
    virtual void issueOrder(side_t side, OrderPtr o);

    /// Sends a cancel to Trader for issued_order.
    /// If a cancel has already been sent, does nothing.
    virtual void cancelOrder(OrderPtr issued_order);

protected:
    struct OrderInfo : public OrderInfoSlot
    {
        BB_DECLARE_ORDER_INFO();
        OrderListenerInfo::SubPtr m_listenerSub;
        OrderList::iterator m_selfIterator; // iterator into OrderHarmonizer::m_pKnownOrdersList
    };

    ITraderPtr m_spTrader;
    IOrderValidatorCPtr m_spLimits;
    int32_t m_maxSideOrders;

    boost::scoped_ptr<OrderList> m_pKnownOrdersList[2];
    typedef bb::ListenerRelay<IOrderStatusListener*> stat_chg_listeners_t;
    mutable stat_chg_listeners_t m_statChgListeners;
};
BB_DECLARE_SHARED_PTR( OrderHarmonizer );

} // namespace trading
} // namespace bb

#endif // BB_TRADING_ORDERHARMONIZER_H

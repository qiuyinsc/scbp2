#ifndef BB_TRADING_IORDERSTATUSLISTENER_H
#define BB_TRADING_IORDERSTATUSLISTENER_H

/* Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved. */


#include <iosfwd>

#include <bb/core/smart_ptr.h>
#include <bb/core/timeval.h>
#include <bb/core/cxlstatus.h>
#include <bb/core/ostatus.h>
#include <bb/core/PrioritizedListenerList.h>
#include <bb/core/PrioritizedListenerList.h>
#include <bb/trading/IFillListener.h>
namespace bb {
namespace trading {

BB_FWD_DECLARE_INTRUSIVE_PTR( Order );

/// Provide an interface to allow any object to be notified of a change in the status of this order.
/// The Orders are refcounted and can be safely modified & stored; the OrderPtrs are passed by const
/// reference for efficiency, though this may be premature optimization.
class IOrderStatusListener:public IFillListener
{
public:
    typedef PrioritizedListenerList<IOrderStatusListener*> Listeners_t;
    typedef Subscription ListenerSubPtr;

    class ChangeFlags;
    typedef const ChangeFlags &CHANGEFLAGS;

    virtual ~IOrderStatusListener() {}

    /// Called whenever an order's OrderStatus or CancelStatus changes.
    /// The details of what has changed can be found in the ChangeFlags.
    /// Listeners typically will only care about a small number of possible changes,
    /// so they can check the order to see whether it is something they care about.
    virtual void onOrderStatusChange( const OrderPtr &order, const ChangeFlags &flags ) {};

    /// Called whenever there is a fill on the order. Be aware that getting the fees
    /// is done lazily - and this might be a costly function. Only call it if you have to!
    virtual void onFill( const FillInfo & info ) { }

    /// Called when we learn that an order has been repriced by the exchange.
    /// This can happen before or after the order becomes STAT_OPEN,
    /// depending on the exchange.
    virtual void onReprice( const OrderPtr &order, double new_px ) {};


    /// Called whenever an order is being modified and the OrderStatus/CancelStatus
    /// for the involved orders changes
    /// The details of what has changed can be found in the ChangeFlags
    /// The default behaviour is to send two callbacks, one for the
    /// original order's status changes and one for the new order's status changes
    virtual void onOrderModifyStatusChange( const OrderPtr& new_order, const ChangeFlags& new_flags
                                            , const OrderPtr& old_order, const ChangeFlags& old_flags )
    {
        onOrderStatusChange( old_order, old_flags );
        onOrderStatusChange( new_order, new_flags );
    }

    /// Represents the state transition that an order is going through.
    /// A single transition can change both the CancelStatus and the OrderStatus.
    class ChangeFlags
    {
    public:
        bool orderStatusChanged() const { return m_orderStatChanged; }
        bool cancelStatusChanged() const { return m_cxlStatChanged; }

        bool orderStatusChangedTo(ostatus_t status) const { return orderStatusChanged() && m_newOrderStatus == status; }
        bool cancelStatusChangedTo(cxlstatus_t status) const { return cancelStatusChanged() && m_newCxlStatus == status; }

        const timeval_t &getChangeTime() const { return m_changeTime; }
        // Returns the exchange's timestamp for this change.
        // May not be available if this message didn't originate at an exchange.
        const timeval_t &getExchangeTime() const { return m_exchTime; }
        // Returns the exchange's refnum for this change. May not be available.
        uint64_t getExchRef() const { return m_exchRef; }

        ostatus_t getNewOrderStatus() const { return m_newOrderStatus; }
        ostatus_t getOldOrderStatus() const { return m_oldOrderStatus; }
        cxlstatus_t getNewCancelStatus() const { return m_newCxlStatus; }
        cxlstatus_t getOldCancelStatus() const { return m_oldCxlStatus; }

    public:
        ChangeFlags(const timeval_t &tv);
        ChangeFlags(const timeval_t &tv, const timeval_t &exch_time, uint64_t exch_ref,
                ostatus_t ostatus, cxlstatus_t cxlstatus);

        // setters
        void updateOrderStatus(ostatus_t ostatus);
        void updateCancelStatus(cxlstatus_t cxlstatus);

    protected:
        bool m_orderStatChanged, m_cxlStatChanged;
        ostatus_t m_oldOrderStatus, m_newOrderStatus;
        cxlstatus_t m_oldCxlStatus, m_newCxlStatus;
        timeval_t m_changeTime, m_exchTime;
        uint64_t m_exchRef;
    };
};
BB_DECLARE_SHARED_PTR(IOrderStatusListener);

std::ostream& operator<<( std::ostream&, const IOrderStatusListener::ChangeFlags& );

} // namespace trading
} // namespace bb


#endif // BB_TRADING_IORDERSTATUSLISTENER_H

#ifndef BB_CORE_PRIORITY_H
#define BB_CORE_PRIORITY_H

/* Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved. */


#include <boost/operators.hpp>

#include <bb/core/timeval.h>

namespace bb {


/**
    Event Distributor Priorities
**/

// BB-wide Priorities
static const uint32_t PRIORITY_HIGHEST_VAL = std::numeric_limits<uint32_t>::max();  /// 2^32 - 1 or 0xFFFFFFFF
static const uint32_t PRIORITY_LOWEST_VAL  = std::numeric_limits<uint32_t>::min();  /// 0

/// The Priority class is used to order the scheduling of events in clientcore.
///
/// A HIGHER raw value is a HIGHER priority.
///
class Priority
    : public boost::totally_ordered<Priority
    , boost::additive<Priority
    , boost::additive<Priority, uint32_t
    > > >
{
public:
    // BB-wide Priorities
    static const Priority HIGHEST;
    static const Priority LOWEST;

public:
    Priority( uint32_t val )
        : m_val( val )
    {}

    /// Returns the integer value of this priority.
    uint32_t value() const    { return m_val; }
    /// Returns the integer value of this priority.
    operator uint32_t() const { return m_val; }

    /// Returns true if the value of this Priority is equal to the value of the other Priority.
    bool operator ==(const Priority& other) const { return m_val == other.m_val; }
    /// Returns true if the value of this Priority is less than the value of the other Priority.
    bool operator < (const Priority& other) const { return m_val <  other.m_val; }

    // Increments the Priority.  The result is clamped to Priority::HIGHEST.
    Priority& operator+=( const Priority rhs )
    {
        if ( PRIORITY_HIGHEST_VAL - rhs.m_val < m_val  )
            *this = Priority::HIGHEST;
        else
            *this = Priority( m_val + rhs.m_val );
        return *this;
    }
    // Increments the Priority.  The result is clamped to Priority::HIGHEST.
    Priority& operator+=( uint32_t rhs )
    {   return (*this += Priority(rhs)); }

    // Decrements the Priority.  The result is clamped to Priority::HIGHEST.
    Priority& operator-=( const Priority rhs )
    {
        if ( PRIORITY_LOWEST_VAL + rhs.m_val > m_val  )
            *this = Priority::LOWEST;
        else
            *this = Priority( m_val - rhs.m_val );
        return *this;
    }
    // Decrements the Priority.  The result is clamped to Priority::LOWEST.
    Priority& operator-=( uint32_t rhs )
    {   return (*this -= Priority(rhs)); }

    /// Returns the next higher priority.  The result is clamped to Priority::HIGHEST.
    Priority& operator++ ()     // prefix  ++
    {   *this += 1; return *this; }
    /// Returns the next higher priority.  The result is clamped to Priority::HIGHEST.
    Priority  operator++ (int)  // postfix ++
    {   Priority save = *this; *this += 1; return save; }

    /// Returns the next lowest Priority.  The result is clamped to Priority::LOWEST.
    Priority& operator-- ()     // prefix  --
    {   *this -= 1; return *this; }
    /// Returns the next lowest Priority.  The result is clamped to Priority::LOWEST.
    Priority  operator-- (int)  // postfix --
    {   Priority save = *this; *this -= 1; return save; }

    /// Returns this Priority offset by another 'shifted' priority.
    /// The composition is the sum of the priorities, with the passed priority's value
    /// being shifted by shift_distance.
    /// TODO: WHY IS THIS USEFUL?
    /// The result is clamped to Priority::HIGHEST.
    Priority offset( const Priority p, uint8_t nibble_shift_distance = 2 ) const
    {
        return *this + (p.m_val >> (4*nibble_shift_distance) );
    }

    /// Returns this Priority offset negatively by another priority.
    /// The composition is the sum of the priorities, with the passed priority's value
    /// being shifted by byteshift_distance bytes.
    /// TODO: WHY IS THIS USEFUL?
    /// The result is clamped to Priority::LOWEST.
    Priority neg_offset( const Priority p, uint8_t nibble_shift_distance = 2 ) const
    {
        return *this - (p.m_val >> (4*nibble_shift_distance) );
    }

    /// Returns a Priority that's greater than this priority by amt
    ///
    Priority higher_by( int32_t amt = 1 ) const { return Priority(m_val) += amt; }

    /// Returns a Priority that's lower than this priority by amt
    ///
    Priority lower_by( int32_t amt = 1 ) const { return Priority(m_val) -= amt; }

private:
    uint32_t m_val;
};


} // namespace bb

#endif // BB_CORE_PRIORITY_H

#ifndef BB_CORE_COMPAT_H
#define BB_CORE_COMPAT_H

/* Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved. */

// Support for other C++ versions

#include <boost/config.hpp>
#if __cplusplus < 201103L
#ifndef override
#define override
#endif

#ifndef noexcept
#define noexcept(IGNORE)
#endif
#endif

#include <memory>

#ifdef BOOST_NO_CONSTEXPR
#pragma GCC system_header
#define constexpr const
#pragma GCC system_header pop
#endif

#ifdef BOOST_NO_NULLPTR
#pragma GCC system_header
#define nullptr NULL
#pragma GCC system_header pop
#endif

// Support for other OSes

#include <boost/preprocessor/stringize.hpp>
#if defined( WIN32 ) || defined( WIN64 )
#    include <bb/core/compat_win.h>
#elif defined( __APPLE__ ) && defined( __MACH__ )
#   include <bb/core/compat_macosx.h>
#else
#   include <bb/core/compat_linux.h>
#endif

#ifdef SWIG
#define _attr_deprecated_
#elif defined(__GNUC__)
#define _attr_deprecated_ __attribute__((deprecated))
#else
#define _attr_deprecated_
#endif

#ifdef SWIG
#define _attr_noreturn_
#elif defined(__GNUC__)
#define _attr_noreturn_ __attribute__((noreturn))
#else
#define _attr_noreturn_
#endif

#ifdef SWIG
#define _attr_noinline_
#elif defined(__GNUC__)
#define _attr_noinline_ __attribute__((noinline))
#else
#define _attr_noinline_
#endif

#ifdef SWIG
#define _attr_hot_
#define _attr_cold_
// Hot and cold require gcc-4.3 or better
#elif defined(__GNUC__) && (__GNUC__ > 4 || (__GNUC__ == 4 && (__GNUC_MINOR__ > 3 || __GNUC_MINOR__ == 3)))
#define _attr_hot_ __attribute__((hot))
#define _attr_cold_ __attribute__((cold))
#else
#define _attr_hot_
#define _attr_cold_
#endif

#ifdef SWIG
#define _attr_packed_
#elif defined(__GNUC__)
#define _attr_packed_ __attribute__((packed))
#else
#error "packed attribute specified but not supported on this platform"
#endif

#ifdef SWIG
#define _attr_used_
#elif defined(__GNUC__)
#define _attr_used_ __attribute__((used))
#else
#define _attr_used_
#endif

#ifdef SWIG
#define _attr_unused_
#elif defined(__GNUC__)
#define _attr_unused_ __attribute__((unused))
#else
#define _attr_unused_
#endif

/// marks a branch as likely (or unlikely) to be taken.
inline bool likely(bool expr)
{
#ifdef __GNUC__
    return __builtin_expect(expr, true);
#else
    return expr;
#endif
}

inline bool unlikely(bool expr)
{
#ifdef __GNUC__
    return __builtin_expect(expr, false);
#else
    return expr;
#endif
}

// Clang will complain about variables we've defined
// but not used. While we fix the actual occurences, let's
// prevent Clang from reminding us.
static void use_variable(const void* p)
{
    asm volatile("" : : "g"(p): "memory" );
}

template<typename T, std::size_t size>
std::size_t size_of_array(T(&)[size]) { return size; }

// And gcc will complain if we don't use this method.
// We can take this struct out once we get rid of use_variable.
static struct suppress_defined_but_not_used_t
{
    public:
        suppress_defined_but_not_used_t()
        {
            int v(0);
            use_variable(&v);
        }
} suppress_defined_but_not_used;

// Expands to the source file location as a string (e.g. "core/Socket.cc:352")
#define SRC_LOCATION     __FILE__ ":" BOOST_PP_STRINGIZE(__LINE__)
#define SRC_LOCATION_SEP "[" SRC_LOCATION "] "

// C++98 compilers can fake unique_ptr support by
// using auto_ptr instead
namespace std {
#if __cplusplus < 201103L
    template<typename T>
    class unique_ptr : public auto_ptr<T>
    {
        public:
            unique_ptr(T * ptr) : auto_ptr<T> ( ptr ) {}
    };
#endif
} // namespace std

#endif // BB_CORE_COMPAT_H

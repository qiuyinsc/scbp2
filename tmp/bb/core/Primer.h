#ifndef BB_CORE_PRIMER_H
#define BB_CORE_PRIMER_H

/* Contents Copyright 2016 Athena Capital Research LLC. All Rights Reserved. */

#include <cxxabi.h>
#include <set>

#include <boost/foreach.hpp>

#include <bb/core/acct.h>
#include <bb/core/type_id.h>
#include <bb/core/Log.h>
#include <bb/core/instrument.h>
#include <bb/core/EventPublisher.h>

namespace bb {

    // Cache primer:
    // - register your I(Something)Primeable using the 'connect' function
    // - implement prime(T const & value) const to read internal data ( to keep it 'warm' and in cache )
    // - call a relevant prime(Single/All)By(Instrument/Account) function to call 'prime' on all IPrimeables
    class Primer
    {
        public:
            template<typename T>
            struct IPrimeable : public bb::IEventSubscriber
            {
                virtual bool prime(T const & value) const = 0;
            };

            typedef IPrimeable<bb::instrument_t> IInstrumentPrimeable;
            typedef IPrimeable<bb::acct_t>       IAccountPrimeable;
            typedef IPrimeable<bb::timeval_t>    ITimePrimeable;

            static Primer& instance()
            {
                static Primer instance;
                return instance;
            }

            size_t size() const
            {
                return m_instrumentPublisher.numConnections() + m_accountPublisher.numConnections() + m_timePublisher.numConnections();
            }

            template<typename Listener>
            size_t connect( Listener & listener )
            {
                int rv;
                char * demangled =  abi::__cxa_demangle(typeid(Listener).name(), 0, 0, &rv);
                LOG_INFO << "connect IPrimeable [" << demangled << "]" << bb::endl;
                free(demangled);

                size_t num_connected(0);
                if ( boost::is_base_of<IInstrumentPrimeable, Listener>::value )
                {
                    num_connected += m_instrumentPublisher.connect(dynamic_cast<IInstrumentPrimeable*>(&listener));
                }
                if ( boost::is_base_of<IAccountPrimeable, Listener>::value )
                {
                    num_connected += m_accountPublisher.connect(dynamic_cast<IAccountPrimeable*>(&listener));
                }
                if ( boost::is_base_of<ITimePrimeable, Listener>::value )
                {
                    num_connected += m_timePublisher.connect(dynamic_cast<ITimePrimeable*>(&listener));
                }
                return num_connected;
            }

            void add(const bb::instrument_t & instrument)
            {
                m_instruments.insert(instrument);
            }

            void remove(const bb::instrument_t & instrument)
            {
                m_instruments.erase(instrument);
            }

            void add(const bb::acct_t & acct)
            {
                m_accounts.insert(acct);
            }

            void remove(const bb::acct_t & acct)
            {
                m_accounts.erase(acct);
            }

            bool primeSingleByAccount( const bb::timeval_t & time = bb::timeval_t::now )
            {
                const bool one = m_accountPublisher.notifySingleForEach<AccountNotifier>( m_accounts );
                const bool two = m_timePublisher.notifySingle<TimeNotifier>( time );
                return one && two;
            }

            bool primeSingleByInstrument( const bb::timeval_t & time = bb::timeval_t::now )
            {
                 const bool one = m_instrumentPublisher.notifySingleForEach<InstrumentNotifier>( m_instruments );
                 const bool two = m_timePublisher.notifySingle<TimeNotifier>( time );
                return one && two;
            }

            bool primeAllByAccount( const bb::timeval_t & time = bb::timeval_t::now )
            {
                const bool one = m_accountPublisher.notifyAllForEach<AccountNotifier>( m_accounts );
                const bool two = m_timePublisher.notifyAll<TimeNotifier>( time );
                return one && two;
            }

            bool primeAllByInstrument( const bb::timeval_t & time = bb::timeval_t::now )
            {
                 const bool one = m_instrumentPublisher.notifyAllForEach<InstrumentNotifier>( m_instruments );
                 const bool two = m_timePublisher.notifyAll<TimeNotifier>( time );
                return one && two;
            }

        private:
            // made private so we're forced to just call instance()
            Primer() {}

            typedef EventPublisher< IInstrumentPrimeable > InstrumentPrimerPublisher;
            InstrumentPrimerPublisher m_instrumentPublisher;
            struct InstrumentNotifier : public InstrumentPrimerPublisher::INotifier
            {
                InstrumentNotifier(IInstrumentPrimeable* callback) : InstrumentPrimerPublisher::INotifier ( callback ) { }
                bool notify ( const bb::instrument_t & instrument )
                {
                    return m_callback->prime ( instrument );
                }
            };

            typedef EventPublisher< IAccountPrimeable > AccountPrimerPublisher;
            AccountPrimerPublisher m_accountPublisher;
            struct AccountNotifier : public AccountPrimerPublisher::INotifier
            {
                AccountNotifier(IAccountPrimeable* callback) : AccountPrimerPublisher::INotifier ( callback ) { }
                bool notify ( const bb::acct_t & account )
                {
                    return m_callback->prime ( account );
                }
            };

            typedef EventPublisher< ITimePrimeable > TimePrimerPublisher;
            TimePrimerPublisher m_timePublisher;
            struct TimeNotifier : public TimePrimerPublisher::INotifier
            {
                TimeNotifier(ITimePrimeable* callback) : TimePrimerPublisher::INotifier ( callback ) { }
                bool notify ( const bb::timeval_t & time )
                {
                    return m_callback->prime ( time );
                }
            };

            instr_no_mkt_no_currency_set m_instruments;
            std::set<acct_t> m_accounts;
    };
};

#endif // BB_CORE_PRIMER_H

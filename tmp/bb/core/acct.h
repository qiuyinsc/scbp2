#ifndef BB_CORE_ACCT_H
#define BB_CORE_ACCT_H

/* Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved. */


#include <bb/core/hash.h>
#include <bb/core/smart_ptr.h>

namespace bb {

BB_FWD_DECLARE_SHARED_PTR(Environment);

/// A unique identifier for account names
class acct_t
{
public:
    acct_t(unsigned int i = 0) { m_acct_id = i; }
    explicit acct_t(const char* sz);

    const char* toString() const;

    bool isValid() const;

    bool operator ==(const acct_t &acct) const { return acct.m_acct_id == m_acct_id; }
    bool operator !=(const acct_t &acct) const { return !(*this == acct); }
    operator unsigned int() const { return m_acct_id; }
    operator int() const { return (int) m_acct_id; }
    bool operator <(const acct_t &a) const { return m_acct_id < a.m_acct_id; }

    unsigned int id() const { return m_acct_id; }

private:
    unsigned int m_acct_id;
};

// hashing function
inline std::size_t hash_value( const acct_t& acct )
{
    boost::hash<int> hasher;
    return hasher( acct.id() );
}

/// Unique identifier for unknown accounts.
const acct_t ACCT_UNKNOWN = 0;

/// This must be kept in sync with conf/acct.table (and probably should not be
/// changed).
const acct_t ACCT_ALL = 199;

/// Converts an acct_t to a string representation.  If acct isn't in our
/// mappings, it returns NULL.
const char *acct2str(const acct_t& acct);

/// Converts a string to an acct_t representation.  If str isn't in our
/// mappings, it returns ACCT_UNKNOWN.  If ok is non-null, sets *ok to true iff
/// parsing succeeded.
acct_t str2acct(const char *str, bool *ok = NULL);

/// acct2str, except it throws on error.
acct_t str2acct_throw(const char *acctStr);
inline acct_t str2acct_throw(std::string s) { return str2acct_throw(s.c_str()); }

/// Stream operator to convert acct_t into human readable form.
std::ostream &operator <<(std::ostream &out, const acct_t &p);

/// Stream operator to convert string into acct_t.
std::istream &operator >>(std::istream &out, acct_t &p);

} // namespace bb


BB_HASH_NAMESPACE_BEGIN {

template<> struct hash<bb::acct_t> : public int_hash<bb::acct_t> { };

} BB_HASH_NAMESPACE_END


#endif // BB_CORE_ACCT_H

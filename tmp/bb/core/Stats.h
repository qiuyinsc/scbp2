#ifndef BB_CORE_STATS_H
#define BB_CORE_STATS_H

/* Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved. */


#include <cmath>
#include <vector>
#include <list>
#include <boost/cstdint.hpp>
#include <boost/optional.hpp>
#include <boost/scoped_array.hpp>

namespace bb
{
    class VarianceCalculator;
    class QuantileCalculator;
};

namespace boost {
namespace serialization {
    // fwd decl, see StatsSerialization.h
    template<class Archive>
    void serialize(Archive &ar, bb::VarianceCalculator &b, const unsigned int version);

    template<class Archive>
    void save(Archive &ar, const bb::QuantileCalculator &b, const unsigned int version);
    template<class Archive>
    void load(Archive &ar, bb::QuantileCalculator &b, const unsigned int version);

} // namespace serialize
} // namespace boost

namespace bb
{

/// Calculates min,max,mean,std deviation for a numeric sequence.
class VarianceCalculator
{
public:
    VarianceCalculator();

    void update(double value);

    uint64_t getCount() const { return m_count; }
    double getMean() const { return m_mean; }
    double getVariance() const { return m_devSum/double(m_count-1); }
    double getStdev() const { return sqrt(getVariance()); }
    double getMin() const { return m_min; }
    double getMax() const { return m_max; }

protected:
    template<class Archive>
    friend void boost::serialization::serialize(Archive &ar, VarianceCalculator &b, const unsigned int version);

    uint64_t m_count;
    double m_mean, m_devSum, m_min, m_max;
};

/// Convolves a sequence f(x) by a square filter that's windowSize wide.
class SimpleMovingAverageCalculator
{
public:
    SimpleMovingAverageCalculator(int windowSize) : m_desiredSize(windowSize), m_size(0), m_updateCount(0), m_sum(0.0) {}

    boost::optional<double> update(double value);
    int getUpdateCount() const { return m_updateCount; }

protected:
    int m_desiredSize;
    int m_size;
    uint64_t m_updateCount;
    double m_sum;
    std::list<double> m_history;
};

/// Calculates the RSquared of a prediction.
class RSquaredCalculator
{
public:
    RSquaredCalculator() : m_sumSquaredError(0.0) {}

    uint64_t getCount() const { return m_yVariance.getCount(); }
    double getYMean() const { return m_yVariance.getMean(); }
    double getYVariance() const { return m_yVariance.getVariance(); }
    double getRSquared() const { return 1.0 - (m_sumSquaredError / ((getCount()-1) * getYVariance())); }

    void update(double y, double prediction)
    {
        double error = y - prediction;
        m_yVariance.update(y);
        m_sumSquaredError += error*error;
    }

protected:
    VarianceCalculator m_yVariance;
    double m_sumSquaredError;
};

/// This is a method for calculating the approximate median (or any other quantile) from a
/// set of numbers, while only taking one pass over the numbers.
/// Based on Chambers' "Monitoring Networked Applications With Incremental Quantile Estimation",
/// see Numerical Recipes s. 8.5.2.
class QuantileCalculator
{
public:
    // Batch size. You may *10 this if you expect > 10^6 data values
    static const int NBUF = 1000;

    QuantileCalculator();
    QuantileCalculator(const QuantileCalculator &q);
    QuantileCalculator &operator=(const QuantileCalculator &q);

    /// Returns the number of data points seen so far.
    size_t getCount() const { return m_consumedSize + m_dataBufSize; }

    /// Assimilates a new value from the stream
    void update(double datum)
    {
        m_dataBuf[m_dataBufSize++] = datum;
        if(datum < m_minDatum) {m_minDatum = datum;}
        if(datum > m_maxDatum) {m_maxDatum = datum;}
        if(m_dataBufSize == NBUF) updateBatch(); // time for a batch update
    }

    /// Internally, the QuantileCalculator tries to measure the quantile values at an array
    /// of p-values. All other values will be interpolated. This returns all the p-values which
    /// are being measured. These value will be the same across all instances of QuantileCalculator.
    std::pair<const double*,const double*> getMeasuredQuantiles() const
        { return std::make_pair(m_pval.get(), m_pval.get()+m_numQuantiles); }

    /// Returns the estimated p-quantile for the data seen so far (e.g. p=0.5 for median).
    /// This shouldn't be called all that often (ideally wait several times NBUF input points).
    /// Getting several different p-values at once is ok, of course.
    double getQuantile(double p);

    /// Reset to initial state (no values counted)
    void reset();

    /// Gets quantiles from the already-batched data
    double getQuantileNoBatch(double p) const;

protected:
    void updateBatch();
    template<class Archive> friend void boost::serialization::save(Archive &ar, const QuantileCalculator &b, const unsigned int version);
    template<class Archive> friend void boost::serialization::load(Archive &ar, QuantileCalculator &b, const unsigned int version);

    const int m_numQuantiles; // if you want this to be modifiable, enhance reset() and load() to respect it
    int m_consumedSize, // number of data values which have been consumed into the quantile statistics
        m_dataBufSize;
    boost::scoped_array<double> m_pval, m_dataBuf, m_quantiles, m_newQuantiles;
    double m_minDatum, m_maxDatum;
};

} // namespace bb

#endif // BB_CORE_STATS_H

#ifndef BB_CORE_HOST_PORT_PAIR_H
#define BB_CORE_HOST_PORT_PAIR_H

/* Contents Copyright 2016 Athena Capital Research LLC. All Rights Reserved. */

#include <deque>
#include <string>
#include <utility> // pair

namespace bb {
typedef std::pair<std::string, int> HostPortPair;
typedef std::deque<HostPortPair> HostPortPairList;
}

#endif // BB_CORE_HOST_PORT_PAIR_H

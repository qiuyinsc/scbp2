#ifndef BB_CORE_STR_MAP_H
#define BB_CORE_STR_MAP_H

/* Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved. */


#include <cstring>
#include <map>
#include <boost/functional/hash.hpp>

#include <bb/core/hash.h>
#include <bb/core/hash_map.h>

namespace bb {

struct bb_ltstr
{
    bool operator()( const char* s1, const char* s2 ) const
    {
        return strcmp( s1, s2 ) < 0;
    }
};

/// string to template T mapping using strcmp as lt operator.  You
/// need to be sure to pass it a new string on assignment.  On
/// lookup, it doesn't matter if it's the same string pointer or not.
template<typename T>
class str_map : public std::map<const char*, T, bb_ltstr> {};

} // namespace bb

#endif // BB_CORE_STR_MAP_H


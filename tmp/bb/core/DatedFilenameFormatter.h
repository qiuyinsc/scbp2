#ifndef BB_CORE_DATEDFILENAMEFORMATTER_H
#define BB_CORE_DATEDFILENAMEFORMATTER_H

/* Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved. */


#include <string>
#include <bb/core/date.h>

namespace bb {

class DatedFilenameFormatter
{
public:
    DatedFilenameFormatter( const char* prefix, const bb::date_t& d, const char* postfix = ".bin" );
    DatedFilenameFormatter( const std::string& prefix, const bb::date_t& d, const std::string& postfix );
    DatedFilenameFormatter( const std::string& prefix, const bb::date_t& d
        , const std::string& postfix
        , const std::string& delim );

    inline operator const std::string& () const { return m_value; }
    inline const std::string& str() const { return m_value; }

private:
    std::string m_value;
};

} // namespace bb

#endif // BB_CORE_DATEDFILENAMEFORMATTER_H

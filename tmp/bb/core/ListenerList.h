#ifndef BB_CORE_LISTENERLIST_H
#define BB_CORE_LISTENERLIST_H

/* Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved. */


#include <boost/function.hpp>
#include <boost/bind.hpp>
#include <bb/core/shared_vector.h>

namespace bb {

/** Maintains a list of listeners of type T, T frequently can be a pointer type.
 *
 * @code
 * class ProviderListener
 * {
 * public:
 *     virtual ~ProviderListener() {}
 *     virtual void onUpdate( Data* d ) = 0;
 * };
 *
 * class Provider : public ListenerList<ProviderListener*>
 * {
 * public:
 *     void update( Data* d )
 *     {
 *         notify( boost::bind( &ProviderListener::onUpdate, _1, d ) );
 *     }
 * };
 *
 * class MyListener : public ProviderListener
 * {
 *     virtual void onUpdate( Data* d ) { cout << "Hello World!" << endl; }
 * };
 *
 * int main()
 * {
 *     Provider p;
 *     MyListener listener;
 *
 *     p.add( &listener );
 *     // or by subscription
 *     Subscription sub;
 *     p.subscribe( sub, &listener );
 *
 *     Data d;
 *     p.update( d );  // prints out "Hello World!"
 *
 * }
 *
 * @endcode
 */

template<typename T> class ListenerList
{
public:
    typedef bb::Subscription Subscription;

protected:
    typedef shared_vector<T> BaseList;

public:
    /// adds the listener to the end of the list if it doesn't exist
    /// returns true if the listener is added, else returns false because it already exists
    /// in the list.
    bool add( T v )
    {
        if( std::find( m_baseList.unsafe_begin(), m_baseList.unsafe_end(), v ) == m_baseList.unsafe_end() )
        {
            m_baseList.explicit_insert(m_baseList.unsafe_end(), v);
            return true;
        }
        return false;
    }

    /// removes the listener from the list if it exists. can be called from within a notification.
    void remove( T& v )
    {
        typename BaseList::unsafe_iterator i = std::find( m_baseList.unsafe_begin(), m_baseList.unsafe_end(), v );
        if( i != m_baseList.unsafe_end() )
            m_baseList.explicit_erase(i);
    }

    /// calls function 'f' on all listeners.
    void notify( const boost::function1<void,T> &f ) const
    {
        typename BaseList::safe_jterator iter(m_baseList);
        while(iter.hasNext())
            f(iter.next());
    }

    /// calls function 'f' on all listeners.
    template<typename F, typename A0> void notify( F f, A0 a0 ) const
    {
        notify( boost::bind( f, _1, a0 ) );
    }

    /// calls function 'f' on all listeners.
    template<typename F, typename A0, typename A1>
    void notify( F f, A0 a0, A1 a1 ) const
    {
        notify( boost::bind( f, _1, a0, a1 ) );
    }

    /// calls function 'f' on all listeners.
    template<typename F, typename A0, typename A1, typename A2>
    void notify( F f, A0 a0, A1 a1, A2 a2 ) const
    {
        notify( boost::bind( f, _1, a0, a1, a2 ) );
    }

    /// calls function 'f' on all listeners.
    template<typename F, typename A0, typename A1, typename A2, typename A3>
    void notify( F f, A0 a0, A1 a1, A2 a2, A3 a3 ) const
    {
        notify( boost::bind( f, _1, a0, a1, a2, a3 ) );
    }

    template<typename V> void notifyWith( V v )
    {
        typename BaseList::safe_jterator iter(m_baseList);
        while(iter.hasNext())
            iter.next()( v );
    }

    size_t size() const { return m_baseList.size(); }
    bool empty() const { return m_baseList.empty(); }

    void subscribe( bb::Subscription& outSub, T v ) { m_baseList.push_back(outSub, v); }
    template<typename C> void addSubscription( C* list, T v )
    {
        typename C::value_type sub;
        subscribe(sub, v);
        list->push_back(sub);
    }
    template<typename C> void addSubscription( std::insert_iterator<C> inserter, T v )
    {
        typename C::value_type sub;
        subscribe(sub, v);
        *inserter = sub;
    }

protected:
    mutable BaseList m_baseList;
};

} // namespace bb

#endif // BB_CORE_LISTENERLIST_H

#ifndef BB_CORE_CANCELABLEDNSRESOLVER_H
#define BB_CORE_CANCELABLEDNSRESOLVER_H

/* Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved. */


#include <bb/core/IDNSResolver.h>
#include <bb/core/shared_vector.h>

namespace bb {

// Proxies for a non-cancelable DNSResolver and provides cancellation,
// meeting the contract for ICancelableDNSResolver.
class CancelableDNSResolver : public ICancelableDNSResolver
{
public:
    CancelableDNSResolver( const IDNSResolverPtr& base );

    virtual ~CancelableDNSResolver();

    virtual void getFQDN( Subscription& outSub,
                          const GetFQDNCallback& resultCb,
                          const ErrorCallback& errorCb );

    virtual void getFQDN( Subscription& outSub,
                          const char* hostname,
                          const GetFQDNCallback& resultCb,
                          const ErrorCallback& errorCb );

    virtual void lookupHostWithFQDN( Subscription& outSub,
                                     const char* hostname,
                                     const LookupHostWithFQDNCallback& resultCb,
                                     const ErrorCallback& errorCb );

    virtual void lookupHost( Subscription& outSub,
                             const char* host,
                             const LookupHostCallback& resultCb,
                             const ErrorCallback& errorCb );

    virtual void lookupAddr( Subscription& outSub,
                             const char* host, int port,
                             const LookupAddrCallback& resultCb,
                             const ErrorCallback& errorCb );

    virtual void lookupName( Subscription& outSub,
                             const sockaddr_ipv4_t& addr,
                             const LookupNameCallback& resultCb,
                             const ErrorCallback& errorCb );

    virtual void lookupNameFQ( Subscription& outSub,
                               const sockaddr_ipv4_t& addr,
                               const LookupNameCallback& resultCb,
                               const ErrorCallback& errorCb );
private:

    BB_FWD_DECLARE_SHARED_PTR(Invalidator);
    BB_FWD_DECLARE_SHARED_PTR(PendingRequest);

    template<typename T>
    class PendingRequestT;

    template<typename T>
    class PendingRequest2T;

    template<typename T>
    bb::shared_ptr< PendingRequestT< T > >
    inline setupForCancel( Subscription&, const T&, const ErrorCallback& );

    template<typename T>
    bb::shared_ptr< PendingRequest2T< T > >
    inline setupForCancel2( Subscription&, const T&, const ErrorCallback& );

    const IDNSResolverPtr m_baseResolver;
    shared_vector<InvalidatorPtr> m_pending;
};

} // namespace bb

#endif // BB_CORE_CANCELABLEDNSRESOLVER_H

#ifndef BB_CORE_CONSTS_BASIC_CONSTS_H
#define BB_CORE_CONSTS_BASIC_CONSTS_H

/* Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved. */


#include <bb/core/utils.h>

namespace basic {

enum event_code_t
{
    EVENT_UNKNOWN                = '\0',
    EVENT_Emergency_Halt         = 'A',
    EVENT_Emergency_Resumption   = 'B',
    EVENT_End_Of_Transmissions   = 'C',
    EVENT_End_Of_System_Hours    = 'E',
    EVENT_End_Of_Market_Hours    = 'M',
    EVENT_Start_Of_Transmissions = 'O',
    EVENT_Start_Of_Market_Hours  = 'Q',
    EVENT_Emergency_Quote_Only   = 'R',
    EVENT_Start_Of_System_Hours  = 'S',
} BB_PACK(event_code_t);

enum market_category_t
{
    MARKET_UNKNOWN       = '\0',
    MARKET_Not_Available = ' ',
    MARKET_Global        = 'G',
    MARKET_Global_Select = 'Q',
    MARKET_Capital       = 'S',
    MARKET_Non_Nasdaq    = 'T',
} BB_PACK(market_category_t);

enum financial_status_t
{
    FINANCIAL_STATUS_UNKNOWN                       = '\0',
    FINANCIAL_STATUS_Complied                      = ' ',
    FINANCIAL_STATUS_Deficient                     = 'D',
    FINANCIAL_STATUS_Delinquent                    = 'E',
    FINANCIAL_STATUS_Deficient_Bankrupt            = 'G',
    FINANCIAL_STATUS_Deficient_Delinquent          = 'H',
    FINANCIAL_STATUS_Delinquent_Bankrupt           = 'J',
    FINANCIAL_STATUS_Deficient_Delinquent_Bankrupt = 'K',
    FINANCIAL_STATUS_Bankrupt                      = 'Q',
} BB_PACK(financial_status_t);

enum security_class_t
{
    SECURITY_UNKNOWN    = '\0',
    SECURITY_Nasdaq     = 'Q',
    SECURITY_Non_Nasdaq = 'T',
} BB_PACK(security_class_t);

enum trading_state_t
{
    TRADING_STATE_UNKNOWN    = '\0',
    TRADING_STATE_Halt       = 'H',
    TRADING_STATE_Quote_Only = 'Q',
    TRADING_STATE_Trading    = 'T',
} BB_PACK(trading_state_t);

enum reg_sho_action_t
{
    REG_SHO_ACTION_UNKNOWN              = '\0',
    REG_SHO_ACTION_Price_Test_Inactive  = '0',
    REG_SHO_ACTION_Price_Test_Active    = '1',
    REG_SHO_ACTION_Price_Test_Continued = '2',
} BB_PACK(reg_sho_action_t);

} // namespace basic

#endif // BB_CORE_CONSTS_BASIC_CONSTS_H

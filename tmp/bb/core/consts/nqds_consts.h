#ifndef BB_CORE_CONSTS_NQDS_CONSTS_H
#define BB_CORE_CONSTS_NQDS_CONSTS_H

/* Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved. */


namespace nqds {

typedef enum {
    HALT='A', RESUME='B', CLOSE='C', STARTDAY='I', ENDDAY='J',
    ENDRETRANS='K', RESET='L', STARTTEST='M', ENDTEST='N',
    OPEN='O', LINEINT='T', ENDTRANS='Z', INVALID=0
} control_t;

}

#endif // BB_CORE_CONSTS_NQDS_CONSTS_H

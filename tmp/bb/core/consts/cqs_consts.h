#ifndef BB_CORE_CONSTS_CQS_CONSTS_H
#define BB_CORE_CONSTS_CQS_CONSTS_H

/* Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved. */


#include <iosfwd>

namespace cqs {

typedef enum {
    STARTDAY='I', EENDDAY='J', OPEN='O', CLOSE='C',
    ENDREQ='K', ENDTRANS='Z', STARTTEST='M', ENDTEST='N',
    LINEINT='T', RESET='L', WIPEOUT='P', INVALID_CONTROL=0
} control_t;

/*
typedef enum {
    REGULAR='@', ACQUISITION='A', BUNCHED='B', CASH='C', DISTRIBUTION='D', BUNCHED_SOLD='G',
    RULE155='K', SOLD_LAST='L', NEXT_DAY='N', OPENED='O', PRIOR_REF='P', MARKET_CENTER='M',
    SELLER='R', SPLIT='S', FORM_T='T', AVG_PX='W', SOLD='Z', STOPPED_REG='1',
    STOPPED_SOLD_LAST='2', STOPPED_SOLD='3', INVALID_COND=0
} condition_t;

typedef enum {
    HALT_ACTION='H', REG_ACTION=' '
} action_t;
*/

typedef enum {
    xUNKNOWN, xAMEX = 'A', xBOST = 'B', xCIN = 'C', xNASD = 'D', xCQS = 'E',
    xISLD = 'I', xCHI = 'M', xNY = 'N', xPAC = 'P', xNASDAQ = 'T', xCHI_OPT = 'W',
    xPHI = 'X'
} exchange_t;

typedef enum {
    UNKNOWN, NYSE, AMEX
} network_t;

std::ostream &operator <<(std::ostream &out, const cqs::exchange_t &);

} // namespace cqs

#endif // BB_CORE_CONSTS_CQS_CONSTS_H

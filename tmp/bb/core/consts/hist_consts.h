#ifndef BB_CORE_CONSTS_HIST_CONSTS_H
#define BB_CORE_CONSTS_HIST_CONSTS_H

/* Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved. */


namespace hist {

/// Used to start, stop, and pause sending of data.  Server sends
/// an END on the end of stream.
typedef enum { START, PAUSE, STOP, END, INVALID } action_t;

}

#endif // BB_CORE_CONSTS_HIST_CONSTS_H

#ifndef BB_CORE_CONSTS_NYSE_CONSTS_H
#define BB_CORE_CONSTS_NYSE_CONSTS_H

/* Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved. */


#include <iosfwd>

#include <bb/core/utils.h>

namespace NYSECommon {

/// This is shared by OpenBook, NYSE Trades & NYSE Quotes, make sure to check all specs when changing
/// or enhancing these constants.
enum retransmit_flag_t
{
    FLAG_UNKNOWN = 0,
    FLAG_ORIGINAL_MESSAGE = 1,
    FLAG_RETRANSMITTED = 2,
    FLAG_MESSAGE_REPLAY = 3,
    FLAG_RETRANSMISSION_OF_MESSAGE_REPLAY = 4,
    FLAG_REFRESH_RETRANSMISSION = 5,
} BB_PACK(retransmit_flag_t);

// shared by Trades & Quotes
enum exchange_id_t
{
    EXCHANGE_ID_UNKNOWN = '\0',
    EXCHANGE_ID_NYSE = 'N',
} BB_PACK(exchange_id_t);

enum security_type_t
{
    SECURITY_TYPE_UNKNOWN = '\0',
    SECURITY_TYPE_EQUITY = 'E',
} BB_PACK(security_type_t);

} // namespace NYSECommon


namespace OpenBook {

enum trading_indicator_t
{
    IND_UNKNOWN = '\0',
    IND_TRADING = 'T',
    IND_HALTED = 'H',
};
BOOST_STATIC_ASSERT(sizeof(trading_indicator_t) == 4);

struct open_book_entry_t
{
    open_book_entry_t() : m_px(0.0), m_quantity(0), m_numOrders(0) {}
    double m_px;
    int m_quantity;
    int m_numOrders; // numOrders is currently unsupported, it will be zero
};
BOOST_STATIC_ASSERT(sizeof(open_book_entry_t) == 16);

std::ostream &operator<<(std::ostream &out, const open_book_entry_t &e);

} // namespace OpenBook


namespace NYSEQuotes {

enum quote_condition_t
{
    QUOTE_COND_UNKNOWN = '\0',
    QUOTE_COND_SLOW_ON_ASK = 'A',
    QUOTE_COND_SLOW_ON_BID = 'B',
    QUOTE_COND_CLOSING = 'C',
    QUOTE_COND_E = 'E', // slow on the bid due to an LRP or Gap Quote
    QUOTE_COND_F = 'F', // slow on the ask due to an LRP or Gap Quote
    QUOTE_COND_SLOW_ON_BOTH = 'H',
    QUOTE_COND_NON_FIRM = 'N',
    QUOTE_COND_OPENING = 'O',
    QUOTE_COND_REGULAR = 'R',
    QUOTE_COND_U = 'U', // slow on the bid and ask due to an LRP or Gap Quote
    QUOTE_COND_W = 'W', // slow on the bid and ask due to a "set slow list"
};
BOOST_STATIC_ASSERT(sizeof(quote_condition_t) == 4);

} // namespace NYSEQuotes


namespace NYSETrades {

// enumerated values from the NYSE Trades Client Specification 1.7a dated 2010-04-23

enum trade_cond1_t
{
    COND1_NA = '\0', // N/A
    COND1_REGULAR_SALE = '@',
    COND1_CASH = 'C',
    COND1_NEXT_DAY_TRADE = 'N',
    COND1_SELLER = 'R',
} BB_PACK(trade_cond1_t);

enum trade_cond2_t
{
    COND2_NA = '\0', // N/A
    COND2_ISO = 'F',
    COND2_OPENED = 'O',
    COND2_DERIVATIVE = '4',
    COND2_REOPENING = '5',
    COND2_CLOSING = '6',
} BB_PACK(trade_cond2_t);

enum trade_cond3_t
{
    COND3_NA = '\0', // N/A
    COND3_SOLD_LAST = 'L',
    COND3_EXTENDED_HOURS = 'T',
    COND3_EXTENDED_HOURS_SOLD = 'U',
    COND3_SOLD = 'Z',
} BB_PACK(trade_cond3_t);

enum trade_cond4_t
{
    COND4_NA = '\0', // N/A
    COND4_AVERAGE_PRICE_TRADE = 'B',
    COND4_AUTO_EXECUTION = 'E',
    COND4_PRICE_VARIATION = 'H',
    COND4_CAP_ELECTION_TRADE = 'I',
    COND4_RULE_127 = 'K',
    COND4_MARKET_CENTER_OFFICIAL_CLOSE = 'M',
    COND4_PRIOR_REFERENCE_PRICE = 'P',
    COND4_MARKET_CENTER_OFFICIAL_OPEN = 'Q',
    COND4_CROSS_TRADE = 'X',
} BB_PACK(trade_cond4_t);

// the NYSE uses some but not all of the same codes, and in different order, in corrections vs. trades

enum corrected_trade_cond1_t
{
    CORR_COND1_NA = '\0', // N/A
    CORR_COND1_REGULAR_SALE = '@',
    CORR_COND1_CASH = 'A',
    CORR_COND1_NEXT_DAY_TRADE = 'N',
    CORR_COND1_SELLER = 'R',
} BB_PACK(corrected_trade_cond1_t);

enum corrected_trade_cond2_t
{
    CORR_COND2_NA = '\0', // N/A
    CORR_COND2_SOLD_LAST = 'L',
    CORR_COND2_OPENED = 'O',
    CORR_COND2_SOLD = 'Z',
} BB_PACK(corrected_trade_cond2_t);

enum corrected_trade_cond3_t
{
    CORR_COND3_NA = '\0', // N/A
    CORR_COND3_AVERAGE_PRICE_TRADE = 'B',
    CORR_COND3_AUTO_EXECUTION = 'E',
    CORR_COND3_RULE_127 = 'J',
} BB_PACK(corrected_trade_cond3_t);

enum corrected_trade_cond4_t
{
    CORR_COND4_NA = '\0', // N/A
    CORR_COND4_REGULAR_SALE = '@',
} BB_PACK(corrected_trade_cond4_t);

} // namespace NYSETrades


namespace NYSEAlerts {

enum alerts_sec_type_t
{
    SEC_TYPE_NA = '\0',
    SEC_TYPE_COMMON_STOCK = 'A',
    SEC_TYPE_PREFERRED_STOCK = 'B',
    SEC_TYPE_WARRANT = 'C',
    SEC_TYPE_RIGHT = 'D',
    SEC_TYPE_CORPORATE_BOND = 'E',
    SEC_TYPE_TREASURY_BOND = 'F',
    SEC_TYPE_STRUCTURED_PRODUCT = 'G',
    SEC_TYPE_ADR_COMMON = 'H',
    SEC_TYPE_ADR_PREFERRED = 'I',
    SEC_TYPE_ADR_WARRANTS = 'J',
    SEC_TYPE_ADR_RIGHTS = 'K',
    SEC_TYPE_ADR_CORPORATE_BOND = 'L',
    SEC_TYPE_NY_REGISTERED_SHARE = 'M',
    SEC_TYPE_GLOBAL_REGISTERED_SHARE = 'N',
    SEC_TYPE_INDEX = 'O',
    SEC_TYPE_FUND = 'P',
    SEC_TYPE_BASKET = 'Q',
    SEC_TYPE_UNIT = 'R',
    SEC_TYPE_LIQUIDATING_TRUST = 'S',
    SEC_TYPE_UNKNOWN = 'U'
};

enum financial_status_t
{
    FISTAT_NONE = 0,
    FISTAT_BELOW_LISTING_STANDARDS = 1,
    FISTAT_LATE_FILING = 2,
    FISTAT_BELOW_LISTING_STANDARDS_AND_LATE_FILING = 3,
};

/// These values suggest that this is a bitfield, but the documentation
/// doesn't actually say.
enum corporate_action_t
{
    CORP_ACT_NONE = 0,
    CORP_ACT_EX_DIVIDEND = 1,
    CORP_ACT_EX_DISTRIBUTION = 2,
    CORP_ACT_EX_RIGHTS = 4,
    CORP_ACT_NEW = 8,
    CORP_ACT_EX_INTEREST = 16,
};

enum security_status_t
{
    SS_UNKNOWN = 0,
    SS_REGULATORY_IMBALANCE = 1,
    SS_CANCEL_REGULATORY_IMBALANCE = 2,
    SS_OPENING_DELAY = 3,
    SS_TRADING_HALT = 4,
    SS_RESUME = 5,
    SS_NO_OPEN_OR_NO_RESUME = 6,
    SS_PRICE_INDICATION = 7,
    SS_TRADING_RANGE_INDICATION = 8,
    SS_PRE_OPENING_INDICATION = 9,
    SS_TRADE_DISSEMINATION_TIME = 10,
};

/// The SecurityStatus field of a Market Imbalance message.
enum market_imbalance_status_t
{
    MI_UNKNOWN = 0,
    MI_BUY = '7',
    MI_SELL = '8',
    MI_MOC_BUY = '9',
    MI_MOC_SELL = 'A',
    MI_NO_IMBALANCE = 'C',
    MI_NO_MOCIMBALANCE = 'D'
};

/// The SecurityStatus field of an Opening Delay or Trading Halt message.
enum delay_halt_status_t
{
    DH_UNKNOWN = 0,
    DH_OPENING_DELAY = '1',
    DH_TRADING_HALT = '2',
    DH_RESUME = '3',
    DH_NO_OPEN_OR_NO_RESUME = '4',
};

/// The SecurityStatus field of an NYSE Indication message
enum indication_status_t
{
    IND_UNKNOWN = 0,
    IND_PRICE_INDICATION = '5',
    IND_TRADING_RANGE_INDICATION = '6',
    IND_PRE_OPENING_INDICATION = 'B',
};

/// The SecurityStatus field of a Trade Dissemination Time message
enum trade_dissemination_status_t
{
    TDT_UNKNOWN = 0,
    TDT_TRADE_DISSEMINATION_TIME = 'T',
};

enum halt_condition_t
{
    HC_UNKNOWN = '\0',
    HC_NONE = '~',
    HC_AS_OF_UPDATE = 'A',
    HC_NEWS_DISSEMINATION = 'D',
    HC_ORDER_INFLUX = 'E',
    HC_ORDER_IMBALANCE = 'I',
    HC_NEWS_DISSEMINATION_RELATED = 'J', // due to related security
    HC_NEWS_PENDING_RELATED = 'K',       // due to related security
    HC_ADDITIONAL_INFORMATION = 'M',
    HC_NEWS_PENDING = 'P',
    HC_RELATED = 'Q',                    // due to related security
    HC_RELATED_SECURITY = 'S',
    HC_RESUME = 'T',                     // due to related security
    HC_VIEW_COMMON = 'V',                // in view of common
    HC_EQUIPMENT_CHANGEOVER = 'X',
    HC_SUB_PENNY_TRADING = 'Y',
    HC_NO_OPEN_OR_NO_RESUME = 'Z',
};

enum adjustment_t
{
    ADJ_NONE = 0,
    ADJ_CANCEL = 1,
    ADJ_CORRECTION = 2,
};

enum circuit_breaker_status_t
{
    CIRCUIT_BREAKER_UNKNOWN = '\0',
    CIRCUIT_BREAKER_FOR_HALF_HOUR = '1',
    CIRCUIT_BREAKER_FOR_HOUR = '2',
    CIRCUIT_BREAKER_FOR_TWO_HOURS = '3',
    CIRCUIT_BREAKER_FOR_DAY = '4',
};

enum exchange_id_t
{
    EXCHANGE_ID_UNKNOWN = '\0',
    EXCHANGE_ID_NYSE = 'N',
};

enum short_sale_restriction_indicator_t
{
    SHORT_SALE_RESTRICTION_UNKNOWN = '\0',
    SHORT_SALE_RESTRICTION_ACTIVATED = 'A',
    SHORT_SALE_RESTRICTION_CONTINUED = 'C',
    SHORT_SALE_RESTRICTION_DEACTIVATED = 'D',
    SHORT_SALE_RESTRICTION_NOT_IN_EFFECT = '~',
    SHORT_SALE_RESTRICTION_IN_EFFECT = 'E',
};

} // namespace NYSEAlerts


namespace NYSELrp {

enum lrp_change_indicator_t
{
    LRPC_UNKNOWN = 0,
    LRPC_LOW = 'L',
    LRPC_HIGH = 'H',
    LRPC_BOTH = 'B',
};
BOOST_STATIC_ASSERT(sizeof(lrp_change_indicator_t) == 4);

} // NYSELrp

#endif // BB_CORE_CONSTS_NYSE_CONSTS_H

#ifndef BB_CORE_CONSTS_BRUT_CONSTS_H
#define BB_CORE_CONSTS_BRUT_CONSTS_H

/* Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved. */


namespace brut {

const unsigned int GTCLOSE = 99998;
const unsigned int GTD = 99999;

typedef enum {
    CANCELLED,
    TIMEOUT,
    HALT,
    SUPERVISORY,
    UNKNOWN_REASON
} reason_t;


typedef enum {
    YES,
    SUBSCRIBER_ONLY,
    UNKNOWN
} display_t;

}

#endif // BB_CORE_CONSTS_BRUT_CONSTS_H

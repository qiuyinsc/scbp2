#ifndef BB_CORE_CONSTS_TSX_CONSTS_H
#define BB_CORE_CONSTS_TSX_CONSTS_H

/* Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved. */


#include <bb/core/utils.h>

namespace bb {
namespace tsx {

enum update_type_t
{
    UPDATE_TYPE_UNKNOWN                = '\0',
    UPDATE_TYPE_Assigned_Time_Priority = 'A',
    UPDATE_TYPE_Booked                 = 'B',
    UPDATE_TYPE_Cancelled              = 'C',
    UPDATE_TYPE_Killed                 = 'K',
    UPDATE_TYPE_Frozen                 = 'F',
    UPDATE_TYPE_Price_Assigned         = 'P',
} BB_PACK(update_type_t);

enum trade_type_t
{
    TRADE_TYPE_UNKNOWN = '\0',
    TRADE_TYPE_Cancel  = 'C',
    TRADE_TYPE_Trade   = 'T',
} BB_PACK(trade_type_t);

enum mbx_type_t
{
    MBX_TYPE_UNKNOWN = '\0',
    MBX_TYPE_Assign_COP   = 'O',
    MBX_TYPE_Assign_Limit = 'L',
    MBX_TYPE_Assign_CCP   = 'C'
} BB_PACK(mbx_type_t);

enum stock_state_t
{
    STOCK_STATE_UNKNOWN                           = '\0',
    STOCK_STATE_Authorized                        = 'A',
    STOCK_STATE_Authorized_Delayed                = 'D',
    STOCK_STATE_Authorized_Frozen                 = 'F',
    STOCK_STATE_Authorized_Halted                 = 'H',
    STOCK_STATE_Authorized_Price_Movement_Delayed = 'P',
    STOCK_STATE_Inhibited                         = '0',
    STOCK_STATE_Inhibited_Delayed                 = '1',
    STOCK_STATE_Inhibited_Frozen                  = '2',
    STOCK_STATE_Inhibited_Halted                  = '3',
    STOCK_STATE_Inhibited_Price_Movement_Delay    = '4',
    STOCK_STATE_Inhibited_Price_Movement_Frozen   = '5',
} BB_PACK(stock_state_t);

enum market_state_t
{
    MARKET_STATE_UNKNOWN                  = '\0',
    MARKET_STATE_Preopen                  = 'N',
    MARKET_STATE_Opening                  = 'G',
    MARKET_STATE_Open                     = 'O',
    MARKET_STATE_Closed                   = 'C',
    MARKET_STATE_Extended_Hours_Open      = 'E',
    MARKET_STATE_Extended_Hours_Close     = 'F',
    MARKET_STATE_Extended_Hours_Cxls      = 'X',
    MARKET_STATE_MOC_Imbalance            = 'M',
    MARKET_STATE_CCP_Determination        = 'D',
    MARKET_STATE_Price_Movement_Extension = 'P',
    MARKET_STATE_Closing                  = 'S',
} BB_PACK(market_state_t);

enum trade_marker_code_t
{
    TRADE_MARKER_UNKNOWN          = '\0',
    TRADE_MARKER_None              = ' ',
    TRADE_MARKER_Delayed_Delivery  = 'B',
    TRADE_MARKER_Contingent_Trade  = 'C',
    TRADE_MARKER_Cash              = 'D',
    TRADE_MARKER_Non_Boardlot      = 'E',
    TRADE_MARKER_Mandatory_Cash    = 'F',
    TRADE_MARKER_VWAP_Trade        = 'G',
    TRADE_MARKER_Sets_Last_Price   = 'K',
    TRADE_MARKER_Sets_Open_Price   = 'L',
    TRADE_MARKER_Special_Terms     = 'M',
    TRADE_MARKER_Non_Voting_Shares = 'N',
    TRADE_MARKER_Basis_Trade       = 'O',
    TRADE_MARKER_Accrued_Interest  = 'P',
    TRADE_MARKER_MOC_Trade         = 'Q',
    TRADE_MARKER_Restricted_Voting = 'R',
    TRADE_MARKER_Special_Session   = 'S',
    TRADE_MARKER_US_Dollars        = 'U',
    TRADE_MARKER_Subordinate       = 'V',
    TRADE_MARKER_Bypass_Trade      = 'W',
    TRADE_MARKER_Internal_Cross    = 'X',
} BB_PACK(trade_marker_code_t);

enum quote_marker_code_t
{
    QUOTE_MARKER_UNKNOWN = '\0',
    QUOTE_MARKER_None    = ' ',
    QUOTE_MARKER_Halted  = 'A',
} BB_PACK(quote_marker_code_t);

enum index_marker_code_t
{
    INDEX_MARKER_UNKNOWN = '\0',
    INDEX_MARKER_None    = ' ',
    INDEX_MARKER_Close   = 'C',
} BB_PACK(index_marker_code_t);

enum dividend_code_t
{
    DIVIDEND_UNKNOWN                      = 0,
    DIVIDEND_Cash_Equivalent              = 1,
    DIVIDEND_Option_Traded                = 2,
    DIVIDEND_Increase_In_Rate             = 3,
    DIVIDEND_Decrease_In_Rate             = 4,
    DIVIDEND_Stock_Dividend               = 5,
    DIVIDEND_First_Dividend_Since_Listing = 6,
    DIVIDEND_First_Dividend_Since_Issue   = 7,
    DIVIDEND_Following_Stock_Split        = 8,
    DIVIDEND_Extra_Dividend               = 9,
    DIVIDEND_US_Funds                     = 10,
    DIVIDEND_Estimated_Dividend           = 11,
    DIVIDEND_Foreign_Currency             = 12,
    DIVIDEND_Partial_Arrears              = 13,
    DIVIDEND_Tax_Deferred                 = 14,
    DIVIDEND_First_Dividend_Since_Reorg   = 15,
    DIVIDEND_Rights_Or_Warrants           = 16,
    DIVIDEND_Stock                        = 17,
    DIVIDEND_Payments_Resumed             = 18,
    DIVIDEND_Omitted                      = 19,
    DIVIDEND_Deferred                     = 20,
    DIVIDEND_Arrears_Full                 = 21,
    DIVIDEND_Rescinded                    = 22,
    DIVIDEND_Formula                      = 99,
} BB_PACK(dividend_code_t);

} // namespace tsx
} // namespace bb

#endif // BB_CORE_CONSTS_TSX_CONSTS_H

#ifndef BB_CORE_CONSTS_ARCA_CONSTS_H
#define BB_CORE_CONSTS_ARCA_CONSTS_H

/* Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved. */


namespace arca {

/**
    Public enumerations for ARCABOOK bindings.
**/

/// describes the type of auction
enum auction_t {
    AT_OPEN      = 'O',    /// Open
    AT_MARKET    = 'M',    /// Market
    AT_HALT      = 'H',    /// Halt
    AT_CLOSE     = 'C',    /// Close
    AT_INVALID   = 0
} __attribute__((__packed__));

/// the security type
enum security_t {
    SEC_ADR                          = 'A',
    SEC_COMMON_STOCK                 = 'C',
    SEC_DEBENTURES                   = 'D',
    SEC_ETF                          = 'E',
    SEC_FORIEGN                      = 'F',
    SEC_AMERICAN_DEPOSITARY_SHARES   = 'H',
    SEC_INDEX_LINKED_NOTES           = 'L',
    SEC_MISC_LIQUID_TRUST            = 'M',
    SEC_ORDINARY_SHARES              = 'O',
    SEC_PREFERRED_STOCK              = 'P',
    SEC_RIGHTS                       = 'R',
    SEC_SHARES_BENEFICIARY_TRUST     = 'S',
    SEC_TEST                         = 'T',
    SEC_UNITS                        = 'U',    // 'I' also represents UNITS in the XDP Common Specs v2.0g
    SEC_WARRANTS                     = 'W',
    SEC_INVALID                      = 0
} __attribute__((__packed__));


/// the Halt condition codes
/// MWCBHL -- Market Wide Circuit Breaker Halt Level
enum halt_cond_t {
    SECURITY_NOT_HALTED              = '~',
    NEWS_DISSEMINATION               = 'D',
    ORDER_IMBALANCE                  = 'I',
    NEWS_PENDING                     = 'P',
    LULD_PAUSE                       = 'M',
    EQUIPMENT_CHANGEOVER             = 'X',
    NO_OPEN_NO_RESUME                = 'Z',
    MWCBHL_1                         = '1',
    MWCBHL_2                         = '2',
    MWCBHL_3                         = '3'
} __attribute__((__packed__));


// Security status codes
// SSRC -- Short Sale Restriction Code
enum sec_sys_code_t{
    SSC_TRADING_HALT     = 4,
    SSC_RESUME           = 5,
    SSRC_ACTIVATED       = 'A',
    SSRC_CONTINUED       = 'C',
    SSRC_DEACTIVATED     = 'D'
}  __attribute__((__packed__));


enum trade_session_t {
    MORNING_SESSION                = 0x01,
    NATIONAL_SESSION               = 0x02,
    MORNING_NATIONAL_SESSION       = 0x03,
    LATE_SESSION                   = 0x04,
    NATIONAL_LATE_SESSION          = 0x06,
    MORNING_NATIONAL_LATE_SESSION  = 0x07
} __attribute__((__packed__));


/// reason code
enum reason_t {
    REASON_NOT_IMPLEMENTED = 0,
    REASON_CANCEL = 1,
    REASON_MODIFY_TAKEN_OFF_BOOK = 2,
    REASON_FILLED = 3,
	// The order's price has been modified and thus the
	// position has been lost
    REASON_CHANGE_LOST_POSITION = 5,
	// The order has been routed away. Some of the stocks in the
	// order still remain. So we  would have to modify the volume of the order
    REASON_CHANGE_DID_NOT_LOSE_POSITION = 6,
	// The order has been partially filled and some shares are still
	// available and thus we keep the position
    REASON_FILLED_DID_NOT_LOSE_POSITION = 7,
} __attribute__((__packed__));

/// describes the type of ARCABOOK system event this is
enum system_event_t {
    /** indicates that all orders in the book for the
        corresponding System Code should be cancelled, i.e. that it should be treated like a new day.
        The message will also include the next expected sequence number. In most cases this will be the
        Sequence # + 1 although it is possible that the system has been reset and the sequence numbers
        will restart at 1.
    **/
    SE_CLEAR_BOOK_BY_SYSTEM = 'C',
    /** indicates that all orders in the book for the
        corresponding System Code should be cancelled, i.e. that it should be treated like a new day.
        The message will also include the next expected sequence number. In most cases this will be the
        Sequence # + 1 although it is possible that the system has been reset and the sequence numbers
        will restart at 1.
    **/
    SE_CLEAR_BOOK_BY_SYMBOL = 'S',
    SE_INVALID = 0
}  __attribute__((__packed__));

} // namespace arca

#endif // BB_CORE_CONSTS_ARCA_CONSTS_H

#ifndef BB_CORE_CONSTS_UTDF_CONSTS_H
#define BB_CORE_CONSTS_UTDF_CONSTS_H

/* Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved. */


#include <bb/core/utils.h>

namespace utdf {

enum control_t
{
    STARTDAY = 'I',
    EENDDAY = 'J',
    OPEN = 'O',
    CLOSE = 'C',
    ENDREP = 'X',
    HALT = 'A',
    RESUME = 'B',
    ENDREQ = 'K',
    ENDTRANS = 'Z',
    STARTTEST = 'M',
    ENDTEST = 'N',
    LINEINT = 'T',
    RESET = 'L',
    ENDCONSOL = 'S',
    INVALID_CONTROL = 0
};

enum action_t
{
    HALT_ACTION = 'H',
    REG_ACTION = ' ',
    INVALID_ACTION = 0
};

enum market_t
{
    MKT_UNKNOWN = 0,
    MKT_NONE = ' ',
    MKT_AMEX = 'A',
    MKT_BX = 'B',
    MKT_NSX = 'C',
    MKT_FINRA_ADF = 'D',
    MKT_INDEPENDENT = 'E',
    MKT_ISE = 'I',
    MKT_CHX = 'M',
    MKT_NYSE = 'N',
    MKT_ARCA = 'P',
    MKT_NASDAQ = 'Q',
    MKT_CBOE = 'W',
    MKT_PHLX = 'X',
    MKT_BATS = 'Z',
} BB_PACK( market_t );

enum sale_condition_settlement_t
{
    SETTLE_UNKNOWN                   = '\0',
    SETTLE_Regular_Trade             = '@',
    SETTLE_Cash_Sale                 = 'C',
    SETTLE_Next_Day                  = 'N',
    SETTLE_Seller                    = 'R',
    SETTLE_Yellow_Flag_Regular_Trade = 'Y',
    SETTLE_Regular_Settlement        = ' '
} BB_PACK( sale_condition_settlement_t );

enum sale_condition_tt_exemption_t
{
    EXEMPTION_UNKNOWN                      = '\0',
    EXEMPTION_Intermarket_Sweep            = 'F',
    EXEMPTION_Opening_Prints               = 'O',
    EXEMPTION_Derivatively_Priced          = '4',
    EXEMPTION_Reopening_Prints             = '5',
    EXEMPTION_Closing_Prints               = '6',
    EXEMPTION_Placeholder_For_611_Exempt_7 = '7',
    EXEMPTION_Placeholder_For_611_Exempt_8 = '8',
    EXEMPTION_Placeholder_For_611_Exempt_9 = '9',
    EXEMPTION_None                         = ' ',
} BB_PACK( sale_condition_tt_exemption_t );

enum sale_condition_extended_t
{
    EXTENDED_UNKNOWN                                     = '\0',
    EXTENDED_Form_T                                      = 'T',
    EXTENDED_Sold_Last                                   = 'L',
    EXTENDED_Sold_out_of_Sequence                        = 'Z',
    EXTENDED_Extended_trading_hours_Sold_Out_of_Sequence = 'U',
    EXTENDED_None                                        = ' ',
} BB_PACK( sale_condition_extended_t );

enum sale_condition_detail_t
{
    DETAIL_UNKNOWN                                     = '\0',
    DETAIL_Acquisition                                 = 'A',
    DETAIL_Bunched_Trade                               = 'B',
    DETAIL_Distribution                                = 'D',
    DETAIL_Automatic_Execution                         = 'E',
    DETAIL_Bunched_Sold_Trade                          = 'G',
    DETAIL_Price_Variation_Trade                       = 'H',
    DETAIL_CAP_Election_Trade                          = 'I',
    DETAIL_Rule_127_or_155                             = 'K',
    DETAIL_Market_Center_Official_Close                = 'M',
    DETAIL_Prior_Reference_Price                       = 'P',
    DETAIL_Market_Center_Official_Open                 = 'Q',
    DETAIL_Split_Trade                                 = 'S',
    DETAIL_Average_Price_Trade                         = 'W',
    DETAIL_Cross_Trade                                 = 'X',
    DETAIL_Stopped_Stock_Regular_Trade                 = '1',
    DETAIL_Stopped_Stock_Sold_Last                     = '2',
    DETAIL_Stopped_Stock_Sold_Out_of_Sequence          = '3',
    DETAIL_None                                        = ' ',
} BB_PACK( sale_condition_detail_t );

enum consolidated_price_change_indicator_t
{
    CONS_PX_UNKNOWN                                   = '\0',
    CONS_PX_No_prices_changed                         = '0',
    CONS_PX_Consolidated_Last_price_changed           = '1',
    CONS_PX_Consolidated_Low_price_changed            = '2',
    CONS_PX_Consolidated_Last_and_low_prices_changed  = '3',
    CONS_PX_Consolidated_High_price_changed           = '4',
    CONS_PX_Consolidated_Last_and_High_prices_changed = '5',
    CONS_PX_Consolidated_High_and_Low_prices_changed  = '6',
    CONS_PX_All_Consolidated_prices_changed           = '7',
} BB_PACK( consolidated_price_change_indicator_t );

enum cancel_function_t
{
    CXL_FUNC_UNKNOWN = '\0',
    CXL_FUNC_Cancel  = 'C',
    CXL_FUNC_Error   = 'E',
} BB_PACK( cancel_function_t );

enum as_of_action_t
{
    AS_OF_UNKNOWN  = '\0',
    AS_OF_Add = 'A',
    AS_OF_Cancel = 'C',
} BB_PACK( as_of_action_t );

} // namespace utdf

#endif // BB_CORE_CONSTS_UTDF_CONSTS_H

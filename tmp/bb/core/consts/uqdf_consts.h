#ifndef BB_CORE_CONSTS_UQDF_CONSTS_H
#define BB_CORE_CONSTS_UQDF_CONSTS_H

/* Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved. */


#include <bb/core/utils.h>

namespace uqdf {

enum temp_suffix_t
{
    TEMP_SUFFIX_UNKNOWN         = '\0',
    TEMP_SUFFIX_Ex_Div          = 'A',
    TEMP_SUFFIX_Ex_Dist         = 'B',
    TEMP_SUFFIX_Ex_Rights       = 'C',
    TEMP_SUFFIX_New             = 'D',
    TEMP_SUFFIX_Ex_Interest     = 'E',
    TEMP_SUFFIX_Not_Applicable  = ' '
} BB_PACK( temp_suffix_t );

enum quote_condition_t
{
    QUOTE_COND_UNKNOWN                               = '\0',
    QUOTE_COND_Slow_Due_to_Ask_Side                  = '0',
    QUOTE_COND_Sub_Penny_Trading                     = '1',
    QUOTE_COND_Equipment_Changeover                  = '2',
    QUOTE_COND_Manual_Ask_Auto_Bid                   = 'A',
    QUOTE_COND_Manual_Bid_Auto_Ask                   = 'B',
    QUOTE_COND_News_Dissemination                    = 'D',
    QUOTE_COND_Slow_Due_to_Bid_Side                  = 'E',
    QUOTE_COND_Fast_Trading                          = 'F',
    QUOTE_COND_Trading_Range_Indication              = 'G',
    QUOTE_COND_Manual_Bid_and_Ask                    = 'H',
    QUOTE_COND_Order_Imbalance                       = 'I',
    QUOTE_COND_Related_Security_News_Dissemination   = 'J',
    QUOTE_COND_Related_Security_News_Pending         = 'K',
    QUOTE_COND_Closed                                = 'L',
    QUOTE_COND_Addl_Info                             = 'M',
    QUOTE_COND_Non_firm                              = 'N',
    QUOTE_COND_Opening_Automated                     = 'O',
    QUOTE_COND_News_Pending                          = 'P',
    QUOTE_COND_Addl_Info_Due_to_Related_Security     = 'Q',
    QUOTE_COND_Regular                               = 'R',
    QUOTE_COND_Related_Security                      = 'S',
    QUOTE_COND_Resume                                = 'T',
    QUOTE_COND_Manual_Bid_and_Ask_Non_firm           = 'U',
    QUOTE_COND_In_View_of_Common                     = 'V',
    QUOTE_COND_Slow_Due_to_Set_Slow_List             = 'W',
    QUOTE_COND_Order_Influx                          = 'X',
    QUOTE_COND_One_Sided_Automated                   = 'Y',
    QUOTE_COND_No_Open_No_Resume                     = 'Z'
} BB_PACK( quote_condition_t );

enum financial_status_t
{
    FIN_STATUS_UNKNOWN                      = '\0',
    FIN_STATUS_Normal                       = '0',
    FIN_STATUS_Bankrupt                     = '1',
    FIN_STATUS_Below_Standards              = '2',
    FIN_STATUS_Bankrupt_Below_Standards     = '3',
    FIN_STATUS_Late_Filing                  = '4',
    FIN_STATUS_Bankrupt_Late_Filing         = '5',
    FIN_STATUS_Below_Standards_Late_Filing  = '6',
    FIN_STATUS_Bankrupt_Below_Stdrds_Late   = '7',
    FIN_STATUS_Not_Applicable               = ' '
} BB_PACK( financial_status_t );

enum settlement_condition_t
{
    SETTLE_COND_UNKNOWN         = '\0',
    SETTLE_COND_Regular_Way     = 'A',
    SETTLE_COND_Cash_Only       = 'B',
    SETTLE_COND_Next_Day        = 'C',
    SETTLE_COND_Not_Applicable  = ' '
} BB_PACK( settlement_condition_t );

enum mkt_condition_t
{
    MKT_COND_UNKNOWN         = '\0',
    MKT_COND_Normal          = 'A',
    MKT_COND_Crossed         = 'B',
    MKT_COND_Locked          = 'C',
    MKT_COND_Not_Applicable  = ' '
} BB_PACK( mkt_condition_t );

enum nat_bbo_app_ind_t
{
    NAT_BBO_APP_IND_UNKNOWN     = '\0',
    NAT_BBO_APP_IND_No_Effect   = '0',
    NAT_BBO_APP_IND_No_Calc     = '1',
    NAT_BBO_APP_IND_Short_Form  = '2',
    NAT_BBO_APP_IND_Long_Form   = '3',
    NAT_BBO_APP_IND_Is_Quote    = '4'
} BB_PACK( nat_bbo_app_ind_t );

enum nasd_bbo_app_ind_t
{
    NASD_BBO_APP_IND_UNKNOWN         = '\0',
    NASD_BBO_APP_IND_No_Effect       = '0',
    NASD_BBO_APP_IND_Is_Quote        = '1',
    NASD_BBO_APP_IND_No_Calc         = '2',
    NASD_BBO_APP_IND_Appendage       = '3',
    NASD_BBO_APP_IND_Not_Applicable  = ' '
} BB_PACK( nasd_bbo_app_ind_t );

enum nasd_adf_app_ind_t
{
    NASD_ADF_APP_IND_UNKNOWN         = '\0',
    NASD_ADF_APP_IND_No_Effect       = '0',
    NASD_ADF_APP_IND_Is_Quote        = '1',
    NASD_ADF_APP_IND_Appendage       = '2',
    NASD_ADF_APP_IND_Not_Applicable  = ' '
} BB_PACK( nasd_adf_app_ind_t );

enum mm_desk_t
{
    MM_DESK_UNKNOWN                 = '\0',
    MM_DESK_SPACE                   = ' '
} BB_PACK( mm_desk_t );

} // namespace uqdf

#endif // BB_CORE_CONSTS_UQDF_CONSTS_H

#ifndef BB_CORE_CONSTS_SX_CONSTS_H
#define BB_CORE_CONSTS_SX_CONSTS_H

/* Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved. */


#include <boost/static_assert.hpp>

namespace bb {
namespace sx {
#ifndef SWIG
#define BB_SX_PACK( ENUM ) \
    __attribute__((__packed__)); \
    BOOST_STATIC_ASSERT( sizeof( ENUM ) == 1 )
#else
#define BB_SX_PACK( ENUM )
#endif

enum l1_category_t
{
    CAT_UNKNOWN = '\0',
    CAT_UQDF = 'Q',
    CAT_CQS = 'E',
    CAT_UTDF = 'T',
    CAT_CTS = 'S',
} BB_SX_PACK( l1_category_t );

enum mkt_session_id_t
{
    MKT_SESSION_UNKNOWN = '\0',
    MKT_SESSION_ID_ALL = 'A',
    MKT_SESSION_ID_US = 'U'
} BB_SX_PACK( mkt_session_id_t );

enum message_type_t
{
    MESSAGE_UNKNOWN = '\0',
    MESSAGE_Add_Order = 'A',
    MESSAGE_Order_Executed = 'E',
    MESSAGE_Order_Executed_With_Price = 'C',
    MESSAGE_Order_Cancelled = 'X',
    MESSAGE_Order_Deleted = 'D',
    MESSAGE_Order_Replaced = 'U',
    MESSAGE_Trade = 'P',
    MESSAGE_Cross_Trade = 'Q',
    MESSAGE_FTEN_Admin = '@',
    MESSAGE_Deprecated = '?',
    MESSAGE_Level_1 = '1',
} BB_SX_PACK( message_type_t );

enum l1_message_type_t
{
    L1_MESSAGE_UNKNOWN = '\0',

    // Quote types, category: 'Q'
    L1_MESSAGE_Short_Form_Quote = 'C',
    L1_MESSAGE_Long_Form_Quote = 'D',

    // Trade types, category: 'T' or 'S'
    L1_MESSAGE_Short_Form_Trade = 'A',
    L1_MESSAGE_Long_Form_Trade = 'W',
    L1_MESSAGE_Trade_Correction = 'Y',
    L1_MESSAGE_Cancel_or_Error_Trade = 'Z',
    L1_MESSAGE_Prior_Day_As_of_Trade = 'H',
} BB_SX_PACK( l1_message_type_t );

enum price_denominator_t
{
    PRICE_DENOM_UNKNOWN         = '\0',
    PRICE_DENOM_10              = 'A',
    PRICE_DENOM_100             = 'B',
    PRICE_DENOM_1000            = 'C',
    PRICE_DENOM_10000           = 'D',
    PRICE_DENOM_100000          = 'E',
    PRICE_DENOM_1000000         = 'F',
    PRICE_DENOM_10000000        = 'G',
    PRICE_DENOM_100000000       = 'H',
    PRICE_DENOM_1               = 'I',
    PRICE_DENOM_8               = '3',
    PRICE_DENOM_16              = '4',
    PRICE_DENOM_32              = '5',
    PRICE_DENOM_64              = '6',
    PRICE_DENOM_128             = '7',
    PRICE_DENOM_256             = '8',
    PRICE_DENOM_0               = '0',
} BB_SX_PACK( price_denominator_t );

#undef BB_SX_PACK

} // namespace sx
} // namespace bb

#endif // BB_CORE_CONSTS_SX_CONSTS_H

#ifndef BB_CORE_UUID_H
#define BB_CORE_UUID_H

/* Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved. */

#include <algorithm>
#include <boost/operators.hpp>

#include <bb/core/compat.h>


namespace bb {

//
// A globally unique identifier
//
class Uuid
    : public boost::less_than_comparable< Uuid
    , boost::equality_comparable< Uuid
    > >
{
public:
    /// Generates a new Uuid, based on the machine-specs and some randomness.
    static Uuid generate();
    /// Generates a new Uuid, based completely on randomness.
    static Uuid generateRandom();
    /// Generates a new Uuid from a string.
    /// The string must be of the form "1B4E28bA-2FA1-11D2-883F-B9A76", case-insensitive.
    /// Returns an invalid Uuid if the string is unparseable.
    static Uuid fromString( const char* sz );

public:
    enum UpperLower
    {
        UPPERCASE = 0,
        LOWERCASE = 1
    };
    enum FormattingMode
    {
        WITH_DASHES = 0, // aka dashing
        WITHOUT_DASHES = 1
    };
    /// Constructs a NULL Uuid.  A NULL is considered invalid.
    Uuid()                                    { uuid_clear(m_uuid); }
    /// Uuid copy constructor
    Uuid( const Uuid& uuid )                  { uuid_copy(this->m_uuid, uuid.m_uuid); }
    /// Constructs a Uuid from a raw uuid_t
    Uuid( const uuid_t uuid )                 { uuid_copy(m_uuid, uuid); }

    // Operators
    bool operator ==( const Uuid& rhs ) const { return uuid_compare(this->m_uuid, rhs.m_uuid) == 0; }
    bool operator <( const Uuid& rhs ) const  { return uuid_compare(this->m_uuid, rhs.m_uuid) < 0; }
    Uuid& operator =( const Uuid& rhs );

    /// Returns true if the Uuid is valid (not NULL).
    operator bool () const                    { return !uuid_is_null(m_uuid); }

    /// Returns a string version of the Uuid in the form: 1B4E28bA-2FA1-11D2-883F-B9A76
    /// By default the result is in uppercase.  Pass LOWERCASE to return it in lowercase.
    /// Also by default it will contain 4 dashes, unless you set it to WITHOUT_DASHES to strip those out.
    std::string toString( const UpperLower upper_lower = UPPERCASE,
        const FormattingMode formatting_mode = WITH_DASHES ) const;

    /// Kept here for legacy reasons
    std::string toString( const bool uppercase ) const
    {
        return toString ( uppercase ? UPPERCASE : LOWERCASE, WITH_DASHES );
    }
private:
    uuid_t   m_uuid;
};


inline Uuid Uuid::generate()
{
    uuid_t uuid;
    uuid_generate( uuid );
    return Uuid( uuid );
}

inline Uuid Uuid::generateRandom()
{
    uuid_t uuid;
    uuid_generate_random( uuid );
    return Uuid( uuid );
}

inline Uuid Uuid::fromString( const char* sz )
{
    uuid_t uuid;
    int res = uuid_parse( sz, uuid );
    if ( res == 0 )
        return Uuid( uuid );
    else
        return Uuid();
}

inline Uuid& Uuid::operator =( const Uuid& rhs )
{
    if (this == &rhs ) return *this;   // Gracefully handle self assignment
    uuid_copy(this->m_uuid, rhs.m_uuid);
    return *this;
}

inline std::string Uuid::toString( const UpperLower upper_lower,
        const FormattingMode formatting_mode ) const
{
    char sz[37];
    if ( upper_lower == UPPERCASE ) uuid_unparse_upper( m_uuid, sz );
    else                            uuid_unparse_lower( m_uuid, sz );
    if ( likely ( formatting_mode == WITH_DASHES ) )
    {
        return std::string( sz, 36 );
    }
    else
    {
        std::string with_dash ( sz, 36 );
        with_dash.erase(std::remove(with_dash.begin(), with_dash.end(), '-'), with_dash.end());
        return with_dash;
    }
}

inline std::ostream& operator <<( std::ostream &out, const bb::Uuid& uu )
{
    out << uu.toString();
    return out;
}

} // namespace bb

#endif // BB_CORE_UUID_H

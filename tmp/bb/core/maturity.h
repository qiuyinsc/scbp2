#ifndef BB_CORE_MATURITY_H
#define BB_CORE_MATURITY_H

/* Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved. */


#include <iosfwd>
#include <string>

#include <bb/core/date.h>
#include <bb/core/bbint.h>
#include <bb/core/expiry.h>

namespace bb {

/// Represents the maturity year/month of an instrument
/// using the de facto standard month codes.
///
/// See:  http://en.wikipedia.org/wiki/Delivery_month
///       http://www.cme.com/trading/res/te/pmcodes.html
///
/// NOTE: A maturity_t has an ambiguous decade.  Converting to
/// an absolute date, for example via maturity_t::expiry, requires the
/// specification of a relative date.
///
/// This class uses a simple algorithm to disambiguate the year:
///    First use the decade of the relative_tv.  If the resulting expiry date is later
///    than the relative_tv, then that expiry is used.  Otherwise, use the following decade.
///
/// For example:
///     assert( maturity_t("H7").to_expiry(date_t(20060201)) == expiry_t(200703) );
///     assert( maturity_t("H7").to_expiry(date_t(20070201)) == expiry_t(200703) );
///     assert( maturity_t("H7").to_expiry(date_t(20070301)) == expiry_t(200703) );
///     assert( maturity_t("H7").to_expiry(date_t(20070401)) == expiry_t(201703) );
///
/// This simple algorithm fails in one particular corner case because the expiration
/// of the underlying instrument is ignored.  For example, if ES expires on the 15th of the month,
/// the following SHOULD hold true, but doesn't with this algorithm:
///     assert( maturity_t("H7").to_expiry(date_t(20070301)) == expiry_t(200703) );
///     assert( maturity_t("H7").to_expiry(date_t(20070331)) == expiry_t(201703) );
///
class maturity_t
{
public:
    static const uint32_t   UNKNOWN_ID = 0;
    static const maturity_t UNKNOWN;

    /// Given a standard month code character, returns its corresponding
    /// numeric month (1-12).  If it is invalid, returns 0.
    static uint32_t monthcode_to_number( char monthcode );

    /// Given a numeric month (1-12), returns its corresponding standard month code.
    /// Returns 0 if it is invalid.
    static char number_to_monthcode( uint32_t mm );

    /// Given a 4-digit numeric year (1000-9999),
    /// returns its corresponding year code (the final digit of that number).
    /// Returns 0 if it is invalid.
    static char number_to_yearcode( uint32_t yyyy );

    /// Returns the string representation used when an invalid maturity_t
    /// is converted to a string.  The default is "**".
    static const std::string& getUnknownStringRep()
    {   return ms_strInvalidRep; }

    /// Sets the string representation used when an invalid maturity_t
    /// is converted to a string.  The default is "**".
    static void setUnknownStringRep( const std::string& str )
    {   ms_strInvalidRep = str; }

    /// Creates a maturity_t from a maturity_t::id.
    /// Returns an invalid maturity_t if the ID is invalid.
    static maturity_t from_id( uint32_t matid );

    /// Creates an invalid maturity_t.
    maturity_t()
    {   set_id( UNKNOWN_ID ); }

    /// Creates a maturity_t from two characters, month and year.
    /// Sets it to maturity_t::UNKNOWN if the characters do not make a valid maturity.
    explicit maturity_t( char m, char y );

    /// Creates a maturity_t from a string.
    /// Sets it to maturity_t::UNKNOWN if the string is malformed.
    ///
    /// Valid formats:
    ///     "YYYYMM"
    ///     "YYYYMMDD"
    ///     "MY" (where M is the month code and Y is the year code)
    explicit maturity_t( const char *s );

    /// Creates a maturity_t from an integer of the form YYYYMM or YYYYMMDD.
    /// This checks the validity of the argument, which you can verify with is_valid.
    /// Note that the absolute year information is lost
    /// and can only be resolved with another relative date.
    /// If it is not valid, it is set as maturity_t::UNKNOWN.
    explicit maturity_t( uint32_t yyyymm )
    {   set_yyyymm( yyyymm ); }

    /// Creates a maturity_t from an expiry_t.
    /// Note that the absolute year information is lost
    /// and can only be resolved with another relative date.
    /// If the expiry_t is invalid, sets it as maturity_t::UNKNOWN.
    explicit maturity_t( expiry_t exp )
    {   set_yyyymm( exp.id() ); }

    /// Returns the numeric id of the expiry_t.
    /// An invalid maturity_t has an id of 0.  This has the convenient property
    /// that you can say "if ( !maturity ) {}"  instead of "if ( !maturity.is_valid() )"
    operator uint32_t() const
    {   return id(); }

    /// Returns the numeric id of the expiry_t.
    /// An invalid maturity_t has an id of 0.
    uint32_t id() const
    {   return ((uint8_t)(m_mat[1]) << 0) + ((uint8_t)(m_mat[0]) << 8); }

    /// Returns the numeric year code of the maturity.  This will be 0-9.
    /// Returns 0 if the maturity is invalid.
    uint32_t year_code() const
    {   return (m_mat[1] == 0) ? 0 : (m_mat[1] - 0x30); }

    /// Returns the character month code of the maturity_t.
    char month_char() const
    {   return m_mat[0]; }

    /// Returns the character year code of the maturity_t.
    char year_char() const
    {   return m_mat[1]; }

    /// Returns the numeric month (1-12) of the month portion of the maturity_t.
    /// Returns 0 if the maturity is invalid.
    uint32_t month() const
    {   return monthcode_to_number( m_mat[0] ); }

    /// Returns true if the maturity_t is valid.
    bool is_valid() const
    {   return (m_mat[0] != 0) && (m_mat[1] != 0); }

    /// Overloaded Comparison Operators
    bool operator ==( const maturity_t mat ) const { return (this->id() == mat.id()); }
    bool operator !=( const maturity_t mat ) const { return !(*this == mat); }

    /// Returns the next maturity_t from the current one.
    /// This is in total disregard to an instrument's maturity series, which may skip months.
    /// Z9 increments to F0.
    /// Incrementing an invalid maturity_t has no effect on its value.
    maturity_t& operator++ ();    // prefix  ++
    maturity_t  operator++ (int); // postfix ++

    /// Returns the previous maturity_t from the current one.
    /// This is in total disregard to an instrument's maturity series, which may skip months.
    /// F0 decrements to Z9.
    /// Incrementing an invalid maturity_t has no effect on its value.
    maturity_t& operator-- ();    // prefix  --
    maturity_t  operator-- (int); // postfix --

    /// Returns the string representation of the maturity_t in the form "MY",
    /// where M is the month code and Y is the year code.
    /// An invalid maturity_t returns "**", but this is configurable with
    /// setUnknownStringRep().
    const char* to_string() const
    {   return is_valid() ? m_mat : getUnknownStringRep().c_str(); }

    /// Given an absolute year and month for a basis, return the corresponding year for this maturity_t.
    /// Returns 0 if the maturity_t is invalid.
    /// See the class documentation for the simple algorithm used.
    uint32_t to_year( uint32_t rel_yyyy, uint32_t rel_mm ) const;

    /// Given an date_t as a basis, return the corresponding year for this maturity_t.
    /// Returns 0 if the maturity_t is invalid.
    /// See the class documentation for the simple algorithm used.
    uint32_t to_year( const date_t& rel_date ) const
    {   return to_year( rel_date.year(), rel_date.month() ); }

    /// Returns the expiry of this maturity, using the given date as a basis.
    /// Returns an invalid expiry if the maturity_t is invalid.
    /// See the class documentation for the simple algorithm used for this conversion.
    expiry_t to_expiry( uint32_t rel_yyyy, uint32_t rel_mm ) const;

    /// Returns the expiry of this maturity, using the given date as a basis.
    /// Returns an invalid expiry if the maturity_t is invalid.
    /// See the class documentation for the simple algorithm used for this conversion.
    expiry_t to_expiry( const date_t& rel_date ) const
    {   return to_expiry( rel_date.year(), rel_date.month() ); }

private:
    /// Sets the id of this maturity_t to be mid.
    void set_id( uint32_t mid )
    {
        m_mat[0] = (char)( (mid >> 8) & 0x000000FF );
        m_mat[1] = (char)( (mid >> 0) & 0x000000FF );
        m_mat[2] = m_mat[3] = 0;
    }

    /// Sets the id based on yyyymm or yyyymmdd.
    /// values outside of [100001,999912] or [10000101, 99991231] are set to UNKNOWN_ID
    void set_yyyymm( uint32_t yyyymm );

    /// Returns true if the maturity_t is valid.
    /// This differs from maturity_t::is_valid, in that this private method performs
    /// more integrity checks.  maturity_t::is_valid assumes all interaction has
    /// occurred through the public interface.
    bool validate() const;

private:
    char                m_mat[4];  // only first two entries are used
    static std::string  ms_strInvalidRep;
};

/// Stream operator to convert maturity_t into human readable form.
std::ostream& operator <<( std::ostream& out, const maturity_t mat );

} // namespace bb

#endif // BB_CORE_MATURITY_H

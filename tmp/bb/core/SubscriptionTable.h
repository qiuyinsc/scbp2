#ifndef BB_CORE_SUBSCRIPTIONTABLE_H
#define BB_CORE_SUBSCRIPTIONTABLE_H

/* Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved. */

#include <boost/foreach.hpp>
#include <boost/function.hpp>
#include <boost/functional/hash.hpp>
#include <bb/core/instrument.h>
#include <bb/core/mtype.h>
#include <bb/core/hash_map.h>
#include <bb/core/Log.h>

namespace bb
{

class SubscriptionTable
{
protected:

public:
    typedef boost::function< void (mtype_t, instrument_t instr) > add_func;
    typedef boost::function< void (mtype_t, instrument_t instr) > remove_func;

    SubscriptionTable( const add_func& add, const remove_func& remove )
        : m_add( add )
        , m_remove( remove )
    {
    }

    ~SubscriptionTable() { clear(); }

    ///
    /// Go through both tables and call the remove callback
    /// for all entries with a non-zero count. Then clear the tables
    ///
    void clear()
    {
        clearTable( m_mtypeSubscriptions );
        clearTable( m_subscriptions );
    }

    ///
    /// Increment the count for the mtype/instrument pair. We keep a local count here and
    /// only call the add callback for "new" subscriptions.
    ///
    void increment( mtype_t mtype, instrument_t instr )
    {
        table_t& table = getTable( instr );

        // Get the current value in the table for the specified mtype/instr
        // and increment it.
        int32_t& val = table[ mtypeinstr_t( mtype, instr ) ];
        ++val;

        // If val == 1 then this is the first subscription, call the add callback
        // otherwise the add CB has been called before. no need to call it again
        if( val == 1 )
        {
            m_add( mtype, instr );
        }
    }

    ///
    /// Decrement the count for the mtype/instrument pair. We keep a local count here and
    /// only call the remove callback when all subscriptions for the pair have been removed
    ///
    void decrement( mtype_t mtype, instrument_t instr )
    {
        table_t&      table = getTable( instr );
        mtypeinstr_t  key( mtype, instr );

        // Get the current value in the table for the specified mtype/sym
        // and decrement it.
        int32_t& val = table[ key ];
        if( val > 0 )
        {
            val -= 1;
            // If there are no more subscriptions for this pair, then
            // call the remove callback
            if( val == 0 )
            {
                m_remove( mtype, instr );
                // remove the element from the table
                table.erase( key );
            }
        }
    }
    ///
    /// Cycle through the hash table and calls the provided add call-back for every
    /// mtype-instr pair that we are currently subscribed to.  This is function
    /// is used when reconnecting to a subscriber
    void resubscribe( const add_func& add_function ){
        resubscribe( m_mtypeSubscriptions, add_function );
        resubscribe( m_subscriptions, add_function );
    }

    std::size_t numOfElements() const {
        return m_mtypeSubscriptions.size() + m_subscriptions.size();
    }
protected:
    ///  Key for hash tables.
    class mtypeinstr_t
    {
    public:
        mtypeinstr_t( mtype_t _mt, instrument_t _instr ) : mtype( _mt ), instr( _instr ) {}
        bool operator == ( const mtypeinstr_t& src ) const
        {
            if( mtype != src.mtype ){
                return false;
            }
            return instrument_t::equals_no_mkt_no_currency()( instr, src.instr );
        }
        mtype_t      mtype;
        instrument_t instr;
    };

    //. custom hash function for the table
    class mtypeinstr_hash
    {
    public:
        size_t operator() ( const mtypeinstr_t& key ) const
        {
            size_t hash = boost::hash_value(key.mtype);
            size_t instrHash = instrument_t::hash_no_mkt_no_currency()(key.instr);
            boost::hash_combine(hash, instrHash );
            return hash;
        }
    };

    /// Custom int32_t field used as the mapped type
    /// that is automatically initialized to 0
    class int32_initialized
    {
    public:
        int32_initialized() : val( 0 ) {}
        operator int32_t& () { return val; }
        operator const int32_t& () const { return val; }
    protected:
        int32_t val;
    };
    typedef bbext::hash_map< mtypeinstr_t, int32_initialized, mtypeinstr_hash > table_t;


private:
    ///
    /// If the count is non-zero call the provided add-callback
    ///
    void resubscribe( table_t& table, const add_func& add_function ){
        BOOST_FOREACH( const table_t::value_type& entry,  table)
        {
            const mtypeinstr_t&        key = entry.first;
            const int32_initialized& count = entry.second;
            if( count != 0 )
            {
                add_function( key.mtype, key.instr );
            }
        }
    }

    table_t& getTable( const instrument_t& instr ){
        // Two tables are kept one for subscriptions based purely on mtype (instr==instrument_t::all())
        // and one for subscriptions including both an mtype and an instrument. Pick the correct
        // table based on the instr passed in
        if( instr == instrument_t::all() )
        {
            return m_mtypeSubscriptions;
        }
        else
        {
            return m_subscriptions;
        }
    }

    void clearTable(table_t& table){
        BOOST_FOREACH( const table_t::value_type& entry,  table)
        {
            const int32_initialized& count = entry.second;
            const mtypeinstr_t&        key = entry.first;
            if( count != 0 )
            {
                m_remove( key.mtype, key.instr );
            }
        }
        table.clear();
    }

    table_t     m_mtypeSubscriptions;
    table_t     m_subscriptions;
    add_func    m_add;
    remove_func m_remove;
};

}

#endif // BB_CORE_SUBSCRIPTIONTABLE_H

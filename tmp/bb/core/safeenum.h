#ifndef BB_CORE_SAFEENUM_H
#define BB_CORE_SAFEENUM_H

/* Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved. */

/*
 * #include <bb/safeenum.h>
 * */
#include <bb/core/relocatablecheck.h>
#include <ostream>
#include <stdexcept>
#include <boost/type_traits.hpp>
#include <bb/core/hash.h>
#include <bb/core/bbassert.h>

namespace bb{
    /*
 * struct SafeEnumPolicy{
 *     enum {};
 *  typedef XXX storage_type;//intergral value
 * typedef XXX enum_type;
 *  storage_type get_default();//const
  * static const enum_type largest_enum_val=//largest enum
 * storage_type narrow(storage_type ){//const if member
 *
 * }
 *
 * };
 *
 * */
namespace example{
struct SafeEnumPolicy{
    typedef unsigned char storage_type;
    enum _EnumVals{
        Larry,Moe,Curly
    };
    typedef _EnumVals enum_type;
    static const _EnumVals largest_enum_val=Curly;
    static storage_type narrow(storage_type _val){

        switch(_val){
            case Larry: case  Moe: case Curly:
                break;
            default:
                throw std::invalid_argument("Value must be either Larry, Moe, or Curly");
        }
        return _val;
    }

    friend std::ostream&operator<<(std::ostream&os,enum_type val){
        switch(val){
        case Moe:
            os<<"Moe";
            break;
        case Larry:
            os<<"Larry";
            break;
        case Curly:
            os<<"Curly";
            break;

        }
        return os;
    }



private:
    BOOST_STATIC_ASSERT(sizeof(storage_type) <=sizeof(unsigned) );
    BOOST_STATIC_ASSERT(unsigned(1<<((sizeof(storage_type)*8)-1 ) ) >=largest_enum_val);

};


}//example
template < class POLICY_TYPE>
class  SafeEnum:public POLICY_TYPE
{
public:
    typedef     POLICY_TYPE policy_type;                                    ///< the policy
    typedef SafeEnum < POLICY_TYPE  > my_type;
    typedef typename policy_type::storage_type storage_type;
    typedef typename policy_type::enum_type enum_type;
private:
    storage_type m_val;

    BOOST_STATIC_ASSERT(boost::is_integral<storage_type>::value==true);
public:

    /// constructor, with a value provided
    /// @param val the new value
    explicit SafeEnum(storage_type val,policy_type const&p=policy_type())
        :policy_type(p),m_val(policy_type::narrow(val)){}
    ///  default constructor
    explicit SafeEnum(policy_type const&p=policy_type()) :policy_type(p),m_val(policy_type::get_default())
    {}
    SafeEnum(enum_type val,policy_type const&p):policy_type(p),m_val(val){

    }
    SafeEnum(enum_type val):m_val(storage_type(val)){

    }
    /// default copy    constructor assignment operator OK
    enum_type get()const{return enum_type(m_val);}

    void reset(storage_type _a){
         m_val=this->narrow(_a);
        }
    void reset(enum_type _a){
         m_val=_a;
    }
    void reset(){
        m_val=this->get_default();
    }
    bool operator==(SafeEnum _r) const
    {   return m_val==_r.m_val;   }
    bool operator!=(SafeEnum _r) const
    {   return m_val!=_r.m_val;   }
    bool operator<(SafeEnum _r) const
    {   return m_val<_r.m_val;   }
    friend bool operator==(enum_type _l,SafeEnum _r){
        return _r==_l;
    }
    friend bool operator!=(enum_type _l,SafeEnum _r){
        return _r!=_l;
    }
    friend bool operator<(enum_type _l,SafeEnum _r){
        return SafeEnum(_l)<_r;
    }
 } ;




/// the stream insertion operator
template<class charT, class Traits, typename POLICY_TYPE>
std::basic_ostream< charT ,Traits > &
operator<<(std::basic_ostream< charT ,Traits >&_m_stream ,
        SafeEnum<POLICY_TYPE> const &val)
{
    _m_stream<<val.get();
    return _m_stream;
}



template<class POLICY_TYPE> struct declare_relocatable<SafeEnum<POLICY_TYPE> >{

    enum{value = 1
        && is_relocatable<POLICY_TYPE>::value
        && is_relocatable<typename SafeEnum< POLICY_TYPE>::storage_type >::value
    };

};


}//bb

BB_HASH_NAMESPACE_BEGIN {
template<class T > struct hash<bb::SafeEnum<T> >
{
    size_t operator()(  bb::SafeEnum<T>  val ) const
    {
        return val.get();
    }
};

} BB_HASH_NAMESPACE_END

#endif // BB_CORE_SAFEENUM_H

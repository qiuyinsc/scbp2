#ifndef BB_CORE_MAXMIN_H
#define BB_CORE_MAXMIN_H

/* Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved. */


#include <boost/optional.hpp>
#include <boost/type_traits/is_fundamental.hpp>

namespace bb {


namespace detail {

/// Maintains the minimum and maximum of a series of value updates.
template <typename T, bool b = true>
class MaxMinBase  // by value, for fundamental types
{
public:
    /// Resets the maximum and minimum values of this MaxMin.
    /// Postcondition: isValid() == false
    void reset() { m_maxmin.reset(); }

    /// Returns the maximum value seen by MaxMin.
    /// If !isValid, the result is undefined and will assert in debug builds.
    T max() const { return m_maxmin->first; }

    /// Returns the minimum value seen by MaxMin.
    /// If !isValid, the result is undefined and will assert in debug builds.
    T min() const { return m_maxmin->second; }

    /// Returns true if this MaxMin has been updated.
    bool isValid() const { return m_maxmin.is_initialized(); }

    /// Updates the maximum and minimum values of this MaxMin with the argument v.
    /// Postcondition: isValid() == true
    void update( const T v )
    {
        if( !m_maxmin )
            m_maxmin = std::make_pair(v,v);
        else
        {
            std::pair<T,T>& p = *m_maxmin;
            if( v > p.first )  p.first  = v;
            if( v < p.second ) p.second = v;
        }
    }

protected:
    // stores the pair of maximum / minimum values
    // the order is (max, min), as denoted by the name
    boost::optional<std::pair<T,T> > m_maxmin;
};


template <typename T>
class MaxMinBase<T, false> // by reference, for non-fundamental types
{
public:
    /// Resets the maximum and minimum values of this MaxMin.
    /// Postcondition: isValid() == false
    void reset() { m_maxmin.reset(); }

    /// Returns the maximum value seen by MaxMin.
    /// If !isValid, the result is undefined and will assert in debug builds.
    const T& max() const { return m_maxmin->first; }

    /// Returns the minimum value seen by MaxMin.
    /// If !isValid, the result is undefined and will assert in debug builds.
    const T& min() const { return m_maxmin->second; }

    /// Returns true if this MaxMin has been updated.
    bool isValid() const { return m_maxmin.is_initialized(); }

    /// Updates the maximum and minimum values of this MaxMin with the argument v.
    /// Postcondition: isValid() == true
    void update( const T& v )
    {
        if( !m_maxmin )
            m_maxmin = std::make_pair(v,v);
        else
        {
            std::pair<T,T>& p = *m_maxmin;
            if( v > p.first )  p.first  = v;
            if( v < p.second ) p.second = v;
        }
    }

protected:
    // stores the pair of maximum / minimum values
    // the order is (max, min), as denoted by the name
    boost::optional<std::pair<T,T> > m_maxmin;
};

} // namespace detail


// chose the right implementation depending on whether T is fundamental or not
template <typename T>
class MaxMin : public detail::MaxMinBase<T, boost::is_fundamental<T>::value >
{};


} // namespace bb

#endif // BB_CORE_MAXMIN_H

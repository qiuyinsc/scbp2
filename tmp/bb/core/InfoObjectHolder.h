#ifndef BB_CORE_INFOOBJECTHOLDER_H
#define BB_CORE_INFOOBJECTHOLDER_H

/* Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved. */


#include <stdint.h>
#include <vector>
#include <boost/shared_ptr.hpp>

namespace bb {

/** Generic container for keeping single instances of objects of each type
 *
 * any type T will have one and only one instance held inside this object.
 *
 * Example usage:
 *
 * class MyClass: public InfoObjectHolder { ... };
 * MyClass mc;
 * assert(!mc.has<int>());
 * mc.get<int>() = 42; // an int with value 42 is now stored in "mc"
 * mc.get<int>() = 7;  // reassigns the single instance of type int in "mc"
 * assert(mc.has<int>());
 * assert(mc.get<int>() == 7);
 */
class InfoObjectHolder
{
public:
    template<typename T> T& get()
    {
        uint32_t type_index = get_type_index<T>();
        if( m_infoObjArray.size() < type_index + 1 )
            m_infoObjArray.resize( type_index + 1 );

        boost::shared_ptr< holder_base >& ptr = m_infoObjArray[type_index];
        if( !ptr )
            ptr.reset( new holder<T>() );

        return static_cast<holder<T>* >( ptr.get() )->val();
    }

    // this returns a pointer rather than a reference so that it can give NULL if the item did not exist
    // this is necessary for const correctness, even if it does make the interface inconsistent vs. non-const
    template<typename T> const T* get() const
    {
        uint32_t type_index = get_type_index<T>();
        if( m_infoObjArray.size() < type_index + 1 )
            return NULL;

        const boost::shared_ptr< const holder_base >& ptr = m_infoObjArray[type_index];
        if( !ptr )
            return NULL;

        return &static_cast<const holder<const T>* >( ptr.get() )->val();
    }

    template<typename T> bool has() const
    {
        uint32_t type_index = get_type_index<T>();
        if( (m_infoObjArray.size() < (type_index + 1)) )
            return false;
        if( !m_infoObjArray[type_index] )
            return false;
        return true;
    }

protected:
    class holder_base 
    {
    public:
        virtual ~holder_base() {}
    };

    template<typename T> class holder : public holder_base
    {
    public:
        T& val() { return m_obj; }
        const T& val() const { return m_obj; }
    protected:
        T m_obj;
    };

    template<typename T> uint32_t get_type_index() const
    {
        static const uint32_t type_index = get_next_index();
        return type_index;
    }

    static uint32_t get_next_index()
    {
        static uint32_t g_map_index_counter = 0;
        return g_map_index_counter++;
    }

protected:
    std::vector< boost::shared_ptr<holder_base> > m_infoObjArray;
};

} // namespace bb

#endif // BB_CORE_INFOOBJECTHOLDER_H

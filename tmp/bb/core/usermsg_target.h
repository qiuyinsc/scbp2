#ifndef BB_CORE_USERMSG_TARGET_H
#define BB_CORE_USERMSG_TARGET_H

/* Contents Copyright 2016 Athena Capital Research LLC. All Rights Reserved. */

/** \file Contains definitions of various enums used in the
BlackBox system.  This file is automatically generated from .build/optimize_trusty/bb/core/usermsg_target.table
with the const-gen.py utility.  DO NOT EDIT THIS FILE DIRECTLY!!
Instead, edit .build/optimize_trusty/bb/core/usermsg_target.table and rerun const-gen.py. */

#include <iosfwd>

namespace bb {
typedef enum {
UMSGT_UNKNOWN = 0,
UMSGT_ISOBOOK = 50,
UMSGT_EQUITY = 300,
UMSGT_MACH = 301,
UMSGT_AUTOFLAT = 302,
UMSGT_BBBO = 303,
UMSGT_ADX = 304,
UMSGT_NEWS = 305,
UMSGT_TORQUE = 306,
UMSGT_FALCON = 307,
UMSGT_CENTURY = 308,
UMSGT_HAL = 309,
UMSGT_ORION = 310,
}usermsg_target_t;

void usermsg_targetPopulateCacheMap();
const char *usermsg_target2str(usermsg_target_t t);
const char *usermsg_target2str(usermsg_target_t t, bool* ok);
bool usermsg_targetIsValid(usermsg_target_t t);
usermsg_target_t str2usermsg_target(const char *s, bool* ok = NULL);
usermsg_target_t str2usermsg_target_throw(const char *s);

// SWIG 1.3.29 doesn't like these in-namespace operators
#ifndef SWIG
std::ostream &operator <<(std::ostream &out, const usermsg_target_t &t);
std::istream &operator >>(std::istream &in, usermsg_target_t &t);
#endif // SWIG

} // namespace bb

#endif // BB_CORE_USERMSG_TARGET_H

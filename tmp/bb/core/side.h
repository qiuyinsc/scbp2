#ifndef BB_CORE_SIDE_H
#define BB_CORE_SIDE_H

/* Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved. */


#include <iosfwd>
#include <limits>

#include <bb/core/bbassert.h>
#include <bb/core/Error.h>
#include <bb/core/fpmath.h>

#include <bb/core/instrument.h>
#include <bb/core/right.h>

namespace bb {

/// side_t indicates which side of the market, Bid or Ask
/// SIDE_BOTH comes last for historical reasons, plus it supports FOR_EACH_SIDE
enum side_t { BID, ASK, SIDE_INVALID, SIDE_BOTH }
// swig does not support packing
#if !defined(SWIG)
__attribute__((__packed__))
#endif
; // for side_t definition

const char *side2str(side_t s);
side_t str2side(const char *s);
side_t str2side_throw(const char *s);

/// loops through each side in order.
#define FOR_EACH_SIDE(s) for(bb::side_t s = bb::BID; s != bb::SIDE_INVALID; s = bb::side_t((int)s+1))

/// Returns the opposite side of the given side (BID => ASK, ASK => BID).
inline side_t opposite_side( const side_t side )
{
    switch( side )
    {
    case BID: return ASK;
    case ASK: return BID;
    default:  return SIDE_INVALID;
    }
}
/// Returns the opposite side of the given side (BID => ASK, ASK => BID).
inline side_t operator~( const side_t side )
{   return opposite_side( side ); }

/// helpers for side comparison
/// The semantics are: conceptually, the best market is the 0 point, so
/// "greater than" means "further from the inside market"
/// bid-side decreases price as it leaves the inside, whereas ask-side increases.






/// Returns true if the given prices are at the same level.
inline bool at(side_t, double d1, double d2, double ep = FP_EPSILON)
{   return EQ(d1, d2, ep); }

/// Returns true if d1 is outside or at d2, following the semantics of the side.
inline bool outside_or_at(side_t side, double d1, double d2, double ep = FP_EPSILON)
{   BB_ASSERT(side == BID || side == ASK); return (side == BID) ? LE(d1, d2, ep) : GE(d1, d2, ep); }

template<side_t side>
inline bool outside_or_at(double d1, double d2, double ep = FP_EPSILON)
{ BOOST_STATIC_ASSERT(side == BID || side == ASK); return (side == BID) ? LE(d1, d2, ep) : GE(d1, d2, ep); }

/// Returns true if d1 is outside d2, following the semantics of the side.
inline bool outside(side_t side, double d1, double d2, double ep = FP_EPSILON)
{   BB_ASSERT(side == BID || side == ASK); return (side == BID) ? LT(d1, d2, ep) : GT(d1, d2, ep); }

template<side_t side>
inline bool outside(double d1, double d2, double ep = FP_EPSILON)
{ BOOST_STATIC_ASSERT(side == BID || side == ASK); return (side == BID) ? LT(d1, d2, ep) : GT(d1, d2, ep); }

/// Returns true if d1 is inside or at d2, following the semantics of the side.
inline bool inside_or_at(side_t side, double d1, double d2, double ep = FP_EPSILON)
{   return !outside(side, d1, d2, ep); }

template<side_t side>
inline bool inside_or_at(double d1, double d2, double ep = FP_EPSILON)
{ return !outside<side>( d1, d2, ep); }

/// Returns true if d1 is inside d2, following the semantics of the side.
inline bool inside(side_t side, double d1, double d2, double ep = FP_EPSILON)
{   return !outside_or_at(side, d1, d2, ep); }

template<side_t side>
inline bool inside(double d1, double d2, double ep = FP_EPSILON)
{ return !outside_or_at<side>( d1, d2, ep); }

inline double outermost_price( side_t side ){ return side == BID ? 0.0 : std::numeric_limits<double>::max(); }
inline double innermost_price( side_t side ){ return side == BID ? std::numeric_limits<double>::max() : 0.0; }

// Book-independent aliases for the functions above:
inline bool is_more_aggressive(side_t side, double d1, double d2, double ep = FP_EPSILON)
{
    return inside(side, d1, d2, ep);
}

template<side_t side>
inline bool is_more_aggressive(double d1, double d2, double ep = FP_EPSILON)
{
    return inside<side>(d1, d2, ep);
}

inline bool is_less_aggressive(side_t side, double d1, double d2, double ep = FP_EPSILON)
{
    return outside(side, d1, d2, ep);
}

template<side_t side>
inline bool is_less_aggressive(double d1, double d2, double ep = FP_EPSILON)
{
    return outside<side>(d1, d2, ep);
}

inline bool is_crossed(double d1, side_t side, double d2, double ep = FP_EPSILON)
{
    return inside(side, d1, d2, ep);
}

template<side_t side>
inline bool is_crossed(double d1, double d2, double ep = FP_EPSILON)
{
    return inside<side>(d1, d2, ep);
}

/// Returns the "sign" of the side (1 for bid, -1 for ask, throws otherwise)
inline int side2sign( const side_t side)
{
    switch( side )
    {
    case BID: return 1;
    case ASK: return -1;
    default:  BB_THROW_ERROR_SS( "invalid side: " << static_cast<int>( side ) );
    }
}

// variant of side2sign which tacks the sign onto a given value.
template<typename T>
inline T side2sign(const side_t side, const T& base)
{
    BOOST_STATIC_ASSERT(std::numeric_limits<T>::is_signed);
    switch( side )
    {
    case BID: return base;
    case ASK: return -base;
    default:  BB_THROW_ERROR_SS( "invalid side: " << static_cast<int>( side ) );
    }
}

// Returns an offset with the correct sign to move a price outside-wards.
// i.e. px+price_offset_outside(BID, 0.05) will be less aggressive than px.
inline double price_offset_outside(side_t s, double v) { return s == ASK ? v : -v; }
/// Returns whichever of (a,b) is innermost.
inline double inner_price(side_t side, double a, double b) { return inside(side, a, b) ? a : b; }
/// Returns whichever of (a,b) is outermost.
inline double outer_price(side_t side, double a, double b) { return outside(side, a, b) ? a : b; }


/// dir_t indicates what type of order.
typedef enum { BUY, SELL, SHORT, DIR_INVALID } dir_t;

const char *dir2str(dir_t d);
dir_t str2dir(const char *s);
dir_t str2dir_throw(const char *s);
inline std::ostream& operator <<( std::ostream& out, dir_t dir ) { return out << dir2str( dir ); }
std::istream& operator >>( std::istream& in, dir_t& dir );

/// Returns the opposite dir of the given dir (BUY => SELL, SELL => BUY, SHORT => BUY).
inline dir_t opposite_dir( const dir_t dir )
{
    switch( dir )
    {
    case BUY:   return SELL;
    case SELL:
    case SHORT: return BUY;
    default:    return DIR_INVALID;
    }
}
/// Returns the opposite dir of the given dir (BUY => SELL, SELL => BUY, SHORT => BUY).
inline dir_t operator~( const dir_t dir ) { return opposite_dir( dir ); }

/// Returns the "sign" of the dir (1 for buy, -1 for sell/short, throws otherwise)
inline int dir2sign( const dir_t dir )
{
    switch( dir )
    {
    case BUY:   return 1;
    case SELL:
    case SHORT: return -1;
    default:    BB_THROW_ERROR_SS( "invalid dir: " << static_cast<int>( dir ) );
    }
}


/// Convert from side_t to dir_t (ignoring short). (BID => BUY, ASK => SELL).
inline dir_t side2dir( const side_t side )
{
    switch( side )
    {
    case BID: return BUY;
    case ASK: return SELL;
    default:  return DIR_INVALID;
    }
}
/// Convert from dir_t to side_t. (BUY => BID, SELL => ASK, SHORT => ASK).
inline side_t dir2side( const dir_t dir )
{
    switch( dir )
    {
    case BUY:   return BID;
    case SELL:
    case SHORT: return ASK;
    default:    return SIDE_INVALID;
    }
}

/// Convert from dir_t to side_t for an option.
/// side_t = BID if dir_t is BUY  and instr.right is RT_CALL
/// side_t = BID if dir_t is SELL and instr.right is RT_PUT
/// side_t = ASK if dir_t is SELL and instr.right is RT_CALL
/// side_t = ASK if dir_t is BUY  and instr.right is RT_PUT
inline side_t dir2sideDelta( const dir_t dir, const bb::instrument_t& instr )
{
    switch( instr.prod )
    {
    case PROD_OPTION:
        switch( instr.right )
        {
        case RT_CALL:
            switch( dir )
            {
            case BUY:  return BID;
            case SELL: return ASK;
            default:   return SIDE_INVALID;
            }
            break;
        case RT_PUT:
            switch( dir )
            {
            case BUY:  return ASK;
            case SELL: return BID;
            default:   return SIDE_INVALID;
            }
            break;
        default:
            return SIDE_INVALID;
            break;
        }
    break;
    default:
        return dir2side( dir );
    }
}

inline int dir2signDelta( const dir_t dir, const bb::instrument_t& instr )
{
    return side2sign( dir2sideDelta( dir, instr ) );
}

} // namespace bb

#endif // BB_CORE_SIDE_H

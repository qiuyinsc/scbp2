#ifndef BB_CORE_SYMBOLDATEIDX_H
#define BB_CORE_SYMBOLDATEIDX_H

/* Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved. */

#include <bb/core/smart_ptr.h>

#include <bb/core/symbol.h>
#include <bb/core/date.h>
#include <ostream>
namespace bb {
/*
 *
 *
 * Index with ordering symid/ymddate
 * Use for daily data of stocks, like corporate actions, daily open/close, etc
 * placed in sorted vector, easy to select range of data for a single symid
 * */

struct SymbolDateIdx{
    SymbolDateIdx(unsigned sym1,unsigned _d)
        :m_symid(sym1),m_ymddate(_d){}
    SymbolDateIdx(bb::symbol_t const& sym1,bb::date_t const& _d)
        :m_symid(sym1.id()),m_ymddate(_d.ymd_date()){}


    bool operator<(SymbolDateIdx const& r)const{
        return  (m_symid<r.m_symid) ||
                ((m_symid==r.m_symid    && m_ymddate<r.m_ymddate)  );
    }
    friend std::ostream& operator<<(std::ostream&os,SymbolDateIdx const& y){
        os<<y.getSymbol()<<"("<< y.m_symid<<")x"<<y.m_ymddate;
        return os;
    }

    bool operator<(bb::symbol_t const&_d)const{
        return m_symid<_d.id();
    }
    friend bool operator<(bb::symbol_t const&_d,SymbolDateIdx const&s){
        return _d.id()<s.m_symid;
    }
    bb::symbol_t getSymbol() const{
        return bb::symbol_t(m_symid);
    }
    bb::date_t getDate() const{
        return bb::date_t(m_ymddate);
    }
    unsigned ymd_date() const{
            return m_ymddate;
    }
    unsigned getSymbolId() const{
        return m_symid;
    }
private:
    unsigned m_symid;
    unsigned m_ymddate;

};


} // bb

#endif // BB_CORE_SYMBOLDATEIDX_H

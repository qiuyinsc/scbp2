#ifndef BB_CORE_GLOB_H
#define BB_CORE_GLOB_H

/* Contents Copyright 2013 Athena Capital Research LLC. All Rights Reserved. */

#include <vector>
#include <string>

namespace bb {
      std::vector<std::string> glob(const std::string& pat);
}

#endif // BB_CORE_GLOB_H

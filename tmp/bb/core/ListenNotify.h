#ifndef BB_CORE_LISTENNOTIFY_H
#define BB_CORE_LISTENNOTIFY_H

/* Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved. */


#include <boost/function.hpp>

#include <bb/core/smart_ptr.h>
#include <bb/core/shared_vector.h>

/****************************************************************************/
/// Generic Listener Framework
///
/// Listener is a boost::function<void(const T&)>.
///
/// Common usage:
///
///  class Foo
///  {
///      BB_DECLARE_LISTENER(Foo);
///
///      /// Adds the given IVolatilityProvider::Listener to this VolatilityProvider.
///      /// Subscription will be initialized with a new subscription to the object.
///      /// When the Subscription is released, the listener is unsubscribed.
///      virtual void subscribeListener( Subscription &outListener, const Listener& listener ) const
///      {  m_notifier.subscribeListener( outListener, listener ); }
///
///      void onStuffHappened()
///      {  m_notifier.notifyListeners( *this ); }
///
///      mutable TNotifier<Foo> m_notifier;
///  };
///
/****************************************************************************/

#define BB_DECLARE_LISTENER( _class_ )                         \
public:                                                        \
    typedef boost::function<void(const _class_&)>  Listener;   \
    typedef bb::Subscription ListenerSubPtr

#define BB_DECLARE_LISTENER_2( _class_, _arg2_ )               \
public:                                                        \
    typedef boost::function<void(const _class_&, _arg2_ )>  Listener;   \
    typedef bb::Subscription ListenerSubPtr

#define BB_DECLARE_LISTENER_2_PREFIX( _prefix_, _class_, _arg2_ )                 \
public:                                                                           \
    typedef boost::function<void(const _class_&, _arg2_ )>  _prefix_##Listener;   \
    typedef bb::Subscription _prefix_##ListenerSubPtr

namespace bb {

/// Notifier class for a unary function (boost::function<void (const T&)>)
template <class T>
class TNotifier
{
public:
    typedef boost::function<void (const T&)> Listener;
    typedef shared_vector<Listener> Listeners_t;
    typedef Subscription ListenerSubPtr;

    /// Invokes (listener)(t) on all listeners.
    void notifyListeners( const T& t ) const
    {
        typename Listeners_t::safe_jterator i(m_listeners);
        while(i.hasNext())
            i.next()(t);
    }

    bool empty() const { return m_listeners.empty(); }
    size_t getNumListeners() const { return m_listeners.size(); }
    void subscribeListener(Subscription &outListener, const Listener &l) const { m_listeners.push_back(outListener, l); }

protected:
    mutable Listeners_t m_listeners;
};

/// Notifier class for a binary function (boost::function<void (const T&, TArg)>)
// TODO: this could totally be scaled up with typelists, right?
template <class T, class TArg>
class TNotifier2
{
public:
    typedef boost::function<void (const T&, TArg)> Listener;
    typedef shared_vector<Listener> Listeners_t;
    typedef Subscription ListenerSubPtr;

    /// Invokes (listener)(t, arg) on all listeners.
    void notifyListeners( const T& t, TArg arg ) const
    {
        typename Listeners_t::safe_jterator i(m_listeners);
        while(i.hasNext())
            i.next()(t, arg);
    }

    bool empty() const { return m_listeners.empty(); }
    size_t getNumListeners() const { return m_listeners.size(); }
    void subscribeListener(Subscription &outListener, const Listener &l) const { m_listeners.push_back(outListener, l); }

protected:
    mutable Listeners_t m_listeners;
};

template<class T>
struct ListenerHelper
{
    typedef ListenerHelper<T> my_type;
    typedef boost::function<void(const T&)>  Listener;
    typedef TNotifier<T> notifier_type;
    typedef typename TNotifier<T>::ListenerSubPtr ListenerSubPtr;
    virtual ~ListenerHelper() {}

    virtual void addListener( ListenerSubPtr &outListener, const Listener &listener ) const = 0;
};

template<class InterfaceType, class ImplType>
struct ListenerHelperImpl : virtual ListenerHelper<InterfaceType>
{
    typedef ListenerHelper<InterfaceType>      base_type;
    typedef typename base_type::Listener       Listener;
    typedef typename base_type::notifier_type  notifier_type;
    typedef typename base_type::ListenerSubPtr ListenerSubPtr;

    void addListener( ListenerSubPtr &outListener, const Listener &listener ) const
    {
        m_notifier.subscribeListener(outListener, listener);
    }

protected:
    void notifyChange() const
    {
        m_notifier.notifyListeners(static_cast<ImplType const&>(*this));
    }

private:
    mutable notifier_type m_notifier;
};

} // namespace bb

#endif // BB_CORE_LISTENNOTIFY_H

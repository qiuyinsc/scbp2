#ifndef BB_CORE_BINARYSEARCHABLEVECTOR_H
#define BB_CORE_BINARYSEARCHABLEVECTOR_H

/* Contents Copyright 2013 Athena Capital Research LLC. All Rights Reserved. */
#include <string>
#include <algorithm>

namespace bb {
// A binary searchable vector provides the binary search interface to the underlying vector
// Unlike an std::map, the vector will be in continuous memory layout which is good for persistence.

// Example: BinarySearchableVector<SimplePersistentVectorAdaptor, symbol_t, int32_t> defines
// a binary-searchable persistent vector which maps symbol_t to int32_t
template<template<class> class VECTOR_T, class KEY, class VALUE>
class BinarySearchableVector
{
public:

    struct data_t
    {
        data_t( const KEY& key_ )
            : key( key_ )
        {}

        bool operator<( const data_t& r ) const
        {
            return key < r.key;
        }

        KEY key;
        VALUE value;
    };

    typedef typename VECTOR_T<data_t>::iterator iterator_type;
    typedef typename VECTOR_T<data_t>::const_iterator const_iterator_type;

    typedef iterator_type iterator;
    typedef const_iterator_type const_iterator;
    typedef data_t value_type;

    // for std::vector
    BinarySearchableVector( std::size_t capacity )
        : m_vector( capacity )
    {}

    // special interface for read_only SimplePersistentVector
    BinarySearchableVector( const std::string& name )
        : m_vector( name )
    {}

    // special interface for read/write SimplePersistentVector
    BinarySearchableVector( const std::string& name, std::size_t capacity )
        : m_vector( name, capacity, VECTOR_T<data_t>::DoNotTruncate )
    {}

    iterator_type findOrAdd( const KEY& key, const VALUE& init_value )
    {
        data_t idx( key );
        iterator_type i = std::lower_bound( m_vector.begin(), m_vector.end(), idx );

        if( i == m_vector.end() || idx < *i )
        {
            idx.value = init_value;

            i = m_vector.insert( i, idx );
        }

        return i;
    }

    iterator_type find( const KEY& key )
    {
        data_t idx( key );
        iterator_type i = std::lower_bound( m_vector.begin(), m_vector.end(), idx );

        if( i == m_vector.end() || idx < *i )
        {
            return m_vector.end();
        }

        return i;
    }

    const_iterator_type begin() const { return m_vector.begin(); }
    const_iterator_type end() const { return m_vector.end(); }

    iterator_type begin() { return m_vector.begin(); }
    iterator_type end() { return m_vector.end(); }

private:

    VECTOR_T<data_t> m_vector;
};

}

#endif // BB_CORE_BINARYSEARCHABLEVECTOR_H

#ifndef BB_CORE_MESSAGES_AUTOGEN_FWD_H
#define BB_CORE_MESSAGES_AUTOGEN_FWD_H

// DO NOT DIRECTLY EDIT THIS FILE!
// It was automagically generated by message-gen-ch.rb from .build/optimize_trusty/bb/core/messages.xml
// Edit .build/optimize_trusty/bb/core/messages.xml instead and re-run message-gen-ch.rb.            

/* Contents Copyright 2016 Athena Capital Research LLC. All Rights Reserved. */

namespace bb {

struct sx_l1_trade_info;
struct sx_l1_consolidated_summary;
struct sx_l1_quote_info;
struct sx_l1_appendage_info;
struct dj_enf_merger_party_info;
struct dj_enf_rating_entity_info;
struct dj_enf_code;
struct dj_enf_cusip;
struct toc_market_level;
struct orc_instrument_id;
struct orc_order_depth_level;
struct opra_best_appendage;
struct opra_index_group;
struct bb_l1_appendage_side;
struct bb_l1_trade_info;
struct bb_l1_consolidated_info;
struct bb_l1_participant_info;
struct tocom_instrument_id;
struct hkfe_instrument_id;
struct order_depth_level;
struct wind_order_depth_level;
struct wind_book_depth_level;
struct match_data;
struct dce_l2_order_depth_level;
struct gta_stock_book_level;
struct twse_quote_data;
struct fos_order_book_level;
struct cqg_trade_session;
struct msg_timing;
struct snapshot_level;
struct shared_memory_book_header;
struct sse_historical_depth_level;

class msg_qd_heartbeat_old;
class msg_text_alert_old;
class msg_wrapper;
class msg_offset;
class msg_nqds_quote;
class msg_nqds_control;
class msg_utdf_old_tick_notime;
class msg_utdf_old_correction;
class msg_utdf_old_cancel;
class msg_utdf_old_action;
class msg_utdf_old_summary;
class msg_utdf_old_volume;
class msg_utdf_old_control;
class msg_utdf_old_tick;
class msg_hist_sub;
class msg_hist_control;
class msg_isld_add_old;
class msg_isld_exe_old;
class msg_isld_cxl_old;
class msg_isld_rpl_old;
class msg_isld_hex_old;
class msg_isld_noii_old;
class msg_isld_cross_trade_old;
class msg_yahoo_tick;
class msg_yahoo_quote;
class msg_cqs_old_control;
class msg_cqs_old_quote_noexch;
class msg_cqs_old_quote_notime;
class msg_cqs_old_quote;
class msg_td_status_change_old;
class msg_td_status_order;
class msg_margin_approve;
class msg_margin_getinfo;
class msg_margin_getorders;
class msg_margin_sendorders;
class msg_margin_remove_order;
class msg_mng_set_var;
class msg_mng_request_cancel;
class msg_mng_clear_order;
class msg_arca_add_old;
class msg_arca_mod_old;
class msg_arca_del_old;
class msg_arca_imbalance_old;
class msg_arca_sys_event_v1_old;
class msg_arca_sys_event_old;
class msg_brut_system;
class msg_brut_add;
class msg_brut_exe;
class msg_brut_cxl;
class msg_brut_text_alert;
class msg_bookserver_subscribe;
class msg_bookserver_subscribe_complete;
class msg_bookserver_unsubscribe;
class msg_bookserver_unsubscribe_complete;
class msg_bookserver_refresh;
class msg_bookserver_refresh_complete;
class msg_bookserver_book_entry;
class msg_bookserver_order_entry;
class msg_bookserver_control;
class msg_tt_add;
class msg_tt_mod;
class msg_tt_del;
class msg_tt_null;
class msg_tt_tick;
class msg_ib_add;
class msg_ib_mod;
class msg_ib_del;
class msg_ib_tick;
class msg_ib_quote;
class msg_tv_base;
class msg_tv_quote;
class msg_tv_quote_old2;
class msg_tv_quote_old;
class msg_tv_action;
class msg_liffe_book_cmd;
class msg_liffe_book_mod;
class msg_liffe_quote;
class msg_liffe_quote_implied;
class msg_liffe_tick;
class msg_liffe_settlement;
class msg_liffe_message;
class msg_liffe_exchange_mode;
class msg_liffe_instrument_mode;
class msg_liffe_instrument_status;
class msg_cme_instrument_status_old;
class msg_cme_instrument_creation_old2;
class msg_cme_market_order;
class msg_cme_theoretical_price;
class msg_cme_instrument_ref_broadcast;
class msg_cme_instrument_creation_old;
class msg_rqd_reload;
class msg_rqd_stop;
class msg_rqd_pulse;
class msg_nyse_alerts_imbalance_old;
class msg_nyse_halt_delay_old;
class msg_nyse_indication_old;
class msg_nyse_trade_dissemination_time_old;
class msg_nyse_trading_collar_old;
class msg_nyse_circuit_breaker_old;
class msg_stat_cast_old;
class msg_timestamp;
class msg_portfolio_effective_position_update;
class msg_portfolio_set_trader_mode;
class msg_qd_admin_filter_instrument;
class msg_qd_admin_filter_mtype;
class msg_feed_stats_old;
class msg_sx_l1_base;
class msg_sx_l1_trade_base;
class msg_sx_l1_trade;
class msg_sx_l1_trade_correction;
class msg_sx_l1_trade_cancel;
class msg_sx_l1_trade_prior_day;
class msg_sx_l1_quote_base;
class msg_sx_l1_quote;
class msg_sx_l1_nat_bbo_appendage;
class msg_sx_l1_nasd_bbo_appendage;
class msg_sx_l1_nasd_adf_appendage;
class msg_sx_l1_bbo_quote;
class msg_sx_l1_seqnum_reset;
class msg_sx_l1_preferred_line;
class msg_dj_enf_base;
class msg_dj_enf_econ_ind_num;
class msg_dj_enf_econ_ind_hl;
class msg_dj_enf_comp_base;
class msg_dj_enf_earnings_base;
class msg_dj_enf_earnings_num;
class msg_dj_enf_earnings_hl;
class msg_dj_enf_guidance_base;
class msg_dj_enf_guidance_num;
class msg_dj_enf_guidance_hl;
class msg_dj_enf_merger;
class msg_dj_enf_analyst_ratings;
class msg_dj_enf_common_flag_base;
class msg_dj_enf_bankruptcy;
class msg_dj_enf_exec_change;
class msg_dj_enf_restatement;
class msg_dj_enf_stock_split;
class msg_dj_enf_codes;
class msg_dj_enf_debt_event_base;
class msg_dj_enf_cusips;
class msg_dj_enf_debt_rating;
class msg_dj_enf_treasury_announce;
class msg_dj_enf_treasury_non_competitive;
class msg_dj_enf_treasury_results;
class msg_dj_enf_commodities_base;
class msg_dj_enf_commodities;
class msg_dj_cdf_numeric;
class msg_ntkn_base;
class msg_ntkn_numeric;
class msg_tsx_tl2_base;
class msg_tsx_tl2_symbol_status;
class msg_tsx_tl2_order_book;
class msg_tsx_tl2_order_update;
class msg_tsx_tl2_trade_report;
class msg_tsx_tl2_stock_status;
class msg_tsx_tl2_market_state_update;
class msg_tsx_tl2_mbx;
class msg_tsx_tl2_moc_imbalance;
class msg_tsx_tl1_base;
class msg_tsx_tl1_trade;
class msg_tsx_tl1_moc_imbalance;
class msg_tsx_tl1_moc_price_movement_delay;
class msg_tsx_tl1_quote;
class msg_tsx_tl1_trade_cancel;
class msg_tsx_tl1_official_index_level;
class msg_tsx_tl1_interim_index_level;
class msg_tsx_tl1_bulletin;
class msg_tsx_tl1_summary;
class msg_tsx_tl1_dividend;
class msg_tsx_tl1_high_low;
class msg_tsx_tl1_volume_value_transactions;
class msg_toc_market_picture;
class msg_test;
class msg_test2;
class msg_test3;
class msg_test4;
class msg_connect;
class msg_client_identify;
class msg_timeout;
class msg_qd_heartbeat;
class msg_client_accept;
class msg_isld_itch_base;
class msg_isld_display;
class msg_isld_system;
class msg_isld_trdbreak;
class msg_isld_stock_dir;
class msg_isld_stock_trading_action;
class msg_isld_market_participant_position;
class msg_isld_base;
class msg_isld_add;
class msg_isld_add_with_mpid;
class msg_isld_exe;
class msg_isld_cxl;
class msg_isld_rpl;
class msg_isld_hex;
class msg_isld_noii;
class msg_isld_cross_trade;
class msg_isld_ipo_quoting_period_update;
class msg_isld_reg_sho_restriction;
class msg_isld_mwcb_decline_level;
class msg_isld_mwcb_status;
class msg_client_disconnect;
class msg_td_order_base;
class msg_td_order;
class msg_td_order_future;
class msg_td_order_new;
class msg_td_order_modify;
class msg_td_order_reserve;
class msg_td_cancel;
class msg_td_status_change_base;
class msg_td_status_change;
class msg_td_modify_status_change;
class msg_td_cancel_reject;
class msg_td_fill_base;
class msg_td_fill_no_counterparty;
class msg_td_fill;
class msg_td_fill_future;
class msg_td_clearing_config;
class msg_td_fill_new;
class msg_td_reprice;
class msg_td_route;
class msg_td_client_config;
class msg_hkfe_trade_info;
class msg_hkfe_series_definition;
class msg_hkfe_quote;
class msg_hkfe_trade_stat;
class msg_hkfe_summary_stat;
class msg_hkfe_calculated_opening_price;
class msg_hkfe_market_commodity_status;
class msg_hkfe_underlying;
class msg_hkfe_control_alert;
class msg_margin_create_fill;
class msg_margin_subscription_base;
class msg_margin_subscribe;
class msg_margin_unsubscribe;
class msg_margin_limits;
class msg_margin_instr_limits;
class msg_margin_account_info;
class msg_option_greeks_limits;
class msg_option_greeks_account_info;
class msg_margin_set_baseline;
class msg_margin_instrument_info;
class msg_margin_aggregate_pos;
class msg_margin_set_pos;
class msg_margin_set_tick;
class msg_margin_broadcast_request;
class msg_user_message;
class msg_bb_control;
class msg_text_alert;
class msg_sasl;
class msg_sasl_error;
class msg_acrindex_info;
class msg_cme_flush_order;
class msg_cme_instrument_price;
class msg_cme_spi_message;
class msg_cme_trade;
class msg_cme_depth;
class msg_cme_depth_implied;
class msg_cme_best_price;
class msg_cme_instrument_group_status;
class msg_cme_instrument_definition;
class msg_cme_instrument_status;
class msg_cme_fast_depth;
class msg_bats_add;
class msg_bats_exe;
class msg_bats_cxl;
class msg_bats_hex;
class msg_bats_trdbreak;
class msg_bats_mod;
class msg_bats_trading_status;
class msg_bats_latency_stat;
class msg_bats_auction_update;
class msg_bats_auction_summary;
class msg_bats_retail_price_improvement;
class msg_edgx_add;
class msg_edgx_exe;
class msg_edgx_cxl;
class msg_edgx_hex;
class msg_edgx_trdbreak;
class msg_edgx_mod;
class msg_edgx_trading_status;
class msg_edgx_latency_stat;
class msg_edgx_auction_update;
class msg_edgx_auction_summary;
class msg_edgx_retail_price_improvement;
class msg_signal_update_common;
class msg_signal_update_16;
class msg_sig_server_heartbeat;
class msg_signal_update_8;
class msg_signal_update_4;
class msg_book_update_1;
class msg_dependent_variable_update_16;
class msg_nyse_openbook_update;
class msg_nyse_quote;
class msg_nyse_trade_base;
class msg_nyse_trade;
class msg_nyse_trade_cancel;
class msg_nyse_trade_correction;
class msg_nyse_imbalance;
class msg_nyse_lrp;
class msg_cts_trade;
class msg_tick_statistics;
class msg_tick_statistics_request;
class msg_qd_subscribe;
class msg_qd_unsubscribe;
class msg_nyse_openbook_ultra_add;
class msg_nyse_openbook_ultra_exe;
class msg_nyse_openbook_ultra_rem;
class msg_nyse_openbook_ultra_update;
class msg_feed_stats;
class msg_perf_qd_latency;
class msg_orc_base;
class msg_orc_order_depth;
class msg_orc_price_feed;
class msg_book_snapshot;
class msg_shfe_qd;
class msg_shfe_info_order;
class msg_shfe_info_trade;
class msg_td_position_offset;
class msg_td_position_offset_frame;
class msg_td_trade_limits;
class msg_td_position_offset_request;
class msg_shfe_depth_update;
class msg_shfe_depth_heartbeat;
class msg_shfe_depth_clear;
class msg_mq_connect;
class msg_mq_accept;
class msg_mq_disconnect;
class msg_acr_ping_connect_tcp;
class msg_acr_ping;
class msg_acr_pong;
class msg_acr_ping_noop;
class msg_acr_ping_identify;
class msg_acr_ping_connect_mq;
class msg_acr_ping_connect_udp;
class msg_acr_ping_connect_unix;
class msg_opra_header;
class msg_opra_trade;
class msg_opra_open_interest;
class msg_opra_eod_summary;
class msg_opra_quote;
class msg_opra_admin;
class msg_opra_control;
class msg_opra_underlying;
class msg_mmap_connect;
class msg_mmap_response;
class msg_mmap_connect2;
class msg_mmap_response2;
class msg_bb_l1_base;
class msg_bb_l1_quote;
class msg_bb_l1_bbo_quote;
class msg_bb_l1_nat_bbo_appendage;
class msg_bb_l1_finra_bbo_appendage;
class msg_bb_l1_finra_adf_appendage;
class msg_bb_l1_trade_base;
class msg_bb_l1_trade;
class msg_bb_l1_trade_correction;
class msg_bb_l1_trade_cancel;
class msg_bb_l1_trade_prior_day;
class msg_bb_l1_trade_prior_day_correction;
class msg_bb_l1_trade_prior_day_cancel;
class msg_bb_l1_trading_status;
class msg_bb_l1_index;
class msg_bb_l1_bid_offer_index;
class msg_bb_l1_cons_eod_summary;
class msg_bb_l1_part_eod_summary;
class msg_bb_l1_admin_unformatted;
class msg_bb_l1_closing_trade;
class msg_bb_l1_most_active;
class msg_bb_l1_closing_bid_offer;
class msg_bb_l1_market_volume;
class msg_bb_l1_market_trades_dollar_value;
class msg_bb_l1_crossing_session;
class msg_bb_l1_trading_action;
class msg_bb_l1_closing_nbbo;
class msg_bb_l1_closing_recap;
class msg_bb_l1_issue_sym_directory;
class msg_bb_l1_reg_sho_price_test;
class msg_spin_server_request;
class msg_spin_server_stream_end;
class msg_spin_server_subscribe;
class msg_nyse_alerts_base;
class msg_nyse_alerts_security_info;
class msg_nyse_alerts_imbalance;
class msg_nyse_alerts_delay_halts;
class msg_nyse_alerts_indication;
class msg_nyse_alerts_trade_dissemination_time;
class msg_nyse_alerts_circuit_breaker;
class msg_nyse_alerts_short_sale_restriction;
class msg_qbbo_base;
class msg_qbbo_sys_event;
class msg_qbbo_stock_dir;
class msg_qbbo_stock_trading_action;
class msg_qbbo_quote;
class msg_qbbo_reg_sho_restriction;
class msg_simulator_msg_base;
class msg_simulator_market_request;
class msg_simulator_market_action;
class msg_simulator_market_response;
class msg_ice_product_definition_base;
class msg_ice_simple_product_definition;
class msg_ice_product_definition;
class msg_ice_realtime_base;
class msg_ice_trade_base;
class msg_ice_trade;
class msg_ice_cancelled_trade;
class msg_ice_market_statistics;
class msg_ice_add_modify_order;
class msg_ice_delete_order;
class msg_ice_market_state_change;
class msg_ice_open_price;
class msg_ice_price_level_base;
class msg_ice_price_level;
class msg_ice_add_price_level;
class msg_ice_change_price_level;
class msg_ice_delete_price_level;
class msg_time_bucket;
class msg_basic_candlestick;
class msg_bbo_candlestick;
class msg_tocom_base;
class msg_tocom_trade;
class msg_tocom_modify_price_depth;
class msg_tocom_price_depth_delete_levels;
class msg_hkfe_base;
class msg_hkfe_trade;
class msg_hkfe_modify_price_depth;
class msg_remote_hedger_identify;
class msg_remote_hedger_rsp_reject;
class msg_remote_hedger_add_bait_fill;
class msg_remote_hedger_add_beta;
class msg_remote_hedger_rsp_hedge_fill;
class msg_protobuf;
class msg_cffex_qd;
class msg_cffex_info_order;
class msg_cffex_info_trade;
class msg_wind_order_depth;
class msg_wind_trade;
class msg_wind_index_data;
class msg_wind_futures_market_data;
class msg_wind_stock_market_data;
class msg_wind_stock_transaction;
class msg_krx_quote;
class msg_krx_trade;
class msg_taifex_trade;
class msg_taifex_book;
class msg_taifex_settlement;
class msg_taifex_final_settlement;
class msg_wind_szse_order_data;
class msg_wind_szse_order_queue_data;
class msg_hkse_trade;
class msg_hkse_new_order;
class msg_hkse_modify_order;
class msg_hkse_iep;
class msg_hkse_delete_order;
class msg_hkse_add_odd_lot_order;
class msg_hkse_delete_odd_lot_order;
class msg_hkse_trade_cancel;
class msg_dce_l2_best_and_deep;
class msg_dce_l2_ten_entrust;
class msg_dce_l2_stats;
class msg_dce_l2_realtime_price;
class msg_dce_l2_match_price_qty;
class msg_gta_stock_snapshot;
class msg_gta_stock_order_queue;
class msg_gta_stock_order_ranking;
class msg_gta_index_snapshot;
class msg_gta_stock_transaction;
class msg_gta_order;
class msg_twse_oddlot_trade;
class msg_twse_book_snapshot;
class msg_twse_index;
class msg_twse_etf;
class msg_td_cancel_by_side;
class msg_tunnel_stats_base;
class msg_tunnel_send_stats;
class msg_tunnel_recv_stats;
class msg_cme_recovery_book_base;
class msg_cme_recovery_book_snapshot;
class msg_cme_recovery_book_fast_depth;
class msg_cme_recovery_book_trade;
class msg_cme_recovery_book_volume;
class msg_cme_recovery_book_daily_stat;
class msg_cme_recovery_book_session_stat;
class msg_cme_recovery_book_limits;
class msg_cme_recovery_book_security_status;
class msg_quant_feed_base;
class msg_quant_feed_l1_snapshot;
class msg_quant_feed_trade;
class msg_quant_feed_l1;
class msg_quant_feed_index;
class msg_quant_feed_mbl_full_refresh;
class msg_quant_feed_mbl_delta_refresh;
class msg_quant_feed_mbl_overlap_refresh;
class msg_quant_feed_mbl_max_visible_depth;
class msg_market_microstructure;
class msg_czce_l1_qd;
class msg_czce_l2_qd;
class msg_cme_recovery_book_trade_detail;
class msg_cme_recovery_book_trade_detail_array;
class msg_arca_base;
class msg_arca_add;
class msg_arca_mod;
class msg_arca_del;
class msg_arca_imbalance;
class msg_arca_sym_update;
class msg_arca_execute;
class msg_arca_add_order_refresh;
class msg_arca_trade;
class msg_arca_trade_cancel;
class msg_arca_trade_correction;
class msg_arca_symbol_clear;
class msg_arca_session_change;
class msg_arca_security_status;
class msg_arca_time_ref;
class msg_shfe_depth_refresh_complete;
class msg_arca_stock_summary;
class msg_mcast_test_request;
class msg_mcast_test_reply;
class msg_cqg_security_definition;
class msg_cqg_base;
class msg_cqg_book_update;
class msg_cqg_trade;
class msg_cqg_statistics;
class msg_cqg_flush_book;
class msg_cqg_snapshot;
class msg_shse_index_data;
class msg_shse_vauction_price;
class msg_shfe_depth_activity_update;
class msg_acr_book;
class msg_acr_trade;
class msg_on_idle;
class msg_stat_collector;
class msg_lme_historical_order;
class msg_lme_historical_trade;
class msg_shared_memory_book_10;
class msg_locate_return;
class msg_locate_request;
class msg_locate_request_response;
class msg_locate_return_response;
class msg_dessert_strategy;
class msg_sse_historical_day_min;
class msg_sse_historical_snap1;
class msg_sse_historical_tick;
class msg_sse_historical_auction;
class msg_sse_historical_snap2_part1;
class msg_sse_historical_snap2_part2;
class msg_sse_historical_snap_option;

class QdHeartbeatOldMsg;
class TextAlertOldMsg;
class WrapperMsg;
class OffsetMsg;
class NqdsQuoteMsg;
class NqdsControlMsg;
class UtdfOldTickNotimeMsg;
class UtdfOldCorrectionMsg;
class UtdfOldCancelMsg;
class UtdfOldActionMsg;
class UtdfOldSummaryMsg;
class UtdfOldVolumeMsg;
class UtdfOldControlMsg;
class UtdfOldTickMsg;
class HistSubMsg;
class HistControlMsg;
class IsldAddOldMsg;
class IsldExeOldMsg;
class IsldCxlOldMsg;
class IsldRplOldMsg;
class IsldHexOldMsg;
class IsldNoiiOldMsg;
class IsldCrossTradeOldMsg;
class YahooTickMsg;
class YahooQuoteMsg;
class CqsOldControlMsg;
class CqsOldQuoteNoexchMsg;
class CqsOldQuoteNotimeMsg;
class CqsOldQuoteMsg;
class TdStatusChangeOldMsg;
class TdStatusOrderMsg;
class MarginApproveMsg;
class MarginGetinfoMsg;
class MarginGetordersMsg;
class MarginSendordersMsg;
class MarginRemoveOrderMsg;
class MngSetVarMsg;
class MngRequestCancelMsg;
class MngClearOrderMsg;
class ArcaAddOldMsg;
class ArcaModOldMsg;
class ArcaDelOldMsg;
class ArcaImbalanceOldMsg;
class ArcaSysEventV1OldMsg;
class ArcaSysEventOldMsg;
class BrutSystemMsg;
class BrutAddMsg;
class BrutExeMsg;
class BrutCxlMsg;
class BrutTextAlertMsg;
class BookserverSubscribeMsg;
class BookserverSubscribeCompleteMsg;
class BookserverUnsubscribeMsg;
class BookserverUnsubscribeCompleteMsg;
class BookserverRefreshMsg;
class BookserverRefreshCompleteMsg;
class BookserverBookEntryMsg;
class BookserverOrderEntryMsg;
class BookserverControlMsg;
class TtAddMsg;
class TtModMsg;
class TtDelMsg;
class TtNullMsg;
class TtTickMsg;
class IbAddMsg;
class IbModMsg;
class IbDelMsg;
class IbTickMsg;
class IbQuoteMsg;
class TvBaseMsg;
class TvQuoteMsg;
class TvQuoteOld2Msg;
class TvQuoteOldMsg;
class TvActionMsg;
class LiffeBookCmdMsg;
class LiffeBookModMsg;
class LiffeQuoteMsg;
class LiffeQuoteImpliedMsg;
class LiffeTickMsg;
class LiffeSettlementMsg;
class LiffeMessageMsg;
class LiffeExchangeModeMsg;
class LiffeInstrumentModeMsg;
class LiffeInstrumentStatusMsg;
class CmeInstrumentStatusOldMsg;
class CmeInstrumentCreationOld2Msg;
class CmeMarketOrderMsg;
class CmeTheoreticalPriceMsg;
class CmeInstrumentRefBroadcastMsg;
class CmeInstrumentCreationOldMsg;
class RqdReloadMsg;
class RqdStopMsg;
class RqdPulseMsg;
class NyseAlertsImbalanceOldMsg;
class NyseHaltDelayOldMsg;
class NyseIndicationOldMsg;
class NyseTradeDisseminationTimeOldMsg;
class NyseTradingCollarOldMsg;
class NyseCircuitBreakerOldMsg;
class StatCastOldMsg;
class TimestampMsg;
class PortfolioEffectivePositionUpdateMsg;
class PortfolioSetTraderModeMsg;
class QdAdminFilterInstrumentMsg;
class QdAdminFilterMtypeMsg;
class FeedStatsOldMsg;
class SxL1BaseMsg;
class SxL1TradeBaseMsg;
class SxL1TradeMsg;
class SxL1TradeCorrectionMsg;
class SxL1TradeCancelMsg;
class SxL1TradePriorDayMsg;
class SxL1QuoteBaseMsg;
class SxL1QuoteMsg;
class SxL1NatBboAppendageMsg;
class SxL1NasdBboAppendageMsg;
class SxL1NasdAdfAppendageMsg;
class SxL1BboQuoteMsg;
class SxL1SeqnumResetMsg;
class SxL1PreferredLineMsg;
class DjEnfBaseMsg;
class DjEnfEconIndNumMsg;
class DjEnfEconIndHlMsg;
class DjEnfCompBaseMsg;
class DjEnfEarningsBaseMsg;
class DjEnfEarningsNumMsg;
class DjEnfEarningsHlMsg;
class DjEnfGuidanceBaseMsg;
class DjEnfGuidanceNumMsg;
class DjEnfGuidanceHlMsg;
class DjEnfMergerMsg;
class DjEnfAnalystRatingsMsg;
class DjEnfCommonFlagBaseMsg;
class DjEnfBankruptcyMsg;
class DjEnfExecChangeMsg;
class DjEnfRestatementMsg;
class DjEnfStockSplitMsg;
class DjEnfCodesMsg;
class DjEnfDebtEventBaseMsg;
class DjEnfCusipsMsg;
class DjEnfDebtRatingMsg;
class DjEnfTreasuryAnnounceMsg;
class DjEnfTreasuryNonCompetitiveMsg;
class DjEnfTreasuryResultsMsg;
class DjEnfCommoditiesBaseMsg;
class DjEnfCommoditiesMsg;
class DjCdfNumericMsg;
class NtknBaseMsg;
class NtknNumericMsg;
class TsxTl2BaseMsg;
class TsxTl2SymbolStatusMsg;
class TsxTl2OrderBookMsg;
class TsxTl2OrderUpdateMsg;
class TsxTl2TradeReportMsg;
class TsxTl2StockStatusMsg;
class TsxTl2MarketStateUpdateMsg;
class TsxTl2MbxMsg;
class TsxTl2MocImbalanceMsg;
class TsxTl1BaseMsg;
class TsxTl1TradeMsg;
class TsxTl1MocImbalanceMsg;
class TsxTl1MocPriceMovementDelayMsg;
class TsxTl1QuoteMsg;
class TsxTl1TradeCancelMsg;
class TsxTl1OfficialIndexLevelMsg;
class TsxTl1InterimIndexLevelMsg;
class TsxTl1BulletinMsg;
class TsxTl1SummaryMsg;
class TsxTl1DividendMsg;
class TsxTl1HighLowMsg;
class TsxTl1VolumeValueTransactionsMsg;
class TocMarketPictureMsg;
class TestMsg;
class Test2Msg;
class Test3Msg;
class Test4Msg;
class ConnectMsg;
class ClientIdentifyMsg;
class TimeoutMsg;
class QdHeartbeatMsg;
class ClientAcceptMsg;
class IsldItchBaseMsg;
class IsldDisplayMsg;
class IsldSystemMsg;
class IsldTrdbreakMsg;
class IsldStockDirMsg;
class IsldStockTradingActionMsg;
class IsldMarketParticipantPositionMsg;
class IsldBaseMsg;
class IsldAddMsg;
class IsldAddWithMpidMsg;
class IsldExeMsg;
class IsldCxlMsg;
class IsldRplMsg;
class IsldHexMsg;
class IsldNoiiMsg;
class IsldCrossTradeMsg;
class IsldIpoQuotingPeriodUpdateMsg;
class IsldRegShoRestrictionMsg;
class IsldMwcbDeclineLevelMsg;
class IsldMwcbStatusMsg;
class ClientDisconnectMsg;
class TdOrderBaseMsg;
class TdOrderMsg;
class TdOrderFutureMsg;
class TdOrderNewMsg;
class TdOrderModifyMsg;
class TdOrderReserveMsg;
class TdCancelMsg;
class TdStatusChangeBaseMsg;
class TdStatusChangeMsg;
class TdModifyStatusChangeMsg;
class TdCancelRejectMsg;
class TdFillBaseMsg;
class TdFillNoCounterpartyMsg;
class TdFillMsg;
class TdFillFutureMsg;
class TdClearingConfigMsg;
class TdFillNewMsg;
class TdRepriceMsg;
class TdRouteMsg;
class TdClientConfigMsg;
class HkfeTradeInfoMsg;
class HkfeSeriesDefinitionMsg;
class HkfeQuoteMsg;
class HkfeTradeStatMsg;
class HkfeSummaryStatMsg;
class HkfeCalculatedOpeningPriceMsg;
class HkfeMarketCommodityStatusMsg;
class HkfeUnderlyingMsg;
class HkfeControlAlertMsg;
class MarginCreateFillMsg;
class MarginSubscriptionBaseMsg;
class MarginSubscribeMsg;
class MarginUnsubscribeMsg;
class MarginLimitsMsg;
class MarginInstrLimitsMsg;
class MarginAccountInfoMsg;
class OptionGreeksLimitsMsg;
class OptionGreeksAccountInfoMsg;
class MarginSetBaselineMsg;
class MarginInstrumentInfoMsg;
class MarginAggregatePosMsg;
class MarginSetPosMsg;
class MarginSetTickMsg;
class MarginBroadcastRequestMsg;
class UserMessageMsg;
class BbControlMsg;
class TextAlertMsg;
class SaslMsg;
class SaslErrorMsg;
class AcrindexInfoMsg;
class CmeFlushOrderMsg;
class CmeInstrumentPriceMsg;
class CmeSpiMessageMsg;
class CmeTradeMsg;
class CmeDepthMsg;
class CmeDepthImpliedMsg;
class CmeBestPriceMsg;
class CmeInstrumentGroupStatusMsg;
class CmeInstrumentDefinitionMsg;
class CmeInstrumentStatusMsg;
class CmeFastDepthMsg;
class BatsAddMsg;
class BatsExeMsg;
class BatsCxlMsg;
class BatsHexMsg;
class BatsTrdbreakMsg;
class BatsModMsg;
class BatsTradingStatusMsg;
class BatsLatencyStatMsg;
class BatsAuctionUpdateMsg;
class BatsAuctionSummaryMsg;
class BatsRetailPriceImprovementMsg;
class EdgxAddMsg;
class EdgxExeMsg;
class EdgxCxlMsg;
class EdgxHexMsg;
class EdgxTrdbreakMsg;
class EdgxModMsg;
class EdgxTradingStatusMsg;
class EdgxLatencyStatMsg;
class EdgxAuctionUpdateMsg;
class EdgxAuctionSummaryMsg;
class EdgxRetailPriceImprovementMsg;
class SignalUpdateCommonMsg;
class SignalUpdate16Msg;
class SigServerHeartbeatMsg;
class SignalUpdate8Msg;
class SignalUpdate4Msg;
class BookUpdate1Msg;
class DependentVariableUpdate16Msg;
class NyseOpenbookUpdateMsg;
class NyseQuoteMsg;
class NyseTradeBaseMsg;
class NyseTradeMsg;
class NyseTradeCancelMsg;
class NyseTradeCorrectionMsg;
class NyseImbalanceMsg;
class NyseLrpMsg;
class CtsTradeMsg;
class TickStatisticsMsg;
class TickStatisticsRequestMsg;
class QdSubscribeMsg;
class QdUnsubscribeMsg;
class NyseOpenbookUltraAddMsg;
class NyseOpenbookUltraExeMsg;
class NyseOpenbookUltraRemMsg;
class NyseOpenbookUltraUpdateMsg;
class FeedStatsMsg;
class PerfQdLatencyMsg;
class OrcBaseMsg;
class OrcOrderDepthMsg;
class OrcPriceFeedMsg;
class BookSnapshotMsg;
class ShfeQdMsg;
class ShfeInfoOrderMsg;
class ShfeInfoTradeMsg;
class TdPositionOffsetMsg;
class TdPositionOffsetFrameMsg;
class TdTradeLimitsMsg;
class TdPositionOffsetRequestMsg;
class ShfeDepthUpdateMsg;
class ShfeDepthHeartbeatMsg;
class ShfeDepthClearMsg;
class MqConnectMsg;
class MqAcceptMsg;
class MqDisconnectMsg;
class AcrPingConnectTcpMsg;
class AcrPingMsg;
class AcrPongMsg;
class AcrPingNoopMsg;
class AcrPingIdentifyMsg;
class AcrPingConnectMqMsg;
class AcrPingConnectUdpMsg;
class AcrPingConnectUnixMsg;
class OpraHeaderMsg;
class OpraTradeMsg;
class OpraOpenInterestMsg;
class OpraEodSummaryMsg;
class OpraQuoteMsg;
class OpraAdminMsg;
class OpraControlMsg;
class OpraUnderlyingMsg;
class MmapConnectMsg;
class MmapResponseMsg;
class MmapConnect2Msg;
class MmapResponse2Msg;
class BbL1BaseMsg;
class BbL1QuoteMsg;
class BbL1BboQuoteMsg;
class BbL1NatBboAppendageMsg;
class BbL1FinraBboAppendageMsg;
class BbL1FinraAdfAppendageMsg;
class BbL1TradeBaseMsg;
class BbL1TradeMsg;
class BbL1TradeCorrectionMsg;
class BbL1TradeCancelMsg;
class BbL1TradePriorDayMsg;
class BbL1TradePriorDayCorrectionMsg;
class BbL1TradePriorDayCancelMsg;
class BbL1TradingStatusMsg;
class BbL1IndexMsg;
class BbL1BidOfferIndexMsg;
class BbL1ConsEodSummaryMsg;
class BbL1PartEodSummaryMsg;
class BbL1AdminUnformattedMsg;
class BbL1ClosingTradeMsg;
class BbL1MostActiveMsg;
class BbL1ClosingBidOfferMsg;
class BbL1MarketVolumeMsg;
class BbL1MarketTradesDollarValueMsg;
class BbL1CrossingSessionMsg;
class BbL1TradingActionMsg;
class BbL1ClosingNbboMsg;
class BbL1ClosingRecapMsg;
class BbL1IssueSymDirectoryMsg;
class BbL1RegShoPriceTestMsg;
class SpinServerRequestMsg;
class SpinServerStreamEndMsg;
class SpinServerSubscribeMsg;
class NyseAlertsBaseMsg;
class NyseAlertsSecurityInfoMsg;
class NyseAlertsImbalanceMsg;
class NyseAlertsDelayHaltsMsg;
class NyseAlertsIndicationMsg;
class NyseAlertsTradeDisseminationTimeMsg;
class NyseAlertsCircuitBreakerMsg;
class NyseAlertsShortSaleRestrictionMsg;
class QbboBaseMsg;
class QbboSysEventMsg;
class QbboStockDirMsg;
class QbboStockTradingActionMsg;
class QbboQuoteMsg;
class QbboRegShoRestrictionMsg;
class SimulatorMsgBaseMsg;
class SimulatorMarketRequestMsg;
class SimulatorMarketActionMsg;
class SimulatorMarketResponseMsg;
class IceProductDefinitionBaseMsg;
class IceSimpleProductDefinitionMsg;
class IceProductDefinitionMsg;
class IceRealtimeBaseMsg;
class IceTradeBaseMsg;
class IceTradeMsg;
class IceCancelledTradeMsg;
class IceMarketStatisticsMsg;
class IceAddModifyOrderMsg;
class IceDeleteOrderMsg;
class IceMarketStateChangeMsg;
class IceOpenPriceMsg;
class IcePriceLevelBaseMsg;
class IcePriceLevelMsg;
class IceAddPriceLevelMsg;
class IceChangePriceLevelMsg;
class IceDeletePriceLevelMsg;
class TimeBucketMsg;
class BasicCandlestickMsg;
class BboCandlestickMsg;
class TocomBaseMsg;
class TocomTradeMsg;
class TocomModifyPriceDepthMsg;
class TocomPriceDepthDeleteLevelsMsg;
class HkfeBaseMsg;
class HkfeTradeMsg;
class HkfeModifyPriceDepthMsg;
class RemoteHedgerIdentifyMsg;
class RemoteHedgerRspRejectMsg;
class RemoteHedgerAddBaitFillMsg;
class RemoteHedgerAddBetaMsg;
class RemoteHedgerRspHedgeFillMsg;
class ProtobufMsg;
class CffexQdMsg;
class CffexInfoOrderMsg;
class CffexInfoTradeMsg;
class WindOrderDepthMsg;
class WindTradeMsg;
class WindIndexDataMsg;
class WindFuturesMarketDataMsg;
class WindStockMarketDataMsg;
class WindStockTransactionMsg;
class KrxQuoteMsg;
class KrxTradeMsg;
class TaifexTradeMsg;
class TaifexBookMsg;
class TaifexSettlementMsg;
class TaifexFinalSettlementMsg;
class WindSzseOrderDataMsg;
class WindSzseOrderQueueDataMsg;
class HkseTradeMsg;
class HkseNewOrderMsg;
class HkseModifyOrderMsg;
class HkseIepMsg;
class HkseDeleteOrderMsg;
class HkseAddOddLotOrderMsg;
class HkseDeleteOddLotOrderMsg;
class HkseTradeCancelMsg;
class DceL2BestAndDeepMsg;
class DceL2TenEntrustMsg;
class DceL2StatsMsg;
class DceL2RealtimePriceMsg;
class DceL2MatchPriceQtyMsg;
class GtaStockSnapshotMsg;
class GtaStockOrderQueueMsg;
class GtaStockOrderRankingMsg;
class GtaIndexSnapshotMsg;
class GtaStockTransactionMsg;
class GtaOrderMsg;
class TwseOddlotTradeMsg;
class TwseBookSnapshotMsg;
class TwseIndexMsg;
class TwseEtfMsg;
class TdCancelBySideMsg;
class TunnelStatsBaseMsg;
class TunnelSendStatsMsg;
class TunnelRecvStatsMsg;
class CmeRecoveryBookBaseMsg;
class CmeRecoveryBookSnapshotMsg;
class CmeRecoveryBookFastDepthMsg;
class CmeRecoveryBookTradeMsg;
class CmeRecoveryBookVolumeMsg;
class CmeRecoveryBookDailyStatMsg;
class CmeRecoveryBookSessionStatMsg;
class CmeRecoveryBookLimitsMsg;
class CmeRecoveryBookSecurityStatusMsg;
class QuantFeedBaseMsg;
class QuantFeedL1SnapshotMsg;
class QuantFeedTradeMsg;
class QuantFeedL1Msg;
class QuantFeedIndexMsg;
class QuantFeedMblFullRefreshMsg;
class QuantFeedMblDeltaRefreshMsg;
class QuantFeedMblOverlapRefreshMsg;
class QuantFeedMblMaxVisibleDepthMsg;
class MarketMicrostructureMsg;
class CzceL1QdMsg;
class CzceL2QdMsg;
class CmeRecoveryBookTradeDetailMsg;
class CmeRecoveryBookTradeDetailArrayMsg;
class ArcaBaseMsg;
class ArcaAddMsg;
class ArcaModMsg;
class ArcaDelMsg;
class ArcaImbalanceMsg;
class ArcaSymUpdateMsg;
class ArcaExecuteMsg;
class ArcaAddOrderRefreshMsg;
class ArcaTradeMsg;
class ArcaTradeCancelMsg;
class ArcaTradeCorrectionMsg;
class ArcaSymbolClearMsg;
class ArcaSessionChangeMsg;
class ArcaSecurityStatusMsg;
class ArcaTimeRefMsg;
class ShfeDepthRefreshCompleteMsg;
class ArcaStockSummaryMsg;
class McastTestRequestMsg;
class McastTestReplyMsg;
class CqgSecurityDefinitionMsg;
class CqgBaseMsg;
class CqgBookUpdateMsg;
class CqgTradeMsg;
class CqgStatisticsMsg;
class CqgFlushBookMsg;
class CqgSnapshotMsg;
class ShseIndexDataMsg;
class ShseVauctionPriceMsg;
class ShfeDepthActivityUpdateMsg;
class AcrBookMsg;
class AcrTradeMsg;
class OnIdleMsg;
class StatCollectorMsg;
class LmeHistoricalOrderMsg;
class LmeHistoricalTradeMsg;
class SharedMemoryBook10Msg;
class LocateReturnMsg;
class LocateRequestMsg;
class LocateRequestResponseMsg;
class LocateReturnResponseMsg;
class DessertStrategyMsg;
class SseHistoricalDayMinMsg;
class SseHistoricalSnap1Msg;
class SseHistoricalTickMsg;
class SseHistoricalAuctionMsg;
class SseHistoricalSnap2Part1Msg;
class SseHistoricalSnap2Part2Msg;
class SseHistoricalSnapOptionMsg;

} // namespace bb

#endif // BB_CORE_MESSAGES_AUTOGEN_FWD_H

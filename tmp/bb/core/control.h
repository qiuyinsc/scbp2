#ifndef BB_CORE_CONTROL_H
#define BB_CORE_CONTROL_H

/* Contents Copyright 2016 Athena Capital Research LLC. All Rights Reserved. */

/** \file Contains definitions of various enums used in the
BlackBox system.  This file is automatically generated from .build/optimize_trusty/bb/core/control.table
with the const-gen.py utility.  DO NOT EDIT THIS FILE DIRECTLY!!
Instead, edit .build/optimize_trusty/bb/core/control.table and rerun const-gen.py. */

#include <iosfwd>

namespace bb {
typedef enum {
CTRL_NONE = 0,
CTRL_RECONNECT = 1,
CTRL_STOP = 2,
CTRL_START = 3,
CTRL_RECONNECT_MARGIN = 4,
CTRL_RESET = 5,
CTRL_SAFETY_OFF = 6,
CTRL_SAFETY_ON = 7,
CTRL_DUMP = 8,
CTRL_CLEAR_CACHE = 9,
CTRL_MAX = 10,
}control_t;

void controlPopulateCacheMap();
const char *control2str(control_t t);
const char *control2str(control_t t, bool* ok);
bool controlIsValid(control_t t);
control_t str2control(const char *s, bool* ok = NULL);
control_t str2control_throw(const char *s);

// SWIG 1.3.29 doesn't like these in-namespace operators
#ifndef SWIG
std::ostream &operator <<(std::ostream &out, const control_t &t);
std::istream &operator >>(std::istream &in, control_t &t);
#endif // SWIG

} // namespace bb

#endif // BB_CORE_CONTROL_H

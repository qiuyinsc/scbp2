#ifndef BB_CORE_SYMBOLTABLECONFIG_H
#define BB_CORE_SYMBOLTABLECONFIG_H

/* Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved. */


#include <bb/core/date.h>
#include <bb/core/smart_ptr.h>

namespace bb {

BB_FWD_DECLARE_SHARED_PTR( Environment );
BB_FWD_DECLARE_SHARED_PTR( ISymbolContext );

class ISymbolTableFactory
{
public:
    virtual ~ISymbolTableFactory();

    virtual ISymbolContextPtr construct( const EnvironmentPtr& environment ) const = 0;
};

BB_DECLARE_SHARED_PTR( ISymbolTableFactory );

class FileSymbolTableConfig
    : public ISymbolTableFactory
{
public:
    std::string table_name;

public:
    FileSymbolTableConfig();

    // Implements ISymbolTableFactory
    ISymbolContextPtr construct( const EnvironmentPtr& environment ) const;
};

BB_DECLARE_SHARED_PTR( FileSymbolTableConfig );

class DBSymbolTableConfig
    : public ISymbolTableFactory
{
public:
    std::string table_name;
    std::string profile;
    date_t date;

public:
    DBSymbolTableConfig();

    // Implements ISymbolTableFactory
    ISymbolContextPtr construct( const EnvironmentPtr& environment ) const;
};

BB_DECLARE_SHARED_PTR( DBSymbolTableConfig );

} // namespace bb

#endif // BB_CORE_SYMBOLTABLECONFIG_H

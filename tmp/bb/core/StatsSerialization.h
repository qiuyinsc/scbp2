#ifndef BB_CORE_STATSSERIALIZATION_H
#define BB_CORE_STATSSERIALIZATION_H

/* Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved. */


#include <boost/serialization/serialization.hpp>
#include <boost/serialization/version.hpp>
#include <bb/core/Stats.h>
#include <bb/core/Error.h>

/// secstat stores the status of some calculations, so that they can be combined across
/// dates if necessary. These are the boost::serialization methods.

namespace boost {
namespace serialization {

template<class Archive>
void serialize(Archive & ar, bb::VarianceCalculator &vc, const unsigned int version)
{
    ar & vc.m_count;
    ar & vc.m_mean;
    ar & vc.m_devSum;
    ar & vc.m_min;
    ar & vc.m_max;
}

template<class Archive>
void load(Archive & ar, bb::QuantileCalculator &qc, const unsigned int version)
{
    if(version <= 0)
        BB_THROW_ERROR("bb::QuantileCalculator: cannot read old boost serialization file format");

    int numQuantiles;
    ar & numQuantiles;
    if(numQuantiles != qc.m_numQuantiles)
        BB_THROW_ERROR_SS("bb::QuantileCalculator: m_numQuantiles is "
                << numQuantiles << " but is required to be " << qc.m_numQuantiles);

    ar & qc.m_consumedSize;
    ar & qc.m_dataBufSize;
    qc.m_pval.reset(new double[qc.m_numQuantiles]);
    qc.m_dataBuf.reset(new double[bb::QuantileCalculator::NBUF]);
    qc.m_quantiles.reset(new double[qc.m_numQuantiles]);
    qc.m_newQuantiles.reset(new double[qc.m_numQuantiles]);

    for(int i = 0; i < qc.m_numQuantiles; ++i)
        ar & qc.m_pval[i];
    for(int i = 0; i < qc.m_dataBufSize; ++i)
        ar & qc.m_dataBuf[i];
    for(int i = 0; i < qc.m_numQuantiles; ++i)
        ar & qc.m_quantiles[i];

    ar & qc.m_minDatum;
    ar & qc.m_maxDatum;
}

template<class Archive>
void save(Archive & ar, const bb::QuantileCalculator &qc, const unsigned int version)
{
    ar & qc.m_numQuantiles;
    ar & qc.m_consumedSize;
    ar & qc.m_dataBufSize;
    for(int i = 0; i < qc.m_numQuantiles; ++i)
        ar & qc.m_pval[i];
    for(int i = 0; i < qc.m_dataBufSize; ++i)
        ar & qc.m_dataBuf[i];
    for(int i = 0; i < qc.m_numQuantiles; ++i)
        ar & qc.m_quantiles[i];
    ar & qc.m_minDatum;
    ar & qc.m_maxDatum;
}

} // namespace serialization
} // namespace boost

BOOST_CLASS_VERSION(bb::QuantileCalculator, 1);
BOOST_SERIALIZATION_SPLIT_FREE(bb::QuantileCalculator);

#endif // BB_CORE_STATSSERIALIZATION_H

#ifndef BB_CORE_INSTRUMENT_H
#define BB_CORE_INSTRUMENT_H

/* Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved. */


/** \file
    Describes an instrument and its components.
**/

#include <cstring>
#include <iosfwd>
#include <set>

#include <boost/functional/hash.hpp>

#include <bb/core/currency.h>
#include <bb/core/expiry.h>
#include <bb/core/hash.h>
#include <bb/core/mktdest.h>
#include <bb/core/product.h>
#include <bb/core/right.h>
#include <bb/core/symbol.h>
#include <bb/core/smart_ptr.h>

namespace bb {

// Forward declaration
class timeval_t;
class instrument_t;

BB_FWD_DECLARE_SHARED_PTR(InstrumentInfoContext);

namespace ProductSpread {
namespace bb_instrument_hooks {
void evalSpreadHash( std::size_t&, const instrument_t& );
} // namespace bb_instrument_hooks
} // namespace ProductSpread

/// Represents a tradable object.
/// Not every field is used for all products.
class instrument_t
{
public:
    /// Returns an instrument that is unknown (SYM_UNKNOWN, the rest are unknown).
    static instrument_t unknown()
    {   return instrument_t( SYM_UNKNOWN, MKT_UNKNOWN, PROD_UNKNOWN, expiry_t::UNKNOWN, 0, RT_UNKNOWN, currency_t::UNKNOWN ); }

    /// Returns an instrument that all instruments (SYM_ALL, the rest are unknown)
    static instrument_t all()
    {   return instrument_t( SYM_ALL, MKT_UNKNOWN, PROD_UNKNOWN, expiry_t::UNKNOWN, 0, RT_UNKNOWN, currency_t::UNKNOWN ); }

    /// Returns an instrument that all instruments (SYM_NONE, the rest are unknown)
    static instrument_t none()
    {   return instrument_t( SYM_NONE, MKT_UNKNOWN, PROD_UNKNOWN, expiry_t::UNKNOWN, 0, RT_UNKNOWN, currency_t::UNKNOWN ); }

    /// Returns an instrument that is a stock
    static instrument_t stock( symbol_t sym_, mktdest_t mkt_ = MKT_UNKNOWN, currency_t curr_ = currency_t::USD )
    {   return instrument_t( sym_, mkt_, PROD_STOCK, expiry_t(), 0, RT_NONE, curr_ ); }

    /// Returns an instrument that is a stock
    static instrument_t stock( const char* symname_, mktdest_t mkt_ = MKT_UNKNOWN, currency_t curr_ = currency_t::USD )
    {   return instrument_t( symbol_t(symname_), mkt_, PROD_STOCK, expiry_t(), 0, RT_NONE, curr_ ); }

    /// Returns an instrument that is a fx spot
    static instrument_t fx( const char* symname_, mktdest_t mkt_ = MKT_UNKNOWN, currency_t curr_ = currency_t::USD )
    {   return instrument_t( symbol_t(symname_), mkt_, PROD_FX, expiry_t(), 0, RT_NONE, curr_ ); }

    /// Returns an instrument that is a future
    static instrument_t future( symbol_t sym_, expiry_t exp_, mktdest_t mkt_ = MKT_UNKNOWN, currency_t curr_ = currency_t::USD )
    {   return instrument_t( sym_, mkt_, PROD_FUTURE, exp_, 0, RT_NONE, curr_ ); }

    /// Returns an instrument that is a future
    static instrument_t future( const char* symname_, expiry_t exp_, mktdest_t mkt_ = MKT_UNKNOWN, currency_t curr_ = currency_t::USD )
    {   return instrument_t( symbol_t(symname_), mkt_, PROD_FUTURE, exp_, 0, RT_NONE, curr_ ); }

    /// Returns an instrument that is an option
    static instrument_t option( symbol_t sym_, expiry_t exp_, float strike_, right_t rt_, mktdest_t mkt_ = MKT_UNKNOWN, currency_t curr_ = currency_t::USD )
    {   return instrument_t( sym_, mkt_, PROD_OPTION, exp_, strike_, rt_, curr_ ); }

    /// Returns an instrument that is an option
    static instrument_t option( const char* symname_, expiry_t exp_, float strike_, right_t rt_, mktdest_t mkt_ = MKT_UNKNOWN, currency_t curr_ = currency_t::USD )
    {   return instrument_t( symbol_t(symname_), mkt_, PROD_OPTION, exp_, strike_, rt_, curr_ ); }

    /// Returns an instrument that only has a valid symid and PROD_SYM_ONLY. Rest is UNKNOWN. This is used to subscribe to all instruments under that symid for instance.
    static instrument_t instr_sym_only( symbol_t sym_ )
    {   return instrument_t( sym_, MKT_UNKNOWN, PROD_SYM_ONLY, expiry_t::UNKNOWN, 0.0, RT_NONE, currency_t::UNKNOWN ); }

    /// Returns an instrument that only has a valid symid and PROD_SYM_ONLY. Rest is UNKNOWN. This is used to subscribe to all instruments under that symid for instance.
    static instrument_t instr_sym_only( const char* symname_ )
    {   return instrument_t( symbol_t(symname_), MKT_UNKNOWN, PROD_SYM_ONLY, expiry_t::UNKNOWN, 0.0, RT_NONE, currency_t::UNKNOWN ); }

    /// Returns an instrument that is an ACR Index
    static instrument_t acrindex( symbol_t sym_, mktdest_t mkt_ = MKT_UNKNOWN, currency_t curr_ = currency_t::USD )
    {   return instrument_t( sym_, mkt_, PROD_ACRINDEX, expiry_t(), 0, RT_NONE, curr_ ); }

    /// Returns an instrument that is an ACR Index
    static instrument_t acrindex( const char* symname_, mktdest_t mkt_ = MKT_UNKNOWN, currency_t curr_ = currency_t::USD )
    {   return instrument_t( symbol_t(symname_), mkt_, PROD_ACRINDEX, expiry_t(), 0, RT_NONE, curr_ ); }

    /// Returns an instrument that is cash.
    static instrument_t cash( currency_t curr_ = currency_t::USD )
    {   return instrument_t( symbol_t("FIXED_CASH_AMT"), MKT_UNKNOWN, PROD_CASH, expiry_t(), 0, RT_NONE, curr_ ); }
    /// Returns an instrument that is cash.
    static instrument_t cash( symbol_t sym_, currency_t curr_ = currency_t::USD )
    {   return instrument_t( sym_, MKT_UNKNOWN, PROD_CASH, expiry_t(), 0, RT_NONE, curr_ ); }

    /// Returns an instrument_t given a string, or instrument_t::unknown() on failure.
    /// This must be "SYM" for a stock, "SYM:YYYYMM" for a future, or "SYM:YYYYMM:[CP]:strike" for an option.
    /// This is the same format as operator<< and toString.
    static instrument_t fromString( const std::string& str );

    /// Returns a string format readable by fromString.
    std::string toString() const;

    /// Returns true if the given instrument_t is valid.
    /// All specific fields for a product_t must be valid for the product to be valid.
    bool is_valid() const;

    bool is_option() const { return prod == PROD_OPTION || prod == PROD_EURO_OPTION; }

    bool is_option_call() const { return is_option() && right == RT_CALL; }
    bool is_option_put() const { return is_option() && right == RT_PUT; }
    bool is_instr_sym_only() const { return prod == PROD_SYM_ONLY; }

    symbol_t getSymbol() const { return sym; }

    expiry_t getExpiry() const { return exp; }

    /// Components -- not every product type uses all components
    symbol_t    sym;
    mktdest_t   mkt;
    product_t   prod;
    expiry_t    exp;    // only valid for futures and options
    float       strike; // only valid for options
    right_t     right;  // only valid for options
    currency_t  currency;
    int32_t pad[5];//DO NOT CHANGE THIS SIZE!!!!!!!!!!!!!!

    /// note: since it is raw access, this constructor allows you to generate invalid instruments
    instrument_t( symbol_t sym_, mktdest_t mkt_, product_t prod_,
                  expiry_t exp_ = expiry_t(), float strike_ = 0,
                  right_t rt_ = RT_NONE, currency_t curr_ = currency_t::USD )
        : sym( sym_ ), mkt( mkt_ ), prod( prod_ ), exp( exp_ )
        , strike( strike_ ), right( rt_ ), currency( curr_ )
    {
        memset( pad, 0, sizeof pad );
    }

    explicit instrument_t( symbol_t sym_ = SYM_NONE )
        : sym( sym_ ), mkt( MKT_UNKNOWN ), prod( PROD_UNKNOWN ),
          exp( expiry_t::UNKNOWN ), strike( 0 ), right( RT_UNKNOWN ),
          currency( currency_t::UNKNOWN )
    {
        memset( pad, 0, sizeof pad );
    }

    /// common comparators

    /// comparator which ignores the mktdest and currency.
    /// note: uses < to compare option strike prices
    struct less_no_mkt_no_currency
    {
        bool operator ()( const instrument_t& r1, const instrument_t& r2 ) const;
    };
    struct equals_no_mkt_no_currency
    {
        // note: uses (fuzzy, nontransitive) bb::NE to compare
        // option strike prices. this is the same as operator==
        bool operator ()( const instrument_t& r1, const instrument_t& r2 ) const;
    };
    struct hash_no_mkt_no_currency
    {
        size_t operator ()( const instrument_t& instr ) const;
    };
};

typedef std::set<bb::instrument_t, bb::instrument_t::less_no_mkt_no_currency> instr_no_mkt_no_currency_set;

/// Equality operator.  Only compares the valid values of the product.
/// Invalid products are equal.
bool operator ==( const instrument_t& lhs, const instrument_t& rhs );
bool operator !=( const instrument_t& lhs, const instrument_t& rhs );
bool operator < ( const instrument_t& lhs, const instrument_t& rhs );

inline bool operator !=( const instrument_t& lhs, const instrument_t& rhs )
{
    return !( lhs == rhs );
}

/// Hash value for instrument
/// Conforms to boost::hash standards
inline std::size_t hash_value( const instrument_t& instr )
{
    size_t seed = 0;
    boost::hash_combine( seed, instr.sym.id() );
    boost::hash_combine( seed, instr.mkt );
    boost::hash_combine( seed, instr.prod );
    boost::hash_combine( seed, instr.currency.id() );

    if ( instr.prod == bb::PROD_FUTURE )
    {
        boost::hash_combine( seed, instr.exp.expid() );
    }
    else if ( instr.prod == bb::PROD_OPTION )
    {
        boost::hash_combine( seed, instr.exp.expid() );
        boost::hash_combine( seed, instr.strike );
        boost::hash_combine( seed, instr.right );
    }
    else if ( instr.prod == bb::PROD_SPREAD )
    {
        boost::hash_combine( seed, instr.exp.expid() );
        ProductSpread::bb_instrument_hooks::evalSpreadHash( seed, instr );
    }

    return seed;
}

product_t sym2product( symbol_t sym );
product_t sym2product( symbol_t sym, const InstrumentInfoContextCPtr& context );
mktdest_t sym2mktdest( symbol_t sym );
mktdest_t sym2mktdest( symbol_t sym, const InstrumentInfoContextCPtr& context );

/// Stream operator to convert instrument_t into human readable form.
std::ostream& operator <<( std::ostream& out, const instrument_t& instr );

/// Stream operator to produce an instrument_t from human readable form.
std::istream& operator >>( std::istream& out, instrument_t& instr );

} // namespace bb


BB_HASH_NAMESPACE_BEGIN {

template<> struct hash<bb::instrument_t>
{
public:
    size_t operator()( const bb::instrument_t & instr ) const
    {
        return boost::hash<bb::instrument_t>()( instr );
    }
};


template<> struct hash<std::pair<bb::instrument_t, bb::timeval_t> >
{
public:
    size_t operator ()( const std::pair<bb::instrument_t, bb::timeval_t>& it ) const
    {
        std::size_t seed = 0;
        boost::hash_combine( seed, it.first );
        boost::hash_combine( seed, it.second );
        return seed;
    }
};

} BB_HASH_NAMESPACE_END


#endif // BB_CORE_INSTRUMENT_H

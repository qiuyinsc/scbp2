#ifndef BB_CORE_ESPINTYPE_H
#define BB_CORE_ESPINTYPE_H

/* Contents Copyright 2016 Athena Capital Research LLC. All Rights Reserved. */

/** \file Contains definitions of various enums used in the
BlackBox system.  This file is automatically generated from .build/optimize_trusty/bb/core/ESpinType.table
with the const-gen.py utility.  DO NOT EDIT THIS FILE DIRECTLY!!
Instead, edit .build/optimize_trusty/bb/core/ESpinType.table and rerun const-gen.py. */

#include <iosfwd>

namespace bb {
typedef enum {
SPIN_TYPE_L1 = 0,
SPIN_TYPE_ISO = 1,
SPIN_TYPE_TICK = 2,
SPIN_TYPE_TICK_PUBLISH = 3,
SPIN_TYPE_BOOK = 4,
SPIN_TYPE_LOCATE = 5,
}ESpinType;

void ESpinTypePopulateCacheMap();
const char *ESpinType2str(ESpinType t);
const char *ESpinType2str(ESpinType t, bool* ok);
bool ESpinTypeIsValid(ESpinType t);
ESpinType str2ESpinType(const char *s, bool* ok = NULL);
ESpinType str2ESpinType_throw(const char *s);

// SWIG 1.3.29 doesn't like these in-namespace operators
#ifndef SWIG
std::ostream &operator <<(std::ostream &out, const ESpinType &t);
std::istream &operator >>(std::istream &in, ESpinType &t);
#endif // SWIG

} // namespace bb

#endif // BB_CORE_ESPINTYPE_H

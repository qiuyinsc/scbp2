#ifndef BB_CORE_STATISTICSMANAGER_H
#define BB_CORE_STATISTICSMANAGER_H

/* Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved. */


#include <sys/stat.h>
#include <fcntl.h>

#include <boost/bind.hpp>
#include <boost/enable_shared_from_this.hpp>
#include <boost/function.hpp>
#include <boost/thread/shared_mutex.hpp>

#include <bb/core/smart_ptr.h>
#include <bb/core/StatisticsTree.h>
#include <bb/core/hash_map.h>
#include <bb/core/hash.h>
#include <bb/core/Error.h>

namespace bb {
namespace statistics {

BB_FWD_DECLARE_SHARED_PTR( StatisticsManager );

BB_FWD_DECLARE_SHARED_PTR( BaseContext );

class BaseContext
{
    friend class StatisticsManager;
    typedef boost::function<void()> CleanupAction;

protected:
    inline BaseContext( const StatisticsManagerPtr& manager,
                        const BaseContextPtr& parent );

    virtual ~BaseContext();

    inline const StatisticsManagerPtr& manager() const;

    void setCleanupAction( const CleanupAction& action );

    // NOTE: This must only be called when holding a write-lock on
    // m_manager->m_lock, since it may mutate the structure of the
    // tree. The best way to ensure this is to call this method from
    // the manager.
    void doCleanupAction();

private:
    const StatisticsManagerPtr m_manager;
    const BaseContextPtr m_parent;
    CleanupAction m_cleanup;
};

BB_FWD_DECLARE_SHARED_PTR( DictionaryContext );
class DictionaryContext : public BaseContext
                        , public boost::enable_shared_from_this<DictionaryContext>
{
    friend class StatisticsManager;

public:
    template<typename T>
    inline bb::shared_ptr<T> open( const std::string& name, int flags );

    inline bool addStatistic( const StatBuilder& builder );

    virtual ~DictionaryContext();

protected:
    typedef DictionaryNode NodeType;
    typedef DictionaryNodePtr NodeTypePtr;

    inline DictionaryContext( const StatisticsManagerPtr& manager,
                              const BaseContextPtr& parent,
                              const NodeTypePtr& node );

    inline const NodeTypePtr& node() const;

private:
    const NodeTypePtr m_node;
};

BB_FWD_DECLARE_SHARED_PTR( AutoIndexContext );
class AutoIndexContext : public BaseContext
                       , public boost::enable_shared_from_this<AutoIndexContext>
{
    friend class StatisticsManager;

public:
    template<typename T>
    inline bb::shared_ptr<T> open( const size_t index );

    template<typename T>
    inline bb::shared_ptr<T> open( const std::string& index );

    template<typename T>
    inline bb::shared_ptr<T> create();

    virtual ~AutoIndexContext();

protected:
    typedef AutoIndexNode NodeType;
    typedef AutoIndexNodePtr NodeTypePtr;

    inline AutoIndexContext( const StatisticsManagerPtr& manager,
                             const BaseContextPtr& parent,
                             const NodeTypePtr& node );

    inline const NodeTypePtr& node() const;

private:
    const NodeTypePtr m_node;
};

class StatisticsManager : public boost::enable_shared_from_this<StatisticsManager>
{
    friend class DictionaryContext;
    friend class AutoIndexContext;

public:
    StatisticsManager();
    ~StatisticsManager();

    // Get the root context for the statistics tree
    const DictionaryContextPtr& getRootContext();

    // Read-lock the tree, and apply the given visitor starting at the
    // named node. Returns 'false' if no such node exists.
    bool applyVisitor( Visitor& visitor, const std::string& where = std::string() ) const;

    // Write lock the tree, and attach 'subtree' as a child named
    // 'name' in the given dictionary context.
    bool attach( const DictionaryNodePtr& where, const std::string& name, const NodePtr& subtree );

private:

    template<typename T>
    bb::shared_ptr<T> open( const DictionaryContextPtr& context,
                            const std::string& name, const int flags );

    template<typename T>
    bb::shared_ptr<T> open( const AutoIndexContextPtr& context,
                            const size_t index );

    template<typename T>
    bb::shared_ptr<T> open( const AutoIndexContextPtr& context,
                            const std::string& index );

    template<typename T>
    bb::shared_ptr<T> create( const AutoIndexContextPtr& context );

    void cleanupContext( BaseContext* doomed, const NodePtr& node );

    void registerNewContext( const BaseContextPtr& context, const NodePtr& node );

    template<typename T>
    bb::shared_ptr<T> getContextForNode( const NodePtr& node );

    mutable boost::shared_mutex m_lock;
    const TreePtr m_tree;
    mutable DictionaryContextPtr m_root;

    typedef bbext::hash_map<Node*, BaseContextWeakPtr> ContextCache;
    typedef bbext::hash_map<std::string, NodeWeakCPtr> NodeCache;

    ContextCache m_contextCache;
    mutable NodeCache m_nodeCache;
};
BB_DECLARE_SHARED_PTR( StatisticsManager );


inline BaseContext::BaseContext( const StatisticsManagerPtr& manager,
                                 const BaseContextPtr& parent )
    : m_manager( manager )
    , m_parent( parent ) {}

inline const StatisticsManagerPtr& BaseContext::manager() const
{
    return m_manager;
}

inline void BaseContext::setCleanupAction( const CleanupAction& action )
{
    m_cleanup = action;
}

inline void BaseContext::doCleanupAction()
{
    m_cleanup();
}

inline DictionaryContext::DictionaryContext(
    const StatisticsManagerPtr& manager,
    const BaseContextPtr& parent,
    const NodeTypePtr& node )
    : BaseContext( manager, parent )
    , m_node( node ) {}

inline const DictionaryContext::NodeTypePtr& DictionaryContext::node() const
{
    return m_node;
}

template<typename T>
inline bb::shared_ptr<T> DictionaryContext::open( const std::string& name, int flags )
{
    return manager()->open<T>( shared_from_this(), name, flags );
}

inline bool DictionaryContext::addStatistic( const StatBuilder& builder )
{
    return manager()->attach( m_node, builder.getName(), builder.getTree() );
}

inline AutoIndexContext::AutoIndexContext( const StatisticsManagerPtr& manager,
                                           const BaseContextPtr& parent,
                                           const NodeTypePtr& node )
    : BaseContext( manager, parent )
    , m_node( node ) {}

inline const AutoIndexContext::NodeTypePtr& AutoIndexContext::node() const
{
    return m_node;
}

template<typename T>
bb::shared_ptr<T> AutoIndexContext::open( const size_t index )
{
    return manager()->open<T>( shared_from_this(), index );
}

template<typename T>
bb::shared_ptr<T> AutoIndexContext::open( const std::string& index )
{
    return manager()->open<T>( shared_from_this(), index );
}

template<typename T>
bb::shared_ptr<T> AutoIndexContext::create()
{
    return manager()->create<T>( shared_from_this() );
}

template<typename T>
bb::shared_ptr<T> StatisticsManager::open( const DictionaryContextPtr& context,
                                           const std::string& name,
                                           const int flags )
{
    const boost::unique_lock<boost::shared_mutex> lock( m_lock );

    const NodePtr node = context->node()->find( name );
    if( !node )
    {
        if( !(flags & O_CREAT) )
            BB_THROW_ERROR_SSX( "Attempt to open non-existent node named: " << name );

        const typename T::NodeTypePtr newNode = T::NodeType::create();
        const bb::shared_ptr<T> newContext( new T( context->manager(), context, newNode ) );
        context->node()->insert( name, newNode );

        BaseContext::CleanupAction cleanup =
            boost::bind( &DictionaryNode::remove, context->node(), name );

        newContext->setCleanupAction( cleanup );
        registerNewContext( newContext, newContext->node() );
        return newContext;
    }
    else
    {
        if( (flags & O_CREAT) && (flags & O_EXCL) )
            BB_THROW_ERROR_SSX( "Attempt to exclusively create existing node named: " << name );

        return getContextForNode<T>( node );
    }
}

#if 0
template<typename T>
bb::shared_ptr<T> StatisticsManager::open( const AutoIndexContextPtr& context,
                                           const size_t index )
{
    const boost::unique_lock<boost::shared_mutex> lock( m_lock );

    const NodePtr node = context->node()->find( index );
    if( !node )
        BB_THROW_ERROR_SSX( "Attempt to open non-existent node with index: " << index );

    return getContextForNode<T>( node );
}
#endif

template<typename T>
bb::shared_ptr<T> StatisticsManager::open( const AutoIndexContextPtr& context,
                                           const std::string& index )
{
    const boost::unique_lock<boost::shared_mutex> lock( m_lock );

    const NodePtr node = context->node()->find( index );
    if( !node )
        BB_THROW_ERROR_SSX( "Attempt to open non-existent node with index: " << index );

    return getContextForNode<T>( node );
}

template<typename T>
bb::shared_ptr<T> StatisticsManager::create( const AutoIndexContextPtr& context )
{
    const boost::unique_lock<boost::shared_mutex> lock( m_lock );

    const typename T::NodeTypePtr newNode = T::NodeType::create();
    const bb::shared_ptr<T> newContext( new T( context->manager(), context, newNode ) );
    const size_t index = context->node()->insert( newNode );

    BaseContext::CleanupAction cleanup =
        boost::bind( &AutoIndexNode::remove, context->node(), index );

    newContext->setCleanupAction( cleanup );
    registerNewContext( newContext, newContext->node() );
    return newContext;
}

template<typename T>
bb::shared_ptr<T> StatisticsManager::getContextForNode( const NodePtr& node )
{
    const ContextCache::iterator where = m_contextCache.find( node.get() );
    if( where == m_contextCache.end() )
        BB_THROW_ERROR( "Node does not have an associated context" );

    const BaseContextPtr locked = where->second.lock();
    if( !locked )
    {
        m_contextCache.erase( where );
        BB_THROW_ERROR( "Node does not have an associated context" );
    }

    const bb::shared_ptr<T> result = boost::dynamic_pointer_cast<T>( locked );
    if( !result )
        BB_THROW_ERROR( "Node does not have desired dynamic type" );

    return result;
}


} // namespace statistics
} // namespace bb

#endif // BB_CORE_STATISTICSMANAGER_H

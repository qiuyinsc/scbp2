#ifndef BB_CORE_LISTENERRELAY_H
#define BB_CORE_LISTENERRELAY_H

/* Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved. */


#include <vector>
#include <boost/foreach.hpp>

#include <bb/core/shared_vector.h>
#include <bb/core/ListenerList.h>
#include <bb/core/PrioritizedListenerList.h>

namespace bb {

/** Relay listeners to another provider
 * Use this object when you want to pass on a list of subscribers to other
 * subscribable object(s)
 *
 * @code
 *
 * IOrderStatusListener listener;
 *
 * bb::ListenerRelay<IOrderStatusListener*> relay_obj;
 * relay_obj.add( &listener, Priority(100) );
 *
 * OrderPtr o(...);
 * /// _1, _2, _3 is Subscription&, Listener*, Priority&
 * /// this will call subscribe for every subscriber
 * relay_obj.relay( boost::bind(&OrderListenerInfo::subscribe
 *         , boost::ref( o->listeners() )
 *         , _1, _2, _3) );
 *
 * // by here, listener will be subscribed to o
 *
 * @endcode
 *
 * Notes: The previous implementation doesn't allow future subscriptions to be
 *        passed on, this was an oversight.  A newly added method "registerTarget"
 *        will register the subscription target so future subscriptions will
 *        apply to the target.
 * Use case:
 * You have a bunch of listeners that want to listen to events from new objects getting created.  You put the
 * listeners <L> in the relay R, then when the new object O gets create, you call
 * "R.relay( boost::bind( &O::subscribe, O, _1, _2, _3 ) )", and then all <L> will be subscribed to O.
 * You also call R.registerTarget( boost::bind( &O::subscribe, O, _1, _2, _3 ) ), and when you add a new L to R,
 * L will be subscribed to O automatically
 */
template<typename ListenerT>
class ListenerRelay
{
public:
    typedef typename boost::function< void ( Subscription&, ListenerT, const Priority& )>
    SubscribeFunc;

public:
//    typedef bb::trading::IOrderStatusListener* ListenerT;
    class SubscriberInfo
    {
    public:
        SubscriberInfo( const ListenerT& l, const Priority& p )
            : m_listener( l )
            , m_priority( p ) {}
        ListenerT m_listener;
        Priority m_priority;
        std::vector<Subscription> m_relayedSubscriptions;
    };
    class ListenerFinder
    {
    public:
        ListenerFinder(const ListenerT& v) : m_v(v) {}
        bool operator()( const SubscriberInfo & a ) { return a.m_listener == m_v; }
        const ListenerT& m_v;
    };
    typedef bb::shared_vector< SubscriberInfo > SubscriberList;

    bool add( const ListenerT& l, const Priority& p )
    {
        typename SubscriberList::unsafe_iterator i = std::find_if(m_subscribers.unsafe_begin(), m_subscribers.unsafe_end(), ListenerFinder(l));
        if( i != m_subscribers.unsafe_end() )
            return false;
        typename SubscriberList::unsafe_iterator j =
            m_subscribers.explicit_insert( m_subscribers.unsafe_end(), SubscriberInfo( l, p ) );
        BOOST_FOREACH( SubscribeFunc& f, m_relayTargets )
        {
            Subscription s;
            f( s, l, p );
            j->m_relayedSubscriptions.push_back( s );
        }
        return true;
    }

    void remove( const ListenerT& l )
    {
        typename SubscriberList::unsafe_iterator i = std::find_if(m_subscribers.unsafe_begin(), m_subscribers.unsafe_end(), ListenerFinder(l));
        if( i != m_subscribers.unsafe_end() )
            m_subscribers.explicit_erase( i );
    }

    void subscribe( Subscription &outSub, const ListenerT& l, const Priority& p )
    {
        m_subscribers.push_back( outSub, SubscriberInfo( l, p ) );
    }

    template<typename C> void addSubscription( C* container, const ListenerT& l, const Priority& p )
    {
        Subscription sub;
        subscribe(sub, l, p);
        container->push_back( sub );
    }

    void relay( PrioritizedListenerList<ListenerT>* list )
    {
        typename SubscriberList::unsafe_iterator it = m_subscribers.unsafe_begin();
        for( ; it != m_subscribers.unsafe_end(); ++it )
            list->addSubscription( &it->m_relayedSubscriptions, it->m_listener, it->m_priority );
    }

    void relay( ListenerList<ListenerT>* list )
    {
        typename SubscriberList::unsafe_iterator it = m_subscribers.unsafe_begin();
        for( ; it != m_subscribers.unsafe_end(); ++it )
            list->addSubscription( &it->m_relayedSubscriptions, it->m_listener );
    }

    void relay( const SubscribeFunc& f )
    {
        typename SubscriberList::unsafe_iterator it = m_subscribers.unsafe_begin();
        for( ; it != m_subscribers.unsafe_end(); ++it )
        {
            Subscription sub;
            f( sub, it->m_listener, it->m_priority );
            it->m_relayedSubscriptions.push_back( sub );
        }
    }

    /// register a subscription function to be called whenever a new
    /// subscription is made
    void registerTarget( const SubscribeFunc& f )
    {
        typename SubscriberList::unsafe_iterator it = m_subscribers.unsafe_begin();
        for( ; it != m_subscribers.unsafe_end(); ++it )
        {
            Subscription sub;
            f( sub, it->m_listener, it->m_priority );
            it->m_relayedSubscriptions.push_back( sub );
        }
        m_relayTargets.push_back( f );
    }

    template<typename O> void registerTarget( void (O::* func) ( Subscription&, ListenerT, const Priority& ), O* o )
    {
        registerTarget( boost::bind( func, o, _1, _2, _3 ) );
    }

    size_t size() const { return m_subscribers.size(); }

protected:
    SubscriberList m_subscribers;
    std::vector<SubscribeFunc> m_relayTargets;
};


} // namespace bb

#endif // BB_CORE_LISTENERRELAY_H

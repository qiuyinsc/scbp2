#ifndef BB_CORE_PROCUTILS_H
#define BB_CORE_PROCUTILS_H

/* Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved. */


#include <sys/types.h>

namespace bb {
namespace ProcUtils {

// A simple wrapper around mlockall(MCL_CURRENT | MCL_FUTURE). This
// will attempt to lock all current process memory in core, as well as
// all future memory. After such a call, it should be impossible for
// the process to take page faults due to pages swapped out or evicted
// from the page cache.
//
// If the calling process has the CAP_IPC_LOCK capability, then memory
// locking is always permitted, without bound up to the physical
// limits of the machine. Be careful.
//
// For processes without CAP_IPC_LOCK, the upper bound is set by the
// RLIMIT_MEMLOCK ulimit entry. If the current process VmSize (see
// /proc/self/status) exceeds RLIMIT_MEMLOCK, then this call will fail
// and no memory will be locked. If the call succeeds, note that the
// upper bound established by RLIMIT_MEMLOCK continues to apply. If
// future allocations would push VmSize beyond RLIMIT_MEMLOCK, those
// allocations will fail. Note that this includes calls to sbrk and
// mmap.
//
// WARNING: Any mmap'ed region counts against VmSize, so it is
// recommended that you DO NOT USE this call in a process that is also
// using the MMapTransport. In the future, linux may offer a
// MAP_UNLOCKED flag to mmap which will allow us to work around this
// problem.

bool enableMemoryLocking();



// So enableMemoryLocking is a really big hammer, and it also can't be
// used in situations where a process has mmaped a large region (like
// when the MMapTransport is in play). The
// 'mlockEvictionThreatenedRegions' is far more selective. It only
// attempts to memory lock regions that meet the following criteria:
//
//  1) Are mapped 'private'
//  2) Are non-writable
//  3) Are backed by an inode
//
// These are exactly the pages that are most likely to be silently
// evicted from the page cache when free page pressure arises, since
// they can 'easily' be faulted back in from disk storage by simply
// re-reading the data.
//
// Note that calling this function will not mlock the heap, thread
// stacks, or anonymous memory mappings. However, none of those
// regions should ever incur major faults unless the process is
// subjected to swapping. If we are swapping (rather than evicting)
// then we have much bigger problems.
//
// This function operates by reading /proc/self/maps, so the memory
// locking that it applies will not affect future changes to the
// address space. If you anticipate that the address space will change
// to incorporate new read-only private file-backed mappings over
// time, then this function should be called periodically, or after
// known address space mutations.
//
// To protect against very large private file backed mappings, an
// upper bound is provided. The function will not attempt to mlock any
// individual region larger than this bound. By default, the bound is
// 32MB.
//
// The function returns no errors, but does log warnings for regions
// that exceed the upper bound, or for which the call to mlock fails.

void mlockEvictionThreatenedRegions( const size_t perRegionUpperBound = (1 << 25) );

} // namespace ProcUtils
} // namespace bb

#endif // BB_CORE_PROCUTILS_H

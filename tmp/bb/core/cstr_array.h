#ifndef BB_CORE_CSTR_ARRAY_H
#define BB_CORE_CSTR_ARRAY_H

/* Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved. */


#include <ostream>
#include <cstring>
#include <string>
#include <boost/array.hpp>
#include <bb/core/Error.h>

namespace bb
{

/// This is the datatype we stuff in our messages for char arrays.
/// See boost::array documentation for a little more info.
///
/// Char arrays in messages may not be null-terminated, be careful
/// when treating them like C strs. The simplest solution is to
/// always use the std::string variants.
template<std::size_t N>
class cstr_array : private boost::array<char, N>
{
public:
    typedef boost::array<char, N> Super;
    using typename Super::value_type;
    using typename Super::iterator;
    using typename Super::const_iterator;
    using typename Super::reverse_iterator;
    using typename Super::const_reverse_iterator;
    using typename Super::reference;
    using typename Super::const_reference;
    using typename Super::size_type;
    using typename Super::difference_type;

    using Super::static_size;
    using Super::max_size;
    using Super::operator[];
    using Super::begin;
    using Super::end;
    using Super::rbegin;
    using Super::rend;
    using Super::front;
    using Super::at;
    using Super::data;
    using Super::c_array;
    using Super::swap;
    using Super::assign;
    /// hides empty(), size(), back(), comparison operators,
    /// since those are ambiguous w.r.t. what to do about the null terminator.

    /// equivalent to (strlen() == 0) but constant time
    bool empty() const { return N <= 0 || front() == '\0'; }

    size_t strlen() const { return strnlen(data(), N); }
    /// Copys a cstr from src into the array.
    /// Like strncpy, will not null-terminate on overflow.
    void strncpy(const char *src) { ::strncpy(data(), src, N); }
    void strncpy(const std::string &src) { strncpy(src.data(), src.length()); }
    /// Copys at most m bytes from src. Will not null-terminate on overflow of N.
    void strncpy(const char *src, size_t m)
    {
        ::strncpy(data(), src, std::min(m, N));
        if(m < N) // null terminate unless we don't have room in our buffer
            data()[m] = '\0';
    }

    void memcpy(const char *src) { ::memcpy(data(), src, N); }
    void memcpy(const char *src, size_t n) { ::memcpy(data(), src, std::min(n, N)); }
    void memcpy_throw(const char *src, size_t n)
    {
        BB_THROW_EXASSERT(n <= N, "cstr_array::memcpy_throw: buffer length exceeded");
        memcpy(src, n);
    }

    /// variant on strncpy; throws if the buffer does not fit the string.
    /// If the string fits exactly, will not null-terminate.
    void strncpy_throw(const std::string &src)
    {
        BB_THROW_EXASSERT(src.length() <= N, "cstr_array::strncpy_throw: buffer length exceeded");
        strncpy(src);
    }
    void strncpy_throw(const char *src)
    {
        BB_THROW_EXASSERT(::strlen(src) <= N, "cstr_array::strncpy_throw: buffer length exceeded");
        strncpy(src);
    }

    std::string toString() const { return std::string(data(), strlen()); }
};

template<std::size_t N>
std::ostream &operator<<(std::ostream &os, const cstr_array<N> &ary)
{
    return os.write(ary.data(), ary.strlen());
}

} // namespace bb

#endif // BB_CORE_CSTR_ARRAY_H

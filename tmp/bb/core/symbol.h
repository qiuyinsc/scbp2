#ifndef BB_CORE_SYMBOL_H
#define BB_CORE_SYMBOL_H

/* Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved. */


/**
 * @file symbol.h
 *
 * A bunch of symbol database manipulation functions on a given symbol context.
 * It loads either a file or a list of sources from the database that contains
 * all the mappings.  The string -> symbol mapping is bijective for any given
 * source, but across different sources we now have the possibility of many-to-
 * one (e.g. multiple symbology representations of the same asset/symbol) or
 * one-to-many (e.g. same representations mapping to different assets due to
 * multiple exchanges).  Requests for unmapped symbols will yield NULLs, while
 * requests for unmapped strings will yield SYM_UNKNOWN, which has id = 0, and
 * prints as SYM_UNKNOWN.
 */

#include <iosfwd>

#include <bb/core/date.h>
#include <bb/core/hash.h>
#include <bb/core/mktdest.h>

namespace bb {

/// Lower limit of where our valid symbols begin
const uint32_t SYM_MIN = 100;

/// Upper limit on the number of symbols
/// Note: Changing this could affect deployed builds, specifically
///       class BaseFilterTransport::FilterGroup
const uint32_t SYM_MAX = 64000;

/// symbol_t is a unique security identifier.  Although symbol_t's may map to
/// multiple representations and certain ticker strings may map to multiple
/// symbol_t's, the implicit contract here is that for any given symbol context,
/// i.e. a collection of symbol mappings, there is a specific symbol_t -> string
/// mapping and a specific string -> symbol_t mapping.
class symbol_t
{
public:
    /// Constructs a symbol_t with the given symid.
    explicit symbol_t( uint32_t symid = 0 )                { m_symid = symid; }
    explicit symbol_t(const char* sz);
    explicit symbol_t(const std::string &s);

    bool operator ==(const symbol_t &sym) const { return m_symid == sym.m_symid; }
    bool operator !=(const symbol_t &sym) const { return m_symid != sym.m_symid; }
    bool operator < (const symbol_t &sym) const { return m_symid <  sym.m_symid; }

    /// Returns the symbol id of this symbol_t.
    uint32_t id() const                           { return m_symid; }

    /// Returns the symbol id of this symbol_t.
    operator uint32_t() const                     { return m_symid; }

    /// Returns true if this symbol_t has a known mapping.  Note that all
    /// symid's < SYM_MIN or > SYM_MAX are invalid.  This is a cheap operation.
    bool isValid() const;

    /// Converts this symbol_t to a string representation.
    /// If the symid isn't in the database, it returns NULL.
    const char* toString() const;

private:
    uint32_t m_symid;
};


// hashing function
inline std::size_t hash_value( const symbol_t& sym )
{
    boost::hash<uint32_t> hasher;
    return hasher( sym.id() );
}


#ifndef SWIG

/// Represents the resolution of a ticker that we don't have a mapping for.
const symbol_t SYM_UNKNOWN(uint32_t(0));

/// Represents the notion of no symbol involved, e.g. with broad a market
/// announcement or economic news.
const symbol_t SYM_NONE(uint32_t(1));

/// Represents the notion of all symbols.
const symbol_t SYM_ALL(uint32_t(2));

/// The smallest valid symbol.
const symbol_t SYM_MIN_VALID_ASSET(SYM_MIN);

/// The largest valid symbol.  Not necessarily the largest mapping in existence.
const symbol_t SYM_MAX_VALID_ASSET(SYM_MAX);

#endif

/// Converts a symbol_t to a string representation.  If the symbol_t isn't in
/// the context's mappings, it returns NULL.
const char* sym2str(symbol_t sym);
const char* sym2str(symbol_t sym, bool* ok);

/// Converts a string to a symbol_t representation.  If the ticker isn't in the
/// context's mappings, it returns SYM_UNKNOWN.
symbol_t str2sym(const char* str);

/// Friendly version of str2sym.  Resolves numeric strings as well, e.g.
/// str2sym_throw("3") will return symbol_t(3).
/// @throw if the context doesn't contain a mapping for the string or the
///        numerical form
symbol_t str2sym_throw(const char *str);
inline symbol_t str2sym_throw(const std::string& str) { return str2sym_throw(str.c_str()); }

inline symbol_t::symbol_t( const char* sz )   { m_symid = str2sym(sz).id(); }
inline symbol_t::symbol_t( const std::string &s ) { m_symid = str2sym(s.c_str()).id(); }
inline const char* symbol_t::toString() const { return sym2str(*this); }

/// Stream operator to convert symbol_t into human readable form.
std::ostream &operator <<(std::ostream &out, const symbol_t &s);
std::istream &operator >>(std::istream &in, symbol_t &s);

} // namespace bb

BB_HASH_NAMESPACE_BEGIN {
template<> struct hash<bb::symbol_t> : public int_hash<bb::symbol_t> { };
} BB_HASH_NAMESPACE_END


#endif // BB_CORE_SYMBOL_H

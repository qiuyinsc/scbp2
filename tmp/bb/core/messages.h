#ifndef BB_CORE_MESSAGES_H
#define BB_CORE_MESSAGES_H

/* Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved. */


#include <bb/core/messages_autogen.h>

/** \file Definitions of all the messages we pass around within the BB
    system.  This file is mostly a wrapper around the generated C++
    code by the message-gen-ch.rb script and messages.xml definition
    file. */

#endif // BB_CORE_MESSAGES_H

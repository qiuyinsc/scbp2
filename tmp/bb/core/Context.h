#ifndef BB_CORE_CONTEXT_H
#define BB_CORE_CONTEXT_H

/* Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved. */


#include <bb/core/hash_map.h>
#include <bb/core/smart_ptr.h>

namespace bb {

/// A dictionary of strings to shared_ptrs
class Context
{
public:
    /// Retrieves the object of type shared_ptr<T> named 'name'.
    /// The type must be compatible with the type passed to set<>!
    /// Returns NULL if the lookup is unsuccessful.
    template<typename T>
    boost::shared_ptr<T> get( const std::string& name ) const
    {
        StringToPtrMap::const_iterator iter = m_ptrMap.find( name );
        if ( iter != m_ptrMap.end() )
            return boost::static_pointer_cast<T>( iter->second );
        return boost::shared_ptr<T>(); // not found
    }

    /// Inserts the object of type shared_ptr<T> named 'name' into this Context.
    /// This will replace any object already existing in the Context.
    template<typename T>
    void set( const std::string& name, boost::shared_ptr<T> spT )
    {
        m_ptrMap[name] = boost::static_pointer_cast<void>( spT );
    }

private:
    typedef bbext::hash_map<std::string, boost::shared_ptr<void> > StringToPtrMap;
    StringToPtrMap m_ptrMap;
};
BB_DECLARE_SHARED_PTR( Context );

} // namespace bb

#endif // BB_CORE_CONTEXT_H

#ifndef BB_CORE_STATISTICSTREE_H
#define BB_CORE_STATISTICSTREE_H

/* Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved. */


#include <list>
#include <string>
#include <vector>

#include <boost/function.hpp>
#include <boost/make_shared.hpp>
#include <boost/noncopyable.hpp>

#include <bb/core/bbint.h>
#include <bb/core/smart_ptr.h>

namespace bb {
namespace statistics {

class Visitor;

// The abstract node type for the tree, defining the abstract
// node visitation hooks.
BB_FWD_DECLARE_SHARED_PTR( Node );
class Node : boost::noncopyable
{
public:
    virtual ~Node();

    // Accept the provided visitor and call back its 'visit' method on
    // *this. Subclasses of 'Node' must implement this as:
    //   'visitor.visit( *this )'
    virtual void accept( Visitor& visitor ) const = 0;

protected:
    inline Node()
        : boost::noncopyable() {}
};


// A base class for nodes which may be searched for children by name.
BB_FWD_DECLARE_SHARED_PTR( SearchableNode );
class SearchableNode : public Node
{
public:
    virtual ~SearchableNode();

    // Return a non-empty shared pointer to the named child node if it
    // exists, or an empty shared pointer if it does not.
    virtual NodePtr find( const std::string& ) = 0;

    // As 'find' above, but returns a const Node.
    virtual NodeCPtr find( const std::string& ) const = 0;

protected:
    inline SearchableNode()
        : Node() {}
};

// VectorNode is a tree node which contains an ordered sequence of
// child nodes. New nodes may be pushed back, but once added a child
// node may not be removed. VectorNodes are primarily useful for
// returning tuples (think x,y,z coordinates) as a single statistic.
BB_FWD_DECLARE_SHARED_PTR( VectorNode );
class VectorNode : public SearchableNode
{
public:
    typedef boost::function<void( Node&, bool )> ChildHandler;
    typedef boost::function<void( const Node&, bool )> ConstChildHandler;

    // Create a new vector node
    static inline VectorNodePtr create()
    {
        return boost::make_shared< VectorNode >();
    }

    inline VectorNode()
        : SearchableNode()
        , m_members() {}

    virtual ~VectorNode();

    // Visitor hook.
    virtual void accept( Visitor& visitor ) const;

    // Add the given node to the end of this vector.
    void push_back( const NodePtr& node );

    // Return a non-empty shared pointer to the node at index 'index'
    // if it exists in this vector, or an empty shared pointer if
    // it does not.
    NodePtr find( size_t index );
    virtual NodePtr find( const std::string& index );

    // As 'find' above, but returns a const Node.
    NodeCPtr find( size_t index ) const;
    virtual NodeCPtr find( const std::string& index ) const;

    // The number of elements in this namespace.
    inline size_t size() const
    {
        return m_members.size();
    }

    // True iff size() == 0
    inline bool empty() const
    {
        return m_members.empty();
    }

    // Call the provided callback once for each child. The second
    // 'bool' argument to the callback will be 'true' for all but the
    // last element.
    void applyToChildren( const ChildHandler& callback );

    // Call the provided callback once for each child. The second
    // 'bool' argument to the callback will be 'true' for all but the
    // last element.
    void applyToChildren( const ConstChildHandler& callback ) const;

private:
    typedef std::vector< NodePtr > member_vec;
    member_vec m_members;
};

// A BaseDictionaryNode is a tree node which contains a map of names
// to nodes. The named nodes are considered children of the node. This
// class is used as a base class for DictionaryNode and
// AutoIndexNode so they can share implementation and is not
// intended for direct use.
BB_FWD_DECLARE_SHARED_PTR( BaseDictionaryNode );
class BaseDictionaryNode : public SearchableNode
{
public:
    typedef boost::function<void( const std::string&, Node&, bool )> ChildHandler;
    typedef boost::function<void( const std::string&, const Node&, bool )> ConstChildHandler;

    virtual ~BaseDictionaryNode();

    // Visitor hook.
    virtual void accept( Visitor& visitor ) const;

    // Return a non-empty shared pointer to the node named 'name' if
    // it exists in this namespace, or an empty shared pointer if it
    // does not.
    virtual NodePtr find( const std::string& name );

    // As 'find' above, but returns a const Node.
    virtual NodeCPtr find( const std::string& name ) const;

    // Remove and return the node named 'name', if it exists, or
    // return an empty shared pointer if it does not. Note: this is
    // somewhat expensive, as it is not expected to be used frequently.
    NodePtr remove( const std::string& name );

    // Remove all children from this node. This is much more efficient
    // than removing each element by name or node pointer.
    void clear();

    // The number of elements in this namespace.
    inline size_t size() const
    {
        return m_children.size();
    }

    // True iff size() == 0
    inline bool empty() const
    {
        return m_children.empty();
    }

    // Call the provided callback once for each child. The third
    // 'bool' argument to the callback will be 'true' for all but the
    // last element.
    void applyToChildren( const ChildHandler& callback );

    // Call the provided callback once for each child. The third
    // 'bool' argument to the callback will be 'true' for all but the
    // last element.
    void applyToChildren( const ConstChildHandler& callback ) const;

protected:
    inline BaseDictionaryNode()
        : SearchableNode()
        , m_children()
        , m_lookupTable() {}

    // Try to insert the provided non-null node under 'name'. If the
    // given node pointer is null, returns false. If the name already
    // exists, the node is not inserted and this method returns
    // false. If the provided node is a namespace node, its
    // descendants must not include the current node, as this would
    // create a cycle. This may or not be checked.
    bool insert( const std::string& name, const NodePtr& node );

private:
    struct NodeInfo
    {
        inline NodeInfo( const std::string& _name, const NodePtr& _node )
            : name(_name), node(_node) {}

        std::string name;
        NodePtr     node;
    };

    typedef std::list<NodeInfo> node_list_t;
    typedef std::vector<node_list_t::iterator> nodeiter_vec_t;

    node_list_t::iterator internal_find(
        const std::string& name,
        nodeiter_vec_t::iterator* piterwhere );

    node_list_t::const_iterator internal_find( const std::string& name ) const;

    void sortLookupTable();

    static inline bool sortLookupTablePredicate(
        const node_list_t::iterator left,
        const node_list_t::iterator right )
    {
        return left->name < right->name;
    }

    static inline bool searchLookupTablePredicate(
        const node_list_t::iterator element,
        const std::string& name )
    {
        return element->name < name;
    }

    node_list_t m_children;
    nodeiter_vec_t m_lookupTable;
};


// A DictionaryNode is a container which maps names to subelements. It simply exports
// BaseDictionaryNode's protected insert method.
BB_FWD_DECLARE_SHARED_PTR( DictionaryNode );
class DictionaryNode : public BaseDictionaryNode
{
public:
    static inline DictionaryNodePtr create()
    {
        return boost::make_shared< DictionaryNode >();
    }

    inline DictionaryNode()
        : BaseDictionaryNode() {}

    virtual ~DictionaryNode();

    inline bool insert( const std::string& name, const NodePtr& node )
    {
        return BaseDictionaryNode::insert( name, node );
    }
};

// An AutoIndex node is a container node which assigns a new
// monotonically increasing id to each element inserted. Elements may
// be removed by the ID returned by 'insert'.
BB_FWD_DECLARE_SHARED_PTR( AutoIndexNode );
class AutoIndexNode : public BaseDictionaryNode
{
public:

    // Create a new namespace node
    static inline AutoIndexNodePtr create()
    {
        return boost::make_shared< AutoIndexNode >();
    }

    inline AutoIndexNode()
        : BaseDictionaryNode()
        , m_nextIndex( 0 ) {}

    virtual ~AutoIndexNode();

    // Insert the given node into this container and return the index
    // it was assigned.
    size_t insert( const NodePtr& node );

    // Remove a node by index. If no node with that index exists, it
    // returns an empty NodePtr.
    NodePtr remove( size_t index );

private:
    size_t m_nextIndex;
};

template<typename T>
class ValueProviderNode;

// A read-only visitor. If you want to extend the tree with new types
// of values, you must add new functions here, then implement them in
// all visitor subclasses.
class Visitor : boost::noncopyable
{
public:
    inline Visitor() {}
    virtual ~Visitor();

    virtual void visit( const VectorNode& node );
    virtual void visit( const BaseDictionaryNode& node );
    virtual void visit( const ValueProviderNode<std::string>& node );
    virtual void visit( const ValueProviderNode<int32_t>& node );
    virtual void visit( const ValueProviderNode<int64_t>& node );
    virtual void visit( const ValueProviderNode<uint32_t>& node );
    virtual void visit( const ValueProviderNode<uint64_t>& node );
    virtual void visit( const ValueProviderNode<bool>& node );
    virtual void visit( const ValueProviderNode<float>& node );
    virtual void visit( const ValueProviderNode<double>& node );
    virtual void visit( const ValueProviderNode<void*>& node );
};

// A value node in the tree represents the ability to extract a value.
template<typename T>
class ValueProviderNode : public Node
{
public:
    virtual ~ValueProviderNode() {}

    // Visitor hook.
    virtual void accept( Visitor& visitor ) const
    {
        visitor.visit( *this );
    }

    // Return the current value of this node.
    virtual T value() const = 0;

protected:
    inline ValueProviderNode()
        : Node() {}
};

// A 'constant' node contains an immutable value, which it always
// returns as the result of its 'value' method.
template<typename T>
class ConstantNode : public ValueProviderNode<T>
{
public:
    // Create a new constant node which will return a copy of 'value'.
    static inline bb::shared_ptr< ConstantNode > create( const T& constant )
    {
        return boost::make_shared< ConstantNode >( constant );
    }

    inline ConstantNode( const T& constant )
        : ValueProviderNode<T>()
        , m_constant( constant ) {}

    virtual ~ConstantNode() {}

    // Return the current value of this node.
    virtual T value() const
    {
        return m_constant;
    }

private:
    const T m_constant;
};

// A 'pointer' node dereferences a user provided pointer to determine
// the value to return.
template<typename T>
class PointerNode : public ValueProviderNode<T>
{
public:
    // Create a new pointer node which will dereference the provided pointer
    // when asked for its value.
    static inline bb::shared_ptr< PointerNode > create( const T* const & pointer )
    {
        return boost::make_shared< PointerNode >( pointer );
    }

    inline PointerNode( const T* pointer )
        : ValueProviderNode<T>()
        , m_pointer( pointer ) {}

    virtual ~PointerNode() {}

    // Return the current value of this node.
    virtual T value() const
    {
        return *m_pointer;
    }

private:
    const T* const m_pointer;
};

// A 'function' node invokes a user-provided callback to determine the
// value to return.
template<typename T>
class FunctionNode : public ValueProviderNode<T>
{
public:
    typedef boost::function<T()> Function;

    // Create a new function node which will invoke the provided
    // function when asked for its value.
    static inline bb::shared_ptr< FunctionNode > create( const Function& function )
    {
        return boost::make_shared< FunctionNode >( function );
    }

    inline FunctionNode( const Function& function )
        : ValueProviderNode<T>()
        , m_function( function ) {}

    virtual ~FunctionNode() {}

    // Return the current value of this node.
    virtual T value() const
    {
        return m_function();
    }

private:
    const Function m_function;
};


class StatBuilder
{
public:
    inline StatBuilder( const std::string& name )
        : m_name( name )
        , m_root( DictionaryNode::create() )
        , m_values( VectorNode::create() )
        , m_meta( DictionaryNode::create() )
    {
        m_root->insert( kValuesKey, m_values );
        m_root->insert( kMetaKey, m_meta );
    }

    template<typename T>
    inline StatBuilder& addConstant( const T& constant )
    {
        m_values->push_back( ConstantNode<T>::create( constant ) );
        m_meta->insert(
            kConstantKey,
            ConstantNode<bool>::create( true ) );
        return *this;
    }

    template<typename T>
    inline StatBuilder& addPointer( const T* pointer )
    {
        m_values->push_back( PointerNode<T>::create( pointer ) );
        return *this;
    }

    template<typename T>
    inline StatBuilder& addFunction( const typename FunctionNode<T>::Function& function )
    {
        m_values->push_back( FunctionNode<T>::create( function ) );
        return *this;
    }

    inline StatBuilder& setDescription( const std::string& description )
    {
        return addMetaData( kMetaDescriptionKey, description );
    }

    inline StatBuilder& addMetaData( const std::string& key, const std::string& value )
    {
        m_meta->insert( key, ConstantNode<std::string>::create( value ) );
        return *this;
    }

    inline const std::string& getName() const
    {
        return m_name;
    }

    inline const DictionaryNodePtr& getTree() const
    {
        return m_root;
    }

    inline ~StatBuilder() {}

private:
    static const char kValuesKey[];
    static const char kMetaKey[];
    static const char kConstantKey[];
    static const char kMetaDescriptionKey[];

    const std::string m_name;
    const DictionaryNodePtr m_root;
    const VectorNodePtr m_values;
    const DictionaryNodePtr m_meta;
};


// The head of a tree of statistics nodes; the root node is a
// DictionaryNode.
BB_FWD_DECLARE_SHARED_PTR( Tree );
class Tree : boost::noncopyable
{
public:
    Tree();
    ~Tree();

    inline DictionaryNodeCPtr root() const
    {
        return m_root;
    }

    const DictionaryNodePtr& mutable_root()
    {
        return m_root;
    }

    NodePtr find( const std::string& path );
    NodeCPtr find( const std::string& path ) const;

private:
    const DictionaryNodePtr m_root;
};

} // namespace statistics
} // namespace bb

#endif // BB_CORE_STATISTICSTREE_H

#ifndef BB_CORE_NETWORK_H
#define BB_CORE_NETWORK_H

/* Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved. */


/** \file Most basic networking functionality.  Stuff here should be
separated from the main C and C++ libraries. */

#include <cstdio>
#include <string>
#include <net/if_arp.h>
#include <netinet/in.h>
#include <sys/socket.h>
#include <netdb.h>

#include <bb/core/compat.h>
#include <bb/core/compat_net.h>
#include <bb/core/CError.h>
#include <bb/core/timeval.h>

// Add code to handle older version of the kernel
#include <linux/version.h>
#include <linux/net_tstamp.h>

#ifndef SOF_TIMESTAMPING_RX_ANY
#define SOF_TIMESTAMPING_RX_ANY ( SOF_TIMESTAMPING_RX_HARDWARE | SOF_TIMESTAMPING_RAW_HARDWARE | SOF_TIMESTAMPING_SYS_HARDWARE )
#endif

namespace bb {

/// Wrapper for MAC addresses. See https://en.wikipedia.org/wiki/MAC_address
class mac_addr_t {
public:
        mac_addr_t();
        mac_addr_t(struct ifreq const & rsp);
        mac_addr_t(struct arpreq const & rsp);
        mac_addr_t(const char * input_str);

        bool empty() const;
        std::string toString() const;

        bool operator == ( mac_addr_t const & other ) const;
        bool operator != ( mac_addr_t const & other ) const;

private:
        unsigned char m_contents[6];
};
std::ostream &operator<<(std::ostream &out, const mac_addr_t &addr);

/// Wrapper for Berkeley Sockets in_addr. Represents an IPv4 IP address.
class ipv4_addr_t {
public:
    ipv4_addr_t() { bzero(&m_raw, sizeof(m_raw)); }
    ipv4_addr_t(const in_addr &addr) : m_raw(addr) {}
    static ipv4_addr_t inaddr_any();
    static ipv4_addr_t from_ip(const char *ipaddr); // from IP string. see also lookuphost
    static ipv4_addr_t from_ip(const std::string& ipaddr) { return from_ip( ipaddr.c_str() ); }
    static ipv4_addr_t localhost();

    std::string toString() const { return raw_to_string(m_raw); }
    bool isMulticast() const;

    const in_addr &raw() const { return m_raw; }
    in_addr &raw() { return m_raw; }
    static std::string raw_to_string(const in_addr &raw);

protected:
    ipv4_addr_t(uint32_t s_addr) { m_raw.s_addr = s_addr; }

    in_addr m_raw;
};

std::ostream &operator<<(std::ostream &out, const ipv4_addr_t &addr);

/// Wrapper for Berkeley Sockets sockaddr_in. Represents an IPv4 address & port pair.
class sockaddr_ipv4_t {
public:
    sockaddr_ipv4_t() { bzero(&m_raw, sizeof(m_raw)); }
    sockaddr_ipv4_t(const sockaddr_in &addr) : m_raw(addr) {}
    sockaddr_ipv4_t(const ipv4_addr_t &ipaddr, int port);
    static sockaddr_ipv4_t inaddr_any(int port); // Represents any IP, given port
    static sockaddr_ipv4_t port_any(const ipv4_addr_t &addr); // Represents any port, given IP

    std::string ipAddrStr() const { return ipv4_addr_t::raw_to_string(m_raw.sin_addr); }
    std::string const & host() const;
    ipv4_addr_t ipAddr() const { return ipv4_addr_t(m_raw.sin_addr); }
    int port() const { return ntohs(m_raw.sin_port); }
    bool empty() const { return ntohl(m_raw.sin_addr.s_addr) == 0 && port() == 0; }
    bool operator == ( sockaddr_ipv4_t const & other ) const
    {
        return
            other.m_raw.sin_family == m_raw.sin_family &&
            other.m_raw.sin_port == m_raw.sin_port &&
            other.m_raw.sin_addr.s_addr == m_raw.sin_addr.s_addr;
    }

    const sockaddr_in &raw() const { return m_raw; }
    sockaddr_in &raw() { return m_raw; }

    std::string toString() const;

protected:
    struct sockaddr_in m_raw;
};

std::ostream &operator<<(std::ostream &out, const sockaddr_ipv4_t &addr);

bool operator <( const sockaddr_ipv4_t &lhs, const sockaddr_ipv4_t &rhs );

/// returns the fully qualified domain name
/// called by core_config.lua, so the (potentially blocking) DNS lookup is done once during program startup
const std::string& getFQDN();

/// Hardware timestamping of the time a network diagram is received. It's advised to use a Solarflare card
/// for this, with onload enabled for accuracy. This is even required for TCP data, which will not work
/// otherwise.

/// Set hardware received time stamping on/off
bool toggleHwRecvTimestamping( const int sockfd, const bool enabled );

/// Set hardware received time stamping on/off
bool toggleHwSendTimestamping( const int sockfd, const bool enabled );

/// returns the timestamp for a given message as a timeval
bb::timeval_t getMsgHwTimestamp( struct ::msghdr* msg );

struct StreamTimestamps{
    bb::timeval_t first;
    bb::timeval_t last;
};

/// returns the timestamp for a given message as a timeval.
// This depends on the solarflare API associated with socket options ONLOAD_SOF_TIMESTAMPING_STREAM 
// refer to openonload src/tests/onload/hwtimestamping/tx_timestamping.c
// need to poll using recvmsg(..., MSG_ERRQUEUE ) until returns >= 0, ex:
//   do {
//       msg.msg_controllen = 1024;
//       got = recvmsg(sock, &msg, MSG_ERRQUEUE);
//   } while (got < 0 && errno == EAGAIN);
// 
// My first try with this took around 500 microseconds to get the ERRQUEUE, but I believe
// the problem could have been that the socket was non-blocking.
//
// USE WITH CAUTION: read above.
StreamTimestamps getMsgStreamTXHwTimestamp( struct ::msghdr* msg );

/// Blocking DNS lookup functions. These methods block the caller, so
/// they are not appropriate to use in an event driven context. See
/// core/IDNSResolver.hpp for details on how to perform DNS
/// operations in a non-blocking way.

/// returns the fully qualified domain name
std::string getFQDN_blocking(const char* hostname); // does DNS lookup on hostname

inline std::string getFQDN_blocking(const std::string& hostname)
{
    return getFQDN_blocking(hostname.c_str());
}

/// Returns the first available listing for HOST. Throws NetDBError on a DNS error.
ipv4_addr_t lookuphost_blocking(const char *host);

inline ipv4_addr_t lookuphost_blocking(const std::string &host)
{
    return lookuphost_blocking(host.c_str());
}

inline sockaddr_ipv4_t lookupaddr_blocking(const char *host, int port)
{
    return sockaddr_ipv4_t(lookuphost_blocking(host), port);
}

inline sockaddr_ipv4_t lookupaddr_blocking(const std::string& host, int port)
{
    return lookupaddr_blocking(host.c_str(), port);
}

/// DNS failure
struct NetDBError : public CError
{
    NetDBError(int err, const char *hostname);
    virtual ~NetDBError() throw() {}
    std::string m_hostname;
};

inline ipv4_addr_t ipv4_addr_t::inaddr_any()
{
    return ipv4_addr_t(htonl(INADDR_ANY));
}

inline ipv4_addr_t ipv4_addr_t::localhost()
{
    return ipv4_addr_t(htonl(INADDR_LOOPBACK));
}

inline sockaddr_ipv4_t::sockaddr_ipv4_t(const ipv4_addr_t &ipaddr, int port)
{
    bzero(&m_raw, sizeof(m_raw));
    m_raw.sin_family = AF_INET;
    m_raw.sin_addr = ipaddr.raw();
    m_raw.sin_port = htons(port);
}

inline sockaddr_ipv4_t sockaddr_ipv4_t::inaddr_any(int port)
{
    sockaddr_ipv4_t ret;
    ret.m_raw.sin_family = AF_INET;
    ret.m_raw.sin_addr.s_addr = htonl(INADDR_ANY);
    ret.m_raw.sin_port = htons(port);
    return ret;
}

inline sockaddr_ipv4_t sockaddr_ipv4_t::port_any(const ipv4_addr_t &addr)
{
    sockaddr_ipv4_t ret;
    ret.m_raw.sin_family = AF_INET;
    ret.m_raw.sin_addr = addr.raw();
    ret.m_raw.sin_port = 0;
    return ret;
}

} // namespace bb

#endif // BB_CORE_NETWORK_H

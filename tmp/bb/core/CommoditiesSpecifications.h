#ifndef BB_CORE_COMMODITIESSPECIFICATIONS_H
#define BB_CORE_COMMODITIESSPECIFICATIONS_H

/* Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved. */

#include <list>
#include <iosfwd>
#include <iostream>
#include <boost/optional.hpp>

#include <bb/core/hash_map.h>
#include <bb/core/instrument.h>
#include <bb/core/smart_ptr.h>

namespace bb {

class CommoditySpecification
{
public:
    CommoditySpecification( const instrument_t& instr, mktdest_t exchange, uint32_t contract_size, double tick_size,
                            double tick_value, double pricing_unit, currency_t currency, const expiry_t& start_expiry,
                            const expiry_t& end_expiry, double volume_mult, const date_t& start_date, const date_t& end_date,
                            const std::string& contract_listings )
        : m_instr( instr )
        , m_start_expiry( start_expiry )
        , m_end_expiry( end_expiry )
        , m_exchange( exchange )
        , m_contract_size( contract_size )
        , m_tick_size( tick_size )
        , m_tick_value( tick_value )
        , m_volume_mult( volume_mult )
        , m_pricing_unit( pricing_unit )
        , m_start_date( start_date )
        , m_end_date( end_date )
        , m_contract_listings( contract_listings )
    {
    }

    const instrument_t& getInstrument() const
    {
        return m_instr;
    }

    date_t getStartDate() const
    {
        return m_start_date;
    }

    date_t getEndDate() const
    {
        return m_end_date;
    }


    expiry_t getStartExpiry() const
    {
        return m_start_expiry;
    }

    expiry_t getEndExpiry() const
    {
        return m_end_expiry;
    }

    symbol_t getSymbol() const
    {
        return m_instr.getSymbol();
    }

    mktdest_t getExchange() const
    {
        return m_exchange;
    }

    uint32_t getContractSize() const
    {
        return m_contract_size;
    }

    double getTickSize() const
    {
        return m_tick_size;
    }

    double getTickValue() const
    {
        return m_tick_value;
    }

    double getVolumeMultiplier() const
    {
        return m_volume_mult;
    }

    double getPricingUnit() const
    {
        return m_pricing_unit;
    }

    const std::string& getContractListings() const
    {
        return m_contract_listings;
    }

private:
    /// Instrument that this contract specification is for
    instrument_t m_instr;

    /// Start expiry that this specification is valid for
    expiry_t m_start_expiry;

    /// End expiry that this specification is valid for
    expiry_t m_end_expiry;

    /// Market that the contract is listed on
    mktdest_t m_exchange;

    /// Gets the contract_size
    uint32_t m_contract_size;

    /// Tick size for the contract
    double m_tick_size;

    /// Tick value for the contract (tick size * contract size)
    double m_tick_value;

    /// Multiplier for volume field. Some exchanges report double of actual volume
    double m_volume_mult;

    /// Pricing Unit of the contract
    double m_pricing_unit;

    /// Start date that this specification is valid for
    date_t m_start_date;

    /// End date that this specification is valid for
    date_t m_end_date;

    // Delivery months
    std::string m_contract_listings;
};

std::ostream& operator<<( std::ostream&, const CommoditySpecification& );

class CommoditySpecificationsList
{
public:
    typedef std::list<CommoditySpecification> container_type;
    typedef container_type::iterator iterator;
    typedef container_type::const_iterator const_iterator;

public:
    void push_back( const CommoditySpecification& spec )
    {
        m_specifications.push_back( spec );
    }

    iterator begin()
    {
        return m_specifications.begin();
    }

    const_iterator begin() const
    {
        return m_specifications.begin();
    }

    iterator end()
    {
        return m_specifications.end();
    }

    const_iterator end() const
    {
        return m_specifications.end();
    }

    iterator find( const expiry_t& expiry )
    {
        for( iterator it = m_specifications.begin(), end = m_specifications.end(); it != end; ++it )
        {
            if( expiry >= it->getStartExpiry() && expiry <= it->getEndExpiry() )
                return it;
        }
        return m_specifications.end();
    }

    const_iterator find( const expiry_t& expiry ) const
    {
        for( const_iterator it = m_specifications.begin(), end = m_specifications.end(); it != end; ++it )
        {
            if( expiry >= it->getStartExpiry() && expiry <= it->getEndExpiry() )
                return it;
        }
        return m_specifications.end();
    }

    iterator findWithTradingDay( const expiry_t& expiry, const date_t& trading_date = date_t::today() )
    {
        for( iterator it = m_specifications.begin(), end = m_specifications.end(); it != end; ++it )
        {
            if( expiry >= it->getStartExpiry() && expiry <= it->getEndExpiry() &&
                trading_date >= it->getStartDate() && trading_date <= it->getEndDate() )
                return it;
        }
        return m_specifications.end();
    }

    const_iterator findWithTradingDay( const expiry_t& expiry, const date_t& trading_date = date_t::today() ) const
    {
        for( const_iterator it = m_specifications.begin(), end = m_specifications.end(); it != end; ++it )
        {
            if( expiry >= it->getStartExpiry() && expiry <= it->getEndExpiry() &&
                trading_date >= it->getStartDate() && trading_date <= it->getEndDate() )
                return it;
        }
        return m_specifications.end();
    }

private:
    container_type m_specifications;
};


class ICommoditiesSpecificationsMap
{
public:
    typedef bbext::hash_map<symbol_t, CommoditySpecificationsList> container_type;
    typedef container_type::const_iterator const_iterator;
    typedef container_type::key_type key_type;

public:
    /// Destructor
    virtual ~ICommoditiesSpecificationsMap() { }

    /// Returns an iterator to the beginning of the container
    virtual const_iterator begin() const = 0;

    /// Returns an iterator to the end of the container
    virtual const_iterator end() const = 0;

    // Returns an iterator to the object or end if not found
    virtual const_iterator find( const key_type& key ) const = 0;

    // Searches the CommoditiesSpecificationMap ( by symbol_t ) and then the CommoditySpecificationList ( expiry_t ).
    // Cannot name this 'find' due to the compiler being unable to distinguish it from the symbol_t param passed into
    // the above 'find' function.
    virtual boost::optional<CommoditySpecification> findByInstrument( const instrument_t& instrument ) const = 0;

    bool exists( const key_type& key ) const
    {
        return find( key ) != end();
    }
};

BB_DECLARE_SHARED_PTR(ICommoditiesSpecificationsMap);

}

#endif // BB_CORE_COMMODITIESSPECIFICATIONS_H

#ifndef BB_CORE_FUNCTION_UTIL_H
#define BB_CORE_FUNCTION_UTIL_H

/* Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved. */


#include <boost/function.hpp>
#include <bb/core/compat.h> // SRC_LOCATION

namespace bb {

/// Wraps a boost::function so that it catches & logs all exceptions.
boost::function<void ()> withCatchAll(const char *srcLocation,
        const std::string &errPrefix, const boost::function<void ()> &fn);

} // namespace bb

#endif // BB_CORE_FUNCTION_UTIL_H

#ifndef BB_CORE_PRICESIZE_H
#define BB_CORE_PRICESIZE_H

/* Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved. */

#include <bb/core/mktdest.h>
#include <bb/core/fpmath.h>

namespace bb {

/// Price and Size information in a handy bite-sized chunk.
class PriceSize
{
public:
    /// Defaults to 0
    PriceSize()
        : m_px(0.0), m_sz(0)
    {}
    PriceSize( double px_, int32_t sz_ )
        : m_px( px_ ), m_sz( sz_ )
    {}

    void setPrice( double px_ )         { m_px = px_; }
    void setSize( int32_t sz_ )           { m_sz = sz_; }

    void set( double px_, int32_t sz_ )   { m_px = px_;  m_sz = sz_; }
    void get( double& px_, int32_t& sz_ ) const { px_ = m_px;  sz_ = m_sz; }

    double getPrice() const             { return m_px; }
    int32_t getSize() const               { return m_sz; }

    double px() const                   { return m_px; }
    int32_t sz() const                    { return m_sz; }

    void reset() { m_px = 0.0; m_sz = 0; }

    /// Returns true if all values are 0.
    bool empty() const                  { return (m_sz == 0) && bb::EQ(m_px, 0.0); }
    bool full() const                   { return (m_sz != 0) && bb::NE(m_px, 0.0); }

    bool operator ==( const PriceSize& rhs ) const
    {   return (m_sz == rhs.m_sz) && bb::EQ(m_px, rhs.m_px); }

    bool operator !=( const PriceSize& rhs ) const
    {   return !(*this == rhs); }

    friend std::ostream& operator<<(std::ostream&, PriceSize const&);
private:
    double  m_px;
    int32_t m_sz;
};

/// An extension to PriceSize that adds a mktdest_t. Note that
/// PriceSize intentionally does not have a virtual dtor, so don't
/// delete a PriceSizeMarket via a base class pointer.
class PriceSizeMarket : public PriceSize
{
public:
    PriceSizeMarket()
        : PriceSize()
        , m_market( MKT_UNKNOWN ) {}

    PriceSizeMarket( const PriceSize& ps, mktdest_t market )
        : PriceSize( ps )
        , m_market( market ) {}

    PriceSizeMarket( double px, int32_t sz, mktdest_t market )
        : PriceSize( px, sz )
        , m_market( market ) {}

    mktdest_t getMarket() const
    {
        return m_market;
    }

    void setMarket( mktdest_t market )
    {
        m_market = market;
    }

    void reset()
    {
        PriceSize::reset();
        m_market = MKT_UNKNOWN;
    }

private:
    mktdest_t m_market;
};

}
#endif // BB_CORE_PRICESIZE_H

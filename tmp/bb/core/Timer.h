#ifndef BB_CORE_TIMER_H
#define BB_CORE_TIMER_H

/* Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved. */

#include <sys/timerfd.h>
#include <bb/core/FD.h>

#include <boost/noncopyable.hpp>
#include <boost/function.hpp>

#include <bb/core/timeval.h>
#include <bb/core/ptime.h>
#include <bb/core/smart_ptr.h>
#include <bb/core/FDSetFwd.h>
#include <bb/core/Subscription.h>

namespace bb {

struct OverrunCounter : private boost::noncopyable
{
    uint64_t overruns;
};
BB_DECLARE_SHARED_PTR(OverrunCounter);

/// POSIX timer wrapper
/// Raises a signal after a specified time, with optional periodic signaling afterward
class Timer : public boost::noncopyable
{
public:
    typedef boost::function<void()> TimerCallback;

    // Create a timer associated and associate the provided callback
    // with the timer via the FDSet.
    Timer(FDSet& fdSet, const TimerCallback& cb, OverrunCounterPtr counter = OverrunCounterPtr());

    // Schedules a timer to occur "next" from now, and then
    // once every "interval" from then on.
    // If next == bb::timeval_t(), the timer will be disabled.
    // If interval == bb::timeval_t(), the timer will be one-shot.
    // If absolute == true, next will be interpreted as an absolute
    //    time (default is relative to now).
    void reset(const bb::timeval_t& next     = bb::timeval_t(),
               const bb::timeval_t& interval = bb::timeval_t(),
               bool absolute = false);

    // Preferred method which does not use timeval_t for relative time.
    void reset(const timeval_t& next,
               const ptime_duration_t& interval,
               bool absolute = false);

    bool isSet() const;

    // a value of >1 means we're overrunning
    int getoverrun() const;
    // will tell us how often we've overrun
    uint64_t getOverrunCnt() const;
    void resetOverrunCnt();
private:
    void onTimerReadable(const TimerCallback& cb);

    FD m_timerfd;
    uint64_t m_overrun;
    OverrunCounterPtr m_overrunCnt;
    Subscription m_timerSub; // only used with FDSet constructor
};
BB_DECLARE_SHARED_PTR(Timer);

} // namespace bb

#endif // BB_CORE_TIMER_H

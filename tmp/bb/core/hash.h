#ifndef BB_CORE_HASH_H
#define BB_CORE_HASH_H

/* Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved. */


#ifndef WIN32
#include <tr1/functional>
#endif

#include <utility>
#include <cstring>

#include <boost/functional/hash.hpp>

#define BB_HASH_NAMESPACE_BEGIN namespace std { namespace tr1
#define BB_HASH_NAMESPACE_END }

// define some useful hash values
BB_HASH_NAMESPACE_BEGIN {

template<class T>
struct int_hash
{
    size_t operator()( const T& t ) const
    {
        return boost::hash_value( static_cast<int>( t ) );
    }
};

template<class A, class B>
struct pair_hash
{
    size_t operator()( const std::pair<A, B>& p ) const
    {
        return boost::hash_value( p );
    }
};

struct cstr_hash
{
    size_t operator()( const char* s ) const
    {
        size_t seed = 0;
        for( ; *s != '\0'; ++s )
            boost::hash_combine( seed, *s );
        return seed;
    }
};

///
///  Hash function for strings that provides good
///  performace for short strings without using hash_combine
struct cstr_quick_hash
{
    size_t operator()( const char* s ) const
    {
        size_t seed = 0;
        for( ; *s != '\0'; ++s )
            seed = seed*101 + *s;
        return seed;
    }
};

} BB_HASH_NAMESPACE_END

namespace bb{

struct bb_eqstr
{
    bool operator()( const char* s1, const char* s2 ) const
    {
        if( s1 == NULL && s2 == NULL )
            return true;
        if( ( s1 == NULL && s2 != NULL ) ||
            ( s1 != NULL && s2 == NULL ) )
            return false;
        return strcmp( s1, s2 ) == 0;
    }
};

}
#endif // BB_CORE_HASH_H

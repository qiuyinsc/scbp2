#ifndef BB_CORE_COMPAT_NET_H
#define BB_CORE_COMPAT_NET_H

/* Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved. */


#if defined( WIN32 ) || defined( WIN64 )
#    include <bb/core/compat_win.h>
#elif defined( __APPLE__ ) && defined( __MACH__ )
#   include <bb/core/compat_macosx.h>
#else
#   include <bb/core/compat_net_linux.h>
#endif

#endif // BB_CORE_COMPAT_NET_H

#ifndef BB_CORE_SORTED_LIST_H
#define BB_CORE_SORTED_LIST_H

/* Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved. */


#include <bb/core/bbassert.h>

#include <list>
#include <algorithm>


namespace bb {


/// sorted_list<T, CMP> is a list that maintains its elements sorted in ascending
/// order by the CMP->operator()(T, T) function, returns true if arg1 < arg2
/// Note: Remember that in the cmp function, arg1 is the old element and arg2 is the new element
/// In following method list, if a method has no documentation it behaves the same
/// as the std::list method of same name
template<typename T, typename CMP>
class sorted_list {
public:
    sorted_list(const CMP &cmp = CMP()) : m_size(0), m_compare(cmp) {}
    // Notice destructor doesn't call delete on the cmpobj pointer
    ~sorted_list() { }

    typedef typename std::list<T>::iterator iterator;
    iterator begin() { return(l.begin()); }
    iterator end() { return(l.end()); }

    typedef typename std::list<T>::reverse_iterator reverse_iterator;
    reverse_iterator rbegin() { return(l.rbegin()); }
    reverse_iterator rend() { return(l.rend()); }

    typedef typename std::list<T>::const_iterator const_iterator;
    const_iterator begin() const { return(l.begin()); }
    const_iterator end() const   { return(l.end()); }

    typedef typename std::list<T>::const_reverse_iterator const_reverse_iterator;
    const_reverse_iterator rbegin() const { return(l.rbegin()); }
    const_reverse_iterator rend() const { return(l.rend()); }

    /// inserts copy of t in correct sorted position and returns iterator.
    iterator insert(const T& t);
    /// inserts copy of t in correct position.
    /// p is a hint; if t belongs directly before p, insert will be O(1)
    iterator insert(iterator p, const T& t);
    iterator erase(iterator i);

    void push_front(const T& t) { insert(begin(), t); }
    void push_back(const T& t) { insert(end(), t); }

    void remove(const T&);
    size_t size() const { return(m_size); }
    bool empty() const { return(m_size == 0); }
    void clear() { m_size = 0; l.clear(); }

    T const &front() const { BB_ASSERT(!empty()); return(l.front()); }
    T&       front() { BB_ASSERT(!empty()); return(l.front()); }
    T const &back() const { BB_ASSERT(!empty()); return(l.back()); }
    T&       back() { BB_ASSERT(!empty()); return(l.back()); }
    void pop_front() { BB_ASSERT(!empty()); --m_size; l.pop_front(); }
    void pop_back() { BB_ASSERT(!empty()); --m_size; l.pop_back(); }

protected:
    std::list<T> l;
    size_t m_size; // we keep track of this so that size() is O(1)
    CMP m_compare;
};

template<typename T, typename CMP>
typename sorted_list<T, CMP>::iterator sorted_list<T, CMP>::insert(const T& t) {
    typename std::list<T>::iterator i = l.begin();
    for (; i != l.end() && m_compare(*i, t); i++)
        ;
    i = l.insert(i, t);
    ++m_size;
    return i;
}

template<typename T, typename CMP>
typename sorted_list<T, CMP>::iterator sorted_list<T, CMP>::insert(iterator p, const T& t) {
    // check that p is indeed the right position in the list
    // allow any ordering of equal (by m_compare) elements
    bool bad_order = false;
    iterator i = p;
    // check that element before p, if one exists, is le t
    if (p != l.begin()) {
        i--;
        if (m_compare(t, *i)) {
            bad_order = true;
        }
    }
    // check that p is either ge t or is l.end()
    if (p != l.end()) {
        if (m_compare(*p, t)) {
            bad_order = true;
        }
    }
    if (bad_order == true) {
        return insert(t);
    }
    ++m_size;
    return(l.insert(p, t));
}

template<typename T, typename CMP>
typename sorted_list<T, CMP>::iterator sorted_list<T, CMP>::erase(iterator i) {
    --m_size;
    return(l.erase(i));
}

template<typename T, typename CMP>
void sorted_list<T, CMP>::remove(const T& t) {
    typename std::list<T>::iterator i = l.begin();
    for (; i != l.end() && m_compare(*i, t); i++) {
    }
    while (i != l.end() && !m_compare(t, *i)) {
        typename std::list<T>::iterator p = i;
        i++;
        --m_size;
        l.erase(p);
    }
}


} // namespace bb


#endif // BB_CORE_SORTED_LIST_H

#ifndef BB_CORE_MPL_H
#define BB_CORE_MPL_H

/* Contents Copyright 2016 Athena Capital Research LLC. All Rights Reserved. */
#include <stdint.h>

#include <boost/type_traits.hpp>

#ifndef UINT64_MAX
#define UINT64_MAX (18446744073709551615ULL)
#endif

namespace bb {

    // Sample usage: This is a constructor of a class that inherits off of 'OrderType'.
    // If 'OrderType' is a 'FutureOrder', then we expect the second argument to be a 'TdOrderFutureMsg' and we pass it along to the base.
    // If 'OrderType' is something else, then we expect the second argument to be a 'TdOrderMsg' and we pass that along to the base.
    //
    // JTdOrderModifyOrderNewPart ( bb::tdcore::TdClient* sender,
    //      typename boost::if_then_else<boost::is_same<OrderType, bb::tdcore::FutureOrder>::value,
    //      bb::TdOrderFutureMsg,
    //      bb::TdOrderMsg >::type const & order )
    //      : OrderType ( sender, order ) {}
    //
    template<bool If, typename Then, typename Else>
    struct if_then_else;

    template<typename Then, typename Else>
    struct if_then_else<true, Then, Else>
    {
        typedef Then type;
    };

    template<typename Then, typename Else>
    struct if_then_else<false, Then, Else>
    {
        typedef Else type;
    };

    template<typename A, typename B>
    struct both_true
    {
        typedef boost::false_type type;
        static const bool value = false;
    };

    template<>
    struct both_true<boost::true_type, boost::true_type>
    {
        typedef boost::true_type type;
        static const bool value = true;
    };

    template<typename A, typename B>
    struct either_true
    {
        typedef boost::true_type type;
        static const bool value = true;
    };

    template<>
    struct either_true<boost::false_type, boost::false_type>
    {
        typedef boost::false_type type;
        static const bool value = false;
    };

    template<typename A, typename B>
    struct both_false
    {
        typedef boost::false_type type;
        static const bool value = false;
    };

    template<>
    struct both_false<boost::false_type, boost::false_type>
    {
        typedef boost::true_type type;
        static const bool value = true;
    };

    namespace internal {

        template<size_t pow>
        struct binary_power
        {
            enum
            {
                value = binary_power<pow-1>::value * 2,
                upper_limit = UINT64_MAX
            };
        };

        template<>
        struct binary_power<0>
        {
            enum
            {
                value = 1
            };
        };
    }

    // In C++98, std::numeric_limits<uint8_t>::max() is NOT a compile time constant.
    // This implementation returns the same answer but does work in a BOOST_STATIC_ASSERT in c++98.
    template<typename T>
    struct numeric_limits
    {
        enum
        {
            max = bb::internal::binary_power<8*sizeof(T)>::value - 1
        };
    };

    template<typename T>
    typename boost::enable_if<boost::is_integral<T>, T>::type
    max_value( const T& )
    {
        // We us numeric_limits<T>::max so this works in C++98 and up.
        return static_cast<T>(numeric_limits<T>::max);
    }
}
#endif // BB_CORE_MPL_H

#ifndef BB_CORE_OREASON_UTILS_H
#define BB_CORE_OREASON_UTILS_H

/* Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved. */


#include <bb/core/Error.h>
#include <bb/core/Log.h>
#include <bb/core/oreason.h>

namespace bb {

// returns true if the order reason is any kind of rejection (risk, TD, exchange, etc.)
inline bool is_reject( oreason_t oreason )
{
    switch( oreason )
    {
    case R_NONE:
    case R_FILL:
    case R_CANCEL:
        return false;

    case R_REJECT:
    case R_MARGIN_REJECT:
    case R_PARTIAL_REJECT:
    case R_TS_REJECT:
    case R_REJECT_TODAY_POSITION_OFFSET:
    case R_REJECT_POSITION_OFFSET:
    case R_REJECT_MKT_NOT_TRADING:
    case R_REJECT_CONFIRM_SETTLEMENT:
    case R_REJECT_PRICE_LIMITS:
    case R_REJECT_NOT_ENOUGH_CAPITAL:
    case R_REJECT_MSG_RATE_EXCEEDED:
    case R_REJECT_NOT_LOGGED_IN:
    case R_REJECT_NO_OPEN_LIMIT:
    case R_AUCTION_CLOSED_REJECT:
    case R_TS_MODIFY_UNFOUND:
    case R_MODIFY_REJECT:
    case R_INVALID_SALE_MARKING:
        return true;

    case R_REJECT_TAIFEX_PROD_CODE_NOT_FOUND:
    case R_REJECT_TAIFEX_CONTRACT_EXPIRED:
    case R_REJECT_TAIFEX_ORDERID_ALREADY_EXISTS:
    case R_REJECT_TAIFEX_WORKING_ORDER_NOT_FOUND:
    case R_REJECT_TAIFEX_FILL_ORDER_NOT_FOUND:
    case R_REJECT_TAIFEX_FILL_ORDER_ALREADY_EXISTS:
    case R_REJECT_TAIFEX_ORDER_ALREADY_CANCELED:
    case R_REJECT_TAIFEX_ORDER_ALREADY_FILLED:
    case R_REJECT_TAIFEX_NO_ORDER_BALANCE_CAN_CANCEL:
    case R_REJECT_TAIFEX_NOT_ENOUGH_QUANT_DEDUCTION:
    case R_REJECT_TAIFEX_NOT_ENOUGH_OPEN_INTEREST:
    case R_REJECT_TAIFEX_CANCEL_QUANTITY_TOO_LARGE:
    case R_REJECT_TAIFEX_CONTRACT_MONTH_NOT_FOUND:
    case R_REJECT_TAIFEX_CHANGE_ORDER_TYPE_TO_OFFSET:
    case R_REJECT_TAIFEX_PRICE_EXCEEDS_DAILY_LIMITS:
    case R_REJECT_TAIFEX_PRICE_OR_ORDERTYPE_WRONG:
    case R_REJECT_TAIFEX_DAY_ORDER_NOT_ALLOWED:
    case R_REJECT_TAIFEX_ROLL_ORDER_NOT_ALLOWED:
    case R_REJECT_TAIFEX_OFFSET_OK_BUT_NOT_ROLL:
    case R_REJECT_TAIFEX_MARKET_BUYORDER_NOT_ALLOWED:
    case R_REJECT_TAIFEX_POSITION_LIMIT_REACHED:
    case R_REJECT_TAIFEX_INSUFICIENT_ORDER_AMOUNT:
    case R_REJECT_TAIFEX_INCORRECT_TICK_PRICE:
    case R_REJECT_TAIFEX_ORDER_BREAKS_POSITION_LIMIT:
    case R_REJECT_TAIFEX_ONLY_OFFSETTING_ORDER_OK:
    case R_REJECT_TAIFEX_MARKET_NOT_YET_OPEN:
        return true;

    case R_REJECT_BY_MARKET_FLAG: // this is supposed to be a bit set within another value
        LOG_PANIC << "is_reject called with R_REJECT_BY_MARKET_FLAG (not a valid value by itself)" << bb::endl;
        return true;

    // no default case so that new values in oreason.table will trigger compiler warnings/errors
    }

    // order is rejected for other unknown detailed reason but it's a reject for sure
    // this could happen when the server side gets upgraded first
    if( oreason & R_REJECT_BY_MARKET_FLAG )
    {
        return true;
    }

    BB_THROW_ERROR_SS( "is_reject called with invalid oreason: " << static_cast<int>( oreason ) );
}

} // namespace bb

#endif // BB_CORE_OREASON_UTILS_H

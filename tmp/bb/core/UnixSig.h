#ifndef BB_CORE_UNIXSIG_H
#define BB_CORE_UNIXSIG_H

/* Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved. */


#include <csignal>
#include <boost/function.hpp>

namespace bb {

/// Signal handler function which can be registered with
/// the SelectDispatcher and other classes.
/// 
/// These handlers are called from usermode context, not signal context.
/// See UnixSigImpl.h for implementation details.
typedef boost::function<void (const siginfo_t *)> UnixSigCallback;

/// Exception which can be thrown on receipt of a signal.
class SignalException : public std::exception
{
public:
    // UnixSigCallback which throws SignalException
    struct Throw
    {
        void operator()(const siginfo_t *info) const;
    };

    explicit SignalException(const siginfo_t *info) throw();

    virtual const char *what() const throw() { return m_buf; }
    const char *sigName() const;
    int sig() const { return m_info.si_signo; }

    siginfo_t m_info;
    char m_buf[50];
};

} // namespace bb

#endif // BB_CORE_UNIXSIG_H

#ifndef BB_CORE_DTOA_H
#define BB_CORE_DTOA_H
/* Contents Copyright 2015 Athena Capital Research LLC. All Rights Reserved. */

#include <cmath>
#include <stdint.h>

#include <bb/core/Log.h>
#include <bb/core/itoa.h>
#include <bb/core/compat.h>

#include <boost/static_assert.hpp>

namespace bb {

template<typename T, size_t BASE, size_t POW>
struct power
{
    static const T value = BASE * power<T,BASE,POW-1>::value;
};

template<typename T, size_t BASE>
struct power<T,BASE,0>
{
    static const T value = 1;
};

template< uint64_t Decimals >
struct SplitDouble{
    SplitDouble( double value );
    uint64_t  intPart;
    uint64_t  decPart;
    bool      negative;
};


///
/// Split a double into two integers for easier printing
///
template< uint64_t Decimals >
SplitDouble<Decimals>::SplitDouble( double value )
{

    uint64_t powerOfTen =  power<uint64_t, 10, Decimals>::value;

    negative = std::signbit( value );
    intPart  = std::abs(value);
    // to convert the decimal part into an integer we do the following:
    // 1) subtract the integer part of the double
    // 2) get the absolute value
    // 3) multiply by 10^( digits + 1 ) and add 5
    // 4) divide by 10
    // steps 3 & 4 ensure that the number is rounded properly
    decPart = ( ( std::abs( value ) - intPart ) * ( powerOfTen * 10L ) + 5 ) / 10L;

    if( unlikely( decPart  >= powerOfTen ) )
    {
        ++intPart;
        decPart = 0;
    }

    return ;
}


template<int Decimals>
inline char * dtoa( char * loc, const double v)
{
    SplitDouble<Decimals> sd( v );
    char *      end;
    // char        buf[8];
    // add the integer part to the buffer
    if( sd.negative )
    {
        *loc = '-';
        ++loc;
    }
    end = bb::u64toa_sse2( sd.intPart, loc );
    *end = '.';
    loc = end + 1;

    loc = bb::u64toa_sse2_padded<Decimals>( sd.decPart, loc );
    return loc;
}

} //end namespace bb

#endif // BB_CORE_DTOA_H

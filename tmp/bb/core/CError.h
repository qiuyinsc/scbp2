#ifndef BB_CORE_CERROR_H
#define BB_CORE_CERROR_H

/* Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved. */


#include <cerrno>

#include <bb/core/Error.h>


namespace bb {

/** CError is a convenience class for wrapping C lib style
    errors. Most of the C functions set a thread global errno variable
    on failure. Pass that into this function and it will be magically
    translated into an Error object with a human readable string
    representation.
*/

class CError : public Error
{
public:
    CError(int err);
    CError(const char *errPrefix, int err);

    static std::string getStr(int err);
    int errnoVal;

protected:
    CError() {}
};

} // namespace bb

#define BB_THROW_ERRNO_SS( _error_stream_ )                                                    \
    do {                                                                                       \
        const int _throw_errno_error_ = errno;                                                 \
        std::ostringstream _throw_error_ss_;                                                   \
        _throw_error_ss_ << _error_stream_;                                                    \
        BB_THROW_EXCEPTION( bb::CError, _throw_error_ss_.str().c_str(), _throw_errno_error_ ); \
    } while( false )

#define BB_THROW_ERRNO_SSX( _error_stream_ ) \
    BB_THROW_ERRNO_SS(SRC_LOCATION_SEP << _error_stream_ )

#define BB_THROW_EXASSERT_ERRNO( _cond_, _error_string_ )                          \
    do {                                                                           \
        if( unlikely( !( _cond_ ) ) ) {                                            \
            const int _throw_errno_error_ = errno;                                 \
            BB_THROW_EXCEPTION( bb::CError, _error_string_, _throw_errno_error_ ); \
        }                                                                          \
    } while( false )

#define BB_THROW_EXASSERT_SS_ERRNO( _cond_, _error_stream_ ) \
    do {                                                     \
        if( unlikely( !( _cond_ ) ) )                        \
            BB_THROW_ERRNO_SS( _error_stream_ );             \
    } while( false )

#define BB_THROW_EXASSERT_SSX_ERRNO( _cond_, _error_stream_ ) \
    BB_THROW_EXASSERT_SS_ERRNO( _cond_, SRC_LOCATION_SEP << _error_stream_ )

#endif // BB_CORE_CERROR_H

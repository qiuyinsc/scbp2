#ifndef BB_CORE_SCRIPTING_H
#define BB_CORE_SCRIPTING_H

/* Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved. */


/// Defines macros for adding scripting capabilities to your classes.
/// This should be included in your header; you will need to include ScriptManager.h
/// in your .cc file.

#include <boost/preprocessor/stringize.hpp>

struct lua_State;


namespace bb {

/// Adds the core library to the scripting environment.
bool registerScripting();
bool registerClassInfo( lua_State& L);
} // namespace bb


///
/// BB_DECLARE_SCRIPTING
///
/// Place this macro in your class to enable scripting.
/// It is your responsibility to implement the Register function which
/// binds the classes methods to their lua counterparts.
/// The first arguement is the name of your class.
///
/// You must implement the following static function in your class:
///
/// static bool registerScripting( lua_State& state );
///
/// Be sure to put the BB_REGISTER_SCRIPTING_ONCE macro at the top of your implementation.
///
#define BB_DECLARE_SCRIPTING()                                                                \
        static bool registerScripting( lua_State& state )

///
/// BB_REGISTER_SCRIPTING_CLASS
///
/// Place this macro in your libraries main registration method to automatically
/// register your class with the scripting environment.
/// The first argument is the name of your class.
/// The second argument is the name of the "library" this class is in.
/// DO NOT PUT QUOTES IN THE LIBRARY NAME.
///
#define BB_REGISTER_SCRIPTING_CLASS( _class_, _library_ ) do {                                \
        bb::getScriptManager().addRegisterFunction(                                           \
                BOOST_PP_STRINGIZE( _library_ ), &_class_::registerScripting );               \
} while( false )

///
/// BB_REGISTER_SCRIPTING_FUNC
///
/// Place this macro in the registration function's header to automatically invoke
/// the registration function with the scripting environment.
/// The first argument is the name of your function.  No namespace specification can be included.
/// The second argument is the name of the "library" this function is in.
/// DO NOT PUT QUOTES IN THE LIBRARY NAME.
///
#define BB_REGISTER_SCRIPTING_FUNC( _func_, _library_ ) do {                                  \
        bb::getScriptManager().addRegisterFunction( BOOST_PP_STRINGIZE( _library_ ), &_func_ ); \
} while( false )

///
/// BB_REGISTER_SCRIPTING_ONCE
///
/// Place this macro in your registerScripting function.
/// It makes is so that the class will only get registered once.
///
#define BB_REGISTER_SCRIPTING_ONCE( _L_, _libname_ )                                          \
        if ( bb::detail::register_scripting_once_impl( &_L_,_libname_ ) )                     \
            return false;                                                                     \
        else // leave this dangling in case the macro is used as the body of an "if" block

namespace bb {
namespace detail {

// returns true if we've already registered libname in L
bool register_scripting_once_impl( lua_State* L, const char* libname );

} // namespace detail
} // namespace bb

#endif // BB_CORE_SCRIPTING_H

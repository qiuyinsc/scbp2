#ifndef BB_CORE_MSTREAMCALLBACK_H
#define BB_CORE_MSTREAMCALLBACK_H

/* Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved. */


#include <bb/core/smart_ptr.h>

namespace bb {

BB_FWD_DECLARE_SHARED_PTR(Msg);
BB_FWD_DECLARE_SHARED_PTR(MStream);
BB_FWD_DECLARE_SHARED_PTR(IRecvTransport);

/// Interface for a listener to an MStream/IRecvTransport, for push-style message classes.
/// This is an interface class (rather than a boost::function) for speed; note that if
/// you're going for speed, having IMStreamCallback as the only base class with a vtable
/// is faster than using multiple inheritance.
class IMStreamCallback
{
public:
    virtual ~IMStreamCallback() {}
    /// Push-style callback, gets called for each message in the stream. The message is only
    /// valid for the lifetime of this callback.
    virtual void onMessage(const Msg &msg) = 0;
};
BB_DECLARE_SHARED_PTR(IMStreamCallback);

}

#endif // BB_CORE_MSTREAMCALLBACK_H

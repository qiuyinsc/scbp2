#ifndef BB_CORE_COMPAT_LINUX_H
#define BB_CORE_COMPAT_LINUX_H

/* Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved. */


namespace bb {

typedef int        fd_t;


/******************************************************************************/
//
// Platform specific initialization and cleanup
//
/******************************************************************************/

struct compat_init_info_t
{};
extern compat_init_info_t compat_init_info;

struct compat_init_result_t
{};
extern compat_init_result_t compat_init_result;

inline void compat_init()    {}
inline void compat_close()    {}


#define BB_PTR_OFFSET(field, base) ( (unsigned long)field - (unsigned long)base )


} // namespace bb


/******************************************************************************/
//
// Platform specific headers
//
/******************************************************************************/

// OS
#include <sys/time.h>
#include <unistd.h>
#include <uuid/uuid.h>


/// Architecture-specific memory barriers. Read the linux memory barrier documentation
/// for exact semantics of these. linux-kernel-source/Documentation/memory-barriers.txt

/// These may only be valid for Linux GCC on Intel architectures.

/// A compiler barrier.
#define compiler_barrier()       __asm__ __volatile__("": : :"memory")

/// Memory barriers for inter-CPU memory ordering control.
#define smp_mb()        asm volatile("mfence":::"memory")
#define smp_rmb()       compiler_barrier()
#define smp_wmb()       compiler_barrier()
#define smp_read_barrier_depends()  do { } while (0)

#endif // BB_CORE_COMPAT_LINUX_H

#ifndef BB_CORE_PREALLOCATEDOBJECTFACTORY_H
#define BB_CORE_PREALLOCATEDOBJECTFACTORY_H

/* Contents Copyright 2016 Athena Capital Research LLC. All Rights Reserved. */

#include <cxxabi.h>

#include <bb/core/Log.h>
#include <bb/core/Countable.h>
#include <bb/core/LuaConfig.h>
#include <bb/core/smart_ptr.h>

#include <boost/pool/object_pool.hpp>
#include <boost/enable_shared_from_this.hpp>
#include <boost/intrusive_ptr.hpp>
#include <boost/function.hpp>
#include <boost/bind.hpp>

namespace bb{
///
///  Object factory.  To use your object must inherit from FactoryObject.
///  Never call delete directly on your object call the deallocate( o )
///  function to release the object.  This will release the object in the
///  correct manner.  Note that objects that inherit from FactoryObject
///  do not need to be craeted by a factory.  In this case the deallocate
///  function will call delete under the covers.
///
///  Usage:
///  class Obj : public ObjectFactory<Obj> {
///  ...
///  }
///
///  Declare factory:
///    typedef PreallocatedObjectFactory<Obj>   ObjFactory
///    typedef boost::smart_ptr<ObjFactory>     ObjFactoryPtr
///  Create Factory:
///    ObjFactoryPtr factory = ObjFactory::createFactory<ObjFactory>( cfg )
///  Getting an object:
///   Obj*  o = factory->getObject();
///  Deallocating an object:
///   o->deallocate( o );

///
/// Class to configure object pool
class ObjectFactoryConfig
    : public LuaConfig<ObjectFactoryConfig>
{
public:
    ObjectFactoryConfig(uint32_t initial = 1024, uint32_t grow = 1024)
        : initialSize( initial )
        , growSize ( grow )
    {}
    static void describe()
    {
        Self::create()
            .param( "initial_size",   &Self::initialSize )
            .param( "grow_size",      &Self::growSize )
            .param( "name",           &Self::name )
            ;
    }

    uint32_t       initialSize;
    uint32_t       growSize;
    std::string    name;
};

class FactoryObjectDestructor
{
public:
    FactoryObjectDestructor(){}
    virtual ~FactoryObjectDestructor(){}
    virtual void returnObject( void* o ) = 0;
};

BB_DECLARE_SHARED_PTR( FactoryObjectDestructor );
///
/// Factory class for preallocating objects of a given type.
/// Objects are created and stored in a vector.  For now only
/// objects with a constructor that takes no parameters are supported
template< typename ObjectType>
class PreallocatedObjectFactory
    : public boost::enable_shared_from_this<PreallocatedObjectFactory<ObjectType> >
    , public Countable<uint32_t>
    , public FactoryObjectDestructor
{
protected:
    ///
    /// Make the Ctor protected to force users to use a shared_ptr.
    /// This is necessary because shared_from_this is used
    PreallocatedObjectFactory( const ObjectFactoryConfig& cfg )
        : m_targetSize( cfg.initialSize )
        , m_allocCount( 0 )
        , m_deallocCount( 0 )
        , m_inUseCount( 0 )
        , m_maxInUseCount( 0 )
        , m_pool( m_targetSize, cfg.growSize )
        , m_desc( cfg.name.empty() ? objectTypeName() : cfg.name )
    {
        // allocate one object.  This will cause the pool to expand to
        // "targetSize"
        ObjectType * ptr = m_pool.malloc();
        m_pool.free( ptr );
    }

public:
    typedef PreallocatedObjectFactory<ObjectType>   FactoryType;
    typedef boost::intrusive_ptr<FactoryType>       FactoryTypePtr;

    /// Create function is templatized so it can be used by inherited
    /// classes
    template<typename Type>
    static
    typename boost::enable_if<boost::is_base_of<FactoryType,Type>, boost::intrusive_ptr<Type> >::type
    createFactory( const ObjectFactoryConfig& cfg )
    {
        boost::intrusive_ptr<Type> factory( new Type( cfg ) );
        return factory;
    }

    virtual ~PreallocatedObjectFactory()
    {
        BB_ASSERT ( m_deallocCount <= m_allocCount );

        LOG_INFO << "ObjectFactory "<< m_desc
                 << " allocated "<< m_allocCount
                 << " maxInUse: "<< m_maxInUseCount
                 << bb::endl;
    }

    ///
    /// returns a pointer to an pbject of type ObjectType.
    /// The memory backing the object is owned by the factory and
    /// must be retured via the objects deallocate API
    ObjectType* getObject()
    {
        ObjectType * p = getObject_impl();
        p = new ( p ) ObjectType() ;
        p->setFactory( this );
        return p;
    };

    template<typename Param1>
    ObjectType* getObject( Param1 & p1)
    {
        ObjectType * p = getObject_impl();
        p = new ( p ) ObjectType( p1) ;
        p->setFactory( this );
        return p;
    };
    template< typename Param1, typename Param2>
    ObjectType* getObject( Param1 & p1, Param2 & p2 )
    {
        ObjectType * p = getObject_impl();
        p = new ( p ) ObjectType( p1, p2) ;
        p->setFactory( this );
        return p;
    };
    template< typename Param1, typename Param2, typename Param3>
    ObjectType* getObject( Param1 & p1, Param2 & p2, Param3 & p3 )
    {
        ObjectType * p = getObject_impl();
        p = new ( p ) ObjectType( p1, p2, p3 ) ;
        p->setFactory( this );
        return p;
    };
    template< typename Param1, typename Param2, typename Param3, typename Param4>
    ObjectType* getObject( Param1 & p1, Param2 & p2, Param3 & p3, Param4 & p4 )
    {
        ObjectType * p = getObject_impl();
        p = new ( p ) ObjectType( p1, p2, p3, p4 ) ;
        p->setFactory( this );
        return p;
    };
    template< typename Param1, typename Param2, typename Param3, typename Param4, typename Param5>
    ObjectType* getObject( Param1 & p1, Param2 & p2, Param3 & p3, Param4 & p4, Param5 & p5 )
    {
        ObjectType * p = getObject_impl();
        p = new ( p ) ObjectType( p1, p2, p3, p4, p5 ) ;
        p->setFactory( this );
        return p;
    };
    template< typename Param1, typename Param2, typename Param3,
              typename Param4, typename Param5, typename Param6>
    ObjectType* getObject( Param1 & p1, Param2 & p2, Param3 & p3, Param4 & p4, Param5 & p5, Param6 & p6 )
    {
        ObjectType * p = getObject_impl();
        p = new ( p ) ObjectType( p1, p2, p3, p4, p5, p6 ) ;
        p->setFactory( this );
        return p;
    };
    template< typename Param1, typename Param2, typename Param3, typename Param4,
              typename Param5, typename Param6, typename Param7>
    ObjectType* getObject( Param1 & p1, Param2 & p2, Param3 & p3, Param4 & p4, Param5 & p5, Param6 & p6, Param7 & p7 )
    {
        ObjectType * p = getObject_impl();
        p = new ( p ) ObjectType( p1, p2, p3, p4, p5, p6, p7 ) ;
        p->setFactory( this );
        return p;
    };
    template< typename Param1, typename Param2, typename Param3, typename Param4,
              typename Param5, typename Param6, typename Param7, typename Param8>
    ObjectType* getObject( Param1 & p1, Param2 & p2, Param3 & p3, Param4 & p4, Param5 & p5, Param6 & p6, Param7 & p7, Param8 & p8 )
    {
        ObjectType * p = getObject_impl();
        p = new ( p ) ObjectType( p1, p2, p3, p4, p5, p6, p7, p8 ) ;
        p->setFactory( this );
        return p;
    };

    template< typename Param1, typename Param2, typename Param3, typename Param4, typename Param5,
              typename Param6, typename Param7, typename Param8, typename Param9>
    ObjectType* getObject( Param1 & p1, Param2 & p2, Param3 & p3, Param4 & p4, Param5 & p5, Param6 & p6, Param7 & p7, Param8 & p8, Param9 & p9 )
    {
        ObjectType * p = getObject_impl();
        p = new ( p ) ObjectType( p1, p2, p3, p4, p5, p6, p7, p8, p9 ) ;
        p->setFactory( this );
        return p;
    };

    ///
    /// Return an object to the factory.
    ///  Note this takes a void pointer as
    ///  casts it to a pointer to type ObjectType
    ///  This is normally dangerous, but because we know
    ///  it id of type ObjectType, then it is ok.
    ///
    ///  Decrement the reference count to allow the
    ///  factory to be destroyed if all intrusive_ptrs
    ///  have been reset and all the objects have been returned.
    void returnObject( void* o )
    {
        ++m_deallocCount;
        destruct( o );
        // decrement the reference count after the objects destructor is called
        // otherwise if this is the last object, the factory may be destroyed, freeing the
        // objects memory, causing the destructor to fail.
        release();
    }
protected:
    ///
    /// Increment the reference count for each object allocated
    /// This ensures that the factory will always out live the
    /// allocated objects.
    ObjectType* getObject_impl()
    {
        // increment the reference count
        add_ref();
        ++m_allocCount;
        ++m_inUseCount;
        m_maxInUseCount = std::max( m_inUseCount, m_maxInUseCount );

        ObjectType * p = m_pool.malloc();
        return p;
    };

    void destruct( void* o ){
        --m_inUseCount;
        ObjectType * p = reinterpret_cast<ObjectType*>( o );
        p->~ObjectType();
        m_pool.free( p );
    }

    static std::string objectTypeName()
    {
        int rv;
        char * demangled =  abi::__cxa_demangle(typeid(ObjectType).name(), 0, 0, &rv);
        const std::string name(demangled);
        free(demangled);
        return name;
    }

private:
    uint64_t                                             m_targetSize;
    uint64_t                                             m_allocCount;
    uint64_t                                             m_deallocCount;
    uint64_t                                             m_inUseCount;
    uint64_t                                             m_maxInUseCount;
    boost::object_pool<ObjectType>                       m_pool;
    const std::string                                    m_desc;
};


template<typename ObjectType>
class FactoryObject
    : public  boost::noncopyable
{
public:

    FactoryObject()
        : m_isFactoryCreated( false ){};


    /// Set custom deallocator.  If not set the object
    /// will be destructed using delete
    void setFactory( FactoryObjectDestructor* f )
    {
        m_isFactoryCreated = true;
        m_factory = f;
    }

    void deallocate( ObjectType * o)
    {
        if( m_isFactoryCreated  )
        {
            m_factory->returnObject( o );
        }
        else
        {
            delete o;
        }
    }

protected:
    bool                        m_isFactoryCreated;
    FactoryObjectDestructor*    m_factory;
};

} // end namespace bb
#endif // BB_CORE_PREALLOCATEDOBJECTFACTORY_H

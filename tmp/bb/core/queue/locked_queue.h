#ifndef BB_CORE_QUEUE_LOCKED_QUEUE_H
#define BB_CORE_QUEUE_LOCKED_QUEUE_H

/* Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved. */


#include <queue>
#include <boost/thread/recursive_mutex.hpp>

namespace bb {
namespace queue {

template<typename T> class node_type
{
public:
    node_type( long _n ) : n (_n ) {}
    virtual ~node_type() {}
    T n;
};

///
/// Empty queue exception
///
class empty_queue : public std::exception {};

/** Basic locked queue
 *
 * @code
 * locked_queue<long> q;
 * try
 * {
 *       while( 1 )
 *       {
 *           long result = q.pop();
 *       }
 * }
 * catch( const empty_queue& )
 * {
 *       cout << "no more data" << endl;
 * }
 * @endcode
 * \throw empty_queue
 */
template<typename T = node_type<long>* > class locked_queue
{
public:
    typedef T node_t;

    void push( T n )
    {
        boost::mutex::scoped_lock l( m );

        dataQueue.push( n );
    }

    /**
     * @code
     * locked_queue<long> q;
     * try
     * {
     *       while( 1 )
     *       {
     *           long result = q.pop();
     *       }
     * }
     * catch( const empty_queue& )
     * {
     *       cout << "no more data" << endl;
     * }
     * @endcode
     * \throw empty_queue
     */
    T pop() throw (empty_queue)
    {
        boost::mutex::scoped_lock l( m );

        if( dataQueue.empty() )
            throw empty_queue();
        else
        {
            node_t result = dataQueue.front();
            dataQueue.pop();
            l.unlock();
            return result;
        }
    }

    /**
     * Exception-free pop
     * @code
     * locked_queue<long> q;
     * long result;
     * while( q.pop( &result ) ) { ... }
     * @endcode
     */
    uint32_t pop( T* result )
    {
        boost::mutex::scoped_lock l( m );

        if( dataQueue.empty() )
            return 0;
        else
        {
            *result = dataQueue.front();
            dataQueue.pop();
            l.unlock();
            return 1;
        }
    }

    uint32_t size() { return dataQueue.size(); }
    bool empty() { return dataQueue.empty(); }
    boost::mutex& mutex() { return m; }

protected:
    boost::mutex m;
    std::queue<T> dataQueue;
};

} // namespace bb::queue
} // namespace bb

#endif // BB_CORE_QUEUE_LOCKED_QUEUE_H

#ifndef BB_CORE_MMAP_H
#define BB_CORE_MMAP_H

/* Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved. */


#include <fcntl.h>
#include <unistd.h>
#include <sys/mman.h>
#include <sys/types.h>

#include <cerrno>
#include <stdexcept>
#include <string>

#include <boost/function.hpp>
#include <boost/noncopyable.hpp>
#include <boost/operators.hpp>
#include <boost/interprocess/creation_tags.hpp>

#include <bb/core/FD.h>
#include <bb/core/bbassert.h>

namespace bb
{

/// File open modes
using boost::interprocess::open_or_create_t; using boost::interprocess::open_or_create;
using boost::interprocess::open_only_t; using boost::interprocess::open_only;
struct create_or_init_t {}; static const create_or_init_t create_or_init = create_or_init_t();

namespace MMapError
{
    /** These objects are thrown in the case of an error in MMap creation. */
    struct error : public std::runtime_error {
    protected:
        error( const std::string& str );
        error( const std::string& str, bool );
    };

    /// Couldn't open a file.
    struct open_error : public error { open_error( const std::string& filename ); };

    /// Couldn't truncate a file to the desired length
    struct trunc_error : public error { trunc_error( const std::string& filename ); };

    /// Couldn't stat the file
    struct stat_error : public error { stat_error( const std::string& filename ); };

    /// File has the wrong size
    struct size_mismatch : public error { size_mismatch( const std::string& filename, off_t filesz, off_t expSz ); };

    /// Call to mmap failed
    struct mmap_error : public error { mmap_error( const std::string& filename ); };

    /// Another process owns the file
    struct lock_error : public error
    {
        lock_error( const std::string& filename, bool exclusive );
        bool m_exclusive;
    };

    /// Misc locking error.
    struct other_lock_error : public error { other_lock_error( const std::string& filename ); };

    struct chmod_error : public error { chmod_error( const std::string& filename ); };
    struct link_error : public error { link_error( const std::string& fromF, const std::string &toF ); };
}

/// Locking mode for openMMap.
///
/// MMAP_LOCK_SHARED_NB
///     The file will be locked in shared mode -- other LOCK_SHARED threads/processes
///     will be able to access the files but LOCK_EXCLUSIVE threads/processes will be blocked.
///     If the lock is unavailable, throws an error.
///
/// MMAP_LOCK_EXCLUSIVE_NB
///     This is the only client which will be allowed to access the file.
///     If the lock is unavailable, throws an error.
enum mmap_lock_mode_t
{
    MMAP_LOCK_SHARED_NB,
    MMAP_LOCK_EXCLUSIVE_NB,
};

namespace mmap_detail
{
    /// Deleter for boost::shared_ptr which munmaps the data range.
    struct Unmapper
    {
        Unmapper(size_t size) : m_size(size) {}
        size_t m_size;
        void operator()(void *data);
    };

    /// Deleter: munmaps the data range and closes a FD.
    struct UnmapCloser : public Unmapper
    {
        UnmapCloser(size_t size, const FDPtr &fd) : Unmapper(size), m_fd(fd) {}
        FDPtr m_fd;
        void operator()(void *data);
    };

 //    typedef void (*InitializeFn_t)(void *);
    typedef boost::function< void (void*) > InitializeFn_t;
    template<typename T> void initialize(void *p) { new (p) T(); }

    enum init_mode_t { CREATE_OR_INIT, OPEN_OR_CREATE, OPEN_ONLY };
    std::pair<boost::shared_ptr<void>, bool> openCommon(
            const std::string &filename, size_t size,
            init_mode_t initMode, mmap_lock_mode_t lockMode, 
            InitializeFn_t initFn);
}

/// Opens the given file, mmaps it and returns a shared_ptr to the (writable) memory region.
///
/// There are 3 different overloads, because that's all we've needed till now.
///
/// openMMap(open_or_create, filename, lock_mode)
///     Opens the file and returns a pointer to its contents. If the file
///     hasn't been initialized, creates it and initializes it with T's default constructor.
///     pair.second will be true if file already existed. Initialization is atomic.
///
/// openMMap(open_only, filename, lock_mode)
///     Opens the given file, failing if doesn't exist or isn't the right size.
///
/// openMMap(create_or_init, filename, lock_mode)
///     Opens or creates the given file, always initializing it to the default
///     state with T's default constructor.

template<typename T>
std::pair<boost::shared_ptr<T>, bool> openMMap(open_or_create_t, const std::string &filename, mmap_lock_mode_t lock_mode)
{
    std::pair<boost::shared_ptr<void>, bool> h =
        mmap_detail::openCommon(filename, sizeof(T),
                mmap_detail::OPEN_OR_CREATE, lock_mode, &mmap_detail::initialize<T>);
    return std::make_pair(boost::static_pointer_cast<T>(h.first), h.second);
}

template<typename T>
boost::shared_ptr<T> openMMap(create_or_init_t, const std::string &filename, mmap_lock_mode_t lock_mode)
{
    return
        boost::static_pointer_cast<T>(
            mmap_detail::openCommon(filename, sizeof(T),
                mmap_detail::CREATE_OR_INIT, lock_mode, &mmap_detail::initialize<T>).first);
}

template<typename T>
boost::shared_ptr<T> openMMap(open_only_t, const std::string &filename, mmap_lock_mode_t lock_mode)
{
    return
        boost::static_pointer_cast<T>(
            mmap_detail::openCommon(filename, sizeof(T),
                mmap_detail::OPEN_ONLY, lock_mode, NULL).first);
}


} // namespace bb

#endif // BB_CORE_MMAP_H

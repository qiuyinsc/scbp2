#ifndef BB_CORE_SCRIPTMANAGER_H
#define BB_CORE_SCRIPTMANAGER_H

/* Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved. */


#include <map>
#include <vector>
#include <string>

#include <boost/noncopyable.hpp>

struct lua_State;

namespace bb {

/// Typedef for a Script Registration Function.
/// It adds classes, functions, and data to the passed lua_State.
/// Returns true if successful.
typedef bool (*ScriptRegFn)(lua_State&);



/// ScriptManager
class ScriptManager
    : private boost::noncopyable
{
public:
    /// Registers loadLibraries and loadLibrary in the Lua environment.
    /// These access the singleton ScriptManager (result of getScriptManager).
    static void registerFunctions( lua_State& L );

public:

    /// Loads all registered libraries into the passed state.
    void loadLibraries( lua_State& L ) const;

    /// Loads the given library into the passed state.
    /// Returns false if unsuccessful (library not found, etc.).
    bool loadLibrary( const std::string& strLibrary, lua_State& L ) const;

    /// Returns the names of the registered libraries.
    std::vector<std::string> getLibraryNames() const;

public:
    /// Adds a register function to the ms_RegisteredFunctions vector.
    /// Do not to call this function directly. To register for scripting functions,
    /// use the below macros (BB_DECLARE_SCRIPTING and BB_IMPLEMENT_SCRIPTING).
    bool addRegisterFunction( const std::string& strLibrary, ScriptRegFn pfnRegFunc );

private:
    /// Vector of register functions. During startup, we iterate over the collection
    /// and execute the functions to register the corresponding lua proxies.
    std::map<std::string, std::vector<ScriptRegFn> > m_registeredFunctions;
};

/// Returns the ScriptManager singleton.
ScriptManager& getScriptManager();

} // namespace bb

#endif // BB_CORE_SCRIPTMANAGER_H

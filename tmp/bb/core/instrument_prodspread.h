#ifndef BB_CORE_INSTRUMENT_PRODSPREAD_H
#define BB_CORE_INSTRUMENT_PRODSPREAD_H

/* Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved. */


/**
 * @file instrument_prodspread.h
 *
 * Describes instruments that are spreads, i.e. multi-legged assets.  Currently,
 * we're using the extra padding in instrument_t to describe spreads, but in the
 * future, this may change into an ID that we can look up so we can handle an
 * arbitrary number of legs.
 *
 * We can only handle 2-legged calendar and 2-legged inter-commodity spreads
 * now.
 */

#include <iosfwd>

#include <boost/static_assert.hpp>
#include <boost/functional/hash.hpp>

#include <bb/core/Error.h>
#include <bb/core/currency.h>
#include <bb/core/expiry.h>
#include <bb/core/instrument.h>
#include <bb/core/mktdest.h>
#include <bb/core/product.h>
#include <bb/core/safeenum.h>
#include <bb/core/symbol.h>
#include <bb/core/side.h>

struct lua_State;

namespace bb {
namespace ProductSpread {

/**
 * ProductSpread
 * Support for instrument_t with PROD_SPREAD product code
 *
 *
 * This implements a number of different overloads of PROD_SPREAD based on the spread type
 */

/// Find out if the instrument is a PROD_SPREAD with known details
/// @param _instr        the instrument
bool isSpread(instrument_t const&_instr);

/// Get the spread detail pointer
/// Returns 0 if it not a spread
/// @param _instr        the instrument
/// @preconditions      assert(_instr.is_valid())

template<class T>
T const* getDetail(instrument_t const&_instr);

/// Return number of legs
/// Returns 0 if _instr is not a spread, or detail is unknown
/// @param _instr       the instrument
/// @preconditions      assert(_instr.is_valid())
std::size_t getNumLegs(instrument_t const&_instr);

/// Return the leg specified by idx
/// @param _instr       the instrument
/// @param idx          the index (0 based)
/// @preconditions      assert(_instr.is_valid())
///                     assert_throw(isSpread(_instr))
///                     assert_throw(I < getNumLegs(_instr))
instrument_t getLeg(instrument_t const&_instr, std::size_t idx);

/// Return the multiplier of leg specified by idx
/// @param _instr       the instrument
/// @param idx          the index (0 based)
/// @preconditions      assert(_instr.is_valid())
///                     assert_throw(isSpread(_instr))
///                     assert_throw(I < getNumLegs(_instr))
int getLegMultiplier(instrument_t const&_instr, std::size_t idx);

/// Return the direction of the specified leg
/// @param instr    the instrument
/// @param idx      the index (0 based)
dir_t getLegDir(const instrument_t& instr, std::size_t idx);

/**
 *
 *
 * Example Lua Program
 *
    local spminiinstr =ProdSpread.Calendar.make(MKT.NYMEX,Symbol("FUT_NYMEX_CL"),
                 Symbol("FUT_NYMEX_HO"),
                Expiry(201008),Expiry(201010))

    print(tostring(spminiinstr))
    assert(ProdSpread.isSpread(spminiinstr))
    assert(ProdSpread.getNumLegs(spminiinstr)==2)
    assert(ProdSpread.getLegMultiplier(spminiinstr,0)==1)
    assert(ProdSpread.getLegMultiplier(spminiinstr,1)==-1)
    assert(ProdSpread.getType(spminiinstr)==ProdSpread.Type.CALENDAR)
    print(tostring(ProdSpread.getLeg(spminiinstr,0)))
    print(tostring(ProdSpread.getLeg(spminiinstr,1)))
 *
 *
 *
 * **/


/*
 *  Spread Detail Types
 *
 *  All have members
 *  //number of legs
 *  std::size_t size();
 *  std::size_t getNumLegs();
 *  //get enclosing instrument
 *  instrument_t getInstrument();
 *
 *  //get the outright corresponding to leg #I
 *  template <std::size_t I> instrument_t get()
 *  instrument_t get(std::size_t I) precondition I<getNumLegs()
 *
 *  get multipler for leg
 *  template <std::size_t I> int getMultipler()
 *  int getMultipler(std::size_t I) precondition I<getNumLegs()
 *

 *
 */

/*
 *  Bull Calendar two month Spread
 *
 *  Discriminator CALENDAR_1_1
 */
struct Calendar_1_1;
/*
 *  Bull InterProduct (interCommodity) two month Spread
 *  Discriminator INTERPRODUCT_1_1
 */
struct InterProduct_1_1;

namespace detail {

class DetailTypePolicy
{
public:
    enum _EnumVals
    {
        NONE = 0,
        CALENDAR_1_1 = 1, //CME "SP" strategy type buy exp1, sell exp2
        INTERPRODUCT_1_1 = 2,
        CALENDAR_1_1_EQ = 3, //CME "EQ" strategy type sell exp1, buy exp2
    };

    typedef uint8_t storage_type;
    typedef _EnumVals enum_type;

    static const _EnumVals largest_enum_val = INTERPRODUCT_1_1;
    static storage_type narrow(storage_type _val);
    static enum_type get_default()
    {
        return NONE;
    }
    friend std::ostream& operator<<(std::ostream& os, enum_type val);
    static bool is_supported(enum_type _val);

private:
    BOOST_STATIC_ASSERT(sizeof(storage_type) <= sizeof(unsigned int));
    BOOST_STATIC_ASSERT(unsigned(1 << ((sizeof(storage_type) * 8) - 1)) >= largest_enum_val);
};

} // namespace detail

typedef SafeEnum<detail::DetailTypePolicy> DetailType;

namespace bb_instrument_hooks {

// these functions depend on preconditions met by instrument_t
// should not be called outside of instrument.cc
void evalSpreadHash(std::size_t&, instrument_t const&_instr);
bool evalSpreadLessThan(instrument_t const&_instrl, instrument_t const&_instrr);
bool evalSpreadEqualTo(instrument_t const&_instrl, instrument_t const&_instrr);
bool evalIsValid(instrument_t const&_instr);

} // bb_instrument_hooks


namespace detail {

template<DetailType::enum_type _detailtype>
struct spread_traits{

};
template<>
struct spread_traits<DetailType::CALENDAR_1_1>{
    enum {Leg0Multiplier=1,Leg1Multiplier=-1};
};
template<>
struct spread_traits<DetailType::INTERPRODUCT_1_1>{
    enum {Leg0Multiplier=1,Leg1Multiplier=-1};
};
template<>
struct spread_traits<DetailType::CALENDAR_1_1_EQ>{
    enum {Leg0Multiplier=-1,Leg1Multiplier=1};
};
/*
 *
 * SpreadDetailBase ensures that
 * 1. That the discriminator is the very first position
 * 2. That spread details can only be placed on top of the instrument.passing member
 *
 *
 * */
template<DetailType::enum_type _detailtype, class most_derived_type>
struct SpreadDetailBase : DetailType
{

    static const DetailType::enum_type discriminator=_detailtype;
    typedef most_derived_type self_type;
    //offsetof generates warnings
    // rather than disabling the warnings, this works
    instrument_t const & getInstrument() const
    {
        static std::size_t const offset =
            reinterpret_cast<std::size_t> (&(reinterpret_cast<instrument_t const*> (1)->pad)) - 1;//offsetof(instrument_t,padding)
        return *reinterpret_cast<instrument_t const*> (reinterpret_cast<char const *> (this) - offset);
    }

protected:
    SpreadDetailBase() :
        DetailType(_detailtype)
    {
        //check for MI that does not make us the leftmost base
        BB_ASSERT(this==static_cast<most_derived_type const*>(this));
        BOOST_STATIC_ASSERT(sizeof(most_derived_type) <= sizeof(reinterpret_cast<instrument_t*>(0)->pad));
    }
    void* operator new(size_t s, instrument_t&i)
    {
        return &i.pad;
    }
    void operator delete(void *, instrument_t&)
    {
    }
    instrument_t const & myinstr() const
    {
        return getInstrument();
    }
private:
    SpreadDetailBase(SpreadDetailBase const&);
    void operator=(SpreadDetailBase const&);
    void* operator new[](size_t size);
    void operator delete[](void *p);
    void* operator new(size_t, void *a);
    void operator delete(void *, void *);
    void* operator new(size_t);//default
    void operator delete(void *);

};

template<DetailType::enum_type _detailtype, class most_derived_type>
struct Spread_1_1_Helper : SpreadDetailBase<_detailtype, most_derived_type>
{
    static std::size_t size()
    {
        return 2;
    }

    static std::size_t getNumLegs()
    {
        return size();
    }

    template<std::size_t I> static
    int getLegMultiplier()
    {
        return (I == 0) ? spread_traits<_detailtype>::Leg0Multiplier
                : spread_traits<_detailtype>::Leg1Multiplier;
    }

    static int getLegMultiplier(std::size_t I)
    {
        BB_ASSERT(I==0 || I==1);
        return (I == 0) ? getLegMultiplier<0>()
                : getLegMultiplier<1>();
    }
};


}//detail


struct Calendar_1_1 : detail::Spread_1_1_Helper<DetailType::CALENDAR_1_1, Calendar_1_1>
{
    typedef Calendar_1_1 self_type;

    symbol_t getSymbol() const
    {
        return m_sym;
    }
    expiry_t getExpiry() const
    {
        return m_exp2;
    }

    // the first leg is a buy
    static dir_t getLegDir( const std::size_t idx )
    {
        return ( idx % 2 == 0 ) ? BUY : SELL;
    }

    template<std::size_t I>
    instrument_t getLeg() const
    {
        BOOST_STATIC_ASSERT(I==0 || I==1);
        return instrument_t(this->getSymbol(), this->myinstr().mkt, PROD_FUTURE, (I == 0)
                ? this->myinstr().exp
                : this->getExpiry(), 0, RT_NONE, this->myinstr().currency);
    }
    instrument_t getLeg(std::size_t I) const
    {
        BB_ASSERT(I==0 || I==1);
        return instrument_t(this->getSymbol(), this->myinstr().mkt, PROD_FUTURE, (I == 0)
                ? this->myinstr().exp
                : this->getExpiry(), 0, RT_NONE, this->myinstr().currency);
    }

    //instrument_t functionality
    static instrument_t fromString(std::string const&str);
    /// Returns a string format readable by fromString.
    std::string toString() const;
    static bb::instrument_t make(mktdest_t m, bb::symbol_t const& base_sym, bb::symbol_t const& sym,
                expiry_t exp1, expiry_t exp2, currency_t curr_ = currency_t::USD) ;


    static std::string const&getPrefix();

private:
    Calendar_1_1(symbol_t sym, expiry_t e) :
        m_sym(sym),
        m_exp2(e)
    {}

    friend void bb_instrument_hooks::evalSpreadHash(std::size_t&, instrument_t const&);
    void evalHash_hook(std::size_t&seed) const;//only use with evalSpreadHash
    friend bool bb_instrument_hooks::evalSpreadLessThan(instrument_t const&_instrl, instrument_t const&_instrr);
    bool less_than_hook(self_type const&r) const;//only use with instrument_t operator<
    friend bool bb_instrument_hooks::evalSpreadEqualTo(instrument_t const&_instrl, instrument_t const&_instrr);
    bool equal_to_hook(self_type const&r) const;//only use with instrument_t operator==
    friend bool bb_instrument_hooks::evalIsValid(instrument_t const&_instr);
    bool is_valid_hook() const;//only use with instrument_t is_valid

    symbol_t m_sym;
    expiry_t m_exp2;
};

struct EquityCalendar_1_1 : detail::Spread_1_1_Helper<DetailType::CALENDAR_1_1_EQ, EquityCalendar_1_1>
{
    typedef EquityCalendar_1_1 self_type;
    static bb::instrument_t make( bb::symbol_t const& sym,expiry_t exp1, expiry_t exp2) ;

    static std::string const&getPrefix();
    expiry_t getExpiry() const
    {
        return m_exp2;
    }


    template<std::size_t I>
    instrument_t getLeg() const
    {
        BOOST_STATIC_ASSERT(I==0 || I==1);
        return instrument_t(myinstr().sym, myinstr().mkt, PROD_FUTURE, (I == 0)
                ? myinstr().exp
                :getExpiry(), 0, RT_NONE, this->myinstr().currency);
    }
    instrument_t getLeg(std::size_t I) const
    {
        BB_ASSERT(I==0 || I==1);
        return instrument_t(myinstr().sym, myinstr().mkt, PROD_FUTURE,
                (I == 0)? myinstr().exp: getExpiry(),
                        0, RT_NONE, myinstr().currency);
    }

    // the first leg is a sell
    static dir_t getLegDir( const std::size_t idx )
    {
        return ( idx % 2 == 0 ) ? SELL : BUY;
    }

    //instrument_t functionality
    static instrument_t fromString(std::string const&str);
    /// Returns a string format readable by fromString.
    std::string toString() const;

private:
    EquityCalendar_1_1( expiry_t e) :m_exp2(e){}

    friend void bb_instrument_hooks::evalSpreadHash(std::size_t&, instrument_t const&);
    void evalHash_hook(std::size_t&seed) const;//only use with evalSpreadHash
    friend bool bb_instrument_hooks::evalSpreadLessThan(instrument_t const&_instrl, instrument_t const&_instrr);
    bool less_than_hook(self_type const&r) const;//only use with instrument_t operator<
    friend bool bb_instrument_hooks::evalSpreadEqualTo(instrument_t const&_instrl, instrument_t const&_instrr);
    bool equal_to_hook(self_type const&r) const;//only use with instrument_t operator==
    friend bool bb_instrument_hooks::evalIsValid(instrument_t const&_instr);
    bool is_valid_hook() const;//only use with instrument_t is_valid

    expiry_t m_exp2;

};

struct InterProduct_1_1 : detail::Spread_1_1_Helper<DetailType::INTERPRODUCT_1_1, InterProduct_1_1>
{
    typedef InterProduct_1_1 self_type;
    symbol_t getSymbol1() const
    {
        return m_sym1;
    }
    symbol_t getSymbol2() const
    {
        return m_sym2;
    }
    expiry_t getExpiry() const
    {
        return m_exp2;
    }

    static instrument_t make(mktdest_t m, symbol_t const& base_sym, symbol_t const& sym1, expiry_t exp1,
        symbol_t const& sym2, expiry_t exp2, currency_t curr_ = currency_t::USD);

    template<std::size_t I>
    instrument_t getLeg() const
    {
        BOOST_STATIC_ASSERT(I==0 || I==1);
        return instrument_t((I == 0) ? getSymbol1() : getSymbol2(), myinstr().mkt, PROD_FUTURE, (I == 0)
            ? myinstr().exp : getExpiry(), 0, RT_NONE, myinstr().currency);
    }
    instrument_t getLeg(std::size_t I) const
    {
        BB_ASSERT(I==0 || I==1);
        return instrument_t((I == 0) ? getSymbol1() : getSymbol2(), myinstr().mkt, PROD_FUTURE, (I == 0)
            ? myinstr().exp : getExpiry(), 0, RT_NONE, myinstr().currency);
    }

    // the first leg is a buy
    static dir_t getLegDir( const std::size_t idx )
    {
        return ( idx % 2 == 0 ) ? BUY : SELL;
    }

    static instrument_t fromString(std::string const&);
    /// Returns a string format readable by fromString.
    std::string toString() const;
    static std::string const&getPrefix();

private:
    InterProduct_1_1(symbol_t _sym1, symbol_t _sym2, expiry_t e) :
        m_sym1(_sym1), m_sym2(_sym2), m_exp2(e)
    {
    }
    friend void bb_instrument_hooks::evalSpreadHash(std::size_t&, instrument_t const&);
    void evalHash_hook(std::size_t&) const;//only use with evalSpreadHash
    friend bool bb_instrument_hooks::evalSpreadLessThan(instrument_t const&_instrl, instrument_t const&_instrr);
    bool less_than_hook(self_type const&) const;//only use with instrument_t operator<
    friend bool bb_instrument_hooks::evalSpreadEqualTo(instrument_t const&_instrl, instrument_t const&_instrr);
    bool equal_to_hook(self_type const&) const;//only use with instrument_t operator==
    friend bool bb_instrument_hooks::evalIsValid(instrument_t const&_instr);
    bool is_valid_hook() const;//only use with instrument_t is_valid

    symbol_t m_sym1;
    symbol_t m_sym2;
    expiry_t m_exp2;
};

inline DetailType getDetailType(instrument_t const&_instr)
{
    if (_instr.prod == PROD_SPREAD)
        return *reinterpret_cast<DetailType const*> (&_instr.pad);
    return DetailType();
}

template <class T>
T const* getDetail(instrument_t const&_instr)
{
    BB_ASSERT(_instr.is_valid());
    DetailType const sdt = getDetailType(_instr);
    if (sdt == static_cast<DetailType::enum_type> (T::discriminator))
    {
        return reinterpret_cast<T const*> (&_instr.pad);
    }
    return 0;
}

inline bool isSpread(instrument_t const&_instr)
{
    if (_instr.prod != PROD_SPREAD)
        return false;
    return getDetailType(_instr) != DetailType();
}

namespace detail {

struct Prefixes
{
    static char const spreadprefix[6];
};




} // namespace detail

inline bool hasSpreadPrefix(std::string const&_string)
{
    return !(_string.size() < sizeof(detail::Prefixes::spreadprefix)) && _string[0] == 's'
        && std::string::traits_type::compare(_string.data(), detail::Prefixes::spreadprefix,
            sizeof(detail::Prefixes::spreadprefix) - 1) == 0;
}

instrument_t fromString(const std::string& str);
std::string toString(instrument_t const& str);
bool registerProdSpreadEntities( lua_State&  L );

} // namespace ProductSpread
} // namespace bb


#endif // BB_CORE_INSTRUMENT_PRODSPREAD_H

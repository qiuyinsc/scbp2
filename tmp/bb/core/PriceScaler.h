#ifndef BB_CORE_PRICESCALER_H
#define BB_CORE_PRICESCALER_H

/* Contents Copyright 2015 Athena Capital Research LLC. All Rights Reserved. */

/*
   This class provides a generic class to convert prices to their desired forms.
   Ex: Arca Feed sends price and priceScale to specify price for each symbol.
   Suppose in a ArcaMessage we get px:429870 for symbol AAPL. The priceScale for
   symbol AAPL, that is already be stored in a SymbolIndexMap, is 4.
   Then the actual_px for AAPL would be: 42.987 (429870 / (10^4)).

   To use this class we would define a converter function like,
   Ex: boost::function<double (uint8_t)> converterFunction.
   This converter function would be used to create object of PriceScaler like,
   Ex:

   template<>
   struct PriceScalerTraits<bb::SRC_ARCA>
   {
       typedef double     ReturnType;
       typedef uint8_t    ScalerType;
       typedef uint32_t   PriceType;
       static  const uint8_t defaultScaler = 0;
       static  double getConversionScaler( uint8_t priceScale )
       {
           return std::pow( 0.1, priceScale );
       }
   };

   typedef PriceScalerTraits<bb::SRC_ARCA> ArcaPriceScalerTraits;

   We declare a PriceScaler object like this, PriceScaler<ArcaPriceScalerTraits> priceConverter
   and use that object to convert prices like this, priceConverter( price, priceScale )
*/

#include <boost/function.hpp>
#include <boost/optional.hpp>
#include <bb/core/EFeedType.h>
#include <cmath>

namespace bb {
template<bb::EFeedType> struct PriceScalerTraits;

template <typename Traits>
class PriceScaler
{
    typedef typename Traits::ReturnType ReturnType;
    typedef typename Traits::ScalerType ScalerType;
    typedef typename Traits::PriceType  PriceType;

private:
    boost::optional<ReturnType> m_ConversionScaler;
    ScalerType m_cachedScaler;
public:
    PriceScaler()
        : m_cachedScaler( Traits::defaultScaler )
    {};
    ReturnType operator ()( PriceType px, ScalerType scaler )
    {
        if( !m_ConversionScaler || m_cachedScaler != scaler )
        {
            m_ConversionScaler = Traits::getConversionScaler( scaler );
            m_cachedScaler     = scaler;
        }
        return (ReturnType) px * m_ConversionScaler.get();
    }
};

} // namespace bb


#endif // BB_CORE_PRICESCALER_H

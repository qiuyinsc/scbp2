#ifndef BB_CORE_LIQUIDITY_H
#define BB_CORE_LIQUIDITY_H

/* Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved. */


#include <iosfwd>

#include <bb/core/mktdest.h>
#include <bb/core/compat.h>

namespace bb {

enum liquidity_t {
    LIQ_UNKNOWN = 0,
#ifdef ATHENACR_BB_LIQUIDITY_BACKWARD_COMPATIBILITY // we now use raw exchange codes instead
    LIQ_ADDED   = 1,
    LIQ_NEUTRAL = 2,
    LIQ_REMOVED = 3,
    LIQ_ROUTED  = 4,
#endif
} _attr_packed_;

// returns true if the passed in liquidity character is the 'added'
// liquidity indicator for the market
// TODO this has very limited use, consider removal out of core?
// and since we use raw exchange codes, should liquidity_t be a char?
bool liquidityAdded( mktdest_t, liquidity_t );

const char* liquidity2str( liquidity_t t );

// SWIG 1.3.29 doesn't like these in-namespace operators
#ifndef SWIG
std::ostream& operator <<( std::ostream&, const liquidity_t& );
std::istream& operator >>( std::istream&, liquidity_t & );
#endif // SWIG

} // namespace bb

#endif // BB_CORE_LIQUIDITY_H

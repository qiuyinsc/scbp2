#ifndef BB_CORE_FD_H
#define BB_CORE_FD_H

/* Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved. */


#include <boost/noncopyable.hpp>
#include <bb/core/compat.h>
#include <bb/core/smart_ptr.h>

namespace bb
{

/// RAII class for making sure an file descriptor gets closed
class FD : public boost::noncopyable
{

    struct nat {int for_bool_;};
public:
    FD(fd_t fd = -1);
    FD(fd_t fd, const char *error_string); // makes sure the FD is valid, throws CError(error_string, errno) if not.
    ~FD();

#if !defined(SWIG)
    operator int nat::*() const {return isOk()? &nat::for_bool_ : 0;}
    bool operator!()const{return !isOk();}
#endif // !defined(SWIG)

    fd_t getFD() const { return m_fd; }
    bool isOk() const { return m_fd != -1; }
    /// prevents the FD from closing m_fd
    fd_t invalidate() { fd_t ret = m_fd; m_fd = -1; return ret; }

protected:
    fd_t m_fd;
};
BB_DECLARE_SHARED_PTR(FD);

/// FD which can be set blocking/nonblocking.
/// If you expect these methods to work, you should not elsewhere set O_NONBLOCK on the underlying FD
class NBFD : public FD
{
public:
    enum InitPolicy { INIT_BLOCKING, INIT_NON_BLOCKING };
    NBFD(fd_t fd = -1);
    NBFD(fd_t fd, InitPolicy policy, const char *error_string);

    void setNonBlocking(bool nonBlocking = true);
    bool isNonBlocking() const { return m_nonBlocking; }

protected:
    void modifyBlocking(bool nonBlocking); // set value without the cache check
    bool getNonBlocking() const; // retrieve value without the cache
    bool m_nonBlocking; // cache value for fast access e.g. in Socket::send()
};
BB_DECLARE_SHARED_PTR(NBFD);

} // namespace bb

#endif // BB_CORE_FD_H

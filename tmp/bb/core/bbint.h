#ifndef BB_CORE_BBINT_H
#define BB_CORE_BBINT_H

/* Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved. */


#include <boost/cstdint.hpp>

using boost::int8_t;
using boost::uint8_t;
using boost::int16_t;
using boost::uint16_t;
using boost::int32_t;
using boost::uint32_t;
using boost::int64_t;
using boost::uint64_t;

#endif // BB_CORE_BBINT_H

#ifndef BB_CORE_SCOPEDCOUNTER_H
#define BB_CORE_SCOPEDCOUNTER_H

/* Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved. */


namespace bb {

// An RAII counter. If you need this to be threadsafe without a
// controlling mutex, you can use it with an atomic type (assuming
// that type implements ++ and --). If the atomic type doesn't offer
// increment and decrement operators, you can specialize ScopedCounter
// for that type.

template<typename T>
class ScopedCounter
{
public:
    inline ScopedCounter( T& counter )
        : m_counter( counter )
    {
        ++m_counter;
    }

    inline ~ScopedCounter()
    {
        --m_counter;
    }

private:
    T& m_counter;
};

} // namespace bb

#endif // BB_CORE_SCOPEDCOUNTER_H

#ifndef BB_CORE_MAPDB_H
#define BB_CORE_MAPDB_H

/* Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved. */


#include <bitset>

#include <bb/core/auto_vector.h>
#include <bb/core/smart_ptr.h>
#include <bb/core/str_map.h>
#include <bb/core/symbol.h>
#include <bb/core/Log.h>
#include <bb/core/hash_map.h>

namespace bb {
class date_t;
BB_FWD_DECLARE_SHARED_PTR(Environment);

enum
{
    MAPKEY_UNKNOWN = 0,
};

enum mapdb_return_t
{
    MAPDB_SUCCESS = 0,
    MAPDB_NOFILE = 1,
    MAPDB_IOERR = 2,
    MAPDB_COLLISION = 3,
    MAPDB_BADNUM = 4,
    MAPDB_BADCONN = 5,
};
typedef HashSpec<uint32_t, const char*>                             key2str_spec;
typedef HashSpec<const char*, unsigned int, bbext::cstr_quick_hash> str2key_spec;
typedef key2str_spec::HashMap key2str_table;
typedef str2key_spec::HashMap str2key_table;

class mapdb
{
public:

    class const_iterator: public std::iterator<std::forward_iterator_tag, symbol_t>{
    public:
        const_iterator( const const_iterator &i )
            : m_mapdb( i.m_mapdb )
            , m_curPos( i.m_curPos )
            , m_maxSize(i.m_maxSize)
        {
        };
        const_iterator( mapdb const * _mapdb, uint32_t  curPos, uint32_t maxSize )
            : m_mapdb( _mapdb )
            , m_curPos( curPos )
            , m_maxSize (maxSize )
        {
            getNextValid();
        };

        const_iterator &operator ++( )
        {
            //increment the position and find the next valid symbol.
            m_curPos++;
            getNextValid();
            return *this;
        };
        bool operator == (const const_iterator &i) const { return i.m_curPos == m_curPos; };
        bool operator != (const const_iterator &i) const { return i.m_curPos != m_curPos; };

        const symbol_t &operator *() { set(); return m_symbol; };
        const symbol_t *operator->() { set(); return &m_symbol; };
    private:
        void set()
        {
            if( m_curPos <= m_maxSize ){
                m_symbol = symbol_t( m_curPos );
            }else{
                m_symbol = symbol_t( SYM_UNKNOWN );
            }
        };

        // go through the list until we find a valid symbol or reach the end
        // if the current position already points to a valid symbol or the end,
        // then nothing is done
        void getNextValid(){
            // Loop until we either reach the end of the bit set or find a valid symbol
            while( (m_curPos <= m_maxSize ) && ( m_mapdb->isValid(m_curPos) == false ) ){
                m_curPos++;
            }
        }

        /// Bitset for testing key values
        mapdb const *                           m_mapdb;
        uint32_t                                m_curPos;
        uint32_t                                m_maxSize;
        symbol_t                                m_symbol;
    };
    mapdb();
    virtual ~mapdb();

    /// Deep copy, including all stored strings.
    mapdb(const mapdb&);

    /// Loads symbol mappings from a file into this mapdb.
    /// @return a code equivalent to 0 on success, non-zero code on error
    mapdb_return_t load_from_file(const std::string& filename, uint32_t min_value);

    /// Retrieves a number given a string.  Returns MAPKEY_UNKNOWN if not found
    /// or error.  If ok is non-null, sets *ok to true iff parsing succeeded.
    unsigned int get(const char* str, bool* ok = NULL) const;

    /// Retrieves a string representation of a number.  Returns NULL if not
    /// found or error.
    const char* get(unsigned int key) const;

    /// Returns if the key exists in the map
    bool isValid(unsigned int key) const;

    /// Returns the maximum size of the mapdb table, not necessarily the maximum
    /// number within it.
    unsigned int size() const;

    /// Returns the maximum key value.
    unsigned int max() const;

    /// Clear the state.
    void clear();

    // Iterator functions
    const_iterator begin() const;
    const_iterator end() const;

    // constants
    static const uint32_t   ErasedKey;
    static const uint32_t   DeletedKey;
protected:
    /// Max size of the mapdb table
    unsigned int m_size;

    /// Max key value
    unsigned int m_max;

    /// ID to ticker mappings
    key2str_table m_key2str;

    /// Ticker to ID mappings
    str2key_table m_str2key;

    /// Bitset for testing key values
    std::bitset<SYM_MAX> m_valid_keys;
};

} // namespace bb

#endif // BB_CORE_MAPDB_H

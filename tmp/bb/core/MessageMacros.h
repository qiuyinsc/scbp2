#ifndef BB_CORE_MESSAGEMACROS_H
#define BB_CORE_MESSAGEMACROS_H

/* Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved. */



///
/// Message Casting, If, and Switch/Case Syntactic Sugar
///


/// If pMessage_ is of type messageType_, dynamic_casts it and store it in varName_ and proceed
/// Example:   BB_MSG_IF( pMsg, TestMsg, pTestMsg )
#define BB_MSG_IF( pMessage_, messageType_, varName_ )     \
    if ( messageType_* varName_ = dynamic_cast<messageType_*>(pMessage_) )

#define BB_MSG_ELSE_IF( pMessage_, messageType_, varName_ )     \
    else if ( messageType_* varName_ = dynamic_cast<messageType_*>(pMessage_) )
/// Dynamically casts origMsg_ to msgType_ and stores it in a msgType_* named varName_.
/// Does not do mtype verification, so can be used to cast to non-leaf message classes.
/// This macro must be followed by a semi-colon.
#define BB_MESSAGE_CAST( origMsg_, msgType_, varName_ )                       \
    const msgType_* varName_ = dynamic_cast<const msgType_*>(origMsg_)

/// Casts origMsg_ to msgType_ and stores it in a msgType_* named varName_.
/// Checks that the mtypes match before the static_cast; sets to NULL if they don't.
/// Due to the mtype check, you cannot successfully cast to non-leaf message classes.
/// This macro must be followed by a semi-colon.
#define BB_MESSAGE_SAFE_CAST( origMsg_, msgType_, varName_ )                  \
    const msgType_* varName_ = ((msgType_::kMType != (origMsg_)->mtype())     \
                       ? NULL : static_cast<const msgType_*>(origMsg_))

/// Start a message switch (_pMessage is a const Msg*).
#define BB_MESSAGE_SWITCH_BEGIN( pMessage_ )                                  \
        { const bb::Msg* pMessage__ = pMessage_;                              \
          const bb::mtype_t mtype__ = pMessage__->mtype(); if (0) {}

/// End a message switch.
#define BB_MESSAGE_SWITCH_END()                                               \
        }

/// Start a particular message case.  Verifies that the message is of type msgType_::kMType and
/// static_casts _varName to it.  _varName is valid only in the scope of the CASE block.
#define BB_MESSAGE_CASE( msgType_, varName_ )                                 \
            else if ( msgType_::kMType == mtype__ )                           \
            {  if ( const msgType_* varName_ = static_cast<const msgType_*>(pMessage__) ) {

/// Ends a BB_MESSAGE_CASE.  This is a mandatory closure.
#define BB_MESSAGE_CASE_END()                                                 \
            } }

/// Start a particular message case.  Does no verification of mtype, but does the static_cast cast.
/// This is useful when you want a base message class.
#define BB_MESSAGE_CASE_CAST_ONLY( msgType_, varName_ )                       \
            else if ( const msgType_* varName_ = dynamic_cast<const msgType_*>(pMessage__) ) \
            { if ( 1 ) {

/// Make a default case.  This must be at the end of the list of cases within a SWITCH block.
#define BB_MESSAGE_CASE_DEFAULT()                                             \
            else if (1) { if (1) {


/// Discards a particular message type, preventing the default case, but not handling the message.
/// This does not use a BB_MESSAGE_CASE_END because it has no body.
#define BB_MESSAGE_CASE_DISCARD( msgType_ )                                   \
            else if ( msgType_::kMType == mtype__ )                           \
            {}


#endif // BB_CORE_MESSAGEMACROS_H

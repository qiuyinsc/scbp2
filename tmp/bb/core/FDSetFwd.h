#ifndef BB_CORE_FDSETFWD_H
#define BB_CORE_FDSETFWD_H

/* Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved. */


#include <bb/core/smart_ptr.h>

namespace bb {

struct SDPolicy;
struct SDReader;
struct SDWriter;
struct SDReactorBase;
BB_FWD_DECLARE_SHARED_PTR(FDSet);
BB_FWD_DECLARE_SHARED_PTR(FD);

struct SDPolicyHandle;
class SelectDispatcher;

} // namespace bb

#endif // BB_CORE_FDSETFWD_H

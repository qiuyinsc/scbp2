#ifndef BB_CORE_MARKETLEVEL_H
#define BB_CORE_MARKETLEVEL_H

/* Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved. */

#include <bb/core/side.h>
#include <bb/core/PriceSize.h>

namespace bb {

class MarketLevel
{
public:
    /// Empty MarketLevel -- all values are zero
    MarketLevel()
        : m_bid( 0.0, 0 ), m_ask( 0.0, 0 )
    {}

    MarketLevel( const PriceSize& bid, const PriceSize& ask ) : m_bid ( bid ), m_ask ( ask )
    {}

    MarketLevel( const int32_t bidsz, const double bidpx, const double askpx, const int32_t asksz )
        : m_bid( bidpx, bidsz ), m_ask( askpx, asksz )
    {}

    /// Returns the PriceSize for a given side.
    template < side_t side >
    PriceSize getPriceSize( ) const
    {   return (side == BID) ? m_bid : m_ask; }

    /// Returns the size for a given side.
    template < side_t side >
    int32_t getSize( ) const
    {   return (side == BID) ? m_bid.getSize() : m_ask.getSize(); }

    /// Returns the price for a given side.
    template < side_t side >
    double getPrice( ) const
    {   return (side == BID) ? m_bid.getPrice() : m_ask.getPrice(); }

    /// Returns the PriceSize for a given side.
    PriceSize getPriceSize( const side_t side ) const
    {   return (side == BID) ? getPriceSize < BID > () : getPriceSize < ASK > (); }

    int32_t getSize( const side_t side ) const
    {   return (side == BID) ? getSize < BID > () : getSize < ASK > (); }

    /// Returns the price for a given side.
    double getPrice( const side_t side ) const
    {   return (side == BID) ? getPrice < BID > () : getPrice < ASK > (); }

    void setSize( const side_t side, int32_t size )
    {
        if( side == BID )
            m_bid.setSize( size );
        else
            m_ask.setSize( size );
    }

    void setPrice( const side_t side, double price )
    {
        if( price == BID )
            m_bid.setPrice( price );
        else
            m_ask.setPrice( price );
    }


    /// Returns all the values of the MarketLevel.
    /// HINT: The argument order is the same as you'd read it in a Book view.
    void get( int32_t& bidsz, double& bidpx, double& askpx, int32_t& asksz ) const
    {
        bidsz = m_bid.getSize();
        bidpx = m_bid.getPrice();
        asksz = m_ask.getSize();
        askpx = m_ask.getPrice();
    }
    template<class sizetype, class pricetype>
    void get( sizetype& bidsz, pricetype& bidpx, pricetype& askpx, sizetype& asksz ) const
    {
        bidsz = m_bid.getSize();
        bidpx = m_bid.getPrice();
        asksz = m_ask.getSize();
        askpx = m_ask.getPrice();
    }
    /// Returns the midprice of this MarketLevel, or 0 if empty or one-sided.
    double getMidPrice() const
    {

        if ( unlikely( bb::EQ(m_bid.getPrice(),0.0 ) || bb::EQ( m_ask.getPrice(),0.0 ) ) )
        {
            return 0.0;
        }
        return (m_bid.getPrice() + m_ask.getPrice()) / 2.0;
    }

    double getSpread() const
    {
        return m_ask.getPrice() - m_bid.getPrice() ;
    }

    /// Returns the total size of the bid and ask sides.
    int32_t getTotalSize() const
    {   return m_bid.getSize() + m_ask.getSize(); }

    /// Returns true if all values are 0.
    bool empty() const
    {
        return ( m_bid.empty() & m_ask.empty() );
    }

    /// Sets the specified side's price and size.
    void set( const side_t side, const PriceSize& pxsz )
    {
        if ( side == BID ) set < BID > ( pxsz );
        else set < ASK > ( pxsz );
    }
    void set( const side_t side, const double px, const int32_t sz )
    {
        if ( side == BID ) set < BID > ( px, sz );
        else set < ASK > ( px, sz );
    }

    /// Sets the specified side's price and size.
    template<side_t side>
    void set( const PriceSize& pxsz )
    {
        if ( side == BID )
        {
            m_bid = pxsz;
        }
        else
        {
            m_ask = pxsz;
        }
    }

    template<side_t side>
    void set( const double px, const int32_t sz )
    {
        if ( side == BID ) { m_bid.set( px, sz );  }
        else               { m_ask.set( px, sz );  }
    }

    /// Sets the market level.
    void set( const PriceSize& bid, const PriceSize& ask )
    {
        m_bid = bid;
        m_ask = ask;
    }
    void set( const double bidpx, const int32_t bidsz, const double askpx, const int32_t asksz)
    {
        m_bid.set( bidpx, bidsz );
        m_ask.set( askpx, asksz );
    }

private:
    PriceSize    m_bid;
    PriceSize    m_ask;
};
std::ostream& operator<<(std::ostream&,MarketLevel const&);

} // namespace bb

#endif // BB_CORE_MARKETLEVEL_H

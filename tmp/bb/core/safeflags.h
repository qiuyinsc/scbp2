#ifndef BB_CORE_SAFEFLAGS_H
#define BB_CORE_SAFEFLAGS_H

/* Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved. */


/*
 * #include <bb/safeflags.h>
 * */
#include <bb/core/relocatablecheck.h>
#include <ostream>
#include <limits>
#include <boost/type_traits.hpp>
#include <bb/core/safevalue.h>
#include <iosfwd>
#include <bb/core/bbassert.h>
namespace bb {

///
/// A template that makes a type safe value storage,
/// in this case we are disallowing implicit conversions to bool
///


template<class T =unsigned, T default_value=0> struct FlagsStorage :
        IntegralInitializer<T,default_value,ExactComparators<T> > {

    typedef T storage_type;

};

/*
 * struct SafeFlagPolicy{
 *     enum {};
 *  typedef XXX storage_type;//intergral value
 * typedef XXX enum_type;
 *  storage_type get_default();//const
 * static const storage_type mask_val=mask;//OR of all values
 * static const storage_type sanitycheck_val=//add of all values
 * static const enum_type largest_enum_val=//largest enum
 * storage_type narrow(storage_type ){//const if member
 *
 * }
 *
 * };
 *
 * */
namespace example {
struct SafeFlagPolicy : FlagsStorage<> {
    typedef FlagsStorage<> base_type;
    typedef base_type::storage_type storage_type;
    enum _EnumVals {
        Larry =1<<0
        ,Moe=1<<1
        ,Curly=1<<2
    };

    typedef _EnumVals enum_type;
    static const _EnumVals largest_enum_val=Curly;
    static storage_type narrow(storage_type _val) {
        return _val&mask;
    }

    friend std::ostream&operator<<(std::ostream&os, enum_type val) {
        switch (val) {
        case Moe:
            os<<"Moe";
            break;
        case Larry:
            os<<"Larry";
            break;
        case Curly:
            os<<"Curly";
            break;

        }
        return os;
    }

protected:
    static const storage_type mask= Larry+Moe+Curly;
    static const storage_type sanitycheck= Larry|Moe|Curly;

    BOOST_STATIC_ASSERT(sizeof(storage_type) <=sizeof(unsigned) );
    BOOST_STATIC_ASSERT(unsigned(1<<((sizeof(storage_type)*8)-1 ) ) >=largest_enum_val);
    BOOST_STATIC_ASSERT(mask==sanitycheck );

};

}

template < class POLICY_TYPE> class SafeFlag : public POLICY_TYPE {
public:
    typedef POLICY_TYPE policy_type; ///< the policy
    typedef SafeFlag < POLICY_TYPE> my_type;
    typedef typename policy_type::storage_type storage_type;
    typedef typename policy_type::enum_type enum_type;
private:
    storage_type m_val;

    BOOST_STATIC_ASSERT(boost::is_integral<storage_type>::value==true);
public:

    /// constructor, with a value provided
    /// @param val the new value
    explicit SafeFlag(storage_type val, policy_type const&p=policy_type()) :
        policy_type(p), m_val(this->narrow(val)) {
    }
    ///  default constructor
    explicit SafeFlag(policy_type const&p=policy_type()) :
        policy_type(p), m_val(this->get_default()) {
    }
    SafeFlag(enum_type val, policy_type const&p=policy_type()) :policy_type(p),
        m_val(val) {

    }
    /// default copy    constructor assignment operator OK
    storage_type get() const {
        return m_val;
    }

    bool test(enum_type _val) const {
        return (m_val&_val)!=0;
    }

    void operator+=(enum_type _val) {
        m_val|=_val;
        //    return *this;
    }
    void operator-=(enum_type _val) {
        m_val&=~_val;
        //       return *this;
    }
    void operator+=(SafeFlag const& _val) {
        m_val|=_val.m_val;
        //    return *this;
    }
    void operator-=(SafeFlag const& _val) {
        m_val&=~_val.m_val;
        //       return *this;
    }
    storage_type operator&(enum_type _val) {
        return m_val&_val;
    }
  //  void flip(){
 //       m_val=(~m_val)&(this->mask);

  //  }
    void reset(storage_type _a) {
        m_val=this->narrow(_a);
    }
    void reset() {
        m_val=this->get_default();
    }
    bool operator==(SafeFlag _r) const {
        return this->equal_to(m_val, _r.m_val);
    }
    bool operator!=(SafeFlag _r) const {
        return this->equal_to(m_val, _r.m_val)==false;
    }
    bool operator<(SafeFlag _r) const {
        return this->less_than(m_val, _r.m_val);
    }
    unsigned size()const{
        unsigned ret=0;
        for (unsigned idx=0; idx<sizeof(storage_type)*8; ++idx) {
            enum_type const t=static_cast<enum_type>(1<<idx);
            if (test(t)) {
                ret++;
            }
        }
        return ret;

    }
};

/// the stream insertion operator
template<class charT, class Traits, typename POLICY_TYPE> std::basic_ostream< charT ,Traits> & operator<<(
        std::basic_ostream< charT ,Traits>&_m_stream,
        SafeFlag<POLICY_TYPE> const &val) {
    typedef SafeFlag<POLICY_TYPE> print_type;
    typedef typename print_type::storage_type storage_type;
    typedef typename print_type::enum_type enum_type;


    bool found=false;
    for (unsigned idx=0; idx<sizeof(storage_type)*8; ++idx) {
        enum_type const t=static_cast<enum_type>(1<<idx);
        if (val.test(t)) {
            if (found)
                _m_stream.put('|');
            _m_stream<<t;
            found =true;
        }
    }
    return _m_stream;
}


template<class POLICY_TYPE> struct declare_relocatable<SafeFlag<POLICY_TYPE> > {

    enum {value = 1
        && is_relocatable<POLICY_TYPE>::value
        && is_relocatable<typename SafeFlag< POLICY_TYPE>::value_type>::value
    };

};




}//bb

#endif // BB_CORE_SAFEFLAGS_H

#ifndef BB_CORE_RUNNINGDETECTOR_H
#define BB_CORE_RUNNINGDETECTOR_H

/* Contents Copyright 2016 Athena Capital Research LLC. All Rights Reserved. */

#include <fstream>

namespace bb {

/*
    Help tell if the program is running or crashed from last run.
    It creates a .running file after being initialized and delete that file in destructor.
*/
class RunningDetector
{
public:
    RunningDetector()
    : m_initialized( false )
    , m_running( false )
    {}

    void initialize( const std::string & app_name );

    ~RunningDetector();

    const bool isInitialized() { return m_initialized; };

    bool isRunning() const;

    const std::string getRunningFilePath() { return m_running_file_path; };

private:
    void createRunningFile( const std::string & path );

    // If the instance has been "initialized"
    bool m_initialized;

    // Will be part of the name of the .running file.
    std::string m_app_name;

    bool m_running;

    std::string m_running_file_path;
};

} // namespace bb

#endif // BB_CORE_RUNNINGDETECTOR_H

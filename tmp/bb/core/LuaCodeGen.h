#ifndef BB_CORE_LUACODEGEN_H
#define BB_CORE_LUACODEGEN_H

/* Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved. */


#include <iosfwd>
#include <set>
#include <vector>
#include <utility>

#include <bb/core/instrument.h> // product_t, right_t
#include <bb/core/maturity.h>
#include <bb/core/mktdest.h>
#include <bb/core/ptimefwd.h>
#include <bb/core/side.h>
#include <bb/core/source.h>
#include <bb/core/tif.h>
#include <bb/core/EFeedDest.h>
#include <bb/core/EFeedOrig.h>
#include <bb/core/EFeedType.h>
#include <bb/core/InstrSource.h>

namespace luabind { namespace adl { class object; } using adl::object; }

// helper functions for generating lua code from C++
// If you want to add codegen ability for your structure MyStruct, you should
// define the function:
//   void luaprint(std::ostream &out, MyStruct const &s, LuaPrintSettings const &ps);
//
// Then, in client code, you can output a MyStruct using:
// MyStruct foo;
// cout << luaMode(foo);
//
// There are predefined luaMode outputters for numbers, strings, bools, etc.,
// though it's incomplete.

namespace bb {

class acct_t;
class currency_t;
class expiry_t;
class instrument_t;
class Uuid;

/// This struct encapsulates information about the pretty-printing context.
struct LuaPrintSettings
{
    LuaPrintSettings() : m_prettyPrint(false), m_indentLevel(0) {}
    LuaPrintSettings(int level) : m_prettyPrint(true), m_indentLevel(level) {}

    bool m_prettyPrint;
    int m_indentLevel;

    /// returns indentation string for start of statement
    std::string indent() const;

    /// returns a newline if we're in prettyPrint mode, a space otherwise
    std::string optNewline() const;

    /// returns the LuaPrintSettings for the next level deeper
    LuaPrintSettings next() const;
};

/// Wrapper class for outputting a given type as Lua code.
template<typename T>
struct LuaMode
{
    LuaMode(T const &a, const LuaPrintSettings &l)
        : m_a(a), m_printSettings(l) {}
    friend std::ostream &operator<<(std::ostream &out, LuaMode const& obj)
    {
        // defer to ADL-based luaprint function
        luaprint(out, obj.m_a, obj.m_printSettings);
        return out;
    }
    T const& get() const { return m_a; }
protected:
    T const &m_a;
    LuaPrintSettings m_printSettings;
};

/// Lua codegen helper
template<typename T>
LuaMode<T> luaMode(const T &a, const LuaPrintSettings &l=LuaPrintSettings());

// SWIG 1.3.29 doesn't like these in-namespace operators
#ifndef SWIG

void luaprint(std::ostream &out, unsigned short i, LuaPrintSettings const &ps);
void luaprint(std::ostream &out, unsigned int i, LuaPrintSettings const &ps);
void luaprint(std::ostream &out, uint64_t i, LuaPrintSettings const &ps);
void luaprint(std::ostream &out, short i, LuaPrintSettings const &ps);
void luaprint(std::ostream &out, int i, LuaPrintSettings const &ps);
void luaprint(std::ostream &out, long i, LuaPrintSettings const &ps);
void luaprint(std::ostream &out, float i, LuaPrintSettings const &ps);
void luaprint(std::ostream &out, double i, LuaPrintSettings const &ps);
void luaprint(std::ostream &out, const char * str, LuaPrintSettings const &ps);
void luaprint(std::ostream &out, std::string const &str, LuaPrintSettings const &ps);
void luaprint(std::ostream &out, bool b, LuaPrintSettings const &ps);
void luaprint(std::ostream &out, bb::mktdest_t dst, LuaPrintSettings const &ps);
void luaprint(std::ostream &out, bb::EFeedType feed_type, LuaPrintSettings const &ps);
void luaprint(std::ostream &out, bb::EFeedOrig feed_orig, LuaPrintSettings const &ps);
void luaprint(std::ostream &out, bb::EFeedDest feed_dest, LuaPrintSettings const &ps);
void luaprint(std::ostream &out, bb::timeval_t const &tm, LuaPrintSettings const &ps);
void luaprint(std::ostream &out, bb::ptime_duration_t const &dur, LuaPrintSettings const &ps);
void luaprint(std::ostream &out, bb::expiry_t exp, LuaPrintSettings const &ps);
void luaprint(std::ostream &out, bb::maturity_t mat, LuaPrintSettings const &ps);
void luaprint(std::ostream &out, bb::symbol_t sym, LuaPrintSettings const &ps);
void luaprint(std::ostream &out, bb::product_t prod, LuaPrintSettings const &ps);
void luaprint(std::ostream &out, bb::currency_t curr, LuaPrintSettings const &ps);
void luaprint(std::ostream &out, bb::side_t side, LuaPrintSettings const &ps);
void luaprint(std::ostream &out, bb::dir_t side, LuaPrintSettings const &ps);
void luaprint(std::ostream &out, bb::right_t rt, LuaPrintSettings const &ps);
void luaprint(std::ostream &out, bb::instrument_t const &instr, LuaPrintSettings const &ps);
void luaprint(std::ostream &out, bb::source_t src, LuaPrintSettings const &ps);
void luaprint(std::ostream &out, bb::Uuid const &uuid, LuaPrintSettings const &ps);
void luaprint(std::ostream &out, bb::acct_t acct, LuaPrintSettings const &ps);
void luaprint(std::ostream &out, bb::tif_t tif, LuaPrintSettings const &ps);
void luaprint(std::ostream &out, bb::InstrSource const &pair, LuaPrintSettings const &ps);
template<typename T>
void luaprint(std::ostream &out, std::vector<T> const &vec, LuaPrintSettings const &ps);
template<typename T>
void luaprint(std::ostream &out, std::set<T> const &s, LuaPrintSettings const &ps);

#endif // !SWIG

template<typename T>
inline LuaMode<T> luaMode(const T &a, LuaPrintSettings const &l)
{
    return LuaMode<T>(a, l);
}

namespace detail {

template<typename Vec> void lua_write_vec( std::ostream& out, Vec const &vec, LuaPrintSettings const &ps )
{
    if(vec.empty())
    {
        out << "{}";
        return;
    }

    bb::LuaPrintSettings ps1 = ps.next();

    // TODO: What we maybe should do here, but don't, is to read from a map<typeid,string> which would be
    // populated by luaBindVector() to figure out the Lua "constructor" name for our Vec type.
    // As it stands, round tripping doesn't work "out of the box" because the input Lua might look like
    // "x = MyTypeVec{ Thing1, Thing2 }" but the code generated below will give "x = { Thing1, Thing2 }".

    out << "{" << ps1.optNewline();

    for(typename Vec::const_iterator i = vec.begin(); i != vec.end(); ++i)
    {
        const typename Vec::value_type& val = *i;
        out << ps1.indent() << luaMode(val, ps1) << "," << ps1.optNewline();
    }

    out << ps.indent() << "}";
}

} // namespace detail

template<typename T>
inline void luaprint(std::ostream &out, std::vector<T> const &vec, LuaPrintSettings const &ps)
{
    detail::lua_write_vec( out, vec, ps );
}

template<typename T>
inline void luaprint(std::ostream &out, std::set<T> const &vec, LuaPrintSettings const &ps)
{
    detail::lua_write_vec( out, vec, ps );
}

/// This converts the given object to lua text which when run, would give back something equal to object.
/// (think lisp's write procedure). Can only handle tables, numbers, and strings right now.
void luaDumpObject( std::ostream &out, const luabind::object &in, LuaPrintSettings const &ps );

} // namespace bb

#endif // BB_CORE_LUACODEGEN_H

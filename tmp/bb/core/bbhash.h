#ifndef BB_CORE_BBHASH_H
#define BB_CORE_BBHASH_H

/* Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved. */


#include <bb/core/hash.h>
#include <bb/core/mktdest.h>
#include <bb/core/ostatus.h>
#include <bb/core/EFeedType.h>
#include <bb/core/mtype.h>


BB_HASH_NAMESPACE_BEGIN {

template<> struct hash<bb::mktdest_t> : public int_hash<bb::mktdest_t> { };
template<> struct hash<bb::ostatus_t> : public int_hash<bb::ostatus_t> { };
template<> struct hash<bb::EFeedType> : public int_hash<bb::EFeedType> { };
template<> struct hash<bb::mtype_t> : public int_hash<bb::mtype_t> { };

} BB_HASH_NAMESPACE_END

#endif // BB_CORE_BBHASH_H

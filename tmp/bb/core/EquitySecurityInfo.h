#ifndef BB_CORE_EQUITYSECURITYINFO_H
#define BB_CORE_EQUITYSECURITYINFO_H

/* Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved. */


/**
 * @file EquitySecurityInfo.h
 *
 * Tools to get basic info about a U.S. Equities security.
 */

#include <boost/static_assert.hpp>
#include <iosfwd>
#include <vector>

#include <bb/core/timeval.h>
#include <bb/core/EFeedType.h>
#include <bb/core/symbol.h>
#include <bb/core/safeenum.h>
#include <bb/core/safevalue.h>
#include <bb/core/smart_ptr.h>

// Forward declarations
struct lua_State;

namespace bb {

namespace detail {

class ListingExchangePolicy
{
public:
    enum _EnumVals
    {
        NotApplicable      = '1',
        Unknown            = '2',
        BATS               = 'z',
        BYX                = 'Y',
        CZCE               = 'Z',
        NYSE               = 'N',
        NYSEAlternext      = 'A',
        NASDAQ             = 'O',
        NYSEArca           = 'P',
        ChicagoOne         = 'c',
        CME                = 'C',
        SHSE               = 'G',
        SZSE               = 'E',
        TSX                = 'T',
        TSXVentureExchange = 't'
    };

    typedef unsigned int storage_type;
    typedef _EnumVals enum_type;

    static const _EnumVals largest_enum_val = TSXVentureExchange;
    static storage_type narrow(storage_type _val);
    static enum_type get_default();
    friend std::ostream& operator<<(std::ostream& os, enum_type val);

private:
    BOOST_STATIC_ASSERT(sizeof(storage_type) <= sizeof(unsigned int));
    BOOST_STATIC_ASSERT(unsigned(1 << ((sizeof(storage_type) * 8) - 1)) >= largest_enum_val);
};

class EquitySecurityTypePolicy
{
public:
    enum _EnumVals
    {
        TestSymbol = -1,
        Common = 0,
        Preferred = 1,
        Warrant = 2,
        Unit = 3,
        Rights = 4,
        Called = 5,
        PreferredCalled = 6,
        Convertible = 7,
        ConvertibleCalled = 8,
        WhenDistributed = 9,
        PreferredWhenDistributed = 10,
        PreferredConvertible = 11,
        WhenIssued = 12,
        RightsWhenIssued = 13,
        PreferredWhenIssued = 14,
        WarrantWhenIssued = 15
    };

    typedef int storage_type;
    typedef _EnumVals enum_type;

    static const _EnumVals largest_enum_val = TestSymbol;
    static storage_type narrow(storage_type _val);
    static enum_type get_default();
    friend std::ostream& operator<<(std::ostream& os, enum_type val);

private:
    BOOST_STATIC_ASSERT(sizeof(storage_type) <= sizeof(int));
};

} // namespace detail

typedef SafeEnum<detail::ListingExchangePolicy> ListingExchange;
typedef SafeEnum<detail::EquitySecurityTypePolicy> EquitySecurityType;

bool registerEquitySecurityInfoEntities(lua_State& L);

class EquitySecurityInfo
{
public:
    /// Constructor
    EquitySecurityInfo(symbol_t sym, const ListingExchange& listing_exch, const EquitySecurityType& sec_type,
        unsigned int unit_of_trade, const float& min_px_var, bool allow_odd_lots);

    /// Returns the primary listing exchange
    ListingExchange getListingExchange() const;

    /// Returns the security type
    EquitySecurityType getEquitySecurityType() const;

    /// Returns the unit of trade, a.k.a. lot size or block size.  Almost
    /// always 100.
    unsigned int getUnitOfTrade() const;

    /// Returns the minimum price variance or tick size.
    float getMinimumPriceVariance() const;

    /// Returns whether this security can be traded in odd lots.  For example,
    /// HOLDRs cannot be traded in odd lot sizes.
    bool getAllowOddLots() const;

    /// Returns the symbol associated with this EquitySecurityInfo.
    symbol_t getSymbol() const;

    bool operator<(const EquitySecurityInfo& info) const;
    bool operator<(const symbol_t& sym) const;
    bool operator==(const symbol_t& sym) const;

    friend bool operator<(const symbol_t& sym, const EquitySecurityInfo& info);
    friend bool operator==(const symbol_t& sym, const EquitySecurityInfo& info);
    friend std::ostream& operator<<(std::ostream& os, const EquitySecurityInfo& info);

private:
    /// Symbol that this info is applicable for
    symbol_t m_symbol;

    /// Primary listing exchange
    ListingExchange m_listing_exch;

    /// Security type
    EquitySecurityType m_sec_type;

    /// The unit of trade/block size/lot size of this security
    unsigned int m_unit_of_trade;

    /// The minimum price variation of this security
    float m_min_px_var;

    /// Whether this security allows odd lots
    bool m_allow_odd_lots;
};

/// Keeps a unique sorted vector of EquitySecurityInfo's
class IEquitySecurityInfoMap
{
public:
    typedef std::vector<EquitySecurityInfo> cont_type;
    typedef cont_type::const_iterator const_iterator;

public:
    /// Destructor
    virtual ~IEquitySecurityInfoMap() { }

    /// Returns an iterator to the beginning of the container
    virtual const_iterator begin() const = 0;

    /// Returns an iterator to the end of the container
    virtual const_iterator end() const = 0;

    /// Prints info about all symbols in this container
    virtual void print(std::ostream& os) const = 0;

    /// Returns the working date of this container
    virtual const date_t& getDate() const = 0;
};

BB_DECLARE_SHARED_PTR(IEquitySecurityInfoMap);

/*
 * Convenience functions
 */

/// Helper function for the is_exch functions.
ListingExchange get_exchange(const symbol_t& sym, const IEquitySecurityInfoMap& info_map);

/// Returns whether sym exists in the info_map
bool exists(const symbol_t& sym, const IEquitySecurityInfoMap& info_map);

/// Returns true if the sym is a NYSE-listed symbol
bool is_nyse(const symbol_t& sym, const IEquitySecurityInfoMap& info_map);

/// Returns true if the sym is an AMEX-listed symbol
bool is_alternext(const symbol_t& sym, const IEquitySecurityInfoMap& info_map);

/// Returns true if the sym is an ARCA-listed symbol
bool is_arca(const symbol_t& sym, const IEquitySecurityInfoMap& info_map);

/// Returns true if the sym is a NASDAQ-listed symbol
bool is_nasdaq(const symbol_t& sym, const IEquitySecurityInfoMap& info_map);

/// Returns true if the sym would be transmitted via CQS
bool is_cqs(const symbol_t& sym, const IEquitySecurityInfoMap& info_map);

/// Returns true if the sym would be transmitted via UQDF
bool is_uqdf(const symbol_t& sym, const IEquitySecurityInfoMap& info_map);

/// Maps ListingExchange to mktdest
mktdest_t get_primary_mktdest( const symbol_t& sym, const IEquitySecurityInfoMap& info_map);

/// Returns SRC_CQS if quotes for symbol print on CQS, returns
/// SRC_UQDF if it prints to SRC_UQDF, or throws.
EFeedType getBBL1QuoteFeed( const symbol_t& sym, const IEquitySecurityInfoMap& info_map);

/// Returns SRC_CTS if quotes for symbol print on CTS, returns
/// SRC_UTDF if it prints to SRC_UTDF, or throws.
EFeedType getBBL1TradeFeed( const symbol_t& sym, const IEquitySecurityInfoMap& info_map);

/// Returns the info about sym from the info_map, throw if not available
EquitySecurityInfo find(const symbol_t& sym, const IEquitySecurityInfoMap& info_map);

/// Returns the info about sym from the info_map, return dummy if not available
EquitySecurityInfo find_no_throw(const symbol_t& sym, const IEquitySecurityInfoMap& info_map);

/// Outputs an info_map in a readable format
std::ostream& operator<<(std::ostream& os, const IEquitySecurityInfoMap& info_map);

} // namespace bb

#endif // BB_CORE_EQUITYSECURITYINFO_H

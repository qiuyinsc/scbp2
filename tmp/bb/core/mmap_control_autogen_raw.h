#ifndef BB_CORE_MMAP_CONTROL_AUTOGEN_RAW_H
#define BB_CORE_MMAP_CONTROL_AUTOGEN_RAW_H

// DO NOT DIRECTLY EDIT THIS FILE!
// It was automagically generated by message-gen-ch.rb from .build/optimize_trusty/bb/core/mmap_control.xml
// Edit .build/optimize_trusty/bb/core/mmap_control.xml instead and re-run message-gen-ch.rb.            

/* Contents Copyright 2016 Athena Capital Research LLC. All Rights Reserved. */



#include <stdint.h>
#include <bb/core/Msg.h>

namespace bb
{
    namespace mmap_control
    {
        typedef uint64_t voff_t; // An offset in virtual space (units of bytes)

        typedef uint32_t virt_pg_t; // A page index in virtual space (units of page size)
        const virt_pg_t INVALID_VIRT_PG = static_cast<virt_pg_t>(-1);

        typedef uint32_t phys_pg_t; // A page index in physical space (units of page size)
        const phys_pg_t INVALID_PHYS_PG = static_cast<phys_pg_t>(-1);

        const MsgSizeField_t SKIP_TO_NEXT_PAGE = static_cast<MsgSizeField_t>(-1);
    }
}


#include <bb/core/bbint.h>
#include <bb/core/timeval.h>

#include <bb/core/cstr_array.h>
namespace bb {
namespace mmap_control {

namespace {
struct skip_init_tag {}; // tag to avoid duplicate initialization of base types
}


/*******************************************************/
// channel struct
/*******************************************************/
struct __attribute__((aligned(64))) channel
{
public:
  channel() { memset(this, 0, sizeof *this); }
  channel(const channel& other) { memcpy(this, &other, sizeof *this); }

  typedef boost::array<bb::mmap_control::phys_pg_t, 1048576> virt_to_phys_t;
  typedef boost::array<bb::mmap_control::virt_pg_t, 1048576> phys_to_virt_t;
  typedef bb::cstr_array<16> id_t;
  typedef bb::cstr_array<256> data_path_t;

  // modified on every message
  bb::mmap_control::voff_t virt_offset;
  uint64_t : 64 __attribute__((__packed__));
  // modified each time a new page is being used.
  // These arrays are big enough for a 1GB with 1024 byte pages.
  virt_to_phys_t virt_to_phys;
  phys_to_virt_t phys_to_virt;
  // below here: rarely read and never modified
  bb::mmap_control::phys_pg_t phys_page_count;
  // pow(2, page_size_shift) gives the size of an individual page.
  uint8_t page_size_shift;
  uint64_t : 56 __attribute__((__packed__));
  // pow(2, virt_to_phys_size_shift) gives the number of entries in virt_to_phys.
  bb::mmap_control::phys_pg_t virt_to_phys_size_shift;
  id_t id;
  data_path_t data_path;
  uint64_t : 64 __attribute__((__packed__));
  uint64_t : 64 __attribute__((__packed__));

  // getters
  std::string getIdStr() const;
  std::string getDataPathStr() const;

  // setters
  void setId_throw( const std::string &p );
  void setDataPath_throw( const std::string &p );
  void fillTest();
  void printCert(std::ostream &out) const;
};

/*******************************************************/
// notifier struct
// one per reader
/*******************************************************/
struct __attribute__((aligned(64))) notifier
{
public:
  notifier() { memset(this, 0, sizeof *this); }
  notifier(const notifier& other) { memcpy(this, &other, sizeof *this); }

  typedef bb::cstr_array<8> id_t;

  // True if kernel-level notifications should be sent to this notifier
  // Read and written often.
  uint32_t notification_enabled;
  uint64_t : 32 __attribute__((__packed__));
  bb::mmap_control::virt_pg_t hazard;
  uint64_t : 32 __attribute__((__packed__));
  // rarely read and never modified
  id_t id;
  uint64_t : 64 __attribute__((__packed__));
  uint64_t : 64 __attribute__((__packed__));
  uint64_t : 64 __attribute__((__packed__));
  uint64_t : 64 __attribute__((__packed__));
  uint64_t : 64 __attribute__((__packed__));

  // getters
  std::string getIdStr() const;

  // setters
  void setId_throw( const std::string &p );
  void fillTest();
  void printCert(std::ostream &out) const;
};

/*******************************************************/
// magic struct
/*******************************************************/
struct magic
{
public:
  magic() { memset(this, 0, sizeof *this); }
  magic(const magic& other) { memcpy(this, &other, sizeof *this); }

  uint64_t : 64 __attribute__((__packed__));
  uint32_t version;
  uint32_t size;
  void fillTest();
  void printCert(std::ostream &out) const;
};

/*******************************************************/
// file struct
// top-level structure
/*******************************************************/
struct file
{
public:
  file() { memset(this, 0, sizeof *this); }
  file(const file& other) { memcpy(this, &other, sizeof *this); }

  typedef boost::array<bb::mmap_control::channel, 8> channels_t;
  typedef boost::array<bb::mmap_control::notifier, 256> notifiers_t;
  typedef bb::cstr_array<256> socket_path_t;

  // magic value for sanity checking
  bb::mmap_control::magic magic;
  uint64_t : 64 __attribute__((__packed__));
  uint64_t : 64 __attribute__((__packed__));
  uint64_t : 64 __attribute__((__packed__));
  uint64_t : 64 __attribute__((__packed__));
  uint64_t : 64 __attribute__((__packed__));
  uint64_t : 64 __attribute__((__packed__));
  channels_t channels;
  notifiers_t notifiers;
  uint32_t numchannels;
  uint32_t numnotifiers;
  socket_path_t socket_path;
  uint64_t : 64 __attribute__((__packed__));
  uint64_t : 64 __attribute__((__packed__));
  uint64_t : 64 __attribute__((__packed__));
  uint64_t : 64 __attribute__((__packed__));
  uint64_t : 64 __attribute__((__packed__));
  uint64_t : 64 __attribute__((__packed__));
  uint64_t : 64 __attribute__((__packed__));

  // getters
  std::string getSocketPathStr() const;

  // setters
  void setSocketPath_throw( const std::string &p );
  void fillTest();
  void printCert(std::ostream &out) const;
};

} // namespace bb
} // namespace mmap_control

#endif // BB_CORE_MMAP_CONTROL_AUTOGEN_RAW_H

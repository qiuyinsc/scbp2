#ifndef BB_CORE_USERMSG_H
#define BB_CORE_USERMSG_H

/* Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved. */


#include <bb/core/usermsg_target.h>
#include <bb/core/usermsg_command.h>

/** \file Definitions of all the messages we pass around within the BB
    system.  This file is mostly a wrapper around the generated C++
    code by the message-gen-ch.rb script and usermsg_target.table and
    usermsg_command.table table files. */

#endif // BB_CORE_USERMSG_H

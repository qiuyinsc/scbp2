#ifndef BB_CORE_GDATE_H
#define BB_CORE_GDATE_H

/* Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved. */


#include <boost/date_time/gregorian/gregorian.hpp>

#include <bb/core/gdatefwd.h>
#include <bb/core/date.h>
#include <bb/core/timeval.h>
#include <bb/core/expiry.h>
#include <bb/core/symbol.h>

namespace bb {

gdate_t timeval_to_gdate( const timeval_t& timeval );
timeval_t gdate_to_timeval( const gdate_t& gdate );

gdate_t date_to_gdate( const date_t& date );
date_t gdate_to_date( const gdate_t& gdate );

gdate_t expiry_to_gdate( const expiry_t& expiry );
gdate_t expiry_to_gdate( const expiry_t& expiry, uint8_t date );
expiry_t gdate_to_expiry( const gdate_t& gdate );

gdate_t next_week_date( symbol_t symbol, const gdate_t& date);
gdate_t prev_week_date( symbol_t symbol, const gdate_t& date);

} // namespace bb

#endif // BB_CORE_GDATE_H

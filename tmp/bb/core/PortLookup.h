#ifndef BB_CORE_PORTLOOKUP_H
#define BB_CORE_PORTLOOKUP_H

/* Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved. */


#include <vector>
#include <bb/core/source.h>

namespace bb {

class acct_t;


/** Class used to look up port mappings from lua config. */
namespace PortLookup {

    /// Returns the port associated with PORTNAME if it exists, 0 otherwise.
    unsigned int get(const char *portname);

    /// Returns the ports for the given source for qd subscriptions, returns
    /// empty vector if there are no qd subscription ports defined.
    /// If quiet is true, do not complain about a lack of ports found.
    std::vector<int16_t> getQDPorts(source_t src, bool quiet = false);
}


namespace HostLookup {

    /// Returns the host associated with PORTNAME if it exists, throws otherwise.
    std::string get(const char *portname);
}


/// Retrieves the TCP port for trading in an account.  Returns 0 on error.
unsigned int acct_port(const char *acct);
unsigned int acct_port(const acct_t &acct);

}

#endif // BB_CORE_PORTLOOKUP_H

#ifndef BB_CORE_PTIMEFWD_H
#define BB_CORE_PTIMEFWD_H

/* Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved. */


namespace boost {
namespace posix_time {
class ptime;
class time_duration;
} // namespace posix_time
} // namespace boost

namespace bb {
typedef boost::posix_time::ptime ptime_t;
typedef boost::posix_time::time_duration ptime_duration_t;

#ifdef int64_t
typedef int64_t timeval_microsecond;
typedef int64_t timeval_nanosecond;
#else
typedef long long int timeval_microsecond;
typedef long long int timeval_nanosecond;
#endif
} // namespace bb

#endif // BB_CORE_PTIMEFWD_H

#ifndef BB_CORE_LUASTATE_H
#define BB_CORE_LUASTATE_H

/* Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved. */


#include <string>

#include <boost/lexical_cast.hpp>
#include <boost/noncopyable.hpp>
#include <boost/thread.hpp>

#include <luabind/object.hpp>

#include <bb/core/bbassert.h>
#include <bb/core/smart_ptr.h>
#include <bb/core/LuabindScripting.h>

namespace bb {

class LuaState
    : protected boost::noncopyable
    , public LuaErrorHandlers
{
public:
    /// LuaState constructor
    LuaState();
    /// LuaState destructor
    ~LuaState();

    /// Returns the root table of the LuaState.
    /// This is actually the Lua global table of the LuaState's lua_State.
    luabind::table<> root() const;

    /// Returns the root table of the LuaState.
    /// This is actually the Lua global table of the LuaState's lua_State.
    luabind::table<> operator()() const    { return root(); }

    /// Like root()[key], though read-only (i.e. assignment of the result will not modify this).
    template<typename T>
    luabind::object operator[]( const T& key ) const { return root()[key]; }

    /// Like operator[], but will only return a valid Lua table (or throw).
    template<typename T>
    luabind::table<> getTable( const T& key ) const { return table_cast(root()[key], boost::lexical_cast<std::string>(key)); }

    /// Returns the internal lua_State for raw manipulation.
    lua_State* getState() const           { return m_state; }

    /// Load a filename into this LuaState.
    /// The loaded configuration is applied over the existing LuaState.
    /// Throws a std::runtime_error if the load fails.
    void load( const char* luaFilename ) { LuaExec(m_state, m_pfnErrorHandler).load(luaFilename); }
    void load( const std::string &luaFilename ) { load(luaFilename.c_str()); }
    void load(std::istream &input, const char *chunkname = "") { LuaExec(m_state, m_pfnErrorHandler).load(input, chunkname); }

    /// Executes the passed string (Lua code) in the Lua environment.
    luabind::object execute( const char* s, size_t len ) { return LuaExec(m_state, m_pfnErrorHandler).execute(s, len); }
    luabind::object execute( const std::string& s ) { return execute(s.c_str(), s.length()); }
    luabind::object execute( const char* s ) { return execute(s, strlen(s)); }

    /// Loads the particular library using the ScriptManager singleton.
    /// Returns true if successful.  May throw exceptions during loading.
    bool loadLibrary( const char* libname);

    /// Sets the error handler used when load and execute methods are invoked.
    /// Set to NULL to use Lua's default error handler.
    void setErrorHandler( lua_CFunction pfn )               { m_pfnErrorHandler = pfn; }

    /// Returns the error handler used when load and execute methods are invoked.
    /// If NULL, it uses Lua's default error handler.
    lua_CFunction getErrorHandler( lua_CFunction ) const    { return m_pfnErrorHandler; }

    /// Transfer ownership of this LuaState to the thread calling this method.
    /// A LuaState is initially owned by the thread which creates it.
    void setThisThreadAsOwner();

private:
    bool onOwnerThread() const;

    lua_State*       m_state;           // our Lua VM
    lua_CFunction    m_pfnErrorHandler; // error handler used when execute and load are invoked

    mutable boost::thread::id m_ownerThreadID;
};

BB_DECLARE_SHARED_PTR(LuaState);

} // namespace bb

#endif // BB_CORE_LUASTATE_H

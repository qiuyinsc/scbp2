#ifndef BB_CORE_PATH_UTILS_H
#define BB_CORE_PATH_UTILS_H

/* Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved. */


#include <boost/filesystem/path.hpp>

namespace {

// Several Boost Filesystem v3 functions return paths where v2 returned strings; path_string aids portability
// note that some of the functions here are inline so that test_path_utils can be compiled as v2 and v3
#if BOOST_FILESYSTEM_VERSION >= 3
inline const std::string& path_string( const boost::filesystem::path& path ) { return path.string(); }
#else
inline const std::string& path_string( const std::string& path ) { return path; }
#endif

} // namespace

namespace bb {

bool create_parent_dir( const char *path ); // create parent directory for the given filename

inline std::string filename_string( const boost::filesystem::path& path )
{
    return path_string( path.filename() );
}

// Boost Filesystem v2 had a path::normalize() method, but it was removed due to not being completely portable
// we just need something that works on our few platforms, so this does what POSIX realpath(3) does
std::string real_path( const std::string& path );
inline std::string real_path( const boost::filesystem::path& path ) { return real_path( path.string() ); }

} // namespace bb

#endif // BB_CORE_PATH_UTILS_H

#ifndef BB_CORE_NOTIFICATION_H
#define BB_CORE_NOTIFICATION_H

/* Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved. */


#include <boost/thread/condition.hpp>
#include <boost/thread/mutex.hpp>

#include <bb/core/smart_ptr.h>

namespace bb {

// A notification is a one-shot rendezvous. The notification is
// created in the unnotified state. Any number of threads can await
// its becoming notified by calling awaitNotify. Once notified,
// threads waiting in awaitNotify will unblock, and future calls to
// awaitNotify will not block. The notification must outlive all
// waiters, so if you are using a Notification in a complex way, it
// may be safest to handle it via NotificationPtr;

class Notification : boost::noncopyable
{
public:
    Notification();
    ~Notification();

    // Toggle the notification to the notified state and awaken all
    // waiters. The notification will remain in the notified
    // state. Subsequent calls to notify have no effect.
    void notify();

    // Wait for the notification to become notified. If the
    // notification is already notified, this will not block.
    void awaitNotify() const;

private:
    bool m_notified;
    mutable boost::mutex m_lock;
    mutable boost::condition m_condition;
    mutable size_t m_waiters;
};
BB_DECLARE_SHARED_PTR( Notification );


// A ScopedNotifier notifies a Notification when destroyed. This is
// useful when you want to notify after calling a callback in an
// exception safe fashion.
class ScopedNotifier : boost::noncopyable
{
public:
    inline ScopedNotifier( Notification& notification )
        : m_notification( notification ) {}

    inline ~ScopedNotifier()
    {
        m_notification.notify();
    }

private:
    Notification& m_notification;
};

} // namespace bb

#endif // BB_CORE_NOTIFICATION_H

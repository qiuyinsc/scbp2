#ifndef BB_CORE_ENV_H
#define BB_CORE_ENV_H

/* Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved. */


#include <bb/core/Subscription.h>
#include <bb/core/Environment.h>

namespace bb {
class acct_t;
BB_FWD_DECLARE_SHARED_PTR(Environment);
BB_FWD_DECLARE_SHARED_PTR(ISymbolContext);
BB_FWD_DECLARE_SHARED_PTR(InstrumentInfoContext);
BB_FWD_DECLARE_SCOPED_PTR(LuaState);

namespace statistics {
BB_FWD_DECLARE_SHARED_PTR(StatisticsManager);
} // namespace statistics


class DefaultCoreContext
{
public:

    /// Returns the singleton instance of DefaultEnvironment
    static bool isEnvironmentCreated();
    static const EnvironmentPtr& createEnvironment( const EnvironmentConfig& cfg );
    static const EnvironmentPtr& getEnvironment();

    /// Returns the SymbolContext
    static const ISymbolContextPtr& getSymbolContext();

    /// Returns the InstrumentInfoContext
    static const InstrumentInfoContextPtr& getInstrInfoContext();

    /// Returns the statistics manager
    static const statistics::StatisticsManagerPtr& getStatisticsManager();
protected:
    static EnvironmentPtr s_environment;
};


/// Generates our default BB environment.
/// This is an optional function to call!  It is possible to avoid the eager
/// construction that this does by handling all the objects by yourself.

struct DefaultInitConfig
{
    bool stop_on_error;
    EnvironmentConfig environment_config;
};
void default_init(bool stop_on_error = true);
void default_init( const DefaultInitConfig& cfg );


/// Use EnvironmentSubscription to keep Lua created objects alive for as long as you need them.
struct EnvironmentSubscription : SubscriptionBase
{
    EnvironmentSubscription(const EnvironmentPtr&);
    virtual ~EnvironmentSubscription();

    bool isValid() const;

private:
    EnvironmentPtr m_env;
};

} // namespace bb

#endif // BB_CORE_ENV_H

#ifndef BB_CORE_COUNTABLE_H
#define BB_CORE_COUNTABLE_H

/* Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved. */


#include <boost/intrusive_ptr.hpp>
#include <boost/noncopyable.hpp>

namespace bb {

// A fairly typical reference counting base class. Declaring
// intrusive_ptr_add_ref and intrusive_ptr_release in this namespace
// relies on argument dependent lookup to enable classes that derive
// from Countable to be used with boost::instrusive_ptr automatically.
//
// Most often, you want Countable< some-integral-type >. If you
// require some sort of memory ordering on refcount manipulation,
// consider Countable< tbb::atomic< int > >, which
// should do the right thing.

template<typename T = int32_t>
class Countable : public boost::noncopyable
{
public:
    inline Countable()
        : m_count( 0 ) {}

    virtual ~Countable() {}

    inline void add_ref() const
    {
        ++m_count;
    }

    inline void release() const
    {
        --m_count;
        if( m_count == 0 )
        {
            // cast away the const'ness we are destructing anyway
            Countable * ptr = const_cast<Countable*>(this);
            ptr->destructObject();
        }
    }

    T use_count() const { return m_count; }

    virtual void destructObject()
    {
        delete this;
    }
private:
    mutable T m_count;
};

template<typename T>
inline void intrusive_ptr_add_ref( const Countable<T>* const countable )
{
    countable->add_ref();
}

template<typename T>
inline void intrusive_ptr_release( const Countable<T>* const countable )
{
    countable->release();
}

} // namespace bb

#endif // BB_CORE_COUNTABLE_H

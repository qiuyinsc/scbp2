#ifndef BB_CORE_CONV_UTILS_H
#define BB_CORE_CONV_UTILS_H

/* Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved. */


#include <cstdlib>
#include <string>
#include <limits>
#include <algorithm>

#include <boost/range.hpp>
#include <boost/type_traits.hpp>
#include <boost/static_assert.hpp>

#include <bb/core/bbint.h>
#include <bb/core/bbassert.h>
#include <bb/core/Error.h>

namespace {
    template <typename T> struct array_traits;
    template <typename T, unsigned int N> struct array_traits<T[N]>
    {
       static const unsigned int size = N;
    };
}

namespace bb {

//
// Templatized string to numerical conversions
//

template<typename T>
inline T char2enum(char c)
{
    return static_cast<T>(c);
}

template<typename T>
inline T char2num(const char& c)
{
    if ('0' <= c && c <= '9')
        return static_cast<T>(c - '0');
    if ('A' <= c && c <= 'Z') // greater than base 10 (to 36) conversions
        return static_cast<T>(c - 'A' + 10);
    return 0;
}

template<typename T>
inline char num2char(T num)
{
    if (0 <= num && num <= 9)
        return num + '0';
    if (10 <= num && num <= 35)
        return num + 'A' - 10;
    BB_THROW_ERROR_SS("cannot convert number to character: " << num);
}

/// specialization for unsigned numbers
template<>
inline char num2char(uint32_t num)
{
    if (num <= 9)
        return num + '0';
    if (10 <= num && num <= 35)
        return num + 'A' - 10;
    BB_THROW_ERROR_SS("cannot convert number to character: " << num);
}

template<>
inline char num2char(uint64_t num)
{
    if (num <= 9)
        return num + '0';
    if (10 <= num && num <= 35)
        return num + 'A' - 10;
    BB_THROW_ERROR_SS("cannot convert number to character: " << num);
}

/// Fixed length string/buffer to number conversion
/// Requires buffer contents to be left-justified
///   If string does not contain blanks and is decimal use
///   fixedstr2num_noloop as it is faster
template<typename T, int len, class Chr>
inline
typename boost::disable_if<boost::is_array<Chr>, T>::type
fixedstr2num(const Chr* str)
{
    // this won't catch all problems, for instance if uint8_t can be max 255 we will
    // bark at converting '1000' but will happily try and overflow on '300'.
    BOOST_STATIC_ASSERT( len > 0 );
    BOOST_STATIC_ASSERT( len <= std::numeric_limits<T>::digits10 + 1 );

    T ret = 0;
    unsigned int i = 0;
    for (; i < len && isdigit(str[i]); ++i)
        ret = ret * 10 + char2num<T>(str[i]);
    for (; i < len; ++i) // remainder of string should not contain numbers
        BB_THROW_EXASSERT_SSX(!isdigit(str[i]), "invalid numeric string: " << std::string(str, len));
    return ret;
}

/// We are using boost enable_if here to resolve ambiguity between this function
/// and the previous fixedstr2num. This wasn't necessary in C++98, but is in C++11 onwards.
template<typename T, class Chr>
inline
typename boost::enable_if<boost::is_array<Chr>, T>::type
fixedstr2num( const Chr & str)
{
    // this won't catch all problems, for instance if uint8_t can be max 255 we will
    // bark at converting '1000' but will happily try and overflow on '300'.
    BOOST_STATIC_ASSERT( (size_t)array_traits<Chr>::size > (size_t)0 );
    BOOST_STATIC_ASSERT( (size_t)array_traits<Chr>::size <= (size_t)std::numeric_limits<T>::digits10 + 1 );

    T ret = 0;
    unsigned int i = 0;
    for (; i < boost::size(str) && isdigit(str[i]); ++i)
        ret = ret * 10 + char2num<T>(str[i]);
    for (; i < boost::size(str); ++i) // remainder of string should not contain numbers
        BB_THROW_EXASSERT_SSX(!isdigit(str[i]), "invalid numeric string: " << std::string(str, boost::size(str)));
    return ret;
}

template<typename T, int len>
inline T fixedstr2num(const unsigned char* str)
{
    return fixedstr2num<T, len>(reinterpret_cast<const char*>(str));
}
/// Fixed length string/buffer to number conversion with base 10
/// This function skips error checks
template<typename T, int len>
inline T fixedstr2num_noloop(const char * str)
{
    BOOST_STATIC_ASSERT( len <= 14 );
    ///
    ///  This function unrolls the loop for converting a string to an integer.  Each character
    ///  is converted into a number using the following formula:
    ///    (c - '0')*10^p
    ///   where p is the position of the chracter in the string
    ///         c is the character representation of the number
    ///   Each character is processed sequentially.  but since
    ///   '0'*10^p is a cummulative constant we can just subtract it
    ///   at the end
    uint64_t  value_ = 0;

    /// perform the base conversion first. we start with the most significant
    /// character and then "fall-through" to the least significant. Because the
    /// value of len is a compile-time constant there is no branching at runtime
    ///  Note that because the subtraction does not happen until the next code block
    ///  the result of this code block is not the real number
    switch(len){
    case 14:    value_ += str[len-14]*10000000000000UL;
    case 13:    value_ += str[len-13]*1000000000000UL;
    case 12:    value_ += str[len-12]*100000000000UL;
    case 11:    value_ += str[len-11]*10000000000UL;
    case 10:    value_ += str[len-10]*1000000000UL;
    case  9:    value_ += str[len- 9]*100000000UL;
    case  8:    value_ += str[len- 8]*10000000;
    case  7:    value_ += str[len- 7]*1000000;
    case  6:    value_ += str[len- 6]*100000;
    case  5:    value_ += str[len- 5]*10000;
    case  4:    value_ += str[len- 4]*1000;
    case  3:    value_ += str[len- 3]*100;
    case  2:    value_ += str[len- 2]*10;
    case  1:    value_ += str[len- 1];
    }

    /// perform the subtraction of the '0' character at each position.
    /// For each len this is ('0'*10^len + '0'*10^(len-1) ... '0'*10^0)
    /// Because the value of len is a compile-time constant there is no
    /// branching at runtime
    switch(len){
    case 14:    value_ -= static_cast<T>('0') * 11111111111111UL; break;
    case 13:    value_ -= static_cast<T>('0') * 1111111111111UL;  break;
    case 12:    value_ -= static_cast<T>('0') * 111111111111UL;   break;
    case 11:    value_ -= static_cast<T>('0') * 11111111111UL;    break;
    case 10:    value_ -= static_cast<T>('0') * 1111111111UL;     break;
    case  9:    value_ -= static_cast<T>('0') * 111111111UL;      break;
    case  8:    value_ -= static_cast<T>('0') * 11111111UL;       break;
    case  7:    value_ -= static_cast<T>('0') * 1111111UL;        break;
    case  6:    value_ -= static_cast<T>('0') * 111111UL;         break;
    case  5:    value_ -= static_cast<T>('0') * 11111UL;          break;
    case  4:    value_ -= static_cast<T>('0') * 1111UL;           break;
    case  3:    value_ -= static_cast<T>('0') * 111UL;            break;
    case  2:    value_ -= static_cast<T>('0') * 11UL;             break;
    case  1:    value_ -= static_cast<T>('0');
    }
    return value_;
}

/// Fixed length string/buffer to number conversion with specified base
/// Input can be padded with spaces on the left or right or both, but cannot be all blank
template<typename T, size_t len, int base>
inline T fixedstr2num(const char* str)
{
    BOOST_STATIC_ASSERT(base > 0 && base <= 36);

    T ret = 0;
    const char* it = str;
    const char* const end = str + len;
    for (; it < end && *it == ' '; ++it); // no body
    BB_THROW_EXASSERT_SSX(it < end, "empty numeric string");
    for (; it < end; ++it)
    {
        T num = char2num<T>( *it );
        if ((num == 0 && *it != '0') || num >= base) // reached invalid input
        {
            BB_ASSERT(*it == ' ');
            break;
        }

        if (ret != 0 && ret > std::numeric_limits<T>::max() / base)
            BB_THROW_ERROR_SS("overflow converting string of length " << len
                    << " to base " << base << ": " << std::string(str, len));

        ret = ret * base + num;
    }
    for (; it < end; ++it) // remainder of string should not contain numbers
        BB_THROW_EXASSERT_SSX(char2num<T>( *it ) == 0 && *it != '0', "invalid numeric string: " << std::string(str, len));
    return ret;
}

/// Known length string/buffer to number conversion with specified base
/// Input can be padded with spaces on the left or right or both, but cannot be all blank
/// Leading zeroes will be ignored.
template<typename T, int base>
inline T fixedstr2num(const char* const str, const char* const end)
{
    BOOST_STATIC_ASSERT(base > 0 && base <= 36);

    T ret = 0;
    const char* it = str;
    for (; it < end && *it == ' '; ++it); // no body
    BB_THROW_EXASSERT_SSX(it < end, "empty numeric string");
    for (; it < end && *it == '0'; ++it); // no body
    for (; it < end; ++it)
    {
        T num = char2num<T>( *it );
        if ((num == 0 && *it != '0') || num >= base) // reached invalid input
        {
            BB_ASSERT(*it == ' ');
            break;
        }

        if (ret != 0 && ret > std::numeric_limits<T>::max() / base)
            BB_THROW_ERROR_SS("overflow converting string of length " << end - str
                    << " to base " << base << ": " << std::string(str, end));

        ret = ret * base + num;
    }
    for (; it < end; ++it) // remainder of string should not contain numbers
        BB_THROW_EXASSERT_SSX(char2num<T>( *it ) == 0 && *it != '0', "invalid numeric string: " << std::string(str, end));
    return ret;
}

template<size_t len, int base, typename T>
inline void num2fixedstr(char* str, T value)
{
    BOOST_STATIC_ASSERT(len > 1);
    BOOST_STATIC_ASSERT(base > 0 && base <= 36);
    BOOST_STATIC_ASSERT(!std::numeric_limits<T>::is_signed);
    BB_ASSERT(value >= 0);

    char* it = str, * const end = str + len;
    if (value == 0) // special case else output will be empty string
        *it++ = '0';
    else
    {
        for (; value != 0 && it != end; ++it)
        {
            *it = num2char(value % base);
            value /= base;
        }
        BB_ASSERT(value == 0); // detect overflow
        std::reverse(str, it);
    }
    std::fill(it, end, ' ');
}

// like num2fixedstr but the result is right-aligned
// pad_char is what is filled to the left of the value
// this one also throws in release builds, rather than just using debug asserts
template<int base, typename T>
inline void num2fixedstrright(char* str, char* end, T value, const char pad_char = ' ')
{
    BB_THROW_EXASSERT(str < end, "num2fixedstrright: target has zero or negative length");
    BOOST_STATIC_ASSERT(base > 0 && base <= 36);
    BOOST_STATIC_ASSERT(!std::numeric_limits<T>::is_signed);

    char* it = end - 1;
    if (value == 0) // special case else output will be empty string
        *it-- = '0';
    else
    {
        for (; value != 0 && it >= str; --it)
        {
            *it = num2char(value % base);
            value /= base;
        }
        BB_THROW_EXASSERT(value == 0, "num2fixedstrright: target too small for value" );
    }

    if( str <= it )
        std::fill(str, it + 1, pad_char);
}

//
// Convenience string to numerical conversions (base 10)
//

inline int chartoi(const char& c)
{
    return char2num<int>(c);
}

inline long strtol10(const char* str)
{
    return std::strtol(str, NULL, 10);
}

inline long strtol10(const std::string &str)
{
    return strtol10(str.c_str());
}

inline unsigned long strtoul10(const char* str)
{
    return std::strtoul(str, NULL, 10);
}

inline unsigned long strtoul10(const std::string& str)
{
    return strtoul10(str.c_str());
}

template<int len>
inline unsigned long fstrtoul(const char* str)
{
    return fixedstr2num<unsigned long, len, 10>(str);
}

template<int len>
inline unsigned long fstrtoul(const unsigned char* str)
{
    return fstrtoul<len>(reinterpret_cast<const char*>(str));
}

inline double strtod(const char* str)
{
    return std::strtod(str, NULL);
}

inline double strtod(const unsigned char* str)
{
    return strtod(reinterpret_cast<const char*>(str));
}

inline double strtod(const std::string& str)
{
    return strtod(str.c_str());
}


//
// uint64_t to std::string conversions
//

/// Returns num as a base10 string
inline std::string toBase10( uint64_t num )
{
    static const char base10_letters[] = "0123456789";
    const size_t kBuffSize = 32;
    char buff[kBuffSize];  // longest is 18446744073709551616 (20 chars)
    uint8_t i = kBuffSize;
    while ( num != 0 )
    {
        uint8_t mod = num % 10;
        num = num / 10;
        BB_ASSERT( i >= 1 );
        buff[--i] = base10_letters[mod];
    }
    if ( i == kBuffSize )
        return "0";
    return std::string( &buff[i], kBuffSize - i );
}

/// Returns num as a base26 string
inline std::string toBase26( uint64_t num )
{
    static const char base26_letters[] = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
    const size_t kBuffSize = 32;
    char buff[kBuffSize];
    uint8_t i = kBuffSize;
    while ( num != 0 )
    {
        uint8_t mod = num % 26;
        num = num / 26;
        BB_ASSERT( i >= 1 );
        buff[--i] = base26_letters[mod];
    }
    if ( i == kBuffSize )
        return std::string( 1, base26_letters[0] );
    return std::string( &buff[i], kBuffSize - i );
}

/// Returns num as a base36 string
inline std::string toBase36( uint64_t num )
{
    static const char base36_letters[] = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ";
    const size_t kBuffSize = 32;
    char buff[kBuffSize];  // longest is 13 chars
    uint8_t i = kBuffSize;
    while ( num != 0 )
    {
        uint8_t mod = num % 36;
        num = num / 36;
        BB_ASSERT( i >= 1 );
        buff[--i] = base36_letters[mod];
    }
    if ( i == kBuffSize )
        return "0";
    return std::string( &buff[i], kBuffSize - i );
}


} // namespace bb

#endif // BB_CORE_CONV_UTILS_H

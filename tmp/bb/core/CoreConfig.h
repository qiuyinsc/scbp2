#ifndef BB_CORE_CORECONFIG_H
#define BB_CORE_CORECONFIG_H

/* Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved. */


#include <bb/core/loglevel.h>
#include <bb/core/Msg.h>
#include <bb/core/ptime.h>
#include <bb/core/source.h>
#include <bb/core/EFeedDest.h>
#include <bb/core/EFeedType.h>
#include <bb/core/LuaConfig.h>
#include <bb/core/MsgPrintFilter.h>
#include <bb/core/SymbolTableConfig.h>
#include <bb/core/TCPKeepAliveOptions.h>
#include <bb/core/sourceset.h>
#include <bb/core/mktdest.h>

namespace bb {

class LiveMStreamConfig : public LuaConfig<LiveMStreamConfig>
{
public:
    bool logAddFeeds;
    std::string feedInterface;
    boost::optional<std::string> bookServerConfig;


    LiveMStreamConfig();
    static void describe();
};

class HistMStreamConfig : public LuaConfig<HistMStreamConfig>
{
public:
    class SourceConfig : public LuaConfig<SourceConfig>
    {
    public:
        EFeedType             feedType;
        msg_time_source_t     msgTimeSource;
        double                qdUsecDelay;
        double                exchangeUsecDelay;

        SourceConfig();
        static void describe();
    };
    class FeedDelay : public LuaConfig<FeedDelay>
    {
    public:
        source_t feed;
        EFeedDest dest;
        double delay;
        mktdest_t market;

        FeedDelay();
        static void describe();
    };

    class OriginWildcard : public LuaConfig<OriginWildcard>
    {
    public:
        EFeedType feedType;
        EFeedDest dest;
        std::vector<EFeedOrig> allOrigins;

        OriginWildcard();
        static void describe();
    };

    bool ignoreMissingFiles;
    bool logMissingFiles;
    bool logAddFeeds;
    bool logAddDataByMType;
    bool enableAddDataByMType;
    EFeedDest runtimeFeedDest;
    std::vector<FeedDelay> feedDelays;
    std::vector<OriginWildcard> originWildcards;
    std::vector<SourceConfig>   sourceConfigs;

    HistMStreamConfig();
    static void describe();
};

class MultipathFeedConfig : public LuaConfig<MultipathFeedConfig>
{
public:
    mktdest_t mkt;
    EFeedDest dest;
    sourceset_t sources;
    source_t primarySource;

    MultipathFeedConfig();
    static void describe();
};

class TDConnConfig : public LuaConfig<TDConnConfig>
{
public:
    class LoginTestReq : public LuaConfig<LoginTestReq>
    {
    public:
        std::string text;
        uint32_t count;
        uint32_t targetCWnd;

        LoginTestReq();
        static void describe();
    };

    // A lua-bound subclass of the BB TCPKeepAliveOptions
    class TCPKeepAliveOptions :
        public ::bb::TCPKeepAliveOptions,
        public LuaConfig<TCPKeepAliveOptions>
    {
    public:
        TCPKeepAliveOptions();
        static void describe();
    };

    bool doAuthentication;
    bool useTcpNoDelay;
    bool useTcpQuickAck;
    bool useTcpKeepAlive;
    bool useNonBlockingSend;
    size_t unixSocketSendBufSize;
    std::string unixSocketPath;

    TCPKeepAliveOptions tcpKeepAliveOptions;
    LoginTestReq loginTestRequest;

    TDConnConfig();
    static void describe();
    std::string getUnixSocketPath( uint32_t port );
};

class ISOBookConfig : public LuaConfig<ISOBookConfig>
{
public:
    bool strictGapDetection;

    ISOBookConfig();
    static void describe();
};

class FeedConfig : public LuaConfig<FeedConfig>
{
public:
    class CheckSeqNum : public LuaConfig<CheckSeqNum>
    {
    public:
        loglevel_t level;
        ptime_duration_t period;
        std::vector<EFeedType> sources;
        bool ignoreAll;
        static void describe();
        CheckSeqNum();
    };

    uint16_t multicastTTL;
    CheckSeqNum checkSequenceNumber;
    boost::optional<uint32_t> udpRecvSocketBufferSize;
    bool                      useHwTimestamps;
    FeedConfig();
    static void describe();
};

class BookConfig
    : public LuaConfig<BookConfig>
{
public:
    static void describe();
    BookConfig();

    bool        useAlphaEntryTime;
};

class CoreConfig : public LuaConfig<CoreConfig>
{
public:
    sourceset_t getMultipathFeedSources( const mktdest_t& mkt, const EFeedDest& dest );
    source_t getMultipathPrimarySource( const mktdest_t& mkt, const EFeedDest& dest );

    bool enableLivePing;
    bool disableLiveAlert;
    ISymbolTableFactoryPtr symbolTableConfig;
    LiveMStreamConfig liveMStreamConfig;
    HistMStreamConfig histMStreamConfig;
    TDConnConfig tdConnection;
    FeedConfig feeds;
    ISOBookConfig isoBook;
    BookConfig     bookConfig;
    MsgPrintFilter msgPrint;
    std::string datafileDbProfile;
    std::vector<MultipathFeedConfig> multipathFeeds;

    CoreConfig();
    static void describe();
};

} // namespace bb

#endif // BB_CORE_CORECONFIG_H

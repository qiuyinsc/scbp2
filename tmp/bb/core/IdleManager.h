#ifndef BB_CORE_IDLEMANAGER_H
#define BB_CORE_IDLEMANAGER_H

/* Contents Copyright 2016 Athena Capital Research LLC. All Rights Reserved. */

#include <bb/core/smart_ptr.h>
#include <bb/core/EventPublisher.h>


namespace bb{


class OnIdleListener
    : public IEventSubscriber
{
public:
    // return true if work was done, otherwise return false
    virtual bool onIdle() = 0;
};

typedef bb::EventPublisher<OnIdleListener>  IdlePublisher;

class IdleManager
{
public:
    IdleManager();

    virtual ~IdleManager(){}
    // Allow the objects to perform
    // maintenance tasks when an OnIdle message is received.
    void addIdleListener( OnIdleListener* l);

    void onIdle( );

    void setIdleAttempts( uint32_t attempts ) { m_idleAttempts = attempts; }
    void setServiceCount( uint32_t count )    { m_serviceCount = count; };

protected:

    struct notifyOnIdle : public IdlePublisher::INotifier
    {
        notifyOnIdle( OnIdleListener* callback) : IdlePublisher::INotifier ( callback ){}
        bool notify()
        {
            return m_callback->onIdle();
        }
    };

    IdlePublisher           m_idleListeners;
    uint32_t                m_idleAttempts;
    uint32_t                m_serviceCount;
};

BB_DECLARE_SHARED_PTR( IdleManager );

} //end namespace bb

#endif // BB_CORE_IDLEMANAGER_H

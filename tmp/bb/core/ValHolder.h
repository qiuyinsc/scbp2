#ifndef BB_CORE_VALHOLDER_H
#define BB_CORE_VALHOLDER_H

/* Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved. */


#include <tr1/array>

namespace bb {

///this is a utility to help us from mixing up integers and enums
/// enum Numbers{Zero,One,Two};
/// enum Ordinals {First,Second,Third};
/// ValHolder<T,Numbers,3} nums;
/// nums[1];//will not compile with warnings==errors
/// nums[First];//will not compile with warnings==errors
/// nums[Zero];//OK
template <typename Val, typename EnumType, std::size_t N>
struct ValHolder : std::tr1::array<Val,N>
{
    typedef std::tr1::array<Val,N> arr_type;

    Val& operator[]( EnumType y )
    {
        return this->arr_type::operator[]( static_cast<std::size_t>( y ) );
    }

    Val const& operator[]( EnumType y ) const
    {
        return this->arr_type::operator[]( static_cast<std::size_t>( y ) );
    }
};

} // namespace bb

#endif // BB_CORE_VALHOLDER_H

#ifndef BB_CORE_MSG_H
#define BB_CORE_MSG_H

/* Contents Copyright 2016 Athena Capital Research LLC. All Rights Reserved. */

#include <iosfwd>
#include <bitset>
#include <stdint.h>

#ifndef UINT32_MAX
#define UINT32_MAX  (0xffffffff)
#endif

#include <boost/noncopyable.hpp>
#include <boost/optional/optional.hpp>

#include <bb/core/acct.h>
#include <bb/core/timeval.h>
#include <bb/core/mtype.h>
#include <bb/core/smart_ptr.h>
#include <bb/core/source.h>
#include <bb/core/symbol.h>
#include <bb/core/fpmath.h>
#include <bb/core/Error.h>
#include <bb/core/instrument.h>
#include <bb/core/MarketLevel.h>

namespace bb {

/// The maximum total size of a message.
const size_t MSG_MAXSIZE = 1024; // this includes the message header
/// The alignment of messages guaranteed by our messaging system.
const size_t MSG_MAXALIGN = 8;

///  Enum to define which time to use for the message
///
enum msg_time_source_t{ HDR_TIME, ORIGIN_TIME, NIC_TIME };


/// Message modifiers.  From at least 2008 to early 2011 these have not been used much.
enum
{
    MOD_NONE         =  0, // default, used for live data and most data in general
    MOD_HIST         =  1, // was used by histd once upon a time
    MOD_EXTERN       =  2, // downloaded transaction data, margin commands such as msendfill
    MOD_BOOK_REFRESH =  4, // was this ever used?
    MOD_SIM_TRADER   =  8, // for messages produced by the simulator
    MOD_ZEROSPLIT    = 16, // quotes split after recording them into a SYM_UNKNOWN file
    MOD_PROTOBUF     = 32  // Google protobuf message
};


typedef uint32_t MsgSizeField_t;

/// message_t encapsulates all the information we need to pass a
/// message from point to point (or point to many points).
/// sizeof(message_hdr_t) == 40 bytes.
struct message_hdr_t
{
    static const uint32_t kEndianSentinelValue = 0x01020304;

    source_t source;
    symbol_t symbol;
    mtype_t mtype;
    uint32_t unused;     // was "sig" before; seems that was always zero, this should be too
    timeval_t time_sent; // time the message was produced by a BB program (not exchange time)
    MsgSizeField_t size; // size of data portion (not including header)
    uint32_t seq;        // for gap detection (zero for streams with no coherent sequencing, e.g. SRC_INFO)
    uint32_t endian;     // store 0x01020304 so that we can determine endian as well as guard against null headers
    uint32_t modifier;   // bitwise combination of the MOD_* enum values (usually zero)
};

typedef std::bitset<128> message_hdr_unique_id;
message_hdr_unique_id unique_id( const message_hdr_t& hdr );
std::string message_hdr_unique_id_to_str( const message_hdr_unique_id& id );
typedef double LimitUp;
typedef double LimitDown;
struct LimitPair : public std::pair<LimitUp,LimitDown>
{
    LimitPair(double limit_up, double limit_down) :
        std::pair<LimitUp, LimitDown>(limit_up, limit_down)
    {
        BB_THROW_EXASSERT_SSX ( bb::GT(up(),dn()), "Limit up should be higher than limit dn!" );
        BB_THROW_EXASSERT_SSX ( bb::GT(limit_up,limit_down), "Limit up should be higher than limit dn!" );
    }
    double const& up() const { return first; }
    double const& dn() const { return second; }
};

/// The maximum total size of a message's data portion.
const size_t MSG_MAXDATA = MSG_MAXSIZE-sizeof(message_hdr_t);


struct MsgHdr : public message_hdr_t
{
    std::ostream& print(std::ostream &out) const;
    std::ostream& print_json(std::ostream &) const;
    static bool print_human_readable_mtype;
};

/// The structs below let us hash message headers and compare on certain fields only.
/// At the moment, it is possible to hash by any combination of 2 fields that can be Source, Symbol or MType.
/// The EventDistributor is hashing by Symbol and MType only, but there's no reason other components wouldn't hash by something else.
///
/// There is nothing to prevent us from adding a template overload that takes all three fields, or from extending this list of fields
/// to encompass more.
enum MsgHdrKeyField
{
    KeySource,
    KeySymbol,
    KeyMType
};

template<MsgHdrKeyField a, MsgHdrKeyField b>
struct MsgHdrKey
{
    public:
        MsgHdrKey ( bb::MsgHdr const & hdr )
            : field_one ( getMsgField < a > ( hdr ) )
            , field_two ( getMsgField < b > ( hdr ) )
        {
        }
        const uint32_t field_one;
        const uint32_t field_two;

    protected:
        template<typename A, typename B>
        MsgHdrKey ( const A s, const B mt )
            : field_one ( static_cast < const uint32_t > ( s ) )
            , field_two ( static_cast < const uint32_t > ( mt ) )
        {
        }

    private:
        template <MsgHdrKeyField field>
        static uint32_t getMsgField ( bb::MsgHdr const & hdr )
        {
            BOOST_STATIC_ASSERT ( field == KeySource || field == KeySymbol || field == KeyMType );
            BOOST_STATIC_ASSERT ( SYM_MAX < UINT32_MAX );
            BOOST_STATIC_ASSERT ( MAX_VALID_MTYPE_LEN < UINT32_MAX );
            BOOST_STATIC_ASSERT ( sizeof(hdr.symbol) == sizeof(uint32_t) && sizeof(symbol_t) == sizeof(uint32_t) );
            BOOST_STATIC_ASSERT ( sizeof(hdr.mtype) == sizeof(uint32_t) && sizeof(mtype_t) == sizeof(uint32_t) );
            switch(field)
            {
                case KeySource:
                    return static_cast < const uint32_t > ( hdr.source.id() );
                case KeySymbol:
                    return static_cast < const uint32_t > ( hdr.symbol );
                case KeyMType:
                    return static_cast < const uint32_t > ( hdr.mtype );
                default:
                    throw std::runtime_error("Unimplemented field. Should adjust BOOST_STATIC_ASSERT above.");
            }
        }
};

struct MsgHdrKeyHasher
{
    public:
        template<MsgHdrKeyField a, MsgHdrKeyField b>
        size_t operator() ( MsgHdrKey<a, b> const & key ) const
        {
            BOOST_STATIC_ASSERT ( sizeof ( key ) == sizeof ( size_t ) );
            return *reinterpret_cast<const size_t*>(&key);
        }
};

struct MsgHdrKeyEqual
{
    public:
        template<MsgHdrKeyField a, MsgHdrKeyField b>
        bool operator() ( MsgHdrKey<a, b> const & left, MsgHdrKey<a, b> const & right ) const
        {
            return left.field_one == right.field_one &&
                left.field_two == right.field_two;
        }
};

/**
   A BB Msg contains a hdr field with the MsgHdr information, and
   a data buffer with info specific to the type. There are getters/setters
   for each field in the data buffer.
   For example, a TestMsg would contain a field "hdr" of type MsgHdr, and body()
   with type type msg_test.

   TestMsg *tmsg = ...
   tmsg->hdr->mtype == bb::MSG_TEST;
   tmsg->body()->a, tmsg->getA(), tmsg->body()->b, etc...

   Its "data" field points to the payload section of the message.
*/
class Msg : public boost::noncopyable
{
    friend class MsgBufferBase;
    friend class MsgBuffer;
    friend class InPlaceMsgBuffer;

public:
    virtual ~Msg()
    {
        // use checked delete to avoid the plt hit to operator delete
        // for frequent case where we do not own the block.
        if( m_ownedBlock )
            delete[] m_ownedBlock;
    }

    MsgHdr *hdr;
    char *data;

    /// Returns a copy of this message.
    Msg *clone() const;

    /// Overwrites the data in m with our data. m's type must match, and
    /// m must have a valid data buffer associated with it!
    void copyToOther(Msg* m) const;

    /// Zeros out this message without reallocating any memory.
    void clear();

    size_t headerSize() const { return sizeof(MsgHdr); }

    /// Returns the size of the data block.
    /// If the application is meant to be message type agnostic, use dataSize and not msgSize.
    /// It is possbile, but unusual, for dataSize != msgSize,
    /// but it can happen when two cooperating programs are built
    /// against different versions of messages.xml.
    MsgSizeField_t dataSize() const { return hdr->size; }

    /// Returns the entire message block - header and data (always contiguous)
    void* asDatagram() const { return hdr; }

    /// Returns the size of the entire message block
    size_t datagramSize() const { return headerSize() + dataSize(); }

    /// Returns the size of the underlying message struct.
    virtual size_t msgSize() const { return 0; }

    /// Returns the mtype determined by class type, not the
    /// contents of hdr.
    virtual mtype_t mtype() const { return MSG_NONE; }

    /// Returns the acct_t from the message (if defined)
    virtual boost::optional<bb::acct_t> acct() const { return boost::none; }

    /// Returns the timestamp from the origin of the message (e.g. the exchange time for quotes).
    virtual boost::optional<timeval_t> originTime() const { return boost::none; }

    /// Returns the timestamp from the nic (i.e. the time the messages was received by the network card at the QD).
    virtual boost::optional<timeval_t> nicTime() const { return boost::none; }

    /// Returns the instrument_t from the message (if defined)
    virtual boost::optional<bb::instrument_t> instrument() const { return boost::none; }

    virtual boost::optional<std::string> exchangeSymbol() const { return boost::none; }

    // Returns the limit up and limit down if both are available
    virtual boost::optional<bb::LimitPair> getLimitUpLimitDown() const { return boost::none; }

    // Returns the best Bid and Offer if both are available
    virtual boost::optional<bb::MarketLevel>   getBbo() const { return boost::none; }

    /// Returns the last trade price from the message (if defined)
    virtual boost::optional<double> tradePx() const { return boost::none; }

    /// Returns the volume of an instr from the message (if defined)
    virtual boost::optional<uint32_t> totalVolume() const { return boost::none; }

    /// Returns the class name.
    virtual const char *name() const { return "Msg"; }

    /// Fills the Msg's header with commonly-used fields.
    /// Does nothing if there is no header allocated yet.
    void setHeader( symbol_t sym, source_t src, uint32_t seq = 0, timeval_t tv_sent = timeval_t( timeval_t::now ) )
    {
        if (hdr)
        {
            hdr->symbol = sym;
            hdr->source = src;
            hdr->time_sent = tv_sent;
            hdr->seq = seq;
        }
    }

    /// Prints this Message to OUT.
    /// This implementation will be used for unrecognized message types.
    virtual std::ostream &print(std::ostream &out) const;

    /// Prints this message in JSON form.
    virtual std::ostream &print_json(std::ostream &out) const;

    static const size_t MAXSIZE = bb::MSG_MAXSIZE;

protected:
    /// Allocates a block of data to fit both the header and data
    /// This block is owned by Msg and will be initialized to 0 values.
    Msg(mtype_t mtype, size_t msgSize);

    /// Sets this class up to use this data block. Ownership of the block is not taken.
    Msg(char *block)
        : hdr(reinterpret_cast<bb::MsgHdr*>(block))
        , data(block ? block + sizeof(MsgHdr) : NULL)
        , m_ownedBlock(NULL)
    {
    }

    /// Allocates a Msg of its own type.  For example, if A is a
    /// child of Msg, then A::mkself would return an instance of A.
    virtual Msg *mkself() const { return new Msg(MSG_NONE, 0); }

    /// Overwrites the data in m with our data. m's type must match, and
    /// m must have a valid data buffer associated with it!
    void copyData(Msg *m) const;

    /// if this is non-null, this block will be deleted on exit.
    char *m_ownedBlock;
};
BB_DECLARE_SHARED_PTR(Msg);


/// The largest a C++ wrapper object can be -- as of now, none of the auto-generated wrappers
/// are larger than Msg itself. We recently introduced ProtoBufMsg which contains an
/// additional shared_ptr member. As the definition of ProtoBufMsgBase is not visible from here,
/// we only add the size of shared_ptr to the MSG_MAX_WRAPPER_SIZE. To be safe in the future, we
/// also added a static assertion to make sure the size of any msg object is no bigger than
/// MSG_MAX_WRAPPER_SIZE.
static const size_t MSG_MAX_WRAPPER_SIZE = sizeof(Msg) + sizeof(boost::shared_ptr<void>);


inline std::ostream &operator <<(std::ostream &out, const bb::MsgHdr &hdr)
{
    return hdr.print(out);
}

inline std::ostream &operator <<(std::ostream &out, const bb::Msg &m)
{
    return m.print(out);
}

} // namespace bb

#endif // BB_CORE_MSG_H

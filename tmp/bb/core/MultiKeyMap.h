#ifndef BB_CORE_MULTIKEYMAP_H
#define BB_CORE_MULTIKEYMAP_H

/* Contents Copyright 2016 Athena Capital Research LLC. All Rights Reserved. */

#include <set>

#include <boost/unordered_map.hpp>
#include <boost/foreach.hpp>
#include <boost/range/adaptor/map.hpp>

namespace bb {

template<typename MULTI_KEY, typename UNIQUE_KEY, typename VALUE>
class MultiKeyMap
{
    public:
    // TODO - Make the iterator look like std::pair<UNIQUE_KEY, VALUE>
    //        SEE DEV-3439 changes for an earlier attempt
    typedef std::pair< std::set<MULTI_KEY>, VALUE> inner_value_t;
    typedef boost::unordered_map<UNIQUE_KEY, inner_value_t > innermap_t;
    typedef typename innermap_t::iterator iterator;
    typedef typename innermap_t::const_iterator const_iterator;
    typedef boost::unordered_map<MULTI_KEY, iterator> outermap_t;

    iterator find_by_multi ( MULTI_KEY const & key )
    {
        typename outermap_t::iterator outer_iter ( m_outermap.find(key ) );
        if ( outer_iter == m_outermap.end() )
        {
            return m_innermap.end();
        }
        return outer_iter->second;
    }

    const_iterator find_by_multi ( MULTI_KEY const & key ) const
    {
        typename outermap_t::const_iterator outer_iter ( m_outermap.find(key ) );
        if ( outer_iter == m_outermap.end() )
        {
            return m_innermap.end();
        }
        return outer_iter->second;
    }

    iterator find_by_unique ( UNIQUE_KEY const & k )
    {
        return m_innermap.find(k);
    }

    const_iterator find_by_unique ( UNIQUE_KEY const & k ) const
    {
        return m_innermap.find(k);
    }

    const_iterator begin() const
    {
        return m_innermap.begin();
    }

    const_iterator end() const
    {
        return m_innermap.end();
    }

    VALUE & getValue( MULTI_KEY const & key )
    {
        iterator iter = find_by_multi ( key );
        BB_THROW_EXASSERT_SSX ( iter != end(), "Can't find value!" );
        return iter->second.second;
    }

    void insert(UNIQUE_KEY key, VALUE const & value)
    {
        typename innermap_t::iterator iter = m_innermap.find(key);
        if ( iter == m_innermap.end() )
        {
            std::set<MULTI_KEY> empty_vct;
            m_innermap[ key ] = std::make_pair(empty_vct, value);

            // this could have invalidated iterators, so we have to re-map the outer map
            m_outermap.clear();
            for(typename innermap_t::iterator iter = m_innermap.begin();
                    iter != m_innermap.end();
                    iter++)
            {
                inner_value_t const & value = iter->second;
                BOOST_FOREACH(MULTI_KEY const & key, value.first )
                {
                    m_outermap[key] = iter;
                }
            }
        }
        else
        {
            iter->second.second = value;
        }
    }

    void assign(MULTI_KEY const & outer, UNIQUE_KEY const & inner)
    {
        typename innermap_t::iterator inner_iter ( find_by_unique ( inner ) );
        inner_iter->second.first.insert(outer);
        m_outermap[outer] = inner_iter;
        BB_THROW_EXASSERT_SSX ( inner_iter != m_innermap.end(), "Can't map to invalid UNIQUE_KEY" );
    }

    private:
    innermap_t m_innermap;
    outermap_t m_outermap;
};

}

#endif // BB_CORE_MULTIKEYMAP_H

#ifndef BB_CORE_SOURCE_H
#define BB_CORE_SOURCE_H

/* Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved. */


#include <iosfwd>
#include <vector>

#include <bb/core/hash.h>
#include <bb/core/EFeedType.h>
#include <bb/core/EFeedOrig.h>
#include <bb/core/EFeedDest.h>


namespace bb {
class LuaState;

/// A source of message data in the BB system.  A message source
/// is a program that generates and sends messages, such as a quote server.
/// Every source has a type, origin, and destination,
/// which together form a unique 32-bit ID.
/// The type, origin, and destination are automatically generated from
/// source.table, feed_orig.table, and feed_dest.table
class source_t
{
public:
    explicit source_t( EFeedType t = SRC_UNKNOWN, EFeedOrig o = ORIG_UNKNOWN, EFeedDest d = DEST_UNKNOWN )
    {   set( t, o, d ); }

    explicit source_t( uint32_t id )  { m_id = id; }

    /// Converts a string of the format TYPE.ORIG.DEST to a source_t
    /// If any of the latter sections are missing, the constructor defaults are used.
    explicit source_t( const char* str );

    /// Returns the identifier of this source_t.
    uint32_t id() const { return m_id; }

    /// Returns true if the source_t is completely specified with known attributes.
    bool isValid() const { return type() != SRC_UNKNOWN && orig() != ORIG_UNKNOWN && dest() != DEST_UNKNOWN; }

    EFeedType type() const { return m_info.type; }
    EFeedOrig orig() const { return m_info.orig; }
    EFeedDest dest() const { return m_info.dest; }
    uint8_t reserved() const { return m_info.reserved; }

    void set( EFeedType t, EFeedOrig o = ORIG_UNKNOWN, EFeedDest d = DEST_UNKNOWN )
    {   m_info.reserved = 0; setType( t ); setOrig( o ); setDest( d ); }

    void setType( EFeedType t ) { m_info.type = t; }
    void setOrig( EFeedOrig o ) { m_info.orig = o; }
    void setDest( EFeedDest d ) { m_info.dest = d; }

    /// Sets the orig and dest using the results of
    /// source_t::getAutoOrig and source_t::getAutoDest.
    void autoSetOrigDest()
    {   setOrig( getAutoOrig() ); setDest( getAutoDest() ); }

    void setPrimaryOrigDest()
    {   setOrig( getPrimaryOrig( type() ) ); setDest( getPrimaryDest( type() ) ); }

    /// two sources are equal if their id's are equal
    bool operator ==(const source_t& id) const { return m_id == id.m_id; }
    bool operator !=(const source_t& id) const { return m_id != id.m_id; }

    /// order: sort by type, sort by orig, sort by dest
    bool operator < (const source_t& src) const
    {
        if ( this->type() < src.type() )
            return true;
        else if ( this->type() == src.type() )
        {
            if ( this->orig() < src.orig() )
                return true;
            else if ( this->orig() == src.orig() )
                return (this->dest() < src.dest());
        }
        return false;
    }

    bool operator >  (const source_t& src) const { return src < *this; }
    bool operator >= (const source_t& src) const { return !(*this < src); }
    bool operator <= (const source_t& src) const { return !(*this > src); }

    /// Converts this source into a string of the format:  TYPE.ORIG.DEST
    /// Uses conversion form specified by source_t::getOutputFormat
    std::string toString() const;

    /// Converts this source into a string of the format:  TYPE.ORIG.DEST
    /// Uses the FORMAT_SHORT conversion form.
    std::string toStringShort() const;

    /// Converts this source into a string of the format:  TYPE.ORIG.DEST
    /// Uses the FORMAT_LONG conversion form.
    std::string toStringLong() const;

    /// Converts this source into a string of the format:  TYPE
    /// Uses the FORMAT_TYPEONLY conversion form.
    std::string toStringTypeOnly() const;

    /// Stream Output Format specifer.  Accessed with setOutputFormat and getOutputFormat.
    enum EOutputFormat {
        FORMAT_LONG,      /// SRC_ISLD.ORIG_ASSENT.DEST_NYMEX
        FORMAT_SHORT,     /// SRC_ISLD.OASSENT.DNYMEX
        FORMAT_TYPEONLY,  /// SRC_ISLD
    };

    /// Sets how you want operator<< to work.  The default is FORMAT_LONG.
    /// This does not affect toString and the string constructor.
    static void setOutputFormat( EOutputFormat outputFormat ) { ms_outputFormat = outputFormat; }

    /// Returns the current stream output format.
    static EOutputFormat getOutputFormat() { return ms_outputFormat; }

    /// Called once by default_init() to make getHostOrig()/getHostDest() thread-safe.
    static void initHostOrigDest( const LuaState& );

    /// Returns the EFeedOrig based on the current hostname.
    static EFeedOrig getHostOrig();
    /// Returns the EFeedDest based on the current hostname.
    static EFeedDest getHostDest();

    static EFeedOrig getPrimaryOrig( EFeedType t );
    static EFeedDest getPrimaryDest( EFeedType t );

    /// Returns the EFeedOrig or EFeedDest based on the given fully-qualified domain name.  Throws on error.
    /// Not thread-safe (because it uses DefaultCoreContext).
    static EFeedOrig getHostOrig( const std::string& fqdn );
    /// Returns the EFeedDest based on the given fully-qualified domain name.  Throws on error.
    /// Not thread-safe (because it uses DefaultCoreContext).
    static EFeedDest getHostDest( const std::string& fqdn );

    /// Returns the EFeedOrig used by make_auto and autoSetOrigDest.
    /// If not explicitly set, this defaults to getHostOrig()
    static EFeedOrig getAutoOrig()
    {   return (ms_auto_orig != ORIG_UNKNOWN) ? ms_auto_orig : getHostOrig(); }

    /// Returns the EFeedDest used by make_auto and autoSetOrigDest.
    /// If not explicitly set, this defaults to getHostDest()
    static EFeedDest getAutoDest()
    {   return (ms_auto_dest != DEST_UNKNOWN) ? ms_auto_dest  : getHostDest(); }

    /// Sets the EFeedOrig used by make_auto and autoSetOrigDest.
    /// Setting the value to ORIG_UNKNOWN will cause these to default to getHostOrig()
    static void setAutoOrig( EFeedOrig orig )
    {   ms_auto_orig = orig; }

    /// Sets the EFeedDest used by make_auto and autoSetOrigDest.
    /// Setting the value to DEST_UNKNOWN will cause these to default to getHostDest()
    static void setAutoDest( EFeedDest dest )
    {   ms_auto_dest = dest; }

    /// Returns a source_t of the particular EFeedType,
    /// with orig/dest set via autoSetOrigDest.
    static source_t make_auto( EFeedType t )
    {   source_t src( t ); src.autoSetOrigDest(); return src; }

    static source_t make_primary( EFeedType t );

    /// Returns source_t(SRC_ALL, ORIG_ALL, DEST_ALL);
    static source_t all() { return source_t(SRC_ALL, ORIG_ALL, DEST_ALL); }

    struct EFeedTypeCompare
    {
        bool operator ()( const source_t& r1, const source_t& r2 ) const { return r1.type() < r2.type(); }
    };

private:
    union
    {
        uint32_t m_id;
        struct
        {
            EFeedType type;     ///  0- 7 (SRC_ISLD, SRC_ARCA, etc.)
            uint8_t   reserved; ///  7-15 (0)
            EFeedOrig orig;     /// 16-23 (ORIG_CARTERET, ORIG_SPIKE, etc.)
            EFeedDest dest;     /// 22-31 (DEST_CARTERET, DEST_SPIKE, etc.)
        } m_info;
    };

    /// the output format
    static EOutputFormat ms_outputFormat;

    /// the auto values
    static EFeedOrig    ms_auto_orig;
    static EFeedDest    ms_auto_dest;
};

// convenient declaration of a vector of sources
typedef std::vector<source_t> sources_t;

// hashing function
inline std::size_t hash_value( const source_t& src )
{
    boost::hash<int32_t> hasher;
    return hasher( src.id() );
}


/// Stream operator to convert source_t into human readable form.
std::ostream &operator <<(std::ostream &out, const source_t &src);

std::istream &operator >>(std::istream &in, source_t &src);

} // namespace bb


BB_HASH_NAMESPACE_BEGIN {

template<> struct hash<bb::source_t>
{
public:
    size_t operator ()(const bb::source_t &src) const { return h(src.id()); }
private:
    hash<int> h;
};

} BB_HASH_NAMESPACE_END

#endif // BB_CORE_SOURCE_H

#ifndef BB_CORE_MATH_H
#define BB_CORE_MATH_H

/* Contents Copyright 2016 Athena Capital Research LLC. All Rights Reserved. */

///
/// returns 1, 0  or -1
///
template<typename T>
int sgn( T val )
{
    return (T(0) < val) - (val < T(0));
}

#endif // BB_CORE_MATH_H

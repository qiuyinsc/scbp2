#ifndef BB_CORE_SDRUNQUEUE_H
#define BB_CORE_SDRUNQUEUE_H

/* Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved. */


#include <boost/noncopyable.hpp>

#include <bb/core/bbassert.h>

namespace bb {

// Custom linked-list implementation (doubly-linked) for the run-queue
// in SelectDispatcher::select. Each SDReactor has pointers embedded
// so that it can belong to a run-queue. The run-queue heads are
// stored inside an SDPolicy, and and when a policy runs it'll 
// pass through the queue and dispatch into the reactors.
//
// boost::intrusive_list would work too, but in profiles this
// looks around 5% faster.

struct SDReactorBase;
struct sd_queue_link_t;

/// Stores next/prev ptrs.
struct sd_queue_ptrs_t : public boost::noncopyable
{
protected:
    friend struct sd_queue_link_t;
    friend struct sd_queue_base_t;
    friend struct sd_queue_loan_base_t;
    enum NoInit_t { NOINIT };
    sd_queue_ptrs_t(NoInit_t) {}
    sd_queue_ptrs_t(sd_queue_ptrs_t *prev, sd_queue_ptrs_t *next) : m_prev(prev), m_next(next) {}
    sd_queue_ptrs_t *m_prev, *m_next;
    sd_queue_link_t *pop();
    bool empty() const { return m_next == this; }
};

/// An element of an SD queue.
struct sd_queue_link_t : public sd_queue_ptrs_t
{
    sd_queue_link_t() : sd_queue_ptrs_t(NULL, NULL) {}
    ~sd_queue_link_t();
    bool is_linked() { return m_next != NULL; }
    void unlink();
};

struct sd_queue_base_t : public sd_queue_ptrs_t
{
    using sd_queue_ptrs_t::empty;
protected:
    sd_queue_base_t() : sd_queue_ptrs_t(this, this) {}
    sd_queue_base_t(NoInit_t) : sd_queue_ptrs_t(NOINIT) {}
    ~sd_queue_base_t();
    friend struct SDReactorBase;
    // Pushes the given object onto the queue.
    // If the object is already linked to some queue, this is a noop.
    void push(sd_queue_link_t *elt);
};

struct sd_queue_loan_base_t : public sd_queue_ptrs_t
{
    using sd_queue_ptrs_t::empty;
protected:
    sd_queue_loan_base_t(sd_queue_base_t &e);
    ~sd_queue_loan_base_t();
    sd_queue_base_t &m_queue;
};

/// Doubly-linked list head.
template<typename T>
struct sd_queue_t : public sd_queue_base_t
{
    sd_queue_t() {}
    // Pushes the given object onto the queue.
    // If the object is already linked to some queue, this is a noop.
    void push(T *elt);
    T *pop();
protected:
    sd_queue_t(NoInit_t) : sd_queue_base_t(NOINIT) {}
};

/// Tricky: a loan object will splice all the objects out of a given sd_queue_t.
/// Then, it can be treated like a normal queue.
///
/// On destruction, all remaining elements will be spliced back into the queue.
/// This is implemented under the assumption that it's  an infrequent operation
/// (probably only for exception handling).
/// 
/// This exists so it'd be possible to implement sd_queue_t as an allocation-free
/// vector instead of a linked list.
template<typename T>
struct sd_queue_loan_t : public sd_queue_loan_base_t
{
    sd_queue_loan_t(sd_queue_t<T> &e) : sd_queue_loan_base_t(e) {}
    T *pop();
};



///////////////////////////////////////////////////////////////////////////////////
// Inline impl
///////////////////////////////////////////////////////////////////////////////////

inline sd_queue_link_t::~sd_queue_link_t()
{
    unlink(); // auto-unlink on destruction
}

inline void sd_queue_link_t::unlink()
{
    if(m_next)
    {
        BB_ASSERT(m_next);
        m_prev->m_next = m_next;
        m_next->m_prev = m_prev;
        m_next = NULL;
    }
}

inline sd_queue_link_t *sd_queue_ptrs_t::pop()
{
    sd_queue_link_t *t = static_cast<sd_queue_link_t*>(m_next);
    BB_ASSERT(m_next->m_prev == this && m_prev->m_next == this);
    m_next = t->m_next;
    m_next->m_prev = t->m_prev;
    t->m_next = NULL;
    return t;
}

inline sd_queue_base_t::~sd_queue_base_t()
{
    if(unlikely(m_next != this))
    {
        // the queue is non-empty (usually only happens on exceptions).
        // clear out all elements.
        sd_queue_ptrs_t *cur = m_next;
        while(cur != this)
        {
            sd_queue_ptrs_t *next = cur->m_next;
            cur->m_next = cur->m_prev = NULL;
            cur = next;
        }
    }
}

inline void sd_queue_base_t::push(sd_queue_link_t *elt)
{
    BB_ASSERT(m_next->m_prev == this && m_prev->m_next == this);
    if(!elt->m_next)
    {
        elt->m_next = this;
        elt->m_prev = m_prev;
        m_prev->m_next = elt;
        m_prev = elt;
    }
}

inline sd_queue_loan_base_t::sd_queue_loan_base_t(sd_queue_base_t &e)
    : sd_queue_ptrs_t(NOINIT)
    , m_queue(e)
{
    if(e.m_next != &e)
    {
        m_prev = e.m_prev;
        m_next = e.m_next;
        e.m_prev = e.m_next = &e;
        m_prev->m_next = this;
        m_next->m_prev = this;
    }
    else
        m_prev = m_next = this;
}

inline sd_queue_loan_base_t::~sd_queue_loan_base_t()
{
    if(unlikely(!empty()))
    {
        // splice back into the sd_queue_t we came from.
        // this should only happen in exceptional circumstances.
        m_queue.m_prev->m_next = m_next;
        m_next->m_prev = m_queue.m_prev;
        m_queue.m_prev = m_prev;
        m_prev->m_next = &m_queue;
    }
}

template<typename T>
inline void sd_queue_t<T>::push(T *elt)
{
    sd_queue_base_t::push(elt);
}

template<typename T>
inline T *sd_queue_t<T>::pop()
{
    T *r = static_cast<T*>(sd_queue_base_t::pop());
    return r;
}

template<typename T>
inline T *sd_queue_loan_t<T>::pop()
{
    T *r = static_cast<T*>(sd_queue_loan_base_t::pop());
    return r;
}


} // namespace bb

#endif // BB_CORE_SDRUNQUEUE_H

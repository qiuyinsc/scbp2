#ifndef BB_CORE_STREAMEXTRACTOR_H
#define BB_CORE_STREAMEXTRACTOR_H

/* Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved. */


#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
#include <iosfwd>
#include <ios>
#include <istream>
#include <bb/core/streamexceptbase.h>



namespace bb {
/*
Helper templates for doing user defined class stream extractors


Example Usage is

class my_class{
    unsigned int j;//an integer from 0 to 100
public:


    //typically, this is "friend operator>> " but here we want
    /// to use a lot of wrapper mechanisms
    template<class charT, class Traits>
    void
    extract(std::basic_istream< charT ,Traits > & is_in,
            std::ios_base::iostate&err){
        //stream state already good before we hit here
        os_in>>j;
        err|=os_in.rdstate();
        if(j>100){
        ////...whatever
            throw std::range_error;//wrapper will handle whether or not to propgate
        }
  };



};

template<class charT, class Traits>
std::basic_istream< charT ,Traits > &
operator>>(std::basic_istream< charT ,Traits >&m ,const my_class&s){
    return bb::stream_extractor(m,s);
};

*/



template<class charT, class Traits ,class Arg>
std::basic_istream< charT , Traits > &
stream_extractor(std::basic_istream<charT,Traits>&strm,  Arg&arg)
{
    if(!strm.good()){
        strm.setstate(  std::ios_base::failbit);
        return strm;

    }
    std::ios_base::iostate err = std::ios_base::goodbit;
    try{
        typename std::basic_istream<charT,Traits>::sentry sntry(strm);
        if (sntry)
            arg.extract(strm,err);
    }
    catch(std::bad_alloc&){
        stream_exception_handler(strm,err,std::ios_base::badbit);
    }catch(...){
        stream_exception_handler(strm,err,std::ios_base::failbit);
    }
    if (err!=std::ios_base::goodbit )
        strm.setstate( err );
    return strm;
};

}


#endif // BB_CORE_STREAMEXTRACTOR_H

#ifndef BB_CORE_RELOCATABLECHECK_H
#define BB_CORE_RELOCATABLECHECK_H

/* Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved. */


#include <boost/type_traits.hpp>

namespace bb {
template<class BB_UTILS_TT>struct declare_relocatable;//fwd decl

namespace detail{

template<class BB_UTILS_TT>struct is_pod{
    union CANNNOT_RELOCATE_AN_OBJECT_OF_THIS_TYPE{
        enum{value=true};
        BB_UTILS_TT boom;
    };
    enum{value=CANNNOT_RELOCATE_AN_OBJECT_OF_THIS_TYPE::value};
};

//virtuals are not relocatable
template <class BB_UTILS_TT,bool>struct is_polymorphiccheck{
    enum {value=false};
};

//no virtuals? then run the declare_relocatable
template<class BB_UTILS_TT>struct is_polymorphiccheck<BB_UTILS_TT,false>{
    typedef  declare_relocatable<BB_UTILS_TT> test_type;
    enum {value=test_type::value};
};

//if class is empty then it is OK (even if it has constructors/destructors)
template <class BB_UTILS_TT,bool>struct is_emptycheck{
    enum {value=true};
};

//if it is a non-emply class, check for virtuals
template<class BB_UTILS_TT>struct is_emptycheck<BB_UTILS_TT,false>{
    enum {value=is_polymorphiccheck<BB_UTILS_TT,boost::is_polymorphic<BB_UTILS_TT>::value>::value};
};

//not a class? run the declare_relocatable check now
template <class BB_UTILS_TT,bool>struct is_classcheck{
    typedef  declare_relocatable<BB_UTILS_TT> test_type;
    enum {value=test_type::value};//not a class/struct run regular check
};

//is a class? see if it is empty
template<class BB_UTILS_TT>struct is_classcheck<BB_UTILS_TT,true>{
    enum {value=is_emptycheck<BB_UTILS_TT,boost::is_empty<BB_UTILS_TT>::value>::value};
};

//is it a class???
template<class BB_UTILS_TT> struct class_check{
    enum {value=detail::is_classcheck<BB_UTILS_TT, boost::is_class<BB_UTILS_TT>::value>::value};
};

//not a class? run the declare_relocatable check now
template <class BB_UTILS_TT,bool>struct is_unioncheck{
    typedef  class_check<BB_UTILS_TT> test_type;
    enum {value=test_type::value};//not a union? see if is a class
};

//is a union? its OK
template<class BB_UTILS_TT>struct is_unioncheck<BB_UTILS_TT,true>{
    enum {value=true};
};

}//detail

//the default just checks for POD-ness
template<class BB_UTILS_TT>
struct declare_relocatable_union{
    enum {value=false};

};

//the default just checks for POD-ness
template<class BB_UTILS_TT>
struct declare_relocatable:detail::is_pod<BB_UTILS_TT>{
    typedef typename detail::is_pod<BB_UTILS_TT> base_type;
    enum {value=base_type::value};
};

//if the type is a regular class/struct, perform some additional test
//then run declare_relocatable
template<class BB_UTILS_TT>struct is_relocatable{

    typedef typename boost::remove_all_extents<BB_UTILS_TT>::type tmp_test_type;
    typedef typename boost::remove_cv<tmp_test_type>::type test_type;
    typedef typename detail::is_unioncheck<test_type,
        declare_relocatable_union<test_type>::value > unionchecktype;
    enum {value=unionchecktype::value
            && !boost::is_pointer<test_type>::value
            && !boost::is_reference<test_type>::value};
};

} // namespace bb

#endif // BB_CORE_RELOCATABLECHECK_H

#ifndef BB_CORE_LUACONFIG_H
#define BB_CORE_LUACONFIG_H

/* Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved. */


/// Config objects represent a lua configuration and abstract away the
/// heavy lifting needed to turn the configuration into usage c++ objects

/** Example usage:

class MyConfig : public LuaConfig<MyConfig>
{
public:
    static void describe() // called automatically by fromLua()
    {
        Self::create()
            .param( "markets",   &Self::markets )
            .param( "converter", &Self::converter )
        ;
    }

    std::vector<mktdest_t> markets;
    // note: the LuaState given to fromLua() must outlive this boost::function
    boost::function<std::string ( const std::string& )> converter;
};

MyConfig config = MyConfig::fromLua( luaState["config"] );
 */

#include <list>
#include <map>
#include <set>
#include <vector>

#include <boost/function.hpp>
#include <boost/make_shared.hpp>
#include <boost/type_traits/is_base_of.hpp>
#include <boost/optional.hpp>

#include <luabind/luabind.hpp>

#include <bb/core/Error.h>
#include <bb/core/Log.h>
#include <bb/core/type_id.h>

namespace bb {

template <typename T>
class LuaConfig;

namespace detail {

class LuaConfigBase {};

// The following three overloads of fromLua() work together: the third decides at compile-time
// which one of the first two needs to be called depending on whether T is a LuaConfig-derived object or not.
// For non-LuaConfig-derived objects we use luabind::object_cast<> directly rather than trying to parse a Lua
// table into a C++ object.
template <typename T>
void fromLua( T& out, const luabind::object& o, const boost::false_type& )
{
    out = luabind::object_cast<T>( o );
}

template <typename T>
void fromLua( boost::optional<T>& out, const luabind::object& o, const boost::false_type& )
{
    out = luabind::object_cast_nothrow<T>( o );
}


template <typename T>
void fromLua( T& out, const luabind::object& o, const boost::true_type& )
{
    out = LuaConfig<T>::fromLua( o );
}

template <typename T>
void fromLua( T& out, const luabind::object& o )
{
    fromLua( out, o, boost::is_base_of<LuaConfigBase, T>() );
}

// process an inline set
template <typename ValueType, typename Compare>
void fromLua( std::set<ValueType, Compare>& out, const luabind::object& o )
{
    if ( luabind::type( o ) == LUA_TTABLE)
    {
        for ( luabind::iterator i( o ), end; i != end; ++i )
        {
            ValueType value;
            fromLua( value, *i );
            bool ok = ( out ).insert( value ).second;
            BB_THROW_EXASSERT_SSX( ok, "failed to insert into map: " << value );
        }
    }
}

// process an inline vector
template <typename ValueType>
void fromLua( std::vector<ValueType>& out, const luabind::object& o )
{
    if ( luabind::type( o ) == LUA_TTABLE)
    {
        for ( luabind::iterator i( o ), end; i != end; ++i )
        {
            ValueType value;
            fromLua( value, *i );
            out.push_back( value );
        }
    }
}

template <typename KeyType, typename ValueType, typename Comparator>
void fromLua( std::map< KeyType, ValueType, Comparator>& out, const luabind::object& o)
{
    if ( luabind::type( o ) == LUA_TTABLE )
    {
        for ( luabind::iterator i( o ), end; i != end ; ++i )
        {
            ValueType r;
            fromLua( r, *i );
            KeyType key = luabind::object_cast<KeyType>( i.key() );
            bool ok = ( out ).insert( std::pair<KeyType, ValueType>( key, r ) ).second;
            BB_THROW_EXASSERT_SSX( ok, "failed to insert into map: " << key );
        }
    }
}

template <typename T>
class Base
{
public:
    Base( const char* name ) : m_name( name ) {}
    virtual ~Base() {}

    virtual void proc( T*, const luabind::object& ) = 0;

    const char* m_name;
};

template <typename T, typename P, typename R>
class Imp : public Base<T>
{
public:
    Imp( R P::* var, const char* name )
        : Base<T>( name )
        , m_var( var )
    {
    }

    virtual ~Imp() {}

    virtual void proc( T* obj, const luabind::object& o )
    {
        fromLua( obj->*m_var, o );
    }

    R P::* m_var;
};

template <typename T, typename P, typename ContainerType>
class ContainerImp : public Base<T>
{
public:
    ContainerImp( ContainerType P::* var, const char* name )
        : Base<T>( name )
        , m_var( var )
    {
    }

    virtual void proc( T* obj, const luabind::object& o )
    {
        if ( luabind::type( o ) == LUA_TTABLE)
        {
            fromLua( obj->*m_var, o );
        }
        else
        {
            obj->*m_var = luabind::object_cast< ContainerType >( o );
        }
    }

    ContainerType P::* m_var;
};

template<typename Target>
inline Target from_lua_cast( const luabind::object& value )
{
    Target ret;
    fromLua( ret, value );
    return ret;
}

template<> inline void from_lua_cast<void>( const luabind::object& __attribute__((unused))value )
{

}

template <typename Function, int Arity>
class CallProxy;

template <typename Function>
class CallProxy<Function, 0>
{
    typedef typename Function::result_type result_type;

public:
    CallProxy( const luabind::object& fn ) : m_fn( fn ) {}

    typename Function::result_type operator()() const
    {
        return from_lua_cast<result_type>( luabind::call_function<luabind::object>( m_fn ) );
    }

private:
    const luabind::object m_fn;
};

template <typename Function>
class CallProxy<Function, 1>
{
    typedef typename Function::result_type result_type;

public:
    CallProxy( const luabind::object& fn ) : m_fn( fn ) {}

    typename Function::result_type operator()( typename Function::arg1_type arg1 ) const
    {
        return from_lua_cast<result_type>( luabind::call_function<luabind::object>( m_fn, arg1 ) );
    }

private:
    const luabind::object m_fn;
};

template <typename Function>
class CallProxy<Function, 2>
{
    typedef typename Function::result_type result_type;

public:
    CallProxy( const luabind::object& fn ) : m_fn( fn ) {}

    typename Function::result_type operator()( typename Function::arg1_type arg1, typename Function::arg2_type arg2 ) const
    {
        return from_lua_cast<result_type>( luabind::call_function<luabind::object>( m_fn, arg1, arg2 ) );
    }

private:
    const luabind::object m_fn;
};

template <typename T, typename P, typename FunctionArgs>
class FunctionImp : public Base<T>
{
    typedef boost::function<FunctionArgs> Function;

public:
    FunctionImp( Function P::* var, const char* name )
        : Base<T>( name )
        , m_var( var )
    {
    }

    virtual void proc( T* obj, const luabind::object& o )
    {
        BB_THROW_EXASSERT_SSX( luabind::type( o ) == LUA_TFUNCTION, "parameter is not a lua function: " << this->m_name );
        obj->*m_var = CallProxy<Function, Function::arity>( o );
    }

private:
    Function P::* m_var;
};

template <typename T, typename P>
class ObjectImp : public Base<T>
{
public:
    ObjectImp( luabind::object P::* var, const char* name )
        : Base<T>( name )
        , m_var( var )
    {
    }

    virtual void proc( T* obj, const luabind::object& o )
    {
        obj->*m_var = o;
    }

private:
    luabind::object P::* m_var;
};

template <typename T>
class Description
{
    typedef boost::shared_ptr<Base<T> > BasePtr;
    typedef std::map<std::string, BasePtr> ParamsList;
    ParamsList m_params;

    typedef Description<T> Self;

    Self& insertParam( const char* name, const BasePtr& value )
    {
        if ( !m_params.insert( typename ParamsList::value_type( name, value ) ).second )
            LOG_WARN << "Trying to describe a parameter again: " << name << bb::endl;
        return *this;
    }

public:

    void initFromLua( T& object, const luabind::object& o )
    {
        BB_THROW_EXASSERT_SSX(
            luabind::type( o ) == LUA_TTABLE,
            "Can't initFromLua except from a LUA_TTABLE" );

        for ( luabind::iterator i( o ), end; i != end; ++i )
        {
            std::string key = luabind::object_cast<std::string>( i.key() );
            typename ParamsList::iterator found = m_params.find( key );
            if ( found == m_params.end() )
            {
                LOG_WARN << "Unknown field: " << key << bb::endl;
            }
            else
            {
                Base<T>* b = found->second.get();
                try
                {
                    b->proc( &object, o[b->m_name] );
                }
                catch( const std::exception& xcp )
                {
                    BB_THROW_ERROR_SSX(
                        "Exception while binding parameter '" << b->m_name << "' "
                        << "in type " << boost::python::type_info(typeid(T)).name() << ": "
                        << xcp.what() );
                }
            }
        }
    }

    T fromLua( const luabind::object& o )
    {
        if ( luabind::type( o ) == LUA_TTABLE )
        {
            T t;
            initFromLua(t, o );
            return t;
        }
        return luabind::object_cast<T>( o );
    }

    template <typename P, typename ValueType>
    Self& param( const char* name, std::vector<ValueType> P::* var )
    {
        return insertParam( name, boost::make_shared<ContainerImp<T, P, std::vector<ValueType> > >( var, name ) );
    }

    template <typename P, typename ValueType>
    Self& param( const char* name, std::list<ValueType> P::* var )
    {
        return insertParam( name, boost::make_shared<ContainerImp<T, P, std::list<ValueType> > >( var, name ) );
    }

    template <typename P, typename ValueType, typename Compare>
    Self& param( const char* name, std::set<ValueType, Compare> P::* var )
    {
        return insertParam( name, boost::make_shared<ContainerImp<T, P, std::set<ValueType, Compare> > >( var, name ) );
    }

    template <typename P, typename KeyType, typename ValueType, typename Comparator>
    Self& param( const char* name, std::map<KeyType, ValueType, Comparator> P::* var )
    {
        return insertParam( name, boost::make_shared<ContainerImp<T, P, std::map<KeyType, ValueType, Comparator> > >( var, name ) );
    }

    /// Warning: The boost::function created by this must not outlive the luabind::object passed to fromLua.
    /// Otherwise the proxy object underlying the boost::function, which contains a luabind::object, will
    /// still be available to call even after its Lua State has been destroyed.
    template <typename P, typename FunctionArgs>
    Self& param( const char* name, boost::function<FunctionArgs> P::* var )
    {
        return insertParam( name, boost::make_shared<FunctionImp<T, P, FunctionArgs> >( var, name ) );
    }

    template <typename P>
    Self& param( const char* name, luabind::object P::* var )
    {
        return insertParam( name, boost::make_shared<ObjectImp<T, P> >( var, name ) );
    }

    template <typename P, typename ValueType>
    Self& param( const char* name, ValueType P::* var )
    {
        return insertParam( name, boost::make_shared<Imp<T, P, ValueType> >( var, name ) );
    }
};

} // namespace detail


// Extend this class providing your class name as the template argument
// A describe method should be implemented which will contain the object description
template <typename T>
class LuaConfig : detail::LuaConfigBase
{
    typedef boost::shared_ptr<detail::Description<T> > DescriptionPtr;
    static DescriptionPtr m_description;

public:
    typedef T Self;

    // convert a lua object into this type
    static T fromLua( const luabind::object& o )
    {
        ensureDescribed();
        return m_description->fromLua( o );
    }

    //checks if the luabind object is valid, returning an empty (null) object otherwise
    static boost::optional<T> fromOptionalLua( const luabind::object& o )
    {
        if( o.is_valid() && luabind::type( o ) != LUA_TNIL )
            return fromLua( o );
        else
        {
            boost::optional<T> ret;
            return ret;
        }
    }

    static void initFromLua( T& self, const luabind::object& o )
    {
        ensureDescribed();
        return m_description->initFromLua( self, o );
    }

    static bool initFromLuaIfPresent( T& self, const luabind::object& o )
    {
        if( o.is_valid() && luabind::type( o ) != LUA_TNIL )
        {
            initFromLua( self, o );
            return true;
        }
        return false;
    }

protected:
    static detail::Description<T>& create()
    {
        if ( !m_description )
            m_description.reset( new detail::Description<T>() );
        return *m_description;
    }

private:
    static void ensureDescribed()
    {
        if( !m_description )
            T::describe();
        BB_THROW_EXASSERT_SSX(
            m_description,
            "Failed to construct description object" );
    }
};

template <typename T>
typename LuaConfig<T>::DescriptionPtr LuaConfig<T>::m_description;

} // namespace bb

#endif // BB_CORE_LUACONFIG_H

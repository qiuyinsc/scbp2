#ifndef BB_CORE_SMART_PTR_H
#define BB_CORE_SMART_PTR_H

/* Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved. */


// Boost smart_ptr includes
#include <boost/enable_shared_from_this.hpp>
#include <boost/intrusive_ptr.hpp>
#include <boost/scoped_ptr.hpp>
#include <boost/shared_ptr.hpp>
#include <boost/weak_ptr.hpp>

// pull the smart pointer and their casts into the bb namespace
namespace bb {

using boost::shared_ptr;
using boost::weak_ptr;
using boost::scoped_ptr;
using boost::intrusive_ptr;

using boost::static_pointer_cast;
using boost::const_pointer_cast;
using boost::dynamic_pointer_cast;
using boost::enable_shared_from_this;
struct NoOpDeleter : public std::unary_function<void*, void>
    { void operator()(const void *) const {} };

/// a shared_ptr which doesn't delete its argument
template<typename T> inline bb::shared_ptr<T> makeNoopSharedPtr(T *x)
    { return typename bb::shared_ptr<T>(x, bb::NoOpDeleter()); }
template<typename T> inline bb::shared_ptr<const T> makeNoopSharedCPtr(const T *x)
    { return typename bb::shared_ptr<const T>(x, bb::NoOpDeleter()); }

/// An Alias is a shared_ptr deleter which holds a reference
/// to another object until destroyed.
template<class Deleter, class T>
struct Alias : protected Deleter
{
    Alias(Deleter const &d, T const& o) : Deleter(d), m_reference(o) {}
    void operator()(typename Deleter::argument_type p) { Deleter::operator()(p); m_reference = T(); }
private:
    T m_reference;
};

/// Returns a deleter for shared_ptr<> which will hold a reference to x,
/// and not delete its pointee. I.e. when the following shared_ptr goes
/// out of scope:
/// shared_ptr<Y>(y,make_alias(x));
/// 1) the reference to x will be released.
/// 2) y will not be deleted
template<class T>
Alias<NoOpDeleter, T> make_alias(T const&v){
    return Alias<NoOpDeleter, T>(NoOpDeleter(), v);
}

/// Variant of make_alias which also calls Deleter on its its pointee.
template<class Deleter, class T>
Alias<Deleter, T> make_alias(Deleter const &d, T const&v){
    return Alias<Deleter, T>(d, v);
}

} // namespace bb


/// BB_DECLARE_SHARED_PTR
///     Given a classname, creates two typedefs for its shared pointer.
///     For example passing Foo, will make FooPtr and FooCPtr.
///     This must be done within the namespace scope of the classname and the
///     Resulting pointers will be within that namespace.
#define BB_DECLARE_SHARED_PTR( _class_ )                         \
    typedef bb::shared_ptr<_class_ >       _class_##Ptr;         \
    typedef bb::shared_ptr<const _class_ > _class_##CPtr;        \
    typedef bb::weak_ptr<_class_ >         _class_##WeakPtr;     \
    typedef bb::weak_ptr<const _class_ >   _class_##WeakCPtr

#define BB_FWD_DECLARE_SHARED_PTR( _class_ ) class _class_; BB_DECLARE_SHARED_PTR(_class_)

#define BB_DECLARE_SCOPED_PTR( _class_ )                         \
    typedef bb::scoped_ptr<_class_ >       _class_##ScopedPtr;   \
    typedef bb::scoped_ptr<const _class_ > _class_##ScopedCPtr

#define BB_FWD_DECLARE_SCOPED_PTR( _class_ ) class _class_; BB_DECLARE_SCOPED_PTR(_class_)

#define BB_DECLARE_INTRUSIVE_PTR( _class_ )                           \
    typedef bb::intrusive_ptr<_class_> _class_##Ptr;                  \
    typedef bb::intrusive_ptr<const _class_> _class_##CPtr            \

#define BB_FWD_DECLARE_INTRUSIVE_PTR( _class_ ) class _class_; BB_DECLARE_INTRUSIVE_PTR(_class_)
#define BB_FWD_DECLARE_INTRUSIVE_STRUCT_PTR( _class_ ) struct _class_; BB_DECLARE_INTRUSIVE_PTR(_class_)


#endif // BB_CORE_SMART_PTR_H

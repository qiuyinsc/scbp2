#ifndef BB_CORE_NUMA_PINNING_H
#define BB_CORE_NUMA_PINNING_H

/* Contents Copyright 2016 Athena Capital Research LLC. All Rights Reserved. */

#include <numa.h>
#include <string>
#include <algorithm>

#include <boost/format.hpp>
#include <boost/foreach.hpp>
#include <boost/type_traits.hpp>
#include <boost/array.hpp>

#include <bb/core/Error.h>
#include <bb/core/Log.h>

namespace bb {
    namespace process { // TODO : shift cpu_pinning functionality into bb::current_thread namespace and adjust callers
        enum PinningMode
        {
            PROCESSING,
            MEMORY,
            PROCESSING_AND_MEMORY
        };

        int num_numa_nodes()
        {
            return numa_num_configured_nodes();
        }

        // Returns a vector of all nodes that are pinned for the specified mode.
        std::vector<int> get_pinned_nodes( const PinningMode mode = PROCESSING_AND_MEMORY, const bool verbose = false );

        // Returns true if the current process is pinned *exclusively* to the nodes provided in the collection:
        // - we can't be pinned to more nodes
        // - or to less
        // - for both/either mode provided
        // False otherwise.
        template<typename Collection>
        typename boost::enable_if<boost::is_integral<typename Collection::value_type>, bool>::type
        am_pinned_to_nodes(const Collection & nodes, const PinningMode mode = PROCESSING_AND_MEMORY, const bool verbose = false)
        {
            if ( mode == PROCESSING_AND_MEMORY )
            {
                return am_pinned_to_nodes ( nodes, MEMORY, verbose ) && am_pinned_to_nodes ( nodes, PROCESSING, verbose );
            }
            else
            {
                const std::vector<int> pinned_nodes = get_pinned_nodes ( mode, verbose );
                return nodes.size() == pinned_nodes.size() &&
                    std::equal(nodes.begin(), nodes.end(), pinned_nodes.begin());
            }
        }

        // Returns true if the current process is pinned *exclusively* to the nodes provided in the collection:
        // - we can't be pinned to more nodes
        // - or to less
        // - for both/either mode provided
        // False otherwise.
        template<typename Type, int LEN>
        typename boost::enable_if<boost::is_integral<Type>, bool>::type
        am_pinned_to_nodes(const Type(& nodes)[LEN], const PinningMode mode = PROCESSING_AND_MEMORY, const bool verbose = false)
        {
            boost::array<Type,LEN> arr;
            for(size_t i = 0 ; i < LEN; i++)
            {
                arr[i] = nodes[i];
            }
            return am_pinned_to_nodes(arr, mode, verbose);
        }

        // Pin the current process ( all threads ) to the specified collection of numa nodes.
        // Most likely we want this to be a single numa node. Will pin:
        // - PROCESSING
        // - MEMORY
        // - BOTH PROCESSING AND MEMORY ( default ).
        // This will raise an exception if you pass in a numa node that's unavailable on this system.
        //
        // Default case ( pinning just to numa #0 ):
        // std::vector<int> bound_nodes;
        // bound_nodes.push_back(0);
        // bb::process::pin_to_nodes(bound_nodes, bb::process::PROCESSING_AND_MEMORY);
        template<typename Collection>
        typename boost::enable_if<boost::is_integral<typename Collection::value_type>, bool>::type
        pin_to_nodes(const Collection & nodes, const PinningMode mode = PROCESSING_AND_MEMORY, const bool verbose = false)
        {
            // allocate and deallocate the numa nodemask, whatever happens
            struct bitmask * numa_nodes = numa_allocate_nodemask();
            struct dealloc_on_exit_t
            {
                dealloc_on_exit_t ( struct bitmask * mask ) : m_mask ( mask ) {}
                ~dealloc_on_exit_t () { numa_free_nodemask ( m_mask ); }
                struct bitmask * m_mask;
            } dealloc_on_exit ( numa_nodes );

            // Make sure we don't do anything stupid, such as pin the memory to numa #0 first,
            // and follow it up by pinning the processing to numa #1. In this case, we raise an exception.
            // ( Alternatively, we could return 'false'.. up for debate. )
            if ( mode != PROCESSING_AND_MEMORY )
            {
                const std::vector<int> pinned_nodes_other_mode = get_pinned_nodes ( mode == PROCESSING ? MEMORY : PROCESSING );
                std::vector<int> intersection(std::max(pinned_nodes_other_mode.size(), nodes.size()));
                std::vector<int>::const_iterator iter = std::set_intersection(nodes.begin(), nodes.end(),
                        pinned_nodes_other_mode.begin(), pinned_nodes_other_mode.end(),
                        intersection.begin());
                intersection.resize(iter-intersection.begin());
                // notice that it is possible to for instance allow memory to reside on node #0 and #1 and the pin
                // processing to node #1 only
                BB_THROW_EXASSERT ( intersection.size() == nodes.size(),
                    "You can't pin memory/processing on different nodes!" );
            }

            // Make sure we don't want to pin to a node that's out of range.
            // If we try to do so we'll raise an exception.
            // Otherwise ( normal case ) just toggle the appropriate bit.
            BOOST_FOREACH(typename Collection::value_type node, nodes)
            {
                BB_THROW_EXASSERT ( node >= 0 && node < num_numa_nodes(),
                    ( boost::format("Can't pin to numa node #%1% because we only have %2% numa nodes.") % node % num_numa_nodes() ).str() );
                numa_bitmask_setbit( numa_nodes, node );
            }

            // Depending on the mode, call the appropriate function.
            std::string mode_str;
            switch(mode)
            {
                case PROCESSING:
                    numa_run_on_node_mask( numa_nodes );
                    mode_str = "*processing*";
                    break;
                case MEMORY:
                    numa_set_membind( numa_nodes );
                    mode_str = "*memory*";
                    break;
                case PROCESSING_AND_MEMORY:
                    numa_bind( numa_nodes );
                    mode_str = "*processing and memory*";
                    break;
                default:
                    throw std::runtime_error("'mode' argument out of range.");
            }

            // Let the peeps know what's up.
            if ( unlikely ( verbose ) )
            {
                std::ostringstream node_set_stream;
                std::copy(nodes.begin(),
                    nodes.end(),
                    std::ostream_iterator<typename Collection::value_type>(node_set_stream, ","));
                std::string set_string = node_set_stream.str();
                set_string.erase(set_string.length()-1);
                LOG_INFO << "Numa: this process has bound " << mode_str << " to NUMA nodes #" << set_string<< bb::endl;
            }
            return true;
        }

        // Pin the current process ( all threads ) to the specified collection of numa nodes.
        // Most likely we want this to be a single numa node. Will pin:
        // - PROCESSING
        // - MEMORY
        // - BOTH PROCESSING AND MEMORY ( default ).
        // This will raise an exception if you pass in a numa node that's unavailable on this system.
        //
        // Sample case ( pinning just to numa #0 and #1 )
        // int bound_nodes[2]; bound_nodes[0] = 0; bound_nodes[1] = 1;
        // bb::process::pin_to_nodes(bound_nodes, bb::process::PROCESSING_AND_MEMORY);
        template<typename Type, int LEN>
        typename boost::enable_if<boost::is_integral<Type>, bool>::type
        pin_to_nodes( const Type(& nodes)[LEN], const PinningMode mode = PROCESSING_AND_MEMORY, const bool verbose = false )
        {
            boost::array<Type,LEN> arr;
            for(size_t i = 0 ; i < LEN; i++)
            {
                arr[i] = nodes[i];
            }
            return pin_to_nodes(arr, mode, verbose);
        }

        // Unpin from any specific numa core - aka pin to all.
        bool unpin ( const PinningMode mode = PROCESSING_AND_MEMORY,
                const bool verbose = false /* currently unused, provided for consistency with cpu_pinning */);

        // Pin the current process ( all threads ) to the specified single numa node.
        // Wil pin:
        // - PROCESSING
        // - MEMORY
        // - BOTH PROCESSING AND MEMORY ( default ).
        // This will raise an exception if you pass in a numa node that's unavailable on this system.
        //
        // Default case ( pinning just to numa #0 ):
        // bb::process::pin_to_node(0, bb::process::PROCESSING_AND_MEMORY);
        bool pin_to_node(const int node, const PinningMode mode = PROCESSING_AND_MEMORY, const bool verbose = false );

        // Returns true if the current process is pinned *exclusively* to the node provided
        // - we can't be pinned to more nodes
        // - or to less
        // - for both/either mode provided
        // False otherwise.
        bool am_pinned_to_node(const int node, const PinningMode mode = PROCESSING_AND_MEMORY, const bool verbose = false );
    };
};

#endif // BB_CORE_NUMA_PINNING_H

#ifndef BB_CORE_OBJECTCACHE_H
#define BB_CORE_OBJECTCACHE_H

/* Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved. */


#include <boost/thread/recursive_mutex.hpp>
#include <boost/optional.hpp>
#include <map>


namespace bb {


/**
  \class TObjectCache
  A template class which is cache of objects of a particular type.
  This class is thread-safe.
**/
template< typename TKey, typename TObj >
class TObjectCache
    : private boost::noncopyable // Can only be accessed via instance()
{
public:
    typedef boost::optional<TObj>  get_type;

public:
    /// Returns the single instance of this TObjectCache
    static TObjectCache<TKey, TObj>& instance();

    /// Retrieves an object from the cache.
    /// Returns an invalid get_type if unsuccessful.
    get_type get( const TKey& key ) const;

    /// Inserts an object into the cache.
    /// If the returned get_type is valid, it holds a TObj that
    /// was replaced by the new insert.
    get_type insert( const TKey& key, TObj& obj );

    /// Removes an object from the cache.
    /// Returns true if the object existed in the map.
    bool remove( const TKey& key );

    /// Removes all objects from the cache.
    void clear();

    /// Returns the number of objects in the cache.
    size_t size() const;

private:
    typedef std::map<TKey, TObj>    map_type;
    map_type                        m_map;
    mutable boost::recursive_mutex  m_mutex;
};


//
// Inline implementation
//

/// Returns the single instance of this TObjectCache
template< typename TKey, typename TObj > inline
TObjectCache<TKey, TObj>& TObjectCache<TKey, TObj>::instance()
{
    static TObjectCache<TKey, TObj> s_objectCache;
    return s_objectCache;
}

/// Retrieves an object from the cache.
/// Returns an invalid get_type if unsuccessful.
template< typename TKey, typename TObj > inline
typename TObjectCache<TKey, TObj>::get_type TObjectCache<TKey, TObj>::get( const TKey& key ) const
{
    boost::recursive_mutex::scoped_lock lock( m_mutex );
    typename map_type::const_iterator iter = m_map.find( key );
    return ( iter == m_map.end() ) ? get_type() : get_type( iter->second );
}

/// Inserts an object into the cache.
/// If the returned get_type is valid, it holds a TObj that
/// was replaced by the new insert.
template< typename TKey, typename TObj > inline
typename TObjectCache<TKey, TObj>::get_type TObjectCache<TKey, TObj>::insert( const TKey& key, TObj& obj )
{
    boost::recursive_mutex::scoped_lock lock( m_mutex );
    std::pair<typename map_type::iterator, bool> res = m_map.insert( std::make_pair(key,obj) );
    if ( !res.second ) // there was already something in there
    {
        // remember the old value, replace it, and return the old one
        get_type old_val( res.first->second );
        res.first->second = obj;
        return old_val;
    }
    return get_type();
}

/// Removes an object from the cache.
/// Returns true if the object existed in the map.
template< typename TKey, typename TObj > inline
bool TObjectCache<TKey, TObj>::remove( const TKey& key )
{
    boost::recursive_mutex::scoped_lock lock( m_mutex );
    size_t num = m_map.erase( key );
    return (num != 0);
}

/// Removes all objects from the cache.
template< typename TKey, typename TObj > inline
void TObjectCache<TKey, TObj>::clear()
{
    boost::recursive_mutex::scoped_lock lock( m_mutex );
    m_map.clear();
}

/// Returns the number of objects in the cache.
template< typename TKey, typename TObj > inline
size_t TObjectCache<TKey, TObj>::size() const
{
//    boost::recursive_mutex::scoped_lock lock( m_mutex );
    return m_map.size();
}


} // namespace bb


#endif // BB_CORE_OBJECTCACHE_H

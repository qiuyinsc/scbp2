#ifndef BB_CORE_LUABINDSCRIPTING_H
#define BB_CORE_LUABINDSCRIPTING_H

/* Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved. */

#include <boost/function.hpp>
#include <luabind/luabind.hpp>
#include <luabind/iterator_policy.hpp>
#include <luabind/operator.hpp>
#include <luabind/out_value_policy.hpp>

#include <bb/core/Error.h>
#include <bb/core/Log.h>
#include <bb/core/Scripting.h>

namespace bb {
struct LuaPrintSettings;

// Introduce luabind::object_cast into the world so we can use it just like static_cast
using luabind::object_cast;

// Convert a Lua object to a luabind::table<>, throwing a friendly error message with the given string on failure
template<class ValueWrapper>
luabind::table<> table_cast( const ValueWrapper& value_wrapper, const std::string& what )
{
    try {
        return object_cast<luabind::table<> >( value_wrapper );
    } catch( luabind::cast_failed& ) {
        throw Error( "Lua table_cast failed: " + what );
    }
}


/// define a Lua function "apply_table" which assigns the key/value pairs of a table on an object
/// call the Lua apply_table() on a Lua-bound C++ object for easy construction
void lua_define_apply_table( lua_State& rL );

template<typename Target>
void lua_apply_table( Target* target, const luabind::table<>& table )
{
    luabind::call_function<void>( table.interpreter(), "apply_table", target, table );
}

/// define a Lua function "apply_table_stric" which assigns the key/value pairs of a table on an object
/// call the Lua apply_table_stric() on a Lua-bound C++ object for easy construction. This function
/// raises an exception if a lua variable is not bound to the C++ object
void lua_define_apply_table_strict( lua_State& rL );

template<typename Target>
void lua_apply_table_strict( Target* target, const luabind::table<>& table )
{
    luabind::call_function<void>( table.interpreter(), "apply_table_strict", target, table );
}


struct LuaErrorHandlers
{
    /// An error handler that appends a stack traceback to the error message.
    /// The error message is also copied to a string, retrievable via
    /// LuaErrorHandlers::getTracebackErrorString().
    static int TracebackErrorHandler( lua_State *L );
    static int ThrowOnPanic( lua_State *L );

    /// Returns the string filled via TracebackErrorHandler.
    /// This is a workaround for some Traceback Error handling issues.
    static const char* getTracebackErrorString() { return ms_traceback_error; }
protected:
    static char      ms_traceback_error[4096];
};


/// LuaExec::LuaExec installs an error handler into the lua_State.
/// Then, call load/execute to run lua code.
class LuaExec
{
public:
    LuaExec(lua_State *state, lua_CFunction errFn = &LuaErrorHandlers::TracebackErrorHandler);
    ~LuaExec();

    /// Load a filename into the lua_State.
    /// Throws a bb::Error if the load fails.
    void load( const char* luaFilename );
    void load( const std::string &luaFilename ) { load(luaFilename.c_str()); }
    void load( std::istream &input, const char *chunkname = "" );

    /// Loads lua from loaded shared objects. It will look for symbols
    /// named '_binary_<embeddingName>_start' and
    /// '_binary_<embeddingName>_end', and inject the bytes that exist
    /// between those addresses into 'load' above. This lets you
    /// compile lua into a shared object.
    void loadEmbedded( const std::string& embeddingName );

    /// Executes the passed string (Lua code) in the Lua environment.
    luabind::object execute( const char* s, size_t len );
    luabind::object execute( const std::string& s ) { return execute(s.c_str(), s.length()); }
    luabind::object execute( const char* s ) { return execute(s, strlen(s)); }

protected:
    lua_State *m_state;
    int m_errorHandlerIndex;
};

template<typename T, typename C>
T get_deprecated_field_imp( const T C::* field, const char *field_name, const char *new_name, const C& c )
{
    LOG_WARN << "getting deprecated field: " << field_name
             << "; please update config to use " << new_name << " instead" << bb::endl;

    return c.*field;
}

template<typename T, typename C>
boost::function<T (const C&)> get_deprecated_field( const T C::* field, const char *field_name, const char *new_name)
{
    return boost::bind( get_deprecated_field_imp<T, C>, field, field_name, new_name, _1 );
}

template<typename T, typename C>
void set_deprecated_field_imp( T C::* field, const char *field_name, const char *new_name, C& c, const T& t )
{
    LOG_WARN << "setting deprecated field: " << field_name
             << "; please update config to use " << new_name << " instead" << bb::endl;

    c.*field = t;
}

template<typename T, typename C>
boost::function<void (C&, const T&)> set_deprecated_field( T C::* field, const char *field_name, const char *new_name)
{
    return boost::bind( set_deprecated_field_imp<T, C>, field, field_name, new_name, _1, _2 );
}

#define DEF_DEPRECATED_RW(name,field,new_name) \
    property(name, bb::get_deprecated_field<>(field,name,new_name), bb::set_deprecated_field<>(field,name,new_name))

template<typename T, typename C>
T get_obsolete_field_imp( const T C::* field, const char *field_name, const char *new_name, const C& c )
{
    BB_THROW_ERROR_SS( "field " << field_name << " is obsolete; please update config to use " << new_name );

    return c.*field;
}

template<typename T, typename C>
boost::function<T (const C&)> get_obsolete_field( const T C::* field, const char *field_name, const char *new_name)
{
    return boost::bind( get_obsolete_field_imp<T, C>, field, field_name, new_name, _1 );
}

template<typename T, typename C>
void set_obsolete_field_imp( T C::* field, const char *field_name, const char *new_name, C& c, const T& t )
{
    BB_THROW_ERROR_SS( "field " << field_name << " is obsolete; please update config to use " << new_name );

    c.*field = t;
}

template<typename T, typename C>
boost::function<void (C&, const T&)> set_obsolete_field( T C::* field, const char *field_name, const char *new_name)
{
    return boost::bind( set_obsolete_field_imp<T, C>, field, field_name, new_name, _1, _2 );
}

#define DEF_OBSOLETE_RW(name,field,new_name) \
    property(name, bb::get_obsolete_field<>(field,name,new_name), bb::set_obsolete_field<>(field,name,new_name))

} // namespace bb

#endif // BB_CORE_LUABINDSCRIPTING_H

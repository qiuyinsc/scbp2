#ifndef BB_CORE_SPARSE_VECTOR_H
#define BB_CORE_SPARSE_VECTOR_H

/* Contents Copyright 2016 Athena Capital Research LLC. All Rights Reserved. */

#include <utility>
#include <algorithm>
#include <vector>

#include <bb/core/mpl.h>

#include <boost/functional.hpp>
#include <boost/utility/enable_if.hpp>
#include <boost/type_traits.hpp>
#include <boost/foreach.hpp>
#include <boost/iterator/transform_iterator.hpp>

namespace bb {

// A vector-like container for classes or numeric values.
// This struct works well if the number of items is *very small* but
// will resort to binary searching when we've got a larger number of items.
template<typename T>
struct sparse_vector
{
    typedef std::pair<size_t, T> inner_value_t;
    struct taker : public std::unary_function<inner_value_t, T>
    {
        typedef inner_value_t first_argument_type;
        typedef T second_argument_type;
        T & operator()(inner_value_t & value) const
        {
            return value.second;
        }
    };
    struct const_taker : public std::unary_function<inner_value_t, T>
    {
        typedef inner_value_t first_argument_type;
        typedef T second_argument_type;
        T const & operator()(inner_value_t const & value) const
        {
            return value.second;
        }
    };
    typedef std::vector<inner_value_t> InnerVct;

    // our iterators actually iterate over the 'T' values.
    // this way, we can easily 'foreach' over the 'vector' without having to return pairs<indx, T>
    typedef typename boost::transform_iterator<taker, typename InnerVct::iterator> iterator;
    typedef typename boost::transform_iterator<const_taker, typename InnerVct::const_iterator> const_iterator;

    sparse_vector()
        : m_sorted ( false )
    {
    }

    sparse_vector(sparse_vector<T> const & other)
        : m_innerVct ( other.m_innerVct )
        , m_sorted ( other.m_sorted )
    {
    }

    iterator begin()
    {
        return boost::make_transform_iterator<taker, typename InnerVct::iterator> ( m_innerVct.begin() );
    }

    const_iterator begin() const
    {
        return boost::make_transform_iterator<const_taker, typename InnerVct::const_iterator> ( m_innerVct.begin() );
    }

    iterator end()
    {
        return boost::make_transform_iterator<taker, typename InnerVct::iterator> ( m_innerVct.end() );
    }

    const_iterator end() const
    {
        return boost::make_transform_iterator<const_taker, typename InnerVct::const_iterator> ( m_innerVct.end() );
    }

    size_t size() const
    {
        return m_innerVct.size();
    }

    bool empty() const
    {
        return m_innerVct.empty();
    }

    T& operator [] ( size_t indx )
    {
        // We expect/want the collection to be so small that
        // simply iterating over them will be quickest of all
        if ( not m_sorted )
        {
            BOOST_FOREACH(inner_value_t& value, m_innerVct)
            {
                if ( value.first == indx )
                {
                    return value.second;
                }
            }
        }

        // If T is a class, we'll use the default constructor
        T def;

        // If T is an int or float ( ie not a class ), we'll set it to 0
        if ( is_number_type::value )
        {
            def = 0;
        }
        inner_value_t new_value = std::make_pair ( indx, def );

        // We've stored 'a lot' of values - binary search to find our value
        if ( m_sorted )
        {
            typename InnerVct::iterator iter = std::lower_bound( m_innerVct.begin(),
                    m_innerVct.end(),
                    new_value,
                    comparer_t() );
            if ( iter != m_innerVct.end() &&
                    iter->first == indx )
            {
                return iter->second;
            }
        }

        // Neither way of finding our element worked - insert it
        m_innerVct.push_back( new_value );

        // This vector is really meant to be kept small, but if we're going
        // to exceed the standard cache line size ( at the time of writing ):
        // 64 bytes, we might as well sort it so we can binary search through it.
        if ( m_innerVct.size() * sizeof(T) > 64 )
        {
            std::sort( m_innerVct.begin(),
                    m_innerVct.end(),
                    comparer_t() );
            m_sorted = true;

            return std::lower_bound( m_innerVct.begin(),
                    m_innerVct.end(),
                    new_value,
                    comparer_t() )->second;
        }
        else
        {
            return m_innerVct.back().second;
        }
    }

    private:
        typedef typename bb::either_true< typename boost::is_integral<T>::type, typename boost::is_float<T>::type > is_number_type;
        InnerVct m_innerVct;
        bool m_sorted;


        struct comparer_t : std::binary_function<inner_value_t, inner_value_t, bool>
        {
            bool operator()(inner_value_t const & a, inner_value_t const & b) const
            {
                return a.first < b.first;
            }
        };
};

}

#endif // BB_CORE_SPARSE_VECTOR_H

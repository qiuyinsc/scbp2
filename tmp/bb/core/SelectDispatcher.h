#ifndef BB_CORE_SELECTDISPATCHER_H
#define BB_CORE_SELECTDISPATCHER_H

/* Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved. */


#include <sys/epoll.h>

#include <vector>

#include <boost/bind.hpp>
#include <boost/noncopyable.hpp>
#include <boost/version.hpp>
#include <boost/intrusive/splay_set.hpp>
#include <boost/type_traits/remove_reference.hpp>
#include <boost/fusion/container/list.hpp>
#include <boost/fusion/algorithm/iteration/fold.hpp>
#include <boost/fusion/algorithm/iteration/for_each.hpp>
#include <boost/fusion/view/filter_view.hpp>
#include <boost/fusion/mpl.hpp>
#include <boost/mpl/assert.hpp>
#include <boost/mpl/equal.hpp>
#include <boost/mpl/int.hpp>
#include <boost/mpl/int.hpp>
#include <boost/mpl/count_if.hpp>
#include <boost/thread.hpp>

#include <bb/core/Error.h>
#include <bb/core/ptime.h>
#include <bb/core/smart_ptr.h>
#include <bb/core/UnixSig.h>
#include <bb/core/FD.h>
#include <bb/core/SDRunQueue.h>
#include <bb/core/FDSet.h>
#include <bb/core/SDPolicy.h>
#include <bb/core/shared_vector.h>

namespace bb
{

void enable_cpu_spin_for_this_thread();
void disable_cpu_spin_for_this_thread();

/// An implementation of FDSet using epoll_ctl/epoll_wait.
class EPollFDSet : public FDSet, public IPolicyDispatcher, public IDeferredWorkDispatcher
{
public:
    static const int DEFAULT_SIZE = 4;

    /// numberFDsHint is a hint on the number of file descriptors which
    /// will be tracked, it doesn't need to be exact.
    EPollFDSet(int numberFDsHint = DEFAULT_SIZE);
    virtual ~EPollFDSet();

    virtual void createReadCB(Subscription &outSub, int fd, const ReactorCallback &readCB);
    virtual void createReadCB(Subscription &outSub, const FDPtr &fd, const ReactorCallback &readCB);
    virtual void createWriteCB(Subscription &outSub, int fd, const ReactorCallback &writeCB);
    virtual void createWriteCB(Subscription &outSub, const FDPtr &fd, const ReactorCallback &writeCB);
    virtual void addReader(const SDPolicyHandle &policy, SDReader *reader, const FDPtr &fd);
    virtual void addWriter(const SDPolicyHandle &policy, SDWriter *writer, const FDPtr &fd);

    virtual void defer(Subscription &outSub, const ReactorCallback& callback);

    static void setCpuSpinning(bool enabled);

    void setUseSelectTimestamps( bool ) override;
    bool eventsAvailable() const;

    const timeval_t& getSelectRdyTime()  const override { return m_selectRdyTime; }
    const timeval_t& getSelectPollTime() const override { return m_selectPollTime; }
    uint32_t getFdRdyCount()             const override { return m_fdRdyCount; }
protected:
    /// Calls epoll_wait, and puts each reactor on its run-queue. doesn't do anything else.
    SelectResult selectMillis(int32_t millis);

    struct FDEntry : public boost::noncopyable,
                     public boost::intrusive::splay_set_base_hook<
                                boost::intrusive::link_mode<boost::intrusive::normal_link> >
    {
        FDEntry(int fd, bool et) : m_fd(fd), m_edgeTriggered(et) { m_callbacks[0] = NULL; m_callbacks[1] = NULL; }
        int events() const;
        int m_fd;
        bool m_edgeTriggered;
        SDReactorBase *m_callbacks[2]; // indexed by FDCallbackType
    };
    struct FDEntryComp
    {
        bool operator()(const FDEntry &x, const FDEntry &y) const { return x.m_fd < y.m_fd; }
        bool operator()(const FDEntry &x, int y_fd) const { return x.m_fd < y_fd; }
        bool operator()(int x_fd, const FDEntry &y) const { return x_fd < y.m_fd; }
    };
    struct ReactorSub;
    struct FallbackSub;

    typedef boost::intrusive::splay_set<FDEntry,
        boost::intrusive::constant_time_size<false>,
        boost::intrusive::compare<FDEntryComp> > FDSet_t;

    virtual void setupFallbackQueue(FDCallbackType type, BoostFnReactor *react) = 0;

    void addToSet(FDCallbackType type, int fd, const SDReactorBasePtr &reactor, const boost::shared_ptr<void> &heldPtr, bool edgeTriggered);
    void addFastReactor(FDCallbackType type, const SDPolicyHandle &policy, SDReactor *reactor, const FDPtr &fd);
    void addSubReactor(Subscription &outSub, FDCallbackType type, int fd, const BoostFnReactorPtr &reactor, const boost::shared_ptr<void> &heldPtr);
    void removeFromSet(FDEntry *entry, FDCallbackType type, SDReactorBase *reactor); // called from ~SDReactorBase

    FDSet_t m_fds;

    int m_epollFD;
    // the buffer that we pass to epoll_wait. We keep m_eventBuf.size() == m_fds.size().
    // (i.e. enough space for all FDs to be ready).
    // it doesn't necessarily have to be this way - if the number of FDs is huge we could limit this.
    mutable std::vector<epoll_event> m_eventBuf;

    // The queue of work deferred during IO callbacks. We work this
    // queue off before doing another call into epoll.
    typedef shared_vector<ReactorCallback> WorkQueue;
    BB_DECLARE_SHARED_PTR(WorkQueue);
    WorkQueuePtr m_deferredWorkQueue;
    WorkQueuePtr m_nextDeferredWorkQueue;

    uint32_t        m_fdRdyCount;
    bb::timeval_t   m_selectRdyTime;
    bb::timeval_t   m_selectPollTime;
    bool            m_useTimestamps;
    static boost::thread_specific_ptr<bool> m_spin;
};

/// Implementation class for select(). Given a list of policies, it passes
/// all the readers & writers to the appropriate policies.
/// Policies_t should be a boost::fusion::list of policies to compile in.
template<typename Policies_t>
struct SDPolicyDriver : public EPollFDSet
{
protected:
    SDPolicyDriver(int numFDSHint = EPollFDSet::DEFAULT_SIZE) : EPollFDSet(numFDSHint) {}
    typedef SDPolicyDriver SDScheduler_t;

    virtual void setupFallbackQueue(FDCallbackType type, BoostFnReactor *react)
    {
        // there can only be one FallbackPolicy in Policies_t
        BOOST_MPL_ASSERT((
            boost::mpl::equal_to<
                boost::mpl::int_<1>,
                boost::mpl::size<FallbackPolicies_t> >));
        typedef typename boost::remove_reference<
            typename boost::fusion::result_of::deref<
                typename boost::fusion::result_of::begin<FallbackPolicies_t>::type>::type>::type
                    FallbackPolicy_t;
        FallbackPolicy_t &t = boost::fusion::deref(boost::fusion::begin(FallbackPolicies_t(m_policies)));
        t.setupFallbackQueue(type, react);
    }

    /// Return the SDPolicy which can dispatch to the given Transport, for passing
    /// to FDSet::addReader/addWriter
    template<typename Transport_t> SDPolicyHandleFor<Transport_t> getPolicy()
    {
        typedef boost::fusion::filter_view<FastPolicies_t, SDPolicyHandlerPred<boost::mpl::_, Transport_t> > MatchingPolicies;
        BOOST_MPL_ASSERT_NOT((boost::mpl::empty<MatchingPolicies>)); // there's no Policy which can handle Transport_t
        BOOST_MPL_ASSERT((
            boost::mpl::equal_to<
                boost::mpl::int_<1>,
                boost::mpl::size<MatchingPolicies> >)); // there's more than one Policy which can handle Transport_t (ambiguous)
        typedef typename boost::remove_reference<
            typename boost::fusion::result_of::deref<
                typename boost::fusion::result_of::begin<MatchingPolicies>::type>::type>::type
                    MatchingPolicy_t;
        FastPolicies_t fastPolicies(m_policies);
        MatchingPolicy_t &t = boost::fusion::deref(
                boost::fusion::begin(
                    MatchingPolicies(fastPolicies)));
        return SDPolicyHandleFor<Transport_t>(boost::bind(&MatchingPolicy_t::setupQueue, &t, _1));
    }

    struct ServiceOp
    {
        template<typename T> void operator()(T &t) const { t.service(); }
    };
    struct ETWorkPendingOp
    {
        typedef bool result_type;
#if BOOST_VERSION >= 104200
        // newer boost versions have flipped the arguments from pre 1.42 versions
        // see: https://svn.boost.org/trac/boost/ticket/2355
        template<typename T> bool operator()(bool pending, T &t) const
#else
        template<typename T> bool operator()(T &t, bool pending) const
#endif
        {
            return pending || t.edgeTriggeredWorkPending();
        }
    };

    /// Calls select() or epoll_wait(), and calls the readers/writers for all ready FDs.
    FDSet::SelectResult selectMillis(int millis)
    {
        // we might need to use 0 timeout because we've got work pending
        bool etWorkPending = boost::fusion::fold(m_policies, false, ETWorkPendingOp());
        int nextTimeout = etWorkPending ? 0 : millis;

        // go into the FDSet and see what's ready
        FDSet::SelectResult r = EPollFDSet::selectMillis(nextTimeout);
        if(unlikely(r != FDSet::QUEUED)) return r;

        // service each policy
        boost::fusion::for_each(m_policies, ServiceOp());
        return r;
    }

    typedef boost::fusion::filter_view<Policies_t, boost::mpl::not_<SDFallbackPolicyPred<boost::mpl::_> > > FastPolicies_t;
    typedef boost::fusion::filter_view<Policies_t, SDFallbackPolicyPred<boost::mpl::_> > FallbackPolicies_t;

    Policies_t m_policies;
};

class ISelectDispatcher
{
public:
    ISelectDispatcher();
    virtual ~ISelectDispatcher();

    //these just convert the unit into milliseconds and call selectMillis
    virtual bool select(int sec = -1);
    virtual bool select(const bb::ptime_duration_t &timeout);

    virtual bool selectMillis(int millis = -1) = 0;
};

BB_DECLARE_SHARED_PTR( ISelectDispatcher );

/// Default implementation of FDSet.
/// Passes all readers/writers to the unoptimized FallbackPolicy.
class SelectDispatcher : public SDPolicyDriver<boost::fusion::list<SDFallbackPolicy> >, public ISelectDispatcher
{
public:
    /// numberFDsHint is a hint on the number of file descriptors which
    /// will be tracked, it doesn't need to be exact.
    SelectDispatcher(int numberFDsHint = EPollFDSet::DEFAULT_SIZE);

    /// Waits until there is data available, or the timeout expires.
    /// If there is data available, calls the callbacks for that FD.
    /// Returns true if there was a timeout; default argument means no timeout.
    /// Throws CError on error.
    virtual bool selectMillis(int millis = -1);
};

BB_DECLARE_SHARED_PTR( SelectDispatcher );

inline bool ISelectDispatcher::select(int sec)
{
    if(sec == -1)
    {
        return selectMillis();
    }

    BB_ASSERT(sec >= 0);
    BB_ASSERT(sec < std::numeric_limits<int>::max() / 1000);

    return selectMillis(sec * 1000);
}

inline bool ISelectDispatcher::select(const bb::ptime_duration_t &timeout)
{
    BB_ASSERT(!timeout.is_special());
    BB_ASSERT(!timeout.is_negative());
    BB_ASSERT(timeout.total_seconds() + 1 < std::numeric_limits<int>::max() / 1000);

    // convert to milliseconds, rounding up rather than down.
    // for stuff which is aiming to wakeup at a specific time, you don't want to
    // wakeup too early (you might wakeup, check the time, discover it's too early
    // and have to sleep again).
    int millis = (timeout.total_nanoseconds() + 999999) / 1000000;
    return selectMillis(millis);
}

} // namespace bb

#endif // BB_CORE_SELECTDISPATCHER_H

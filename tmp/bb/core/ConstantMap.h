#ifndef BB_CORE_CONSTANTMAP_H
#define BB_CORE_CONSTANTMAP_H

/* Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved. */


#include <utility>
#include <vector>
#include <algorithm>
#include <functional>
#ifdef CENTOS6
#include <bitset>
#endif

#include <bb/core/Error.h>
#include <bb/core/bbassert.h>
#include <bb/core/str_map.h>  // for bb_ltstr

namespace bb {

// ConstantMap is used in const-gen.py generated code to do efficient
// bi-directional constant to string conversions. Instead of using a
// std::map, it uses sorted std::vectors and lower-bound. Adding new
// entries is somewhat expensive as the vectors must be
// re-sorted. Lookups are very efficient. In general, T will be an
// enumerated type.

template<typename T, int MapSize>
class ConstantMap
{
    typedef std::pair<T, const char*> TypeToStringEntry;
    typedef std::vector<TypeToStringEntry> TypeToStringMap;

    typedef std::pair<const char*, T> StringToTypeEntry;
    typedef std::vector<StringToTypeEntry> StringToTypeMap;

    template<typename PairType, typename Comparator>
    class CompareFirst : public std::binary_function<PairType, PairType, bool>
    {
    public:
        inline CompareFirst( const Comparator& comparator = Comparator() )
            : m_comparator( comparator ) {}

        // For sort
        inline bool operator()( const PairType& left, const PairType& right )
        {
            return m_comparator( left.first, right.first );
        }

        // For lower_bound
        inline bool operator()( const PairType& pair, const typename PairType::first_type& value )
        {
            return m_comparator( pair.first, value );
        }

        // For lower_bound
        inline bool operator()( const typename PairType::first_type& value, const PairType& pair )
        {
            return m_comparator( value, pair.first );
        }

    private:
        const Comparator m_comparator;
    };

    typedef CompareFirst<TypeToStringEntry, std::less<T> > TypeToStringComparator;
    typedef CompareFirst<StringToTypeEntry, bb_ltstr>      StringToTypeComparator;

public:

    inline ConstantMap()
        : m_typeToStringMap()
        , m_stringToTypeMap() {}

    // Returns the number of bijective mappings (aliases are ignored).
    inline size_t size() const
    {
        BB_ASSERT( m_typeToStringMap.size() <= m_stringToTypeMap.size() );
        return m_typeToStringMap.size();
    }

    inline bool empty() const
    {
        return (size() == 0);
    }

    // Adds a new entry to the map. 'value' must point to a string
    // which will outlive the ConstantMap; a string literal is
    // intended. Duplicate entries are forbidden. This implies a sort
    // operation. If you are adding a number of elements in one place,
    // consider the three argument version of 'add' below.
    //
    // If the mapping option is set to 'inhibit', then the mapping
    // from the typeValue to the stringValue will not be recorded,
    // though the reverse mapping will. This is useful for cases where
    // the same numeric value has two valid string representations, so
    // we want two entries in the string to type map, but only one in
    // the type-to-string map.
    enum MappingOptions { MAPPING_NORMAL, MAPPING_INHIBIT_TO_STRING };

    void add( const T& typeValue, const char* stringValue,
              const MappingOptions options = MAPPING_NORMAL )
    {
        const SortPromise promise( *this );
        add( promise, typeValue, stringValue, options );
    }

    class SortPromise
    {
        friend class ConstantMap;
    public:
        inline SortPromise( ConstantMap& map )
            : m_map( map ) {}

        inline ~SortPromise()
        {
            m_map.sort();
        }

    private:
        ConstantMap& m_map;
    };
    friend class SortPromise;


    // Like the two argument add, but does not sort the entries after
    // each addition - you provide a promise to sort them later.
    void add( const SortPromise& promise,
              const T& typeValue, const char* stringValue,
              const MappingOptions options = MAPPING_NORMAL )
    {
        BB_ASSERT( this == &promise.m_map );

        if( options == MAPPING_NORMAL )
        {
            BB_THROW_EXASSERT_SSX(
                !find( typeValue, NULL ) && !find( stringValue, NULL ),
                "Attempt to add duplicate key/value to ConstantMap: " << typeValue << " : " << stringValue );
        }
        else if( options == MAPPING_INHIBIT_TO_STRING )
        {
            BB_THROW_EXASSERT_SSX(
                find( typeValue, NULL ) && !find( stringValue, NULL ),
                "Attempt to add aliasing key/value, but no aliasee exists: " << typeValue << " : " << stringValue );
        }

        if( options != MAPPING_INHIBIT_TO_STRING )
            m_typeToStringMap.push_back( std::make_pair( typeValue, stringValue ) );

        m_stringToTypeMap.push_back( std::make_pair( stringValue, typeValue ) );
        m_validKeys.set( static_cast<uint32_t>(typeValue) );
    }

    bool isKeyValid( const T& typeValue ){
        try{
            return m_validKeys.test( static_cast<uint32_t>(typeValue) );
        }catch(const std::out_of_range& e){
            return false;
        }
    }

    inline void clear()
    {
        m_typeToStringMap.clear();
        m_stringToTypeMap.clear();
    }

    // Look for the string matching 'key' in the TypeToString map. If the
    // element is found, returns 'true' and assigns to *pval. If the
    // element is not found, returns false, and pval is not
    // modified. If pval is null, find will not assign to it, this can
    // be used as an existence check.
    inline bool find( const T& key, const char** const pval ) const
    {
        return find_internal<TypeToStringMap, TypeToStringComparator>( m_typeToStringMap, key, pval );
    }

    // Look for the T value matching 'key' in the StringToType map. If the
    // element is found, returns 'true' and assigns to *pval. If the
    // element is not found, returns false, and pval is not
    // modified.
    inline bool find( const char* const key, T* const pval ) const
    {
        return find_internal<StringToTypeMap, StringToTypeComparator>( m_stringToTypeMap, key, pval );
    }

private:

    inline void sort()
    {
        std::sort( m_typeToStringMap.begin(), m_typeToStringMap.end(), TypeToStringComparator() );
        std::sort( m_stringToTypeMap.begin(), m_stringToTypeMap.end(), StringToTypeComparator() );
    }

    template<typename MapType, typename Comparator>
    static inline bool find_internal( const MapType& map,
                                      const typename MapType::value_type::first_type& key,
                                      typename MapType::value_type::second_type* const pval )
    {
        Comparator comp;

        const typename MapType::const_iterator end = map.end();
        const typename MapType::const_iterator where =
            std::lower_bound( map.begin(), end, key, comp );

        if( where == end || comp( key, *where ) )
            return false;

        if( pval )
            *pval = where->second;

        return true;
    }

    TypeToStringMap m_typeToStringMap;
    StringToTypeMap m_stringToTypeMap;

    /// Bitset for testing key values
    std::bitset<MapSize> m_validKeys;
};

}; // namespace bb

#endif // BB_CORE_CONSTANTMAP_H

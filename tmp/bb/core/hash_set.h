#ifndef BB_CORE_HASH_SET_H
#define BB_CORE_HASH_SET_H

/* Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved. */



#if (defined( __INTEL_COMPILER ) && ( __INTEL_COMPILER <= 800 )) || defined( __INTEL_CXXLIB_ICC )

#include <hash_set>
#define bbext std

#elif defined(WIN32) || defined(WIN64)

#include <hash_set>
#define bbext stdext

#elif defined( __GNUC__ )

#include <tr1/unordered_set>
#define bbext std::tr1
#define hash_set unordered_set
#define hash_multiset unordered_multiset

#else

#error bbext::hash_set not supported on this platform

#endif

#endif // BB_CORE_HASH_SET_H

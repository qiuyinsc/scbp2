#ifndef BB_CORE_STREAMEXCEPTBASE_H
#define BB_CORE_STREAMEXCEPTBASE_H

/* Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved. */


#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
#include <iosfwd>
#include <ios>



namespace bb {

template<class stream_t>
void
stream_exception_handler(stream_t& strm, //the stream
                           std::ios_base::iostate& err,// the reported error
                           std::ios_base::iostate err_default) //default err state
{
    const std::ios_base::iostate exception_mask(strm.exceptions());
    if(err==std::ios_base::goodbit)//not set by callee, so default to fail
    {
        const std::ios_base::iostate strm_state(strm.rdstate());
        err =(strm_state==std::ios_base::goodbit)?err_default:strm_state;
    }
    if(err & exception_mask)//the exception matches something in exception mask
    {
        try { strm.setstate( err ); }
        catch( std::ios_base::failure& ) { }
        throw;
    }
}

}


#endif // BB_CORE_STREAMEXCEPTBASE_H

#ifndef BB_CORE_GDATEFWD_H
#define BB_CORE_GDATEFWD_H

/* Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved. */


/// boost's gregorian::date in bb is gdate_t, rep
/// See http://www.boost.org/doc/html/date_time/gregorian.html
/// Internally boost::gregorian::date is stored as a 32 bit integer type.  The
/// class is specifically designed to NOT contain virtual functions.  This design
/// allows for efficient calculation and memory usage with large collections of
/// dates.  The construction of a date validates all input so that it is not
/// possible to construct and 'invalid' date.  That is 2001-Feb-29 cannot be
/// constructed as a date. Various exceptions derived from std::out_of_range are
/// thrown to indicate which aspect of the date input is invalid.  Note that the
/// special value not-a-date-time can be used as 'invalid' or 'null' date if so
/// desired.

namespace boost { namespace gregorian { class date; } }

namespace bb {

typedef boost::gregorian::date gdate_t;
int gdate2YMDDate(const gdate_t &gdt);

} // namespace bb

#endif // BB_CORE_GDATEFWD_H

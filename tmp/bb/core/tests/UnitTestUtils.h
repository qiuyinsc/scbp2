#ifndef BB_CORE_TESTS_UNITTESTUTILS_H
#define BB_CORE_TESTS_UNITTESTUTILS_H

/* Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved. */


// some random utils that are useful for unit tests

#include <cstring>
#include <cstdlib>
#include <cppunit/extensions/HelperMacros.h>
#include <cppunit/CompilerOutputter.h>
#include <cppunit/extensions/TestFactoryRegistry.h>
#include <cppunit/ui/text/TestRunner.h>
#include <cppunit/TestResult.h>
#include <cppunit/TestResultCollector.h>
#include <cppunit/TextOutputter.h>
#include <cppunit/XmlOutputter.h>
#include <stdexcept>
#include <boost/thread.hpp>
#include <boost/function.hpp>
#include <boost/format.hpp>
#include <boost/scoped_array.hpp>
#include <bb/core/CError.h>

CPPUNIT_NS_BEGIN

// Template specialization for std::string so it prints with quotes ("")
template<>
struct assertion_traits<std::string>
{
    static bool equal(const std::string& left, const std::string& right)
    {
        return (left == right);
    }

    static std::string toString(const std::string& text)
    {
        OStringStream oss;
        oss << '"' << text << '"';
        return oss.str();
    }
};

// Template specialization for const char* so we can call CPPUNIT_ASSERT_EQUAL
// on C-style strings.
template<>
struct assertion_traits<const char*>
{
    static bool equal(const char* left, const char* right)
    {
        return (strcmp(left, right) == 0);
    }

    static std::string toString(const char* text)
    {
        OStringStream oss;
        oss << '"' << text << '"';
        return oss.str();
    }
};

/// Gives us the ability to do const char[] comparisons to const char* so we can
/// call CPPUNIT_ASSERT_EQUAL on static C-strings of known size.
template<class ConstCharType, class T>
void assertEquals(const ConstCharType& expected, const T& actual,
    SourceLine sourceLine, const std::string& message)
{
    // Use const char* conversion
    if (!assertion_traits<T>::equal(expected, actual))
    {
        Asserter::failNotEqual(assertion_traits<const ConstCharType>::toString( expected ),
            assertion_traits<T>::toString(actual), sourceLine, message);
    }
}

CPPUNIT_NS_END

namespace bb {
namespace tests {

inline std::string readFile(const char *f)
{
    FILE *inF = fopen(f, "r");
    if(!inF)
        throw std::runtime_error("couldn't open " + std::string(f));

    std::string result;
    while(!feof(inF))
    {
        char buf[256];
        memset(buf, 0, sizeof(buf));
        size_t sz = fread(buf, 1, sizeof(buf)-1, inF);
        if(ferror(inF) != 0)
        {
            std::string err = "couldn't read file ";
            err += f;
            err += ": ";
            err += strerror(ferror(inF));
            throw std::runtime_error(err);
        }
        result.append(buf,sz);
    }
    fclose(inF);
    return result;
}

/// Helper for multithreaded cppunit tests.
///
/// If you use a raw boost:thread in your test and fail a CPPUNIT_ASSERT
/// somewhere inside the thread, it'll terminate without giving contextual
/// information. Creating a tests::TestThread avoids this.
/// You must join() the thread before your test finishes.
class TestThread : protected boost::thread
{
public:
    typedef boost::function<void ()> FuncType;

    explicit TestThread(FuncType const &f)
        : boost::thread( boost::bind( &TestThread::runWithGuard, this, f) )
        , m_needsThrow(false)
        , m_exception(NULL)
    {
    }

    ~TestThread()
    {
        delete m_exception;
    }

    template<typename TimeDuration>
    bool timed_join(TimeDuration const& rel_time)
    {
        bool ret = boost::thread::timed_join(rel_time);
        if(ret)
            checkException();
        return ret;
    }

    bool try_join() { return timed_join(boost::posix_time::seconds(0)); }

    void join()
    {
        boost::thread::join();
        checkException();
    }
    bool joinable() { return boost::thread::joinable(); }

protected:
    void checkException()
    {
        if(m_exception && m_needsThrow)
        {
            m_needsThrow = false;
            throw *m_exception;
        }
    }

    void runWithGuard(FuncType const &f)
    {
        try
        {
            f();
        }
        catch(const CppUnit::Exception &e)
        {
            m_exception = e.clone();
        }
        catch(const std::exception &e)
        {
            std::string msg = "TestThread: thread terminated with exception ";
            msg += e.what();
            m_exception = new CppUnit::Exception(CppUnit::Message(msg));
        }
        catch(...)
        {
            m_exception = new CppUnit::Exception(CppUnit::Message(
                "TestThread: thread terminated with unknown exception"));
        }
    }

    bool m_needsThrow;
    CppUnit::Exception *m_exception;
};

inline std::string getTestRundir()
{
    const char* rundir = getenv("BB_TEST_RUNDIR");
    BB_THROW_EXASSERT( rundir != NULL && *rundir != '\0', "BB_TEST_RUNDIR is not set" );
    return std::string(rundir);
}

inline std::string inTestRundir(const char* name)
{
    // CppUnit instantiates a test class once for every test, all up front, so we need to
    // generate a different ClientContext ID string for each Utilities instance in the program,
    // otherwise the separate TradingContexts will open and try to lock the same IOT log more than once
    static unsigned instanceNum = 0;

    std::ostringstream stream;
    stream << getTestRundir() << '/' << ++instanceNum << '.' << name;
    return stream.str();
}

inline std::string makeTempDirInTestRundir(const char* subtemplate = "XXXXXX")
{
    const std::string const_template = tests::inTestRundir( subtemplate );
    boost::scoped_array<char> mutable_template( new char[const_template.size() + 1] );
    memcpy( &mutable_template[0], const_template.data(), const_template.size() );
    mutable_template[const_template.size()] = '\0';
    const char* result = mkdtemp( &mutable_template[0] );
    BB_THROW_EXASSERT_SSX_ERRNO( result != 0, "Failed to create temp directory: " << std::string(&mutable_template[0]) );
    return result;
}

inline std::pair<int, std::string> openTempFileInTestRundir(const char* subtemplate = "XXXXXX")
{
    const std::string const_template = tests::inTestRundir( subtemplate );
    boost::scoped_array<char> mutable_template( new char[const_template.size() + 1] );
    memcpy( &mutable_template[0], const_template.data(), const_template.size() );
    mutable_template[const_template.size()] = '\0';
    const int fd = mkstemp( &mutable_template[0] );
    BB_THROW_EXASSERT_SSX_ERRNO( fd != -1, "Failed to create temp file: " << std::string(&mutable_template[0]) );
    return std::make_pair( fd, std::string( &mutable_template[0] ) );
}

inline int cppUnitMain( const char * xml_filename = NULL )
{
    // Get the registry
    CppUnit::TestFactoryRegistry& registry = CppUnit::TestFactoryRegistry::getRegistry();

    // Adds the test to the list of test to run
    CppUnit::TextUi::TestRunner runner;
    CppUnit::TestResult controller;
    runner.addTest( registry.makeTest() );

    CppUnit::CompilerOutputter *outputter = new CppUnit::CompilerOutputter(
            &runner.result(), std::cerr );
    outputter->setLocationFormat("%p:%l:"); // include the full path in the test failures

    // Change the default outputter to a compiler error format outputter
    runner.setOutputter( outputter );

    struct output_holder_t
    {
        output_holder_t ( CppUnit::TestResult & controller,
               const char * xml_filename ) :
               xml_filename ( xml_filename )
        {
            controller.addListener (&collectedresults);
        }

        ~output_holder_t()
        {
            if ( xml_filename != NULL )
            {
                std::ofstream xmlFileOut( xml_filename );
                CppUnit::XmlOutputter xmlOut(&collectedresults, xmlFileOut);
                xmlOut.write();
            } else {
                CppUnit::TextOutputter textOutputter (&collectedresults, std::cout);
                textOutputter.write();
            }
        }

        bool wasSuccessful() const
        {
            return collectedresults.wasSuccessful();
        }

        CppUnit::TestResultCollector collectedresults;
        const char * xml_filename;
    };
    output_holder_t output_holder ( controller, xml_filename );

    // Run the tests.
    runner.run( controller );
    const bool wasSuccessful ( output_holder.wasSuccessful() );

    // Return error code 1 if the one of test failed.
    return wasSuccessful ? EXIT_SUCCESS : EXIT_FAILURE;
}

} // namespace tests
} // namespace bb

#endif // BB_CORE_TESTS_UNITTESTUTILS_H

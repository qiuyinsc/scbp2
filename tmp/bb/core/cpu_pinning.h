#ifndef BB_CORE_CPU_PINNING_H
#define BB_CORE_CPU_PINNING_H

/* Contents Copyright 2016 Athena Capital Research LLC. All Rights Reserved. */

#include <pthread.h>
#include <string.h>
#include <sched.h>
#include <errno.h>

#include <algorithm>

#include <boost/type_traits.hpp>
#include <boost/format.hpp>
#include <boost/lexical_cast.hpp>
#include <boost/thread.hpp>
#include <boost/foreach.hpp>
#include <boost/array.hpp>

#include <bb/core/Log.h>
#include <bb/core/Error.h>

namespace bb {
    // TODO: namespace current_thread
    static const int NO_PINNING = -1;

    // 'Pin' the current thread exclusively to the specified cores.
    // Will fail if we try to pin to a core that's too high, for instance if we want
    // to pin to core #100 on a 32 core machine. Will also fail if for any reason
    // we can't 'phtread_setaffinitiy_np'.
    template<typename Collection>
    typename boost::enable_if<boost::is_integral<typename Collection::value_type>, bool>::type
    pin_to_cores(const Collection & cores, const bool verbose = false)
    {
        cpu_set_t cpuset;
        CPU_ZERO( &cpuset );
        BOOST_FOREACH(typename Collection::value_type core_num, cores)
        {
            BB_THROW_EXASSERT ( static_cast<int>(core_num) < static_cast<int>( boost::thread::hardware_concurrency() ),
                    ( boost::format("Can't pin to core #%1% because we only have %2% cpus.") % core_num % boost::thread::hardware_concurrency() ).str());
            CPU_SET( static_cast<int>(core_num), &cpuset );
        };
        const int res = pthread_setaffinity_np( pthread_self(), sizeof(cpuset), &cpuset );
        BB_THROW_EXASSERT( res == 0,
            ( boost::format( "CPU pinning failed with error code (%1%) description: %2%" ) % res % strerror(res) ).str() );

        if ( unlikely ( verbose ) )
        {
            std::ostringstream cpu_set_stream;
            std::copy(cores.begin(),
                cores.end(),
                std::ostream_iterator<typename Collection::value_type>(cpu_set_stream, ","));
            std::string set_string = cpu_set_stream.str();
            set_string.erase(set_string.length()-1);
            LOG_INFO << "Thread " << boost::lexical_cast<std::string>(boost::this_thread::get_id()) << " pinned to CPUS #" << set_string<< bb::endl;
        }
        return true;
    }

    // 'Pin' the current thread exclusively to the specified cores.
    // Will fail if we try to pin to a core that's too high, for instance if we want
    // to pin to core #100 on a 32 core machine. Will also fail if for any reason
    // we can't 'phtread_setaffinitiy_np'.
    // We're happy to take an integer type array; say int[32] or uint32_t[32].
    template<typename Type, int LEN>
    typename boost::enable_if<boost::is_integral<Type>, bool>::type
    pin_to_cores( const Type(& cores)[LEN], const bool verbose = false )
    {
        boost::array<Type,LEN> arr;
        for(size_t i = 0 ; i < LEN; i++)
        {
            arr[i] = cores[i];
        }
        return pin_to_cores(arr, verbose);
    }

    // 'Pin' the current thread exclusively to the specified core.
    // Will fail if we try to pin to a core that's too high, for instance if we want
    // to pin to core #100 on a 32 core machine. Will also fail if for any reason
    // we can't 'phtread_setaffinitiy_np'.
    bool pin_to_core(const int core, const bool verbose = false );

    // Un'pin' from any specific cpu. In other words, set the affinity of the current
    // thread to 'all cpus'.
    bool unpin(const bool verbose = false);

    // Returns a vector of cpu core numbers for which the current thread
    // has an affinity. This will be just a vector of all cores if we don't have
    // any specific affinity.
    std::vector<int> get_pinned_cores( const bool verbose = false );

    // Returns whether or not the current thread is bound
    // *exclusively* to the cores specified. So, it will return false if:
    // - it's tied to another cpu that isn't specified in the collection
    // - it isn't tied to a core that is specified in the collection
    template<typename Collection>
    typename boost::enable_if<boost::is_integral<typename Collection::value_type>, bool>::type
    am_pinned_to_cores(const Collection & cores, const bool verbose = false)
    {
        const std::vector<int> pinned_cores ( get_pinned_cores ( verbose ) );
        return pinned_cores.size() == cores.size() &&
            std::equal(cores.begin(), cores.end(), pinned_cores.begin());
    }

    // Returns whether or not the current thread is bound
    // *exclusively* to the cores specified. So, it will return false if:
    // - it's tied to another cpu that isn't specified in the collection
    // - it isn't tied to a core that is specified in the collection
    // We're happy to take an integer type array; say int[32] or uint32_t[32].
    template<typename Type, int LEN>
    typename boost::enable_if<boost::is_integral<Type>, bool>::type
    am_pinned_to_cores( const Type(& cores)[LEN], const bool verbose = false )
    {
        boost::array<Type,LEN> arr;
        for(size_t i = 0 ; i < LEN; i++)
        {
            arr[i] = cores[i];
        }
        return am_pinned_to_cores(arr, verbose);
    }

    // Returns whether or not the current thread is bound
    // *exclusively* to the core specified. So, it will return false if:
    // - it's tied to another cpu as well
    // - it isn't tied to the specified core
    bool am_pinned_to_core(const int core, const bool verbose = false);
}

#endif // BB_CORE_CPU_PINNING_H

#ifndef BB_CORE_TIMEVAL_H
#define BB_CORE_TIMEVAL_H

/* Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved. */


#include <cmath>
#include <cstring>
#include <limits>
#include <string>
#include <iostream>

#include <boost/static_assert.hpp>

#include <bb/core/bbassert.h>
#include <bb/core/bbint.h>
#include <bb/core/compat.h>

namespace bb {

/// timeval_t is a time representation accurate to the nanosecond.
/// This class should only be used to store absolute points in time, i.e.
/// the number of seconds since 1970, and that is its predominant use in BB.
/// Historically, timeval_t was used for all sorts of things in BB: absolute
/// times, relative times, intervals, negative offsets, latencies, etc.
/// Negative time representations probably aren't what you expect.  A timeval_t
/// of -1.6 seconds would actually be (-2, 400000), not (-1, -600000).  This is
/// done to make some of the internal math simpler.  You may, however, construct
/// a timeval_t(-1, -600000) and expect it to do the right thing.  Comparisons
/// with negative timevals should work as expected.
class timeval_t
{
public:
    enum FormatStyle
    {
        FORMAT_HHMMSS = 0,
        FORMAT_HHMMSSUUUUUU = 1,
        FORMAT_DEFAULT = 2
    };

    /// current_time is a type indicator for using timeval_t's with the current
    /// time.  See the timeval_t class variable "now".
    struct current_time
    {
        operator timeval_t() const { return timeval_t(*this); }
        timeval_t operator()() const { return timeval_t(*this); }
    };

    /// Serves as a placeholder for manipulating timeval_t's with the current
    /// time.  For example, "timeval_t t = timeval_t::now" will set t to the
    /// current time.  "t < timeval_t::now" will compare t with the current time.
    static current_time now;

    /// Beginning of time (no absolute timeval_t is earlier than this)
    static const timeval_t earliest;

    /// End of time (no timeval_t is later than this)
    static const timeval_t latest;

    /// Default constructor sets timeval_t to the start of 1970, GMT.
    timeval_t();

    /// Sets this timeval_t to the current time.
    explicit timeval_t(const current_time &);

    /// Sets this timeval_t to the given SEC and USEC
    timeval_t(int32_t _sec, int32_t _usec);
    timeval_t(int32_t _sec);

    explicit timeval_t(uint32_t _sec);

    timeval_t( double sec );

    /// Constructs a timeval_t corresponding to the given string.
    /// The format is strictly "[-]secs[.nsecs]", which means this can parse
    /// the output of timeval_t operator<< and toString().
    /// If the string is invalid, a bb::Error is thrown.
    explicit timeval_t( const char* sz );

    static timeval_t from_millisec( const int64_t millis );

    static timeval_t from_microsec( const int64_t micros );

    /// Converts this timeval_t to milliseconds.
    int64_t to_millisec() const;

    /// Converts this timeval_t to microseconds.
    int64_t to_microsec() const;

    /// Converts this timeval_t to nanoseconds.
    int64_t to_nanosec() const;

    /// Converts this timeval_t to a struct timespec (for use with
    /// mq_timedreceive, stat, nanosleep, etc.).
    timespec to_timespec() const;
    static timeval_t from_timespec( const timespec& ts );

    timeval to_timeval() const;
    static timeval_t from_timeval( const timeval& tv );

    /// Returns a string corresponding to "sec.usec"
    /// Same as the value output by operator<<
    std::string toString() const;

    /// Same as C strftime(3).
    /// A return value of 0 indicates an error unless the format produced an empty string.
    size_t strftime( char* buf, int32_t len, const std::string& fmt ) const;
    std::string strftime( const std::string& fmt ) const;

    /// Returns a double corresponding to the "sec.usecnsec"
    double toDouble() const;

    /// Returns the seconds portion of this timeval_t.
    int32_t sec() const { return m_sec; }
    /// Returns the fractional portion of this timeval_t in microseconds.
    int32_t usec() const { return m_fraction.f.usec; }
    /// returns the fractional portion of this timeval_t in nanoseconds.
    int32_t nsec() const { return m_fraction.f.usec * 1000 + m_fraction.f.nsec; }

    /// Returns the seconds portion of this timeval_t, rounding fractional parts.
    int32_t sec_round() const { return m_sec + ( m_fraction.f.usec >= 500000 ? 1 : 0 ); }

    /// Sets this timeval_t to the current time.
    /// Default behavior is to use the realtime clock.  This means that time can
    /// go backwards versus CLOCK_MONOTONIC where time always goes forwards on
    /// the system.
    const timeval_t set_current(clockid_t cid = CLOCK_REALTIME);

    bool before( const timeval_t& other ) const;
    bool on_or_before( const timeval_t& other ) const;
    bool after( const timeval_t& other ) const;
    bool on_or_after( const timeval_t& other ) const;

    bool between( const timeval_t& first, const timeval_t& last ) const;
    bool on_or_between( const timeval_t& first, const timeval_t& last ) const;
    bool outside( const timeval_t& first, const timeval_t& last ) const;
    bool on_or_outside( const timeval_t& first, const timeval_t& last ) const;

    /// Does not support msec values which are negative or overflow into the seconds part
    static timeval_t from_sec_msec( int32_t sec, uint32_t msec );

    /// Does not support nsec values which are negative or overflow into the seconds part
    static timeval_t from_sec_nsec( int32_t sec, uint32_t nsec );

    /// Slower version of from_sec_nsec for use where nsec could be negative or huge
    static timeval_t from_sec_nsec_unrestricted( int32_t sec, int64_t nsec );

    /// Converts an absolute time into a timeval_t.
    static timeval_t make_time     (int year, int month, int day, int hour, int minute, int sec);
    static timeval_t make_time     (int year, int month, int day, int hour, int minute, int sec, int usec);
    static timeval_t make_time_nsec(int year, int month, int day, int hour, int minute, int sec, int nsec);

    /// date is a number of the form YYYYMMDD
    static timeval_t make_time(int date, int hour = 0, int minute = 0, int sec = 0);

    /// Returns a timeval_t corresponding to the given string.
    /// Valid input formats:
    ///   "YYYYMMDD HHMMSS.uuuuuu" or   this is identical to the date_t operator<<
    ///   "YYYY-MM-DD HH:MM:SS.uuuuuu"  (some or all of the decimal part can be omitted)
    ///
    ///   "YYYYMMDD" or "YYYY-MM-DD"    the time-part will be set to midnight
    ///
    ///   "HHMMSS" or "HH:MM:SS"        the day-part will be set to today
    ///                                 (some or all of the decimal part can be omitted)
    ///
    ///   "ssssssssss.uuuuuu"           seconds.microseconds since the epoch
    ///                                 (some of the u's can be omitted)
    ///                                 secs must be 9 or 10 digits, so can represent
    ///                                 1973-03-04 to 2038-01-19 (end of Unix time)
    ///
    /// If the string is invalid, a bb::Error is thrown.
    static timeval_t make_time( const char* sz );
    static timeval_t make_time( const std::string& str ) { return make_time( str.c_str() ); }
    static timeval_t make_time( std::string& date_str, std::string& time_str ){ return make_time( date_str + " " + time_str ); }

    /// Converts a double hhmmss.uuuuuu to a timeval.
    /// Throws if hhmmss is malformed.
    static timeval_t from_hhmmss( double hhmmss );

    /// Converts a non-negative integral hhmmss to a a timeval.
    /// Throws if hhmmss is malformed.
    static timeval_t from_hhmmss( int32_t hhmmss );

    static timeval_t from_hms( const uint32_t hour, const uint32_t min, const uint32_t sec );

    static timeval_t from_hmsu( const uint32_t hour, const uint32_t min, const uint32_t sec, const uint32_t usec );

    static timeval_t from_hms( const std::string& timestr );

    /// Set the decimal precision to print (from 0 to 9)
    static void set_print_precision( int prec );
    static int get_print_precision() { return s_print_precision; }

private:
    /// Set the seconds
    void set_sec(int32_t sec);

    /// Set the microseconds (sets the nanoseconds to zero)
    void set_usec(int64_t usec);

    /// Set the nanoseconds (also sets the microseconds)
    void set_nsec(int64_t nsec);

    /// Faster versions of the above for non-negative input
    /// These do not support values which overflow into the seconds part
    void set_usec_unsigned(uint32_t usec);
    void set_nsec_unsigned(uint32_t nsec);

    // note that the total storage size should be 8 bytes (two 32 bit integers)
    // this is to maintain backwards compatibility with the previous timeval_t
    // which had int32 microseconds. Because microseconds are < 1000000 there
    // were unused bits which could contain nanoseconds beyond microseconds
    // binary compat is kept at the expense of more annoying access

    // we store our bit-fields into a single struct with an efficient default constructor
    // otherwise the timeval_t constructors would initialize the fields one-by-one,
    // and GCC is not smart enough to see that it can be done with a single four-byte write
    struct fraction_t
    {
        unsigned int usec:20;

        // not actual nanoseconds, actually nanoseconds - microseconds
        // (i.e. nanoseconds past the last microsecond)
        // milli micro seconds would be a better term
        unsigned int nsec:10;

        // unused bits available for later magic
        unsigned int unused:2;
    };

    union fraction_holder_t
    {
        fraction_holder_t() : n(0) {}
        fraction_t f;
        uint32_t   n;
    };

    // similar to time_t, but explicitly 32 bits for binary compatibility in BB messages
    int32_t m_sec;
    // fractional part (with nanosecond resolution)
    fraction_holder_t m_fraction;

    // decimal digits to print (defaults to 6: print microseconds)
    static int s_print_precision;
};

// arithmetic operators
timeval_t operator +( const timeval_t &, const timeval_t & );
timeval_t operator -( const timeval_t &, const timeval_t & );

// comparison operators
bool operator ==( const timeval_t &, const timeval_t & );
bool operator !=( const timeval_t &, const timeval_t & );
bool operator  <( const timeval_t &, const timeval_t & );
bool operator  >( const timeval_t &, const timeval_t & );
bool operator <=( const timeval_t &, const timeval_t & );
bool operator >=( const timeval_t &, const timeval_t & );

/// Stream operator, used to print values in human readable form.
std::ostream &operator <<(std::ostream &out, const timeval_t &t);
std::istream &operator >>(std::istream &in, timeval_t &t);

/// Pretty printing
namespace detail {
    static int const hhmmss_index = std::ios_base::xalloc();
    static int const hhmmssuuuuuu_index = std::ios_base::xalloc();
}

template<timeval_t::FormatStyle style>
std::ostream& format(std::ostream& stream);

// for boost hashing
std::size_t hash_value(const timeval_t &a);

/// implementation ///

/// constructors

inline
timeval_t::timeval_t()
{
    // GCC elides the fraction constructor's memset()
    memset( this, 0, sizeof *this );
}

inline
timeval_t::timeval_t(int32_t sec, int32_t usec)
    : m_sec(sec)
{
    BOOST_STATIC_ASSERT_MSG ( sizeof(fraction_t) == sizeof(uint32_t), "size mismatch" );
    BOOST_STATIC_ASSERT_MSG ( sizeof(fraction_holder_t) == sizeof(uint32_t), "size mismatch" );
    set_usec( usec );
}

inline
timeval_t::timeval_t(const current_time &)
{
    set_current(); // sets m_sec, m_usec, m_nsec
}

inline
timeval_t::timeval_t(int32_t _sec)
    : m_sec(_sec)
{
}

inline
timeval_t::timeval_t(uint32_t _sec)
    : m_sec(_sec)
{
    BB_ASSERT( _sec <= static_cast<uint32_t>( std::numeric_limits<int32_t>::max() ) );
}

/// methods

inline
void timeval_t::set_sec(int32_t sec)
{
    m_sec = sec;
}

inline
void timeval_t::set_usec(int64_t usec)
{
    // The reason for this madness is because we never store negative
    // microseconds. This means that to store -1.25 seconds we would actually
    // store -2 seconds and 750000 microseconds (see comment for timeval_t class)
    // the main cases to be concerned about are
    ///    usec > 1000000
    ///    -1000000 < usec < 0
    ///    usec < -1000000

    static const int64_t kMillion = 1000000;

    // n.b. this code was hand-tuned by inspecting the assembly generated by g++ 4.4.5 x64
    // something strange: perftest_MulticastPitchSource is a bit faster without the unlikely(),
    // but I'm leaving it there because it really is unlikely, and it looks like an i-cache win

    // convert excess microseconds to seconds
    if( unlikely( usec >= kMillion || usec <= -kMillion ) )
    {
        // it's tempting to use lldiv(3) here to divide & mod at the same time, but GCC has an
        // intrinsic version of it on their wishlist only - currently it does a function call!
        m_sec += usec / kMillion;
        usec   = usec % kMillion;
    }

    // normalize negative microseconds
    if( usec < 0 )
    {
        m_sec -= 1;
        usec  += kMillion;
    }

    BB_ASSERT( usec >= 0 );
    BB_ASSERT( usec < kMillion );

    // assign usec in a tricky way so that the nsec & unused parts of m_fraction are zero (little-endian)
    m_fraction.n = static_cast<uint32_t>(usec);
}

inline
void timeval_t::set_usec_unsigned(uint32_t usec)
{
    BB_ASSERT( usec < 1000000 );

    m_fraction.n = usec;
}

inline
void timeval_t::set_nsec(int64_t nsec)
{
    static const int64_t kThousand = 1000;

    int64_t usec = 0;

    // convert excess nanoseconds to microseconds
    if( nsec >= kThousand || nsec <= -kThousand )
    {
        usec += nsec / kThousand;
        nsec  = nsec % kThousand;
    }

    // normalize negative nanoseconds
    if( nsec < 0 )
    {
        usec -= 1;
        nsec += kThousand;
    }

    BB_ASSERT( nsec >= 0 );
    BB_ASSERT( nsec < kThousand );

    set_usec( usec );
    m_fraction.f.nsec = nsec;
}

inline
void timeval_t::set_nsec_unsigned(uint32_t nsec)
{
    static const uint32_t kThousand = 1000;

    uint32_t usec = 0;

    // convert excess nanoseconds to microseconds
    if( nsec >= kThousand )
    {
        usec += nsec / kThousand;
        nsec  = nsec % kThousand;
    }

    BB_ASSERT( nsec < kThousand );

    set_usec_unsigned( usec );
    m_fraction.f.nsec = nsec;
}

inline
const timeval_t timeval_t::set_current(clockid_t clk)
{
    struct timespec timespec;
    const int ret = clock_gettime(clk, &timespec);
    (void)ret; // unusused variable
    BB_ASSERT(ret == 0);

    set_sec( timespec.tv_sec );

    // despite being a signed integer, timespec::tv_nsec is required to be in [0,999999999]
    // see http://pubs.opengroup.org/onlinepubs/009695399/functions/xsh_chap02_08.html
    set_nsec_unsigned( timespec.tv_nsec );

    return *this;
}

inline
bool timeval_t::between( const timeval_t& first, const timeval_t& last ) const
{
    BB_ASSERT(first <= last);
    return after(first) && before(last);
}

inline
bool timeval_t::on_or_between( const timeval_t& first, const timeval_t& last ) const
{
    BB_ASSERT(first <= last);
    return on_or_after(first) && on_or_before(last);
}

inline
bool timeval_t::outside( const timeval_t& first, const timeval_t& last ) const
{
    BB_ASSERT(first <= last);
    return !on_or_between(first, last);
}

inline
bool timeval_t::on_or_outside( const timeval_t& first, const timeval_t& last ) const
{
    BB_ASSERT(first <= last);
    return !between(first, last);
}

inline
bool timeval_t::before( const timeval_t& other ) const
{
    static const uint64_t NANO_PER_SEC = 1000000000;
    static const uint64_t NANO_PER_USEC =      1000;
    const int64_t my_nanos = m_sec * NANO_PER_SEC + m_fraction.f.usec * NANO_PER_USEC + m_fraction.f.nsec;
    const int64_t other_nanos = other.m_sec * NANO_PER_SEC + other.m_fraction.f.usec * NANO_PER_USEC + other.m_fraction.f.nsec;
    return my_nanos < other_nanos;
}

inline
bool timeval_t::on_or_before( const timeval_t& other ) const
{
    return *this <= other;
}

inline
bool timeval_t::after( const timeval_t& other ) const
{
    return *this > other;
}

inline
bool timeval_t::on_or_after( const timeval_t& other ) const
{
    return *this >= other;
}

/// operators

inline
timeval_t operator +( const timeval_t &t1, const timeval_t &t2)
{
    return timeval_t::from_sec_nsec_unrestricted( t1.sec() + t2.sec(), t1.nsec() + t2.nsec() );
}

inline
timeval_t operator -( const timeval_t &t1, const timeval_t &t2 )
{
    return timeval_t::from_sec_nsec_unrestricted( t1.sec() - t2.sec(), t1.nsec() - t2.nsec() );
}

inline
bool operator ==( const timeval_t &t1, const timeval_t &t2 )
{
    // compare all 8 bytes at once (not using memcmp because it generates a loop to decide which is "less")
    return reinterpret_cast<const int64_t&>( t1 ) == reinterpret_cast<const int64_t&>( t2 );
}

inline
bool operator <( const timeval_t &t1, const timeval_t &t2 )
{
    return t1.before(t2);
}

inline
bool operator !=( const timeval_t &t1, const timeval_t &t2 )
{
    return !( t1 == t2 );
}

inline
bool operator >( const timeval_t &t1, const timeval_t &t2 )
{
    return t2 < t1;
}

inline
bool operator <=( const timeval_t &t1, const timeval_t &t2 )
{
    return !( t1 > t2 );
}

inline
bool operator >=( const timeval_t &t1, const timeval_t &t2 )
{
    return !( t1 < t2 );
}

inline
int64_t timeval_t::to_millisec() const
{
    return sec() * 1000L + usec() / 1000;
}

inline
int64_t timeval_t::to_microsec() const
{
    return sec() * 1000000L + usec();
}

inline
int64_t timeval_t::to_nanosec() const
{
    return sec() * 1000000000L + nsec();
}

/// builders

/// Note this function skips the bound checks so the user must ensure
/// that msec is < 1000
inline
timeval_t timeval_t::from_sec_msec( int32_t sec, uint32_t msec )
{
    timeval_t ret( sec );
    ret.set_usec_unsigned( msec * 1000 );
    return ret;
}

inline
timeval_t timeval_t::from_sec_nsec( int32_t sec, uint32_t nsec )
{
    timeval_t ret( sec );
    ret.set_nsec_unsigned( nsec );
    return ret;
}

inline
timeval_t timeval_t::from_sec_nsec_unrestricted( int32_t sec, int64_t nsec )
{
    timeval_t ret( sec );
    ret.set_nsec( nsec );
    return ret;
}

inline
timeval_t timeval_t::from_millisec( const int64_t millis )
{
    const int32_t  sec = millis / 1000;
    const int32_t msec = millis % 1000;
    return timeval_t( sec, msec * 1000 );
}

inline
timeval_t timeval_t::from_microsec( const int64_t micros )
{
    static const uint64_t MICROS_PER_SECOND = 1000000;
    const int32_t  sec = micros / MICROS_PER_SECOND;
    const int32_t usec = micros % MICROS_PER_SECOND;
    return timeval_t( sec, usec );
}
/// converters

inline
double timeval_t::toDouble() const
{
    return static_cast<double>(sec()) + static_cast<double>(nsec())*1.0e-9;
}

inline
timespec timeval_t::to_timespec() const
{
    timespec ts = { sec(), nsec() };
    return ts;
}

inline
timeval_t timeval_t::from_timespec( const timespec& ts )
{
    return timeval_t::from_sec_nsec( ts.tv_sec, ts.tv_nsec );
}

inline
timeval timeval_t::to_timeval() const
{
    timeval tv = { sec(), usec() };
    return tv;
}

inline
timeval_t timeval_t::from_timeval( const timeval& tv )
{
    return timeval_t( tv.tv_sec, tv.tv_usec );
}

inline
timeval_t timeval_t::from_hms( uint32_t hour, uint32_t min, uint32_t sec )
{
    return from_hmsu ( hour, min, sec, 0 );
}

inline
timeval_t timeval_t::from_hmsu( const uint32_t hour, const uint32_t min, const uint32_t sec, const uint32_t usec )
{
    return timeval_t( hour*3600 + min * 60 + sec, usec );
}

// current_time methods

inline
bool operator ==( const timeval_t::current_time& t1, const timeval_t &t2 )
{
    return timeval_t( t1 ) == t2;
}

inline
bool operator !=( const timeval_t::current_time& t1, const timeval_t &t2 )
{
    return timeval_t( t1 ) != t2;
}

inline
bool operator <( const timeval_t::current_time& t1, const timeval_t &t2 )
{
    return timeval_t( t1 ) < t2;
}

inline
bool operator >( const timeval_t::current_time& t1, const timeval_t &t2 )
{
    return timeval_t( t1 ) > t2;
}

inline
bool operator <=( const timeval_t::current_time& t1, const timeval_t &t2 )
{
    return timeval_t( t1 ) <= t2;
}

inline
bool operator >=( const timeval_t::current_time& t1, const timeval_t &t2 )
{
    return timeval_t( t1 ) >= t2;
}

} // namespace bb

#endif // BB_CORE_TIMEVAL_H

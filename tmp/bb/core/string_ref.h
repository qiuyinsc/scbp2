#ifndef BB_CORE_STRING_REF_H
#define BB_CORE_STRING_REF_H

/* Contents Copyright 2015 Athena Capital Research LLC. All Rights Reserved. */

#include <boost/functional/hash/hash.hpp>
#include <bb/core/bbassert.h>

#include <string>
#include <cstring>

// A non-owning const reference to a string (see LLVM's StringRef)

namespace bb {

class string_ref
{
public:
    typedef const char *iterator;
    typedef const char *const_iterator;
    typedef size_t size_type;
    static const size_t npos = ~size_t(0);

public:
    string_ref()
        : m_data(0), m_len(0)
    {}

    string_ref(const char *s)
        : m_data(s)
    {
        BB_ASSERT(s && "string_ref cannot be built from a null string");
        m_len = ::strlen(s);
    }

    string_ref(const char *s, size_t len)
        : m_data(s), m_len(len)
    {
        BB_ASSERT((s || len == 0) &&
                "string_ref cannot be build from a null string with non-zero "
                "length");
    }

    string_ref(const std::string &s)
        : m_data(s.data()), m_len(s.size())
    {}

    iterator begin() const
    {
        return m_data;
    }

    iterator end() const
    {
        return m_data + m_len;
    }

    size_t size() const
    {
        return m_len;
    }

    size_t length() const
    {
        return m_len;
    }

    char operator[](size_t idx) const
    {
        BB_ASSERT(idx < m_len && "invalid subscript index");
        return m_data[idx];
    }

    string_ref substr(size_t start, size_t n = npos) const
    {
        start = std::min(start, m_len);
        return string_ref(m_data + start, std::min(n, m_len - start));
    }

    int compare(string_ref rhs) const
    {
        size_t min_len = std::min(m_len, rhs.m_len);
        int r;
        if (min_len == 0)
        {
            r = 0;
        }
        else
        {
            r = ::memcmp(m_data, rhs.m_data, min_len);
        }
        if (r)
        {
            return r < 0 ? -1 : 1;
        }
        else
        {
            if (m_len == rhs.m_len)
            {
                return 0;
            }
            else
            {
                return m_len < rhs.m_len ? -1 : 1;
            }
        }
    }

    bool equals(string_ref rhs) const
    {
        return compare(rhs) == 0;
    }

    size_type find(string_ref rhs, size_t from) const;
    size_type rfind(string_ref rhs) const;

private:
    const char *m_data;
    size_t m_len;
};

inline bool operator==(string_ref lhs, string_ref rhs)
{
    return lhs.equals(rhs);
}

inline bool operator!=(string_ref lhs, string_ref rhs)
{
    return !(lhs.equals(rhs));
}

inline std::size_t hash_value(const string_ref &s)
{
    std::size_t r = 0;
    boost::hash_range(r, s.begin(), s.end());
    return r;
}

}

#endif // BB_CORE_STRING_REF_H

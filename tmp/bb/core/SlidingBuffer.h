#ifndef BB_CORE_SLIDINGBUFFER_H
#define BB_CORE_SLIDINGBUFFER_H

/* Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved. */


#include <cstring>
#include <iterator>

#include <boost/scoped_array.hpp>
#include <boost/noncopyable.hpp>

#include <bb/core/bbassert.h>
#include <bb/core/compat.h>

namespace bb {

class SlidingBuffer : public boost::noncopyable
{
public:
    // Ensure that room exists for at least 'needed' bytes in the buffer.
    // Non-zero capacity is equivalent to passing zero then calling reserve().
    SlidingBuffer( size_t capacity = 0 )
        : m_region( capacity ? new char[capacity] : NULL )
        , m_capacity( capacity )
        , m_data( m_region.get() )
        , m_size( 0 )
    {}

    ~SlidingBuffer() {}

    // Pointer to beginning of valid data in the buffer.
    char* data() const
    {
        return m_data;
    }

    // Beginning of valid data
    char* begin() const
    {
        return data();
    }

    // End of valid data
    char* end() const
    {
        return begin() + size();
    }

    // Number of bytes of valid data in the buffer.
    size_t size() const
    {
        return m_size;
    }

    // Is the buffer empty?
    bool empty() const
    {
        return size() == 0;
    }

    // Total capacity of the current buffer. Note that this is not the
    // same as 'available'.
    size_t capacity() const
    {
        return m_capacity;
    }

    // The number of bytes that can be efficiently appended (requiring
    // only a memcpy, and no memory allocations or memmove
    // operations).
    size_t available() const
    {
        return std::distance( end(), m_region.get() + capacity() );
    }

    // Ensure that room exists for at least 'needed' bytes in the
    // buffer. You don't need to call this before calling 'append',
    // since append will call reserve for you. However, you might want
    // to call 'reserve' to set the initial size of the
    // buffer. Calling 'reserve' may invalidate existing iterators.
    void reserve( size_t needed )
    {
        if( unlikely( needed > available() ) )
            grow( needed );
    }

    // Append 'size' bytes starting at 'data' to the buffer. The
    // contents of the buffer may be shifted, reallocated,
    // etc. Existing pointers derived from earlier calls to 'data' may
    // be invalidated. The data passed here must not alias data already
    // in the buffer.
    void append( const void* const data, const size_t size )
    {
        reserve( size );
        add( data, size );
    }

    // Remove the first 'count' bytes of data from the buffer. It is
    // safe to pop more data than is in the buffer, this is equivalent
    // to calling 'clear'.
    void pop_front( const size_t count )
    {
        if( count >= size() )
        {
            m_data = m_region.get();
            m_size = 0;
        }
        else
        {
            m_data += count;
            m_size -= count;
        }
    }

    void clear()
    {
        pop_front( size() );
    }

private:

    // Add the data to the end of the current data. You must call
    // 'reserve(size)' before calling this function.
    void add( const void* const data, const size_t size )
    {
        // Ensure that [data, data + size) does not overlap with
        // [m_region.get(), m_region.get() + capacity()).
        BB_ASSERT(
            ( data >= ( m_region.get() + capacity() ) ) ||
            ( ( reinterpret_cast<const char *>(data) + size ) <= m_region.get() ) );

        ::memcpy( end(), data, size );
        m_size += size;
    }

    void grow( size_t needed );

    // These are constant except for a reallocation
    boost::scoped_array<char> m_region;
    size_t m_capacity;

    // These vary as the buffer grows and shrinks.
    char* m_data;
    size_t m_size;
};

} // namespace bb

#endif // BB_CORE_SLIDINGBUFFER_H

#ifndef BB_CORE_FIXEDSIZEMAP_H
#define BB_CORE_FIXEDSIZEMAP_H

/* Contents Copyright 2016 Athena Capital Research LLC. All Rights Reserved. */


#include <algorithm>
#include <functional>

#include <boost/array.hpp>
#include <boost/range/algorithm/copy_backward.hpp>

namespace bb {

template<typename KeyT, typename ValueT, size_t SZ, class Compare = std::less<KeyT> >
struct FixedSizeMap
{
    typedef KeyT key_type;
    typedef typename std::pair< KeyT, ValueT > value_type;
    typedef typename boost::array< value_type, SZ > ArrayT;
    typedef typename ArrayT::iterator iterator;
    typedef typename ArrayT::const_iterator const_iterator;

    FixedSizeMap()
        : m_numItems ( 0 )
    {}

    iterator begin()
    {
        return m_array.begin();
    }

    iterator end()
    {
        return m_array.end();
    }

    const_iterator begin() const
    {
        return m_array.begin();
    }

    const_iterator end() const
    {
        return m_array.end();
    }

    size_t erase ( const key_type& key )
    {
        const_iterator iter ( find ( key ) );
        if ( iter != end() )
        {
            erase ( iter );
            return 1;
        }
        else
        {
            return 0;
        }
    }

    iterator erase ( const_iterator iter )
    {
        if ( iter == end() || empty() )
        {
            return end();
        }
        const_iterator b ( begin() );
        const size_t index ( std::distance( b, iter ) );
        std::copy ( m_array.begin() + index,
                             m_array.begin() + index + 1,
                             m_array.begin() + m_numItems);
        m_numItems--;
        return m_array.begin() + index;
    }

    std::pair<iterator, bool> insert ( value_type const & pair )
    {
        iterator iter = std::lower_bound (
                            m_array.begin(),
                            m_array.begin() + m_numItems,
                            pair,
                            comparer() );

        if ( m_numItems != 0 &&
                iter != end() &&
                iter->first == pair.first )
        {
            return std::make_pair ( iter, false );
        }
        else if ( m_numItems == SZ )
        {
            return std::make_pair ( end(), false );
        }
        else
        {
            const size_t index ( std::distance( m_array.begin(), iter ) );
            std::copy_backward ( m_array.begin() + index,
                                 m_array.begin() + m_numItems,
                                 m_array.begin() + m_numItems + 1);
            m_array[index] = pair;
            m_numItems++;
            return std::make_pair ( iter, true );
        }
    }

    iterator find ( KeyT const & key )
    {
        for ( size_t i = 0 ; i < m_numItems; i++ )
        {
            if ( m_array[i].first == key )
            {
                return m_array.begin() + i;
            }
        }
        return m_array.end();
    }

    const_iterator find ( KeyT const & key ) const
    {
        for ( size_t i = 0 ; i < m_numItems; i++ )
        {
            if ( m_array[i].first == key )
            {
                return m_array.begin() + i;
            }
        }
        return m_array.end();
    }

    size_t size() const
    {
        return m_numItems;
    }

    bool empty() const
    {
        return m_numItems == 0;
    }

    bool full() const
    {
        return m_numItems == SZ;
    }

private:
    size_t m_numItems;
    ArrayT m_array;

    struct comparer : public std::binary_function < value_type, value_type, bool >
    {
        bool operator() ( value_type const & one, value_type const & two) const
        {
            return Compare()(one.first, two.first);
        }
    };
};

} // namespace bb

#endif // BB_CORE_FIXEDSIZEMAP_H

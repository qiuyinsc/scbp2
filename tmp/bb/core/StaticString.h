#ifndef BB_CORE_STATICSTRING_H
#define BB_CORE_STATICSTRING_H

/* Contents Copyright 2016 Athena Capital Research LLC. All Rights Reserved. */

#include <bb/core/Error.h>
#include <bb/core/Log.h>

namespace bb {

///
/// Simple class to help us manipulate a char buffer
/// of a fixed size
template<uint32_t Size>
struct StaticString
{
    typedef StaticString<Size>  ThisTypeT;
public:
    static const uint32_t static_size = Size;
    StaticString()
        : m_next( m_buff )
    {
        init();
    }

    StaticString( const ThisTypeT& other)
    {
        copy_impl( other );
    }

    template<uint32_t OtherSize>
    StaticString( const StaticString<OtherSize>& other)
    {
        copy_impl( other );
    }

    StaticString( const std::string& other)
    {
        copy_impl( other );
    }

    template<uint32_t OtherSize>
    ThisTypeT& operator=( const StaticString<OtherSize>& other)
    {
        copy_impl(other);
        return *this;
    }

    ThisTypeT& operator=( const std::string& other)
    {
        copy_impl(other);
        return *this;
    }

    ThisTypeT& operator=( const ThisTypeT& other)
    {
        copy_impl(other);
        return *this;
    }

    template<typename OtherT>
    bool operator== ( const OtherT& other) const
    {
        return ( length() == other.length() ) &&
            ( 0 ==memcmp(m_buff, other.data(), length() ) );
    }

    operator std::string() const
    {
        return std::string( m_buff, length() );
    }

    void init()
    {
        m_next = m_buff;
    }

    uint32_t length() const
    {
        return m_next - m_buff;
    }

    const char * data() const
    {
        return m_buff;
    }

    char * data()
    {
        return m_buff;
    }

    void advance( uint32_t sz )
    {
        BB_THROW_EXASSERT_SSX( length() + sz <= Size,
                               "Advance would exceed the size of the string"  );
        m_next += sz;
    }

    bool empty() const
    {
        return m_next == m_buff;
    }

    char* next()
    {
        return m_next;
    }

    char * next( const char* newNext)
    {
        BB_THROW_EXASSERT_SSX( (newNext - m_buff) <= Size,
                               "Next would exceed the size of the string"  );
        m_next = newNext;
        return m_next;
    }

    char * next( char* newNext )
    {
        BB_THROW_EXASSERT_SSX( (newNext - m_buff) <= Size,
                               "Next would exceed the size of the string"  );
        m_next = newNext;
        return m_next;
    }

    template<uint32_t OtherSize>
    StaticString<Size>& operator<<(const StaticString<OtherSize>& other)
    {
        advance( other.length() );
        memcpy( m_next - other.length(), other.data(), other.length() );
        return *this;
    }

private:
    template<typename OtherT>
    void copy_impl( const OtherT& other)
    {
        BB_THROW_EXASSERT_SSX( other.length() <= Size,
                               "String provided is too long. Max size:" << Size
                               << ", string size:"<<other.length()  );
        memcpy(m_buff, other.data(), other.length() );
        m_next = m_buff + other.length();
    }

    char        m_buff[Size];
    char*       m_next;
};

} // end namespace bb

#endif // BB_CORE_STATICSTRING_H

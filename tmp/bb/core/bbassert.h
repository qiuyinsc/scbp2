#ifndef BB_CORE_BBASSERT_H
#define BB_CORE_BBASSERT_H

/* Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved. */


// When we issue NDEBUG, we don't want asserts
#ifdef NDEBUG
#define BOOST_DISABLE_ASSERTS
#endif // NDEBUG


#define BOOST_ENABLE_ASSERT_HANDLER        // we are going to override default behavior
#include <boost/assert.hpp>
#include <boost/static_assert.hpp>

// We use BB_ASSERT, not BOOST_ASSERT
#define BB_ASSERT( x )    (BOOST_ASSERT( x ))



#endif // BB_CORE_BBASSERT_H

#ifndef BB_CORE_MSGPRINTFILTER_H
#define BB_CORE_MSGPRINTFILTER_H

/* Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved. */


#include <vector>
#include <boost/variant.hpp>
#include <bb/core/mtype.h>

/// See clientcore/MsgPrinter for info on how this works.
///
/// The only reason this lives here is that it is used from core_config.lua.
/// Otherwise, it belongs in clientcore.
///
/// To use these types, #include <boost/variant.hpp>.

namespace bb
{
    namespace msg_print_filters
    {
        struct NoMessages {};
        struct AllMessages {};
        struct MTypes
        {
            MTypes(const std::vector<mtype_t> &mtypes) : m_mtypes(mtypes) {}
            std::vector<mtype_t> m_mtypes;
        };
    }

    typedef boost::variant<
        msg_print_filters::NoMessages,
        msg_print_filters::AllMessages,
        msg_print_filters::MTypes> MsgPrintFilter;
}

#endif // BB_CORE_MSGPRINTFILTER_H

#ifndef BB_CORE_BUFFER_H
#define BB_CORE_BUFFER_H

/* Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved. */

#include <algorithm>
#include <boost/noncopyable.hpp>

namespace bb {

    class Buffer : public boost::noncopyable {
    public:
        Buffer(const size_t size);
        virtual ~Buffer();

        /// Copies AMT bytes from BUF into internal buffer.
        bool put(const void *buf, size_t amt);

        /// Copies AMT bytes from internal buffer into BUF.  If AMT is
        /// greater than the number of bytes availble, it doesn't copy
        /// anything at all and returns false.
        bool get(void *buf, size_t amt);

        /// Same as get, but does not advance the buffer at all.
        bool peek(void *buf, size_t amt);

        /// Returns number of bytes in buffer.
        size_t size() const { return len; }

        /// Returns number of bytes free in buffer.
        size_t left() const { return bufsize-len; }

        /// Returns buffer size.
        size_t maxsize() const { return bufsize; }

        /// Clears out the buffer.
        void clear();

        /// Returns true if buffer is empty.
        bool empty() const { return len == 0; }

        /// Returns true if buffer is full.
        bool full() const { return len == bufsize; }

        /// Returns a pointer to the buffer.  Use at your own risk.
        char *expose() { return buffer; }

        /// Returns a pointer to the front of the buffer.  This is the
        /// location that contains the data returned on the next call
        /// to get.
        char *exposeFront() { return buffer+front; }

        /// Returns the first free character.
        char *exposeEnd() { return buffer+( (front + len) % bufsize ); }

        /// Returns the number of filled bytes that lay contiguously
        /// after exposeFront().
        size_t frontSize() const { return std::min(bufsize-front, size()); }

        /// Returns the number of empty bytes that lay contiguously
        /// after exposeEnd().
        size_t endSize() const { return std::min(bufsize - ((front+len)%bufsize), bufsize-size()); }

        /// Sets the size of the buffer.
        void setSize(size_t s) { len = s; }

        /// Adds to the size of the buffer.
        void addSize(size_t s) { len += s; }

        /// Pops off AMT bytes from the buffer. Must not modify the buffer memory
        /// (people might be zero-copy reading the data).
        void pop(size_t amt)
        {
            if (amt >= len)
                clear();
            else {
                front = (front+amt) % bufsize;
                len -= amt;
            }
        }

        /// Rotate the data inside the buffer such that expose() == exposeFront.
        void rotate();

        /// Shifts the data inside the buffer such that expose() == exposeFront()
        /// Only moves size() data.
        void shift();

    protected:
        Buffer(const size_t size, char * contents);

    private:
        size_t bufsize;
        bool shared_buffer;
        char *buffer;

        size_t front;
        size_t len;
    };

    template<size_t SZ>
    class StaticBuffer : public Buffer
    {
        public:
            StaticBuffer() : Buffer( SZ, contents ) {}
            virtual ~StaticBuffer() {}
        private:
            char contents[SZ];
    };
}

#endif // BB_CORE_BUFFER_H

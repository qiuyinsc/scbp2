#ifndef BB_CORE_SHARED_VECTOR_H
#define BB_CORE_SHARED_VECTOR_H

/* Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved. */


#include <algorithm>
#include <boost/noncopyable.hpp>
#include <bb/core/bbassert.h>
#include <bb/core/Error.h>
#include <bb/core/Subscription.h>
#include <boost/intrusive/list.hpp>
namespace bb
{

template <typename T> class shared_vector;
template <typename T> struct shared_vector_safe_iterator;
template <typename T> struct shared_vector_safe_jterator;

/// shared_vector is a generic helper for managing a list using the RAII subscription pattern.
/// You should think of this as a list of T's that has an insert() call
/// but no no remove(). When you call insert(), you get a RAII token class (Subscription).
/// When all references to that Subscription are released, the corresponding element will
/// be destroyed and automatically removed from the list.
///
/// Iteration over shared_vector like so:
/// my_shared_list::safe_jterator j(mylist);
/// while(j.hasNext()
///   dostuff(j.next());
/// protects against calls to insert/remove. However, there can only be one
/// iteration at a time over the list. shared_vector is not thread-safe.
template <typename T>
class shared_vector : public boost::noncopyable
{
private:
    struct SubImpl;
    typedef std::vector<T> base_type;
    typedef std::vector<SubImpl*> sub_vec;
public:
    typedef Subscription token_ptr;
    typedef T value_type;
    typedef T &reference;
    typedef const T &const_reference;

    typedef typename base_type::iterator unsafe_iterator;
    typedef typename base_type::const_iterator const_unsafe_iterator;

    struct shared_vector_safe_iterator_tag;
    typedef  boost::intrusive::list_base_hook< boost::intrusive::link_mode<boost::intrusive::auto_unlink> ,
                        boost::intrusive::tag<shared_vector_safe_iterator_tag> > shared_vector_safe_iterator_hook;
    typedef shared_vector_safe_iterator<T> iterator;

    typedef boost::intrusive::list<iterator, boost::intrusive::constant_time_size<false>
                    ,boost::intrusive::base_hook<shared_vector_safe_iterator_hook> > iter_list;

    struct shared_vector_safe_jterator_tag;
    typedef  boost::intrusive::list_base_hook< boost::intrusive::link_mode<boost::intrusive::auto_unlink> ,
                        boost::intrusive::tag<shared_vector_safe_jterator_tag> > shared_vector_safe_jterator_hook;
    typedef shared_vector_safe_jterator<T> safe_jterator;

    typedef boost::intrusive::list<safe_jterator, boost::intrusive::constant_time_size<false>
                    ,boost::intrusive::base_hook<shared_vector_safe_jterator_hook> > jter_list;

    shared_vector();
    ~shared_vector();

    /// Inserts an item into the list. outSub will be initialized with a shared_token_ptr.
    /// When all references to that ptr are released, the element will be removed from the list.
    unsafe_iterator insert( Subscription &outSub, unsafe_iterator insertPos, const T &elem );

    void push_front(Subscription &outSub, const T &t) { insert(outSub, unsafe_begin(), t); }
    void push_back(Subscription &outSub, const T &t) { insert(outSub, unsafe_end(), t); }

    size_t size() const { return m_elements.size(); }
    bool empty() const { return m_elements.empty(); }

    // Iteration over the shared list.
    // "safe" because iterators are not invalidated by insertion/deletion.
    // Like safe_jterator but idiomatic C++ (jterator came first).
    iterator begin() { return iterator(*this); }
    iterator end() { return iterator(*this, true/*end*/); }

    // Iteration over the shared list.
    // "unsafe" because any iterators are invalidated by insertion/deletion.
    // See iterator and safe_jterator for alternatives.
    unsafe_iterator unsafe_begin() { return m_elements.begin(); }
    const_unsafe_iterator unsafe_begin() const { return m_elements.begin(); }
    unsafe_iterator unsafe_end() { return m_elements.end(); }
    const_unsafe_iterator unsafe_end() const { return m_elements.end(); }

    // insert/erase variants that do not use refcounting -- an explicit_insert
    // must be paired with an explicit_erase.
    unsafe_iterator explicit_insert( const unsafe_iterator &insertPos, const T &elem );
    void explicit_erase( const unsafe_iterator &insertPos );

private:
    template<typename> friend struct shared_vector_safe_iterator;
    template<typename> friend struct shared_vector_safe_jterator;

    struct SubStorage
    {
        SubStorage(shared_vector *parent) : m_parent(parent), m_refCount(0) {}

        sub_vec m_subscriptions;
        shared_vector *m_parent;
        int m_refCount;

        void insert( Subscription &outSub, const unsafe_iterator &insertPos, const T &elem );
        void remove( SubImpl *subImpl );

        friend inline void intrusive_ptr_add_ref(SubStorage *e) { ++e->m_refCount; }
        friend inline void intrusive_ptr_release(SubStorage *e) { if(--e->m_refCount == 0) delete e; }
    };
    typedef boost::intrusive_ptr<SubStorage> SubStoragePtr;

    struct SubImpl : public SubscriptionBase
    {
        SubImpl(const SubStoragePtr &parent) : m_parent(parent), m_orphanCount( 0 ) {}
        ~SubImpl() { m_parent->remove(this); }


        bool isOrphan() const { return m_orphanCount >= m_refCount; }
        virtual bool isValid() const { return m_parent->m_parent; }
        SubStoragePtr m_parent;
        int m_orphanCount;
    };

    // adjust an iterator so it's consistent w.r.t. a modification
    enum ModType_t { INSERT, INSERT_END, REMOVE };
    template<ModType_t type>
    static inline ptrdiff_t preadjust_iter(const unsafe_iterator &iter, const unsafe_iterator &begin,
            int modPosition)
    {
        ptrdiff_t iterN = iter - begin;
        switch(type)
        {
        case INSERT: if(iterN > modPosition) iterN++; break;
        // use >= when we're adding -- this means that the end iterator will
        // get adjusted forward when something does a push_back
        case INSERT_END: if(iterN >= modPosition) iterN++; break;
        case REMOVE: if(iterN > modPosition) iterN--; break;
        }
        return iterN;
    }
    static inline unsafe_iterator postadjust_iter(ptrdiff_t saved, const unsafe_iterator &begin)
        { return begin + saved; }

    base_type m_elements;
    iter_list m_iterlist;
    jter_list m_jterlist;
    unsafe_iterator *m_curInsertLoc;
    SubStoragePtr m_subStorage;
};

// A C++-style iterator over a shared_vector. unlike shared_vector::unsafe_iterator,
// the list can safely be mutated while an iterator is iterating over the list.
template<typename T>
struct shared_vector_safe_iterator
    : shared_vector<T>::shared_vector_safe_iterator_hook
{
private:
    // constructible only by shared_vector<T>
    explicit shared_vector_safe_iterator(shared_vector<T> &container, bool end = false)
        : m_next(end ? container.size() : 0)
        , m_list(&container)
    {
        m_list->m_iterlist.push_back(*this);
    }

public:
    shared_vector_safe_iterator(const shared_vector_safe_iterator& from) // copy constructor
        : m_next(from.m_next)
        , m_list(from.m_list)
    {
        m_list->m_iterlist.push_back(*this);
    }

    shared_vector_safe_iterator &operator=(const shared_vector_safe_iterator& from) // assignment operator
    {
        m_next = from.m_next;

        if(m_list != from.m_list) // move our intrusive list hook from the old container to the new
        {
            shared_vector<T>::shared_vector_safe_iterator_hook::unlink();
            from.m_list->m_iterlist.push_back(*this);
            m_list = from.m_list;
        }

        return *this;
    }

    shared_vector_safe_iterator &operator++() // pre-increment
    {
        ++m_next;
        return *this;
    }

    shared_vector_safe_iterator operator++(int) // post-increment
    {
        shared_vector_safe_iterator prev = *this;
        ++*this;
        return prev;
    }

    T &operator*() const
    {
        BB_ASSERT(*this != m_list->end());
        return m_list->m_elements[m_next];
    }

    T *operator->() const
    {
        return &operator*();
    }

    const shared_vector<T>& rawContainer() const { return *m_list; }
    typename std::vector<T>::size_type rawPosition() const { return m_next; }

private:
    friend class shared_vector<T>;
    typename std::vector<T>::size_type m_next;
    shared_vector<T> *m_list;
};

template<typename T>
bool operator ==(const shared_vector_safe_iterator<T>& lhs, const shared_vector_safe_iterator<T>& rhs)
{
    BB_ASSERT(&lhs.rawContainer() == &rhs.rawContainer());
    return lhs.rawPosition() == rhs.rawPosition();
}

template<typename T>
bool operator !=(const shared_vector_safe_iterator<T>& lhs, const shared_vector_safe_iterator<T>& rhs)
{
    return !(lhs == rhs);
}

// A Java-style iterator over a shared_vector. unlike shared_vector::unsafe_iterator,
// the list can safely be mutated while a jterator is iterating over the list.
template<typename T>
struct shared_vector_safe_jterator
: shared_vector<T>::shared_vector_safe_jterator_hook, boost::noncopyable
{
    explicit shared_vector_safe_jterator(shared_vector<T> &b)
        : m_next()
        , m_list(b)
    {
        b.m_jterlist.push_back(*this);
    }
    bool hasNext() const { return m_next < m_list.m_elements.size(); }
    T &cur() {
        BB_ASSERT(hasNext());
        return m_list.m_elements[m_next];
    }
    T &next()
    {
        BB_THROW_EXASSERT(hasNext(), "shared_vector_safe_jterator: no next element");
        return m_list.m_elements[m_next++];
    }
    shared_vector_safe_jterator* clone() const
    {
        shared_vector_safe_jterator* cloned = new shared_vector_safe_jterator(m_list);
        cloned->m_next = m_next;
        return cloned;
    }
private:

    friend class shared_vector<T>;
    typename std::vector<T>::size_type m_next;
    shared_vector<T>  &m_list;
};
// shared_vector impl

template<typename T>
shared_vector<T>::shared_vector() 
    : m_curInsertLoc(NULL)
    , m_subStorage(new SubStorage(this)) { }

template<typename T>
shared_vector<T>::~shared_vector()
{
    m_subStorage->m_parent = NULL;

    sub_vec poorOrphans( m_subStorage->m_subscriptions );
    for( typename sub_vec::iterator iSub( poorOrphans.begin() ), iEnd( poorOrphans.end() ); iEnd != iSub; ++iSub )
    {
        if( (*iSub)->isOrphan() )
        {
            delete *iSub;
        }
    }

}

template<typename T>
inline typename shared_vector<T>::unsafe_iterator
shared_vector<T>::insert( Subscription &outSub, unsafe_iterator insertPos, const T &elem )
{
    // protect insertPos from any modification that outSub.reset() might do
    BB_THROW_EXASSERT(m_curInsertLoc == NULL, "shared_vector<T>::insert: reentrant insertion");
    m_curInsertLoc = &insertPos;
    outSub.reset(); // cannot throw
    m_curInsertLoc = NULL;

    unsafe_iterator preadjust_begin = m_elements.begin();
    ptrdiff_t insertDistance = insertPos - preadjust_begin;
    BB_ASSERT(!(insertDistance<0));
    for(typename iter_list::iterator i=m_iterlist.begin(),e=m_iterlist.end();i!=e;++i){
        if( (i->m_next>static_cast<std::size_t>(insertDistance )) ){
            i->m_next++;
        }
    }
    for(typename jter_list::iterator i=m_jterlist.begin(),e=m_jterlist.end();i!=e;++i){
        if( (i->m_next>static_cast<std::size_t>(insertDistance )) ){
            i->m_next++;
        }
    }
    SubImpl *impl(new SubImpl(m_subStorage));
    unsafe_iterator loc = m_elements.insert(insertPos, elem);
    m_subStorage->m_subscriptions.insert(m_subStorage->m_subscriptions.begin()+insertDistance, impl);
    outSub.reset(impl);
    return loc;
}

template<typename T>
inline void shared_vector<T>::SubStorage::remove(SubImpl *subImpl)
{
    // called from destructor, should not throw
    typename sub_vec::iterator subRemovePos = std::find(
            m_subscriptions.begin(), 
            m_subscriptions.end(), 
            subImpl);
    BB_ASSERT(subRemovePos != m_subscriptions.end());
    if(subRemovePos == m_subscriptions.end())
        return; // not found

    ptrdiff_t removeDistance = subRemovePos - m_subscriptions.begin();
    BB_ASSERT(!(removeDistance<0));
    ptrdiff_t bakInsertLoc = 0;
    if(m_parent && m_parent->m_curInsertLoc)
    {
        unsafe_iterator preadjust_begin = m_parent->m_elements.begin();
        bakInsertLoc = preadjust_iter<REMOVE>(*m_parent->m_curInsertLoc, preadjust_begin, removeDistance);
    }

    m_subscriptions.erase(subRemovePos);
    if(m_parent)
    {
        BB_ASSERT(ptrdiff_t(m_parent->m_elements.size()) >= removeDistance);
        m_parent->m_elements.erase(m_parent->m_elements.begin()+removeDistance);
        for(typename iter_list::iterator i=m_parent->m_iterlist.begin(),e=m_parent->m_iterlist.end();i!=e;++i){
            if( (i->m_next>static_cast<std::size_t>(removeDistance )) ){
                i->m_next--;
            }
        }
        for(typename jter_list::iterator i=m_parent->m_jterlist.begin(),e=m_parent->m_jterlist.end();i!=e;++i){
            if( (i->m_next>static_cast<std::size_t>(removeDistance )) ){
                i->m_next--;
            }
        }
        if(m_parent->m_curInsertLoc){
            unsafe_iterator postadjust_begin = m_parent->m_elements.begin();
            *m_parent->m_curInsertLoc = postadjust_iter(bakInsertLoc, postadjust_begin);
        }
    }
}

template<typename T>
inline typename shared_vector<T>::unsafe_iterator
shared_vector<T>::explicit_insert( const unsafe_iterator &insertPos, const T &elem )
{
    Subscription sub;
    unsafe_iterator i = insert(sub, insertPos, elem);
    intrusive_ptr_add_ref(sub.getBase());
    static_cast<SubImpl*>( sub.getBase() )->m_orphanCount++;
    return i;
}

template<typename T>
inline void shared_vector<T>::explicit_erase( const unsafe_iterator &insertPos )
{
    SubImpl *sub = m_subStorage->m_subscriptions[insertPos - m_elements.begin()];
    intrusive_ptr_release(static_cast<SubscriptionBase*>(sub));
}

} // namespace bb

#endif // BB_CORE_SHARED_VECTOR_H

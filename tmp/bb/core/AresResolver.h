#ifndef BB_CORE_ARESRESOLVER_H
#define BB_CORE_ARESRESOLVER_H

/* Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved. */


#include <ares.h>

#ifndef NDEBUG
#include <boost/thread/thread.hpp>
#endif

#include <csignal>
#include <map>

#include <boost/exception_ptr.hpp>

#include <bb/core/FDSetFwd.h>
#include <bb/core/IDNSResolver.h>
#include <bb/core/Timer.h>

namespace bb {

// An AresResolver uses an FDSet to drive the c-ares asynchronous DNS
// resolver library and implement the IDNSResolver interface. After
// creating an AresResolver, you must only interact with it from the
// context of the thread driving the FDSet.

class AresResolver : public IDNSResolver
{
public:
    typedef std::vector<ipv4_addr_t> ipv4_addr_vec_t;

    // Construct a new AresResolver. The provided FDSet will be used
    // for IO readiness notification. The lifetime of the resolver
    // must be a subset of the lifetime of the FDSet.
    AresResolver( FDSet& fdSet );

    // This overrides the set of nameservers to use the specified
    // servers, ignoring any local resolv.conf or named. This is
    // primarily useful for testing.
    AresResolver( FDSet& fdSet,
                  const ipv4_addr_vec_t& nameServers );

    virtual ~AresResolver();

public:
    // We implement the IDNSResolver interface; these are
    // probably the methods you want to call, rather than the more
    // low-level and c-ares specific functions below. See
    // core/IDNSResolver.h for details.

    virtual void getFQDN( const GetFQDNCallback& resultCb,
                          const ErrorCallback& errorCb );

    virtual void getFQDN( const char* hostname,
                          const GetFQDNCallback& resultCb,
                          const ErrorCallback& errorCb );

    virtual void lookupHostWithFQDN( const char* host,
                                     const LookupHostWithFQDNCallback& resultCb,
                                     const ErrorCallback& errorCb );

    virtual void lookupHost( const char* host,
                             const LookupHostCallback& resultCb,
                             const ErrorCallback& errorCb );

    virtual void lookupAddr( const char* host, int port,
                             const LookupAddrCallback& resultCb,
                             const ErrorCallback& errorCb );

    virtual void lookupName( const sockaddr_ipv4_t& addr,
                             const LookupNameCallback& resultCb,
                             const ErrorCallback& errorCb );

    virtual void lookupNameFQ( const sockaddr_ipv4_t& addr,
                               const LookupNameCallback& cb,
                               const ErrorCallback& errorCb );

public:
    // Low-level resolution API, this calls you back with raw hostent
    // pointers, c-ares status codes, etc. You probably want to use
    // the IDNSResolver interface methods if possible.

    typedef int ares_status_t;
    typedef boost::function<void(ares_status_t, const struct hostent*)> AresHostCallback;
    typedef boost::function<void(ares_status_t, const char*, const char*)> AresNameInfoCallback;

    // Initiate an asynchronous name resolution on 'name'. The user
    // provided callback will be invoked with the results of the
    // resolution.
    void getHostByName( const char* name, int family,
                        const AresHostCallback& cb );

    // Initiate a reverse lookup on 'addr'. The user provided callback
    // will be invoked with the results.
    void getHostByAddr( const void *addr, int addrlen, int family,
                        const AresHostCallback& cb );

    // Initiate an asynchronous address-to-name resolution on the
    // provided socket address. The user provided callback will be
    // invoked with the results of the resolution.
    void getNameInfo( const struct sockaddr* sa, socklen_t salen, int flags,
                      const AresNameInfoCallback& cb );

private:
    typedef std::pair<Subscription, Subscription> SubscriptionPair;
    typedef std::map<ares_socket_t, SubscriptionPair> SubscriptionMap;

    void init( const ipv4_addr_vec_t& nameServers );

    // A helper function to back both lookupName and lookupNameFQ; the
    // bool parameter specifies if we want the FQDN.
    void lookupName( const sockaddr_ipv4_t& addr, bool fqdn,
                     const LookupNameCallback& resultCb,
                     const ErrorCallback& errorCb );

    // Completion hooks for IDNSResolver functions.
    static void getFQDNComplete( const GetFQDNCallback&,
                                 const ErrorCallback&,
                                 ares_status_t,
                                 const struct hostent* );

    static void lookupHostComplete( const LookupHostCallback&,
                                    const ErrorCallback&,
                                    ares_status_t,
                                    const struct hostent* );

    static void lookupHostWithFQDNComplete( const LookupHostWithFQDNCallback&,
                                            const ErrorCallback&,
                                            ares_status_t,
                                            const struct hostent* );

    static void lookupAddrComplete( const LookupAddrCallback&,
                                    const ErrorCallback&,
                                    int port,
                                    ares_status_t,
                                    const struct hostent* );

    static void lookupNameComplete( const LookupNameCallback&,
                                    const ErrorCallback&,
                                    ares_status_t,
                                    const char*, const char* );

    // This callback is invoked by c-ares whenever it creates new
    // sockets, or wants to adjust whether we are monitoring an
    // existing socket for read/write.
    static void aresSocketCallbackStatic( void*, ares_socket_t, int, int );
    void aresSocketCallback( ares_socket_t, int, int );

    // These are called back by the FDSet when readiness is detected
    // on the associated sockets.
    void readHandler( ares_socket_t );
    void writeHandler( ares_socket_t );

    void armTimer();
    void disarmTimer();
    void onTimerExpiry();

    static void logException( const char* );

#ifndef NDEBUG
    // check that the thread pumping m_fdSet is the same one requesting DNS resolutions
    bool onSingleThread() const;
    mutable boost::thread::id m_threadId;
#endif

    FDSet& m_fdSet;
    ares_channel m_channel;
    SubscriptionMap m_submap;
    TimerPtr m_timer;

    // We can't throw exceptions across c-ares contexts, so we use a
    // boost::exception_ptr to cache exceptions that occur when we
    // know we have c-ares frames above us on the stack, then re-throw
    // the exception when we are back in a safe zone. This is
    // obviously not MT safe, but could be made so with
    // thread-specific data. That doesn't seem warranted at the moment
    // since currently an AresResolver is always driven by a
    // single-threaded FDSet.
    boost::exception_ptr m_pendingException;
};
BB_DECLARE_SHARED_PTR( AresResolver );

} // namespace bb

#endif // BB_CORE_ARESRESOLVER_H

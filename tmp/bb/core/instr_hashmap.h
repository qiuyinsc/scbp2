#ifndef BB_CORE_INSTR_HASHMAP_H
#define BB_CORE_INSTR_HASHMAP_H

/* Contents Copyright 2013 Athena Capital Research LLC. All Rights Reserved. */

#include <bb/core/instrument.h>
#include <bb/core/hash_map.h>
#include <bb/core/fpmath.h>

namespace bb {
namespace detail {

struct hash_instrument_normalized
{
    size_t operator()( const bb::instrument_t& instr ) const
    {
        size_t seed = 0;
        boost::hash_combine( seed, instr.sym.id() );
        boost::hash_combine( seed, instr.prod );

        if ( instr.prod == bb::PROD_FUTURE )
        {
            boost::hash_combine( seed, instr.exp.expid() );
        }
        else if ( instr.prod == bb::PROD_OPTION )
        {
            boost::hash_combine( seed, instr.exp.expid() );
            boost::hash_combine( seed, instr.strike );
            boost::hash_combine( seed, instr.right );
        }

        return seed;
    }
};

struct instrument_compare
{
    bool operator()( const bb::instrument_t& instr0, const bb::instrument_t& instr1 ) const
    {
        using namespace bb;
        if( (instr0.prod == PROD_UNKNOWN) && (instr1.prod == PROD_UNKNOWN) )
            return true;

        if( (instr0.prod != instr1.prod) || (instr0.sym != instr1.sym) )
            return false;
        switch ( instr0.prod )
        {
            // Consider PROD_STOCKSs are equal if the basics are true
        case PROD_STOCK:     return true;

            // Consider PROD_FUTUREs need to have matching expiries
        case PROD_FUTURE:    return (instr0.exp == instr1.exp);

            // Consider PROD_OPTIONSs need to have matching expiries, rights, and strikes
        case PROD_OPTION:    return (instr0.exp == instr1.exp)
                && (instr0.right == instr1.right)
                && (bb::EQ(instr0.strike, instr1.strike));

            // Consider PROD_ACRINDEXs are equal if the basics are true
        case PROD_ACRINDEX:  return true;

        default:             return false;
        }
    }
};

}

template <typename T> class instr_hashmap
    : public bbext::hash_map<bb::instrument_t
                             , T
                             , detail::hash_instrument_normalized
                             , detail::instrument_compare>
{
public:
    typedef std::pair<bb::instrument_t, T&> pair;
};

} // namespace bb

#endif // BB_CORE_INSTR_HASHMAP_H
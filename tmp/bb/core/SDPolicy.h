#ifndef BB_CORE_SDPOLICY_H
#define BB_CORE_SDPOLICY_H

/* Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved. */


#include <boost/mpl/bool.hpp>
#include <boost/optional.hpp>

#include <bb/core/bbassert.h>
#include <bb/core/CError.h>
#include <bb/core/FDSet.h>

namespace bb {

enum FDCallbackType { FD_READ, FD_WRITE };
enum FDCallbackMode { FD_LEVEL_TRIGGERED, FD_EDGE_TRIGGERED };
struct BoostFnReactor;
BB_DECLARE_SHARED_PTR(BoostFnReactor);

////////////////////////////////////////////////////////////////////////
// SDReactor
//
// A reactor is a generic object which receives a callback on
// readability/writeability events, and performs the io.
////////////////////////////////////////////////////////////////////////


// most generic reactor
struct SDReactorBase : public sd_queue_link_t
{
    SDReactorBase();
protected:
    sd_queue_base_t *m_runQueue;
public:
    /// Responsible for removing the SDReactor from the internal SelectDispatcher
    /// datastructures on destruction. If you subclass SDReactor, note that your class
    /// will be destroyed before the SDSubscription is.
    struct SDSubscription : public SubscriptionBase
    {
        virtual ~SDSubscription() {}
        /// Causes the destructor to become a noop.
        /// Called when an FDSet is being destroyed, and there are callbacks still registered.
        virtual void orphan() = 0;
    };

    void orphan();
    bool hasQueue() { return m_runQueue; }
    void enqueue() { BB_ASSERT(m_runQueue); m_runQueue->push(this); }
    SDSubscription *m_sub;
};
BB_DECLARE_SHARED_PTR(SDReactorBase);

/// Fast reader or writer callback for SD. SDReactors can have custom
/// support inside the SelectDispatcher for optimized implementation.
struct SDReactor : public SDReactorBase
{
protected:
    ~SDReactor();
    void removeReactor();
};
BB_DECLARE_SHARED_PTR(SDReactor);

/// A fast SelectDispatcher Reader callback.
struct SDReader : public SDReactor
{
    void removeReader() { return removeReactor(); } // Undoes the effect of an FDSet::addReader
};

/// A fast SelectDispatcher Writer callback.
struct SDWriter : public SDReactor
{
    void removeReader() { return removeReactor(); } // Undoes the effect of an FDSet::addWriter
};

/// Slow "fallback" reactor which delegates to a boost function.
struct BoostFnReactor : public SDReactorBase
{
    static BoostFnReactorPtr create(const ReactorCallback &cb) { return BoostFnReactorPtr(new BoostFnReactor(cb)); }
    BoostFnReactor(const ReactorCallback &cb) : m_cb(cb) { BB_ASSERT(m_cb); }
    void setRunQueue(sd_queue_t<BoostFnReactor> *queue) { m_runQueue = queue; }
    ReactorCallback m_cb;
};




////////////////////////////////////////////////////////////////////////
// SDPolicy
//
// An SDPolicy is a scheduling policy which can dispatch to SDReader/SDWriters.
// There may be multiple policies in a program, each one handling a different
// type of reactor in in an optimized, transport-specific manner.
//
////////////////////////////////////////////////////////////////////////
struct SDPolicyHandle
{
    virtual ~SDPolicyHandle() {}
    /// This must set SDReactor::m_runQueue to indicate which run-queue
    /// the reactor should be push()ed on when it becomes readable or writable.
    /// Also decides whether the readable/writable event should be edge or level triggered.
    virtual boost::optional<FDCallbackMode> setupQueue(SDReactor *f) const { return boost::none; }
    virtual const ReactorCallback &getFallbackImpl() const
        { BB_THROW_ERROR("SDPolicyHandle: illegal call to getFallbackImpl"); }
};

/// Type-specific policy. An SDPolicyHandleFor<MyTransport> is a handle to a policy
/// which can dispatch readability or writeability events into MyTransport.
template<typename T>
struct SDPolicyHandleFor : public SDPolicyHandle
{
    SDPolicyHandleFor(const boost::function<FDCallbackMode (T*)> &setupQueueFn) : m_setupQueueFn(setupQueueFn) {}
    virtual boost::optional<FDCallbackMode> setupQueue(SDReactor *f)
    {
        BB_ASSERT(dynamic_cast<T*>(f));
        return m_setupQueueFn(static_cast<T*>(f));
    }
    boost::function<FDCallbackMode (T*)> m_setupQueueFn;
};

/// Handle to a generic (non-type-specific) policy.
struct SDFallbackPolicyHandle : public SDPolicyHandle
{
    SDFallbackPolicyHandle(const ReactorCallback &cb) : m_cb(cb) {}
    virtual const ReactorCallback &getFallbackImpl() const { return m_cb; }
    ReactorCallback m_cb;
};


/// MPL metafunction: returns true if the structure is an implementation
/// of the unoptimized Fallback policy.
template<typename C> struct SDFallbackPolicyPred { typedef boost::mpl::false_ type; };

/// MPL metafunction: returns true if the given Policy is an optimized
/// Policy dispatching into the given Reactor.
template<typename Policy, typename Reactor> struct SDPolicyHandlerPred { typedef boost::mpl::false_ type; };

/// The default implementation of fallback policy. Calls all readable/writable reactors in FIFO order.
struct SDFallbackPolicy
{
    bool edgeTriggeredWorkPending() { return false; }
    void setupFallbackQueue(FDCallbackType type, BoostFnReactor *react) { react->setRunQueue(&m_runQueue); }

    void service()
    {
        while(!m_runQueue.empty())
            m_runQueue.pop()->m_cb();
    }

    sd_queue_t<BoostFnReactor> m_runQueue;
};
template<> struct SDFallbackPolicyPred<SDFallbackPolicy> { typedef boost::mpl::true_ type; };
template<typename T> struct SDPolicyHandlerPred<SDFallbackPolicy, T> { typedef boost::mpl::false_ type; };

/// Alternative, 2-queue fallback policy. Calls readers first, then writers.
struct SDFallbackPolicy2Q
{
    bool edgeTriggeredWorkPending() { return false; }
    void setupFallbackQueue(FDCallbackType type, BoostFnReactor *react)
    {
        react->setRunQueue(
                type == FD_WRITE ? &m_levelTriggeredWriters
                                 : &m_levelTriggeredReaders);
    }

    void service()
    {
        while(!m_levelTriggeredReaders.empty())
            m_levelTriggeredReaders.pop()->m_cb();
        while(!m_levelTriggeredWriters.empty())
            m_levelTriggeredWriters.pop()->m_cb();
    }

    sd_queue_t<BoostFnReactor> m_levelTriggeredReaders,
                               m_levelTriggeredWriters;
};
template<> struct SDFallbackPolicyPred<SDFallbackPolicy2Q> { typedef boost::mpl::true_ type; };
template<typename T> struct SDPolicyHandlerPred<SDFallbackPolicy2Q, T> { typedef boost::mpl::false_ type; };

} // namespace bb

#endif // BB_CORE_SDPOLICY_H

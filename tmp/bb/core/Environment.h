#ifndef BB_CORE_ENVIRONMENT_H
#define BB_CORE_ENVIRONMENT_H

/* Contents Copyright 2013 Athena Capital Research LLC. All Rights Reserved. */

#include <string>
#include <vector>

#include <bb/core/acct.h>
#include <bb/core/CoreConfig.h>
#include <bb/core/LuaState.h>
#include <bb/core/mapdb.h>
#include <bb/core/RunningDetector.h>

namespace bb {

BB_FWD_DECLARE_SCOPED_PTR(LuaState);

/// Describes all things related to a self-contained BB environment.
struct EnvironmentConfig
{
    std::vector<std::string> additional_configs;
};

class Environment
{
public:
    static EnvironmentPtr create();
    static EnvironmentPtr create( const EnvironmentConfig& );
    ~Environment();

    /// Returns the configuration instance.
    LuaState& luaState();

    // This should be const, the initial environment config should not be modifiable?
    CoreConfig& config();
    const CoreConfig& config() const;

    /// Returns our BB root
    const std::string& getRoot() const;

    /// Returns the BB root, resolving any symlinks.
    /// Useful mostly for when you're logging your BB root.
    std::string getResolvedRoot();

    /// Returns our datafiles root
    const std::string& getDatafileRoot();

    /// Returns our unsplit datafiles root
    const std::string& getUnsplitDatafileRoot();

    /// Returns our tradelogs root
    const std::string& getTradelogsRoot();

    /// Returns the hostname
    const std::string& getHostname() const;

    /// Resolves an account string into an acct_t
    bool acctIsValid(const acct_t& acct) const;

    /// Resolves an account string into an acct_t
    acct_t str2acct(const char* str, bool* ok = NULL) const;

    /// Resolves an acct_t into an account string
    const char* acct2str(const acct_t& acct) const;

    // symbol_table.lua config accessors
    std::string getSymbolTableFilename(const std::string& symdb_name) const;
    std::vector<unsigned int> getSymbolTableSources(const std::string& symdb_name) const;

    RunningDetector& getRunningDetector();

private:
    /// Constructor.
    Environment( const EnvironmentConfig& );

    /// Initializes the default BB configuration.
    /// - If $BB_CONFIG exists, loads the file specified there, otherwise, loads
    ///   /with/bb/root/conf/core_config.lua
    /// - If $BB_CONFIG_VAL exists, it'll be executed after the config is loaded.
    void initCoreConfig( const EnvironmentConfig& cfg );

private:
    /// LuaState instance for this environment.
    LuaStateScopedPtr m_luaState;

    /// Root directory that we're dynamically linked to.
    std::string m_env_root, m_env_root_realpath;

    /// Datafile root directory
    std::string m_dfs_root, m_dfs_root_realpath;

    /// Unsplit datafile root directory
    std::string m_udfs_root, m_udfs_root_realpath;

    /// Tradelogs directory
    std::string m_tradelogs_root, m_tradelogs_root_realpath;

    /// Hostname of the machine this program is running on
    std::string m_hostname;

    /// Contains our account to ID table.
    mapdb m_acct_table;

    CoreConfig m_coreConfig;

    RunningDetector m_runningDetector;
};
}

#endif // BB_CORE_ENVIRONMENT_H

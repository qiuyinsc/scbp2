#ifndef BB_DB_EXCHANGES_H
#define BB_DB_EXCHANGES_H

/* Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved. */


#include <cstddef> // needed by bmfwd.h

#include <bm/bmfwd.h>

#include <string>
namespace bb {

class date_t;
class symbol_t;

namespace db {


typedef bm::bvector<> ExchangeSymbolBitVector;


/// Returns true if the symbol is a NYSE symbol.
///
/// The first invocation of this hits the database, but all following invocations are very cheap.
/// It may throw an exception on this first invocation, and will thereafter always return false without exception.
/// A best practice is to "prime the pump" with bb::db::is_nyse( SYM_UNKNOWN ) and handle the exception.
bool is_nyse( const symbol_t& sym );
bool is_nyse( const symbol_t& sym,std::string const&prof );
/// Returns a BitMagic vector which indicates NYSE symbols by ID.
///
/// The first invocation of this hits the database, but all following invocations are very cheap.
/// It may throw an exception on this first invocation, and will thereafter always return an empty bvector without exception.
const ExchangeSymbolBitVector& get_nyse_symbol_bit_vector();
const ExchangeSymbolBitVector& get_nyse_symbol_bit_vector(std::string const&prof);

/// Returns true if the symbol is an AMEX symbol.
///
/// The first invocation of this hits the database, but all following invocations are very cheap.
/// It may through an exception on this first invocation, and will thereafter always return false without exception.
/// A best practice is to "prime the pump" with bb::db::is_amex( SYM_UNKNOWN ) and handle the exception.
bool is_amex( const symbol_t& sym );
bool is_amex( const symbol_t& sym,std::string const&prof );
/// Returns a BitMagic vector which indicates AMEX symbols by ID.
///
/// The first invocation of this hits the database, but all following invocations are very cheap.
/// It may throw an exception on this first invocation, and will thereafter always return an empty bvector without exception.
const ExchangeSymbolBitVector& get_amex_symbol_bit_vector();
const ExchangeSymbolBitVector& get_amex_symbol_bit_vector(std::string const&prof);

/// Returns a BitMagic vector which indicates NYSE symbols by ID as of the given date.
///
/// This data comes from a different table than the non-historical one, so calling this for
/// "today" may not give the same results as calling the function which does not take a date.
/// No caching is done.
ExchangeSymbolBitVector get_nyse_symbol_bit_vector( const date_t& date );
ExchangeSymbolBitVector get_nyse_symbol_bit_vector( const date_t& date,std::string const&prof );

} // namespace db
} // namespace bb

#endif // BB_DB_EXCHANGES_H

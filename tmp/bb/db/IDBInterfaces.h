#ifndef BB_DB_IDBINTERFACES_H
#define BB_DB_IDBINTERFACES_H

/* Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved. */


#include <bb/core/safevalue.h>
#include <string>
#include <bb/core/smart_ptr.h>
//#include <cstdint>
#include <stdint.h>
namespace bb {
namespace db {
namespace detail{
struct RunLogIdPolicy:SafeFundamentalDefaultPolicy<uint64_t>{};
}
typedef SafeValue<detail::RunLogIdPolicy> RunLogId;

class IDBConfig
{
public:
    virtual std::string getDBProfileString() const=0;
    virtual std::string getDBSchemaName() const=0;
    virtual ~IDBConfig() ;
};
BB_DECLARE_SHARED_PTR(IDBConfig);

class IDBRunLog : public virtual IDBConfig
{
public:
    virtual RunLogId getRunLogId() const=0;

};
BB_DECLARE_SHARED_PTR(IDBRunLog);

class IDBRunLogUpdater : public virtual IDBRunLog
{
public:
    virtual void update_exit(int exit_status, std::string const&exit_msg)=0;
    virtual void update_app_id(std::string const& app_id)=0;

};
BB_DECLARE_SHARED_PTR(IDBRunLogUpdater);

class DBConfigDefaultImpl : public virtual IDBConfig
{
public:
    std::string getDBProfileString() const;
    std::string getDBSchemaName() const;
protected:
    DBConfigDefaultImpl(std::string const& _dbprof , std::string const& _dbschema);
    std::string         m_dbprof;
    std::string         m_dbname;
};
class DBConfigSimple : public  IDBConfig
{
public:
    std::string getDBProfileString() const;
    std::string getDBSchemaName() const;
    DBConfigSimple(std::string const& _dbprof , std::string const& _dbschema);
    DBConfigSimple(std::string const& _dbprof );
private:

    std::string         m_dbprof;
    std::string         m_dbname;
};
} // namespace db
} // namespace bb

#endif // BB_DB_IDBINTERFACES_H

#ifndef BB_DB_EXPIRYUTILS_H
#define BB_DB_EXPIRYUTILS_H

/* Contents Copyright 2015 Athena Capital Research LLC. All Rights Reserved. */

#include <boost/optional.hpp>

#include <bb/core/expiry.h>
#include <bb/core/date.h>
#include <bb/core/right.h>
#include <bb/core/instrument.h>

#include <bb/db/TradingCalendar.h>


namespace bb {

namespace db {

// This class provides a way to retrieve exact expiration dates for a particular instrument, therefore implementing the map (expiry_t, mktdest_t) -> date_t
// Each market has its own expiration date calculation rules so it needs to be mapped 1 by 1

class ExpiryUtils
{
public:
    enum WeekOfMonth {
        FirstWeek  = 1,
        SecondWeek = 2,
        ThirdWeek  = 3,
        FourthWeek = 4,
        FifthWeek  = 5
    };

    static CalendarCPtr getCachedCalendar( const bb::mktdest_t& mkt );

    static bb::date_t getExpiryDate( const bb::instrument_t& instr );
    static bb::date_t cacheExpiryDate( const bb::instrument_t& instr, const bb::date_t& expiryDate );

    static bool isTradingDay( const bb::date_t& date, const bb::mktdest_t& mkt );
    static uint32_t getNumberOfTradingDays( const bb::date_t& dateA, const bb::date_t& dateB, const bb::mktdest_t& mkt );
    static uint32_t getNumberOfCalendarDays( const bb::date_t& dateA, const bb::date_t& dateB );

protected:
    typedef std::map<bb::expiry_t, bb::date_t> ExpiryCacheEntryType;
    static std::map<bb::mktdest_t, ExpiryCacheEntryType> s_cachedExpiries;

    typedef std::map<bb::mktdest_t, CalendarCPtr> CalendarCacheType;
    static CalendarCacheType s_cachedCalendars;
};

}  // namespace db
}  // namespace bb

#endif // BB_DB_EXPIRYUTILS_H

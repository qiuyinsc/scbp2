#ifndef BB_DB_EQUITYSECURITYINFODB_H
#define BB_DB_EQUITYSECURITYINFODB_H

/* Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved. */


#include <iosfwd>
#include <string>
#include <set>

#include <bb/core/date.h>
#include <bb/core/smart_ptr.h>
#include <bb/core/EquitySecurityInfo.h>

struct lua_State;

namespace mysqlpp { class UseQueryResult; }

namespace bb {
namespace db {


bool registerEquitySecurityInfoDBEntities( lua_State& L );

class EquitySecurityInfoMapDB
    : public IEquitySecurityInfoMap
{
public:
    EquitySecurityInfoMapDB( const date_t& trade_date, const std::string& db_profile );

    const date_t& getDate() const;

    const_iterator begin() const;
    const_iterator end() const;

    void print( std::ostream& os ) const;

private:
    void populateFromQuery( mysqlpp::UseQueryResult& result, std::set<EquitySecurityInfo>& out ) const;

    date_t m_date;

    std::string m_db_profile;
    cont_type m_vals;
};

BB_DECLARE_SHARED_PTR( EquitySecurityInfoMapDB );

} // namespace db
} // namespace bb

#endif // BB_DB_EQUITYSECURITYINFODB_H

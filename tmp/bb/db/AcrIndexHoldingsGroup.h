#ifndef BB_DB_ACRINDEXHOLDINGSGROUP_H
#define BB_DB_ACRINDEXHOLDINGSGROUP_H

/* Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved. */


// Dependent includes

#include <iosfwd>
#include <vector>
#include <bb/core/smart_ptr.h>
#include <bb/core/instrument.h>
#include <bb/core/symbol.h>
#include <bb/core/safevalue.h>
#include <bb/core/safeenum.h>
#include <boost/static_assert.hpp>
#include <bb/core/Scripting.h>
struct lua_State;
namespace bb {
namespace AcrIndex{
BB_FWD_DECLARE_SHARED_PTR( Holdings );
class IAcrIndexHoldingsGroup
{
public:
    typedef HoldingsCPtr value_type;
    typedef std::vector<value_type> cont_type;
    typedef cont_type::const_iterator const_iterator;
    virtual ~IAcrIndexHoldingsGroup() {}
    virtual const_iterator begin()const=0;
    virtual const_iterator end()const=0;

    virtual bb::symbol_t    getSymbol() const=0;
    virtual bb::date_t      getTradeDate() const=0;
protected:
    virtual void print(std::ostream& )const=0;
    friend std::ostream& operator<<(std::ostream&,IAcrIndexHoldingsGroup const&);
};
BB_DECLARE_SHARED_PTR(IAcrIndexHoldingsGroup);

class AcrIndexHoldingsGroup : public IAcrIndexHoldingsGroup
{
public:
    typedef IAcrIndexHoldingsGroup::value_type value_type;
    typedef IAcrIndexHoldingsGroup::cont_type cont_type;
    typedef IAcrIndexHoldingsGroup::const_iterator const_iterator;
    bb::symbol_t                getSymbol() const;
    bb::date_t getTradeDate() const;
    void push_back(value_type const&);
    BB_DECLARE_SCRIPTING();
    const_iterator begin()const;
    const_iterator end()const;
    AcrIndexHoldingsGroup(bb::symbol_t const&,bb::date_t const&);
    void print(std::ostream& )const;
private:
    cont_type m_vals;
    bb::symbol_t m_symbol;
    bb::date_t m_date;
};
BB_DECLARE_SHARED_PTR(AcrIndexHoldingsGroup);
} // namespace AcrIndex
} // namespace bb


#endif // BB_DB_ACRINDEXHOLDINGSGROUP_H

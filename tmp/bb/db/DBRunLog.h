#ifndef BB_DB_DBRUNLOG_H
#define BB_DB_DBRUNLOG_H

/* Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved. */


#include <bb/db/IDBInterfaces.h>
#include <bb/core/smart_ptr.h>
#include <string>

namespace bb {
namespace db{

BB_FWD_DECLARE_SHARED_PTR( RunStats );
/*
 * This class is used to aid in keeping structured logs in the SQL database
 *
 * Could be used for productino trading, but more intended to
 * aid in backtesting and research
 *
 * The idea is to use this when the program starts
 * and record the RunLogId in all the info this instance stores in the db
 *
 * Makes it real easy to see how different parameters affect the output
 * when launching dozens of programs, or something else automated
 *
 *
 * This class supports
 *
DROP TABLE IF EXISTS `runlog`;

CREATE TABLE `runlog` (
  `run_id` bigint NOT NULL auto_increment,
  `command_line` text collate latin1_general_cs,
  `exit_status` int(11) default NULL,
  `start_time` datetime default NULL,
  `end_time` datetime default NULL,
  `exit_response` text collate latin1_general_cs,
  `hostname` varchar(30) collate latin1_general_cs default NULL,
  `appid` varchar(30) collate latin1_general_cs default NULL,
  PRIMARY KEY  (`run_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 COLLATE=latin1_general_cs;
 *
 *
 *
 * */
struct DBRunLog: DBConfigDefaultImpl,virtual IDBRunLogUpdater
{
    DBRunLog(std::string const&cmd_line, std::string const& _dbname, std::string const& _dbprof, std::string const& tblname="runlog");
    void update_exit(int exit_status, std::string const&exit_msg);
    void update_app_id(std::string const& app_id);
    RunLogId getRunLogId() const;
private:
    std::string  m_tablename;
    RunLogId             m_id;
};
BB_DECLARE_SHARED_PTR( DBRunLog );
//does nothing
struct EmptyDBRunLog: DBConfigDefaultImpl,virtual IDBRunLogUpdater
{
    EmptyDBRunLog(std::string const& _dbname, std::string const& _dbprof);
    void update_exit(int exit_status, std::string const&exit_msg);
    void update_app_id(std::string const& app_id);
    RunLogId getRunLogId() const;
};
BB_DECLARE_SHARED_PTR( EmptyDBRunLog );


}} //namespace db,bb

#endif // BB_DB_DBRUNLOG_H

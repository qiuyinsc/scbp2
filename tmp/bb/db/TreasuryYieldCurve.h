#ifndef BB_DB_TREASURYYIELDCURVE_H
#define BB_DB_TREASURYYIELDCURVE_H

/* Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved. */


// Dependent includes
#include <bb/core/smart_ptr.h>
#include <bb/core/date.h>
#include <bb/core/gdate.h>
#include <vector>
#include <tr1/array>
namespace bb {
struct TreasuryYieldCurve ;
/**
 TreasuryYieldCurve
 Represents summary information for a TreasuryYieldCurve.

 **/
struct TreasuryYieldCurve {

    struct Data{
        enum YieldTime{ZeroMonth,OneMonth,ThreeMonth,SixMonth,OneYear,TwoYear,ThreeYear,FiveYear,SevenYear,TenYear,TwentyYear,ThirtyYear};

        //Note that Treasuries use Actual/Actual for their computations
        enum YieldDays{
            ZeroMonthDays=0
            ,OneMonthDays=30
            ,ThreeMonthDays=91
            ,SixMonthDays=182
            ,OneYearDays=365
            ,TwoYearDays=365*2
            ,ThreeYearDays=365*3
            ,FiveYearDays=(365*5)+1 //one leap year
            ,SevenYearDays=(365*7)+1
            ,TenYearDays=(365*10)+2 //
            ,TwentyYearDays=(365*20)+5
            ,ThirtyYearDays=(365*30)+7
            };

        float operator[](YieldTime y)const{
            if(y==ZeroMonth)return 0;
            return m_yields[y-1];
        }
        bool operator<(Data const& d)const{
            return m_ymddate<d.m_ymddate;
        }
        bool operator<(bb::date_t const& d)const{
            return m_ymddate<unsigned(d.ymd_date());
        }
        friend bool operator<(bb::date_t const& d,Data const& d2){
            return unsigned(d.ymd_date()) <d2.m_ymddate;
        }
        static std::pair<TreasuryYieldCurve::Data::YieldTime,TreasuryYieldCurve::Data::YieldDays>
        getYieldTimeAfter(unsigned days);
        static std::pair<TreasuryYieldCurve::Data::YieldTime,TreasuryYieldCurve::Data::YieldDays>
        getYieldTimeBefore(unsigned days);
        unsigned ymd_date()const{return m_ymddate;}
private:
        typedef std::tr1::array<float,ThirtyYear> yields_t;
        Data(unsigned _date  ):m_ymddate(_date){}
        void push_back(float v,YieldTime y );
        friend struct TreasuryYieldCurve;
        unsigned m_ymddate;
        yields_t m_yields;
    };
    typedef std::vector<Data> vector_t;
    typedef vector_t::const_iterator const_iterator;

    const_iterator begin()const;
    const_iterator end()const;
    //which record to use for rate calculation
    enum NowDateOptions{
        Actual//use date specified,throw if record is not found
        ,AtOrBefore //use date specified, or use previous
        ,AtOrAfter //use date specified, or use next
    };
    float getRate(bb::date_t const& now,bb::date_t const& maturity,NowDateOptions)const;
    float getForwardRate(bb::date_t const& now,bb::date_t const& _inception,bb::date_t const& maturity,NowDateOptions)const;
    /// Constructs a TreasuryYieldCurve with all data in DB, which is not that much
    TreasuryYieldCurve(std::string const& _dbprof);
private:
    //helpers
    const_iterator find_record(bb::date_t const&,NowDateOptions)const;
    std::pair<double,unsigned> compute_spotrate(Data const& _dd,bb::gdate_t const& now,bb::gdate_t const& maturity)const;
    vector_t m_vals;

};
BB_DECLARE_SHARED_PTR( TreasuryYieldCurve );

} //namespace bb


#endif // BB_DB_TREASURYYIELDCURVE_H

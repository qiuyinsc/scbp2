#ifndef BB_DB_ASHARERIGHTDIVIDEND_H
#define BB_DB_ASHARERIGHTDIVIDEND_H

/* Contents Copyright 2015 Athena Capital Research LLC. All Rights Reserved. */

#include <bb/core/instrument.h>
#include <bb/core/symbol.h>
#include <bb/core/timeval.h>
#include <bb/core/date.h>
#include <bb/core/env.h>
#include <bb/core/Error.h>
#include <bb/core/Calendar.h>
#include <bb/core/Log.h>
#include <bb/db/Connection.h>
#include <bb/db/QueryBuilder.h>
#include <bb/db/TradingCalendar.h>

#include <map>
#include <mysql++/mysql++.h>

namespace bb {
namespace db{

class AshareRightsDividendStorage{

public:

    AshareRightsDividendStorage( const instrument_t &ins, const date_t &ex_dt, double cash_dvd_pre_tax, double cash_dvd_after_tax, double bonus_share_rt,
            double conversedissue_rt,double rightsissue_px,double rightsissue_rt,double seo_px,double seo_rt,double consolidate_split_rt );

    instrument_t getInstrument() const {return m_instrument;}
    date_t getEXDate() const {return m_ex_date;}
    double getCashDividendPreTax() const  {return m_cash_dividend_pre_tax;}
    double getCashDividendAfterTax() const {return m_cash_dividend_after_tax;}
    double getBonusShareRatio() const {return m_bonus_share_ratio;}
    double getConversedIssueRatio() const {return m_conversedissue_ratio;}
    double getRightsIssuePrice() const {return m_rightsissue_price;}
    double getRightsIssueRatio() const {return m_rightsissue_ratio;}
    double getSEOPrice() const {return m_seo_price;}
    double getSEORatio() const {return m_seo_ratio;}
    double getConsolidateSplitRatio() const {return m_consolidate_split_ratio;}
    void print();

private:

    instrument_t m_instrument;
    date_t m_ex_date;
    double m_cash_dividend_pre_tax;
    double m_cash_dividend_after_tax;
    double m_bonus_share_ratio;
    double m_conversedissue_ratio;
    double m_rightsissue_price;
    double m_rightsissue_ratio;
    double m_seo_price;
    double m_seo_ratio;
    double m_consolidate_split_ratio;
};

class AshareDividendsManager{

public:

    AshareDividendsManager( const date_t &start_date, const date_t &end_date );

    double referencePrice( const instrument_t &ins, const date_t &date, const CalendarCPtr &calendar, bool after_tax=false );
    double referencePrice( const instrument_t &ins, const date_t &date, const double & last_px, bool after_tax=false );
    std::pair<double, bool> getOvernightAdjustFactor( const instrument_t &ins, const date_t &date, const double &last_px, bool after_tax=false );

    bool updatePortfolio( const instrument_t &ins, const date_t & date, double & cash, double & position, double & last_px,  bool after_tax=false );
    enum { WINDCODE, EX_DATE, CASH_DIVIDEND_PRE_TAX, CASH_DIVIDEND_AFTER_TAX, BONUS_SHARE_RATIO,CONVERSED_RATIO, RIGHTSISSUE_PRICE, RIGHTSISSUE_RATIO, SEO_PRICE, SEO_RATIO, CONSOLIDATE_SPLIT_RATIO , MAX_SIZEENUM };

protected:

    typedef std::map<date_t, AshareRightsDividendStorage> DateCorpActionMap;
    typedef std::map<instrument_t, DateCorpActionMap> AshareRightsDividendsMap;

    const date_t  start_date,end_date;
    AshareRightsDividendsMap asharemap;

    std::string getQueryAshareRightsDividend( const instrument_t &ins );
    std::string getQueryAshareRightsDividend();

    void updateAshareRightsDividentsMap( const instrument_t &ins, const std::string& profile="dbdev" );
    void updateAshareRightsDividentsMap( const std::string& profile="dbdev" );
    double closePriceOnDate( const instrument_t &ins, const date_t &date, const std::string& profile = "dbstage" );

};

BB_DECLARE_SHARED_PTR( AshareDividendsManager );
} //namespace db
} //namespace bb
#endif // BB_DB_ASHARERIGHTDIVIDEND_H

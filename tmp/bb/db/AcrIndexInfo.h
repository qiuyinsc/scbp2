#ifndef BB_DB_ACRINDEXINFO_H
#define BB_DB_ACRINDEXINFO_H

/* Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved. */


// Dependent includes
#include <vector>
#include <iosfwd>
#include <bb/core/smart_ptr.h>
#include <bb/core/instrument.h>
#include <bb/core/symbol.h>
#include <bb/core/safevalue.h>
#include <bb/core/safeenum.h>
#include <boost/static_assert.hpp>
#include <tr1/type_traits>
#include <bb/db/AcrIndexSpec.h>
struct lua_State;
namespace bb {
namespace AcrIndex{

typedef SafeValue<detail::HoldingAlternateIdPolicy> HoldingAlternateId;


//this is the spec for choosing which basket of components we will use
struct IInfo{
    virtual ~IInfo();
    friend std::ostream& operator<<(std::ostream&,IInfo const&);
    virtual void print(std::ostream&)const=0;
};
BB_DECLARE_SHARED_PTR( IInfo );

struct InfoContainer{
    typedef  std::vector<IInfoCPtr>  cont_type;
    typedef  cont_type::const_iterator  const_iterator;
    typedef  const_iterator  iterator;
    template<typename T > bb::shared_ptr<T const> get()const{
        BOOST_STATIC_ASSERT((std::tr1::is_base_of<IInfo, T>::value));
        for(const_iterator i=m_infos.begin(),e=m_infos.end();i!=e;++i){
            bb::shared_ptr<T const> ret=bb::dynamic_pointer_cast<T const>(*i);
            if(ret)return ret;
        }
        return bb::shared_ptr<T const>();
    }
    template<typename T >
    void add(bb::shared_ptr<T const> const&v){
        BOOST_STATIC_ASSERT((std::tr1::is_base_of<IInfo, T>::value));
        if(!v)return;
        for(cont_type::iterator i=m_infos.begin(),e=m_infos.end();i!=e;++i){
             bb::shared_ptr<T const> ret=bb::dynamic_pointer_cast<T const>(*i);
             if(ret){
                 *i=v;
                 return;
             }
         }
        m_infos.push_back(v);
    }


    void clear();
private:
    std::vector<IInfoCPtr> m_infos;
};

struct IAlternate:IInfo{
    virtual HoldingAlternateId getId()const=0;
};
BB_DECLARE_SHARED_PTR( IAlternate );
struct AlternateInfo: IAlternate {
    HoldingAlternateId getId() const;
    void print(std::ostream&os) const;
    AlternateInfo(HoldingAlternateId const& _id);
private:
    HoldingAlternateId m_id;
};
BB_DECLARE_SHARED_PTR( AlternateInfo );
namespace detail{
struct SolverTypePolicy{
    typedef unsigned  storage_type;
    enum _EnumVals{
        InteriorPoint=0,LevenbergMarquart=1,GoldfarbIdnani=2
    };
    typedef _EnumVals enum_type;
    static const _EnumVals largest_enum_val=GoldfarbIdnani;
    static storage_type narrow(storage_type _val);
    friend std::ostream&operator<<(std::ostream&os,enum_type val);
private:
    BOOST_STATIC_ASSERT(sizeof(storage_type) <=sizeof(unsigned) );
    BOOST_STATIC_ASSERT(unsigned(1<<((sizeof(storage_type)*8)-1 ) ) >=largest_enum_val);

};

}//detail

typedef SafeEnum<detail::SolverTypePolicy> SolverType;
struct ISolverStats:IInfo{
    virtual SolverType getSolverType()const=0;
    virtual int   getResultCode()const=0;///0 == success
    virtual float getResultMean()const=0;
    virtual float getResultVariance()const=0;
    virtual float getResultMax()const=0;
    virtual float getResultMin()const=0;
    virtual unsigned getNumSamples()const=0;

};
BB_DECLARE_SHARED_PTR( ISolverStats );

bool registerInfoEntities( lua_State&  L );

}// namespace AcrIndex

} // namespace bb


#endif // BB_DB_ACRINDEXINFO_H

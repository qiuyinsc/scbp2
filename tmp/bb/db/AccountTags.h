#ifndef BB_DB_ACCOUNTTAGS_H
#define BB_DB_ACCOUNTTAGS_H

/* Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved. */


#include <map>
#include <vector>

#include <antlr/ASTFactory.hpp>
#include <antlr/ASTRefCount.hpp>

#include <bm/bm.h>

#include <bb/core/smart_ptr.h>
#include <bb/db/Connection.h>

namespace bb {

BB_FWD_DECLARE_SHARED_PTR( Environment );

namespace db {

typedef std::vector<std::string> AccountTagList;
typedef bm::bvector<> AccountBitmap;

// use these functions if you will do a very small number of queries
AccountBitmap accountsFromTags( const AccountTagList& tags, const std::string& db_profile = "dbprod" );
AccountBitmap accountsFromConfiguredTagExpression( const std::string& expression, const std::string& db_profile = "dbprod" );
AccountBitmap accountsFromTagExpression( const std::string& expression, const std::string& db_profile = "dbprod" );

// use this class if you will do many queries, especially if they will contain some of the same tags
// there is no expiration, so long-running programs may see stale data (we could make a flush() method)
class AccountTagsCache
{
public:
    AccountTagsCache( const bb::EnvironmentPtr& env, const std::string& db_profile = "dbprod" );

    // takes a single tag, such as "newedge"
    AccountBitmap accountsFromTag( const std::string& tag );

    // takes a tag expression and adds configured tag to list only currently active accounts for eg. "(etc - strategy) & configured"
    // recommended for use in webapps
    AccountBitmap accountsFromConfiguredTagExpression( const std::string& expression );

    // takes a tag expression, such as "strategy & newedge"
    AccountBitmap accountsFromTagExpression( const std::string& expression );

private:
    AccountBitmap compile( const antlr::RefAST& );

    typedef std::map<std::string, AccountBitmap> TagAccountsMap; // cache of single tags to accounts
    TagAccountsMap m_map;
    ConnectionPtr m_db;
    antlr::ASTFactory m_factory;
};
BB_DECLARE_SHARED_PTR( AccountTagsCache );

} // namespace db
} // namespace bb

#endif // BB_DB_ACCOUNTTAGS_H

#ifndef BB_DB_BBOCACHE_H
#define BB_DB_BBOCACHE_H

/* Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved. */


#include <boost/shared_ptr.hpp>
#include <boost/none.hpp>

#include <bb/core/timeval.h>
#include <bb/core/hash_map.h>
#include <bb/core/symbol.h>
#include <bb/core/mktdest.h>
#include <bb/core/smart_ptr.h>
#include <bb/core/MarketLevel.h>

namespace bb {

/*
 * Components of a bbo that we put into the database
 */
struct BBOCache
{
    BBOCache()
        : m_bid_mkt(bb::MKT_UNKNOWN)
        , m_ask_mkt(bb::MKT_UNKNOWN)
        , m_sym(bb::SYM_UNKNOWN)
        , m_date(bb::date_t::today())
        , m_update_time(bb::timeval_t::earliest)
        , m_last_modified_tv(bb::timeval_t::now)
    {
    }

    BBOCache(const bb::symbol_t &sym)
        : m_bid_mkt(bb::MKT_UNKNOWN)
        , m_ask_mkt(bb::MKT_UNKNOWN)
        , m_sym(sym)
        , m_date(bb::date_t::today())
        , m_update_time(bb::timeval_t::earliest)
        , m_last_modified_tv(bb::timeval_t::now)
    {
    }
    bb::mktdest_t m_bid_mkt;
    bb::mktdest_t m_ask_mkt;
    bb::symbol_t  m_sym;
    MarketLevel   m_mlevel; //contains bid (px, sz), ask(px, sz)
    bb::date_t    m_date;
    bb::timeval_t m_update_time;
    bb::timeval_t m_last_modified_tv;
};

BB_DECLARE_SHARED_PTR(BBOCache);

namespace db {

/// Retrieves the BBOCachePtr corresponding to the specified symbol
/// on the day of the specified timeval.
/// If the corresponding BBOCache doesn't exist, the return value is an invalid shared_ptr.
/// This can be tested with a bool cast or ! operation.
BBOCachePtr getBBOCache( symbol_t sym, const std::string& profile = "production_inp" );

} // namespace db


} // namespace bb


#endif // BB_DB_BBOCACHE_H

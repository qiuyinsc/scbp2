#ifndef BB_DB_TRADINGCALENDAR_H
#define BB_DB_TRADINGCALENDAR_H

/* Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved. */


#include <iosfwd>

#include <bb/core/date.h>
#include <bb/core/Calendar.h>
#include <bb/core/Scripting.h>
#include <bb/core/smart_ptr.h>
#include <bb/core/mktdest.h>

namespace bb {

BB_FWD_DECLARE_SHARED_PTR( Environment );

namespace db {

class TradingCalendarFactory : public ICalendarFactory
{
    BB_DECLARE_SCRIPTING();

public:
    enum Source { UnknownCalendar = '0', USEquities = 'A', CanadianEquities = 'C', ShanghaiFutures = 'S',TocomFutures = 'J', HongKongFutures = 'H', CMEFutures = 'M', TaiwanFutures = 'T', KoreaExchange = 'K', SingaporeFutures = 's' };

    //todo: move TradingCalendarFactor::Source to core
    static Source mktdest_to_primary_calendar( const mktdest_t& mkt );

    // ICalendarFactory implementation
    CalendarCPtr create() const;
    CalendarCPtr create(bb::date_t const& start_dt, bb::date_t const& end_dt) const;
    CalendarCPtr create(bb::date_t const& start_dt, int _days) const;
    Source getSource() const;

    TradingCalendarFactory( const bb::EnvironmentPtr& env, Source source, std::string const& _dbprof );
    TradingCalendarFactory( Source source, std::string const& _dbprof );

    friend std::ostream& operator<<( std::ostream& os, TradingCalendarFactory const& c );

private:
    CalendarCPtr m_basecalendar;
};
BB_DECLARE_SHARED_PTR( TradingCalendarFactory );

typedef TradingCalendarFactory::Source CalendarSource;

std::ostream& operator <<(std::ostream&, TradingCalendarFactory::Source);

} // namespace db
} // namespace bb


#endif // BB_DB_TRADINGCALENDAR_H

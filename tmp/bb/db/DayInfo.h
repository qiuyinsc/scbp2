#ifndef BB_DB_DAYINFO_H
#define BB_DB_DAYINFO_H

/* Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved. */


#include <vector>

#include <boost/function.hpp>
#include <boost/none.hpp>
#include <boost/optional.hpp>

#include <bb/core/env.h>
#include <bb/core/timeval.h>
#include <bb/core/hash_map.h>
#include <bb/core/instrument.h>
#include <bb/core/smart_ptr.h>
#include <bb/core/symbol.h>

namespace bb {

// Forward declarations
BB_FWD_DECLARE_SHARED_PTR(DayInfo);
BB_FWD_DECLARE_SHARED_PTR(DayInfoFuture);
BB_FWD_DECLARE_SHARED_PTR(DayInfoGlobal);
BB_FWD_DECLARE_SHARED_PTR(DayInfoMap);
BB_FWD_DECLARE_SHARED_PTR(DayInfoFuturesMap);
BB_FWD_DECLARE_SHARED_PTR(DayInfoGlobalMap);

namespace db {

/// Retrieves the DayInfoPtr corresponding to the specified symbol
/// on the day of the specified timeval.

/// If the corresponding DayInfo(Map) doesn't exist, the return value is an invalid shared_ptr.
/// This can be tested with a bool cast or ! operation.
DayInfoPtr getDayInfo( const bb::EnvironmentPtr& env, symbol_t sym, const timeval_t& tv
    , const std::string& profile = "production" );

/// Retrieves the DayInfoMapPtr corresponding to the day of the specified timeval.
DayInfoMapPtr getDayInfoMap( const bb::EnvironmentPtr& env, const timeval_t& tv
    , const std::string& profile = "production" );

/// Retrieves the DayInfoMapPtr corresponding to the most recent day in the past week with valid data.
DayInfoMapPtr getDayInfoMapLatest( const EnvironmentPtr& env, const timeval_t& tv
    , const std::string& profile = "production" );

/// Update the day info cache (both Map and sym/date) if exists
void updateDayInfo( const DayInfo& day_info );

/// Retrieves the DayInfoMapPtr corresponding to the day of the specified timeval.
DayInfoFuturesMapPtr getDayInfoFuturesMap( const bb::EnvironmentPtr& env, const timeval_t& tv, const std::string& profile );
DayInfoFuturesMapPtr getDayInfoFuturesMapAvg( const bb::EnvironmentPtr& env
    , timeval_t const& start_tv, timeval_t const& end_tv, const std::string& profile );
DayInfoFuturesMapPtr getDayInfoFuturesMapLatest( const bb::EnvironmentPtr& env
    , const timeval_t& tv, const std::string& profile, boost::optional<date_t> cutOffDate = boost::none );

const std::string getQueryDayInfoGlobalMap( const date_t& bbdate, const uint32_t session );
DayInfoGlobalMapPtr getDayInfoGlobalMap( const bb::EnvironmentPtr& env, const date_t& bbdate, const uint32_t session, const std::string& profile = "production" );
DayInfoGlobalMapPtr getDayInfoGlobalMap( const EnvironmentPtr& env, const timeval_t& tv, const uint32_t session, const std::string& profile = "production" );

const std::string getQueryDayInfoGlobalMapAvg( date_t const& bb_start_date , date_t const& bb_end_date, const uint32_t session );
DayInfoGlobalMapPtr getDayInfoGlobalMapAvg( const EnvironmentPtr& env, date_t const& startdate
    , date_t const& enddate, const uint32_t session, const std::string& profile = "production" );
DayInfoGlobalMapPtr getDayInfoGlobalMapAvg( const bb::EnvironmentPtr& env
    , timeval_t const& start_tv, timeval_t const& end_tv, const uint32_t session, const std::string& profile = "production" );

const std::string getQueryDayInfoGlobalMapLatest(const date_t& bbdate, const uint32_t session, boost::optional<date_t> cutOffDate = boost::none );
DayInfoGlobalMapPtr getDayInfoGlobalMapLatest( const bb::EnvironmentPtr& env
    , const timeval_t& tv, const uint32_t session, const std::string& profile = "production", boost::optional<date_t> cutOffDate = boost::none );
DayInfoGlobalMapPtr getDayInfoGlobalMapLatest( const EnvironmentPtr& env
    , const date_t& bbdate, const uint32_t session, const std::string& profile = "production", boost::optional<date_t> cutOffDate = boost::none);

typedef bbext::hash_map<symbol_t, uint64_t> AverageDailyVolumeMap;
BB_DECLARE_SHARED_PTR( AverageDailyVolumeMap );

/// Retrieves the average daily volume over the specified interval.
AverageDailyVolumeMapCPtr getAverageDailyVolumeMap( const bb::EnvironmentPtr& env
    , const timeval_t& end_time, int previous_days, const std::string& profile = "production" );

} // namespace db


namespace detail {
struct DayInfoStorage
{
    /// Sets the opening price.
     void setOpen( double open_ )                { m_open = open_; }

     /// Returns the opening price.
     double getOpen() const                      { return m_open; }

     /// Sets the closing price.
     void setClose( double close_ )              { m_close = close_; }

     /// Returns the closing price.
     double getClose() const                     { return m_close; }

     /// Sets the lowest price.
     void setLow( double low )                   { m_low = low; }

     /// Returns the lowest price.
     double getLow() const                       { return m_low; }

     /// Sets the highest price.
     void setHigh( double high )                 { m_high = high; }

     /// Returns the highest price.
     double getHigh() const                      { return m_high; }

     /// Sets the volume.
     void setVolume( unsigned long vol )         { m_vol = vol; }

     /// Returns the volume.
     unsigned long getVolume() const             { return m_vol; }

     // Sets the last tick px
     void setLast( double last_tick )            { m_last = last_tick; }

     // returns last tick or close if last_tick is 0.0
     double getLast() const                      { return m_last; }

protected:
     DayInfoStorage();

private:
     double         m_open, m_close;
     double         m_low, m_high;
     unsigned long  m_vol;
     double         m_last;
};
} // namespace detail


/**
   DayInfo
   Represents daily information for a particular symbol/date pair.
**/
class DayInfo : public detail::DayInfoStorage
{
public:
    // Empty constructor
    DayInfo();

    /// Constructs a DayInfo with the given sym and tv.
    /// Once created, a DayInfo's sym and tv cannot be changed.
    /// The other attributes are left uninitialized.
    DayInfo( symbol_t sym, const timeval_t& tv );

    /// Returns the symbol this DayInfo refers to.
    symbol_t getSymbol() const                  { return m_sym; }

    /// Returns the timeval_t this DayInfo refers to.
    /// The timeval_t will midnight of that day's morning.
    timeval_t getTimeval() const                { return m_tv; }

private:
    symbol_t       m_sym;
    timeval_t      m_tv;
};

class DayInfoFuture : public detail::DayInfoStorage
{
public:
    // Empty constructor
    DayInfoFuture();

    /// Constructs a DayInfo with the given sym and tv.
    /// Once created, a DayInfo's sym and tv cannot be changed.
    /// The other attributes are left uninitialized.
    DayInfoFuture( instrument_t const& sym, const timeval_t& tv );

    /// Returns the symbol this DayInfoFuture refers to.
    instrument_t getInstrument() const      { return m_sym; }

    /// Returns the date_t this DayInfoFuture refers to.
    /// The date_t will midnight of that day's morning.
    timeval_t    getTimeval() const         { return m_tv; }
    double       getSettlement() const      { return m_settlement;}

    // Sets the settlement price
    void setSettlement( double settlement )     { m_settlement = settlement; }

private:
    instrument_t       m_sym;
    timeval_t              m_tv;
    double             m_settlement;
};

std::ostream& operator<<( std::ostream&, DayInfoFuture const& );


class DayInfoGlobal : public detail::DayInfoStorage
{
public:
    // Empty constructor
    DayInfoGlobal();

    /// Constructs a DayInfoGlobal with the given sym and tv.
    /// Once created, a DayInfoGlobal's sym and tv cannot be changed.
    /// The other attributes are left uninitialized.
    DayInfoGlobal( instrument_t const& instr, const timeval_t& tv );

    /// Returns the symbol this DayInfoGlobal refers to.
    instrument_t getInstrument() const      { return m_instr; }

    /// Returns the date_t this DayInfoGlobal refers to.
    /// The date_t will midnight of that day's morning.
    timeval_t    getTimeval() const         { return m_tv; }

    // Sets the settlement price
    void setSettlement( double settlement )     { m_settlement = settlement; }

    // returns the settlement price
    double getSettlement() const                { return m_settlement; }

    // Sets the open interest
    void setOpenInterest( double open_interest ){ m_open_interest = open_interest; }

    // returns the open interest
    double getOpenInterest() const              { return m_open_interest; }

    // Sets the session id
    void setSessionId( double session_id ){ m_session_id = session_id; }

    // returns the session id
    double getSessionId() const              { return m_session_id; }

private:
    instrument_t       m_instr;
    timeval_t          m_tv;
    double             m_settlement;
    unsigned long      m_open_interest;
    uint32_t           m_session_id;
};
BB_DECLARE_SHARED_PTR(DayInfoGlobal);
std::ostream& operator<<( std::ostream&, DayInfoGlobal const& );





/**
   DayInfoMap
   Represents a set of daily information for symbols on a particular date.
   DayInfoMaps are created through the db factory function db::getDayInfoMap().
**/
class DayInfoMap
{
public:
    DayInfoMap( const timeval_t& tv )
        : m_tv( tv )
    {}

    /// Returns the DayInfo for the given symbol.
    /// Returns boost::none if that symbol is not found.
    boost::optional<DayInfo> get( symbol_t sym ) const
    {
        SymDayInfoMap::const_iterator iter = m_sym_DayInfo_map.find( sym );
        if ( iter == m_sym_DayInfo_map.end() )
            return boost::none;
        return iter->second;
    }

    /// Returns the timeval of this DayInfo.
    const timeval_t& getDate() const { return m_tv; }

    /// Adds a DayInfo to the map.
    void addDayInfo( const DayInfo& dayInfo ) { m_sym_DayInfo_map[dayInfo.getSymbol()] = dayInfo; }

    size_t size() const { return m_sym_DayInfo_map.size(); }
    size_t empty() const { return m_sym_DayInfo_map.empty(); }
    void forEach( boost::function<void (const DayInfo&)> callback ) const
    {
        for( bbext::hash_map<symbol_t, DayInfo>::const_iterator it = m_sym_DayInfo_map.begin();
                it != m_sym_DayInfo_map.end(); ++it )
            callback( it->second );
    }

private:
    timeval_t m_tv;

    typedef bbext::hash_map<symbol_t, DayInfo> SymDayInfoMap;
    SymDayInfoMap m_sym_DayInfo_map;
};

/**
   DayInfoFuturesMap
   Represents a set of daily information for futures on a particular date.
   DayInfoMaps are created through the db factory function db::DayInfoSet().
**/
class DayInfoFuturesMap
{
public:
    typedef std::vector< DayInfoFuture> cont_type;
    typedef cont_type::value_type value_type;
    typedef cont_type::const_iterator const_iterator;
    typedef cont_type::iterator iterator;

    DayInfoFuturesMap( const timeval_t& tv )
        : m_tv( tv )
    {}

    /// Returns the DayInfo for the given symbol.
    /// Returns boost::none if that symbol is not found.
    boost::optional<DayInfoFuture> get( instrument_t const& instr ) const
    {
        const_iterator i = std::lower_bound(m_sym_DayInfo_map.begin(), m_sym_DayInfo_map.end(), instr, Comp());
        if( i == m_sym_DayInfo_map.end() || Comp()(instr, *i) )
            return boost::none;
        return *i;
    }

    /// Returns the timeval of this DayInfo.
    const timeval_t& getDate() const { return m_tv; }

    /// Adds a DayInfo to the map.
    void addDayInfo( const DayInfoFuture& dayInfo ) ;

    size_t size() const;
    bool empty() const;

    // DayInfoFuturesMap is sorted according to this comparator
    struct Comp
    {
        bool operator()(DayInfoFuture const& l, instrument_t const& r) const
        {
            unsigned const symdl = l.getInstrument().sym.id();
            unsigned const symdr = r.sym.id();
            return (symdl < symdr) || (symdl == symdr && l.getInstrument().exp < r.exp);
        }

        bool operator()(instrument_t const& l, DayInfoFuture const& r) const
        {
            unsigned const symdr = r.getInstrument().sym.id();
            unsigned const symdl = l.sym.id();
            return (symdl < symdr) || (symdl == symdr && l.exp < r.getInstrument().exp);
        }

        bool operator()(DayInfoFuture const& l, symbol_t const& r) const
        {
            return l.getInstrument().sym < r;
        }

        bool operator()(symbol_t const& l, DayInfoFuture const& r) const
        {
            return l < r.getInstrument().sym;
        }
    };

    const_iterator begin() const { return m_sym_DayInfo_map.begin(); }
    const_iterator end()   const { return m_sym_DayInfo_map.end(); }

private:
    timeval_t m_tv;
    cont_type m_sym_DayInfo_map;
};


/**
   DayInfoGlobalMap
   Represents a set of daily information for equities/futures/options on a particular date.
**/
class DayInfoGlobalMap
{
public:
    typedef std::vector<DayInfoGlobal> cont_type;
    typedef cont_type::value_type value_type;
    typedef cont_type::const_iterator const_iterator;
    typedef cont_type::iterator iterator;

    DayInfoGlobalMap( const timeval_t& tv, const uint32_t sessionId )
        : m_tv( tv )
        , m_session_id( sessionId )
    {}

    /// Returns the DayInfoGlobal for the given instrument.
    /// Returns boost::none if that symbol is not found.
    boost::optional<DayInfoGlobal> get( instrument_t const& instr ) const
    {
        const_iterator i = std::lower_bound(m_sym_DayInfo_map.begin(), m_sym_DayInfo_map.end(), instr, Comp());
        if( i == m_sym_DayInfo_map.end() || Comp()(instr, *i) )
            return boost::none;
        return *i;
    }

    /// Returns the timeval of this DayInfoGlobalMap.
    const timeval_t& getDate() const { return m_tv; }

    /// Returns the session id of this DayInfoGlobalMap.
    const uint32_t getSessionId() const { return m_session_id; }

    /// Adds a DayInfo to the map.
    void addDayInfo( const DayInfoGlobal& dayInfo ) ;

    size_t size() const;
    bool empty() const;

    //DayInfoGlobalMap is sorted according to this comparator:
    //    - per symid
    //    - per expiry
    //    - per right
    //    - per strike

    struct Comp
    {
        bool operator()(DayInfoGlobal const& l, instrument_t const& r) const
        {
            unsigned const symdl = l.getInstrument().sym.id();
            unsigned const symdr = r.sym.id();
            return (symdl < symdr) ||
                   (symdl == symdr && l.getInstrument().exp < r.exp) ||
                   (symdl == symdr && l.getInstrument().exp == r.exp && l.getInstrument().right < r.right) ||
                   (symdl == symdr && l.getInstrument().exp == r.exp && l.getInstrument().right == r.right && l.getInstrument().strike < r.strike);
        }

        bool operator()(instrument_t const& l, DayInfoGlobal const& r) const
        {
            unsigned const symdr = r.getInstrument().sym.id();
            unsigned const symdl = l.sym.id();
            return (symdl < symdr) ||
                   (symdl == symdr && l.exp < r.getInstrument().exp) ||
                   (symdl == symdr && l.exp == r.getInstrument().exp && l.right < r.getInstrument().right) ||
                   (symdl == symdr && l.exp == r.getInstrument().exp && l.right < r.getInstrument().right && l.strike < r.getInstrument().strike);
        }

        bool operator()(DayInfoGlobal const& l, symbol_t const& r) const
        {
            return l.getInstrument().sym < r;
        }

        bool operator()(symbol_t const& l, DayInfoGlobal const& r) const
        {
            return l < r.getInstrument().sym;
        }
    };

    const_iterator begin() const { return m_sym_DayInfo_map.begin(); }
    const_iterator end()   const { return m_sym_DayInfo_map.end(); }

private:
    timeval_t m_tv;
    uint32_t  m_session_id;
    cont_type m_sym_DayInfo_map;
};


} // namespace bb

#endif // BB_DB_DAYINFO_H

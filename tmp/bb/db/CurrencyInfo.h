#ifndef BB_DB_CURRENCYINFO_H
#define BB_DB_CURRENCYINFO_H

/* Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved. */
#include <bb/core/date.h>
#include <bb/core/currency.h>
#include <bb/core/smart_ptr.h>

namespace bb {

BB_FWD_DECLARE_SHARED_PTR( Environment );

namespace db {

class CurrencyInfo
{
public:
    CurrencyInfo( const bb::EnvironmentPtr& env, currency_t currency
        , date_t asOfDate = date_t::today(), std::string db_profile = "production" );
    currency_t getCurrency() const;
    double getConversionRateTo( currency_t to_currency ) const;
    double getConversionRateFrom( currency_t to_currency ) const;
    date_t asOfDate() const;

private:
    EnvironmentPtr m_env;
    currency_t m_Currency;
    double m_usdRate;
    date_t m_asOfDate;
    std::string m_dbProfile;

};

}
}


#endif // BB_DB_CURRENCYINFO_H

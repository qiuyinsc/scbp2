#ifndef BB_THREADING_LOCKFREE_H
#define BB_THREADING_LOCKFREE_H

/* Contents Copyright 2016 Athena Capital Research LLC. All Rights Reserved. */

namespace bb {
namespace threading {

/// These LockFree fields are only suitable to be used in a single writer-multiple reader
/// environment, since we rely on m_guard value to make sure that we get a clean value
/// while reading value from SharedMemory.


/// We maintain synchronization between the writer and the reader using a guard value.
/// The guard value starts at 2. The writer increments the guard before it starts writing and
/// then increments it one more time when it finishes writing. So the value is back to being even
/// once the write is finished. From the readers perspective, we store the value of the guard when
/// we start reading and then after finishing reading we compare the current guard value
/// to the guard value we saved. If they match and are even we have a clean read.



#define LOCK_FREE_FIELD(type, field)                                \
private:                                                            \
    type field;                                                     \
public:                                                             \
    void set##field(type val) {                                     \
            ++m_guard;                                              \
            field = val;                                            \
            ++m_guard;                                              \
    }                                                               \
    bool get##field(type& retval, uint32_t attempts) const {        \
        while( 0 != attempts ) {                                    \
            int guard = m_guard;                                    \
            retval = field;                                         \
            --attempts;                                             \
            if(guard % 2 == 0 && guard == m_guard) return true;     \
        }                                                           \
        return false;                                               \
    }

#define LOCK_FREE_STRUCT_MEMCPY(type, field)                            \
private:                                                                \
     type field;                                                        \
public:                                                                 \
     void set##field(type &other, size_t copySize )                     \
     {                                                                  \
         ++m_guard;                                                     \
         memcpy(&field, &other, copySize );                             \
         ++m_guard;                                                     \
     }                                                                  \
                                                                        \
     void set##field(type &other)                                       \
     {                                                                  \
         ++m_guard;                                                     \
         memcpy(&field, &other, sizeof(type) );                         \
         ++m_guard;                                                     \
     }                                                                  \
                                                                        \
     bool get##field(type& retval, uint32_t attempts, size_t copySize) const \
     {                                                                  \
         while( 0 != attempts )                                         \
         {                                                              \
             int guard = m_guard;                                       \
             memcpy( &retval, &field, COPY_SIZE );                      \
             --attempts;                                                \
             if(guard % 2 == 0 && guard == m_guard) return true;        \
         }                                                              \
         return false;                                                  \
     }                                                                  \
                                                                        \
     bool get##field(type& retval, uint32_t attempts) const             \
     {                                                                  \
         while( 0 != attempts )                                         \
         {                                                              \
             int guard = m_guard;                                       \
             memcpy( &retval, &field, sizeof(type) );                   \
             --attempts;                                                \
             if(guard % 2 == 0 && guard == m_guard) return true;        \
         }                                                              \
         return false;                                                  \
     }                                                                  \


#define LOCK_FREE_ARRAY_MEMCPY(type, field, size)                       \
private:                                                                \
     boost::array<type, size> field;                                    \
public:                                                                 \
     void set##field(type &other, uint8_t index, size_t copySize = sizeof( type ) ) \
     {                                                                  \
         BB_ASSERT( index <= size - 1 );                                \
         ++m_guard;                                                     \
         memcpy(&field[index], &other, copySize );                      \
         ++m_guard;                                                     \
     }                                                                  \
                                                                        \
     bool get##field(type& retval, uint8_t index, uint32_t attempts, size_t copySize = sizeof(type) ) const \
     {                                                                  \
         BB_ASSERT( index <= size - 1 );                                \
         while( 0 != attempts )                                         \
         {                                                              \
             int guard = m_guard;                                       \
             memcpy( &retval, &field[index], copySize );                \
             --attempts;                                                \
             if(guard % 2 == 0 && guard == m_guard) return true;        \
         }                                                              \
         return false;                                                  \
     }                                                                  \
                                                                        \

// Guarded struct.  Inherit from this struct and add new fields using
// the LOCK_FREE_FIELD macro.  Always call init() before you start using it
// to ensure that the shared memory is properly initialized
struct LockFreeStruct
{
protected:
    volatile mutable int m_guard;
public:
    int getGuard() const {return m_guard;}
    void init() const { m_guard=2; }
    LockFreeStruct() { init(); }
};



}// end namespace threading
} // end namespace bb

#endif // BB_THREADING_LOCKFREE_H

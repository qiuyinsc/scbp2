#ifndef BB_THREADING_THREADPOOL_H
#define BB_THREADING_THREADPOOL_H

/* Contents Copyright 2014 Athena Capital Research LLC. All Rights Reserved. */

#include <bb/core/Log.h>
#include <bb/core/Error.h>
#include <bb/core/timeval.h>
#include <bb/core/smart_ptr.h>
#include <bb/core/LuaConfig.h>
#include <bb/io/CFile.h>
#include <bb/io/ByteSink.h>

#include <tbb/concurrent_queue.h>
#include <tbb/atomic.h>

#include <boost/asio.hpp>
#include <boost/thread.hpp>
#include <boost/cstdint.hpp>
#include <boost/function.hpp>
#include <boost/scoped_ptr.hpp>
#include <boost/noncopyable.hpp>
#include <boost/optional.hpp>
#include <boost/exception/exception.hpp>
#include <boost/enable_shared_from_this.hpp>

#include <tbb/atomic.h>
#include <iostream>
#include <vector>

namespace bb{
namespace threading{

BB_FWD_DECLARE_SHARED_PTR(ThreadPool);
BB_FWD_DECLARE_SHARED_PTR(ThreadPoolManager);

typedef boost::asio::io_service  IoService;
typedef boost::asio::io_service::strand  IoServiceStrand;

BB_DECLARE_SHARED_PTR(IoServiceStrand);

class ThreadPoolConfig;

/* Athena threading:
 *
 * 1) Modelled after boost::asio ( which it in fact uses ), our threading functions give us easy access to
 * a shared or freshly constructed thread pool. In the case of a shared pool, the pool contains 1 thread and
 * when we create one manually it's up to us to specify that, along with optional thread affinity ( pinning ).
 *
 * 2) Once a pool has been created, up next is creating a Producer. A producer lets us post() jobs to the thread
 *    pool that it's connected to. Such a job will be a boost::function<void()> that we have constructed using
 *    boost::bind. There are two kinds of producers:
 * 2a) Parallel - jobs run on any thread in the pool. They run on a first come first serve basis but don't
 *     wait for each other to finish.
 * 2b) Serial - jobs run one after the other, regardless of how many threads are available in the pool.
 *
 * 3) Besides being able to post() jobs manually, it's possible ( and easier ) to post a bound function to a
 *    producer and get a Future back. Futures have the same return type as the bound function and will block when
 *    you try to retrieve it. This makes it easier to do map/reduce like operations ( think large slurm job with
 *    dependencies ) and hides the gory details of threading and synchronisation. Modelled after std::future.
 *    If an exception is thrown in the execution of the bound function, we will store the exception and rethrow
 *    upon retrieving the value. You get a Future by calling getFuture<ReturnType> on a Producer.
 * 3*) The std::future and boost::future implementations are much too kind - they block using a condition variable.
 *    Our implementation also does that ( by default ), but the 'getFuture<ReturnType> takes a second boolean argument.
 *    Setting this argument to true will make the future spin while waiting for the answer to return. This will
 *    help with latency sensitive operations ( and of course, hurt everything else if the computer is overloaded already ).
 *
 * Examples:
 *
 * ThreadPoolPtr pool = ThreadPoolManager::getInstance()->createThreadPool(2);
 * ThreadedProducerPtr producer(pool->getParallelProducer());
 * producer->post(boost::bind(&SomeClass::SomeVoidFunction));
 *
 * 'Void' future ( we don't care about the return value )
 *
 * ThreadPoolPtr pool = ThreadPoolManager::getInstance()->createThreadPool(2);
 * ThreadedProducerPtr producer(pool->getParallelProducer());
 * Future<void> future = producer->getFuture<void>(boost::bind(&SomeClass::SomeVoidFunction));
 * future.get(); // block until the function has returned
 *
 * 'ReturnType' future ( we *do* care about the return value )
 *
 * ThreadPoolPtr pool = ThreadPoolManager::getInstance()->createThreadPool(2);
 * ThreadedProducerPtr producer(pool->getParallelProducer());
 * Future<int> future = producer->getFuture<int>(boost::bind(&SomeClass::SomeFunctionThatReturnsAnInt));
 * int a = future; // block until the function has returned and we get the value
 *
 */


/**
 * The ThreadPoolManager is used to keep a track of all ThreadPools that are being used
 * within a single program.
 *
 * Since there is no way to 'force' a threadpool to empty its pending task queue. It's better
 * to wait until all jobs inside have been correctly processed.
 **/
class ThreadPoolManager
{
public:
    enum ProcessingMode
    {
        POLLING = 0,
        BLOCKING = 1
    };

    typedef std::set<uint32_t> cpuset_t;

    /**
     * Return a globally shared ThreadPool.
     **/
    ThreadPoolPtr getSharedThreadPool();

    /**
     * The create function receives as input the number of threads
     * in the pool
     *  @param numThreads - the number of threads in the thread pool
     *  @param cpus       - a list of CPUs on which the threads can run
     */
    ThreadPoolPtr createThreadPool(
        const unsigned int numThreads,
        const cpuset_t& cpus=cpuset_t(),
        const ProcessingMode mode = BLOCKING );
    ThreadPoolPtr createThreadPool( const ThreadPoolConfig& config );

    ~ThreadPoolManager();

    /**
     * Singleton method to return the same instance of ThreadPoolManager.
     **/
    static ThreadPoolManagerPtr getInstance();

    void resetAll();
private:
    ThreadPoolManager();

    // Static instance for ThreadPoolManager.
    static ThreadPoolManagerPtr s_instance;

    // Ptr for the shared ThreadPool object.
    ThreadPoolPtr m_shared_threadpool;

    // List of ThreadPools that we have created.
    // We need to wait for them to finish their work before we can safely exit.
    std::vector<ThreadPoolPtr> m_threadpools;
};

BB_DECLARE_SHARED_PTR(ThreadPoolManager);


class ThreadPoolConfig: public LuaConfig<ThreadPoolConfig>
{
public:
    typedef ThreadPoolManager::cpuset_t cpuset_t;
    typedef ThreadPoolManager::ProcessingMode ProcessingMode;
    ThreadPoolConfig();
    ProcessingMode getMode() const;
    static void describe();
    uint32_t     m_threadnum;
    cpuset_t     m_cpus;
    std::string  m_mode;
};
// Stream operator to convert the config into human readable form
std::ostream& operator <<( std::ostream& out, const ThreadPoolConfig& config );


BB_DECLARE_SHARED_PTR(ThreadPool);
class IThreadedProducer
{
    public:
        typedef boost::function<void()> ProducerCallback;
        virtual ~IThreadedProducer() {}
        virtual void post(ProducerCallback cb) = 0;
};
class ThreadedProducer;

namespace details
{
    // We 'need' this dummy threaded producer for dummy futures where we supply the value
    // in the constructor. One reason for such a dummy future is to allow daisy-chaining of
    // futures, where some values are yet to be calculated and some are readily available.
    class DummyThreadedProducer
        : public IThreadedProducer
    {
        public:
            virtual ~DummyThreadedProducer() {}
            void post(ProducerCallback cb) override {}
    };
     static DummyThreadedProducer dummy;
}

// Modelled after STD's std::future ( hence the confusing name ), the Future class provides access
// to a value that is yet to be retrieved in the future. This future value is retrieved in the
// background by calling the supplied 'boost::function<T()>' on the specified ThreadedProducer.
// While we wait for the value to be retrieved, execution on the calling thread will continue.
// It is however when we try to get the value ( 'T const & get()' ) where we block if/while the
// value is not yet available.
template<typename ReturnType>
class Future
{
    public:
        typedef boost::function<ReturnType(void)> Callback;

        // Returns the return value of the bound function.
        // Blocks in the meantime.
        ReturnType const & get() const
        {
            return m_impl->get(true);
        }

        // Returns the return value of the bound function.
        // Blocks in the meantime.
        operator ReturnType const & () const
        {
            return get();
        }

        // Construct a dummy Future<ReturnType> - this is an object that looks and acts
        // like a Future<ReturnType>, but you've actually already supplied the value.
        static Future<ReturnType> getDummy(ReturnType const & value)
        {
            return Future<ReturnType>(value);
        }

    private:
        friend class ThreadedProducer;
        // Only the (I)ThreadedProducer will create new Future objects.
        template<typename ProducerType>
        Future ( ProducerType & producer
                , Callback callback
                , const bool spinning )
            : m_impl ( new FutureImpl ( producer, callback, spinning ) )
        {
            m_impl->post();
        }

        // Support for the 'dummy' future which will always be 'done' and have the default value
        Future (ReturnType const & value)
            : m_impl ( new FutureImpl(value) )
        {
        }

        // We want to make these Futures copy constructable so we don't have to return a shared_ptr.
        // Then again, we can't make them copy constructable, because the post() to the producer will have happened already.
        // The easiest way to combat this is by keeping the 'actual' state in a shared_ptr and hide this dirty little secret
        // in the implementation.
        class FutureImpl : private boost::noncopyable
                    , public::boost::enable_shared_from_this<FutureImpl>
        {
            public:
                friend class Future<ReturnType>;
                friend class ThreadedProducer;
                typedef Future<ReturnType>::Callback Callback;
                typedef IThreadedProducer::ProducerCallback ProducerCallback;

                // Returns the return value of the callback, once it finishes.
                // Blocks while the return value isn't available yet.
                ReturnType const & get( const bool rethrow_exception ) const
                {
                    if ( !m_done )
                    {
                        if ( m_spinning )
                        {
                            while( !m_done ) { /* spin, spinnin' around. over and and over again.. */ }
                        }
                        else
                        {
                            boost::mutex::scoped_lock lock(m_mtx);
                            while ( !m_done ) // not if as that won't help with spurious wakeups
                            {
                                m_cond.wait(lock);
                            }
                        }
                    }
                    if ( unlikely ( m_exception && rethrow_exception ) )
                    {
                        boost::rethrow_exception ( m_exception.get() );
                    }
                    return m_value;
                }

                // Reduce the scope for unpleasantness - when the Future goes out of scope,
                // we will actually 'get' and potentially block while destructing.
                // This is not very performance friendly, so potentially we could get rid of this step.
                ~FutureImpl()
                {
                    get(false);
                }

            protected:
                template<typename ProducerType>
                FutureImpl( ProducerType & producer
                    , Callback callback
                    , const bool spinning )
                    : m_producer ( producer )
                    , m_callback ( callback )
                    , m_spinning ( spinning )
                {
                    m_done = false;
                }

                void post()
                {
                    // There should be no reason for using shared_from_this. After all the destructor
                    // already calls get(), which would make sure the FutureImpl doesn't go out of scope
                    // while we wait for the answer to come in.
                    // However - in practice this isn't the case.
                    m_producer.post( boost::bind(&Future<ReturnType>::FutureImpl::run, this->shared_from_this() ) ) ;
                }

            private:

                FutureImpl( ReturnType const & val )
                    : m_producer ( details::dummy )
                    , m_spinning ( false )
                    , m_value ( val )
                {
                    m_done = true;
                }

                IThreadedProducer & m_producer;
                Callback m_callback;
                mutable boost::mutex m_mtx;
                mutable boost::condition_variable m_cond;
                boost::optional<boost::exception_ptr> m_exception;
                tbb::atomic<bool> m_done;
                const bool m_spinning;
                ReturnType m_value;

                // The heart of the operation:
                // - lock if not spinning
                // - retrieve the value 'ReturnType'
                // - if not spinning - notify so the 'get' function will stop blocking
                // - if spinning - just toggle the 'm_done' value
                void run()
                {
                    if ( m_spinning )
                    {
                        try
                        {
                            m_value = m_callback();
                        }
                        catch ( std::exception & ex )
                        {
                            m_exception = boost::current_exception();
                        }
                        m_done = true;
                    }
                    else
                    {
                        boost::mutex::scoped_lock lock(m_mtx);
                        try
                        {
                            m_value = m_callback();
                        }
                        catch ( std::exception & ex )
                        {
                            m_exception = boost::current_exception();
                        }

                        m_done = true;
                        m_cond.notify_all();
                    }
                }
        };
        mutable boost::shared_ptr < FutureImpl > m_impl;
};

// Void is special - you can't have a 'void m_value'.
// In this case, we don't expect nor store a return value from the callback
// and don't return anything in get() const.
template<>
class Future<void>
{
   public:
        friend class ThreadedProducer;
        typedef boost::function<void(void)> Callback;

        void get() const
        {
            m_impl->get(true);
        }

        // This is here for completness sake - see Future<ReturnType>::getDummy for a better
        // reason to have this function.
        static Future<void> getDummy()
        {
            return Future<void>();
        }

    private:
        // Only the (I)ThreadedProducer will create new Future objects.
        template<typename ProducerType>
        Future<void> ( ProducerType & producer
            , Callback callback
            , const bool spinning )
            : m_impl ( new FutureImpl ( producer, callback, spinning ) )
        {
            m_impl->post();
        }

        // Support for the 'dummy' future<void> which will always be 'done'.
        Future<void> ()
            : m_impl ( new FutureImpl() )
        {
        }

        // We want to make these Futures copy constructable so we don't have to return a shared_ptr.
        // Then again, we can't make them copy constructable, because the post() to the producer will have happened already.
        // The easiest way to combat this is by keeping the 'actual' state in a shared_ptr and hide this dirty little secret
        // in the implementation.
        class FutureImpl : private boost::noncopyable
                    , public::boost::enable_shared_from_this<FutureImpl>
        {
            public:
                friend class Future<void>;
                friend class ThreadedProducer;
                typedef IThreadedProducer::ProducerCallback ProducerCallback;

                // Returns once the callback has returned, blocking in the meantime.
                // Think of this as 'joining' a thread.
                void get( const bool rethrow_exception ) const
                {
                    if ( !m_done )
                    {
                        if ( m_spinning )
                        {
                            while( !m_done ) { /* spin, spinnin' around. over and and over again.. */ }
                        }
                        else
                        {
                            boost::unique_lock<boost::mutex> lock(m_mtx);
                            while ( !m_done ) // not if as that won't help with spurious wakeups
                            {
                                m_cond.wait(lock);
                            }
                        }
                    }
                    if ( unlikely ( m_exception && rethrow_exception ) )
                    {
                        boost::rethrow_exception ( m_exception.get() );
                    }
                }

                // Reduce the scope for unpleasantness - when the Future goes out of scope,
                // we will actually 'get' and potentially block while destructing.
                // This is not very performance friendly, so potentially we could get rid of this step.
                ~FutureImpl()
                {
                    get(false);
                }

            protected:
                template<typename ProducerType>
                FutureImpl( ProducerType & producer
                    , Callback callback
                    , const bool spinning )
                    : m_producer ( producer )
                    , m_callback ( callback )
                    , m_spinning ( spinning )
                {
                    m_done = false;
                }

                void post()
                {
                    // There should be no reason for using shared_from_this. After all the destructor
                    // already calls get(), which would make sure the FutureImpl doesn't go out of scope
                    // while we wait for the answer to come in.
                    // However - in practice this isn't the case.
                    m_producer.post( boost::bind(&Future<void>::FutureImpl::run, this->shared_from_this()) );
                }

            private:
                FutureImpl()
                    : m_producer ( details::dummy )
                    , m_spinning ( false )
                {
                    m_done = true;
                }

                IThreadedProducer & m_producer;
                Callback m_callback;
                mutable boost::mutex m_mtx;
                mutable boost::condition_variable m_cond;
                boost::optional<boost::exception_ptr> m_exception;
                tbb::atomic<bool> m_done;
                const bool m_spinning;

                // The heart of the operation:
                // - lock if not spinning
                // - call the callback function
                // - if not spinning - notify so the 'get' function will stop blocking
                // - if spinning - just toggle the 'm_done' value
                void run()
                {
                    if ( m_spinning )
                    {
                        try
                        {
                            m_callback();
                        }
                        catch ( std::exception & ex )
                        {
                            m_exception = boost::current_exception();
                        }
                        m_done = true;
                    }
                    else
                    {
                        boost::unique_lock<boost::mutex> lock(m_mtx);
                        try
                        {
                            m_callback();
                        }
                        catch ( std::exception & ex )
                        {
                            m_exception = boost::current_exception();
                        }
                        m_done = true;
                        m_cond.notify_all();
                    }
                }
        };
        mutable boost::shared_ptr < FutureImpl > m_impl;
};

BB_FWD_DECLARE_SHARED_PTR(ThreadPool);

/**
 * The ThreadedProducer implements the base functionality used to interact
 * with a ThreadPool object.  To add a job to the thread pool queue use the post function.
 * passing in as a parameter the function to be executed by thread.  Once started by a thread
 * the posted function will run until completion so implementers should take care not to loop
 * forever or they will monopolize the thread.
 */
class ThreadedProducer : public IThreadedProducer
{
public:
    typedef ThreadPoolManager::ProcessingMode ProcessingMode;
    virtual ~ThreadedProducer();

    void consumeCallbackWrapper(ProducerCallback & cb);

    void post(ProducerCallback cb) override;

    // We provide a function to be called on the ThreadedProducer.
    // This function has to return a 'T'. The postFuture function will not
    // block while we retrieve the value, but its internal 'get()' function will,
    // while we wait for the passed in function to be scheduled and processed in
    // the background, by the ThreadedProducer.
    //
    // @param callback - the bound function to call back
    // @param spinning - set to true to make spin furiously why we don't have our answer back
    template<typename ReturnType>
    Future<ReturnType> getFuture(
        boost::function<ReturnType(void)> callback,
        const ProcessingMode mode = ThreadPoolManager::BLOCKING )
    {
        return Future<ReturnType>(*this
            , callback
            , mode == ThreadPoolManager::POLLING);
    }

    /**
     * Initialization function.
     *  @param threadPool - a pointer to the ThreadPool instance on which
     *                      all jobs will be scheduled.
     */
    virtual void connectToThreadpool(ThreadPoolPtr threadPool);

public: // TODO - can be protected once we fix callers
    /**
     * Constructors, only the ThreadPool itself call these.
     */
    ThreadedProducer();
    ThreadedProducer(ThreadPoolPtr threadPool);

protected:
    ThreadPoolPtr                      m_threadpool;

    /**
     * The following raw pointer variables refers members in threadpool.
     * They will be released by ThreadPool.
     */
    IoService*                         m_ioService;

    void initConnection(ThreadPoolPtr threadpool);

private:
    virtual void postImpl(ProducerCallback & cb);
};

/**
 * The SerializedThreadedProducer implements the base functionality used to interact
 * with a ThreadPool object.  The task to be performed is defined in the function
 * consumeCallback.  To add a job to the thread pool queue use the post function.
 * All tasks produced by an object of this class are serialized.  This is accomplished
 * through the use of a strand class object.
 */
class SerializedThreadedProducer : public ThreadedProducer{
public:
    typedef boost::function<void()> ProducerCallback;

    virtual ~SerializedThreadedProducer();

    virtual void connectToThreadpool(ThreadPoolPtr threadPool);

public: // TODO - can be protected once we fix callers
    /**
     * Constructors, only the ThreadPool itself call these.
     */
    SerializedThreadedProducer();
    SerializedThreadedProducer(ThreadPoolPtr threadPool);
private:
    virtual void postImpl(ProducerCallback & cb);

    IoServiceStrandPtr       m_strand;
};

BB_DECLARE_SHARED_PTR(ThreadedProducer);
BB_DECLARE_SHARED_PTR(SerializedThreadedProducer);

/**
 * The ThreadPool combines the Boost thread group with the Asynchronous IO (ASIO)
 * service class to produce a thread pool.  The class is designed to allow the main
 * thread to off-load work to other threads.  It also allows multiple producers to share
 * a group of threads.
 */
class ThreadPool : public boost::enable_shared_from_this<ThreadPool> {
public:
    typedef ThreadPoolManager::cpuset_t cpuset_t;
    typedef ThreadPoolManager::ProcessingMode ProcessingMode;
    friend class ThreadPoolManager;
    // for access to increase/decrease ProducerCounter
    friend class ThreadedProducer;
    friend class SerializedThreadedProducer;
private:
    /**
     * The constructor receives as input the number of threads
     * in the pool
     *  @param numThreads - the number of threads in the thread pool
     *  @param cpus       - a list of CPUs on which the threads can run
     */
    ThreadPool(
        const unsigned int numThreads,
        const cpuset_t& cpus=cpuset_t(),
        const ProcessingMode mode = ThreadPoolManager::BLOCKING );
    ThreadPool( const ThreadPoolConfig& config );

    /**
     * This function is used to block the root thread until the threadpool finishes all the
     * scheduled jobs for this producer.
     */
    void waitForJobCompletion();
    /**
     * Poll for new work rather than block - will make the cpu usage jump to 100% for each
     * thread we add to this ThreadPool. Will of course also reduce latency.
     */
    void poll();

    uint32_t getThreadNum();

public:
    /**
     * Return the thread pools io service.  The IO Service is used to
     * post work to be executed by the threads. Jobs posted through the io service
     * may be executed in parallel.  For jobs that must be executed serially, such as
     * writes to a common file the io_service must be wrapped in a strand
     */
    IoService*  getIoService();
    /**
     * Return a strand for this thread pool.  A strand is similar to a service
     * except that all jobs posted through this strand will be serialized.
     */
    IoServiceStrandPtr getIoServiceStrand();

    ~ThreadPool();

    /**
     * Returns a producer which will process post()'ed jobs or futures in order, but will
     * not have one job wait for another.
     */
    ThreadedProducerPtr getParallelProducer();

    /**
     * Returns a producer which will process post()'ed jobs or futures in order, but only
     * at a time. This is for instance useful when writing to a log file, where you need
     * the order of events to stay the same.
     */
    SerializedThreadedProducerPtr getSerialProducer();

private:
    void init( const unsigned int numThreads, const cpuset_t& cpus, const ProcessingMode mode );

    boost::thread_group                                m_threadpool;
    boost::asio::io_service                            m_ioService;
    boost::scoped_ptr<boost::asio::io_service::work>   m_work;
    uint32_t                                           m_threadNum;
    bool                                               m_stopping;

    // A count of how many producers are connected to the threadpool.
    tbb::atomic<uint32_t>                              m_producer_count;

    // A pair of functions to manipulate the producer counter.
    void increaseProducerCounter();
    void decreaseProducerCounter();
};

} // end namespace threading
} // end namespace bb
#endif // BB_THREADING_THREADPOOL_H

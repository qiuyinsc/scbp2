#ifndef BB_THREADING_LOCK_H
#define BB_THREADING_LOCK_H

/* Contents Copyright 2015 Athena Capital Research LLC. All Rights Reserved. */

#include <pthread.h>

#include <bb/core/Error.h>
#include <bb/core/smart_ptr.h>

#include <boost/noncopyable.hpp>

namespace bb {
namespace threading {

class Lock : public boost::noncopyable
{
public:
    Lock(){};
    virtual ~Lock(){};

    virtual bool lock() = 0;
    virtual bool unlock() = 0;
};
BB_DECLARE_SHARED_PTR(Lock);

class Spinlock: public Lock
{
public:
    Spinlock();
    virtual ~Spinlock();

    // try to obtain the lock
    virtual bool lock();

    // release the lock
    virtual bool unlock();

private:
    pthread_spinlock_t      m_lock;
};
BB_DECLARE_SHARED_PTR(Spinlock);


///
/// LockGuard - obtains the lock when created and
/// releases it when destryoed.
///
class LockGuard
{
public:
    LockGuard( LockPtr lock)
        : m_spLock(lock)
    {
        if( !m_spLock->lock() ){
            BB_THROW_ERROR_SSX( "failed to acquire spinlock" );
        }
    };

    ~LockGuard()
    {
        if( !m_spLock->unlock( ) ){
            BB_THROW_ERROR_SSX( "failed to release spinlock" );
        }
    };
private:
    LockPtr    m_spLock;
};

} // end threading

} // end bb

#endif // BB_THREADING_LOCK_H

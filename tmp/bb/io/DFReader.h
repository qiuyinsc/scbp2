#ifndef BB_IO_DFREADER_H
#define BB_IO_DFREADER_H

/* Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved. */

#include <boost/format/format_fwd.hpp>
#include <boost/noncopyable.hpp>

#include <bb/core/compat.h>
#include <bb/core/MsgBuffer.h>
#include <bb/core/MStream.h>
#include <bb/io/ByteSourceFactory.h>


namespace bb {

BB_FWD_DECLARE_SHARED_PTR( ByteSource );

/// Converts a stream of bytes to a stream of messages.
class DFReader : public HistMStream, public boost::noncopyable
{
public:
    /// Takes in a valid input stream.  Can throw an HistMStream::Exception.
    DFReader( const bb::ByteSourcePtr inp, const bb::ByteSourceFactoryPtr byteSourceFactory = ByteSourceFactoryPtr() );
    virtual ~DFReader();

    virtual const Msg* next();

protected:
    DFReader( const ByteSourceFactoryPtr byteSourceFactory = ByteSourceFactoryPtr() );

    /// the return value when we get an on EOF, can be overridden to combine multiple
    /// files into one.
    virtual const Msg* onEOF()
    {
        return nullptr;
    }

    virtual void onError( boost::format& f );

    ByteSourcePtr m_inp;
    MsgBuffer m_msgBuf;
    size_t m_bytesRead;
    bool m_bDisableEOFHandling;
    ByteSourceFactoryPtr m_byteSourceFactory;
};
BB_DECLARE_SHARED_PTR( DFReader );


} // namespace bb

#endif // BB_IO_DFREADER_H

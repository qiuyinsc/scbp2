#ifndef BB_IO_SYMDATAFILE_H
#define BB_IO_SYMDATAFILE_H

/* Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved. */

#include <string>

#include <bb/core/gdate.h>
#include <bb/core/timeval.h>

#include <bb/io/DFSearchReader.h>
#include <bb/io/ByteSourceFactory.h>


namespace bb {


/** This class reads in a datafile for a given source & sym on a date range.
 *  and exports it as a HistDFStream. Internally, it computes a filename
 *  for each day in the range, and then lumps all those
 *  datafiles together into one stream.
 *
 *  This class inherits from DFReader (rather than having it as a member)
 *  for efficiency -- this means SymDataFile::next() entails one virtual
 *  function call instead of 2.
 *  */
class SymDataFile
    : public DFSearchReader
{
public:
    /// Indicates a problem with the data file (usually missing data file or broken file format).
    struct Exception : virtual SingleDFStream::Exception
    {
        Exception( const std::string& err,
                   source_t source,
                   symbol_t sym,
                   const gdate_t& date,
                   const std::string& filename );

        ~Exception() throw()
        {
        }

        virtual const char* what() const throw()
        {
            return m_fullErrBuf.c_str();
        }

        source_t m_source;
        symbol_t m_sym;
        gdate_t m_date;
    };

    /// Thrown when a data file doesn't exist in the filesystem.
    struct MissingDataFile
        : virtual public SymDataFile::Exception
        , virtual public SingleDFStream::MissingDataFile
    {
        MissingDataFile( const std::string& err,
                         source_t source,
                         symbol_t sym,
                         const gdate_t& date,
                         const std::string& filename )
            : SingleDFStream::Exception( err, filename )
            , SymDataFile::Exception( err, source, sym, date, filename )
            , HistMStream::MissingData( err )
            , SingleDFStream::MissingDataFile( err, filename )
        {
        }

        ~MissingDataFile() throw()
        {
        }
    };

    typedef boost::function<bb::gdate_t( symbol_t sym, bb::gdate_t )> NextDateFunc;
    typedef boost::function<bb::gdate_t( symbol_t sym, bb::gdate_t )> PrevDateFunc;
    typedef boost::function<bb::gdate_t( symbol_t sym, bb::gdate_t )> ShiftDateFunc;
    typedef boost::function<bool( symbol_t sym, const bb::gdate_t )> IsTradingDateFunc;

    static bb::gdate_t defaultGetNextDate( symbol_t, const bb::gdate_t curdate )
    {
        return curdate + boost::gregorian::days( 1 );
    }

    static bb::gdate_t defaultGetPrevDate( symbol_t, const bb::gdate_t curdate )
    {
        return curdate - boost::gregorian::days( 1 );
    }

    static bool defaultIsTradingDate( symbol_t, const bb::gdate_t date )
    {
        const boost::gregorian::greg_weekday dow = date.day_of_week();
        const bool rval = (dow != boost::gregorian::Sunday) && (dow != boost::gregorian::Saturday);
        return rval;
    }

    /// nextDateFunc takes in a date and returns the next valid
    /// date for this symbol
    SymDataFile( source_t source
                 , symbol_t sym
                 , const timeval_t& starttv
                 , const timeval_t& endtv
                 , const NextDateFunc ndf = &defaultGetNextDate
                 , const PrevDateFunc pdf = &defaultGetPrevDate
                 , const IsTradingDateFunc isTradeDate = &defaultIsTradingDate
                 , const ByteSourceFactoryPtr byteSourceFactory = ByteSourceFactoryPtr() );

    source_t getSource() const
    {
        return m_source;
    }

    symbol_t getSymbol() const
    {
        return m_sym;
    }

    bb::gdate_t getCurDate() const
    {
        return m_curDate;
    }

    /// Returns a tuple of <filename, isGz> for the file which corresponds to this source/sym on this date.
    /// Will access the filesystem to determine isGz.
    static boost::tuple<std::string, bool> getPath( source_t source, symbol_t sym, const bb::gdate_t& date );

    /// Returns the path this data file will have, without extension and prefix path.
    /// i.e. 2008/04/04/01/datafile.16777994
    static std::string getRelativePath( source_t source, symbol_t sym, const bb::gdate_t& date );

    const Msg* next();

protected:
    virtual const Msg* onEOF();
    virtual void onError( boost::format& f );

    static FileStreamInfo openSourceFile( const source_t& source, symbol_t sym, const gdate_t& date );

    gdate_t shift_date( const symbol_t& sym,
                        const source_t& source,
                        const timeval_t& tv,
                        PrevDateFunc prev_date_func,
                        NextDateFunc next_date_func,
                        IsTradingDateFunc is_trading_date_func );

    source_t m_source;
    symbol_t m_sym;
    timeval_t m_search;
    gdate_t m_curDate, m_endDate;
    NextDateFunc m_nextDateFunc;
    IsTradingDateFunc m_isTradingDateFunc;
    const timeval_t m_starttv, m_endtv;
    bool m_bFoundMsg;
};

BB_DECLARE_SHARED_PTR( SymDataFile );

} // namespace bb

#endif // BB_IO_SYMDATAFILE_H

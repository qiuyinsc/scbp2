#ifndef BB_IO_RECVMMSG_GLIBC_H
#define BB_IO_RECVMMSG_GLIBC_H

/* Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved. */


#if !(defined(WIN32) || defined(WIN64))
#define BB_HAS_RECVMMSG 1

/* recvmmsg alone requires 2.6.32-22acr2 or later at runtime */
/* MSG_WAITFORONE requires 2.6.32-24acr2 or later at runtime */

#include <features.h>
#if __GLIBC_PREREQ(2,12)
#include <linux/socket.h>
#else
#include <unistd.h>
#include <sys/socket.h>//definition of struct msghdr
/* our glibc is too old (< 2.11), so we hacked this together */

#ifdef MSG_WAITFORONE
#error MSG_WAITFORONE was defined already - this should not happen
#endif

#define MSG_WAITFORONE 0x10000 /* recvmmsg(): block until 1+ packets avail */

__BEGIN_DECLS

struct mmsghdr
{
    struct msghdr msg_hdr;     /* Actual message header.  */
    unsigned int msg_len;      /* Number of received bytes for the entry.  */
};

inline int recvmmsg (int fd, struct mmsghdr *vmessages,
        unsigned int vlen, int flags, const struct timespec *tmo)
{
    return syscall(299, fd, vmessages, vlen, flags, tmo);
}

__END_DECLS

#endif /* __GLIBC_PREREQ */
#endif /* !(WIN32 || WIN64) */

#if !defined(BB_HAS_RECVMMSG)

// Provide a dummy implementation that should compile on any
// platform. It will return -1 and set errno to ENOSYS.

struct mmsghdr {};
int recvmmsg(
    int, struct mmsghdr*,
    unsigned int, int, const void*);

#endif

#endif // BB_IO_RECVMMSG_GLIBC_H

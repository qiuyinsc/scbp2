#ifndef BB_IO_NONBLOCKINGBYTESINK_H
#define BB_IO_NONBLOCKINGBYTESINK_H

/* Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved. */


#include <boost/cstdint.hpp>
#include <boost/thread.hpp>
#include <boost/thread/condition.hpp>
#include <tbb/concurrent_queue.h>
#include <bb/core/smart_ptr.h>
#include <bb/core/timeval.h>
#include <bb/io/ByteSink.h>
#include <bb/threading/ThreadPool.h>

#include <boost/enable_shared_from_this.hpp>

namespace bb {
class FDSet;

BB_FWD_DECLARE_SHARED_PTR(Timer);
BB_FWD_DECLARE_SHARED_PTR( NonBlockingByteSink );
/// A NonBlockingByteSink wraps another ByteSink and buffers data to it.
/// Calls to NonBlockingByteSink::write will not block; it'll be sent to
/// another thread which sends to the wrapped ByteSink.
///
/// The threads communicate by a fixed-size queue of buffers; all buffers
/// are preallocated. The write() call blocks if the buffer queue is exhausted.
///
/// Note that the methods of this class are NOT threadsafe.
/// The consumer is internally threadsafe, but you cannot write to this
/// from multiple threads.
class NonBlockingByteSink
    : public ByteSink
    , public threading::SerializedThreadedProducer
    , public boost::enable_shared_from_this<NonBlockingByteSink>
{
public:
    /// @param buffer_size   The size of a data buffer.
    /// @param max_buffers   The maximal number of buffers before a NonBlockingByteSink actually blocks.
    /// @param filled_percentage  How filled (%-wise) a buffer must be to be pushed onto the queue.
    static NonBlockingByteSinkPtr create(
        ByteSinkPtr spByteSink,
        FDSet& fd_set,
        const size_t buffer_size, const size_t max_buffers,
        float flushTime = 10.0, double filled_percentage = 1.0 );

    static NonBlockingByteSinkPtr create(
        ByteSinkPtr spByteSink,
        FDSet& fd_set,
        threading::ThreadPoolPtr threadPool,
        const size_t buffer_size, const size_t max_buffers,
        float flushTime = 10.0, double filled_percentage = 1.0 );
public:

    /// Destructor
    virtual ~NonBlockingByteSink();

    /// NonBlockingByteSink does not support flush().
    virtual void flush();

    /// returns id the byte sink is flushable
    virtual bool isFlushable();

    /// Sets whether verbose logging occurs.
    void setVerbose( bool verbose )
    {   m_verbose = verbose; }

    /// Returns true if we're in verbose mode.
    bool getVerbose() const
    {   return m_verbose; }

    void initCallback();

protected:
    // Copy buf into buffer -- push it into the queue once it is sufficiently filled
    virtual size_t ByteSink_write(const void *buf, size_t size);

private:
    /// @param buffer_size   The size of a data buffer.
    /// @param max_buffers   The maximal number of buffers before a NonBlockingByteSink actually blocks.
    /// @param filled_percentage  How filled (%-wise) a buffer must be to be pushed onto the queue.
    NonBlockingByteSink(
        ByteSinkPtr spByteSink,
        FDSet& fd_set,
        const size_t buffer_size, const size_t max_buffers,
        float flush_time = 10.0, double filled_percentage = 1.0 );

    NonBlockingByteSink(
        ByteSinkPtr spByteSink,
        FDSet& fd_set,
        threading::ThreadPoolPtr threadPool,
        const size_t buffer_size, const size_t max_buffers,
        float flushTime = 10.0, double filled_percentage = 1.0 );

    void onFlushTimeout();
    void commitCurrentBuffer();

    // Thread task, pulls buffers from the queue and writes it to the wrapped ByteSink
    void consume();

    void initialize();

    class Buffer;
    BB_DECLARE_SHARED_PTR(Buffer);

    typedef tbb::concurrent_bounded_queue<BufferPtr>  MsgQueue;

private:
    ByteSinkPtr                 m_spByteSink;

    TimerPtr                    m_flush_timer;
    bool                        m_flush_timer_armed;
    timeval_t                   m_flush_interval; /*seconds*/

    const size_t                m_buffer_size;
    const size_t                m_fill_line;
    const size_t                m_max_buffers;
    bool                        m_verbose;

    shared_ptr<boost::thread>   m_spThread;
    volatile bool               m_terminated;
    volatile bool               m_write_failed;

    // A buffer which isn't full yet, where our current writes() are going to.
    BufferPtr                   m_current_buffer;
    // queue of spare Buffers (those that have no data and can be reused).
    MsgQueue                    m_empty_queue;
    // queue of full Buffers (will be written to m_spByteSink by consume()
    // then put back on the empty queue).
    MsgQueue                    m_full_queue;

    // stats
    uint64_t                    m_num_pushes, m_num_pops, m_num_blocking_writes;
    size_t                      m_max_backlog_seen;
    threading::ThreadPoolPtr    m_threadpool;
};


} // namespace bb

#endif // BB_IO_NONBLOCKINGBYTESINK_H

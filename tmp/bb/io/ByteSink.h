#ifndef BB_IO_BYTESINK_H
#define BB_IO_BYTESINK_H

/* Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved. */

#include <string>
#include <iosfwd>

#include <boost/optional/optional.hpp>

#include <bb/core/smart_ptr.h>

namespace bb {

class ByteSink {
public:
    virtual ~ByteSink() { }

    /// Writes a block of data, returning the size actually written.
    /// Never writes 0 bytes unless the buffer is full and it's non-blocking.
    /// Throws an exception on write failure.
    size_t write(const void *buf, size_t size);

    /// Calls write repeatedly until the whole block is written.
    /// Throws an exception on write failure.
    void writeN(const void *buf, size_t size);

    /// Flushes the ByteSink.
    /// Throws an exception on flush failure.
    virtual void flush() = 0;

    virtual bool isFlushable();

protected:
    /// Pure virtual method which does the actual work of ByteSink writing.
    /// Implementors must override this method, rather than ByteSink::write.
    /// Cannot return 0; size_t is unsigned.  Throws an exception on write failure.
    virtual size_t ByteSink_write(const void *buf, size_t size) = 0;
};
BB_DECLARE_SHARED_PTR(ByteSink);


class OStreamByteSink : public ByteSink {
public:
    /// A ByteSink wrapper for std::ostream.
    /// Activates exceptions on the ostream.
    OStreamByteSink(std::ostream &);

    virtual void flush();

protected:
    virtual size_t ByteSink_write(const void *buf, size_t size);

    std::ostream &m_ostr;
};
BB_DECLARE_SHARED_PTR(OStreamByteSink);

/// A ByteSink that writes no data
class NoOpByteSink : public bb::ByteSink
{
public:
    NoOpByteSink() : ByteSink() {}
    virtual void flush();
protected:
    virtual size_t ByteSink_write(const void *buf, size_t size);
};

} // namespace bb

#endif // BB_IO_BYTESINK_H

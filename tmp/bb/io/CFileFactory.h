#ifndef BB_IO_CFILEFACTORY_H
#define BB_IO_CFILEFACTORY_H

/* Contents Copyright 2014 Athena Capital Research LLC. All Rights Reserved. */


#include <bb/core/LuaConfig.h>
#include <bb/io/CFile.h>

namespace bb {

class CFileConfig: public LuaConfig<CFileConfig>
{
public:
    friend class CFileFactory;

    enum FileType{
        kBuffered,
        kUnbuffered,
        kMmapped
    };

    CFileConfig();
    CFileConfig(std::string fileType, std::string fileMode,
                CFile::Flags flags, size_t bufferSize = 0);
    CFileConfig(std::string fileType, std::string fileMode,
                CFile::Flags flags, size_t preallocSize,
                size_t incrementSize, bool truncate, bool flockOnOpen);

    // setter functions
    CFileConfig &setType(FileType);
    CFileConfig &setMode(std::string mode);
    CFileConfig &setFlags(CFile::Flags flags);
    CFileConfig &setBufferSize(size_t size);
    CFileConfig &setPreallocatedSize(size_t size);
    CFileConfig &setIncrementSize(size_t size);
    CFileConfig &setTruncateOnClose(bool truncate);
    CFileConfig &setFlockOnOpen(bool isToLock);

    // getter functions
    FileType                      getType() const;
    std::string                   getTypeStr() const;
    std::string                   getMode() const;
    CFile::Flags                  getFlags() const;
    size_t                        getBufferSize() const;
    size_t                        getPreallocatedSize() const;
    size_t                        getIncrementSize() const;
    bool                          getTruncateOnClose() const;
    bool                          getFlockOnOpen() const;

    static void describe();


private:
    std::string                       m_type;
    std::string                       m_mode;
    CFile::Flags                      m_flags;
    size_t                            m_bufferSize;
    size_t                            m_preallocSize;
    size_t                            m_incrementSize;
    bool                              m_truncateOnClose;
    bool                              m_flockOnOpen;
};

// Stream operator to convert the config into human readable form
std::ostream& operator <<( std::ostream& out, const CFileConfig& config );

class CFileFactory {
public:
    CFileFactory( const CFileConfig &config );
    CFileFactory( const luabind::object& lbo );

    void setConfig(CFileConfig &config);
    CFileConfig& getConfig();

    ///
    /// Create using the provided filename. All other options
    /// taken from the CFileConfig instance variable
    CFilePtr create(std::string fileName, int fd=-2);
    ///
    /// Create using the provided filename. All other options
    /// taken from the CFileConfig instance variable
    CFilePtr createIndexedFilename(std::string fileName, uint32_t & cur_index);

private:
    CFileConfig         m_config;

};
BB_DECLARE_SHARED_PTR(CFileFactory);


} // namespace bb

#endif // BB_IO_CFILEFACTORY_H

#ifndef BB_IO_BYTESINKFACTORY_H
#define BB_IO_BYTESINKFACTORY_H

/* Contents Copyright 2014 Athena Capital Research LLC. All Rights Reserved. */

#include <string>

#include <bb/core/FDSet.h>
#include <bb/core/LuaConfig.h>
#include <bb/core/smart_ptr.h>

namespace bb {
namespace threading{
class ThreadPool;
BB_FWD_DECLARE_SHARED_PTR(ThreadPool);
}

BB_FWD_DECLARE_SHARED_PTR(CFileFactory);
BB_FWD_DECLARE_SHARED_PTR(ByteSink);

class ByteSinkConfig : public LuaConfig<ByteSinkConfig>{
public:
    friend class ByteSinkFactory;

   enum SinkType{
        kNone,
        kProducer
    };

    ByteSinkConfig();

    static void describe();

    ByteSinkConfig &setType(SinkType type);
    ByteSinkConfig &setBufferNum(size_t num);
    ByteSinkConfig &setBufferSize(size_t size);
    ByteSinkConfig &setFlushInterval(float flushInterval);

    SinkType    getType() const;
    std::string getTypeString() const;
    size_t      getBufferNum() const;
    size_t      getBufferSize() const;
    float       getFlushInterval() const;

    std::string                               m_type;
    float                                     m_flushInterval;
    size_t                                    m_bufferSize;
    size_t                                    m_bufferNum;
};
// Stream operator to convert the config into human readable form
std::ostream& operator <<( std::ostream& out, const ByteSinkConfig& config );

class ByteSinkFactory
{
public:
    ByteSinkFactory( ByteSinkConfig config );
    ByteSinkFactory( ByteSinkConfig config, CFileFactoryPtr fileFactory );
    ByteSinkFactory( FDSet &fdSet, ByteSinkConfig config, CFileFactoryPtr fileFactory );

    void setFdSet(FDSet& fdset);
    void setThreadpool(threading::ThreadPoolPtr tpool);
    void setConfig(ByteSinkConfig & config);

    // Use this function if the Byte sink already exists and only the wrapper
    // needs to be created.
    ByteSinkPtr create( ByteSinkPtr byteSink );
    // Use this function if the underlying byte sink must be created
    // with the factory object
    ByteSinkPtr create( const std::string fileName, int fd=-2 );
    // Use this function if the underlying byte sink must be created
    // with the factory object
    ByteSinkPtr createIndexedFilename( const std::string fileName, uint32_t & file_index );

private:
    ByteSinkConfig                            m_config;
    CFileFactoryPtr                           m_fileFactory;
    boost::optional<threading::ThreadPoolPtr> m_threadpool;
    boost::optional<FDSet&>                   m_fdSet;
};
BB_DECLARE_SHARED_PTR(ByteSinkFactory);

} // namespace bb

#endif // BB_IO_BYTESINKFACTORY_H

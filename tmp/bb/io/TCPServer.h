#ifndef BB_IO_TCPSERVER_H
#define BB_IO_TCPSERVER_H

/* Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved. */


#include <boost/thread/thread.hpp>
#include <bb/core/Error.h>
#include <bb/core/SelectDispatcher.h>
#include <bb/core/ThreadPipe.h>
#include <bb/io/Socket.h>

namespace bb
{

struct PortBindError : public Error
{
    PortBindError(int port) : Error("couldn't bind port %d", port) {};
};

/// TCPServer is a building block for making a multi-threaded server program.
/// It implements a model where you have some controlling thread (called the main
/// thread here), and one thread per TCP connection (called a conn thread here).
///
/// TCPServer will start a thread for each connection, and create a pipe per thread
/// for interthread communication. There is one pipe to the main thread, and one pipe
/// to each thread.
///
/// This is an abstract class, see below (search for overrides)
class TCPServer
{
public:
    struct ThreadMetadata;
    typedef std::list<ThreadMetadata> ThreadList_t;


    /// Each thread in a TCPServer has a ThreadPipe for inter-process wakeups.
    /// You can use all the pipes however you want, except that the value EXIT_CMD
    /// is reserved internally for thread management.
    static const uint8_t EXIT_CMD = 255;

    /// TCPServer::ConnectionThread stores all the data in a running thread which
    /// handles a TCP connection. You can subclass off ConnectionThread to add any per-thread
    /// data you want, including shared data (though you have to protect it yourself).
    class ConnectionThread
    {
    public:
        ConnectionThread(const ThreadPipeWriter &toMainPipe, const ThreadPipeReader &toSelf, TCPSocket *socket, const sockaddr_ipv4_t &peerAddr);
        virtual ~ConnectionThread();

        void run();
        virtual void handleWakeup(uint8_t cmd);

    protected:
        void onWakeup();

        /// This can be used to send wakeups to the main thread.
        ThreadPipeWriter m_toMainPipe;
        /// This contains all the wakeups that the conn thread gets from other threads.
        ThreadPipeReader m_toSelf;

        TCPSocket *m_socket;
        sockaddr_ipv4_t m_peerAddr;
        SelectDispatcher m_select;
        bool m_quit; // set to true to gracefully exit the thread

    private:
        Subscription m_pipeSub;
        friend class TCPServer;
        /// this is set from the ConnectionThread and tested from the main thread...
        /// since it's a bool there's probably no way to write to it that isn't
        /// atomic. so we don't bother using a mutex.
        volatile bool m_exited;
        /// backreference to this thread's entry in the TCPServer thread list.
        /// this is only accessed from the main thread; technically, it has no business
        /// here, but we put it here so TCPServer::lookupThread can be implemented O(1).
        ThreadList_t::iterator m_metadataIterator;
    };

    /// This is all the data that the main thread knows about each connection thread.
    struct ThreadMetadata
    {
        sockaddr_ipv4_t m_peerAddr;
        ThreadPipeWriter m_toThreadPipe;
        boost::thread *m_thread;
        /// ConnectionThread runs in a separate thread, so you must protect any thread-unsafe access
        ConnectionThread *m_connObj;
    };


    /// Starts a server by setting up callbacks for accepting connections from listenSocket.
    /// TCPServer will delete listenSocket when it's done. All of the connection threads
    /// must exit before TCPServer is destroyed.
    TCPServer(FDSet &select, int port);
    TCPServer(FDSet &select, TCPSocketPtr listenSocket);
    virtual ~TCPServer();

    /// Returns the current TCP port for accepting connections (useful if constructed with port 0)
    int port() const;

    /// can be used to ask a thread to prematurely drop a connection and exit the thread.
    void exitThread(ThreadMetadata &entry);

    ThreadPipeWriter &getMainLoopPipe() { return m_toMainLoopWriter; }

protected:
    // overrides:
    /// Handle a wakeup in the main thread. This means that some conn thread has written to
    /// ConnData::m_toMainPipe.
    virtual void handleWakeup(uint8_t cmd) = 0;

    /// Notification that a new connection has arrived (and a new thread has started from it).
    /// You can store the ThreadMetadata for later, should you ever need use it.
    virtual void onNewThread(const ThreadMetadata &entry) {}
    virtual void onThreadExited(const ThreadMetadata &entry) {}

    /// Creates a ConnectionThread object which will handle one TCP connection. The
    /// ConnectionThread object is created from the main thread, but then it is immediately
    /// passed off to a new thread, so any access later on must be thread-safe.
    virtual ConnectionThread *createConnection(const ThreadPipeWriter &toMainPipe,
            const ThreadPipeReader &toSelf,
            TCPSocket *socket,
            const sockaddr_ipv4_t &peerAddr) = 0;

    /// looks up the metadata for the given ConnectionThread
    ThreadMetadata &lookupThread(ConnectionThread *thread);

private:
    void init(FDSet &select, TCPSocketPtr listenSocket);
    void startThread(ThreadPipeWriter toMainPipe,
        ThreadPipeReader toSelf,
        TCPSocket *socket,
        sockaddr_ipv4_t peerAddr);
    void acceptConnection();
    void onWakeup();
    void reapThreads();

    TCPSocketPtr m_listenSocket;
    ThreadPipeReader m_toMainLoopReader;
    ThreadPipeWriter m_toMainLoopWriter;
    Subscription m_mainLoopReaderSub, m_listenSocketSub;

    // thread group for all client threads
    ThreadList_t m_threads;
    boost::thread_group m_threadGroup;
};

} // namespace bb

#endif // BB_IO_TCPSERVER_H

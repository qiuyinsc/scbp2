#ifndef BB_IO_LOGGEDSENDTRANSPORT_H
#define BB_IO_LOGGEDSENDTRANSPORT_H

/* Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved. */


#include <bb/io/LoggedTransport.h>
#include <bb/io/SendTransport.h>

namespace bb {

///
/// Convenient instantiations of LoggedSendTransport for the ISendTransport interface
///
class LoggedSendTransport : public LoggedTransport<ISendTransport>
{
public:
    LoggedSendTransport( ISendTransportPtr _parent, ByteSinkPtr sink = ByteSinkPtr() )
        : LoggedTransport<ISendTransport>( _parent, sink )
    {}
};

BB_DECLARE_SHARED_PTR( LoggedSendTransport );

} // namespace bb

#endif // BB_IO_LOGGEDSENDTRANSPORT_H

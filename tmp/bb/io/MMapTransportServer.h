#ifndef BB_IO_MMAPTRANSPORTSERVER_H
#define BB_IO_MMAPTRANSPORTSERVER_H

/* Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved. */


#include <sys/types.h>
#include <map>
#include <boost/intrusive/list.hpp>
#include <boost/integer_traits.hpp>
#include <bb/core/smart_ptr.h>
#include <bb/core/MsgBuffer.h>
#include <bb/core/QuitMonitor.h>
#include <bb/io/MMapTransport.h>
#include <bb/io/Socket.h>
#include <bb/io/MMapConfig.h>

namespace bb {

class MmapConnect2Msg;

BB_DECLARE_SHARED_PTR(MMapRecvTransport);
BB_FWD_DECLARE_SHARED_PTR(MmapResponse2Msg);

/// server-side class for establishing MMapTransport connections.
/// Accepts client connections over unix sockets,
/// sets up EventFD+shared-mem listener slot and then replies to
/// the client.
///
/// This class must be allocated & pumped inside the (one and only)
/// thread that calls MMapSendTransport::send().
class MMapServerFeedType
{
    struct Channel
    {
        Channel(std::string const &channelID, mmap_control::channel *channelSHM,
                mmap_control::FileCPtr const &controlSHM, uint32_t index);

        void addListener(mmap_control::notifier *notifierData, EventFDPtr const&ev);
        // Removes a listener from the notifier list. O(n_listeners).
        void removeListener(mmap_control::notifier *notifierData);

        const MMapSendTransportPtr &getSendTransport() { return m_sendTransport; }
        uint32_t getIndex() const { return m_index; }

    protected:
        uint32_t m_index;
        std::string m_channelID;
        MMapSendTransportPtr m_sendTransport;
        MMapSendTransport::ListenerArray m_listeners;
    };
    BB_DECLARE_SHARED_PTR(Channel);

    static const uint32_t INVALID_NOTIFIER = boost::integer_traits<uint32_t>::const_max;

    // Created when a client connects over unix socket, stays around as long
    // as the socket does.
    // Handles protocol details and delegates the rest to MMapServerFeedType
    class Client
        : public boost::intrusive::list_base_hook< boost::intrusive::link_mode<boost::intrusive::auto_unlink> >
    {
    public:
        Client(MMapServerFeedType *parent, FDSet &fdSet, UnixSocketPtr const &sock);
        void onReadable();
        void onData(size_t amt, fd_t passedFD);
        void onConnect(const MmapConnect2Msg &connect_msg);
        MMapServerFeedType *m_parent;
        UnixSocketPtr m_sock;
        Subscription m_sub;
        uint32_t m_notifierSlot;
        ChannelPtr m_channel;
    };

public:
    /// Callback for when new clients connect. Will be called from multiple threads.
    typedef boost::function<void(EFeedType ft, MMapSendTransportPtr sendTrans)> ConnectCB;
    typedef boost::function<void()> DisconnectCB;

    MMapServerFeedType(FDSet &fdSet,
                       const mmap_control::FilePtr& controlSHM,
                       const ConnectCB &connectCB=ConnectCB(),
                       const DisconnectCB &disconnectCB=DisconnectCB());

    // throws if already registered
    MMapSendTransportPtr registerChannel(std::string const&id, bb::EFeedType ft);

    typedef std::map<bb::EFeedType, std::map<int, ChannelPtr> > ChannelMap;

private:
    typedef boost::intrusive::list<Client, boost::intrusive::constant_time_size<false> > ClientList;
    typedef std::vector<uint32_t> NotifierSlots;

    void accept();
    void removeClient(Client *client);
    std::pair<ChannelPtr, uint32_t> connect(EFeedType feedType,
                                            int channelToConnect,
                                            EventFDPtr const &eventFD);

    UnixSocket                  m_unixSocket;
    MsgBuffer                   m_msgBuf;
    mmap_control::FilePtr       m_controlSHM;
    ChannelMap                  m_channels;
    ConnectCB                   m_connectCB;
    DisconnectCB                m_disconnectCB;
    ClientList                  m_clients;
    Subscription                m_acceptSub;
    FDSet                      &m_fdSet;
    MmapResponse2MsgPtr         m_responseMsg;
    NotifierSlots               m_freeSlots;
};
BB_DECLARE_SHARED_PTR(MMapServerFeedType);

/// Subscribe to an mmap feed. This is the client side of MMapServerFeedType.
/// Throws on error.
/// Can block on a response from the server side.
MMapRecvTransportPtr subscribeMMap( mmap_control::FilePtr const &controlSHM,
                                    EFeedType const &ft,
                                    bool verbose,
                                    int feedChannel);

std::vector<int> getAllChannelsAvailabe( mmap_control::FilePtr const &controlSHM,
                                         EFeedType const &ft);

} // namespace bb

#endif // BB_IO_MMAPTRANSPORTSERVER_H

#ifndef BB_IO_SASLSERVER_H
#define BB_IO_SASLSERVER_H

/* Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved. */


#include <boost/noncopyable.hpp>

#include <bb/core/smart_ptr.h>
#include <bb/core/source.h>

struct sasl_conn;
typedef sasl_conn sasl_conn_t;

namespace bb {

class Msg;
class SaslMsg;
BB_FWD_DECLARE_SHARED_PTR( ISendTransport );

class SaslServer : boost::noncopyable
{
public:
    SaslServer( const source_t &source, ISendTransportPtr sendTrans, const char* serviceName );
    virtual ~SaslServer();

    bool isAuthenticated() const { return m_authenticated; }
    bool isAuthStarted()   const { return m_authStarted; }

    const char* authenticatedUsername() const; // only call after isAuthenticated() returned true

    void handleSaslMessage( const SaslMsg& saslMsg );

private:
    void saslStart();

    static sasl_conn_t* saslInit( const char* serviceName );

    source_t          m_source;
    ISendTransportPtr m_sendTrans;
    bool              m_authenticated;
    bool              m_authStarted;
    sasl_conn_t*      m_saslConn;
};
BB_DECLARE_SHARED_PTR( SaslServer );

} // namespace bb

#endif // BB_IO_SASLSERVER_H

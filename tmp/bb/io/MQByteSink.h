#ifndef BB_IO_MQBYTESINK_H
#define BB_IO_MQBYTESINK_H

/* Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved. */

#include <bb/io/ByteSink.h>

#include <stdint.h>
#include <zmq.hpp>

namespace bb {

class ZmqPubByteSink : public ByteSink {
public:
    /// A ByteSink wrapper
    ZmqPubByteSink( int port );
    ZmqPubByteSink( int zmq_pub_port, std::string zmq_channel, std::string zmq_pub_id, uint32_t high_water_mark = 1000 );

    // This one is used as client side ZMQByteSink, IP address for server needs to be specified.
    ZmqPubByteSink( const std::string &ipaddress );

    // This one can used either as client side ByteSink or server side ByteSink.
    // When in server side situation, the IP address indicate which NIC to bind with.
    ZmqPubByteSink( const std::string& host, int port );

    virtual void flush();

    ~ZmqPubByteSink() {};

private:
    void init( const std::string& host, int port );

protected:
    virtual size_t ByteSink_write( const void *buf, size_t size );

    zmq::context_t m_context;
    zmq::socket_t m_socket;
    std::string m_pub_addr;
    boost::optional<std::string> m_prefix;
};
BB_DECLARE_SHARED_PTR(ZmqPubByteSink);

class ZmqPublisher : public ZmqPubByteSink
{
public:
    // host: specify NIC to bind to.
    ZmqPublisher( const std::string& host, int port );

    // Bind to all interfaces.
    ZmqPublisher( int port );

    void publish( const std::string& );
    void publish( const char* );
};
BB_DECLARE_SHARED_PTR(ZmqPublisher);

} // namespace bb

#endif // BB_IO_MQBYTESINK_H

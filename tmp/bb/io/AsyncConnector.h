#ifndef BB_IO_ASYNCCONNECTOR_H
#define BB_IO_ASYNCCONNECTOR_H

/* Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved. */


#include <sys/types.h>
#include <sys/socket.h>

#include <cerrno>

#include <boost/bind.hpp>
#include <boost/function.hpp>
#include <boost/make_shared.hpp>
#include <boost/shared_ptr.hpp>

#include <bb/core/FDSet.h>
#include <bb/core/bbassert.h>

#include <bb/io/Socket.h>

namespace bb {

// Use an AsyncConnector to initiate a non-blocking connection to a
// listening socket. The callback provided to the 'connect' call will
// be called back with the results of the connection
// attempt.
//
// Returns 'true' if either the connection was made immediately,
// or if the FDSet has been configured to await completion of the
// connect request. The callback will be called exactly once in
// this case (though this one time may be before the call returns).
//   Callback meanings:
//     connection succeeded:
//       sock => new instance of SocketType
//       error => 0
//     connection failed:
//       sock => NULL
//       error => nonzero
//     connection cancelled:
//       sock => NULL
//       error => ECANCELED.
//
// Returns 'false' if the connector was unable to allocate a
// socket, or if the underlying 'connect' call failed immediately.
// The callback will never be called in this case.
//
// You must pass an unconnected, unbound socket, which has been
// set to non-blocking mode.  The underlying network protocol
// type represented by SocketType must be one for which connection
// establishment is meaningful. In addition, SocketType must provide:
//     int getFD()
//     bool isNonBlocking()
//     bool connect( const typename SocketType::AddrType & )
//
// Note that bb::NetSocket provides all of these, so any subclass of
// NetSocket that supports connection establishment operations should
// work here.
//
// Note: you cannot set socket buffer sizes after a connect() call,
// so you should tweak any socket options first before you call
// async_connect. See tcp(7) for details.
template<typename SocketType>
bool async_connect(Subscription &sub, FDSet &fdSet,
        const boost::shared_ptr<SocketType> &socket,
        const typename SocketType::AddrType &address,
        const boost::function<void(const boost::shared_ptr<SocketType> &sock, int error)> &callback);

bool async_tcp_connect(Subscription &sub, FDSet &fdSet, const TCPSocketPtr &socket,
        const sockaddr_ipv4_t &address,
        const boost::function<void(const TCPSocketPtr &, int error)> &callback);


template<typename SocketType>
struct AsyncConnectImpl : public SubscriptionBase
{
    typedef boost::shared_ptr<SocketType> SocketTypePtr;

    // The user callback. The callback will be called with a non-empty
    // SocketTypePtr if the connection completes. If SocketTypePtr is
    // empty, the error parameter should contain an errno value
    // explaining the error.
    typedef boost::function< void(const SocketTypePtr&, int error) > AsyncConnectCB;

    static bool connect(Subscription &sub, FDSet &fdSet,
            const SocketTypePtr &socket,
            const typename SocketType::AddrType &address,
            const boost::function<void (const SocketTypePtr &, int error)> &callback)
    {
        BB_THROW_EXASSERT(socket, SRC_LOCATION_SEP "async_connect: socket is NULL");
        BB_THROW_EXASSERT(socket->isNonBlocking(), SRC_LOCATION_SEP "async_connect: socket must be non-blocking");

        if( socket->connect( address ) )
        {
            // we connected right away, call the users callback directly.
            callback( socket, 0 );
            return true;
        }
        int error = errno;
        BB_THROW_EXASSERT(error != EINTR, "nonblocking connect gave EINTR"); // bmb: this shouldn't happen

        // Anything other than EINPROGRESS is a permanent error.
        if( error != EINPROGRESS )
        {
            return false;
        }

        std::auto_ptr<AsyncConnectImpl> impl(new AsyncConnectImpl(socket, callback));
        fdSet.createWriteCB(
            impl->m_fdSub,
            socket,
            boost::bind( &AsyncConnectImpl::onConnectComplete, impl.get() ) );
        sub.reset(impl.release());
        return true;
    }

    AsyncConnectImpl(const SocketTypePtr &socket, const AsyncConnectCB &callback)
        : m_socket(socket)
        , m_callback(callback)
    {
    }

    virtual ~AsyncConnectImpl()
    {
        m_fdSub.reset();
        m_socket.reset();
        if(m_callback)
            m_callback(m_socket, ECANCELED);
    }

    virtual bool isValid() const { return m_fdSub.isValid(); }

    void onConnectComplete()
    {
        SocketTypePtr socket;
        AsyncConnectCB callback;
        int error = 0;

        socket.swap( m_socket );
        callback.swap( m_callback );
        m_fdSub.reset();

        socklen_t error_len = sizeof(error);
        if( getsockopt( socket->getFD(), SOL_SOCKET, SO_ERROR, &error, &error_len ) != 0 )
        {
            // We were unable to extract the socket error. Since we can't
            // say with certainty whether the connection is ok, return an
            // error.
            error = errno;
        }
        else
            BB_ASSERT( error_len == sizeof(error) );

        if( error != 0 )
            socket.reset();

        callback( socket, error );
    }

    SocketTypePtr m_socket;
    Subscription m_fdSub;
    AsyncConnectCB m_callback;
};

template<typename SocketType>
inline bool async_connect(Subscription &sub, FDSet &fdSet, const boost::shared_ptr<SocketType> &socket,
        const typename SocketType::AddrType &address,
        const boost::function<void(const boost::shared_ptr<SocketType> &sock, int error)> &callback)
    { return AsyncConnectImpl<SocketType>::connect(sub, fdSet, socket, address, callback); }

inline bool async_tcp_connect(Subscription &sub, FDSet &fdSet, const TCPSocketPtr &socket,
        const sockaddr_ipv4_t &address,
        const boost::function<void(const TCPSocketPtr &, int error)> &callback)
{
    return async_connect(sub, fdSet, socket, address, callback);
}

} // namespace bb

#endif // BB_IO_ASYNCCONNECTOR_H

#ifndef BB_IO_LOGGEDRECVTRANSPORT_H
#define BB_IO_LOGGEDRECVTRANSPORT_H

/* Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved. */

#include <bb/io/Socket.h>
#include <bb/io/LoggedTransport.h>
#include <bb/io/RecvTransport.h>

namespace bb {

/// Convenient instantiation of LoggedRecvTransport for the IRecvTransport interface
/// IMStreamCallback is the first base class to maximize efficiency of the onMessage callback
class LoggedRecvTransport : private IMStreamCallback, public LoggedTransport<IRecvTransport>
{
public:
    LoggedRecvTransport( IRecvTransportPtr _parent, ByteSinkPtr sink = ByteSinkPtr() )
        : LoggedTransport<IRecvTransport>( _parent, sink )
        , m_messageCB( NULL )
    {}

    virtual void beginDispatch( FDSet &d, IMStreamCallback *messageCB,
            const EndOfStreamCB &endOfStreamCB = EndOfStreamCB(),
            const ErrorCB &errorCB = ErrorCB() )
    {
        m_messageCB = messageCB;
        parent->beginDispatch( d, this, endOfStreamCB, errorCB );
    }

    virtual void endDispatch()
    {
        parent->endDispatch();
        m_messageCB = NULL;
    }

    const timeval_t& getCallbackTimestamp() const {
        if( parent )
        {
            return parent->getCallbackTimestamp();
        }
        return timeval_t::earliest;
    }

private:
    void onMessage( const Msg &msg )
    {
        // The client identify message will contain the source and destination IP and MAC as observed by the client.
        // Before logging this message, the server will fill in its own observation.
        //
        // This is only applicable to tcp and sctp sockets.
        // Note that we adjust the msg_size below. This is to support older clients that as the time of writing
        // do not support these newer fields.
        if ( unlikely ( msg.hdr->mtype == MSG_CLIENT_IDENTIFY ) )
        {
            bb::mac_addr_t remote_mac_addr, local_mac_addr;
            bb::sockaddr_ipv4_t remote_ip_addr, local_ip_addr;

            if ( TCPSocketRecvTransportPtr tcp_transport = boost::dynamic_pointer_cast<TCPSocketRecvTransport> ( parent ) )
            {
                if ( TCPSocketPtr tcp_socket = tcp_transport->getSocket() )
                {
                    remote_mac_addr = tcp_socket->getRemoteMacAddr();
                    remote_ip_addr = tcp_socket->getRemoteSockName();
                    local_mac_addr = tcp_socket->getLocalMacAddr();
                    local_ip_addr = tcp_socket->getSockName();
                }
            }

            if ( SCTPSocketRecvTransportPtr sctp_transport = boost::dynamic_pointer_cast<SCTPSocketRecvTransport> ( parent ) )
            {
                if ( SCTPSocketPtr sctp_socket = sctp_transport->getSocket() )
                {
                    remote_mac_addr = sctp_socket->getRemoteMacAddr();
                    remote_ip_addr = sctp_socket->getRemoteSockName();
                    local_mac_addr = sctp_socket->getLocalMacAddr();
                    local_ip_addr = sctp_socket->getSockName();
                }
            }

            if ( ClientIdentifyMsg const * client_msg_ptr = dynamic_cast < ClientIdentifyMsg const * > ( &msg ) )
            {
                ClientIdentifyMsg const & identify_const_msg ( *client_msg_ptr );
                ClientIdentifyMsg & identify_msg ( const_cast < ClientIdentifyMsg & > ( identify_const_msg ) );

                identify_msg.setSourceMacSeenByServer ( remote_mac_addr );
                identify_msg.setSourceIpSeenByServer ( remote_ip_addr );

                identify_msg.setDestMacSeenByServer ( local_mac_addr );
                identify_msg.setDestIpSeenByServer ( local_ip_addr );

                identify_msg.hdr->size = ClientIdentifyMsg::MSG_SIZE;
            }
        }
        log( &msg );
        m_messageCB->onMessage(msg);
    }

    IMStreamCallback *m_messageCB;
};

BB_DECLARE_SHARED_PTR( LoggedRecvTransport );

} // namespace bb

#endif // BB_IO_LOGGEDRECVTRANSPORT_H

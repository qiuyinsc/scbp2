#ifndef BB_IO_MQ_H
#define BB_IO_MQ_H

/* Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved. */


// POSIX Message Queue wrappers

#include <bb/core/FD.h>
#include <bb/core/MsgBuffer.h>
#include <bb/core/ptime.h>

#include <bb/io/ByteSource.h>
#include <bb/io/ByteSink.h>

namespace bb {


class MQByteSource : public ByteSource, public FD
{
public:
    MQByteSource(std::string const &name, size_t max_num_msg, size_t max_msg_size);
    MQByteSource(std::string const &name); // only open, do not create
    MQByteSource();
    virtual ~MQByteSource();

    bool bind(std::string const &name, size_t max_num_msg, size_t max_msg_size);
    bool bind(std::string const &name);

    void setRecvTimeout(const ptime_duration_t& timeout) { m_timeout = timeout; }

protected:
    virtual size_t ByteSource_read(void *buf, size_t size);

    bool m_created;
    std::string m_name;
    ptime_duration_t m_timeout;
};
BB_DECLARE_SHARED_PTR(MQByteSource);


class MQByteSink : public ByteSink, public NBFD
{
public:
    MQByteSink(std::string const &name, size_t max_num_msg, size_t max_msg_size);
    MQByteSink(std::string const &name); // only open, do not create
    MQByteSink();
    virtual ~MQByteSink();

    void setSendTimeout(const ptime_duration_t& timeout) { m_timeout = timeout; }

protected:
    virtual size_t ByteSink_write(const void *buf, size_t size);
    virtual void flush() { /*no-op*/ }

    bool m_created;
    std::string m_name;
    ptime_duration_t m_timeout;
};
BB_DECLARE_SHARED_PTR(MQByteSink);


class MQPseudoSocket : public MQByteSource, public MQByteSink
{
public:
    MQPseudoSocket(std::string const &source_name, std::string const &sink_name,
            size_t source_max_num_msg, size_t sink_max_num_msg,
            size_t source_max_msg_size, size_t sink_max_msg_size);
    MQPseudoSocket(std::string const &source_name, std::string const &sink_name);
    MQPseudoSocket();
    virtual ~MQPseudoSocket();
};
BB_DECLARE_SHARED_PTR(MQPseudoSocket);


class MQServer : public MQByteSource
{
public:
    MQServer(std::string const &name);

    MQPseudoSocket* accept();

private:
    MsgBuffer m_msgBuf;
};
BB_DECLARE_SHARED_PTR(MQServer);


class MQClient : public MQByteSource, public MQByteSink
{
public:
    MQClient(size_t max_num_msg = 1000);
    virtual ~MQClient();

    bool connect(std::string const &name);

private:
    bool m_connected;
};
BB_DECLARE_SHARED_PTR(MQClient);


} // namespace bb

#endif // BB_IO_MQ_H

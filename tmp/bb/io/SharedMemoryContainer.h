#ifndef BB_IO_SHAREDMEMORYCONTAINER_H
#define BB_IO_SHAREDMEMORYCONTAINER_H

/* Contents Copyright 2016 Athena Capital Research LLC. All Rights Reserved. */
#include <bb/core/Log.h>
#include <bb/core/Error.h>
#include <bb/core/messages.h>
#include <bb/core/instrument.h>
#include <bb/core/mtype.h>

#include <bb/threading/LockFree.h>
#include <bb/clientcore/SharedMemoryStructs.h>

#include <boost/interprocess/permissions.hpp>
#include <boost/interprocess/containers/map.hpp>
#include <boost/interprocess/containers/vector.hpp>
#include <boost/interprocess/managed_shared_memory.hpp>

namespace bb{

typedef boost::interprocess::managed_shared_memory  ManagedSharedMem;
typedef boost::interprocess::shared_memory_object   SharedMemObj;

template<typename ElementT>
struct SharedType
{
    typedef ElementT value_type;
    typedef boost::interprocess::allocator<ElementT, ManagedSharedMem::segment_manager> allocator;
};

// Define the spec for a Map.
// A specific Compare Functor is required to create a SharedMemoryMap
template<typename KeyType, typename ValType, typename CmpFunctor>
struct MapType : public SharedType<std::pair<const KeyType, ValType> >
{
    typedef typename SharedType<std::pair<const KeyType, ValType> >::allocator allocator;

    // This is the REAL type for our Map object.
    typedef boost::interprocess::map<KeyType, ValType, CmpFunctor, allocator> type;

    // Construct function for a Map
    static type* construct(ManagedSharedMem& shmSegment, const std::string& containerName, allocator allctr, uint32_t size)
    {
        return shmSegment.construct<type>(containerName.c_str())( CmpFunctor(), allctr );
    }
};

// Define the spec for a Vector.
template <typename ElementType>
struct VecType : public SharedType<ElementType>
{
    typedef typename SharedType<ElementType>::allocator allocator;

    // The REAL type for our Map object.
    typedef boost::interprocess::vector<ElementType, allocator> type;

    // Construct function for a Vector
    static type* construct(ManagedSharedMem& shmSegment, const std::string& containerName, allocator allctr, uint32_t size)
    {
        type* r = shmSegment.construct<type>(containerName.c_str())( allctr );

        // Resize the vector to be its max size.
        r->resize(size);

        return r;
    }
};

class SharedMemPermissions
{
public:
    SharedMemPermissions()
    {
        perms.set_unrestricted();
    }

    const boost::interprocess::permissions& getPermissions() const
    {
        return perms;
    }
protected:
    boost::interprocess::permissions            perms;
};

// General SharedMemory Container class
template<typename TypeConfig>
class SharedMemoryContainer
{
public:
    SharedMemoryContainer(const std::string& shmFilename, const std::string containerName, uint32_t size )
        : m_shmFilename(shmFilename)
        , m_shmemSize( 2 * size * sizeof(typename TypeConfig::value_type) ) // we mltiply by 2 to account for metadata used by the obj
        , m_permissions()
        , m_shmSegment( boost::interprocess::open_or_create,
                        m_shmFilename.c_str(), m_shmemSize, 0, m_permissions.getPermissions() )
        , m_allocator( m_shmSegment.get_segment_manager() )
    {
        BB_THROW_EXASSERT_SSX( m_shmSegment.get_size() == m_shmemSize,
                               "Shared memory with name: /dev/shm/"   << m_shmFilename
                               << " has size: "              << m_shmSegment.get_size()
                               << " but requested size is: " << m_shmemSize
                               << " remove file and try again" );

        // Loop until we can either find or create the vector
        while(1){
            // try and find the vector
            std::pair<typename TypeConfig::type*, bool> ret = m_shmSegment.find<typename TypeConfig::type>( containerName.c_str() );

            // if we found it we are done
            if( ret.second ){
                m_container = ret.first;
                break;
            }

            try{
                // we did not find it. try and create it
                m_container = TypeConfig::construct(m_shmSegment, containerName, m_allocator, size);
            }
            catch( const boost::interprocess::interprocess_exception& ex )
            {
                // nothing to do here.  The construct failed, most likely someone else already constructed it
                // go back to the top and the find should work
            }
        }
    }

    virtual ~SharedMemoryContainer()
    {
        m_container = 0;
    }

    typedef typename TypeConfig::type::iterator iterator;
    typedef typename TypeConfig::type::const_iterator const_iterator;
    typedef typename TypeConfig::value_type value_type;

    iterator begin()             { return m_container->begin(); }
    iterator end()               { return m_container->end(); }
    const_iterator begin() const { return m_container->begin(); }
    const_iterator end()   const { return m_container->end(); }

protected:
    const std::string              m_shmFilename;
    const uint32_t                 m_shmemSize;
    const SharedMemPermissions     m_permissions;
    ManagedSharedMem               m_shmSegment;
    typename TypeConfig::allocator m_allocator;
    typename TypeConfig::type*     m_container;
};

template<typename KeyType, typename ValType, typename CmpFunctor = std::less<KeyType> >
class SharedMemoryMap : public SharedMemoryContainer<MapType<KeyType, ValType, CmpFunctor> >
{
    // typedef the base class to make the code simpler.
    typedef SharedMemoryContainer<MapType<KeyType, ValType, CmpFunctor> > base;

public:
    SharedMemoryMap(const std::string& shmFilename, const std::string containerName, uint32_t size )
        :base(shmFilename, containerName, size) {};

    // Override the operators for Map
    const ValType& operator[] ( KeyType key) const { return (*base::m_container)[key]; };
    ValType&       operator[] ( KeyType key)       { return (*base::m_container)[key]; };
    size_t             size()                const { return base::m_container->size(); };
};

template<typename ElementT>
class SharedMemoryVector : public SharedMemoryContainer<VecType<ElementT> >
{
    // typedef the base class to make the code simpler.
    typedef SharedMemoryContainer<VecType<ElementT> > base;

public:
    SharedMemoryVector(const std::string& shmFilename, const std::string containerName, uint32_t size )
        :base(shmFilename, containerName, size) {};

    // Override the operators for Vector
    const ElementT&    operator[] ( uint32_t key) const { return (*base::m_container)[key]; };
    ElementT&          operator[] ( uint32_t key)       { return (*base::m_container)[key]; };
    size_t             size()                     const { return base::m_container->size(); };
};

class MemAcrBookSharedVec
    : public SharedMemoryVector<MemAcrBook>
{
public:
    MemAcrBookSharedVec(const std::string& shmFilename, const std::string vectorName = s_defaultVectorName )
        : SharedMemoryVector<MemAcrBook>(shmFilename, vectorName, SYM_MAX )
    {}
    const static std::string s_defaultVectorName;
};
BB_DECLARE_SHARED_PTR( MemAcrBookSharedVec );

class MemAcrBookSharedMap
    : public SharedMemoryMap<bb::instrument_t, MemAcrBook, instrument_t::less_no_mkt_no_currency>
{
public:
    MemAcrBookSharedMap(const std::string& shmFilename, const std::string mapName = s_defaultMapName )
        : SharedMemoryMap<bb::instrument_t, MemAcrBook, instrument_t::less_no_mkt_no_currency>(shmFilename, mapName, SYM_MAX )
    {}
    const static std::string s_defaultMapName;
};
BB_DECLARE_SHARED_PTR( MemAcrBookSharedMap );

} /* namespace bb */

#endif // BB_IO_SHAREDMEMORYCONTAINER_H

#ifndef BB_IO_INPROCESSTRANSPORT_H
#define BB_IO_INPROCESSTRANSPORT_H

/* Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved. */


#include <string>
#include <boost/noncopyable.hpp>
#include <bb/core/smart_ptr.h>
#include <bb/io/ISendTransport.h>
#include <bb/io/IRecvTransport.h>

namespace bb {

BB_FWD_DECLARE_SHARED_PTR( InProcessTransport );
class InProcessTransport
{
public:
    // Construct a new InProcessTransport object. To connect to an
    // InProcessTransportServer, you must call 'connect' after
    // creating the InProcessTransport.
    inline InProcessTransport(
        const ISendTransportPtr& send = ISendTransportPtr() ,
        const IRecvTransportPtr& recv = IRecvTransportPtr() )
        : sendTransport( send )
        , recvTransport( recv ) {}

    // Attempt to connect to an InProcessTransportServer that has been
    // bound to the name 'name' with InProcessTransportServer::bind.
    bool connect( const std::string& name );

    // Public and mutable shared pointers for the send and recv
    // transports. Calling 'reset' on these will have the effect of
    // closing that part of the connection, if the InProcessTransport
    // object holds the last reference to the transport object.
    ISendTransportPtr sendTransport;
    IRecvTransportPtr recvTransport;
};

BB_FWD_DECLARE_SHARED_PTR( InProcessTransportServer );
class InProcessTransportServer : public boost::noncopyable
{
    // Grant InProcessTransport::connect access to
    // InProcessTransportServer::handleConnect.
    friend class InProcessTransport;

public:
    typedef boost::function<void( const InProcessTransportPtr& )> AcceptHandler;

    // Construct a new InProcessTransportServer. The server will
    // listen for calls to InProcessTransport::connect at 'name', and
    // will accept the connection by invoking the callback
    // 'acceptHandler' with a new InProcessTransport object. The
    // lifetime of the acceptHandler callback (and the objects the
    // callback references), must be greater than that of the
    // InProcessTransportServer.
    static InProcessTransportServerPtr bind(
        const std::string& name,
        const AcceptHandler& acceptHandler );

    // Return the name that this server is bound to.
    inline const std::string& name() const
    {
        return m_name;
    }

private:
    // Constructs a new InProcessTransportServer with the given name
    // and accept handler.
    inline InProcessTransportServer(
        const std::string& name,
        const AcceptHandler& acceptHandler )
        : m_name( name )
        , m_acceptHandler( acceptHandler ) {}

    // Called from InProcessTransport::connect to complete the
    // connection handshake.
    void handleConnect( InProcessTransport* );

    const std::string m_name;
    const AcceptHandler m_acceptHandler;
};

} // namespace bb

#endif // BB_IO_INPROCESSTRANSPORT_H

#ifndef BB_IO_IO_SCRIPTING_H
#define BB_IO_IO_SCRIPTING_H

/* Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved. */


namespace bb {

/// Adds the bbio library to the scripting environment.
bool io_registerScripting();

} // namespace bb

#endif // BB_IO_IO_SCRIPTING_H

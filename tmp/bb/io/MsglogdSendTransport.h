#ifndef BB_IO_MSGLOGDSENDTRANSPORT_H
#define BB_IO_MSGLOGDSENDTRANSPORT_H

#include <bb/io/CFile.h>
#include <bb/io/SendTransport.h>
#include <bb/io/ByteSinkFactory.h>

#include <bb/core/FDSet.h>
#include <bb/core/date.h>
#include <bb/core/AresResolver.h>

/* Contents Copyright 2015 Athena Capital Research LLC. All Rights Reserved. */
namespace bb {

class MsglogdSendTransport: public ISendTransport
{
public:
    /// Constructor for the MsglogdSendTransport. This is used to log data. Data is logged by
    /// either directly writing it to a file or by connecting to a msglogd server.  The transport
    /// takes as input a lua config file that has the following format.
    ///     msglogd = {
    ///         unix = "/com/athenacr/msglogd/cts",
    ///         sink_config = {
    ///             type           = 'producer',    -- use producer ByteSink
    ///             buffer_num     = 256,           -- use 256 buffer
    ///             buffer_size    = 256*1024,      -- 256k bytes per buffers
    ///             flush_interval = 60,            -- force a buffer flush every 60 seconds
    ///         },
    ///         threadpool_config = {
    ///             thread_num = 1,                  -- number of threads in threadpool
    ///             allowed_cpu_list = { 0, 1 },     -- allowed CPUs for threads in threadpool
    ///             processing_mode = 'blocking',    -- by default we won't spin the CPU
    ///         },
    MsglogdSendTransport( FDSet& fdSet, const source_t qdSource, const luabind::object& msglogd_config,
                          const date_t tradeDate=date_t::today(), const std::string suffix = "");

    /// generate a conventional filename prefix for quote daemon logs:
    /// convert the EFeedType part of source to a friendly name, e.g. SRC_ISLD to "isld"
    /// if qualifier is not null, include it in the result, to distinguish e.g. tunnelsend from QDs
    /// this will return a string in the form:
    /// <feed_type>[-<qualifier>]-<datacenter>
    static std::string getSourceLogPrefix( const source_t& source, const char* qualifier = NULL );

    /// send a message
    virtual void send( const Msg* );

private:
    std::string makeLogFileName( const std::string& directory, const source_t& source, bool compress,
                                 const date_t& tradeDate, const std::string suffix );
    CFilePtr    createLogFile( CFileFactory& factory, const std::string& logNameFormat );
    void        onMsglogdNameResolution( const sockaddr_ipv4_t& msglogd_addr );

    ByteSinkFactoryPtr        m_byteSinkFactory;
    DGramWriteTransportPtr    m_spTransport;
    AresResolverPtr           m_dnsResolver;
    // date_t                    m_tradeDate;
    // std::string               m_suffix;
    bool                      m_verbose;
};

} //end namespace bb

#endif // BB_IO_MSGLOGDSENDTRANSPORT_H

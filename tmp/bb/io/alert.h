#ifndef BB_IO_ALERT_H
#define BB_IO_ALERT_H

/* Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved. */


#include <string>
#include <bb/core/smart_ptr.h>
#include <bb/core/loglevel.h>

namespace bb {

BB_FWD_DECLARE_SCOPED_PTR(TextAlertMsg);
BB_FWD_DECLARE_SHARED_PTR(ISendTransport);

class LiveAlertTransport
{
public:
    LiveAlertTransport();
    ~LiveAlertTransport();

    /// Broadcasts out an alert, where hopefully an alertd is
    /// listening.  alertd's job is to convert the broadcast to an
    /// actual alarm.  Valid values of level can be found in Log.h (as
    /// of this writing, they are L_INFO, L_WARN, and L_PANIC.
    /// identityStr should be the name/description of the program sending the alert.
    /// Throws bb::CError on error.
    void sendAlert(const std::string &identityStr, loglevel_t level, const std::string &msg);

protected:
    ISendTransportPtr m_sendTrans;
    const TextAlertMsgScopedPtr m_message;
};

} // namespace bb

#endif // BB_IO_ALERT_H

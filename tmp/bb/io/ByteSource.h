#ifndef BB_IO_BYTESOURCE_H
#define BB_IO_BYTESOURCE_H

/* Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved. */

#include <boost/shared_ptr.hpp>
#include <boost/make_shared.hpp>

#include <iosfwd>
#include <bb/core/smart_ptr.h>
#include <bb/core/Buffer.h>


namespace bb {

class ByteSource {
public:
    virtual ~ByteSource() { }

    /// Reads in a block of data.  Doesn't necessarily have to
    /// read in all the data requested.  It returns the number of bytes actually read.
    /// Reads in at least 1 byte, unless the end of the stream is reached.
    /// Throws an exception on read failure.
    size_t read(void *buf, size_t size);

    /// Continuously calls read until size bytes have been read.
    /// Returns the number of bytes read, which can be less than 'size'
    /// if the end of the stream is reached.
    /// Throws an exception on read failure.
    size_t readN(void *buf, size_t size);

protected:
    /// Pure virtual method which does the actual work of ByteSource reading.
    /// Implementors must override this method, rather than ByteSource::read.
    /// Reads in at least 1 byte, unless the end of the stream is reached.
    /// Throws an exception on read failure.
    virtual size_t ByteSource_read(void *buf, size_t size) = 0;
};
BB_DECLARE_SHARED_PTR(ByteSource);



class IStreamByteSource : public ByteSource {
public:
    /// A ByteSource wrapper for std::istream.
    /// Activates exceptions on the istream.
    IStreamByteSource(std::istream &);

protected:
    virtual size_t ByteSource_read(void *buf, size_t size);

    std::istream &m_istr;
};
BB_DECLARE_SHARED_PTR(IStreamByteSource);


// wraps a ByteSource in a fixed-size read buffer.
BB_FWD_DECLARE_SHARED_PTR(BufferedByteSource);
class BufferedByteSource : public ByteSource
{
public:
    BufferedByteSource(ByteSourcePtr src, unsigned int bufSize);
    // Wraps a buffering layer around a ByteSourcePtr and returns another ByteSourcePtr.
    static ByteSourcePtr create(ByteSourcePtr bs, unsigned int bufSize)
    {    return boost::static_pointer_cast < ByteSource > ( boost::make_shared < BufferedByteSource > (bs, bufSize)); }

protected:
    virtual size_t ByteSource_read(void *buf, size_t size);
    ByteSourcePtr m_src;
    Buffer m_buffer;
};

} // namespace bb

#endif // BB_IO_BYTESOURCE_H

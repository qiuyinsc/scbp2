#ifndef BB_IO_RECVTRANSPORT_H
#define BB_IO_RECVTRANSPORT_H

/* Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved. */


#include <sys/types.h>
#include <sys/socket.h>

#include <boost/function.hpp>
#include <boost/scoped_array.hpp>

#include <bb/core/loglevel.h>
#include <bb/core/MsgBuffer.h>
#include <bb/core/Buffer.h>
#include <bb/core/MStreamCallback.h>
#include <bb/core/FDSetFwd.h>
#include <bb/core/shared_vector.h>
#include <bb/core/DestructorGuard.h>
#include <bb/core/ReactorCallback.h>
#include <bb/core/CoreConfig.h>

#include <bb/io/IRecvTransport.h>
#include <bb/io/recvmmsg_glibc.h>

#include <bb/io/MsgBuf.h>

namespace bb {

BB_FWD_DECLARE_SHARED_PTR( TCPSocket );
BB_FWD_DECLARE_SHARED_PTR( UDPSocket );
BB_FWD_DECLARE_SHARED_PTR( ByteSource );
BB_FWD_DECLARE_SHARED_PTR( BaseTCPSendTransport );
BB_FWD_DECLARE_SHARED_PTR( UnixSocket );
BB_FWD_DECLARE_SHARED_PTR( SCTPSocket );

/// There are two encodings that we use for messages.
///
/// DGram encoding: this is simply raw packets, one after another
/// This is used for UDP.
///
/// Stream transports are different than datagram transports, because
/// they include extra size information before sending the actual
/// message.  Thus, all messages are actually sent using
/// sizeof(MsgSizeField_t)+sizeof(message) bytes, where the MsgSizeField_t
/// variable contains the value of sizeof(message).

const uint32_t DEFAULT_RECV_BUF_SIZE = 1024*1024;


struct UDPSocketRecvTransportConfig
{
    static const size_t DEFAULT_MAX_READ_MSGS;
    enum ShutdownFlag_t { SOCK_SHUTDOWN, SOCK_NO_SHUTDOWN };
    UDPSocketRecvTransportConfig()
        : recvBufferSize( DEFAULT_RECV_BUF_SIZE )
        , maxMessagesPerSocketRead( DEFAULT_MAX_READ_MSGS )
        , shutdownFlag( SOCK_SHUTDOWN )
    {}
    uint32_t                         recvBufferSize;
    int                              maxMessagesPerSocketRead;
    ShutdownFlag_t                   shutdownFlag;
};


/// Receives BB messages from a UDP socket.
class UDPSocketRecvTransport : public IRecvTransport
{
public:


    typedef UDPSocketRecvTransportConfig Config;


    /// UDPSocketRecvTransport will set a large recv buffer size,
    /// specified by the constant DEFAULT_RECV_BUF_SIZE.
    ///
    /// This'll place the socket in non-blocking mode and shutdown the sending side,
    /// so essentially, don't do anything with the socket once you create this.
    /// The kernel has special optimizations if the sock is non-blocking.
    UDPSocketRecvTransport(UDPSocketPtr s, const Config& config = Config() );

    /// IRecvTransport impl
    virtual void beginDispatch(FDSet &d, IMStreamCallback *messageCB,
            const EndOfStreamCB &endOfStreamCB = EndOfStreamCB(),
            const ErrorCB &errorCB = ErrorCB());
    virtual void endDispatch();

    void setCompress( bool compress ){ m_compress = compress; }

protected:
    class ReadyCallback;
    void onSelectCallback();

    UDPSocketPtr m_source;
    const size_t m_maxMessages;

    Subscription m_readSub;
    IMStreamCallback *m_messageCB;
    ErrorCB m_errorCB;

    boost::scoped_array<MsgBuffer> m_buffers;
    boost::scoped_array<MsgBuffer> m_compressedBuffers;
    boost::scoped_array<util::MsgBuf> m_cntlBuffers;
    boost::scoped_array<struct ::iovec> m_iovs;
    boost::scoped_array<struct ::mmsghdr> m_headers;
    boost::scoped_array<struct sockaddr_in> m_from;

    DestructorMarker m_dtor;

    bool m_compress;
    boost::scoped_array<unsigned char> m_compressBuffer;
    unsigned long m_compressBufferLen;
};
BB_DECLARE_SHARED_PTR( UDPSocketRecvTransport );

/// Wraps a UDPSocketRecvTransport to check its sequence numbers.
/// The Lua config should be like { level = LogLevel.WARN, period = Timeval(10), sources = { SRC.NYSE_LRP } }
class SequenceCheckingRecvTransport : public IMStreamCallback, public IRecvTransport
{
public:
    SequenceCheckingRecvTransport(UDPSocketRecvTransportPtr s, const bb::FeedConfig::CheckSeqNum& config, EFeedType sourceType );

    /// IRecvTransport impl
    virtual void beginDispatch(FDSet &d, IMStreamCallback *messageCB,
            const EndOfStreamCB &endOfStreamCB = EndOfStreamCB(),
            const ErrorCB &errorCB = ErrorCB());
    virtual void endDispatch();

    uint32_t gaps() const { return m_seqGaps; }
    uint32_t reversals() const { return m_seqRevs; }

    typedef boost::function<void()> SequenceErrorCallback;
    typedef shared_vector<SequenceErrorCallback> CallbackVector;

    // Add a new callback to be called if a gap or reversal is
    // detected. Reset the returned subscription to remove the
    // callback.
    void addSequenceErrorCallback(Subscription &sub, const SequenceErrorCallback& cb);

protected:
    void onMessage(const Msg &msg);

    void callSequenceErrorCallbacks();

    UDPSocketRecvTransportPtr m_source;
    EFeedType m_sourceType;
    IMStreamCallback *m_messageCB;
    uint32_t m_expectedSeq;
    uint32_t m_seqNumPrior;          // the seq num at the time we last logged
    uint32_t m_seqGaps;
    uint32_t m_seqRevs;
    uint32_t m_seqGapsPrior;
    uint32_t m_seqRevsPrior;
    timeval_t m_nextLog;
    ptime_duration_t m_logPeriod;
    loglevel_t m_logLevel;
    CallbackVector m_callbacks;
};
BB_DECLARE_SHARED_PTR( SequenceCheckingRecvTransport );


/// Base class for stream-oriented (SOCK_STREAM) transports
class StreamRecvTransport : public IRecvTransport
{
public:
    virtual void endDispatch();

protected:
    static const int DEFAULT_MEM_BUFFER_SIZE = 1024*1024;

    StreamRecvTransport(size_t processRecvBufSize = DEFAULT_MEM_BUFFER_SIZE );

    void beginDispatchImpl(FDSet &fdSet, const FDPtr &source, const ReactorCallback &readCB,
            IMStreamCallback *messageCB, const EndOfStreamCB &endOfStreamCB, const ErrorCB &errorCB);
    const Msg* constructMsg(); // ends dispatching and throws if the stream becomes corrupt

    Buffer m_recvBuffer;
    MsgBuffer m_msgBuf;

    Subscription m_readSub;
    IMStreamCallback *m_messageCB;
    EndOfStreamCB m_endOfStreamCB;
    ErrorCB m_errorCB;

    DestructorMarker m_dtor;

};
BB_DECLARE_SHARED_PTR(StreamRecvTransport);


/// Receives BB messages over a TCP socket, with internal buffering.
///
/// Strategy: call recv once into a big circular buffer, then loop until all the
/// messages have been read out of the buffer. This is one-copy (messages will be
/// constructed directly out of the buffer); this requires all message sizes to
/// be MSG_MAXALIGN aligned.
class TCPSocketRecvTransport : public StreamRecvTransport
{
public:
    TCPSocketRecvTransport(TCPSocketPtr s,
                           size_t kernelRecvBufSize = DEFAULT_RECV_BUF_SIZE,
                           size_t processRecvBufSize = DEFAULT_MEM_BUFFER_SIZE);

    /// enableQuickAck will enable the following algorithm on every message:
    /// 1) was there a response message sent? do nothing
    /// 2) otherwise, trigger an immediate ACK on the TCP socket.
    /// Without this, the delayed ack algorithm in TCP can introduce latency on message reception.
    /// Some information about delayed ack is found at
    /// http://www.stuartcheshire.org/papers/NagleDelayedAck/
    void enableQuickAck(BaseTCPSendTransportPtr quickAckSender);

    /// IRecvTransport impl
    virtual void beginDispatch(FDSet &d, IMStreamCallback *messageCB,
            const EndOfStreamCB &endOfStreamCB = EndOfStreamCB(),
            const ErrorCB &errorCB = ErrorCB());
    TCPSocketPtr getSocket();

protected:
    void onSelectCallback();
    bool fillBuffer(); // returns true on EOF

    util::MsgBuf m_cntlBuffer;
    TCPSocketPtr m_source;
    BaseTCPSendTransportPtr m_quickAckSender;

    struct ::msghdr      m_msghdr;
    struct ::iovec       m_iovec;
    struct ::sockaddr_in m_from;
};
BB_DECLARE_SHARED_PTR( TCPSocketRecvTransport );


/// Receives BB messages from a SOCK_STREAM Unix socket.
/// Implementation stolen from TCPSocketRecvTransport.
class UnixSocketRecvTransport : public StreamRecvTransport
{
public:
    UnixSocketRecvTransport(UnixSocketPtr s,
            size_t processRecvBufSize = DEFAULT_MEM_BUFFER_SIZE);

    /// IRecvTransport impl
    virtual void beginDispatch(FDSet &d, IMStreamCallback *messageCB,
            const EndOfStreamCB &endOfStreamCB = EndOfStreamCB(),
            const ErrorCB &errorCB = ErrorCB());

protected:
    bool fillBuffer();
    void onSelectCallback();

    UnixSocketPtr m_source;
};
BB_DECLARE_SHARED_PTR( UnixSocketRecvTransport );


/// Receives no messages, ever.
class DevNullRecvTransport : public IRecvTransport
{
public:
    /// IRecvTransport impl
    virtual void beginDispatch(FDSet &d, IMStreamCallback *messageCB,
            const EndOfStreamCB &endOfStreamCB = EndOfStreamCB(),
            const ErrorCB &errorCB = ErrorCB()) {}
    virtual void endDispatch() {}
};
BB_DECLARE_SHARED_PTR( DevNullRecvTransport );

/// Receives BB messages from a message oriented SCTP socket.
class SCTPSocketRecvTransport : public IRecvTransport
{
public:
    SCTPSocketRecvTransport(const SCTPSocketPtr& s, const size_t mMaxMessages = 256);

    /// IRecvTransport impl
    virtual void beginDispatch(FDSet &d, IMStreamCallback *messageCB,
            const EndOfStreamCB &endOfStreamCB = EndOfStreamCB(),
            const ErrorCB &errorCB = ErrorCB());
    virtual void endDispatch();

    SCTPSocketPtr getSocket();
protected:
    class ReadyCallback;
    void onSelectCallback();

    const SCTPSocketPtr m_source;
    const size_t m_maxMessages;

    Subscription m_readSub;
    IMStreamCallback *m_messageCB;
    EndOfStreamCB m_endOfStreamCB;
    ErrorCB m_errorCB;

    boost::scoped_array<MsgBuffer> m_buffers;
    boost::scoped_array<struct ::iovec> m_iovs;
    boost::scoped_array<struct ::mmsghdr> m_headers;
    boost::scoped_array<struct ::sockaddr_in> m_from;

    DestructorMarker m_dtor;
};
BB_DECLARE_SHARED_PTR( SCTPSocketRecvTransport );

} // namespace bb

#endif // BB_IO_RECVTRANSPORT_H

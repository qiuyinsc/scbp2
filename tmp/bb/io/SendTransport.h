#ifndef BB_IO_SENDTRANSPORT_H
#define BB_IO_SENDTRANSPORT_H

/* Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved. */

#include <sstream>

#include <bb/core/Log.h>
#include <bb/core/Msg.h>
#include <bb/core/JsonCodeGen.h>
#include <bb/core/smart_ptr.h>
#include <bb/io/AsyncByteSink.h>
#include <bb/io/ISendTransport.h>

namespace bb {
class Msg;

/// ISendTransport abstracts out the sending of messages.
/// See RecvTransport.h for info on the protocol.
/// We have specialized send transports for UDP and TCP sockets.

BB_FWD_DECLARE_SHARED_PTR( TCPSocket );
BB_FWD_DECLARE_SHARED_PTR( UDPSocket );
BB_FWD_DECLARE_SHARED_PTR( UnixSocket );
BB_FWD_DECLARE_SHARED_PTR( QuantileCalculator );
BB_FWD_DECLARE_SHARED_PTR( SCTPSocket );

/// Sends dgram-format messages over a UDP socket.
class UDPSocketSendTransport : public ISendTransport {
public:
    UDPSocketSendTransport(UDPSocketPtr s);
    virtual ~UDPSocketSendTransport();

    /// Sends the message over the Socket.
    /// Throws an exception (usually CError) if an error occurred.
    virtual void send(const Msg *);

    UDPSocketPtr getSocket() { return m_sink; }

protected:
    UDPSocketPtr m_sink;

    unsigned long m_bytesSent;
};
BB_DECLARE_SHARED_PTR( UDPSocketSendTransport );



class GzipUDPSocketSendTransport : public UDPSocketSendTransport {
public:
    GzipUDPSocketSendTransport(UDPSocketPtr s);
    virtual ~GzipUDPSocketSendTransport();

    /// Sends the message over the Socket.
    /// Throws an exception (usually CError) if an error occurred.
    virtual void send(const Msg *);
protected:
    boost::scoped_array<unsigned char> m_compressBuffer;
    unsigned long m_compressBufferLen;

    unsigned long m_uncompressedBytes;
};
BB_DECLARE_SHARED_PTR( GzipUDPSocketSendTransport );


/// Common baseclass for  TCPSocketSendTransport and NonBlockingTCPSendTransport
class BaseTCPSendTransport : public ISendTransport
{
public:
    BaseTCPSendTransport(TCPSocketPtr s) : m_sock(s), m_ackNeeded(false) {}

    TCPSocketPtr getSocket() { return m_sock; }

    /// If there has been no data sent since the last time resetAckNeeded() was called,
    /// sends a quick ack.
    void ackData() { if(m_ackNeeded) sendQuickAck(); }
    void resetAckNeeded() { m_ackNeeded = true; }
    void noAckNeeded() { m_ackNeeded = false; }

    /// Like send() but sends just one byte at a time
    /// One use of this is to intentionally enlarge the TCP congestion window
    virtual void sendOneByteAtATime(const Msg *) = 0;

protected:
    /// Forces the TCP stack to ACK some recv'd data, useful for avoiding TCP delayed ACKs.
    virtual void sendQuickAck() = 0;

    TCPSocketPtr m_sock;
    bool m_ackNeeded;
};
BB_DECLARE_SHARED_PTR(BaseTCPSendTransport);


/// Sends stream-formated messages over a TCP socket. Operates in blocking mode.
class TCPSocketSendTransport : public BaseTCPSendTransport
{
public:
    TCPSocketSendTransport(TCPSocketPtr s);

    /// Sends the message over the Socket.
    /// Throws a exception (usually CError) if an error occurred.
    virtual void send(const Msg *);

    virtual void sendOneByteAtATime(const Msg *);

    /// Forces the TCP stack to ACK some recv'd data, useful for avoiding TCP delayed ACKs.
    virtual void sendQuickAck();
};
BB_DECLARE_SHARED_PTR( TCPSocketSendTransport );


/// Sends messages in stream format over a TCPSocket using an AsyncByteSink.
class NonBlockingTCPSendTransport : public BaseTCPSendTransport
{
public:
    typedef AsyncByteSink::ErrorCB ErrorCB;
    NonBlockingTCPSendTransport(TCPSocketPtr sink, FDSet& fdset, const ErrorCB &errCB = ErrorCB());

    /// Throws an exception if an error occurred, typically propagating ByteSink failures.
    /// Errors can come after send() returns; see the documentation re: ErrorCB in AsyncByteSink.h
    virtual void send(const Msg *m);
    virtual void sendOneByteAtATime(const Msg *);
    virtual void sendQuickAck();

protected:
    AsyncByteSink m_async;
};
BB_DECLARE_SHARED_PTR(NonBlockingTCPSendTransport);


/// Sends messages in datagram format over a UnixSocket using an AsyncByteSink.
class NonBlockingUnixSendTransport : public ISendTransport
{
public:
    typedef AsyncByteSink::ErrorCB ErrorCB;
    NonBlockingUnixSendTransport(UnixSocketPtr const &sink, FDSet& fdset, const ErrorCB &errCB = ErrorCB());

    virtual void send(const Msg *m);

protected:
    AsyncByteSink m_async;
};
BB_DECLARE_SHARED_PTR(NonBlockingUnixSendTransport);


/// Sends messages in datagram format over a generic ByteSink.
/// There are better alternatives for TCP, UDP, etc. etc.
class DGramWriteTransport : public ISendTransport
{
public:
    DGramWriteTransport(ByteSinkPtr s) : m_sink(s) {}

    /// Throws an exception if an error occurred, typically propagating ByteSink failures.
    void send(const Msg *m);

protected:
    ByteSinkPtr m_sink;
};
BB_DECLARE_SHARED_PTR(DGramWriteTransport);


/// Sends messages in text format over an ostream.
class OStreamTextWriteTransport : public ISendTransport
{
public:
    OStreamTextWriteTransport(std::ostream &s) : m_sink(s) {}

    /// Throws an exception if an error occurred.
    void send(const Msg *m);

protected:
    std::ostream &m_sink;
};
BB_DECLARE_SHARED_PTR(OStreamTextWriteTransport);


/// Sends messages in text format to stdout
class StdOutTextWriteTransport : public OStreamTextWriteTransport
{
public:
    StdOutTextWriteTransport() : OStreamTextWriteTransport(std::cout) {}
};
BB_DECLARE_SHARED_PTR(StdOutTextWriteTransport);


// Serializer must take a const Msg& and have operator<< overloaded
struct NoSettings
{
    NoSettings(){}
};


template<typename Serializer, typename Settings>
class SerializeSendTransport : public ISendTransport
{
public:
    SerializeSendTransport( const ByteSinkPtr &s, const Settings& settings = Settings() )
        : m_sink(s)
        , m_settings( settings )
    {
    }

    /// Throws an exception if an error occurred.
    void send( const Msg *m )
    {
        std::ostringstream stream;
        stream << Serializer( *m, m_settings ) << '\n';
        m_sink->write(stream.str().data(), stream.tellp());
    }

protected:
    ByteSinkPtr m_sink;
    Settings m_settings;
};


template<typename Serializer>
class SerializeSendTransport<Serializer,NoSettings> : public ISendTransport
{
public:
    SerializeSendTransport( const ByteSinkPtr &s )
        : m_sink(s)
    {
    }

    void send( const Msg *m )
    {
        std::ostringstream stream;
        stream << Serializer( *m ) << '\n';
        m_sink->write(stream.str().data(), stream.tellp());
    }
protected:
    ByteSinkPtr m_sink;
};

typedef Msg& TextMsgSerializer;
typedef SerializeSendTransport<TextMsgSerializer,NoSettings> TextWriteTransport;
BB_DECLARE_SHARED_PTR(TextWriteTransport);

typedef JsonMode<Msg> JsonMsgSerializer;
typedef SerializeSendTransport<JsonMsgSerializer,NoSettings> JsonWriteTransport;
BB_DECLARE_SHARED_PTR(JsonWriteTransport);

/// Prints the difference between message generation and origin timestamps (for performance testing).
class TimeDiffTextWriteTransport : public ISendTransport
{
public:
    TimeDiffTextWriteTransport(std::ostream &s);

    /// Throws an exception if an error occurred.
    void send(const Msg *m);

protected:
    std::ostream &m_sink;
    QuantileCalculatorPtr m_stats;
};
BB_DECLARE_SHARED_PTR(TimeDiffTextWriteTransport);


class DevNullSendTransport : public ISendTransport
{
public:
    virtual void send(const Msg *msg) {}
};
BB_DECLARE_SHARED_PTR(DevNullSendTransport);


class BaseSCTPSendTransport : public ISendTransport
{
public:
    BaseSCTPSendTransport(const SCTPSocketPtr& s) : m_sock(s) {}

    const SCTPSocketPtr& getSocket() { return m_sock; }

protected:
    const SCTPSocketPtr m_sock;
};
BB_DECLARE_SHARED_PTR(BaseSCTPSendTransport);

class SCTPSocketSendTransport : public BaseSCTPSendTransport
{
public:
    SCTPSocketSendTransport(const SCTPSocketPtr& s);

    virtual void send(const Msg *);
};
BB_DECLARE_SHARED_PTR(SCTPSocketSendTransport);

/// Sends messages in stream format over a TCPSocket using an AsyncByteSink.
class NonBlockingSCTPSendTransport : public BaseSCTPSendTransport
{
public:
    typedef AsyncByteSink::ErrorCB ErrorCB;
    NonBlockingSCTPSendTransport(const SCTPSocketPtr& sink, FDSet& fdset, const ErrorCB &errCB = ErrorCB());

    /// Throws an exception if an error occurred, typically propagating ByteSink failures.
    /// Errors can come after send() returns; see the documentation re: ErrorCB in AsyncByteSink.h
    virtual void send(const Msg *m);

protected:
    AsyncByteSink m_async;
};
BB_DECLARE_SHARED_PTR(NonBlockingSCTPSendTransport);

} // namespace bb

#endif // BB_IO_SENDTRANSPORT_H

#ifndef BB_IO_XMLLOG_H
#define BB_IO_XMLLOG_H

/* Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved. */


#include <cstdio>
#include <iosfwd>
#include <vector>

#include <boost/interprocess/streams/vectorstream.hpp>
#include <boost/io/ios_state.hpp>
#include <boost/compressed_pair.hpp>
#include <boost/noncopyable.hpp>
#include <boost/scoped_ptr.hpp>
#include <boost/variant.hpp>

#include <bb/core/timeval.h>
#include <bb/core/hash_map.h>
#include <bb/core/tif.h>
#include <bb/core/side.h>
#include <bb/core/acct.h>
#include <bb/core/liquidity.h>
#include <bb/core/ostatus.h>
#include <bb/core/cxlstatus.h>
#include <bb/core/symbol.h>
#include <bb/core/fillflag.h>

#include <bb/io/ByteSink.h>

// predeclare genx stuff
struct genxWriter_rec; typedef struct genxWriter_rec * genxWriter;
struct genxNamespace_rec; typedef struct genxNamespace_rec * genxNamespace;
struct genxElement_rec; typedef struct genxElement_rec * genxElement;
struct genxAttribute_rec; typedef struct genxAttribute_rec * genxAttribute;

namespace bb {

/** XML log output object
 *
 * Writes all output as XML to a stream.
 *
 * @code
 * bb::XmlLog log( cout );
 *
 * XmlElem err(log,"error");
 * err
 *   << bb::attr( "id", "OM::onEvent" )
 *   << bb::attr( "osz", 12 )
 *   << bb::attr( "price", 15.6 )
 *   << bb::attr( "comment", string( "Assuming daemon is right somehow" ) );
 * {
 *     XmlElem(err,"interesting")
 *       << bb::attr( "info", 11 );
 * }
 * @endcode
 *
 * Output:
 * <pre>
 * <error comment="Assuming daemon is right somehow" id="OM::onEvent" osz="12" price="15.6"><interesting info="11"></interesting></error>
 * </pre>
 */
class XmlLog
{
public:
    struct XMLComment {
        XMLComment(std::string t) : m_text(t) {}
        std::string m_text;
    };
    typedef genxAttribute Attribute;
    typedef genxElement Element;

    XmlLog( std::ostream& os, const std::string &tagWhitespace = "\n" );
    XmlLog( const boost::shared_ptr<std::ostream> &os, const std::string &tagWhitespace = "\n" );
    XmlLog( const ByteSinkPtr &bs, const std::string &tagWhitespace = "\n" );
    virtual ~XmlLog();

    /// returns a text description of the underlying library (for testing only)
    const std::string getLibDesc() const;

    /// Shorthand for starting a tag
    XmlLog& operator % ( const char *_tag );
    XmlLog& operator % ( const std::string& _tag ) { return operator%(_tag.c_str()); }
    XmlLog& operator % ( const Element & _tag );
    XmlLog& operator % ( const XMLComment & );

    XmlLog& operator << ( XmlLog& (*term) (XmlLog&) )
    {
        return term( *this );
    }

    XmlLog& operator << ( const char* text );
    XmlLog& operator << ( const std::string &str );
    XmlLog& operator << ( const bb::timeval_t& time );

    /// dummy class to return the implementation defined timestamp
    class logtime {};
    const logtime time;
    XmlLog& operator << ( const logtime& time ) { return (*this) << getSelfTime(); }

    virtual void addAttribute( const char *key, const char *value );
    virtual void addAttribute( Attribute attr, const char *value );

    /// Predeclares elements and attributes, which can then be passed around instead of const char *.
    /// For genx, this makes things much faster.
    Element declareElement(const char *name);
    Element getCachedElement(const char *name);  // name must be a pointer to a constant literal.

    Attribute declareAttribute(const char *name);
    Attribute getCachedAttribute(const char* name);  // name must be a pointer to a constant literal.

protected:

    // We cache and re-use an ostream for the generic xml_attr_printer
    // because constructin ostreams is stupidly expensive, mostly due
    // to locale construction. We use a ovectorstream so we can swap
    // out the results, rather than having to copy like we would with
    // ostringstream.
    typedef boost::interprocess::basic_ovectorstream<std::vector<char> > ovstream;

    template<typename T> friend struct xml_attr_printer;
    inline ovstream& getCachedOutputStream()
    {
        // clear any errors from earlier IO.
        // clear is ambiguous in boost 1.45 because ovectorstream has two base classes with clear methods
        m_cachedStream.std::ostream::clear();
        m_cachedStream.rdbuf()->clear();  // restart at beginning of stream
        return m_cachedStream;
    }

    /// returns the current depth of the XML tree, 1 means we're formatting the root element.
    int32_t getDepth() const { return m_depth; }

    friend XmlLog& end( XmlLog& log );
    friend class XmlElem;
    virtual void startXml() {}
    virtual void endXml();
    virtual const bb::timeval_t getSelfTime() const { return bb::timeval_t::now; }

    genxWriter m_writer;
    int32_t m_depth;

    // reference to the output stream, for automatic storage management. can be NULL
    boost::variant<boost::shared_ptr<std::ostream>, ByteSinkPtr> m_outputPtr;
    boost::shared_ptr<void> m_sender; // pointer to genxSender; can't fwd declare unfortunately
    std::string m_tagWhitespace;

    Attribute m_tsAttribute;
    void *m_userData;

    typedef bbext::hash_map<const char*, Attribute> AttributeCache;
    AttributeCache m_attributeCache;

    typedef bbext::hash_map<const char*, Element> ElementCache;
    ElementCache m_elementCache;

    ovstream m_cachedStream;

private:
    XmlLog(const XmlLog &);
    XmlLog &operator =(const XmlLog &);
};
BB_DECLARE_SHARED_PTR(XmlLog);

inline XmlLog& end( XmlLog& log )
{
    log.endXml();
    return log;
}

/// Base class for XmlElem; this represents an XML element which you
/// can only add content to, not attributes.
struct XmlElemContent : public boost::noncopyable
{
    XmlElemContent& operator<< ( const std::string &str ) { m_log << str; return *this; }
    XmlElemContent& operator<< ( const char* text ) { m_log << text; return *this; }

    XmlLog &getLog() { return m_log; }
protected:
    friend class XmlElem;
    XmlElemContent( XmlLog &log ) : m_log(log) {}
    XmlLog &m_log;
};

/// Auto-ending RAII tag. Example:
/// XmlElem(log, "foo")
///    << attr("bar", 42);
/// writes <foo bar="42"></foo>.
class XmlElem : public XmlElemContent
{
public:
    XmlElem( XmlLog &log, const char *tag ) : XmlElemContent(log) { m_log % tag; }
    XmlElem( XmlLog &log, const std::string& tag ) : XmlElemContent(log) { m_log % tag; }
    XmlElem( XmlLog &log, const XmlLog::Element & tag ) : XmlElemContent(log) { m_log % tag; }
    XmlElem( XmlElemContent &parentTag, const char *tag ) : XmlElemContent(parentTag.m_log) { m_log % tag; }
    XmlElem( XmlElemContent &parentTag, const std::string& tag ) : XmlElemContent(parentTag.m_log) { m_log % tag; }
    XmlElem( XmlElemContent &parentTag, const XmlLog::Element & tag ) : XmlElemContent(parentTag.m_log) { m_log % tag; }
    ~XmlElem();
    template<typename T>
    XmlElem &operator<<(const T& t) { m_log << t; return *this; }
};


template<typename T> struct xml_attr_printer
{
    const char *operator()(XmlLog& log, const T& t) const
    {
        XmlLog::ovstream& stream = log.getCachedOutputStream();
        boost::io::ios_base_all_saver ias( stream );
        stream << t << '\0';
        return &stream.vector()[0];
    }
};

template<> struct xml_attr_printer<std::string>
{
    inline const char *operator()(XmlLog& log, const std::string& t) const { return t.c_str(); }
};

template<> struct xml_attr_printer<char *>
{
    inline const char *operator()(XmlLog& log, char *s) const { return s; }
};

template<> struct xml_attr_printer<const char *>
{
    inline const char *operator()(XmlLog& log, const char *s) const { return s; }
};

template<> struct xml_attr_printer<double>
{
    inline const char *operator()(XmlLog& log, const double& t) const { snprintf(m_buf, 50, "%g", t); return m_buf; }
    mutable char m_buf[50];
};

template<> struct xml_attr_printer<int>
{
    inline const char *operator()(XmlLog& log, const int& t) const { snprintf(m_buf, 25, "%d", t); return m_buf; }
    mutable char m_buf[25];
};

template<> struct xml_attr_printer<unsigned int>
{
    inline const char *operator()(XmlLog& log, const int& t) const { snprintf(m_buf, 25, "%u", t); return m_buf; }
    mutable char m_buf[25];
};

template<> struct xml_attr_printer<size_t>
{
    inline const char *operator()(XmlLog& log, const size_t& t) const { snprintf(m_buf, 25, "%zu", t); return m_buf; }
    mutable char m_buf[25];
};

template<> struct xml_attr_printer<bool>
{
    inline const char *operator()(XmlLog& log, const bool& b) const { return b ? "1" : "0"; }
};

template<> struct xml_attr_printer<tif_t>
{
    inline const char *operator()(XmlLog& log, const tif_t& t) const { return tif2str(t); }
};

template<> struct xml_attr_printer<dir_t>
{
    inline const char *operator()(XmlLog& log, const dir_t& d) const { return dir2str(d); }
};

template<> struct xml_attr_printer<ostatus_t>
{
    inline const char *operator()(XmlLog& log, const ostatus_t& t) const { return ostatus2str(t); }
};

template<> struct xml_attr_printer<mktdest_t>
{
    inline const char *operator()(XmlLog& log, const mktdest_t& m) const { return mktdest2str(m); }
};

template<> struct xml_attr_printer<acct_t>
{
    inline const char *operator()(XmlLog& log, const acct_t& a) const { return acct2str(a); }
};

template<> struct xml_attr_printer<cxlstatus_t>
{
    inline const char *operator()(XmlLog& log, const cxlstatus_t& c) const { return cxlstatus2str(c); }
};

template<> struct xml_attr_printer<symbol_t>
{
    inline const char *operator()(XmlLog& log, const symbol_t& s) const
    {
        const char* str = sym2str(s);
        return str ? str : "SYM_UNKNOWN";
    }
};


template<typename T> class xml_attr_obj
{
public:
    /// if xml_attr_printer<T> is an empty class, apply the empty base class optimization
    typedef boost::compressed_pair<T, xml_attr_printer<T> > TPair;

    xml_attr_obj( const std::string& key, const T& v ) : m_key( key ), m_val( TPair(v, xml_attr_printer<T>()) ) {}

    const char *getValueStr( XmlLog& log ) const { return m_val.second()(log, m_val.first()); }
    std::string m_key;
    TPair m_val;
};

template<typename T> class predecl_attr_obj
{
public:
    /// if xml_attr_printer<T> is an empty class, apply the empty base class optimization
    typedef boost::compressed_pair<T, xml_attr_printer<T> > TPair;

    predecl_attr_obj( XmlLog::Attribute key, const T& v )
        : m_key( key ), m_val( TPair(v, xml_attr_printer<T>()) ) {}

    const char *getValueStr( XmlLog& log ) const { return m_val.second()(log, m_val.first()); }
    XmlLog::Attribute m_key;
    TPair m_val;
};

template<typename T> class cache_attr_obj
{
public:
    typedef boost::compressed_pair<T, xml_attr_printer<T> > TPair;

    cache_attr_obj( const char* key, const T& v )
        : m_key( key )
        , m_val( TPair(v, xml_attr_printer<T>()) ) {}

    const char *getValueStr( XmlLog& log ) const { return m_val.second()(log, m_val.first()); }
    const char* m_key;
    TPair m_val;
};

template<typename T> inline const xml_attr_obj<T> attr( const std::string& key, const T& val )
{
    return xml_attr_obj<T>( key, val );
}

template<typename T> inline const predecl_attr_obj<T> attr( XmlLog::Attribute key, const T& val )
{
    return predecl_attr_obj<T>( key, val );
}

template<typename T> inline const cache_attr_obj<T> attr_literal( const char* key, const T& val )
{
    return cache_attr_obj<T>( key, val );
}


// The following overloads are necessary to bind to const char[] values.
template<size_t N>
inline const xml_attr_obj<const char*> attr( const std::string& key, const char (&val)[N] )
{
    return xml_attr_obj<const char*>( key, &val[0] );
}

template<size_t N>
inline const predecl_attr_obj<const char*> attr( XmlLog::Attribute key, const char (&val)[N] )
{
    return predecl_attr_obj<const char*>( key, &val[0] );
}

template<size_t N>
inline const cache_attr_obj<const char*> attr_literal( const char* key, const char (&val)[N] )
{
    return cache_attr_obj<const char*>( key, &val[0] );
}


inline bb::XmlLog& operator << ( XmlLog &log, int t )
{
    char buf[25];
    snprintf(buf, 25, "%d", t);
    log << buf;
    return log;
}

inline bb::XmlLog& operator << ( XmlLog &log, double t )
{
    char buf[50];
    snprintf(buf, 50, "%g", t);
    log << buf;
    return log;
}

template<typename T> inline XmlLog& operator << ( XmlLog &log, const xml_attr_obj<T>& attrib )
{
    log.addAttribute(attrib.m_key.c_str(), attrib.getValueStr(log));
    return log;
}

template<typename T> inline XmlLog& operator << ( XmlLog &log, const predecl_attr_obj<T>& attrib )
{
    log.addAttribute(attrib.m_key, attrib.getValueStr(log));
    return log;
}

template<typename T> inline XmlLog& operator << ( XmlLog &log, const cache_attr_obj<T>& attrib )
{
    log.addAttribute(log.getCachedAttribute( attrib.m_key ), attrib.getValueStr(log));
    return log;
}


inline XmlLog::XMLComment xmlComment(std::string text) { return XmlLog::XMLComment(text); }

} // namespace bb

#endif // BB_IO_XMLLOG_H

#ifndef BB_IO_ZBYTESINK_H
#define BB_IO_ZBYTESINK_H

/* Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved. */


#include <zlib.h>
#include <boost/noncopyable.hpp>
#include <boost/scoped_array.hpp>
#include <bb/io/ByteSink.h>

namespace bb {

// ByteSink compression adapter
class ZByteSink : public ByteSink, public boost::noncopyable
{
public:
    static const size_t SYNC_SIZE;
    enum FormatType { GZ_FORMAT, ZLIB_FORMAT };

    // bs is the stream which will get the raw compressed data.
    // syncInterval is the (approximate) distance in bytes between sync points,
    // which can be used for coarse-grained random access during decompression.
    // the best values for syncInterval are probably multiples of 16*1024.
    // syncInterval of 0 means don't insert full sync points.
    // flush() also inserts full sync points if syncInterval is nonzero.
    // This can throw std::exceptions.
    ZByteSink(const ByteSinkPtr& bs, FormatType format = GZ_FORMAT,
            int compressionLevel = Z_DEFAULT_COMPRESSION, size_t syncInterval = SYNC_SIZE );
    virtual ~ZByteSink();

    // flushes the bytesink -- note that this doesn't write the
    // the gzip trailer, so even if you call flush, there might be more
    // data to write when closing the file. This data will be written from
    // the destructor.
    // flushing may degrade compression -- use only when necessary.
    virtual void flush();

protected:
    virtual size_t ByteSink_write(const void *buf, size_t size);
    void do_write(const void *buf, size_t size, int flushFlag, const char* caller);
    void do_sync(const char* caller);

    ByteSinkPtr m_bs;

private:
    z_stream m_zs;
    static const size_t BUF_SIZE;
    boost::scoped_array<Bytef> m_buf;
    const size_t m_syncInterval;
    size_t m_bytesSinceSync;
};
BB_DECLARE_SHARED_PTR(ZByteSink);

} // namespace bb

#endif // BB_IO_ZBYTESINK_H

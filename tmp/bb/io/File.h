#ifndef BB_IO_FILE_H
#define BB_IO_FILE_H

/* Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved. */


#include <cstdio>

#include <bb/core/smart_ptr.h>
#include <bb/core/Error.h>

#include <bb/io/ByteSource.h>
#include <bb/io/ByteSink.h>


namespace bb {

/// Abstract representation of a file.
class File : public ByteSource
           , public ByteSink {
public:
    File();
    virtual ~File();

    /// Returns true if the file exists.  Doesn't matter if the file is open or not.
    virtual bool exists() const = 0;
    /// Opens the file.  Throws errors if file already opened, or
    /// if there was a problem opening.
    virtual void open() = 0;
    /// Returns true if the file is opened.
    virtual bool opened() const = 0;
    /// Flushes and closes file.
    virtual void close() = 0;
    /// Returns true if EOF.
    virtual bool eof() const = 0;
    /// Flushes file.
    virtual void flush() = 0;


    // some error classes

    /// Problem opening file.
    class OpenError : public Error {
    public:
        OpenError()
            : Error( "File::OpenError" ) { }
        OpenError( const std::string &s )
            : Error( std::string( "File::OpenError " ) + s ) { }
        OpenError( const char *s )
            : Error( std::string( "File::OpenError ") + s ) { }
    };

    /// File does not exist
    class MissingFileError : public Error {
    public:
        MissingFileError( const char *filename )
            : Error( "File::MissingFileError %s", filename ) { }
    };

    /// File already opened.
    class AlreadyOpenedError : public OpenError {
    public:
        AlreadyOpenedError()
            : OpenError( "File::AlreadyOpenedError" ) { }
    };

    /// File not opened error.
    class NotOpenedError : public Error {
    public:
        NotOpenedError()
            : Error( "File::NotOpenedError" ) { }
    };

    /// Tried to write with NULL pointer error.
    class NullError : public Error {
    public:
        NullError()
            : Error( "File::NullError" ) { }
    };

};

BB_DECLARE_SHARED_PTR( File );



} // namespace bb

#endif // BB_IO_FILE_H

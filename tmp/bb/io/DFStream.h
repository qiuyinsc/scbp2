#ifndef BB_IO_DFSTREAM_H
#define BB_IO_DFSTREAM_H

/* Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved. */


#include <locale> // needed by boost/format/format_fwd.hpp
#include <vector>

#include <boost/tuple/tuple.hpp>
#include <boost/scoped_array.hpp>
#include <boost/shared_ptr.hpp>
#include <boost/noncopyable.hpp>
#include <boost/tuple/tuple.hpp>
#include <boost/format/format_fwd.hpp>
#include <boost/date_time/gregorian/greg_date.hpp>

#include <bb/core/ptime.h>
#include <bb/core/source.h>
#include <bb/core/symbol.h>
#include <bb/core/gdate.h>
#include <bb/core/MStream.h>
#include <bb/core/UnixSig.h>
#include <bb/core/Subscription.h>

#include <bb/io/ByteSource.h>
#include <bb/io/SingleDFStream.h>
#include <bb/io/SymDataFile.h>

namespace bb {

// Forward declarations
class Msg;
class MsgBuffer;
BB_FWD_DECLARE_SHARED_PTR( CFile );
BB_FWD_DECLARE_SHARED_PTR( ZByteSource );
BB_FWD_DECLARE_SHARED_PTR( OnIdleMsg );


/** A MemMStream consisting of an arbitrary in-memory Msgs which will be sorted in time order */
class MemMStream : public HistMStream
{
public:
    MemMStream();
    virtual ~MemMStream();

    /// Adds a message to the queue. This can be done at any time, as long as
    /// the message comes after all the messages which have already been removed
    /// from this mstream.
    ///
    /// The Msg is cloned, so there are no issues concerning ownership.
    void addMessage( const Msg& msg );

    virtual const Msg* next();

protected:
    struct MsgHeapCmp;

    typedef std::vector<const Msg*> MsgHeap;
    MsgHeap m_msgHeap;
    const Msg* m_lastMsg;
};
BB_DECLARE_SHARED_PTR( MemMStream );

/** A class that introduces a fixed time delay to a message stream. */
class FixedDelayDFStream : public HistMStream
{
public:
    /// Takes a stream, S, and introduces a delay to each of the
    /// messages.  FixedDelayDFStream owns the S that is passed in.
    FixedDelayDFStream( const HistMStreamPtr& s, const bb::ptime_duration_t& delay );
    virtual ~FixedDelayDFStream();

    virtual const Msg* next();

protected:
    HistMStream*     m_stream;
    HistMStreamPtr   m_spStream;
    ptime_duration_t m_delay;
};

BB_DECLARE_SHARED_PTR( FixedDelayDFStream );

/// A class that introduces replaces the send_time with the origin time + the specified delay.
/// If the origin time is not available then the msg is left unchanged
class OriginTimeDFStream : public FixedDelayDFStream
{
public:
    /// Takes a stream, S, and introduces a delay to each of the
    /// messages.  FixedDelayDFStream owns the S that is passed in.
    OriginTimeDFStream( const HistMStreamPtr& s, const bb::ptime_duration_t& delay );
    virtual ~OriginTimeDFStream(){};

    virtual const Msg* next();
};
BB_DECLARE_SHARED_PTR( OriginTimeDFStream );

/// A class that introduces replaces the send_time with the nic time + the specified delay.
/// If the nic time is not available then the msg is left unchanged
class NicTimeDFStream : public FixedDelayDFStream
{
public:
    /// Takes a stream, S, and introduces a delay to each of the
    /// messages.  FixedDelayDFStream owns the S that is passed in.
    NicTimeDFStream( const HistMStreamPtr& s, const bb::ptime_duration_t& delay );
    virtual ~NicTimeDFStream(){};

    virtual const Msg* next();
};
BB_DECLARE_SHARED_PTR( NicTimeDFStream );

/** Takes multiple HistMStream and multiplexes them, producing a
    single HistMStream in time sorted order.  The internal
    algorithm is a heap of pair<const Msg*, HistMStream>. **/
class DFStreamMplex : public HistMStream
{
public:
    DFStreamMplex();
    virtual ~DFStreamMplex();

    void sigAction( int sig, const UnixSigCallback& cb );

    /// Adds this MFStream.
    void add( const IHistMStreamPtr& df );
    unsigned int size() const { return m_streams.size(); }

    virtual const Msg* next();

    /// Override run() to poll the UnixSigDispatcher for pending signals, every message.
    void run( IMStreamCallback* c );

    IHistMStreamCPtr getOrigin() const;

    // re-implement this function to check for timeouts that are too small and would
    // kill simulation
    virtual void setNextTimeout(const bb::ptime_duration_t &timeout);

private:
    bool shouldSendIdle( const Msg* nextMsg);
    void bootstrapNewStreams();

    typedef boost::tuple<const Msg*, IHistMStreamPtr, bb::timeval_t> MsgStreamPair;
    struct StreamHeapCmp;

    /// The first m_heapSize elements of m_streams form a heap sorted by time,
    /// with the lowest time at the front of the heap.
    /// The remaining elements have not been added to the heap yet and are unsorted.
    typedef std::vector<MsgStreamPair> StreamHeap;
    StreamHeap m_streams;
    size_t m_heapSize;
    bool m_signalsRegistered;
    std::vector<Subscription> m_signalSubscriptions;
    const Msg*                      m_nextMsg;
    timeval_t                       m_lastMsgTime;
    OnIdleMsgPtr                    m_onIdleMsg;
};

BB_DECLARE_SHARED_PTR( DFStreamMplex );



} // namespace bb

#endif // BB_IO_DFSTREAM_H

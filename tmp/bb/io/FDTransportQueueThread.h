#ifndef BB_IO_FDTRANSPORTQUEUETHREAD_H
#define BB_IO_FDTRANSPORTQUEUETHREAD_H

/* Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved. */


#include <boost/thread.hpp>

#include <bb/core/queue/waitable_queue.h>
#include <bb/core/queue/ThreadPipeNotifier.h>

#include <bb/io/RecvTransport.h>

namespace bb {

/** Creates a thread that can write to a queue through a Transport
 * This object allows a process to create a thread and post message to a queue that
 * can be consumed through notification on messages through a transport.
 *
 * This simple mechanism allows applications to trigger events and operations to be
 * executed within the main EventDistributor thread by adding this object as a
 * transport to be selected upon.
 *
 * The thread does not start does not start until the start() method is called,
 * mainly because the thread might start before all its members are constructed.
 *
 */
template<typename T> class FDTransportQueueThread : public IRecvTransport
{
public:
    FDTransportQueueThread() {}

    virtual void run() = 0;
    virtual void onQueueNotify()
    {
        queue().pop();
    }
    void start()
    {
        m_driverThread.reset( new boost::thread( boost::bind( &FDTransportQueueThread<T>::run, this ) ) );
    }

protected:
    typedef bb::queue::waitable_queue<long, bb::queue::ThreadPipeNotifier> queue_t;
    virtual void beginDispatch( FDSet& sd, IMStreamCallback* messageCB,
            const EndOfStreamCB& endOfStreamCB, const ErrorCB& errorCB )
    {
        m_queue.getNotifier().registerDispatch(
            sd, m_readSub,
            boost::bind( &FDTransportQueueThread<T>::onQueueNotify, this ) );
    }

    virtual void endDispatch()
    {
        m_readSub.reset();
    }

    queue_t& queue() { return m_queue; }

    virtual const bb::Msg* recv() { return 0; }

private:
    queue_t m_queue;
    Subscription m_readSub;
    boost::scoped_ptr<boost::thread> m_driverThread;
};

} // namespace bb

#endif // BB_IO_FDTRANSPORTQUEUETHREAD_H

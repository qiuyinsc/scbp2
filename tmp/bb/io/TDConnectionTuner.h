#ifndef BB_IO_TDCONNECTIONTUNER_H
#define BB_IO_TDCONNECTIONTUNER_H

/* Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved. */


#include <bb/core/smart_ptr.h>
#include <bb/core/TDConnectionConfig.h>

namespace bb {
BB_FWD_DECLARE_SHARED_PTR(TCPSocket);

class TDConnectionTuner
{
public:
    void init(const TDConnectionConfig::TestRequests& config);

    // returns the text to be sent in each test request.
    // the specific text to send typically doesn't matter, any will do.
    const std::string& getText() const;

    // returns how many TCP packets worth of test requests
    // you should send right now. 0 means you're done completely.
    int numPacketsToSend(const TCPSocketPtr &sock);

    // Call after you've sent one test request;
    // adjusts m_count and remainingTCPCount, and returns true
    // if you need to continue sending in this iteration.
    bool continueIteration(int *remainingTCPCount, int tcpSent);

    // Logs info about the TCP state to LOG_INFO/WARN.
    static void logTCPInfo(const TCPSocketPtr &sock, const char *reason);

private:
    TDConnectionConfig::TestRequests m_config;
};

} // namespace bb

#endif // BB_IO_TDCONNECTIONTUNER_H

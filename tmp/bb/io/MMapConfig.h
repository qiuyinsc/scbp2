#ifndef BB_IO_MMAPCONFIG_H
#define BB_IO_MMAPCONFIG_H

/* Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved. */

#include <bb/core/LuaConfig.h>

namespace bb {

namespace mmap_config {

const int ALL_CHANNELS = -1;
const int NULL_CHANNEL = std::numeric_limits<uint32_t>::max();

class MMapSharedConfig : public LuaConfig<MMapSharedConfig> {
public:
    MMapSharedConfig() :
         mmap_min_messages_threshold(10)
        ,mmap_max_messages_threshold(400)
        ,mmap_max_page_lag_threshold(1)
        ,threshold_change_rate(2)
    {}

    static void describe() {
        Self::create()
            .param("mmap_min_messages_threshold", &Self::mmap_min_messages_threshold)
            .param("mmap_max_messages_threshold", &Self::mmap_max_messages_threshold)
            .param("mmap_max_page_lag_threshold", &Self::mmap_max_page_lag_threshold)
            .param("threshold_change_rate", &Self::threshold_change_rate);
    }

    uint32_t mmap_min_messages_threshold; // default messages threshold to read per call-back.
    uint32_t mmap_max_messages_threshold; // maximum messages threshold to read per call-back
    uint32_t mmap_max_page_lag_threshold; // page lag indicator, start incresing message threshold when
                                          // reader behind the sender by this amount of messages.

    uint32_t threshold_change_rate;        // by what ratio increase message rate when falling behind
};

} // close namespace mmap_config

} // close namespace bb

#endif // BB_IO_MMAPCONFIG_H

#ifndef BB_IO_MMAPSERVERSENDTRANSPORT_H
#define BB_IO_MMAPSERVERSENDTRANSPORT_H

/* Contents Copyright 2016 Athena Capital Research LLC. All Rights Reserved. */

#include <bb/core/FDSetFwd.h>
#include <bb/core/Log.h>
#include <bb/core/smart_ptr.h>

#include <bb/io/ISendTransport.h>

#include <luabind/luabind.hpp>

#include <sstream>

namespace bb {

BB_FWD_DECLARE_SHARED_PTR(MMapServerFeedType);
BB_FWD_DECLARE_SHARED_PTR(MMapSendTransport);

class MMapServerSendTransport : public ISendTransport {

    void init(FDSet& fdSet, luabind::object const& mmapconfig);

   public:
    // This constructor will try to lookup for mmap information in global
    // LuaState, exception will be thrown if mmap information is missing or
    // misconfigured.
    explicit MMapServerSendTransport(FDSet& fdSet);

    // Exception will be thrown if specified `mmapconfig` has misconfigured
    // mmap configuration.
    MMapServerSendTransport(FDSet& fdSet, luabind::object const& mmapconfig);

    virtual ~MMapServerSendTransport();

    virtual void send(const Msg*);

   protected:
    MMapServerFeedTypePtr m_mmapServerFeedType;
    MMapSendTransportPtr m_mmapSendTransport;
};

BB_DECLARE_SHARED_PTR(MMapServerSendTransport);

}  // namespace bb

#endif // BB_IO_MMAPSERVERSENDTRANSPORT_H

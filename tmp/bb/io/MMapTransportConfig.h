#ifndef BB_IO_MMAPTRANSPORTCONFIG_H
#define BB_IO_MMAPTRANSPORTCONFIG_H

/* Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved. */


#include <sys/types.h>
#include <string>
#include <bb/core/smart_ptr.h>
#include <vector>
#include <iosfwd>
#include <bb/core/Scripting.h>
#include <bb/io/MMapTransport.h>

namespace bb {

// Returns int(floor(log2(x)))
size_t ilog2(size_t x);

// Round up to a power of 2.
size_t clp2(size_t x);

BB_FWD_DECLARE_SHARED_PTR(MMapTransportConfig);

///This adapts a configuration spec (a lua file, or hardcoded
///so that the MMapTransportControl structure is properly initialized
class MMapTransportConfig
{
public:
    static MMapTransportConfigPtr construct(std::string const&sharedmem,std::string const&configfile);
    struct NotifierProperties
    {
        bool operator<(NotifierProperties const&r) const;
        bool operator==(NotifierProperties const&r) const;
        bool operator<(std::string const&r) const;
        bool operator==(std::string const&r) const;
        std::string m_id;
        friend std::ostream& operator<<(std::ostream&,
                NotifierProperties const&);
    };

    typedef std::vector<NotifierProperties> NotifierProperties_vec_t;
    struct ChannelProperties
    {
        bool operator<(ChannelProperties const&r) const;
        bool operator==(ChannelProperties const&r) const;
        bool operator<(std::string const&r) const;
        bool operator==(std::string const&r) const;
        ChannelProperties();
        std::string m_id;
        std::string m_dataPath;
        size_t m_fileSize;
        size_t m_pageSize;
        friend std::ostream& operator<<(std::ostream&, ChannelProperties const&);
    };

    typedef std::vector<ChannelProperties> ChannelProperties_vec_t;
    NotifierProperties_vec_t m_notifiers;
    ChannelProperties_vec_t m_channels;
    std::string m_socket_path;
    std::string m_configfile;
    std::string m_sharedmem;
    unsigned m_numnotifiers;
    friend std::ostream& operator<<(std::ostream&, MMapTransportConfig const&);
    static void registerScripting();
    MMapTransportConfig();

    // Initializes a fresh control structure with the configuration data.
    mmap_control::FilePtr initControl(mmap_lock_mode_t mode = MMAP_LOCK_SHARED_NB) const;

private:
    void initControlImpl(mmap_control::File *file) const;

    BB_DECLARE_SCRIPTING();
};

}
// namespace bb


#endif // BB_IO_MMAPTRANSPORTCONFIG_H

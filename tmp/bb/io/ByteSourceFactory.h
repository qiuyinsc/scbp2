#ifndef BB_IO_BYTESOURCEFACTORY_H
#define BB_IO_BYTESOURCEFACTORY_H

/* Contents Copyright 2014 Athena Capital Research LLC. All Rights Reserved. */

#include <string>

#include <bb/core/FDSet.h>
#include <bb/core/CoreConfig.h>
#include <bb/core/LuaConfig.h>
#include <bb/core/smart_ptr.h>
#include <bb/io/ByteSource.h>

namespace bb {
namespace threading{
class ThreadPool;
BB_FWD_DECLARE_SHARED_PTR(ThreadPool);
}

BB_FWD_DECLARE_SHARED_PTR(CFileFactory);
BB_FWD_DECLARE_SHARED_PTR(ByteSource);

static const std::string BYTESOURCE_CONFIG_THREADED = "threaded";
static const std::string BYTESOURCE_CONFIG_NONE = "none";

// We put ByteSourceConfig here instead of in ByteSourceFactory is to avoid linking
// the bbcore library against bbio.
class ByteSourceConfig : public LuaConfig<ByteSourceConfig>{
public:
   enum SourceType{
        kNone,
        kThreaded
    };

    ByteSourceConfig(SourceType type = kThreaded, size_t buffer_size = 1024 * 1024, size_t buffer_num = 16 );

    static void describe();

    ByteSourceConfig &setType(SourceType type);
    ByteSourceConfig &setBufferNum(size_t num);
    ByteSourceConfig &setBufferSize(size_t size);

    SourceType  getType() const;
    std::string getTypeString() const;
    size_t      getBufferNum() const;
    size_t      getBufferSize() const;

    std::string                               m_type;
    size_t                                    m_bufferSize;
    size_t                                    m_bufferNum;
};

std::ostream& operator <<( std::ostream& out, const ByteSourceConfig& config );

class ByteSourceFactory
{
public:
    ByteSourceFactory( const ByteSourceConfig& config, threading::ThreadPoolPtr threadPool = threading::ThreadPoolPtr() );

    void setThreadpool(threading::ThreadPoolPtr tpool);
    const threading::ThreadPoolPtr getThreadPool();
    void setConfig( const ByteSourceConfig& config );

    // Use this function if the Byte source already exists and only the wrapper
    // needs to be created.
    ByteSourcePtr create( ByteSourcePtr byteSource );

private:
    ByteSourceConfig            m_config;
    threading::ThreadPoolPtr    m_threadpool;
};
BB_DECLARE_SHARED_PTR(ByteSourceFactory);

} // namespace bb

#endif // BB_IO_BYTESOURCEFACTORY_H

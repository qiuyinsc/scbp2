#ifndef BB_IO_PCAPBYTESOURCE_H
#define BB_IO_PCAPBYTESOURCE_H

/* Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved. */


#include <pcap.h>

#include <bb/io/ByteSource.h>



namespace bb {

/** ByteSource for libpcap (tcpdump) capture UDP/multicast packets. */
class PCapOfflineByteSource : public ByteSource {
public:
    PCapOfflineByteSource(const char *filename);
    virtual ~PCapOfflineByteSource();

private:
    virtual size_t ByteSource_read(void *buf, size_t size);

    pcap_t *pcap;
    struct pcap_pkthdr pcap_hdr;

    char *errbuf;
    unsigned int errbuf_size;
};

BB_DECLARE_SHARED_PTR( PCapOfflineByteSource );

}


#endif // BB_IO_PCAPBYTESOURCE_H

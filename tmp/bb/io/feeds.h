#ifndef BB_IO_FEEDS_H
#define BB_IO_FEEDS_H

/* Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved. */

#include <string>
#include <vector>

#include <boost/optional.hpp>

#include <bb/core/network.h>
#include <bb/core/smart_ptr.h>
#include <bb/core/EFeedType.h>
#include <bb/core/EFeedOrig.h>
#include <bb/core/ESpinType.h>
#include <bb/core/host_port_pair.h>

#include <bb/io/IRecvTransport.h>

#include <boost/tuple/tuple.hpp>

namespace bb {

//BB_FWD_DECLARE_SHARED_PTR( IRecvTransport );
BB_FWD_DECLARE_SHARED_PTR( ISendTransport );

/// Subscribes to the a feed at the specified hostname and port.
/// Specifying the interface is optional.
/// Returns the RecvTransport to listen on, or throws CError on network/OS-level failure.
IRecvTransportPtr subscribeGeneral( const sockaddr_ipv4_t& sockaddr, const char* iface = NULL,
                                    EFeedType feed_type = SRC_UNKNOWN, bool* sequenceChecking = NULL );

/// Subscribes to the particular feed type.
/// The hostname/port is determined via the 'feed_channels' configuration entry.
/// Specifying the interface is optional.
/// Returns the RecvTransport to listen on.
/// sequenceChecking is an output parameter, set to true if sequence checking is enabled
/// Throws CError on network/OS-level failure.
/// Throws invalid_argument if the feed_type is invalid.
IRecvTransportPtr subscribeFeed( EFeedType feed_type,
                                 const char* iface = NULL,
                                 bool* sequenceChecking = NULL,
                                 EFeedOrig orig = ORIG_UNKNOWN );

/// Like subscribeFeed, but for MMap data sources
// If `feed_key` is not specified, subscribe to all feeds and their
// corresponding channels.
IRecvTransportVec subscribeFeedMMap( EFeedType feed_type,
                                     std::string feed_key = "");

// Try to subscribe to mmap feed first `subscribeFeedMMap`, fallback on regular
// a feed `subscribeFeed`, in case if mmap subscription fails.
IRecvTransportVec subscribeFeeds( EFeedType feed_type,
                               const char* iface = NULL,
                               bool* sequenceChecking = NULL,
                               EFeedOrig orig = ORIG_UNKNOWN );

/// Returns the Send transport for the specified hostname and port.
/// Throws runtime_error if unsuccessful.
ISendTransportPtr getSendTransportGeneral( const sockaddr_ipv4_t& sockaddr, const char* iface = NULL );

/// Returns the Send transport for the particular feed.
/// Throws runtime_error if unsuccessful.
ISendTransportPtr getSendTransportForFeedType(
    EFeedType feedtype,
    const char* iface = NULL,
    EFeedOrig orig = ORIG_UNKNOWN );

/// Returns the host and port for the given feed type.
const HostPortPair lookupFeedInfoHostPort( EFeedType feedtype, EFeedOrig orig = ORIG_UNKNOWN );
sockaddr_ipv4_t lookupFeedInfo( EFeedType feedtype, EFeedOrig orig = ORIG_UNKNOWN );
std::ostream& operator<<( std::ostream&, HostPortPair const& );

/// Shared memory location, channels to use, verbose
struct MMapFeedInfo_t {
    MMapFeedInfo_t()
        : m_shared_mem(), m_channels(), m_verbose() {}
    MMapFeedInfo_t(std::string const& shared_mem,
                   std::vector<int> const& channels,
                   bool verbose)
        : m_shared_mem(shared_mem), m_channels(channels), m_verbose(verbose) {}
    std::string m_shared_mem;
    std::vector<int> m_channels;
    bool m_verbose;
};

typedef std::vector< MMapFeedInfo_t > MMapFeedInfoVec;
MMapFeedInfoVec lookupFeedInfoMultiMMap( EFeedType feedtype, const std::string& feed_key = "" );
MMapFeedInfo_t lookupFeedInfoMMap( EFeedType feedtype );

/// Returns all the host-port pairs that can be used to subscribe to a given feed (on the current network).
HostPortPairList lookupSubscriptionInfo( EFeedType feedtype );

/// Returns all the host-port pairs for spin servers of
/// the given source and spin type (on the current
/// network). Typically, feedType is going to be one of SRC_CQS or
/// SRC_UQDF.
HostPortPairList lookupSpinServerInfo( EFeedType feedType, ESpinType spinType );

} // namespace bb

#endif // BB_IO_FEEDS_H

#ifndef BB_IO_NONBLOCKINGBYTESOURCE_H
#define BB_IO_NONBLOCKINGBYTESOURCE_H

/* Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved. */

#include <boost/cstdint.hpp>
#include <boost/thread.hpp>
#include <boost/thread/mutex.hpp>
#include <boost/thread/condition.hpp>
#include <tbb/concurrent_queue.h>
#include <bb/core/smart_ptr.h>
#include <bb/core/timeval.h>
#include <bb/core/Buffer.h>
#include <bb/io/ByteSource.h>
#include <bb/threading/ThreadPool.h>
#include <time.h>

#include <boost/enable_shared_from_this.hpp>

namespace bb {
class FDSet;

BB_FWD_DECLARE_SHARED_PTR( Buffer );
BB_FWD_DECLARE_SHARED_PTR( Timer );
BB_FWD_DECLARE_SHARED_PTR( NonBlockingByteSource );

/// A NonBlockingByteSource takes in an instance of a ByteSource and buffers
/// the data into memory.
///
/// If you pass in a ZByteSource, the decompression will be handled by another
/// thread. Calls to READ method will be blocked only when the buffer is empty
/// and the source doesn't reach the end.
///
/// Note that the methods of this class are NOT threadsafe.
class NonBlockingByteSource
    : public ByteSource
    , public threading::SerializedThreadedProducer
    , public boost::enable_shared_from_this<NonBlockingByteSource>
{
public:
    /// @param buffer_size   The size of a data buffer.
    /// @param max_buffers   The maximal number of buffers before a
    /// NonBlockingByteSource actually blocks.
    static NonBlockingByteSourcePtr create(
        ByteSourcePtr spByteSource,
        const size_t buffer_size = 1024 * 1024, const size_t max_buffers = 32 );

    static NonBlockingByteSourcePtr create(
        ByteSourcePtr spByteSource,
        threading::ThreadPoolPtr threadPool,
        const size_t buffer_size = 1024 * 1024, const size_t max_buffers = 32 );

public:
    virtual ~NonBlockingByteSource();

protected:
    /// Inherit from ByteSource
    virtual size_t ByteSource_read(void *buf, size_t size);

private:
    /// @param buffer_size   The size of a data buffer.
    /// @param max_buffers   The maximal number of buffers before a
    /// NonBlockingByteSource actually blocks.
    NonBlockingByteSource(
        ByteSourcePtr spByteSource,
        const size_t buffer_size, const size_t max_buffers );

    NonBlockingByteSource(
        ByteSourcePtr spByteSource,
        threading::ThreadPoolPtr threadPool,
        const size_t buffer_size, const size_t max_buffers );

    void initialize();

    // Enable buffering at the beginning.
    void fill_empty_queue();

    /// The method that actually populate the buffers.
    void populate_queue();

    typedef tbb::concurrent_bounded_queue<BufferPtr> BufferQueue;

    ByteSourcePtr                   m_spByteSource;

    const size_t                    m_buffer_size;
    const size_t                    m_max_buffers;

    /// The buffer that we are not finishing reading from.
    BufferPtr                       m_current_buffer;
    /// Empty buffer backets.
    BufferQueue                     m_empty_queue;
    /// Buffers that are fully populated.
    BufferQueue                     m_full_queue;

    threading::ThreadPoolPtr        m_threadpool;

    uint64_t                        m_block_count;
    uint64_t                        m_internal_read_count;
    uint64_t                        m_total_empty_count;
};


} // namespace bb

#endif // BB_IO_NONBLOCKINGBYTESOURCE_H

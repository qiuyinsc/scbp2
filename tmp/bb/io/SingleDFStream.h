#ifndef BB_IO_SINGLEDFSTREAM_H
#define BB_IO_SINGLEDFSTREAM_H

/* Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved. */

#include <string>

#include <bb/core/smart_ptr.h>
#include <bb/io/DFReader.h>

namespace bb {


BB_FWD_DECLARE_SHARED_PTR( ByteSource );

/** This class reads in a single datafile and exports it as an MStream.
 */
class SingleDFStream
    : public DFReader
{
public:
    /// Indicates a problem with the data file (usually missing data file or broken file format).
    struct Exception : virtual public HistMStream::Exception
    {
        Exception( std::string const& err, std::string const& filename );
        ~Exception() throw( )
        {
        }

        std::string m_err, m_filename;
    };

    /// File is missing
    struct MissingDataFile
        : virtual public SingleDFStream::Exception
        , virtual public HistMStream::MissingData
    {
        MissingDataFile( std::string const& err, std::string const& filename )
            : SingleDFStream::Exception( err, filename )
            , HistMStream::MissingData( err )
        {
        }
    };

    SingleDFStream( const bb::ByteSourceFactoryPtr byteSourceFactory = ByteSourceFactoryPtr() );
    SingleDFStream( std::string filename, const bb::ByteSourceFactoryPtr byteSourceFactory = ByteSourceFactoryPtr() ); // guesses isGz on the filename
    SingleDFStream( std::string filename, bool isGz, const bb::ByteSourceFactoryPtr byteSourceFactory = ByteSourceFactoryPtr() );

    virtual ~SingleDFStream(){}

    std::string getFilename() const
    {
        return m_filename;
    }

protected:
    static ByteSourcePtr openFile( std::string filename );
    static ByteSourcePtr openFile( std::string filename, bool isGz );

    std::string m_filename;
};
BB_DECLARE_SHARED_PTR( SingleDFStream );

// This class is meant for reading client logs ( files
// made up of bb msgs ) while they are still being written
// to.  Programs like 'attribution' use this.
//
// This class is NOT meant to be used with the Transmplexer
class IncompleteSingleDFStream : public SingleDFStream
{
    Buffer m_ringBuffer;
    size_t m_readsize; //bytes read so far
    MsgBuffer m_msgBuf;

public:
     IncompleteSingleDFStream( const std::string& filename );

    virtual ~IncompleteSingleDFStream()
    {
    }

    // next will attempt to read another message from the stream
    // if no full message can be read it will return null
    // when a full message is available, a pointer to it will be returned
    virtual const Msg* next();

 };

BB_DECLARE_SHARED_PTR( IncompleteSingleDFStream );


} // namespace bb

#endif // BB_IO_SINGLEDFSTREAM_H

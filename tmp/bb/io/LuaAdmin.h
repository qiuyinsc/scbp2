#ifndef BB_IO_LUAADMIN_H
#define BB_IO_LUAADMIN_H

/* Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved. */


#include <bb/core/LuaState.h>
#include <bb/io/TCPServer.h>

namespace bb
{

/// This is a stupidly simple RPC implementation. It works by having someone
/// remotely connect and send a sequence of commands as lua text.
///
/// The command protocol is:
/// The first line specifies a string which terminates
/// (the newline _is_ included in the terminator).
/// Then, the remainder of the stream will be broken into chunks each time
/// the terminator is seen. The last chunk must also have a terminator.
///
/// This encoding was chosen so you can just telnet to the server port and
/// type commands, like:
/// CMD
/// myCommand1
/// CMD
/// myCommand2
/// CMD
///
/// This class is abstract, you must subclass it and override sendCommandImpl
/// (and optionally executeCommandImpl)
class LuaCommandProtocol
{
public:
    LuaCommandProtocol();
    virtual ~LuaCommandProtocol() {}

    const LuaState &getConfig() const { return m_config; }
    LuaState &getConfig() { return m_config; }

    void handleData(const char *data, size_t length);

    std::ostream &outBuf() { return m_outStream; }
    void sendCommand();

protected:
    // Overrideables:
    /// execute a command that we received
    virtual void executeCommandImpl(const std::string &chunk);
    /// send a command to the other side
    virtual void sendCommandImpl(const std::string &chunk) = 0;

    // the terminator string we use for sending to the other side.
    static const std::string TERM;

private:
    LuaState m_config;

    std::ostringstream m_outStream;
    bool m_terminatorWasRead;
    std::string m_terminator;

    std::string m_buffer, m_matchingBuffer;
};
BB_DECLARE_SHARED_PTR(LuaCommandProtocol);


/// This integrates the LuaCommandProtocol class into TCPServer.
/// You will need to create your own TCPServer subclass which instantiates
/// objects of these types from within your TCPServer::createConnection
/// override.
class LuaConnectionThread
    : public LuaCommandProtocol
    , public TCPServer::ConnectionThread
{
public:
    LuaConnectionThread(const ThreadPipeWriter &toMainPipe, const ThreadPipeReader &toSelf, TCPSocket *socket, const sockaddr_ipv4_t &peerAddr);

    virtual void sendCommandImpl(const std::string &chunk);

protected:
    void onTCPData();
    Subscription m_socketSub;
};




/// LuaAdminInterface/LuaAdminServer is yet another abstraction layer atop the
/// TCPServer / LuaConnectionThread interface. This is used in settings where you
/// have one central thread (the main thread), and then you want to accept TCP
/// connections for a lua-based admin protocol.  These threads will parse the lua
/// commands and turn it into a Request_t object, which will get passed on to the
/// main thread. The main thread will handle them, and optionally pass back a
/// Response_t to the thread that originated the request.
///
/// These classes abstract out the locking/interthread communication aspect of this.
///
/// This isn't appropriate for all settings, but if you've got a strict master-slave
/// relationship between your client threads and 1 main thread, this should do the trick.
template<typename Request_t, typename Response_t> class LuaAdminInterface;
template<typename Request_t, typename Response_t> class LuaAdminServer;


/// A thread for each client.
/// You will need to override onMainThreadResponse.
template<typename Request_t, typename Response_t>
class LuaAdminInterface
    : public LuaConnectionThread
{
public:
    static const uint8_t WAKEUP_CMD = 254;

    typedef typename boost::shared_ptr<Request_t> RequestPtr;
    typedef typename boost::shared_ptr<Response_t> ResponsePtr;
    typedef LuaAdminServer<Request_t, Response_t> Server_t;

    LuaAdminInterface(LuaAdminServer<Request_t, Response_t> *server,
            const ThreadPipeWriter &toMainPipe, const ThreadPipeReader &toSelf, TCPSocket *socket, const sockaddr_ipv4_t &peerAddr);

    /// sends a request to the main thread.
    void queueRequest(RequestPtr &request); // will null out request before exiting

    /// Override: handle a response from the main thread.
    virtual void onMainThreadResponse(ResponsePtr response) = 0;

protected:
    virtual void handleWakeup(uint8_t cmd);
    Server_t *m_server;

    friend class LuaAdminServer<Request_t, Response_t>;

    // everything below here is shared by the master thread and this thread.
    boost::mutex m_responseListMutex;
    typedef typename std::list<ResponsePtr> ResponseList;
    ResponseList m_responses;
};


/// The object in the main thread which is responsible for handling requests and generating responses
/// for the client threads.
/// You will need to override onClientThreadRequest and createConnection.
template<typename Request_t, typename Response_t>
class LuaAdminServer : public TCPServer
{
public:
    static const uint8_t WAKEUP_CMD = LuaAdminInterface<Request_t, Response_t>::WAKEUP_CMD;

    typedef typename boost::shared_ptr<Request_t> RequestPtr;
    typedef typename boost::shared_ptr<Response_t> ResponsePtr;
    typedef LuaAdminInterface<Request_t, Response_t> ClientThread_t;

    LuaAdminServer(FDSet &fdSet, int port) : TCPServer(fdSet, port) {}
    LuaAdminServer(FDSet &fdSet, TCPSocketPtr listenSocket) : TCPServer(fdSet, listenSocket) {}

    /// Override: handle a request from a client thread. Optionally returns a
    /// response to send to the requesting thread (can be null).
    virtual ResponsePtr onClientThreadRequest(RequestPtr request) = 0;

protected:
    virtual void onThreadExited(const ThreadMetadata &entry);
    virtual void handleWakeup(uint8_t cmd);

    friend class LuaAdminInterface<Request_t, Response_t>;

    // everything below here is shared by all threads
    boost::mutex m_requestListMutex;
    typedef std::list<std::pair<ClientThread_t*, RequestPtr> > RequestList;
    RequestList m_requests;
};

}



template<typename Request_t, typename Response_t>
bb::LuaAdminInterface<Request_t, Response_t>::LuaAdminInterface(
    LuaAdminServer<Request_t, Response_t> *server,
    const ThreadPipeWriter &toMainPipe, const ThreadPipeReader &toSelf, TCPSocket *socket, const sockaddr_ipv4_t &peerAddr)
    : LuaConnectionThread(toMainPipe, toSelf, socket, peerAddr)
    , m_server(server)
{
}

template<typename Request_t, typename Response_t>
void bb::LuaAdminInterface<Request_t, Response_t>::queueRequest(RequestPtr &request)
{
    boost::mutex::scoped_lock l(m_server->m_requestListMutex);

    {
        // thread safety paranoia - create the local shared ptr so all the decrefs
        // happen while the mutex is held -- this guarantees that the IAdminRequest
        // destructor is always called from the main thread.
        RequestPtr sharedPtr(request);
        request.reset();
        m_server->m_requests.push_back(std::make_pair(this, sharedPtr));
        m_toMainPipe.sendWakeup(WAKEUP_CMD);
    }
}

template<typename Request_t, typename Response_t>
void bb::LuaAdminInterface<Request_t, Response_t>::handleWakeup(uint8_t cmd)
{
    if(cmd == WAKEUP_CMD)
    {
        // handle the responses that the main thread has sent
        ResponseList responses;
        {
            boost::mutex::scoped_lock l(m_responseListMutex);
            responses.swap(m_responses);
        }

        for(typename ResponseList::iterator i = responses.begin(); i != responses.end(); ++i)
            onMainThreadResponse(*i);
    }
    else
        LuaConnectionThread::handleWakeup(cmd);
}



template<typename Request_t, typename Response_t>
void bb::LuaAdminServer<Request_t, Response_t>::onThreadExited(const ThreadMetadata &entry)
{
    boost::mutex::scoped_lock l(m_requestListMutex);

    // forget about any queued requests that this thread has made
    for(typename RequestList::iterator i = m_requests.begin(); i != m_requests.end();)
    {
        if(i->first == entry.m_connObj)
        {
            i = m_requests.erase(i);
        }
        else
            ++i;
    }
}

template<typename Request_t, typename Response_t>
void bb::LuaAdminServer<Request_t, Response_t>::handleWakeup(uint8_t cmd)
{
    if(cmd == WAKEUP_CMD)
    {
        RequestList requests;

        {
            boost::mutex::scoped_lock l(m_requestListMutex);
            requests.swap(m_requests);
        }

        for(typename RequestList::iterator i = requests.begin(); i != requests.end(); ++i)
        {
            ResponsePtr response = onClientThreadRequest(i->second);
            i->second.reset();

            if(response)
            {
                // take the thread's response list mutex.
                boost::mutex::scoped_lock l(i->first->m_responseListMutex);

                TCPServer::ThreadMetadata &threadInfo = lookupThread(i->first);
                i->first->m_responses.push_back(response);
                response.reset();

                threadInfo.m_toThreadPipe.sendWakeup(WAKEUP_CMD);
            }
        }
    }
}

#endif // BB_IO_LUAADMIN_H

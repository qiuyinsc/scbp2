#ifndef BB_IO_MQSENDTRANSPORT_H
#define BB_IO_MQSENDTRANSPORT_H

/* Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved. */

#include <sstream>

#include <bb/core/Log.h>
#include <bb/core/Msg.h>

#include <bb/io/AsyncByteSink.h>
#include <bb/io/ISendTransport.h>
#include <bb/io/SendTransport.h>

namespace bb {
class Msg;

BB_FWD_DECLARE_SHARED_PTR( MQByteSink );

/// Sends dgram-format messages over an MQ.
class MQSendTransport : public ISendTransport {
public:
    MQSendTransport(MQByteSinkPtr s);

    /// Sends the message over the Socket.
    /// Throws an exception (usually CError) if an error occurred.
    virtual void send(const Msg *);

protected:
    MQByteSinkPtr m_sink;
};
BB_DECLARE_SHARED_PTR( MQSendTransport );


/// Sends messages in datagram format over an MQByteSink using an AsyncByteSink.
class NonBlockingMQSendTransport : public MQSendTransport
{
public:
    typedef AsyncByteSink::ErrorCB ErrorCB;
    NonBlockingMQSendTransport(MQByteSinkPtr sink, FDSet& fdset, const ErrorCB &errCB = ErrorCB());

    /// Throws an exception if an error occurred, typically propagating ByteSink failures.
    virtual void send(const Msg *m);

protected:
    AsyncByteSink m_async;
};
BB_DECLARE_SHARED_PTR(NonBlockingMQSendTransport);


#ifndef SWIG
struct ZmqMsgSerializer
{
    struct Settings
    {
        Settings( const std::string& _pub_id = bb::getLogger()->getname() );

        std::string pub_id;
    };

    ZmqMsgSerializer( const Msg&, const ZmqMsgSerializer::Settings& );

    friend std::ostream &operator<<(std::ostream &out, ZmqMsgSerializer const& serializer);

    const Msg& msg;
    ZmqMsgSerializer::Settings settings;
};

std::ostream &operator<<(std::ostream &out, ZmqMsgSerializer const& serializer);

typedef SerializeSendTransport<ZmqMsgSerializer,ZmqMsgSerializer::Settings> ZmqSendTransport;
BB_DECLARE_SHARED_PTR(ZmqSendTransport);
#endif // SWIG

} // namespace bb

#endif // BB_IO_MQSENDTRANSPORT_H

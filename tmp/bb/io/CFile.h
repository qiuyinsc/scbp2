#ifndef BB_IO_CFILE_H
#define BB_IO_CFILE_H

/* Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved. */


#include <boost/scoped_array.hpp>

#include <bb/io/File.h>


namespace bb {


/// Tried to write with NULL pointer error.
class FlockFailError : public Error {
public:
    FlockFailError()
        : Error( "CFile::flock failed" ) { }
};



/** A wrapper class around the C FILE operations.  There is one
    tangible reason to use this over the standard C++ classes,
    which is that libstdc++ (as of this writing) has bugs that
    prevent you from using Linux LFS (Large File Support).
    However, code has been written for glibc to use LFS, and so
    the standard C file stream operations work.  Given that some
    files that we read and write are over the old 2GB limit, we
    need classes implemented using the standard C stream
    functions. */
class CFile : public File
{
public:
    /// Returns true if the given file exists.
    static bool exists( const char *filename );

    enum Flags { NONE = 0, OPEN = 1 };

    // Note on buffering: CFile uses C stdio (FILE*). FILE* objects
    // have user space buffering by default. You should not need to
    // pass any argument for 'bufferSize' unless you wish to change
    // this default. You may explicitly select the default, if that
    // seems useful, by passing a negative size for the buffer. To
    // disable buffering, you may pass a size of zero, but if you want
    // to use an unbuffered file, see UnbufferedCFile below: it avoids
    // the overhead of fread/fwrite in favor of using read/write with
    // an unbuffered file. Finally, if you wish to explicitly specify
    // a buffer size, pass a positive integer value for
    // bufferSize. This buffer size is used for the total buffer size
    // (including read and write buffers).

    /// Represents this file with mode attributes that match fopen.
    CFile( const char *filename, const char *mode,
           Flags flags = NONE,
           ssize_t bufferSize = -1 );

    /// Constructs a CFile that attach the specified file descriptor.
    /// The filename is user-defined and not used by CFile.
    /// The mode attributes match fdopen and must be compatible with the file descriptor.
    /// The CFile begins at the position of the fd when CFile::open is called.
    CFile( int fd, const char *filename, const char *mode,
           Flags flags = NONE,
           ssize_t bufferSize = -1 );

    /// Constructs a CFile that attach the specified FILE.
    /// The filename is user-defined and not used by CFile.
    CFile( FILE *file, const char *filename,
           Flags flags = NONE,
           ssize_t bufferSize = -1 );

    /// Destructor.
    virtual ~CFile();

    /// Returns true if the file exists.  Doesn't matter if the file is open or not.
    virtual bool exists() const;
    /// Opens the file.  Throws errors if file already opened, or
    /// if there was a problem opening.
    virtual void open();
    /// Returns true if the file is opened.
    virtual bool opened() const;
    /// Flushes and closes file.
    virtual void close();
    /// Returns true if EOF.
    virtual bool eof() const;
    /// Flushes file.
    virtual void flush();

    virtual void seek( size_t pos );
    virtual size_t pos() const;
    virtual size_t size() const;

    const std::string& name() const { return m_filename; }

    // Low level FILE* and fd access methods. You are strongly
    // encouraged to ignore these. Use them at your peril.
    inline FILE* file() const
    {
        return m_file;
    }

    inline int fd() const
    {
        return m_fd;
    }

    const std::string& mode() { return m_mode;}

    bool flock();
    bool fUnlock();


protected:
    virtual size_t ByteSource_read( void *buf, size_t len );
    virtual size_t ByteSink_write( const void *buf, size_t len );

private:
    std::string m_filename, m_mode;
    int m_fd;
    FILE *m_file;
    ssize_t m_bufferSize;
    boost::scoped_array<char> m_buffer;
};
BB_DECLARE_SHARED_PTR(CFile);


// A CFile which does no buffering. Writes write, reads read, flush is
// a no-op. This is good for things like writing logfiles, where you
// don't want to buffer and then immediately call flush (which is
// wasteful), nor do you want the log to be arbitrarily out of date
// until the next time you overflow whatever buffering the OS is
// using. Note that this does not imply that the writes are
// synchronous to disk - this just gets them into the kernels buffer
// cache without userspace buffering. If you need O_SYNC behavior,
// specify that when you open the file and use the constructor here
// that takes an fd.

class UnbufferedCFile : public CFile
{
public:
    UnbufferedCFile( const char *filename, const char *mode,
                     Flags flags = NONE );

    UnbufferedCFile( int fd, const char *filename, const char *mode,
                    Flags flags = NONE );

    UnbufferedCFile( FILE *file, const char *filename,
                    Flags flags = NONE );

    virtual void flush();

protected:
    virtual size_t ByteSink_read( void *buf, size_t len );
    virtual size_t ByteSink_write( const void *buf, size_t len );
};
BB_DECLARE_SHARED_PTR(UnbufferedCFile);


class MMappedCFile: public CFile
{
public:
    MMappedCFile( const char *filename, const char *mode,
                  size_t size, size_t increment, bool truncate, Flags flags = NONE );

    MMappedCFile( int fd, const char *filename, const char *mode,
                  size_t size, size_t increment, bool truncate, Flags flags = NONE );

    MMappedCFile( FILE *file, const char *filename,
                  size_t size, size_t increment, bool truncate, Flags flags = NONE );

    virtual ~MMappedCFile();

    /// Opens the file.  Throws errors if file already opened, or
    /// if there was a problem opening.
    virtual void open();

    /// Flushes and closes file.
    virtual void close();

    /// Returns true if EOF.
    virtual bool eof() const;

    /// Flushes file.
    virtual void flush();

    virtual void seek( size_t pos );
    virtual size_t pos() const;
    virtual size_t size() const;

protected:
    virtual size_t ByteSink_read( void *buf, size_t len );
    virtual size_t ByteSink_write( const void *buf, size_t len );
private:
    void   init( CFile::Flags flags);
    void   resize( size_t newSize);

    unsigned char    * m_baseAddr;

    // the current offset into the mmapeed region into which we will write next
    size_t             m_curOffset;

    // The length of the currently allocated region
    size_t             m_length;

    // The increment to use when resizing the file.
    size_t             m_increment;
    bool               m_truncateOnClose;
};
BB_DECLARE_SHARED_PTR(MMappedCFile);

} // namespace bb

#endif // BB_IO_CFILE_H

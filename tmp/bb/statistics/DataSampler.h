#ifndef BB_STATISTICS_DATASAMPLER_H
#define BB_STATISTICS_DATASAMPLER_H

/* Contents Copyright 2016 Athena Capital Research LLC. All Rights Reserved. */


/**
   @file DataSampler.h

   IDataSampler interface and a base implementation.

**/

#include <ostream>
#include <string>

#include <boost/optional.hpp>

#include <bb/core/EventPublisher.h>
#include <bb/core/Priority.h>
#include <bb/core/Scripting.h>
#include <bb/core/ptime.h>
#include <bb/core/smart_ptr.h>

namespace bb {

class IDataRequirements;
struct LuaPrintSettings;

namespace statistics {

const bb::Priority PRIORITY_DATASAMPLER_DELAY_SAMPLING         (0x69000000); // it is just lower than signals.

BB_FWD_DECLARE_SHARED_PTR( IStatisticBuilder );
BB_FWD_DECLARE_SHARED_PTR( IDataSamplerSpec );

class IDataSamplerListener;

// An interface for arbitrary sampling.
class IDataSampler
{
public:
    virtual ~IDataSampler() {}

    virtual void startSampling() = 0;
    virtual void stopSampling() = 0;

    // DataSamplers may not actually need a state, but just in case
    virtual double getValue( bool* pSuccess = NULL ) const = 0;
    virtual timeval_t getLastSampleTime() const = 0;
    virtual const std::string& getDescription() const = 0;
    // It may be useful to know what the priority, if any, of the sampling is
    virtual boost::optional<bb::Priority> getPriority() const = 0;

    virtual bool isOK() const = 0;

    virtual void addDataSamplerListener( IDataSamplerListener* listener ) const = 0;
    virtual void removeDataSamplerListener( IDataSamplerListener* listener ) const = 0;
};
BB_DECLARE_SHARED_PTR( IDataSampler );

class IDataSamplerListener : public bb::IEventSubscriber
{
public:
    virtual ~IDataSamplerListener() {}
    virtual void onDataSample( const IDataSampler* dataSampler ) {}
};
BB_DECLARE_SHARED_PTR( IDataSamplerListener );

class IDataSamplerSpec
{
public:
    BB_DECLARE_SCRIPTING();

    virtual ~IDataSamplerSpec() {}

    virtual const std::string& getDescription() const = 0;

   /// Instantiates a concrete IDataSampler according to the Spec
    virtual IDataSamplerPtr build( IStatisticBuilder* builder ) const = 0;

    /// deep copy this IDataSamplerSpec
    virtual IDataSamplerSpec* clone() const = 0;
    inline static IDataSamplerSpec* clone( const IDataSamplerSpec* e ) { return e ? e->clone() : NULL; }
    inline static IDataSamplerSpec* clone( IDataSamplerSpecCPtr e ) { return clone( e.get() ); }

    /// Combines this IDataSamplerSpec into the given hash value
    virtual void hashCombine( size_t& result ) const = 0;

    /// Compares two data sampler specs.
    virtual bool compare( const IDataSamplerSpec* other ) const = 0;

    /// Prints the lua representation of this DataSamplerSpec.
    virtual void print( std::ostream& o, const LuaPrintSettings& ps ) const = 0;

    /// ensures sure that all fields have been set correctly, or throws runtime_error
    virtual void checkValid() const {}

    /// Propagates all the required data (instr/src pairs) to the IDataRequirements.
    virtual void getDataRequirements( IDataRequirements* rqs ) const = 0;
};
BB_DECLARE_SHARED_PTR( IDataSamplerSpec );

/// Implements the common addListener support.
class DataSamplerImpl
    : public IDataSampler
{
public:
    virtual void startSampling() {}
    virtual void stopSampling() {}

    virtual double getValue( bool* pSuccess = NULL ) const { return 0; }
    virtual boost::optional<bb::Priority> getPriority() const { return boost::none; }
    virtual const std::string& getDescription() const { return m_description; }

    virtual bool isOK() const { return true; }

    virtual void addDataSamplerListener( IDataSamplerListener* listener ) const;
    virtual void removeDataSamplerListener( IDataSamplerListener* listener ) const;

protected:
    DataSamplerImpl( const std::string& description )
        : m_description( description )
    {}

    virtual void notifyDataSample() const;

    typedef bb::EventPublisher<IDataSamplerListener> IDataSamplerPub;

    struct notifyDataSampleImpl : public IDataSamplerPub::INotifier
    {
        notifyDataSampleImpl( IDataSamplerListener* callback ) : IDataSamplerPub::INotifier( callback ) {}
        bool notify( const IDataSampler* dataSampler )
        {
            m_callback->onDataSample( dataSampler );
            return true;
        }
    };

    mutable IDataSamplerPub m_listeners;

    const std::string m_description;
};

class DataSamplerSpec
    : public IDataSamplerSpec
{
public:
    BB_DECLARE_SCRIPTING();

    DataSamplerSpec() {}

    DataSamplerSpec( const DataSamplerSpec& e )
        : m_description( e.m_description )
    {}

    virtual void checkValid() const;

    virtual const std::string& getDescription() const { return m_description; }

    virtual void hashCombine( size_t& result ) const;
    virtual bool compare( const IDataSamplerSpec* other ) const;
    virtual void getDataRequirements( IDataRequirements* rqs ) const {}

    std::string m_description;
};

std::size_t hash_value( const IDataSamplerSpec& a );

// SWIG 1.3.29 doesn't like these in-namespace operators
#ifndef SWIG

bool operator==( const IDataSamplerSpec& a, const IDataSamplerSpec& b );
inline bool operator!=( const IDataSamplerSpec& a, const IDataSamplerSpec& b ) { return !( a == b ); }

std::ostream& operator<<( std::ostream& out, const IDataSamplerSpec& spec );
void luaprint( std::ostream& out, IDataSamplerSpec const& ds, LuaPrintSettings const& ps );

#endif // !SWIG

// helpers for hash_map
struct DataSamplerSpecHasher : public std::unary_function<size_t, IDataSamplerSpecCPtr>
{
    size_t operator()( const IDataSamplerSpecCPtr& a ) const { return hash_value( *a ); }
};

struct DataSamplerSpecComparator : public std::binary_function<bool, IDataSamplerSpecCPtr, IDataSamplerSpecCPtr>
{
    bool operator()( const IDataSamplerSpecCPtr& a, const IDataSamplerSpecCPtr& b ) const { return *a == *b; }
};

} // namespace statistics
} // namespace bb

#endif // BB_STATISTICS_DATASAMPLER_H

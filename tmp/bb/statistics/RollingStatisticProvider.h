#ifndef BB_STATISTICS_ROLLINGSTATISTICPROVIDER_H
#define BB_STATISTICS_ROLLINGSTATISTICPROVIDER_H

/* Contents Copyright 2016 Athena Capital Research LLC. All Rights Reserved. */

#include <ostream>

#include <boost/optional.hpp>

#include <bb/core/ptime.h>

#include <bb/clientcore/PriceProvider.h>
#include <bb/clientcore/PriceSizeProviderSpec.h>

#include <bb/statistics/RollingStatistic.h>

namespace bb {

class PriceSize;
class instrument_t;
class timeval_t;

namespace statistics {

BB_FWD_DECLARE_SHARED_PTR( RollingStatisticProvider );

class RollingStatisticProvider
    : public PriceSizeProviderImpl
    , public IRollingStatisticListener
{
public:
    RollingStatisticProvider( const IRollingStatisticPtr& rollingStatistic, const bb::instrument_t& instrument );

    virtual ~RollingStatisticProvider();

    virtual PriceSize getRefPriceSize( bool* pSuccess = NULL ) const;
    virtual bool isPriceSizeOK() const { return m_isOK; }
    virtual bb::instrument_t getInstrument() const { return m_instrument; }
    virtual bb::timeval_t getLastChangeTime() const;

    virtual void onRollingStatisticChanged( const IRollingStatistic* rollingStatistic );

protected:
    IRollingStatisticPtr m_rollingStatistic;
    const bb::instrument_t m_instrument;
    bb::PriceSize m_priceSize;
    bool m_isOK;
};

BB_DECLARE_SHARED_PTR( RollingStatisticProvider );

/// PxSzPSpec corresponding to a RollingStatisticProvider
class RollingStatisticPxSzPSpec : public bb::IPxSzProviderSpec
{
public:
    BB_DECLARE_SCRIPTING();

    RollingStatisticPxSzPSpec() {}
    RollingStatisticPxSzPSpec( const RollingStatisticPxSzPSpec& a );

    virtual bb::instrument_t getInstrument() const { return m_instrument; }
    virtual bb::IPriceSizeProviderPtr build( bb::PriceProviderBuilder* builder ) const;
    virtual RollingStatisticPxSzPSpec* clone() const;
    virtual void hashCombine( size_t& result ) const;
    virtual bool compare( const bb::IPxSzProviderSpec* other ) const;
    virtual void print( std::ostream& o, const bb::LuaPrintSettings& pset ) const;
    virtual void checkValid() const;
    virtual void getDataRequirements( bb::IDataRequirements* rqs ) const {}

    IRollingStatisticSpecPtr m_rollingStatistic;
    bb::instrument_t m_instrument;
};
BB_DECLARE_SHARED_PTR( RollingStatisticPxSzPSpec );

} // namespace statistics
} // namespace bb

#endif // BB_STATISTICS_ROLLINGSTATISTICPROVIDER_H

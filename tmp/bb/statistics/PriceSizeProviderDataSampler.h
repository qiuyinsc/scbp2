#ifndef BB_STATISTICS_PRICESIZEPROVIDERDATASAMPLER_H
#define BB_STATISTICS_PRICESIZEPROVIDERDATASAMPLER_H

/* Contents Copyright 2016 Athena Capital Research LLC. All Rights Reserved. */

#include <bb/clientcore/PriceProvider.h>

#include <bb/statistics/DataSampler.h>

namespace bb {

BB_FWD_DECLARE_SHARED_PTR( IPxSzProviderSpec );

namespace statistics {

class PriceSizeProviderDataSampler
    : public DataSamplerImpl
{
public:
    PriceSizeProviderDataSampler( const std::string& desc, const IPriceSizeProviderPtr& priceSizeProvider );

    virtual bool isOK() const { return m_priceSizeProvider->isPriceSizeOK(); }
    virtual timeval_t getLastSampleTime() const { return m_priceSizeProvider->getLastChangeTime(); }

protected:
    void onPriceSizeChanged( const IPriceSizeProvider& psp );

    IPriceSizeProviderPtr m_priceSizeProvider;
    Subscription m_sub;
};
BB_DECLARE_SHARED_PTR( PriceSizeProviderDataSampler );

class PriceSizeProviderDataSamplerSpec
    : public DataSamplerSpec
{
public:
    BB_DECLARE_SCRIPTING();

    PriceSizeProviderDataSamplerSpec();
    PriceSizeProviderDataSamplerSpec( const PriceSizeProviderDataSamplerSpec &e );

    virtual IDataSamplerPtr build( IStatisticBuilder* builder ) const;

    virtual PriceSizeProviderDataSamplerSpec* clone() const;
    virtual void checkValid() const;
    virtual void print( std::ostream& o, const LuaPrintSettings& ps ) const;
    virtual void hashCombine( size_t& result ) const;
    virtual bool compare( const IDataSamplerSpec* other ) const;

    IPxSzProviderSpecPtr m_pxszp;
};
BB_DECLARE_SHARED_PTR( PriceSizeProviderDataSamplerSpec );

} // namespace statistics
} // namespace bb

#endif // BB_STATISTICS_PRICESIZEPROVIDERDATASAMPLER_H

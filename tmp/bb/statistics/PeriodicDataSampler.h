#ifndef BB_STATISTICS_PERIODICDATASAMPLER_H
#define BB_STATISTICS_PERIODICDATASAMPLER_H

/* Contents Copyright 2016 Athena Capital Research LLC. All Rights Reserved. */

#include <bb/clientcore/PeriodicWakeup.h>

#include <bb/statistics/DataSampler.h>

namespace bb {

class ClockMonitor;

namespace statistics {

class PeriodicDataSampler
    : public DataSamplerImpl
    , protected PeriodicWakeup
{
public:
    PeriodicDataSampler( const std::string& desc, ClockMonitor* clockMonitor, const ptime_duration_t& wakeupInterval
                         , const ptime_duration_t& wakeupOffset, uint32_t priority );

    virtual void startSampling();
    virtual void stopSampling();
    virtual timeval_t getLastSampleTime() const { return m_tv; }
    virtual boost::optional<bb::Priority> getPriority() const { return m_priority; };
    virtual void onPeriodicWakeup( const timeval_t& ctv, const timeval_t& swtv );

protected:
    timeval_t m_tv;
    const boost::optional<bb::Priority> m_priority;
};
BB_DECLARE_SHARED_PTR( PeriodicDataSampler );

class PeriodicDataSamplerSpec
    : public DataSamplerSpec
{
public:
    BB_DECLARE_SCRIPTING();

    PeriodicDataSamplerSpec();
    PeriodicDataSamplerSpec( const PeriodicDataSamplerSpec &e );

    virtual IDataSamplerPtr build( IStatisticBuilder* builder ) const;

    virtual PeriodicDataSamplerSpec* clone() const;
    virtual void checkValid() const;
    virtual void print( std::ostream& o, const LuaPrintSettings& ps ) const;
    virtual void hashCombine( size_t& result ) const;
    virtual bool compare( const IDataSamplerSpec* other ) const;

    ptime_duration_t m_wakeupInterval, m_wakeupOffset;
    uint32_t m_wakeupPriority;
};
BB_DECLARE_SHARED_PTR( PeriodicDataSamplerSpec );

} // namespace statistics
} // namespace bb

#endif // BB_STATISTICS_PERIODICDATASAMPLER_H

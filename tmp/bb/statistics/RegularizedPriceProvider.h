#ifndef BB_STATISTICS_REGULARIZEDPRICEPROVIDER_H
#define BB_STATISTICS_REGULARIZEDPRICEPROVIDER_H

/* Contents Copyright 2016 Athena Capital Research LLC. All Rights Reserved. */

#include <deque>

#include <bb/clientcore/IClockListener.h>
#include <bb/clientcore/PeriodicWakeup.h>
#include <bb/clientcore/PriceProvider.h>
#include <bb/clientcore/PriceProviderSpec.h>
#include <bb/clientcore/PriceSizeProviderSpec.h>

namespace bb {

class Priority;

BB_FWD_DECLARE_SHARED_PTR( ClockMonitor );

namespace statistics {

class RegularizedPriceProvider
    : public PriceProviderImpl
    , protected PeriodicWakeup
    , protected IClockListener
{
public:
    RegularizedPriceProvider( IPriceProviderCPtr underlying, const ClockMonitorPtr& clockMonitor, const bb::ptime_duration_t& interval, const bb::ptime_duration_t& offset );

    virtual ~RegularizedPriceProvider() {}

    virtual double getRefPrice( bool* pSuccess = NULL ) const { if ( pSuccess ) *pSuccess = isPriceOK(); return m_px; }
    virtual bool isPriceOK() const { return m_ok; }
    virtual instrument_t getInstrument() const { return m_underlying->getInstrument(); }
    virtual timeval_t getLastChangeTime() const { return m_tv; }

protected:
    virtual void onPeriodicWakeup( const timeval_t& ctv, const timeval_t& swtv );
    void onWakeupCall( const timeval_t& ctv, const timeval_t& swtv, int reason, void* pData );
    virtual void onPriceChanged( const IPriceProvider& sender );

    const IPriceProviderCPtr m_underlying;
    const ClockMonitorPtr m_clockMonitor;
    const bb::ptime_duration_t m_interval, m_offset;
    const Priority m_clockNoticePriority;
    Subscription m_sub;
    bool m_ok;
    double m_px;
    timeval_t m_tv;
};
BB_DECLARE_SHARED_PTR( RegularizedPriceProvider );

/// PxPSpec for building a RegularizedPxP
class RegularizedPxPSpec : public IPxProviderSpec
{
public:
    BB_DECLARE_SCRIPTING();

    RegularizedPxPSpec() {}
    RegularizedPxPSpec( const RegularizedPxPSpec& a, const boost::optional<InstrSubst>& instrSubst );

    virtual instrument_t getInstrument() const { return m_underlying->getInstrument(); }
    virtual IPriceProviderPtr build( PriceProviderBuilder* builder ) const;
    virtual RegularizedPxPSpec* clone( const boost::optional<InstrSubst>& instrSubst = boost::none ) const;
    virtual void hashCombine( size_t& result ) const;
    virtual bool compare( const IPxProviderSpec* other ) const;
    virtual void print( std::ostream& o, const LuaPrintSettings& pset ) const;
    virtual void checkValid() const;
    virtual void getDataRequirements( IDataRequirements* rqs ) const;

    IPxProviderSpecPtr m_underlying;
    bb::ptime_duration_t m_interval, m_offset;
};
BB_DECLARE_SHARED_PTR( RegularizedPxPSpec );

class RegularizedPriceSizeProvider
    : public PriceSizeProviderImpl
    , protected PeriodicWakeup
    , protected IClockListener
{
public:
    RegularizedPriceSizeProvider( IPriceSizeProviderCPtr underlying, const ClockMonitorPtr& clockMonitor, const bb::ptime_duration_t& interval, const bb::ptime_duration_t& offset );

    virtual ~RegularizedPriceSizeProvider() {}

    virtual PriceSize getRefPriceSize( bool* pSuccess = NULL ) const { if ( pSuccess ) *pSuccess = isPriceSizeOK(); return m_pxsz; }
    virtual bool isPriceSizeOK() const { return m_ok; }
    virtual instrument_t getInstrument() const { return m_underlying->getInstrument(); }
    virtual timeval_t getLastChangeTime() const { return m_tv; }

protected:
    virtual void onPeriodicWakeup( const timeval_t& ctv, const timeval_t& swtv );
    void onWakeupCall( const timeval_t& ctv, const timeval_t& swtv, int reason, void* pData );
    virtual void onPriceSizeChanged( const IPriceSizeProvider& sender );

    const IPriceSizeProviderCPtr m_underlying;
    const ClockMonitorPtr m_clockMonitor;
    const bb::ptime_duration_t m_interval, m_offset;
    const Priority m_clockNoticePriority;
    Subscription m_sub;
    bool m_ok;
    PriceSize m_pxsz;
    timeval_t m_tv;
};
BB_DECLARE_SHARED_PTR( RegularizedPriceSizeProvider );

/// PxSzPSpec for building a RegularizedPxSzP
class RegularizedPxSzPSpec : public IPxSzProviderSpec
{
public:
    BB_DECLARE_SCRIPTING();

    RegularizedPxSzPSpec() {}

    virtual instrument_t getInstrument() const { return m_underlying->getInstrument(); }
    virtual IPriceSizeProviderPtr build( PriceProviderBuilder* builder ) const;
    virtual RegularizedPxSzPSpec* clone() const;
    virtual void hashCombine( size_t& result ) const;
    virtual bool compare( const IPxSzProviderSpec* other ) const;
    virtual void print( std::ostream& o, const LuaPrintSettings& pset ) const;
    virtual void checkValid() const;
    virtual void getDataRequirements( IDataRequirements* rqs ) const;

    IPxSzProviderSpecPtr m_underlying;
    bb::ptime_duration_t m_interval, m_offset;
};
BB_DECLARE_SHARED_PTR( RegularizedPxSzPSpec );

} // namespace statistics
} // namespace bb

#endif // BB_STATISTICS_REGULARIZEDPRICEPROVIDER_H

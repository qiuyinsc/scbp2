#ifndef BB_STATISTICS_STATISTICS_SCRIPTING_H
#define BB_STATISTICS_STATISTICS_SCRIPTING_H

/* Contents Copyright 2016 Athena Capital Research LLC. All Rights Reserved. */

namespace bb {

namespace statistics {
/// Registers the clientcore library with ScriptManager.
/// The library can be loaded from C++ with ScriptManager::loadLibrary( "clientcore" )
/// or from Lua with loadLibrary( "clientcore" ).
bool registerScripting();
} // namespace statistics

} // namespace bb

#endif // BB_STATISTICS_STATISTICS_SCRIPTING_H

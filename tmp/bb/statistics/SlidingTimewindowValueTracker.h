#ifndef BB_STATISTICS_SLIDINGTIMEWINDOWVALUETRACKER_H
#define BB_STATISTICS_SLIDINGTIMEWINDOWVALUETRACKER_H

/* Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved. */

#include <deque>
#include <stdint.h>
#include <bb/core/compat.h>
#include <boost/type_traits/conditional.hpp>
#include <boost/type_traits/is_fundamental.hpp>

/* This class is used to keep track of the max or min value in a fix sized sliding time window
 * SlidingTimewindowValueTracker should not used directly, please the subclass instead.
 * The reason using int32 instead of bb::timeval_t to keep the time is, we don't need that accuracy in most cases,
 * and timeval_t version is bit slower
 */
template<typename T, typename CMP>
class SlidingTimewindowValueTracker {
public:

    SlidingTimewindowValueTracker(const int32_t windowSizeInSec, const CMP& cmp = CMP()) :
            m_compare(cmp), m_winSizeInSec(windowSizeInSec) {
    }

    virtual ~SlidingTimewindowValueTracker() {
    }

    typedef typename boost::conditional<boost::is_fundamental<T>::value, const T, const T&>::type T_PARAM_TYPE;

    void push(const int32_t timeInSec, T_PARAM_TYPE value) {
        const int32_t timeWindowBegin = timeInSec - m_winSizeInSec;

        // remove the elements which are out of the windows
        while (!m_timeValuePairDeque.empty() && m_timeValuePairDeque.front().first < timeWindowBegin) {
            m_timeValuePairDeque.pop_front();
        }

        // remove all the elements smaller(or greater, depends on the m_cmp) than
        // the currently being added elements
        while (!m_timeValuePairDeque.empty() && m_compare(value, m_timeValuePairDeque.back().second)) {
            m_timeValuePairDeque.pop_back();
        }

        m_timeValuePairDeque.push_back(std::make_pair(timeInSec, value));
    }

    int getWindowSizeInSec() const {
        return m_winSizeInSec;
    }
    ;

    void clear() {
        m_timeValuePairDeque.clear();
    }

protected:
    bool getValue(std::pair<int32_t, T>& outTimeMaxvalPair) const {
        if (unlikely(m_timeValuePairDeque.empty())) {
            return false;
        } else {
            outTimeMaxvalPair = m_timeValuePairDeque.front();
            return true;
        }
    }

private:
    const CMP m_compare;
    const int32_t m_winSizeInSec;
    std::deque<std::pair<int32_t, T> > m_timeValuePairDeque;
};

//C98 does not support typedef on partial specialized template.
//Could be replaced by "using" key word after migrate to C11.
template<typename T>
class SlidingTimewindowMaxTracker: public SlidingTimewindowValueTracker<T, std::greater_equal<T> > {
public:
    SlidingTimewindowMaxTracker(const int32_t windowSizeInSec) :
            SlidingTimewindowValueTracker<T, std::greater_equal<T> >(windowSizeInSec) {
    }

    bool getMax(std::pair<int32_t, T>& outTimeMaxvalPair) const {
        return SlidingTimewindowValueTracker<T, std::greater_equal<T> >::getValue(outTimeMaxvalPair);
    }
};

template<typename T>
class SlidingTimewindowMinTracker: public SlidingTimewindowValueTracker<T, std::less_equal<T> > {
public:
    SlidingTimewindowMinTracker(const int32_t windowSizeInSec) :
            SlidingTimewindowValueTracker<T, std::less_equal<T> >(windowSizeInSec) {
    }

    bool getMin(std::pair<int32_t, T>& outTimeMaxvalPair) const {
        return SlidingTimewindowValueTracker<T, std::less_equal<T> >::getValue(outTimeMaxvalPair);
    }
};

#endif // BB_STATISTICS_SLIDINGTIMEWINDOWVALUETRACKER_H

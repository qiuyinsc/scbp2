#ifndef BB_STATISTICS_REGULARIZEDPRICEPROVIDERSKYRIM_H
#define BB_STATISTICS_REGULARIZEDPRICEPROVIDERSKYRIM_H

/* Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved. */

#include <bb/statistics/RegularizedPriceProvider.h>
#include <boost/circular_buffer.hpp>

namespace bb {

BB_FWD_DECLARE_SHARED_PTR( RegularizedPriceProviderSkyrim );

namespace statistics {

class RegularizedPriceProviderSkyrim
		: public RegularizedPriceProvider
		  {
		  public:
	RegularizedPriceProviderSkyrim( IPriceProviderCPtr underlying, const ClockMonitorPtr& clockMonitor, const bb::ptime_duration_t& interval,
			const bb::ptime_duration_t& offset );

	RegularizedPriceProviderSkyrim( IPriceProviderCPtr underlying, const ClockMonitorPtr& clockMonitor, const bb::ptime_duration_t& interval,
			const bb::ptime_duration_t& offset, const double returns_duration_in_intervals );

	virtual ~RegularizedPriceProviderSkyrim() {};

	virtual bool isPriceOK() const { return m_ok; }
	virtual double getRefPrice( bool* pSuccess = NULL ) const { return getRefLiveReturns( pSuccess ); }
	virtual double getRefLiveReturns( bool* pSuccess ) const; // SKYRIM
	std::string getDescription() const
	{
		return getInstrument().toString() + "." + boost::lexical_cast<std::string>( m_returns_duration_in_intervals ) +
				" # " + getInstrument().toString() + "." + boost::lexical_cast<std::string>( m_returns_duration_in_intervals ) + "_weighted";
	}

		  protected:
	virtual void onPeriodicWakeup( const timeval_t &ctv, const timeval_t &swtv );
	void onWakeupCall( const timeval_t& ctv, const timeval_t& swtv, int reason, void* pData );
	void onPriceChanged( const IPriceProvider& sender );

	mutable bool m_ok;
	double m_returns_duration_in_intervals;
	boost::circular_buffer<double> m_px_history;
	double m_initial_Price;
	double m_last_price;
	double m_EMA;
	double m_histAvgPrice;
		  };


/// PxPSpec for building a RegularizedPxP
class RegularizedPxPSkyrimSpec : public RegularizedPxPSpec
{
public:
	BB_DECLARE_SCRIPTING();

	RegularizedPxPSkyrimSpec() {}
	RegularizedPxPSkyrimSpec( const RegularizedPxPSkyrimSpec& a, const boost::optional<InstrSubst> &instrSubst );

	virtual IPriceProviderPtr build( PriceProviderBuilder *builder ) const;
	virtual void hashCombine( size_t &result ) const;
	virtual bool compare( const IPxProviderSpec *other ) const;
	virtual void print( std::ostream &o, const LuaPrintSettings &pset ) const;

	double m_returns_duration_in_intervals;
};
BB_DECLARE_SHARED_PTR( RegularizedPxPSkyrimSpec );

} //statistics
} // namespace bb

#endif // BB_STATISTICS_REGULARIZEDPRICEPROVIDERSKYRIM_H

#ifndef BB_STATISTICS_PRICEPROVIDERDATASAMPLER_H
#define BB_STATISTICS_PRICEPROVIDERDATASAMPLER_H

/* Contents Copyright 2016 Athena Capital Research LLC. All Rights Reserved. */

#include <bb/clientcore/PriceProvider.h>

#include <bb/statistics/DataSampler.h>

namespace bb {

BB_FWD_DECLARE_SHARED_PTR( IPxProviderSpec );

namespace statistics {

class PriceProviderDataSampler
    : public DataSamplerImpl
{
public:
    PriceProviderDataSampler( const std::string& desc, const IPriceProviderPtr& priceProvider );

    virtual bool isOK() const { return m_priceProvider->isPriceOK(); }
    virtual timeval_t getLastSampleTime() const { return m_priceProvider->getLastChangeTime(); }
    virtual boost::optional<bb::Priority> getPriority() const { return PRIORITY_DATASAMPLER_DELAY_SAMPLING; }

protected:
    void onPriceChanged( const IPriceProvider& pp );

    IPriceProviderPtr m_priceProvider;
    Subscription m_sub;
};
BB_DECLARE_SHARED_PTR( PriceProviderDataSampler );

class PriceProviderDataSamplerSpec
    : public DataSamplerSpec
{
public:
    BB_DECLARE_SCRIPTING();

    PriceProviderDataSamplerSpec();
    PriceProviderDataSamplerSpec( const PriceProviderDataSamplerSpec &e );

    virtual IDataSamplerPtr build( IStatisticBuilder* builder ) const;

    virtual PriceProviderDataSamplerSpec* clone() const;
    virtual void checkValid() const;
    virtual void print( std::ostream& o, const LuaPrintSettings& ps ) const;
    virtual void hashCombine( size_t& result ) const;
    virtual bool compare( const IDataSamplerSpec* other ) const;

    IPxProviderSpecPtr m_pxp;
};
BB_DECLARE_SHARED_PTR( PriceProviderDataSamplerSpec );

} // namespace statistics
} // namespace bb

#endif // BB_STATISTICS_PRICEPROVIDERDATASAMPLER_H

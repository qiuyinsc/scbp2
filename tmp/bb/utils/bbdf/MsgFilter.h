#ifndef BB_UTILS_BBDF_MSGFILTER_H
#define BB_UTILS_BBDF_MSGFILTER_H

/* Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved. */


#include <bb/core/timeval.h>
#include <bb/core/MStreamCallback.h>
#include <bb/core/smart_ptr.h>

namespace bb {
namespace df {

class IMsgFilter
{
public:
    // Destructor
    virtual ~IMsgFilter() {}

    /// Return false to stop this message from moving down the pipeline.
    /// To cause processing to end completely, reset the shared_ptr.
    ///
    /// Throw exceptions on errors.
    virtual bool process( MsgPtr& spMsg ) = 0;

    virtual bool does_output() const { return false; } // override in filters which publish messages to the outside
};
BB_DECLARE_SHARED_PTR(IMsgFilter);

/// The first item in a bbdf pipeline must be an IMsgInput, which creates input
/// without having a filter on the other side. This typically wraps an IMStream.
class IMsgInput
{
public:
    virtual ~IMsgInput() {}

    /// Calls the callback with each message that gets input;
    virtual void run(IMStreamCallback *cb) = 0;
    /// Stops the message stream.
    virtual void stop() = 0;
};
BB_DECLARE_SHARED_PTR(IMsgInput);


inline void sleepTill( const timeval_t& wake )
{
    struct timespec tspec = wake.to_timespec();
    int ret = clock_nanosleep( CLOCK_MONOTONIC, TIMER_ABSTIME, &tspec, NULL );
    BB_ASSERT( ret == 0 ); (void)ret;
}

} // namespace df
} // namespace bb

#endif // BB_UTILS_BBDF_MSGFILTER_H

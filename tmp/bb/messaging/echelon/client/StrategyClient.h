#ifndef BB_MESSAGING_ECHELON_CLIENT_STRATEGYCLIENT_H
#define BB_MESSAGING_ECHELON_CLIENT_STRATEGYCLIENT_H

/* Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved. */


#include <string>

#include <boost/function.hpp>

#include <bb/core/smart_ptr.h>
#include <bb/core/Buffer.h>
#include <bb/io/Socket.h>

namespace bb {
namespace messaging {

class StrategyClient
{
public:
    StrategyClient( const std::string& host, int16_t port );
    virtual ~StrategyClient() {}

    void setId( const std::string& id ) { m_id = id; }
    void setInstr( const std::string& instr ) { m_instr = instr; }
    void setType( const std::string& type ) { m_type = type; }

    bool connect();
    bool isConnected() const { return m_bIsConnected; }
    bb::fd_t getFD() const { return m_spTcpSocket->getFD(); }

    virtual bool send( const std::string& msg );

    typedef boost::function< const std::string() > format_func_t;
    virtual bool send( const std::string& prefix, const format_func_t& format_func );

    void read();

protected:
    bool processOne();
    virtual void onConnected();
    virtual void onMessage( const std::string& str );
    virtual void onDisconnected();

private:
    bool send_immed( const std::string& msg );

protected:
    bb::TCPSocketPtr m_spTcpSocket;
    std::string m_host;
    int16_t m_port;
    bb::Buffer m_buffer;
    bool m_bIsConnected;
    std::string m_id;
    std::string m_instr;
    std::string m_type;
};

BB_DECLARE_SHARED_PTR( StrategyClient );

} // namespace messaging
} // namespace bb

#endif // BB_MESSAGING_ECHELON_CLIENT_STRATEGYCLIENT_H
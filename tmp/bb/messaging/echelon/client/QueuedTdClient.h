#ifndef BB_MESSAGING_ECHELON_CLIENT_QUEUEDTDCLIENT_H
#define BB_MESSAGING_ECHELON_CLIENT_QUEUEDTDCLIENT_H

/* Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved. */


#include <string>
#include <boost/function.hpp>

#include <bb/core/Buffer.h>
#include <bb/core/ListenerList.h>
#include <bb/core/SelectDispatcher.h>
#include <bb/core/queue/ThreadPipeNotifier.h>
#include <bb/core/queue/waitable_queue.h>
#include <bb/io/Socket.h>

#include <bb/messaging/echelon/client/IStringMsgListener.h>

namespace bb {
namespace messaging {

class BasicClient
{
public:
    BasicClient( const std::string& host, int16_t port );
    virtual ~BasicClient() {}
    virtual bool connect();
    bool isConnected() const { return m_bIsConnected; }
    bb::fd_t getFD() const { return m_spTcpSocket->getFD(); }

    virtual bool send( const std::string& msg );
    void read();

protected:
    bool processOne();
    virtual void onConnected();
    virtual void onMessage( const std::string& str ) = 0;
    virtual void onDisconnected();
    virtual void sendIdentify() = 0;

protected:
    bb::TCPSocketPtr m_spTcpSocket;
    std::string m_host;
    int16_t m_port;
    bb::Buffer m_buffer;
    bool m_bIsConnected;
};

namespace echelon {

class QueuedClient
    : public BasicClient
    , public bb::ListenerList<IStringMsgListener*>
{
public:
    QueuedClient( const std::string& host, int16_t port )
        : BasicClient( host, port ) {}

    virtual bool send( const std::string& s );

    void registerReadDispatch( bb::SelectDispatcher& sd );
    void registerSendDispatch( bb::SelectDispatcher& sd );

protected:
    typedef boost::function< void ( ) > action_t;
    typedef bb::queue::waitable_concurrent_queue< action_t, bb::queue::ThreadPipeNotifier > queue_t;
    typedef bb::queue::waitable_concurrent_queue< std::string, bb::queue::ThreadPipeNotifier > sendqueue_t;

protected:
    void postAction( const action_t& action );
    void doAction();
    void processQueuedSend();

    virtual void onMessage( const std::string& msg );
    virtual void processMessage( const std::string& msg );

protected:
    bb::Subscription m_subSend;
    bb::Subscription m_subRead;

    queue_t m_actionQueue;
    sendqueue_t m_sendQueue;
};

class NullQueuedClient : public QueuedClient
{
public:
    NullQueuedClient() : QueuedClient( "localhost", 0 ) {}

    virtual bool send( const std::string& s ) { return false; }
    virtual void sendIdentify() {}
    virtual bool connect() { m_bIsConnected = true; return true; }
};

class QueuedTdClient
    : public QueuedClient
{
public:
    QueuedTdClient( const std::string& host, int16_t port );
    virtual ~QueuedTdClient() {}

    void setId( const std::string& id ) { m_id = id; }
    void setAccounts( const std::string& accts ) { m_accounts = accts; }

protected:
    virtual void onConnected();
    virtual void onDisconnected();
    virtual void sendIdentify();

protected:
    std::string m_id;
    std::string m_accounts;
};

}

} // namespace messaging
} // namespace bb

#endif // BB_MESSAGING_ECHELON_CLIENT_QUEUEDTDCLIENT_H
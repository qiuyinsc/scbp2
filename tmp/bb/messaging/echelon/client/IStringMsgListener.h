#ifndef BB_MESSAGING_ECHELON_CLIENT_ISTRINGMSGLISTENER_H
#define BB_MESSAGING_ECHELON_CLIENT_ISTRINGMSGLISTENER_H

/* Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved. */


#include <string>

namespace bb {
namespace messaging {

class IStringMsgListener
{
public:
    virtual ~IStringMsgListener() {}
    virtual void onMsg( const std::string& msg ) {}
};

} // namespace messaging
} // namespace bb

#endif // BB_MESSAGING_ECHELON_CLIENT_ISTRINGMSGLISTENER_H
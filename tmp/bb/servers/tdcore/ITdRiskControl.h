#ifndef BB_SERVERS_TDCORE_ITDRISKCONTROL_H
#define BB_SERVERS_TDCORE_ITDRISKCONTROL_H

/* Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved. */


#include <boost/noncopyable.hpp>
#include <boost/function.hpp>
#include <boost/tuple/tuple.hpp>

#include <bb/core/side.h>
#include <bb/core/tif.h>
#include <bb/core/fillflag.h>

namespace bb {

class acct_t;
class instrument_t;
class TdOrderMsg;
class TdOrderNewMsg;
class TdOrderFutureMsg;
class TdOrderModifyMsg;
class tid_t;
class MarginLimitsMsg;
class MarginAccountInfoMsg;

namespace tdcore {

class TdOrder;

class SymbolData
{
public:
    SymbolData( double _price ) : price( _price )
                                , actual_shares( 0 )
                                , baseline_shares( 0 )
                                , position( 0 )
                                , pnl( 0 )
                                , halted( false )
                                , trade_price_received( false )
                                , actual_shares_delta( 0 )
                                , premium ( 100000 ) //setting the option premium value to a infinite safe value
                                , underlier_symbol_data( NULL )
                                , settlement_price ( 0 )
                                , expiration_date( date_t() )
                                , time_to_expiry_days( 0.0 )
                                , sqrt_time_to_expiry_days( 0.0 )
                                , sqrt_time_to_expiry_months_vega( 0.0 )
                                , sqrt_time_to_expiry_months_gamma( 0.0 )
                                , sqrt_time_to_expiry_year( 0.0 )
        {
            BOOST_STATIC_ASSERT( BID == 0 && ASK == 1 );  // make sure there won't be a buffer overflow
            potential_shares[ BID ] = potential_shares[ ASK ] = potential_shares_delta[ BID ] = potential_shares_delta[ ASK ] = 0;
            price_limits[0] = -std::numeric_limits<double>::infinity();
            price_limits[1] = std::numeric_limits<double>::infinity();
        }

    double price;                // Important: the name is misleading, for stocks = price, for futures = price x contract size, for options = underlier price x option contract size
    int32_t potential_shares[2]; // number of shares (+/-) if all the side's orders were filled.
                                 // e.g. if the current position is +1000 and there are 10000 shares
                                 // in outstanding sell orders and 5 shares in outstanding buy orders,
                                 // then potential_shares[BID] = 1005
                                 //      potential_shares[ASK] = -9000
    int32_t actual_shares; // number of shares (+/-) currently
    int32_t baseline_shares; // not really used for risk control, but needed by some trading programs
    double position; // actual_shares multiplied by the VWAP for those shares
    double pnl; // realized profit (+) or loss (-) in dollars
    bool halted;
    bool trade_price_received;

    typedef boost::function<double( double )> PriceValueConverterType;
    PriceValueConverterType m_exchange_value_to_priceConverter;
    PriceValueConverterType m_price_to_exchange_valueConverter;

    // used for options and maintained by TdRiskControlOptions
    int32_t potential_shares_delta[2];        // same as potential_shares but considering the delta of the instrument
    int32_t actual_shares_delta;              // same as actual_shares but considering the delta of the instrument
    double premium;                           // option premium. Used to calculate the PnL.
    const SymbolData* underlier_symbol_data;  // pointer to underlier SymbolData
    double settlement_price;                  // last settlement price. Note that definition is as misleading as for price. See above.
    date_t expiration_date;
    double time_to_expiry_days;
    double sqrt_time_to_expiry_days;
    double sqrt_time_to_expiry_months_vega;
    double sqrt_time_to_expiry_months_gamma;
    double sqrt_time_to_expiry_year;
    double price_limits[2];                   // price limits up and down for sanity checks

    int get_delta_multiplier(const instrument_t& instr ) const { return instr.is_option_put() ? -1 : 1; }
};

class AccountData
{
public:
    AccountData() { clear(); }
    void clear()
    {
        m_exposure[ BID ] = m_exposure[ ASK ] = 0;
        m_position[ BID ] = m_position[ ASK ] = 0;
        m_open_orders = 0;
        m_total_orders = 0;
        m_total_rejects = 0;
        m_pnl = 0;
        m_max_order_size = 0;
    }

    double m_exposure[2], // dollar value (+/-) if all the side's orders were filled
           m_position[2]; // dollar value (+/-) currently on each side
    int32_t m_open_orders;
    uint32_t m_total_orders;
    uint32_t m_total_rejects;

    double m_pnl; // realized profit (+) or loss (-) in dollars

    uint32_t m_max_order_size;
    std::string m_acct_descr;
};

class ITdRiskControl
{
public:
    virtual ~ITdRiskControl() {}

    // public methods are to be called only from the thread which constructed this
    virtual bool approve( const TdOrderMsg& order )       = 0; // true if order is approved
    virtual bool approve( const TdOrderFutureMsg& order ) = 0; // true if order is approved
    virtual bool approve( const TdOrderNewMsg& order )    { BB_THROW_ERROR_SS( __FUNCTION__ << " is not supported" ); } // true if order is approved
    virtual bool approve( const TdOrderModifyMsg& order ) { BB_THROW_ERROR_SS( __FUNCTION__ << " for TdOrderModifyMsg is not supported" ); } // true if order is approved

    virtual void halt()                              { BB_THROW_ERROR_SS( __FUNCTION__ << " is not supported" ); }
    virtual void resume()                            { BB_THROW_ERROR_SS( __FUNCTION__ << " is not supported" ); }
    virtual void halt( const instrument_t& instr )   { BB_THROW_ERROR_SS( __FUNCTION__ << " is not supported" ); }
    virtual void resume( const instrument_t& instr ) { BB_THROW_ERROR_SS( __FUNCTION__ << " is not supported" ); }
    virtual bool isHalted() const                    { BB_THROW_ERROR_SS( __FUNCTION__ << " is not supported" );  return false;}

    virtual void open ( const TdOrder& order ) = 0; // submit order (must have been approved previously)
    virtual void close( const TdOrder& order ) = 0; // end order (filled or not)

    // update position for an open order and return shares of the instrument

    virtual int32_t fill( dir_t dir, const instrument_t& instr, double price, int32_t size, fillflag_t fillflag = FILLFLAG_NONE ) = 0; // for unknown executions
    virtual int32_t fill( const TdOrder& order, double price, int32_t size ) = 0; // partial or complete fill received
    virtual int32_t late_fill( const TdOrder& order, double price, int32_t size ) = 0;
    virtual int32_t bust( const TdOrder& order, double price, int32_t size ) = 0; // automated trade bust "undoes" the fill

    /// handles a fill that does not correspond to an approved order
    /// this can be used for margin_create_fill messages, or for fill daemons processing out-of-band order flow
    virtual int32_t fill_without_order( dir_t dir, const instrument_t& instr, double price, int32_t size )
    { BB_THROW_ERROR_SS( __FUNCTION__ << " is not supported" ); };

    virtual void setSymbolPrice( const instrument_t& instr, double price ) = 0; // set price and adjust account values
    virtual void setSymbolNotionalValueFromPrice( const instrument_t& instr, double price ) = 0; // set notional value from price and adjust account values

    virtual void populateMessage( MarginLimitsMsg& msg ) = 0;
    virtual void populateMessage( MarginAccountInfoMsg& msg ) = 0;
};
BB_DECLARE_SHARED_PTR( ITdRiskControl );

class ITdRiskControlExt
{
public:
    virtual ~ITdRiskControlExt() {}

    virtual bool approve( const acct_t& acct, uint32_t orderid, dir_t dir, const instrument_t& instr, double price, int32_t size, tif_t tif ) = 0;

    virtual void open ( uint32_t orderid, const tid_t& tradeid, dir_t dir, const instrument_t& instr, int32_t size ) = 0;
    virtual void close( uint32_t orderid, const tid_t& tradeid, dir_t dir, const instrument_t& instr, int32_t left ) = 0;

    virtual int32_t fill( uint32_t orderid, const tid_t& tradeid, dir_t dir,
             const instrument_t& instr, double price, int32_t size ) = 0;

    virtual int32_t bust( uint32_t orderid, const tid_t& tradeid, dir_t dir,
              const instrument_t& instr, double price, int32_t size ) = 0;
};
BB_DECLARE_SHARED_PTR( ITdRiskControlExt );

} // tdcore
} // bb

#endif // BB_SERVERS_TDCORE_ITDRISKCONTROL_H

#ifndef BB_SERVERS_TDCORE_TDSCRIPTING_H
#define BB_SERVERS_TDCORE_TDSCRIPTING_H

/* Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved. */


#include <bb/core/Scripting.h>
#include <bb/servers/tdcore/TdServer.h>

struct lua_State;

namespace bb {
namespace tdcore {

///
/// Scriptable interface to the application
///
class TdScriptApp
{
public:
    BB_DECLARE_SCRIPTING();

protected:
    virtual ~TdScriptApp() {};

    virtual TdServer& server() const = 0;

private:
    void setVerbosity( int32_t ) const;

public: // needed by OrderRouterScriptApp due to a Luabind overloading quirk
    void login () { server().login (); }
    void logout() { server().logout(); }

private:
    bool halt  () { server().halt  (); return true; }
    bool halt( const std::string& instr )
        { server().halt( instrument_t::fromString( instr ) ); return true; }
    bool resume() { server().resume(); return true; }
    bool resume( const std::string& instr )
        { server().resume( instrument_t::fromString( instr ) ); return true; }
    bool isHalted() const { return server().isHalted(); }

    void sendTestRequest( const std::string& text = std::string() ) { server().sendTestRequest( text ); }
    void sendTestRequestEmpty() { sendTestRequest(); } // for Luabind function overloading

    void setNextOrderSeqNum( long order_seq ) { server().setNextOrderSeqNum( order_seq ); }

    void cancelOrder( unsigned int tid ) { server().cancelOrder( tid ); }
    void cancelOrders( const std::string& side, const std::string& symbol )
        { server().cancelOrders( bb::str2side( side.c_str() ), bb::symbol_t( symbol.c_str() ) ); }
    void cancelOrdersForClient( uint32_t clientId ) { server().cancelOrdersForClient( clientId ); }

    void enableCpuSpinning();
    void disableCpuSpinning();
    void pinToCore( int core );
    void unpin();

    void forceDoneOrder(unsigned int tid) {
        server().forceDoneOrder(tid);
    }
    void forceDoneOrders(const std::string& side, const std::string& symbol) {
        server().forceDoneOrders(bb::str2side( side.c_str() ), bb::symbol_t( symbol.c_str() ) );
    }

    void forceDoneOrdersForClient(uint32_t id) {
        server().forceDoneOrdersForClient(id);
    }

    void forceDoneOrdersForClientIdent(const std::string& ident) {
        server().forceDoneOrdersForClientIdent(ident);
    }

    void printStatus() const { server().printStatus(); }
    void savePositions( const std::string& filename ) const { server().savePositions( filename ); }

    void   printRiskLimits() const { server().printRiskLimits(); }
    void  reloadRiskLimits()       { server().reloadRiskLimits(); }
    void  reloadRiskLimits( const std::string& config_file ) { server().reloadRiskLimits( config_file ); }
    void recheckRiskLimits() const { server().recheckRiskLimits(); }
    void reapplyRiskLimits() const { server().reapplyRiskLimits(); }

    void printAllowedUsers() const { server().printAllowedUsers(); }
    void reloadAllowedUsers() { server().reloadAllowedUsers(); }

    void setAccountLimits(int ordersLimits) { server().setAccountLimits(ordersLimits); }
    void setAccountInstrumentLimits(const std::string& instr, int maxOrderQty, double maxPosition, double maxExposure)
    {
        server().setAccountInstrumentLimits(instrument_t::fromString( instr ), maxOrderQty, maxPosition, maxExposure);
    }
};

bool registerScriptingInterfaces(lua_State& state);

} // namespace tdcore
} // namespace bb

#endif // BB_SERVERS_TDCORE_TDSCRIPTING_H

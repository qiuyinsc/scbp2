#ifndef BB_SERVERS_TDCORE_TDCONFIG_H
#define BB_SERVERS_TDCORE_TDCONFIG_H

/* Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved. */


#include <set>

#include <luabind/object.hpp>

#include <bb/io/CFileFactory.h>
#include <bb/io/ByteSinkFactory.h>

#include <bb/servers/tdcore/TdRiskLimits.h>

#include <bb/core/env.h>
#include <bb/core/FDSet.h>
#include <bb/core/LuaConfig.h>
#include <bb/core/ptimefwd.h>

#include <bb/threading/ThreadPool.h>

namespace bb {
class LuaState;

namespace tdcore {

class TdConfig;

class TdLogConfig : public bb::LuaConfig<TdLogConfig>
{
public:
    TdLogConfig();
    static void describe();

    CFileConfig m_fileConfig;
    ByteSinkConfig m_byteSinkConfig;
};

// Stream operator to convert the config into human readable form
std::ostream& operator<<( std::ostream& out, const TdLogConfig& config );

class TdClientConfig : public bb::LuaConfig<TdClientConfig>
{
public:
    //enum ClientLogType{ UNBUFFERED_WRITE, BUFFERED_WRITE  /*, MMAP_WRITE, THREADED_WRITE */ };

    TdClientConfig();
    static void describe();

    TdLogConfig m_logConfig;
    threading::ThreadPoolConfig m_threadpoolConfig;
    bool m_rebroadcastFills;
    bool m_logAfterDisconnectIfHasOpenOrder;

    template<class T>
    static const TdClientConfig& getTdClientConfig( const T& config )
    {
        throw std::logic_error( "getTdClientConfig: not implemented" );
    }

    template<class T>
    static void setTdClientConfig( T& config, const luabind::table<>& table )
    {
        LuaConfig<TdClientConfig>::initFromLua( config.tdClientConfig, table );
    }
};

// Stream operator to convert the config into human readable form
std::ostream& operator<<( std::ostream& out, const TdClientConfig& config );


class TdOrderManagerConfig : public bb::LuaConfig<TdOrderManagerConfig>
{
public:
    TdOrderManagerConfig();
    static void describe();

    ptime_duration_t m_allowCancelAgainDuration;
    bool m_bSuppressCancelRejects;
    bool m_useFileBasedPersistentSeqNum;

    template<class T>
    static const TdOrderManagerConfig& getTdOrderManagerConfig( const T& config )
    {
        throw std::logic_error( "getTdClientConfig: not implemented" );
    }

    template<class T>
    static void setTdOrderManagerConfig( T& config, const luabind::table<>& table )
    {
        LuaConfig<TdOrderManagerConfig>::initFromLua( config.tdOrderManagerConfig, table );
    }
};

std::ostream& operator<<( std::ostream& os, const TdOrderManagerConfig& config );
class RiskLimits
    : public bb::tdcore::TdRiskLimits
{
public:
    RiskLimits();
    RiskLimits( const luabind::table<>& table, double default_price );
    static bool luaRegister( bb::LuaState& state, double default_price );
};

class TdConfig
{

public:
    template<typename Config>
    static Config load( bb::LuaState& state, const std::string& filename,
                        double default_price = std::numeric_limits<double>::infinity() );

    static TdConfig load( bb::LuaState& state, const std::string& filename );

    RiskLimits riskLimits;

    uint32_t firstOrderSeqNum;
    std::string lanInterface;

    typedef std::map<bb::mktdest_t, std::string> FixSessionMap;
    FixSessionMap sessionMap;

    struct AllowedUsers
    {
        typedef std::set<std::string> Names;

        void clear();
        bool empty() const;
        bool contains( const std::string& name ) const;

        Names admins;
        Names traders;
    };
    AllowedUsers allowedUsers;

    TdClientConfig tdClientConfig;

    TdOrderManagerConfig tdOrderManagerConfig;

protected:
    TdConfig();

    static luabind::object load_impl( bb::LuaState& state, const std::string& filename );

private:
    static bool luaRegister( bb::LuaState& state, double default_price );
    static bool luaRegisterOnce( bb::LuaState& state, const char* derivedClassName ); // true if not registered
};

/// Create a TdConfig object from filename.
/// Register TdConfig into Lua the first time this is called.
template<typename Config>
Config TdConfig::load( bb::LuaState& state, const std::string& filename, double default_price )
{
    luaRegister( state, default_price ); // our method
    if( luaRegisterOnce( state, typeid( Config ).name() ) )
        Config::luaRegister( state ); // derived class method

    return luabind::object_cast<Config>( load_impl( state, filename ) );
}

std::ostream& operator<<( std::ostream& out, const TdConfig::AllowedUsers& users );

// Class for configuring the allowed users
class AllowedUsersConfig
    : public LuaConfig<AllowedUsersConfig>
{
public:
    AllowedUsersConfig();
    static void describe();
    bool isTrader(const std::string& user);
    bool isAdmin( const std::string& user);

    typedef std::set<std::string>   UserSet;

    UserSet m_traders;
    UserSet m_admins;
};

// clearing information to be sent to the TDs on connection and to the exchange on each order
class ClearingConfig
    : public LuaConfig<ClearingConfig>
{
public:
    ClearingConfig();
    static void describe();

    std::string                  m_mpid;
    std::string                  m_account; // if specified, MPID must also be specified
};

// Account configuration
class TdAccountConfig: public LuaConfig<TdAccountConfig>
{
public:
    TdAccountConfig();
    static void describe();

    AllowedUsersConfig               m_allowedUsers;
    RiskLimits                       m_limits;
    ClearingConfig                   m_clearingInfo;
    bool                             m_isExchangeAccount;
};

// Base MultiAccountConfig class.  To use this create a new LuaConfig
// class that also inherits from this class.
class MultiAccountTdConfig
{
public:
    MultiAccountTdConfig()
        : m_firstOrderSeqNum(0)
        , m_enableWebServer( false )
    {};

    // Call this class from the child class describe function like this:
    // static void describe(){
    // bb::tdcore::MultiAccountTdConfig::describe( Self::create() )
    //    .param("param1",     &Self::m_param1)
    //    .param("param2",     &Self::m_param2)
    //    ;
    // }
    template<typename Description>
    static Description& describe( Description& desc)
    {
        desc.param("accounts",             &MultiAccountTdConfig::m_accountMap)
            .param("enable_web_server",    &MultiAccountTdConfig::m_enableWebServer)
            .param("order_manager_config", &MultiAccountTdConfig::m_orderManagerConfig)
            .param("client_config",        &MultiAccountTdConfig::m_clientConfig )
            .param("first_order_seq_num",  &MultiAccountTdConfig::m_firstOrderSeqNum )
            ;
        return desc;
    }

    typedef std::map<bb::acct_t, TdAccountConfig>  AccountConfigMap;

    template<typename ConfigType>
    static ConfigType load(const std::string& fname)
    {
        // get the base lua State
        LuaState& luaState = bb::DefaultCoreContext::getEnvironment()->luaState();
        RiskLimits::luaRegister( luaState, 1000 );

        // Load the file
        luaState.load( fname );
        return ConfigType::fromLua( luaState["MultiAccountTdConfig"] );
    }

    TdAccountConfig& getAccountConfig( const acct_t account )
    {
        AccountConfigMap::iterator iter = m_accountMap.find( account );
        if( iter == m_accountMap.end() )
        {
            BB_THROW_ERROR_SSX("Account not in account map: " << account );
        }
        return iter->second;
    }
    uint32_t              m_firstOrderSeqNum;
    bool                  m_enableWebServer;
    AccountConfigMap      m_accountMap;
    TdClientConfig        m_clientConfig;
    TdOrderManagerConfig  m_orderManagerConfig;
};

std::ostream& operator<<( std::ostream& out, const AllowedUsersConfig& users );

} // namespace tdcore
} // namespace bb

#endif // BB_SERVERS_TDCORE_TDCONFIG_H

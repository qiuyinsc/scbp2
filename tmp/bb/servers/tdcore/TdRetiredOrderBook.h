#ifndef BB_SERVERS_TDCORE_TDRETIREDORDERBOOK_H
#define BB_SERVERS_TDCORE_TDRETIREDORDERBOOK_H

/* Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved. */


#define BOOST_MULTI_INDEX_DISABLE_SERIALIZATION // eliminate unnecessary dependency
#include <boost/multi_index_container.hpp>
#include <boost/multi_index/hashed_index.hpp>
#include <boost/multi_index/member.hpp>
#undef BOOST_MULTI_INDEX_DISABLE_SERIALIZATION

#include <bb/core/tid.h>
#include <bb/servers/tdcore/TdOrder.h>

namespace bb {
namespace tdcore {

namespace TdRetiredOrderBook_detail { // private namespace which we are free to pollute with "using namespace"

using namespace boost::multi_index;

typedef uint32_t seqnum_t;

// this container provides several ways of finding an order
// its declaration is hideous, yes, but its functionality is many-splendored
typedef multi_index_container<TdOrderPtr, indexed_by<
        // index by tradeid
        hashed_unique<tag<tid_t>, member<TdOrder, tid_t, &TdOrder::tradeid> >
    > > orders_t;

} // namespace TdOrderBook_detail

class TdOrdersHolder
{
protected:
    typedef TdRetiredOrderBook_detail::orders_t orders_t;

public:
    void insert( const TdOrderPtr& order )
    {
        m_orders.insert( order );
    }

    TdOrderPtr find( const tid_t& tid ) const
    {
        const orders_t::index<tid_t>::type& index = m_orders.get<tid_t>();
        orders_t::index<tid_t>::type::const_iterator it = index.find( tid );
        return it == index.end() ? TdOrderPtr() : *it;
    }

    template<typename Action>
    void forEachOrder( Action action ) const
    {
        std::for_each( m_orders.begin(), m_orders.end(), action );
    }

protected:
    orders_t m_orders;
};

class TdRetiredOrderBook : public TdOrdersHolder
{
};
BB_DECLARE_SHARED_PTR( TdRetiredOrderBook );

} // namespace tdcore
} // namespace bb

#endif // BB_SERVERS_TDCORE_TDRETIREDORDERBOOK_H

#ifndef BB_SERVERS_TDCORE_MULTIACCOUNTTDDISPATCHERBASE_H
#define BB_SERVERS_TDCORE_MULTIACCOUNTTDDISPATCHERBASE_H

/* Contents Copyright 2014 Athena Capital Research LLC. All Rights Reserved. */

#include <vector>
#include <set>

#include <bb/core/acct.h>
#include <bb/core/Msg.h>
#include <bb/core/oreason.h>
#include <bb/core/source.h>
#include <bb/core/FDSetFwd.h>
#include <bb/core/Subscription.h>

#include <bb/io/InProcessTransport.h>

#include <bb/servers/tdcore/TdConfig.h>

namespace bb {
class UserMessageMsg;
BB_FWD_DECLARE_SHARED_PTR( ISendTransport );
BB_FWD_DECLARE_SHARED_PTR( TCPSocket );
BB_FWD_DECLARE_SHARED_PTR( UnixSocket );
BB_FWD_DECLARE_SHARED_PTR( SelectDispatcher );

namespace tdcore {
class MultiAccountTdPositionBroadcaster;
class TdPositionSender;
class TdRiskControl;
class MultiAccountRiskControl;
BB_FWD_DECLARE_SHARED_PTR( TdClient );
BB_FWD_DECLARE_SHARED_PTR( TdClientFactory );

/// for programs which do not enter orders, such as fill daemons
class MultiAccountTdDispatcherBase
{
    typedef boost::function<void( const TdClientPtr& )> ClientConnectionEndCB;

public:
    void setClientConnectionEndCB( const ClientConnectionEndCB& );
    void setAllowedUsers( const TdConfig::AllowedUsers& allowedUsers );

    void addAccount( const acct_t& account );

    virtual void run();

    ByteSinkFactoryPtr getLogSinkFactory() { return m_logSinkFactory; };
protected:
    // The first constructor is for the normal mode of operation for
    // MultiAccountTdDispatcherBase. Use the second constructor to inhibit the normal
    // construction of an internal SelectDispatcher, for cases where
    // the MultiAccountTdDispatcherBase will be driven by an external event loop.
    MultiAccountTdDispatcherBase( EFeedType sourceType );
    MultiAccountTdDispatcherBase( EFeedType sourceType, SelectDispatcher& fdSet);

    virtual ~MultiAccountTdDispatcherBase() {}

    virtual void stop();

    virtual const TdClientFactoryPtr getClientFactory( acct_t account ){ return m_clientFactory; }

    virtual bool isAllowedUser(acct_t account, const char* user) const;

    // support for derived class
    source_t              getSource() const     { return m_source; }
    ISendTransportPtr&    getRebroadcaster()    { return m_rebroadcaster; }
    SelectDispatcher&     getDispatcher()       { return m_dispatcher; }

    // support from derived class
    virtual MultiAccountRiskControl&         getRiskControl()         = 0;

    // derived classes may implement this to return true if they handled the given Msg
    virtual bool handleClientMsgHook( const TdClientPtr&, const Msg& ) { return false; }
    virtual void assertUserIsAdmin( const TdClientPtr&, const Msg& ) const;

    void initializeFactories(const TdClientConfig &config);

    std::set<acct_t> m_activeAccounts;

    bool m_stop;

    void setActiveClient( TdClient* client ) { m_activeClient = client; };
    TdClient* getActiveClient() const { return m_activeClient; };


    void createLogFactory (const TdClientConfig &config);

private:

    void handleConnectionTCP( bb::acct_t account, const bb::TCPSocketPtr& server );
    void handleConnectionUnix( bb::acct_t account, const bb::UnixSocketPtr& server );
    void handleConnectionInProc( bb::acct_t account, const InProcessTransportPtr& client );

    void handleClientMsg( const TdClientPtr& bbc, const Msg& msg );
    void handleUserMessage( const UserMessageMsg& user, const TdClientPtr& bbc );

    const source_t m_source;

    ClientConnectionEndCB m_clientConnectionEndCB;

    ISendTransportPtr m_rebroadcaster;

    // client connection acceptors
    std::vector<bb::TCPSocketPtr>  m_serverSockets;
    std::vector<bb::UnixSocketPtr> m_serverUnixSockets;
    std::vector<InProcessTransportServerPtr> m_serverInProcess;

    std::vector<bb::Subscription> m_selectSubs;

   // m_optinalInternalDispatcher is set if TdDispatcher owns its own SelectDispatcher.
    shared_ptr<SelectDispatcher> m_optionalInternalDispatcher;
    SelectDispatcher&            m_dispatcher;

    const TdConfig::AllowedUsers* m_allowedUsers; // if NULL, all users are allowed

    TdClient*                   m_activeClient;
    TdClientFactoryPtr          m_clientFactory;
    threading::ThreadPoolPtr    m_loggingThreadPool;
    ByteSinkFactoryPtr          m_logSinkFactory;

};

} // namespace tdcore
} // namespace bb

#endif // BB_SERVERS_TDCORE_MULTIACCOUNTTDDISPATCHERBASE_H

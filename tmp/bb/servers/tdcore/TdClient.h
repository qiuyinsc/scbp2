#ifndef BB_SERVERS_TDCORE_TDCLIENT_H
#define BB_SERVERS_TDCORE_TDCLIENT_H

/* Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved. */


#include <boost/enable_shared_from_this.hpp>
#include <boost/foreach.hpp>

#include <bb/core/hash_map.h>
#include <bb/core/fillflag.h>
#include <bb/core/CError.h>
#include <bb/core/FDSet.h>
#include <bb/core/Log.h>
#include <bb/core/Msg.h>
#include <bb/core/messages.h>
#include <bb/core/CompactMsg.h>
#include <bb/core/StaticString.h>

#include <bb/io/ByteSink.h>
#include <bb/io/ByteSinkFactory.h>
#include <bb/io/MQ.h>
#include <bb/io/LoggedRecvTransport.h>
#include <bb/io/LoggedSendTransport.h>

#include <bb/servers/tdcore/ITdClient.h>
#include <bb/servers/tdcore/TdConfig.h>
#include <bb/servers/tdcore/TdAccountManager.h>

#include <bb/threading/ThreadPool.h>

namespace bb {

class SaslMsg;

BB_FWD_DECLARE_SHARED_PTR( CFile );
BB_FWD_DECLARE_SHARED_PTR( SaslServer );
BB_FWD_DECLARE_SHARED_PTR( InProcessTransport );
BB_FWD_DECLARE_SHARED_PTR( ByteSinkFactory );

namespace tdcore {
BB_FWD_DECLARE_SHARED_PTR( TdClient );

typedef StaticString<24> ClearingAccountString;
/// Represents a client connection.  TdClient instances persist
/// throughout the entire run of the TD, even if clients disconnect.
/// This is because there may be stray pointers to a TdClient due to
/// outstanding orders if a client unexpectedly disconnects.  If these
/// outstanding orders enter back into the TD as a message and
/// TdClients are deleted when clients disconnect, they'll be mapped
/// to a non-existant instance.  Thus, TdClients are deleted only
/// during program termination (by calling remove_all).
///
/// @param sock Takes ownership of the passed in socket.
/// @note This should probably be managed by shared pointers.
///
class TdClient
    : public ITdClient
    , private IMStreamCallback
    , public boost::enable_shared_from_this<TdClient>
{
public:
    typedef boost::function<void( const TdClientPtr&, const Msg& )> MessageCB;
    typedef boost::function<void( const TdClientPtr& )> ConnectionEndCB;


    void setIdent( const std::string& name ) { ident = name; }
    const std::string& getIdent() const { return ident; }
    virtual bool isAuthenticated() const;
    const char* authenticatedUsername() const;

    uint32_t getId() const { return id; }
    acct_t getAccount() const { return acct; }

    /// send ClientAcceptMsg to indicate the connection is established
    bool sendAccept();

    void increaseOpenOrderCount() { ++m_openOrderCount; }
    void decreaseOpenOrdercount() {
        BB_THROW_EXASSERT_SSX_DEBUG ( m_openOrderCount >= 1, "Expected order count to be >= 1" );
        --m_openOrderCount;
        if(unlikely(connectionClosed && m_openOrderCount == 0)) {
            resetLogSink();
        }
    }

    /// send TdStatusChange message
    virtual bool sendChange( const instrument_t& instr
        , const tid_t& tradeid
        , uint32_t clientid
        , uint32_t orderid
        , uint32_t seq
        , ostatus_t old_status
        , ostatus_t new_status
        , cxlstatus_t old_cxlstatus
        , cxlstatus_t new_cxlstatus
        , oreason_t reason
        , uint64_t exchref
        , const timeval_t& exch_time
        , const TdTimingInfo & timing_info
        /// TODO: move the next 3 fields to TdTimingInfo
        , const timeval_t& application_time = timeval_t() //< This is the time the FixApplication saw the message
        , const timeval_t& nic_time = timeval_t()
        );

    /// This message is introduced to send status changes back after
    /// orders are modified
    virtual bool sendChange( const instrument_t& instr
                     , const tid_t& tradeid
                     , uint32_t clientid
                     , uint32_t orderid
                     , uint32_t seq
                     , ostatus_t old_status
                     , ostatus_t new_status
                     , cxlstatus_t old_cxlstatus
                     , cxlstatus_t new_cxlstatus
                     , oreason_t reason
                     , uint64_t exchref
                     , const timeval_t& exch_time
                     , const TdTimingInfo & timing_info
                     /// Fields related to the original order
                     , const tid_t& orig_tradeid
                     , uint32_t orig_orderid
                     , uint32_t orig_seq
                     , ostatus_t old_status_orig
                     , ostatus_t new_status_orig
                     , cxlstatus_t old_cxlstatus_orig
                     , cxlstatus_t new_cxlstatus_orig
                     , oreason_t orig_reason
                     , uint64_t orig_exchref
                     ///

                     , const timeval_t& application_time = timeval_t() //< This is the time the FixApplication saw the message
                     , const timeval_t& nic_time = timeval_t()
        );


    // for stocks
    virtual bool sendFill( symbol_t sym
        , const tid_t& tradeid
        , unsigned int clientid
        , unsigned int orderid
        , unsigned int seq
        , double px
        , unsigned int size
        , unsigned int left
        , dir_t dir
        , mktdest_t dest
        , int32_t cur_pos
        , const timeval_t& exch_time
        , liquidity_t liquidity = LIQ_UNKNOWN
        , mktdest_t contra_broker = MKT_UNKNOWN
        , const std::string& fee_code = "");

    virtual bool sendFillFuture( const instrument_t& instr
        , const tid_t& tradeid
        , unsigned int clientid
        , unsigned int orderid
        , unsigned int seq
        , double px
        , unsigned int size
        , unsigned int left
        , dir_t dir
        , const char* cparty
        , mktdest_t dest
        , int32_t cur_pos
        , const timeval_t& exch_time
        , liquidity_t liquidity = LIQ_UNKNOWN
        , fillflag_t fillflags = FILLFLAG_NONE
        /// TODO: move the next 3 fields to TdTimingInfo
        , const uint64_t exchangeOrderId = 0
        , const timeval_t& application_time = timeval_t()
        , const timeval_t& nic_time = timeval_t()
        );

    // for options (and eventually for everything)
    virtual bool sendFill( const instrument_t& instr
        , const tid_t& tradeid
        , unsigned int clientid
        , unsigned int orderid
        , double px
        , unsigned int size
        , unsigned int left
        , dir_t dir
        , mktdest_t dest
        , int32_t cur_pos
        , const timeval_t& exch_time
        , liquidity_t liquidity
        , mktdest_t contra_broker
        , fillflag_t fillflags );

    virtual bool sendCancelReject( symbol_t sym, unsigned int orderid );

    virtual bool sendReprice( const instrument_t& instr
        , const tid_t& tradeid
        , unsigned int clientid
        , unsigned int orderid
        , double px
        , const timeval_t& exch_time );

    virtual bool sendRoute( const instrument_t& instr
        , const tid_t& tradeid
        , unsigned int orderid
        , mktdest_t via_mkt );

    /// This is not intended for business-oriented messages,
    /// but rather for side-band communications like UserMessages.
    virtual bool send( const Msg& msg );
    void send( const Msg* msg ); // ISendTransport

    /// if set, the client wishes for any open orders to be canceled when it disconnects
    /// this is a failsafe feature only, and there are various reasons why orders might not be canceled
    /// traders should explicitly cancel their orders before disconnecting under normal operation
    void setCancelOnDisconnect( bool enable );
    bool getCancelOnDisconnect() const { return cancelOnDisconnect; }

    void setClearingConfig( const std::string& mpid, const std::string& account );
    const boost::optional<std::string>& getClearingMPID()    const { return clearingMPID; }
    const boost::optional<std::string>& getClearingAccount() const { return clearingAccount; }
    const ClearingAccountString& getOrSetClearingAccount( const std::string& acct) const;

    void handleSaslMessage( const SaslMsg& msg );
    bool isConnectionClosed() const { return connectionClosed; }

    static void sendAll( const Msg& msg );

    /// returns the number of send failures
    static uint32_t sendAllConnected(
        const Msg& msg,
        const boost::function< bool ( const TdClientPtr& client ) >& pred = 0 );

    static void each( const boost::function< void ( const TdClientPtr& client ) >& f )
    {
        typedef std::pair<unsigned int, TdClientPtr> pair_t;
        BOOST_FOREACH( const pair_t& e, s_clientMap )
        {
            f( e.second );
        }
    }
    void init(FDSet & fdSet, const MessageCB& msgCB, const ConnectionEndCB &endCB);


public:
    /// open a "client log file" with a unique name
    /// the "add" methods use this internally; it's exposed for direct use by fill daemons only
    static CFilePtr createLogFile( acct_t acct );

    // closes connection with client, but keeps open any listeners that were already in place
    void closeClient( bool unexpected = false );

    void resetLogSink();

    // allows the registration of a client as the terminator of the program
    // this allows the class to avoid considering this client when it decides
    // if any clients are still connected when remove_all is called
    static void make_terminator( TdClientPtr client );
    // returns true if there were any clients (other than the terminator) to remove
    // when this function is called during program shutdown, this indicates badness
    static bool remove_all();

    static int count() { return num_clients; }

    TdClient( ISendTransportPtr send, IRecvTransportPtr recv, acct_t _acct,
              const ISendTransportPtr& rebroadcast, const source_t& source,
              ByteSinkPtr _logSink, bool continueLogMsgAfterDisconnect = false,
              TdAccountManager* acctMgr = nullptr);

    const timeval_t& getLastClientNicTime();
    void setLastMsgRecvTime( const timeval_t& tv ) { m_lastMsgRecvTime=tv; };
    void setLastMsgNicTime( const timeval_t& tv ) { m_lastMsgNicTime=tv; };

    TdPositionSender& getPositionSender()    { return m_accountMgr->m_positionSender; };
    ITdRiskControl&   getRiskControl() const { return m_accountMgr->m_riskControl; };

protected:
    TdClient( ISendTransportPtr send, IRecvTransportPtr recv, acct_t _acct
        , const ISendTransportPtr& rebroadcast, const source_t& source
        , bool continueLogMsgAfterDisconnect = false);

private:
    void onMessage( const Msg &msg );

    // like send(), but also sends to the rebroadcastSendTransport
    bool sendFill( const Msg& msg );

    acct_t acct;
    uint32_t id;
    source_t src;
    std::string ident;

    bool cancelOnDisconnect;
    const bool m_continueLogMsgAfterDisconnect;

    boost::optional<std::string>  clearingMPID;
    boost::optional<std::string>  clearingAccount;
    mutable ClearingAccountString cachedClearingAccount;

    ByteSinkPtr logSink;

    LoggedSendTransportPtr loggedSendTrans;
    LoggedRecvTransportPtr loggedRecvTrans;

    MessageCB messageCB;
    ConnectionEndCB connectionEndCB;;

    ISendTransportPtr rebroadcastSendTransport; // for sending fills on SRC_ORDERS

    SaslServerPtr saslServer;
    bool connectionClosed;

    int32_t m_openOrderCount;

protected:
    timeval_t m_lastMsgRecvTime;
    timeval_t m_lastMsgNicTime;

private:
    TdAccountManager*  m_accountMgr;

    // keep messages as member variables to prevent
    // a memset each time a msg is sent
    CompactMsg<TdStatusChangeMsg>  m_statusChange;
    CompactMsg<TdModifyStatusChangeMsg>  m_modifyStatusChange;
    CompactMsg<TdFillFutureMsg>    m_fillFuture;
    CompactMsg<TdFillMsg>          m_fill;
    CompactMsg<TdFillNewMsg>       m_fillNew;
    CompactMsg<TdRepriceMsg>       m_reprice;
    CompactMsg<TdRouteMsg>         m_route;
    CompactMsg<TdCancelRejectMsg>  m_cancelReject;

    template< class MsgT >
    void setMsgStats( MsgT& msg, const TdTimingInfo & timing_info );
    const timeval_t& getRecvCbTimeval();

public:
    typedef bbext::hash_map<unsigned int, TdClientPtr> TdClientMap;

    // Make the static variables public so the factory can get to them
    // This is temporary until all the static variables have been migrated to the factory
    static unsigned int global_id; //! unique id for clients
    static TdClientMap s_clientMap;    //! holds global list of clients
    static int num_clients;        //! count of connected clients (minus the terminator, if any)
};

inline bool TdClient::send( const Msg& msg )
{
    try {
        loggedSendTrans->send( &msg );
        return true;
    } catch ( const std::exception& ex ) {
        LOG_WARN << "failed to send " << msg.mtype() << " message to " << ident << ": " << ex.what() << bb::endl;
        return false;
    }
}

inline void TdClient::send( const Msg* msg )
{
    if( !send( *msg ) )
        BB_THROW_ERROR_SS( "TdClient failed to send message to " << ident );
}

inline void TdClient::sendAll( const Msg& msg )
{
    typedef std::pair<unsigned int, TdClientPtr> pair_t;

    BOOST_FOREACH( const pair_t& clientid, s_clientMap )
    {
        clientid.second->send( &msg );
    }
}

inline uint32_t TdClient::sendAllConnected(
    const Msg& msg,
    const boost::function< bool ( const TdClientPtr& client ) >& pred )
{
    typedef std::pair<unsigned int, TdClientPtr> pair_t;

    uint32_t numFailure = 0;

    BOOST_FOREACH( const pair_t& clientid, s_clientMap )
    {
        if( !clientid.second->isConnectionClosed() )
        {
            if( pred && !pred( clientid.second ) ) continue;

            if( !clientid.second->send( msg ) )
            {
                ++numFailure;
            }
        }
    }

    return numFailure;
}

inline bool TdClient::sendFill( const Msg& msg )
{
    // only fills should be rebroadcast on SRC_ORDERS
    BB_ASSERT( msg.mtype() == MSG_TD_FILL || msg.mtype() == MSG_TD_FILL_FUTURE || msg.mtype() == MSG_TD_FILL_NEW );

    bool ok = send( msg ); // first send to client

    if( rebroadcastSendTransport )
    {
        try {
            rebroadcastSendTransport->send( &msg );
        } catch ( const std::exception& ex ) {
            LOG_WARN << "failed to broadcast " << msg.mtype() << " message for " << ident << ": " << ex.what() << bb::endl;
            ok = false;
        }
    }

    return ok;
}

BB_FWD_DECLARE_SHARED_PTR(TdClientFactory);
/// TdClient factory object
///   This class is designed to encapsulate all the static behavior currently
///   embedded in the TdClient class. It currently uses the static variables in
///   the TdClient class untill all code has been migrated to use the factory
///   object.  Once the migration has completed all static functions and variable
///   will be contained in this class
class TdClientFactory{

public:
    TdClientFactory( const TdClientConfig& config, ByteSinkFactoryPtr factory, TdAccountManager* accountMgr = NULL);

    /// static function to add a client, returning its ID
    uint32_t add( TCPSocket* s, acct_t acct, const ISendTransportPtr& rebroadcast
            , const source_t& src, FDSet& fdSet, const TdClient::MessageCB& messageCB,
            const TdClient::ConnectionEndCB &endCB = TdClient::ConnectionEndCB() );

    uint32_t add( MQPseudoSocket* s, acct_t acct, const ISendTransportPtr& rebroadcast
            , const source_t& src, FDSet& fdSet, const TdClient::MessageCB& messageCB,
            const TdClient::ConnectionEndCB &endCB = TdClient::ConnectionEndCB() );

    uint32_t add( UnixSocket* s, acct_t acct, const ISendTransportPtr& rebroadcast
            , const source_t& src, FDSet& fdSet, const TdClient::MessageCB& messageCB,
            const TdClient::ConnectionEndCB& endCB = TdClient::ConnectionEndCB() );

    uint32_t add( const InProcessTransportPtr& transport, acct_t acct, const ISendTransportPtr& rebroadcast
            , const source_t& src, FDSet& fdSet, const TdClient::MessageCB& messageCB,
            const TdClient::ConnectionEndCB& endCB = TdClient::ConnectionEndCB() );

    uint32_t add( SCTPSocket* s, acct_t acct, const ISendTransportPtr& rebroadcast
            , const source_t& src, FDSet& fdSet, const TdClient::MessageCB& messageCB,
            const TdClient::ConnectionEndCB &endCB = TdClient::ConnectionEndCB() );

private:

    uint32_t addImpl( ISendTransportPtr send, IRecvTransportPtr recv
                      , acct_t acct, const ISendTransportPtr& rebroadcast, const source_t& src
                      , FDSet& fdSet, const TdClient::MessageCB& messageCB, const TdClient::ConnectionEndCB& connectionEndCB );

    ByteSinkPtr createLogFile( acct_t acct );

    const TdClientConfig&        m_clientConfig;
    ByteSinkFactoryPtr           m_sinkFactory;
    TdAccountManager*            m_accountManager;
};

} // tdcore
} // bb

#endif // BB_SERVERS_TDCORE_TDCLIENT_H

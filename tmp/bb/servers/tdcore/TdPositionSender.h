#ifndef BB_SERVERS_TDCORE_TDPOSITIONSENDER_H
#define BB_SERVERS_TDCORE_TDPOSITIONSENDER_H

/* Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved. */


#include <bitset>
#include <map>

#include <boost/noncopyable.hpp>

#include <bb/core/acct.h>
#include <bb/core/instrument.h>
#include <bb/core/EFeedType.h>
#include <bb/core/symbol.h>

#include <bb/io/SendTransport.h>

namespace bb {

BB_FWD_DECLARE_SCOPED_PTR(MarginInstrumentInfoMsg);

namespace tdcore {

class SymbolData;
class ITdPositionStore;

/// Broadcasts position updates periodically, using TdRiskControl to provide the position data
class TdPositionSender
    : boost::noncopyable
{
public:
    TdPositionSender( ITdPositionStore&, acct_t, EFeedType = SRC_TD );

    // send message for the given instrument now and on future fills from other clients
    void   subscribe( bb::instrument_t const&, ISendTransportWeakPtr const&);
    // stop sending messages for the given instrument
    // SYM_ALL may be used to unsubscribe from all updates
    void unsubscribe( bb::instrument_t const&, ISendTransportWeakPtr  const&);

    // propagate a position update for the given symbol to all clients
    // skip sending to the given client (if any) to reduce message traffic - it will get a fill message anyway
    void update( bb::instrument_t, const ISendTransport* = NULL );

private:
    void send( const ISendTransportPtr&, const instrument_t&, const SymbolData& );

    typedef std::bitset<SYM_MAX> SymbolSet;
    typedef std::map<ISendTransportWeakPtr, SymbolSet> ClientSymbolMap;

    ITdPositionStore& m_riskControl;
    ClientSymbolMap m_clientSymbolMap;
    const MarginInstrumentInfoMsgScopedPtr m_msg; // to avoid repeated creations
};

BB_FWD_DECLARE_SHARED_PTR( TdPositionSender );

} // namespace tdcore
} // namespace bb

#endif // BB_SERVERS_TDCORE_TDPOSITIONSENDER_H

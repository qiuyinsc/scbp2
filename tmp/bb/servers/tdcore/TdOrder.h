#ifndef BB_SERVERS_TDCORE_TDORDER_H
#define BB_SERVERS_TDCORE_TDORDER_H

/* Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved. */


#include <boost/noncopyable.hpp>
#include <boost/utility/base_from_member.hpp>
#include <boost/function.hpp>
#include <boost/bind.hpp>

#include <bb/core/messages.h>
#include <bb/core/oflag.h>
#include <bb/servers/tdcore/TdTimingInfo.h>

namespace bb {
namespace tdcore {

// Contains the reason for allowing/blocking an order to go through:
// - R_MARGIN_REJECT  : risk control specifically rejected the order or threw an exception
// - R_TS_REJECT      : the basic checks within the order manager rejected the order, or the order manager thew an exception
// - R_NONE           : all checks passed and the order was sent
// It also returns the timings before and after TdRiskControl performed its actions and any future timings we might add.
struct TdHandleOrderResult
{
    TdHandleOrderResult ( const oreason_t rsn,
        const bb::tdcore::TdTimingInfo & info ) :
        reason ( rsn ),
        timing_info ( info ) {}

    const oreason_t reason;
    const bb::tdcore::TdTimingInfo timing_info;
};

class ITdOrderRouter;
class TdOrderCancelState;
class TdOrderState;
BB_FWD_DECLARE_SHARED_PTR( TdClient );

/// Application-specific data which may be attached to an order
struct OrderExtraData
{
    virtual ~OrderExtraData() = 0;
};

/// Base class for all orders represented within a trade daemon
class TdOrder
    : private boost::noncopyable
{
public:
    enum OrderValidateState
    {
        VALIDATE_STATE_SUCCESS = 0,
        VALIDATE_STATE_FAILURE = 1,
        VALIDATE_STATE_PENDING = 2
    };
    typedef boost::function<void( TdOrder* )> DeallocFunction;

    TdOrder( TdClient*, const acct_t&, const instrument_t&, uint32_t _orderid, int32_t _left, const DeallocFunction& deallocFunc );
    virtual ~TdOrder() { delete m_extraData; }

    virtual const symbol_t& getSymbol() const = 0;
    virtual double getLimitPrice()      const = 0;
    virtual acct_t getAccount()         const = 0;
    virtual int getSize()               const = 0;
    virtual dir_t getDir()              const = 0;
    virtual mktdest_t getMktDest()      const = 0;
    virtual int getTimeout()            const = 0;
    virtual bool getVisible()           const = 0;
    virtual tif_t getTIF()              const = 0;

    bool isNoPreTradeAnonymity()const { return getFlags() & OFLAG_NO_PRE_TRADE_ANONYMITY; }
    bool isProactive()          const { return getFlags() & OFLAG_PROACTIVE; }
    bool isStabilize()          const { return getFlags() & OFLAG_STABILIZE; }
    bool isDarkPool()           const { return getFlags() & OFLAG_DARK_POOL; }
    bool isBatsDart()           const { return getFlags() & OFLAG_BATS_DART; }
    bool isStop()               const { return getFlags() & OFLAG_STOP; }
    bool isPostOnly()           const { return getFlags() & OFLAG_POST_ONLY; }
    bool isPegPrimary()         const { return getFlags() & OFLAG_PEG_PRIMARY; }
    bool isNasdaqMopp()         const { return getFlags() & OFLAG_NASDAQ_MOPP; }
    bool isPegMidpoint()        const { return getFlags() & OFLAG_PEG_MIDPOINT; }
    bool isRecycle()            const { return getFlags() & OFLAG_RECYCLE; }
    bool isISO()                const { return getFlags() & OFLAG_ISO; }
    bool isBatsParallel2D()     const { return getFlags() & OFLAG_BATS_PARALLEL_2D; }
    bool isBatsParallelT()      const { return getFlags() & OFLAG_BATS_PARALLEL_T; }
    bool isManual()             const { return getFlags() & OFLAG_MANUAL; }
    bool isShortLocated()       const { return getFlags() & OFLAG_SHORT_LOCATED; }
    bool isCancelOrigOnReject()  const { return getFlags() & OFLAG_CANCEL_ORIG_ON_REJECT; }

/* This worked well on precise but the compiler that ships with trusty gets all confused
   and calls this delete() when we call delete m_extraData above.
    void operator delete ( void* p )
    {
        if(!((TdOrder*)p)->deallocator.empty())
        {
            ((TdOrder*)p)->deallocator((TdOrder*)p);
        }
        else
        {
            ::operator delete(p);
        }
    }
*/

    /// The isModifiable condition for an order depends on each exchange
    /// So this would enable us to over-ride the isModifiable function in the ITdOrderRouter
    /// to perform exchange specific checks for whether an order is modifiable.
    bool isModifiable( const ITdOrderRouter& orderRouter ) const;

    /// To make sure that a order is modifiable, it should either consist a fixid
    /// or exch_ref. If both are not present then that means that we cannot modify
    /// the message.
    /// Note: This is not true for BATS Binary Protocol. But for uniformity we are going
    /// to follow the same rule for all Fix and Binary protocols.
    /// If in the future we do need special behaviour we can over-ride this function.

    virtual void dispatchSendNewOrder( ITdOrderRouter& orderRouter ) const = 0;

    virtual oflag_t getFlags() const = 0;

    bool isDone() const;

    void setSeq( uint32_t seq ) { tradeid.orderid = seq; }
    void setTdTimingInfo( TdTimingInfo* info) { m_tdTimingInfo = info; }

    TdClient*                 getSender()      const { return m_client; }
    const instrument_t&       getInstrument()  const { return m_instr; }
    const tid_t&              getTradeId()     const { return tradeid; }
    uint32_t                  getOrderId()     const { return orderid; }
    const TdOrderState*       getOrderState()  const { return orderState; }
    const TdOrderCancelState* getCancelState() const { return cancelState; }
    TdTimingInfo*             getTdTimingInfo() const { return m_tdTimingInfo; }

    template<typename T> T* getOrderExtraDataT() const { return static_cast<T*>( m_extraData ); }

    // convert fixid for use as exch_ref in BB messages
    static void decodeFixId( uint64_t& decoded, const std::string& fixid, int base );
    static void decodeFixIdForMkt( uint64_t& decoded, const std::string& fixid, mktdest_t mkt );

    // default Deallocator
    static void defaultDeallocator( TdOrder* ptr);

    uint64_t getExchRef() const;

    bool cancel; // whether or not order has been canceled
    int32_t left;
    uint32_t clientid; // unique identifier per client connection
    uint32_t orderid; // client's order id (client-assigned)
    uint32_t seqnum; // exchange sequence number
    tid_t tradeid; // our trade id (td-assigned)
    uint32_t cancelid; // if order is cancelled, this is the ID for the cancellation (otherwise 0)

    //TODO - fixid and via_mkt are exactly the kind of things that we should put in an 'OrderExtraData' struct.
    std::string fixid; // FIXOrderID (comes back in each ExecutionReport)
    mutable mktdest_t via_mkt; // differs from getMktDest for directed routing; changes interpretation of fixid & liquidity
    mutable uint64_t exch_ref; // BB-normalized exchange reference number for the order

    ostatus_t status;

    timeval_t orderTimings[STAT_NUM];
    timeval_t cancelTimings[CXLSTATUS_NUM];

    const DeallocFunction&   deallocator;

protected:
    instrument_t m_instr;
    mutable OrderExtraData *m_extraData;
    TdClient*      m_client;
    TdTimingInfo*  m_tdTimingInfo;

protected:
    friend class TdOrderState;
    friend class TdOrderCancelState;
    friend class TdOrderBook;
    const TdOrderCancelState* cancelState;
    const TdOrderState* orderState;

    /// @remark takes ownership of the extradata memory
    void setOrderExtraData( OrderExtraData *xd ) const
    {
        BB_THROW_EXASSERT_SSX ( m_extraData == nullptr || xd == nullptr, "Setting ExtraData twice!" );
        m_extraData = xd;
    }
};
BB_DECLARE_SHARED_PTR( TdOrder );

/// A TdOrder based on an old-style msg_td_order_base
class TdOrderBase
    : public TdOrder
{
public:
    TdOrderBase( TdClient*, msg_td_order_base*, const instrument_t&, uint32_t _orderid, int32_t _left, const DeallocFunction& deallocFunc );

    virtual const symbol_t& getSymbol() const { return m_baseOrder->symbol; }
    virtual double getLimitPrice()      const { return m_baseOrder->px; }
    virtual acct_t getAccount()         const { return m_baseOrder->account; }
    virtual int getSize()               const { return m_baseOrder->size; }
    virtual dir_t getDir()              const { return m_baseOrder->dir; }
    virtual mktdest_t getMktDest()      const { return m_baseOrder->dest; }
    virtual tif_t getTIF()              const { return m_baseOrder->tif; }
    virtual int getTimeout()            const { return m_baseOrder->timeout; }
    virtual bool getVisible()           const { return m_baseOrder->visible; }

    virtual oflag_t getFlags() const { return static_cast<oflag_t>( m_baseOrder->flags ); }

protected:
    msg_td_order_base* m_baseOrder;
};

class StockOrder
    : private boost::base_from_member<msg_td_order>
    , public TdOrderBase
{
public:
    StockOrder( TdClient* sender, const TdOrderMsg& _order, const DeallocFunction& deallocFunc = boost::bind( &TdOrder::defaultDeallocator, _1 ) );

    void dispatchSendNewOrder( ITdOrderRouter& orderRouter ) const;
protected:
    typedef boost::base_from_member<msg_td_order> msg_type;
    msg_td_order *getMsg() { return &this->msg_type::member; }
};

class FutureOrder
    : private boost::base_from_member<msg_td_order_future>
    , public TdOrderBase
{
public:
    FutureOrder( TdClient* sender, const TdOrderFutureMsg& order, const DeallocFunction& deallocFunc = boost::bind( &TdOrder::defaultDeallocator, _1 ) );

    virtual oflag_t getFlags() const { return static_cast<oflag_t>( getMsg()->oflags ); }

    void dispatchSendNewOrder( ITdOrderRouter& orderRouter ) const;
protected:
    typedef boost::base_from_member<msg_td_order_future> msg_type;
    msg_td_order_future *getMsg() { return &this->msg_type::member; }
    const msg_td_order_future *getMsg() const { return &this->msg_type::member; }
};

/// A TdOrder based on a new-style msg_td_order_new (supports all product types)
class Order : public TdOrder
{
public:
    Order( TdClient* sender, const TdOrderNewMsg& _order, const DeallocFunction& deallocFunc = boost::bind( &TdOrder::defaultDeallocator, _1 ) );

    void dispatchSendNewOrder( ITdOrderRouter& orderRouter ) const;

    const symbol_t& getSymbol() const { return getMsg()->instr.sym; }
    double getLimitPrice()      const { return getMsg()->px; }
    acct_t getAccount()         const { return getMsg()->account; }
    int getSize()               const { return getMsg()->size; }
    dir_t getDir()              const { return getMsg()->dir; }
    mktdest_t getMktDest()      const { return getMsg()->dest; }
    tif_t getTIF()              const { return getMsg()->tif; }
    int getTimeout()            const
    {
        // need to be able to handle special case of 0 timeout
        if (getMsg()->timeout == timeval_t::earliest)
            return 0;
        return (getMsg()->timeout - timeval_t::now).sec();
    }
    bool getVisible()           const { return !(getMsg()->oflags & OFLAG_INVISIBLE); }

    virtual oflag_t getFlags() const { return static_cast<oflag_t>( getMsg()->oflags ); }

protected:
    msg_td_order_new m_msg;
    msg_td_order_new *getMsg() { return &m_msg; }
    const msg_td_order_new *getMsg() const { return &m_msg; }
};

/// A TdOrder based on a new-style msg_td_order_new (supports all product types)
class ModifyOrder : public TdOrder
{
public:
    ModifyOrder( TdClient* sender, const TdOrderModifyMsg& _modifyMsg
           , const DeallocFunction& deallocFunc = boost::bind( &TdOrder::defaultDeallocator, _1 ) );

    void dispatchSendNewOrder( ITdOrderRouter& orderRouter ) const
    {
        BB_THROW_ERROR( "dispatchSendNewOrder not supported. Maybe call dispatchModifyOrder for this order type" );
    }

    void modifyOrder( ITdOrderRouter& orderRouter ) const;

    /// Since the fixid does not change for the lifetime of a order
    /// When we are sending a modify order for a previous order
    /// we use the fixid for the original order only.
    /// Usually the fixid is set for an order when we execute
    /// the OrderConfirm action on the order, as we get it form the exchange.
    /// But since we are modifying an order that is already accepted by
    /// the exchange, we have a fixid, which does not change for the lifetime
    /// of an order. So this function is there to provide an unambiguos
    /// way to get the fixid for modify orders
    const std::string getOrigFixId()  const { return fixid; }
    const tid_t& getOrigTradeId() const { return origTradeid; }
    uint32_t getOrigOrderId()   const { return getMsg()->orig_orderid; }
    const symbol_t& getSymbol() const { return getMsg()->instr.sym; }
    double getLimitPrice()      const { return getMsg()->px; }
    acct_t getAccount()         const { return getMsg()->account; }
    int getSize()               const { return getMsg()->size; }
    dir_t getDir()              const { return getMsg()->dir; }
    mktdest_t getMktDest()      const { return getMsg()->dest; }
    tif_t getTIF()              const { return getMsg()->tif; }
    int getTimeout()            const
    {
        // need to be able to handle special case of 0 timeout
        if (getMsg()->timeout == timeval_t::earliest)
            return 0;
        return (getMsg()->timeout - timeval_t::now).sec();
    }
    bool getVisible()           const { return !(getMsg()->oflags & OFLAG_INVISIBLE); }

    OrderValidateState getValidateState()          const { return m_validateState; }
    void setValidateState ( const OrderValidateState state = VALIDATE_STATE_SUCCESS ) { m_validateState = state; }

    virtual oflag_t getFlags() const { return static_cast<oflag_t>( getMsg()->oflags ); }

    bool getInFlightMitigation() const { return m_binFlightMitigation; }

    tid_t origTradeid;

protected:
    msg_td_order_modify m_msg;
    msg_td_order_modify *getMsg() { return &m_msg; }
    const msg_td_order_modify *getMsg() const { return &m_msg; }

private:
    bool m_binFlightMitigation;
    OrderValidateState m_validateState;
};

struct ReserveOrder : public Order
{
    ReserveOrder( TdClient* sender, const TdOrderReserveMsg&  order, const DeallocFunction& deallocFunc = boost::bind( &TdOrder::defaultDeallocator, _1 ) );

    // Temporary solution. TODO: reconcile with how the base class does this
    virtual int getTimeout() const
    {
        if (getMsg()->timeout == timeval_t::earliest)
            return 0;

        return getMsg()->timeout.sec();
    }


    uint32_t getMaxFloor() const { return m_maxFloor; }

    void dispatchSendNewOrder( ITdOrderRouter& orderRouter ) const;

private:
    const uint32_t m_maxFloor;
};


} // namespace tdcore
} // namespace bb

#endif // BB_SERVERS_TDCORE_TDORDER_H

#ifndef BB_SERVERS_TDCORE_TDRISKCONTROLOPTIONS_H
#define BB_SERVERS_TDCORE_TDRISKCONTROLOPTIONS_H

/* Contents Copyright 2015 Athena Capital Research LLC. All Rights Reserved. */

#include <set>

#include <boost/function.hpp>
#include <boost/optional.hpp>
#include <boost/thread/mutex.hpp>
#include <boost/thread/thread.hpp>
#include <boost/variant.hpp>

#include <bb/servers/tdcore/TdRiskControl.h>
#include <bb/servers/tdcore/ITdRiskControl.h>
#include <bb/servers/tdcore/TdConfig.h>

namespace bb {

namespace tdcore {

class AccountDataOptions
{
public:
    AccountDataOptions() { clear(); }

    void clear()
    {
        m_delta_position[ BID ] = m_delta_position[ ASK ] = 0;
        m_delta_exposure[ BID ] = m_delta_exposure[ ASK ] = 0;
        m_gamma_position[ BID ] = m_gamma_position[ ASK ] = 0;
        m_vega_position[ BID ] = m_vega_position[ ASK ] = 0;
        m_gamma_exposure[ BID ] = m_gamma_exposure[ ASK ] = 0;
        m_vega_exposure[ BID ] = m_vega_exposure[ ASK ] = 0;
    }

    // Important note: When deciding which side to update, the decision will be from a risk management point of view, ie:
    // For delta      : BID <-- buy call or sell put, ASK <-- sell call or buy put
    // For gamma, vega: BID <-- buy call or buy put,  ASK <-- sell call or sell put

    double m_delta_position[2]; // delta dollar value (+/-) currently on each side
    double m_gamma_position[2]; // gamma dollar value (+/-) currently on each side
    double m_vega_position[2];  // vega dollar value (+/-) currently on each side
    double m_delta_exposure[2]; // delta dollar value (+/-) if all the side's orders were filled
    double m_gamma_exposure[2]; // gamma dollar value (+/-) if all the side's orders were filled
    double m_vega_exposure[2];  // vega dollar value (+/-) if all the side's orders were filled
};

struct TdRiskControlOptionsConfig : public bb::LuaConfig<TdRiskControlOptionsConfig>
{
    int m_sessionId;

    typedef std::map<symbol_t, symbol_t> opt_underlying_map_t;
    opt_underlying_map_t m_opt_underlier;
    opt_underlying_map_t m_weekly_opt_underlier;

    static void describe()
    {
        Self::create()
        .param( "opt_session_id"              , &Self::m_sessionId )
        .param( "opt_underlying_map"          , &Self::m_opt_underlier )
        .param( "weekly_opt_underlying_map"   , &Self::m_weekly_opt_underlier )
        ;
    }
};

class TdRiskControlOptions
    : public TdRiskControl
{
public:
    // map between option and underlyer
    typedef std::map<symbol_t, symbol_t> opt_underlying_map_t;

    // a function which can be called to get the delta of a instrument
    typedef boost::function<double ( const instrument_t& )> delta_callback_t;

    // limits are held by reference so they can be changed on the fly
    // cancel_callback is used to cancel certain groups of orders if limits are exceeded
    TdRiskControlOptions( const TdRiskLimits& limits, const cancel_callback_t& cancel_callback,
                   PriceInitMode priceInitMode = kInitPricesFromDbUSD, const timeval_t& db_prices_todays_date = timeval_t::now,
                   const std::string& db_profile = "production", const std::string& opt_underlying_map_definitions = "/with/bb/conf/td/opt_underlying_map.lua" );

    using TdRiskControl::approve;
    using TdRiskControl::setSymbolPriceIfUnset;

    void setDeltaCallback( const delta_callback_t& cb ) { m_delta_callback = cb; }

    bool approve( const acct_t& acct, uint32_t orderid, dir_t dir, const instrument_t& instr, double price, int32_t size, tif_t tif );

    SymbolData& setSymbolPriceIfUnset( const instrument_t& instr, double price );
    void setSymbolPrice( const instrument_t& instr, double price ); // set price and adjust account values
    void setSymbolSize( const instrument_t& instr, int32_t dir_size ); // set position size and adjust account values

    void clear( bool keep_baselines = false ); // to be used when loading positions (does not clear prices)

    void log_option_risk( const int lineNumber = 0 ) const;

    void populateOptionGreeksAccountInfo( OptionGreeksAccountInfoMsg& outMsg) const;
    void populateOptionGreeksLimits( OptionGreeksLimitsMsg& outMsg ) const;

private:

    void update_exposure( dir_t dir, const instrument_t& instr, int32_t size );
    int32_t update_position( dir_t dir, const instrument_t& instr, double price, int32_t size ); // return shares of the instrument

    bool init_prices( const timeval_t& todaysDate, uint32_t sessionId );

    symbol_data_optional_t init_option_price( const instrument_t& instr, double price,
            const  ICommoditiesSpecificationsMapCPtr& spec, bool applyCSizeAndCurr, bool tryIntrisic = false );

    delta_callback_t m_delta_callback;

    TdRiskControlOptionsConfig m_options_config;

    AccountDataOptions m_account_data_options;

    const date_t m_db_run_date;

    friend class ::TdRiskControlTest; // let the tests peer into our soul
};
BB_DECLARE_SHARED_PTR( TdRiskControlOptions );

} // namespace tdcore
} // namespace bb

#endif // BB_SERVERS_TDCORE_TDRISKCONTROLOPTIONS_H

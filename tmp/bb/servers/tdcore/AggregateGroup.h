#ifndef BB_SERVERS_TDCORE_AGGREGATEGROUP_H
#define BB_SERVERS_TDCORE_AGGREGATEGROUP_H

/* Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved. */
#include <boost/optional.hpp>

#include <bb/core/instrument.h>

namespace bb {
namespace tdcore {
namespace detail {

class AggregateGroup
{
public:
    AggregateGroup( symbol_t sym = SYM_NONE );
    const instrument_t &getInstrumentRepresentation() const;

    void setProduct( product_t prod );

    size_t hashValue() const
    {
        std::size_t seed = 0;
        boost::hash_combine( seed, m_sym.id() );
        if( m_prod ) boost::hash_combine( seed, *( m_prod ) );

        return seed;
    }

    operator const instrument_t&() const
    {
        return getInstrumentRepresentation();
    }

    bool operator==( const AggregateGroup& agg ) const
    {
        return m_sym == agg.m_sym && m_prod == agg.m_prod;
    }

private:
    symbol_t m_sym;
    boost::optional<product_t> m_prod;
    instrument_t m_instrRepresentation;
};

}
}
}

BB_HASH_NAMESPACE_BEGIN
{
    template<> struct hash<bb::tdcore::detail::AggregateGroup>
    {
        size_t operator()( const bb::tdcore::detail::AggregateGroup& agg ) const
        {
            return agg.hashValue();
        }
    };
}
BB_HASH_NAMESPACE_END

#endif // BB_SERVERS_TDCORE_AGGREGATEGROUP_H

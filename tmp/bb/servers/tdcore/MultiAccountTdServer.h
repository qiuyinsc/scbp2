#ifndef BB_SERVERS_TDCORE_MULTIACCOUNTTDSERVER_H
#define BB_SERVERS_TDCORE_MULTIACCOUNTTDSERVER_H

/* Contents Copyright 2015 Athena Capital Research LLC. All Rights Reserved. */


#include <bb/core/side.h>
#include <bb/core/symbol.h>

#include <bb/servers/tdcore/TdServer.h>
#include <bb/servers/tdcore/TdConfig.h>
#include <bb/servers/tdcore/MultiAccountRiskControl.h>
#include <bb/servers/tdcore/MultiAccountTdDispatcher.h>

namespace bb {
namespace tdcore {

class TdOrderManager;

/*
 * This class inherits from both a TdServer and a MultiAccountTdDispatcher.
 * It implements the base TdServer functionality that is common to all TDs.
 * This includes halt/resume/cancel/reloadRiskLimits/etc..
 * The most basic difference between this class and the TdServerImpl is that
 * for lua commands/functions the active account must be obtained from the
 * MultiAccountTdDispatcher so the object know whic account to apply the function
 * to.
 */
class MultiAccountTdServer
    : public TdServer
    , public MultiAccountTdDispatcher
{
public:
    virtual void halt();
    virtual void resume();
    virtual void halt( const instrument_t& );
    virtual void resume( const instrument_t& );
    virtual bool isHalted() const;

    virtual void printAllowedUsers() const;
    virtual void reloadAllowedUsers();

    virtual void forceDoneOrder( unsigned int tid );
    virtual void forceDoneOrders( side_t, symbol_t );
    virtual void forceDoneOrders( side_t, instrument_t );
    virtual void forceDoneOrdersForClient( uint32_t );
    virtual void forceDoneOrdersForClientIdent( const std::string& );

    virtual void printRiskLimits() const;
    virtual void reloadRiskLimits();
    virtual void reloadRiskLimits(const std::string& filename );
    virtual void recheckRiskLimits() const ;
    virtual void reapplyRiskLimits() const;

    virtual void setAccountLimits( int ordersLimits );
    virtual void setAccountInstrumentLimits( const bb::instrument_t& instr, int maxOrderQty, double maxPosition, double maxExposure );
    virtual void savePositions( const std::string& filename ) const;

    const acct_t getActiveAccount() const;

protected:
    MultiAccountTdServer( const std::string& configFile, MultiAccountTdConfig& config );

    virtual ~MultiAccountTdServer() {};
    virtual MultiAccountTdConfig loadConfig( const std::string& configFile ) = 0;

    virtual TdOrderManager& getOrderManager() = 0;
    virtual const TdOrderManager& getOrderManager() const = 0;

    virtual void cancelOrders( side_t, symbol_t );
    virtual void cancelOrders( side_t, const instrument_t & );
    virtual void cancelOrdersForClient( uint32_t );

    using MultiAccountTdDispatcher::cancelAll;
    virtual void cancelAll( acct_t account, side_t side, instrument_t instr );

    MultiAccountRiskControl& getRiskControl();


private:
    virtual void setNextOrderSeqNum( unsigned int order_seq_num );

    virtual void printStatus() const;

    void assertClientIdAllowed( uint32_t );

    const std::string&        m_configFile;

    MultiAccountRiskControl   m_riskControl;

};

} // namespace tdcore
} // namespace bb

#endif // BB_SERVERS_TDCORE_MULTIACCOUNTTDSERVER_H

#ifndef BB_SERVERS_TDCORE_TDPOSITIONSAVER_H
#define BB_SERVERS_TDCORE_TDPOSITIONSAVER_H

/* Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved. */


#include <iosfwd>

#include <boost/noncopyable.hpp>

#include <bb/core/instrument.h>


namespace bb {
namespace tdcore {

class SymbolData;
class ITdPositionStore;

class TdPositionSaver
    : boost::noncopyable
{
public:
    enum EInitAction { INIT_NONE, INIT_LOAD, INIT_LOAD_NO_UNLINK, INIT_LOAD_READONLY };
    enum EFileType   { NON_DATED, DATED };

    ///
    /// INIT_LOAD will initialize the risk control with the positions specified in <filename>
    /// and rename the file to <filename>.bak
    /// INIT_LOAD_NO_UNLINK does the same as INIT_LOAD, but leaves <filename> in place.
    /// INIT_LOAD_READONLY  only loads, but does not save
    /// INIT_NONE will not load the file. but only saves
    /// default is INIT_LOAD
    ///
    TdPositionSaver( ITdPositionStore& riskControl, const char* filename, EInitAction action = INIT_LOAD );
    ~TdPositionSaver();

    void save( const char* filename = NULL, EFileType dated = NON_DATED ) const;
private:
    static void load( std::istream& in, ITdPositionStore& riskControl );
    static void saveSymbol( std::ostream& out, const instrument_t& instr, const SymbolData& symbol_data );

    ITdPositionStore& m_riskControl;
    std::string m_filename;
    bool m_doNotSave;
};
BB_DECLARE_SHARED_PTR( TdPositionSaver );

} // namespace tdcore
} // namespace bb

#endif // BB_SERVERS_TDCORE_TDPOSITIONSAVER_H

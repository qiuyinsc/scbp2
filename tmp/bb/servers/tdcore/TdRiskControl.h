#ifndef BB_SERVERS_TDCORE_TDRISKCONTROL_H
#define BB_SERVERS_TDCORE_TDRISKCONTROL_H

/* Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved. */

#include <set>

#include <boost/function.hpp>
#include <boost/optional.hpp>

#include <boost/thread/mutex.hpp>
#include <boost/thread/thread.hpp>
#include <boost/variant.hpp>

#include <bb/core/hash_map.h>
#include <bb/core/instrument.h>
#include <bb/core/tif.h>
#include <bb/core/tid.h>
#include <bb/core/Primer.h>

#include <bb/servers/tdcore/ITdPositionStore.h>
#include <bb/servers/tdcore/ITdRiskControl.h>
#include <bb/servers/tdcore/TdOrder.h>

#include <bb/servers/tdcore/AggregateGroup.h>

#include <bb/db/DayInfo.h>
#include <bb/db/CurrencyInfo.h>
#include <bb/db/CommoditiesSpecifications.h>

class TdRiskControlTest;

namespace bb {

BB_FWD_DECLARE_SHARED_PTR( ICommoditiesSpecificationsMap );
BB_FWD_DECLARE_SHARED_PTR( Environment );

class tid_t;
class TdOrderBaseMsg;

namespace tdcore {

class TdRiskLimits;
struct InstrumentLimits;
struct OrderInfo;

struct AggregatePosition
{
    enum Active {
        kInactive=0,
        kActive=1
    };
    AggregatePosition()
        : actual_size( 0 )
    {
        potential_size[BID] = potential_size[ASK] = 0;
    };
    typedef bbext::hash_map<instrument_t, Active,
                            instrument_t::hash_no_mkt_no_currency,
                            instrument_t::equals_no_mkt_no_currency> InstrMap;
    typedef std::pair<instrument_t, Active >  InstrPair;
    typedef InstrMap::iterator                InstrMapIter;

    int32_t potential_size[2];
    int32_t actual_size;

    InstrMap m_constituents[2];
};


class TdRiskControl
    : public ITdRiskControl
    , public ITdRiskControlExt
    , public ITdPositionStore
    , public bb::Primer::IInstrumentPrimeable
{
public:
    // a function which can be called to cancel all orders on a side, for a given symbol (or SYM_ALL)
    typedef boost::function<void ( side_t, const instrument_t& )> cancel_callback_t;

    typedef bbext::hash_map<instrument_t, SymbolData,
                            instrument_t::hash_no_mkt_no_currency,
                            instrument_t::equals_no_mkt_no_currency> symbol_data_map_t;

    enum PriceInitMode {
        kManuallyInitPrices,          // do not load initial prices from db
        kInitPricesFromDbUSD,         // load initial prices from db and convert futures to USD
        kInitPricesFromDbNoCurrency   // load initial prices from db using original currency
    };

    enum TdRiskControlInit {
        kNonDerived,                  // TdRiskControl is the class used by the TD. No child class, so all the init and validation can be done there.
        kDerived                      // The TD is instantiating a derived class of TdRiskControl ( e.g. TdRiskControlOptions ). Validation is done by the child class.
    };

    // limits are held by reference so they can be changed on the fly
    // cancel_callback is used to cancel certain groups of orders if limits are exceeded
    TdRiskControl( const TdRiskLimits& limits, const cancel_callback_t& cancel_callback,
                   PriceInitMode priceInitMode = kInitPricesFromDbUSD, const timeval_t& db_prices_todays_date = timeval_t::now,
                   const std::string& db_profile = "production", const TdRiskControlInit = kNonDerived );

    // public methods are to be called only from the thread which constructed this (see mutex notes below)

    void halt();
    void halt( const instrument_t& instr );
    void resume();
    void resume( const instrument_t& instr );

    virtual bool approve( const acct_t& acct, uint32_t orderid, dir_t dir, const instrument_t& instr, double price, int32_t size, tif_t tif );
    bool approve( const TdOrderMsg& order ); // true if order is approved
    bool approve( const TdOrderFutureMsg& order ); // true if order is approved
    bool approve( const TdOrderNewMsg& order ); // true if order is approved
    bool approve( const TdOrderModifyMsg& order ); // true if order is approved
    void open( uint32_t orderid, const tid_t& tradeid, dir_t dir, const instrument_t& instr, int32_t size );
    void open( const TdOrder& order ); // submit order (must have been approved previously)
    void close( uint32_t orderid, const tid_t& tradeid, dir_t dir, const instrument_t& instr, int32_t left );
    void close( const TdOrder& order ); // end order (filled or not)

    // Spread Clearly Erroneous Price is used for calculating the pre-trade clearly-erroneous limit for spread
    double getSpreadClearlyErroneousPrice(const instrument_t& instrSprd, double clearly_erroneous_percentage = 0.0);

    // set the price converter for spread, based on its leg information
    void setPriceValueConvertersByUnderlier(const instrument_t& instrSprd, SymbolData& symDataToSet);

    // update position for an open order and return shares of the instrument
    int32_t fill( uint32_t orderid, const tid_t& tradeid, dir_t dir,
            const instrument_t& instr, double price, int32_t size );
    int32_t fill( dir_t dir, const instrument_t& instr, double price, int32_t size,
                  fillflag_t fillflag = FILLFLAG_NONE ); // for unknown executions
    int32_t fill( const TdOrder& order, double price, int32_t size ); // partial or complete fill received

    /// handles a fill that comes in after the order is closed, assuming that
    /// the order.left was not updated prior to the fill.  This practically adjusts
    /// the potential_shares[side] field correctly in addition to the regular
    /// fill code path.
    int32_t late_fill( const TdOrder& order, double price, int32_t size );

    /// handles a fill that does not correspond to an approved order
    /// this can be used for margin_create_fill messages, or for fill daemons processing out-of-band order flow
    int32_t fill_without_order( dir_t dir, const instrument_t& instr, double price, int32_t size );

    int32_t bust( uint32_t orderid, const tid_t& tradeid, dir_t dir,
            const instrument_t& instr, double price, int32_t size );
    int32_t bust( const TdOrder& order, double price, int32_t size ); // automated trade bust "undoes" the fill

    // to be used after changing risk limits
    void warnIfViolated()   const { invokeIfViolated( &TdRiskControl::warn   ); }
    void cancelIfViolated() const { invokeIfViolated( &TdRiskControl::cancel ); }

    virtual void clear( bool keep_baselines = false ); // to be used when loading positions (does not clear prices)

    SymbolData& setSymbolPriceIfUnset( const instrument_t& instr ); // use m_limits.default_price (supports stocks and options)
    virtual SymbolData& setSymbolPriceIfUnset( const instrument_t& instr, double price );
    virtual void setSymbolPrice( const instrument_t& instr, double price ); // set price and adjust account values
    void setSymbolNotionalValueFromPrice( const instrument_t& instr, double price ); // set notional value from price and adjust account values

    virtual void setSymbolSize( const instrument_t& instr, int32_t dir_size ); // set position size and adjust account values
    void setSymbolBaseline( const instrument_t& instr, int32_t baseline );

    const SymbolData& getSymbolData( const acct_t&, const instrument_t& instr ) const
        { BB_ASSERT( on_main_thread() ); return get_symbol_data( instr ); }
    const SymbolData& getSymbolData( const instrument_t& instr ) const
        { BB_ASSERT( on_main_thread() ); return get_symbol_data( instr ); }

    // these can be called from a separate thread, e.g. for position broadcasting
    void forEach( boost::function<void ( const instrument_t&, const SymbolData& )> callback ) const;
    void forInstrument( const instrument_t& instr, boost::function<void ( const SymbolData& )> callback ) const;
    AccountData getAccountData() const { return m_account_data; }
    const TdRiskLimits& getLimits() const { return m_limits; }
    bool isHalted() const { return m_halted; }
    symbol_data_map_t allSymbolData() const; // returns a copy of the symbol data (for swig)
    void setAccountDesc( const std::string& acct_descr ) { m_account_data.m_acct_descr = acct_descr; }

    typedef boost::function<void ( const instrument_t&, const InstrumentLimits& )> InstrumentLimitCallback;
    void forEachInstrumentLimit( const InstrumentLimitCallback& callback ) const;


    typedef boost::function<void ( const detail::AggregateGroup&, const AggregatePosition& )> AggregatePositionCallback;
    void forEachAggregatePosition( const AggregatePositionCallback& callback ) const;

    void populateMessage( MarginLimitsMsg& msg );
    void populateMessage( MarginAccountInfoMsg& msg );

    static double computeValueFromExchangePrice( double price, const bb::CommoditySpecification& spec, const bb::db::CurrencyInfo& currency_info, TdRiskControl::PriceInitMode priceInitMode );
    static double computeExchangePriceFromValue( double value, const bb::CommoditySpecification& spec, const bb::db::CurrencyInfo& currency_info, TdRiskControl::PriceInitMode priceInitMode );

protected:
    virtual bool prime(bb::instrument_t const & instr) const;

    uint32_t getAggregateGroups( const instrument_t& instr ) const;
    AggregatePosition& provideAggregatePosition( const detail::AggregateGroup& instr );

    typedef boost::optional<symbol_data_map_t::iterator> symbol_data_optional_t;

    bool init_prices( const timeval_t& todaysDate );

    void validate_available_prices() const;

    symbol_data_optional_t init_stock_price( symbol_t symbol, double price );
    symbol_data_optional_t init_future_price( const instrument_t& instr, double price, const  ICommoditiesSpecificationsMapCPtr& spec );
    symbol_data_optional_t init_instrument_price( const instrument_t& instr, double price );

    SymbolData& get_symbol_data( const instrument_t& instr );
    const SymbolData& get_symbol_data( const instrument_t& instr ) const;

    bool preTradeInstrumentCheck( const OrderInfo& order, const SymbolData& symbol_data );

    virtual void update_exposure( dir_t dir, const instrument_t& instr, int32_t size );
    virtual int32_t update_position( dir_t dir, const instrument_t& instr, double price, int32_t size ); // return shares of the instrument
    void update_aggregate_position( dir_t dir, const instrument_t& instr, double price, int32_t size );

    bool reject( const acct_t& acct, uint32_t orderid, dir_t dir, const instrument_t& instr, const char* reason,
                 boost::variant<double, tif_t, symbol_t, int32_t> value, boost::optional<double> limit = boost::none );
    bool warn  ( side_t side, const instrument_t& instr, const char* reason, double value, double limit ) const;
    bool cancel( side_t side, const instrument_t& instr, const char* reason, double value, double limit ) const;

    // re-checks orders & position against limits and invokes the given method (e.g. warn or cancel) if violated
    typedef bool ( TdRiskControl::* violation_action_t )( side_t side, const instrument_t& instr, const char* reason, double value, double limit ) const;
    void invokeIfViolated( violation_action_t action ) const;

    void debug( const TdOrder& order, const char* text ) const;
    void debug( uint32_t orderid, const tid_t& tradeid, dir_t dir, const instrument_t& instr, const char* text ) const;

    void verifyPerInstrumentLimits( const instrument_t& instr,
                                    const SymbolData& symData,
                                    side_t side,
                                    const InstrumentLimits& limit,
                                    violation_action_t callback ) const;

    void verifyAggregateLimits( violation_action_t callback ) const;

    bool isAllowedInstrument(const instrument_t& instr) const;

    // Function to estimate the trade price for each individual leg of a spread execution
    // This is used for post-trade "position" check, and "stop-loss" check
    std::pair<double, double> estimateSpreadLegTradePrice(const bb::instrument_t& frontLegInstr, const bb::instrument_t& rearLegInstr, double sprdTradePx) const;

    typedef bbext::hash_map<detail::AggregateGroup, AggregatePosition> aggregate_data_map_t;

    bb::EnvironmentPtr m_env;
    const TdRiskLimits& m_limits; // kept by reference so it can be modified by its owner
    volatile bool m_halted;
    cancel_callback_t m_cancel_callback;

    // Access to m_symbol_data needs synchronization, but only in a very specific way:
    // Every call into TdRiskControl is designed to be from a single thread,
    // except a few methods called out above, including forEach(), which is called from
    // a separate thread in TdPositionBroadcaster.
    // The lock must be taken when modifying the container in the main thread,
    // and when iterating over it in the position broadcasting thread.
    // Read access to the container (i.e. that which does not insert/erase, but may modify)
    // can be done without locking in the main thread, since that thread is the sole writer.
    //
    // If we had a no-invalidation guarantee for symbol_data_map_t::insert(), we could probably
    // avoid locking altogether, as we only insert and iterate.  Some hashtable implementations
    // support this, e.g. SGI and IBM, and it looked like TR1's might, but N1745 says it doesn't:
    // http://www.open-std.org/jtc1/sc22/wg21/docs/papers/2005/n1745.pdf
    // And unfortunately it seems that C++0x too may invalidate iterators on insert() in some cases:
    // http://groups.google.com/group/comp.lang.c++.moderated/browse_thread/thread/7e2b89944f270984/
    // Anyway, the locking doesn't seem so bad: the only possible contention is when a symbol's
    // price is set (ideally done at startup) and another thread is iterating over the symbol data.
    typedef boost::mutex mutex_t;
    mutable mutex_t m_mutex; // to protect m_symbol_data
    mutable mutex_t m_mutex_agg; // to protect m_agg_data

    symbol_data_map_t m_symbol_data;
    aggregate_data_map_t m_agg_data;
    AccountData m_account_data;
    uint32_t m_total_rejects;

    boost::thread::id m_thread_id; // ID of the thread which constructed this
    bool on_main_thread() const; // returns true if called from the thread which constructed this

    friend class ::TdRiskControlTest; // let the tests peer into our soul
    friend struct CurrentOrderRejecter;
    const std::string m_dbProfile;
    const timeval_t m_db_date;

    typedef int32_t time_second_t;
    typedef uint32_t order_counter_t;
    order_counter_t m_order_count; // potential open order count so far.
    time_second_t m_last_open_second; // the last time (in second) an order is successfully opened

    const PriceInitMode m_priceInitMode;
    TdRiskControlInit m_tdRiskControlInit;
    DayInfoFuturesMapPtr m_pFuturesInfo;
    ICommoditiesSpecificationsMapCPtr m_pSpecs;

    // This constant sets the maximum number
    static const uint32_t             s_maxAggregateGroupSize=2;

    mutable detail::AggregateGroup    m_aggregateInstruments[s_maxAggregateGroupSize];
};
BB_DECLARE_SHARED_PTR( TdRiskControl );

inline void TdRiskControl::close( const TdOrder& order )
    { return close( order.getOrderId(), order.getTradeId(), order.getDir(), order.getInstrument(), order.left ); }
inline int32_t TdRiskControl::fill( const TdOrder& order, double price, int32_t size )
    { return fill( order.getOrderId(), order.getTradeId(), order.getDir(), order.getInstrument(), price, size ); }
inline int32_t TdRiskControl::fill( dir_t dir, const instrument_t& instr, double price, int32_t size, fillflag_t fillflag )
    { return fill( 0, tid_t(), dir, instr, price, size ); }

inline int32_t TdRiskControl::late_fill( const TdOrder& order, double price, int32_t size )
{
    update_exposure( order.getDir(), order.getInstrument(), size );
    return fill( order.getOrderId(), order.getTradeId(), order.getDir(), order.getInstrument(), price, size );
}

inline int32_t TdRiskControl::bust( const TdOrder& order, double price, int32_t size )
    { return bust( order.getOrderId(), order.getTradeId(), order.getDir(), order.getInstrument(), price, size ); }


struct OrderInfo
{
    OrderInfo( const acct_t& p_acct,
               uint32_t p_orderid,
               dir_t p_dir,
               const instrument_t& p_instr,
               double p_price,
               int32_t p_size )
        : acct( p_acct)
        , orderid( p_orderid )
        , dir( p_dir )
        , instr( p_instr )
        , price( p_price )
        , size( p_size )
        , side( dir2side( dir ) )
        , dir_size( size * dir2sign( dir ) )
    {
    }

    const acct_t acct;
    const uint32_t orderid;
    const dir_t dir;
    const instrument_t& instr;
    const double price;
    const int32_t size;

    const side_t side;
    const int32_t dir_size;
};

struct CurrentOrderRejecter
{
    CurrentOrderRejecter( const OrderInfo& ord, TdRiskControl& rc ) : order( ord ), risk( rc )
    {
    }

    bool operator() ( const char* reason, double value, double limit )
    {
        return risk.reject( order.acct, order.orderid, order.dir, order.instr, reason, value, limit );
    }

    const OrderInfo& order;
    TdRiskControl& risk;
};

//The Macro define to add more descriptive information for RiskLimit reject message
//Example:
//    doCheck(CHECKINFODESCRIPTIVE("total notional limit breached reject: ", size * price, notional_limit))
//will actually call function as:
//    doCheck("total notional limit breached reject: (size * price, notional_limit)", size * price, notional_limit)
//As it is a MACRO, it won't take any extra running time compared to our previous code
#define CHECKINFODESCRIPTIVE(A, B, C) A " ( " #B ", " #C " )", B, C

template <class Func = boost::function<bool ( const char*, double, double )> >
struct LimitCheckHelper
{
    typedef Func FailCallback;
    LimitCheckHelper( const FailCallback& callback )
        : m_callback( callback ), m_result( true ), m_shortcircuit( false )
    {

    }

    template <typename LimitType>
    void doCheck( const char *description, LimitType value, LimitType limit )
    {
        if( !m_shortcircuit && value > limit )
        {
            m_shortcircuit = true;
            m_result = m_callback( description, value, limit );
        }
    }

    bool getResult() const { return m_result; }

    void setFailCallback( const FailCallback& fc )
    {
        m_callback = fc;
    }

private:
    FailCallback m_callback;
    bool m_result;
    bool m_shortcircuit;

};

} // namespace tdcore
} // namespace bb

#endif // BB_SERVERS_TDCORE_TDRISKCONTROL_H

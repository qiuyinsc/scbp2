#ifndef BB_SERVERS_TDCORE_TDPOSITIONBROADCASTERCORE_H
#define BB_SERVERS_TDCORE_TDPOSITIONBROADCASTERCORE_H

/* Contents Copyright 2015 Athena Capital Research LLC. All Rights Reserved. */

#include <bitset>

#include <boost/noncopyable.hpp>
#include <boost/thread.hpp>
#include <boost/random.hpp>

#include <bb/core/acct.h>
#include <bb/core/instrument.h>
#include <bb/core/symbol.h>
#include <bb/core/Error.h>
#include <bb/core/messages.h>
#include <bb/servers/tdcore/TdRiskControl.h>
#include <bb/servers/tdcore/TdRiskControlOptions.h>
#include <bb/io/SendTransport.h>

namespace bb {

namespace tdcore {

class SymbolData;
class TdRiskControl;
struct InstrumentLimits;
struct AggregatePosition;

struct AccountInfo
{
    acct_t m_account;

    MarginInstrumentInfoMsg m_instrInfoMsg;
    MarginLimitsMsg m_limitsMsg;
    MarginInstrLimitsMsg m_instrLimitsMsg;
    MarginAccountInfoMsg m_accountInfoMsg;
    MarginAggregatePosMsg m_aggMsg;

    MarginInstrumentInfoMsg m_instrInfoMsgInstant;
    MarginLimitsMsg m_limitsMsgInstant;
    MarginInstrLimitsMsg m_instrLimitsMsgInstant;
    MarginAccountInfoMsg m_accountMsgInstant;

    AccountInfo( const acct_t& );
};
BB_DECLARE_SHARED_PTR(AccountInfo);

struct AccountInfoOption
{
    const acct_t m_account;

    OptionGreeksAccountInfoMsg m_optionAccountMsg;
    OptionGreeksLimitsMsg m_optionLimitsMsg;

    OptionGreeksLimitsMsg m_optionLimitsMsgInstant;
    OptionGreeksAccountInfoMsg m_optionAccountMsgInstant;

    AccountInfoOption( const acct_t& );
};
BB_DECLARE_SHARED_PTR(AccountInfoOption);

class ITdPositionBroadcaster
{
    public:
        virtual ~ITdPositionBroadcaster() {}
        virtual void broadcastRiskLimits( const acct_t& acct = ACCT_UNKNOWN  ) = 0;
        virtual void broadcastAccountInfo( const acct_t& acct = ACCT_UNKNOWN ) = 0;
        virtual void setupPositionBroadcast( const instrument_t& instr,
                                 bool enable = true,
                                 bool immediate = false,
                                 acct_t acct = ACCT_UNKNOWN
        ) = 0;
};

/// Broadcasts position updates periodically, using TdRiskControl to provide the position data
/// This class is different from TdPositionBroadcaster because it is a singleton and supports multi account
class TdPositionBroadcasterCore
    : private boost::noncopyable
    , public ITdPositionBroadcaster
{
public:

    TdPositionBroadcasterCore();
    virtual ~TdPositionBroadcasterCore();

    // to be called from the same thread which constructed this
    void setupPositionBroadcast( const instrument_t& instr,
                                 bool enable = true,
                                 bool immediate = false,
                                 acct_t acct = ACCT_UNKNOWN
        );
    void broadcastRiskLimits( const acct_t& acct = ACCT_UNKNOWN  );
    void broadcastAccountInfo( const acct_t& acct = ACCT_UNKNOWN );

    void addAccount( const TdRiskControl* riskControl, const acct_t& account );
    void addAccount( const TdRiskControlOptions* optionRiskControl, const acct_t& account );

    void setPlanckSleep( bool isEnabled ) { m_planckSleepActive = isEnabled; }

    void activateBroadcast();
    void deactivateBroadcast();

protected:

    unsigned BROADCAST_CYCLE;
    unsigned BROADCAST_CHANGES_INTERVAL_SECS;
    unsigned BROADCAST_MAX_OFFSET_SECS;
    unsigned BROADCAST_PLANK_SLEEP_MS;

    typedef std::bitset<SYM_MAX> SymbolSet;
    typedef boost::variate_generator< boost::mt19937, boost::random::uniform_int_distribution<> > RandGen;

    virtual void broadcastPositions() const = 0; // m_broadcastThread entry function

    void broadcastCycle( int ii, bool is_cycle_end ) const;

    void setupPositionBroadcastIfNonZero( const instrument_t& instr, const SymbolData& symbol_data );

    void broadcastPositionIfSet( const AccountInfoPtr& account, const SymbolSet& set, int mod, const instrument_t& instr, const SymbolData& symbol_data ) const;
    void broadcastAggregatePositionIfSet( const AccountInfoPtr& account, const SymbolSet& set, int mod, const instrument_t& instr, const AggregatePosition& symbol_data ) const;

    void broadcastPosition( const ISendTransportPtr& transport, MarginInstrumentInfoMsg& msg, const instrument_t& instr, const SymbolData& symbol_data ) const;

    void broadcastRiskLimits( TdRiskControl* riskControl, const ISendTransportPtr& transport, MarginLimitsMsg& msg, MarginInstrLimitsMsg& msg_instr, bool doPlanckSleep = true ) const;
    void broadcastOptionGreeksLimit( const TdRiskControlOptions* riskControlOption, const ISendTransportPtr& transport, OptionGreeksLimitsMsg& msg, bool doPlanckSleep = true) const;

    void broadcastAccountInfo( TdRiskControl* riskControl, const ISendTransportPtr& transport, MarginAccountInfoMsg& msg, bool doPlanckSleep = true ) const;
    void broadcastOptionGreeksAccountInfo(const TdRiskControlOptions* riskControlOption, const ISendTransportPtr& transport, OptionGreeksAccountInfoMsg& msg, bool doPlanckSleep = true) const;

    void broadcastInstrumentLimit( const ISendTransportPtr& transport, MarginInstrLimitsMsg& msg, const instrument_t& instr, const InstrumentLimits& instr_data ) const;

    void planckSleep() const;

    template <class BBMsgType>
    void sendMsgWithPlanckSleep( const ISendTransportPtr& sendTrans, BBMsgType& msg, bool sleepAfterSend ) const
    {
        msg.hdr->time_sent = timeval_t::now;
        ++msg.hdr->seq;
        sendTrans->send(&msg);
        if( sleepAfterSend )
        {
            planckSleep();
        }
    }

    std::auto_ptr<RandGen> m_broadcast_delay_gen;
    bool m_planckSleepActive;

    ISendTransportPtr m_transport;
    // second instance of the transport & message for use when sending fills etc.
    ISendTransportPtr m_transport_instant;

    SymbolSet m_broadcastSymbols;
    mutable SymbolSet m_changedSymbols;

    boost::thread m_broadcastThread; // spawned thread has to be initialized last

    boost::thread::id m_mainThreadID; // ID of the thread which constructed this
    bool onMainThread() const; // returns true if called from the thread which constructed this
    bool onBroadcastThread() const;

    typedef std::map< AccountInfoPtr, TdRiskControl* > AccountsInfoMap;
    typedef std::map< AccountInfoOptionPtr, const TdRiskControlOptions*> AccountsInfoOptionMap;
    AccountsInfoMap m_accounts_info;  // every account has its own TdRiskControl
    AccountsInfoOptionMap m_accounts_info_option;

    unsigned long m_seed;

};

} // namespace tdcore
} // namespace bb

#endif // BB_SERVERS_TDCORE_TDPOSITIONBROADCASTERCORE_H

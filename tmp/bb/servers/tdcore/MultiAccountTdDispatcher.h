#ifndef BB_SERVERS_TDCORE_MULTIACCOUNTTDDISPATCHER_H
#define BB_SERVERS_TDCORE_MULTIACCOUNTTDDISPATCHER_H

/* Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved. */


#include <bb/core/acct.h>
#include <bb/core/EFeedType.h>

#include <bb/servers/tdcore/TdOrderManager.h>
#include <bb/servers/tdcore/TdRiskControl.h>
#include <bb/servers/tdcore/TdAccountManager.h>
#include <bb/servers/tdcore/TdConfig.h>
#include <bb/servers/tdcore/MultiAccountTdDispatcherBase.h>

namespace bb {
class TdCancelMsg;

namespace tdcore {

/// like TdDispatcherBase, but with additional functionality for order entry, not just receiving fills
class MultiAccountTdDispatcher
    : public MultiAccountTdDispatcherBase
{
public:
    virtual void run();

protected:
    // The first constructor is for the normal mode of operation for
    // TdDispatcher. Use the second constructor to inhibit the normal
    // construction of an internal SelectDispatcher, for cases where
    // the TdDispatcher will be driven by an external event loop.
    MultiAccountTdDispatcher( MultiAccountTdConfig& config, EFeedType sourceType = SRC_TD );
    MultiAccountTdDispatcher( MultiAccountTdConfig& config, SelectDispatcher& fdSet, EFeedType sourceType = SRC_TD );

    virtual ~MultiAccountTdDispatcher(){};

    // this function should be called by all sub-classes to initialize the
    // client factories for all the accounts and start the listeners
    void initializeAccounts( acct_t exchangeAccount );

    virtual bool handleClientMsgHook( const TdClientPtr&, const Msg& );

    template<typename OrderMsgT>
    TdHandleOrderResult handleOrderRequest( const OrderMsgT& order, const TdClientPtr& bbc );

    virtual bool handleCancelRequest( const TdCancelMsg&, const TdClientPtr& );

    virtual TdHandleOrderResult handleModifyRequest( const TdOrderModifyMsg&, const TdClientPtr& );

    virtual void cancelAll( uint32_t clientId );
    virtual void cancelAll( side_t side , const instrument_t& instr );
    virtual void cancelAll( acct_t acct, side_t side , const instrument_t& instr );

    virtual TdOrderManager& getOrderManager() = 0;

    virtual const TdClientFactoryPtr getClientFactory( acct_t account );

    // derived classes can override this to check whether a given clearing configuation is valid
    // if a problem is found, a string describing it is returned, otherwise NULL is returned
    // if not overridden, the default implementation assumes the TD does not support clearing configuration
    virtual const char* validateClearingConfig( const std::string& mpid, const std::string& account ) const;

    // some TDs, like NYSE, use something other than the MPID to identify the firm
    // the default implementation returns the MPID passed in
    virtual const std::string& mapClearingMPID( const std::string& mpid ) const;

    virtual bool isAllowedUser(acct_t account, const char* user) const;

protected:
    void addAccount( acct_t account, const TdAccountConfig& acctConfig );

    virtual void stop();

    MultiAccountTdConfig&         m_baseConfig;

    virtual void onRunningOrPreviousCrashDetected() {};

private:
    typedef std::map<bb::acct_t,bb::tdcore::TdAccountManagerPtr>   AccountManagerMap;

    void handleTdCancelBySide(const TdCancelBySideMsg&, const TdClientPtr&);
    virtual void assertUserIsAdmin( const TdClientPtr&, const Msg& ) const;

    void handleStop( const TdClientPtr& bbc );

    typedef std::map<acct_t, TdClientFactoryPtr>  AccountFactoryMap;

    AccountFactoryMap             m_accountFactoryMap;
    boost::optional<std::string>  m_clearingMPID;
    TdRiskControlPtr              m_exchangeRiskControl;
    acct_t                        m_exchangeAccount;
    AccountManagerMap             m_accountMgrMap;
};

} // namespace tdcore
} // namespace bb

#endif // BB_SERVERS_TDCORE_MULTIACCOUNTTDDISPATCHER_H

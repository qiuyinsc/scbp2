#ifndef BB_SERVERS_TDCORE_TDORDERCANCELSTATE_H
#define BB_SERVERS_TDCORE_TDORDERCANCELSTATE_H

/* Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved. */


#include <string>

#include <bb/core/cxlstatus.h>

namespace bb {
namespace tdcore {

class TdOrder;
class ITdOrderRouter;
class OrderCancelReject;
class OrderClose;
class OrderReject;
class TdClient;
class OrderModified;
class OrderModifyReject;

class TdOrderCancelState
{
public:
    virtual ~TdOrderCancelState() {}
    virtual cxlstatus_t getStatus() const = 0;

    virtual const TdOrderCancelState* onCancelRequestRecv( TdOrder* o, ITdOrderRouter* r ) const
    {
        return BadEvent( o, "CancelRequestRecv" );
    }

    virtual const TdOrderCancelState* onCancelRequest( TdOrder* o, ITdOrderRouter* r ) const
    {
        return BadEvent( o, "CancelRequest" );
    }

    virtual const TdOrderCancelState* onCancelConfirm( TdOrder* o, ITdOrderRouter* r ) const
    {
        return BadEvent( o, "CancelConfirm" );
    }

    virtual const TdOrderCancelState* onCancelReject( TdOrder* o, ITdOrderRouter* r
        , const OrderCancelReject& creject ) const
    {
        return BadEvent( o, "CancelReject" );
    }

    virtual const TdOrderCancelState* onModifyRequestRecv( TdOrder* o, ITdOrderRouter* r ) const
    {
        return BadEvent( o, "ModifyRequestRecv" );
    }

    virtual const TdOrderCancelState* onModifyRequest( TdOrder* o, ITdOrderRouter* r ) const
    {
        return BadEvent( o, "ModifyRequest" );
    }

    virtual const TdOrderCancelState* onOrderModified( TdOrder* origOrder, TdOrder* newOrder
                                                       , ITdOrderRouter* orderRouter, const OrderModified& modify ) const
    {
        return BadEvent( origOrder, "OrderModified" );
    }

    virtual const TdOrderCancelState* onModifyReject( TdOrder* origOrder, TdOrder* newOrder, ITdOrderRouter* orderRouter ) const;

protected:
    const TdOrderCancelState* nextState( TdOrder* order, const TdOrderCancelState* s ) const;
    const TdOrderCancelState* BadEvent( TdOrder* order, const std::string& func ) const;
};

template<typename T, cxlstatus_t s> class TdOrderCancelStateImpl : public TdOrderCancelState
{
public:
    static const TdOrderCancelState* instance()
    {
        static T obj;
        return &obj;
    }
    cxlstatus_t getStatus() const { return s; }
};

class TdOrderCancelStateNone : public TdOrderCancelStateImpl<TdOrderCancelStateNone, CXLSTATUS_NONE>
{
public:
    virtual const TdOrderCancelState* onCancelRequestRecv( TdOrder* o, ITdOrderRouter* r ) const;
    virtual const TdOrderCancelState* onCancelConfirm( TdOrder* order, ITdOrderRouter* r ) const;
    virtual const TdOrderCancelState* onModifyRequestRecv( TdOrder* order, ITdOrderRouter* r ) const;
};

class TdOrderCancelStatePending : public TdOrderCancelStateImpl<TdOrderCancelStatePending, CXLSTATUS_PENDING>
{
public:
    virtual const TdOrderCancelState* onCancelRequest( TdOrder* o, ITdOrderRouter* r ) const;
    virtual const TdOrderCancelState* onCancelConfirm( TdOrder* o, ITdOrderRouter* r ) const;
    virtual const TdOrderCancelState* onModifyRequest( TdOrder* order, ITdOrderRouter* router ) const;
};

class TdOrderCancelStateTransit : public TdOrderCancelStateImpl<TdOrderCancelStateTransit, CXLSTATUS_TRANSIT>
{
public:
    virtual const TdOrderCancelState* onCancelRequestRecv( TdOrder* o, ITdOrderRouter* r ) const;
    virtual const TdOrderCancelState* onCancelRequest( TdOrder* o, ITdOrderRouter* r ) const;
    virtual const TdOrderCancelState* onCancelConfirm( TdOrder* order, ITdOrderRouter* r ) const;
    virtual const TdOrderCancelState* onCancelReject( TdOrder* o, ITdOrderRouter* r, const OrderCancelReject& creject ) const;
    virtual const TdOrderCancelState* onOrderModified( TdOrder* origOrder, TdOrder* newOrder
                                                       , ITdOrderRouter* orderRouter, const OrderModified& modify ) const;
};

class TdOrderCancelStateConfirmed : public TdOrderCancelStateImpl<TdOrderCancelStateConfirmed, CXLSTATUS_CONFIRMED>
{
public:
    virtual const TdOrderCancelState* onCancelRequest( TdOrder* o, ITdOrderRouter* r ) const;
};

class TdOrderCancelStateReject : public TdOrderCancelStateImpl<TdOrderCancelStateReject, CXLSTATUS_REJECT>
{
public:
    virtual const TdOrderCancelState* onCancelRequestRecv( TdOrder* o, ITdOrderRouter* r ) const;
    virtual const TdOrderCancelState* onCancelRequest( TdOrder* o, ITdOrderRouter* r ) const;
    virtual const TdOrderCancelState* onCancelConfirm( TdOrder* order, ITdOrderRouter* r ) const;
    virtual const TdOrderCancelState* onModifyRequestRecv( TdOrder* order, ITdOrderRouter* r ) const;
};

} // namespace tdcore
} // namespace bb

#endif // BB_SERVERS_TDCORE_TDORDERCANCELSTATE_H

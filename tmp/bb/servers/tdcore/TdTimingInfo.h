#ifndef BB_SERVERS_TDCORE_TDTIMINGINFO_H
#define BB_SERVERS_TDCORE_TDTIMINGINFO_H

/* Contents Copyright 2015 Athena Capital Research LLC. All Rights Reserved. */

#include <bb/core/timeval.h>

namespace bb {
namespace tdcore {

struct TdTimingInfo
{
    TdTimingInfo ( const bb::timeval_t rb = bb::timeval_t::earliest,
                   const bb::timeval_t re = bb::timeval_t::earliest,
                   const bb::timeval_t ab = bb::timeval_t::earliest,
                   const bb::timeval_t ae = bb::timeval_t::earliest,
                   const bb::timeval_t os = bb::timeval_t::earliest,
                   const bb::timeval_t dp = bb::timeval_t::earliest,
                   const bb::timeval_t dr = bb::timeval_t::earliest,
                   const uint32_t dc = 0
        );

    static TdTimingInfo & empty();
    static TdTimingInfo instance;
    void reset();

    bb::timeval_t risk_begin_time;
    bb::timeval_t risk_end_time;
    bb::timeval_t vendor_api_begin_time;
    bb::timeval_t vendor_api_end_time;
    bb::timeval_t order_send_time;
    bb::timeval_t dispatcher_poll_time;
    bb::timeval_t dispatcher_ready_time;
    uint32_t      dispatcher_ready_count;
};

}
}

#endif // BB_SERVERS_TDCORE_TDTIMINGINFO_H

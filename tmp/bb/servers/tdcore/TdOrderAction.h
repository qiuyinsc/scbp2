#ifndef BB_SERVERS_TDCORE_TDORDERACTION_H
#define BB_SERVERS_TDCORE_TDORDERACTION_H

/* Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved. */


#include <bb/core/timeval.h>
#include <bb/core/fillflag.h>
#include <bb/core/instrument.h>
#include <bb/core/liquidity.h>
#include <bb/core/oreason.h>
#include <bb/core/side.h>
#include <bb/core/tid.h>
#include <bb/core/Log.h>

namespace bb {
namespace tdcore {

///
/// Represents an action that can happen on an order
///
class OrderAction
{
public:
    virtual ~OrderAction() {}

    const bb::tid_t& getTradeId() const { return tradeId; }
    const bb::timeval_t& getExchangeTime() const { return exchTime; }

    bb::timeval_t& nicTime() { return m_nicTime; }
    bb::timeval_t& applicationTime() { return m_applicationTime; }

    const bb::timeval_t& nicTime() const { return m_nicTime; }
    const bb::timeval_t& applicationTime() const { return m_applicationTime; }

    std::string& exchangeOrderId(){ return m_exchangeOrderId; }
    const std::string& exchangeOrderId() const { return m_exchangeOrderId; }
protected:
    OrderAction( const bb::tid_t& _id, const bb::timeval_t& _time ) : tradeId( _id ), exchTime(_time) {}
    const bb::tid_t tradeId;
    const bb::timeval_t exchTime;
    bb::timeval_t m_nicTime;
    bb::timeval_t m_applicationTime;

    std::string m_exchangeOrderId;
};

class OrderConfirm : public OrderAction
{
public:
    OrderConfirm( const bb::tid_t& tid, const bb::timeval_t& _time, const std::string& _fixid = std::string()
            , bool _canceled = false, double _price = 0, uint64_t _exch_ref = 0 )
        : OrderAction( tid, _time ), fixid( _fixid ), canceled( _canceled ), price( _price ), exch_ref( _exch_ref ) {}

    std::string fixid;
    bool canceled; // order was canceled in the confirmation (e.g. OUCH can do this)
    double price; // if nonzero, order was repriced by the exchange (e.g. on OUCH for Post-Only)
    uint64_t exch_ref;
};

// class OrderCancelConfirm : public OrderAction
// {
// public:
//     OrderCancelConfirm( const bb::tid_t& tid, const bb::timeval_t& _time ) : OrderAction( tid ), exchConfirmTime( _time ) {}

//     bb::timeval_t exchConfirmTime;
// };

class OrderReject : public OrderAction
{
public:
    OrderReject( const bb::tid_t& tid, const bb::timeval_t& _time, const std::string& _text,
            bb::oreason_t _reason = bb::R_REJECT )
    : OrderAction( tid, _time ), text( _text ), reason( _reason ) {}
    oreason_t getRejectReason() const { return reason; }
    const std::string& getReasonText() const { return text; }

// protected:
    std::string text;
    bb::oreason_t reason;
};

class OrderCancelReject : public OrderAction
{
public:
    OrderCancelReject( const bb::tid_t& tid, const bb::timeval_t& _time ) : OrderAction( tid, _time ) {}
};

class OrderExecution : public OrderAction
{
public:
    OrderExecution( const bb::tid_t& tid
        , const bb::timeval_t& _time
        , const bb::instrument_t& instr
        , bb::dir_t _dir
        , double _price, uint32_t _size
        , const std::string& _counter = ""
        , bb::liquidity_t _liquidity = LIQ_UNKNOWN
        , bb::mktdest_t _contra_broker = MKT_UNKNOWN
        , bb::fillflag_t fill_flags = FILLFLAG_NONE
        , const char* fee_code = ""
        , const std::string& execId = ""
        ) : OrderAction( tid, _time )
                , instrument( instr )
                , dir( _dir )
                , executionPrice( _price )
                , executionSize( _size )
                , counterParty( _counter )
                , liquidity( _liquidity )
                , contra_broker( _contra_broker )
                , fillFlags( fill_flags )
                , feeCode(fee_code)
                , execId(execId)
        {}

    uint32_t getSeqNum() const { return getTradeId().orderid; }

    bb::instrument_t instrument;
    bb::dir_t dir;
    double executionPrice;
    uint32_t executionSize;
    std::string counterParty;
    liquidity_t liquidity;
    mktdest_t contra_broker;
    fillflag_t fillFlags;
    std::string feeCode;
    std::string execId;
};

class OrderClose : public OrderAction
{
public:
    enum CloseReason { UNKNOWN, FILLED, CANCELLED, DROPPED, CANCELLED_BY_EXCHANGE, UNKNOWN_MODIFY };

    OrderClose( const bb::tid_t& tid, const bb::timeval_t& _time, CloseReason r )
        : OrderAction( tid, _time ), reason( r )
        , volumeTraded( 0 )
        , volumeRemaining( 0 )
    {}

    CloseReason reason;
    int32_t volumeTraded;
    int32_t volumeRemaining;
};

class TradeBust : public OrderAction
{
public:
    enum STATUS { UNKNOWN, ORDER_OPEN, ORDER_CLOSED };
    TradeBust( const bb::tid_t& tid, const bb::timeval_t& _time
        , double _price, uint32_t _size, STATUS _status )
        : OrderAction( tid, _time )
        , bustPrice( _price )
        , bustSize( _size )
    {}

    double bustPrice;
    uint32_t bustSize;
    STATUS status;
};

class OrderReprice : public OrderAction
{
public:
    OrderReprice( const bb::tid_t& tid, const bb::timeval_t& _time, double _price )
        : OrderAction( tid, _time ), price( _price ) {}

    double price;
};

class OrderRoute : public OrderAction
{
public:
    OrderRoute( const tid_t& tid, const bb::timeval_t& _time, mktdest_t _via_mkt )
        : OrderAction( tid, _time ), via_mkt( _via_mkt ) {}

    mktdest_t via_mkt;
};

class OrderModified
    : public OrderAction
{
public:
    enum CloseReason { UNKNOWN, FILLED, CANCELLED, DROPPED, CANCELLED_BY_EXCHANGE, REPLACED };

    OrderModified( const tid_t& new_tid, const tid_t& orig_tid, const bb::timeval_t& _time, CloseReason _reason
                   , const std::string& _fixid = std::string(), double _price = 0, uint64_t _exch_ref = 0 )
        : OrderAction( new_tid, _time )
        , orig_tradeId( orig_tid )
        , fixid( _fixid )
        , price( _price )
        , exch_ref( _exch_ref )
        , reason( _reason )
    {}

    const bb::tid_t& getOrigTradeId() const { return orig_tradeId; }

    const bb::tid_t orig_tradeId;
    std::string fixid;
    double price; // if nonzero, order was repriced by the exchange (e.g. on OUCH for Post-Only)
    uint64_t exch_ref;
    CloseReason reason;
};

class OrderModifyReject
    : public OrderAction
{
public:
    OrderModifyReject( const tid_t& orig_tid, const tid_t& new_tid, const bb::timeval_t& _time,
                       const std::string& _text, bb::oreason_t _reason = bb::R_REJECT )
        : OrderAction( new_tid, _time )
        , text( _text )
        , reason( _reason )
        , orig_tradeId( orig_tid )
    {}

    const bb::tid_t& getOrigTradeId() const { return orig_tradeId; }
    oreason_t getRejectReason() const { return reason; }
    const std::string& getReasonText() const { return text; }
protected:
    std::string text;
    bb::oreason_t reason;
    const bb::tid_t orig_tradeId;
};

} // namespace bb
} // namespace tdcore

#endif // BB_SERVERS_TDCORE_TDORDERACTION_H

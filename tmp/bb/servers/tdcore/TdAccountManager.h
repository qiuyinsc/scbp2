#ifndef BB_SERVERS_TDCORE_TDACCOUNTMANAGER_H
#define BB_SERVERS_TDCORE_TDACCOUNTMANAGER_H

/* Contents Copyright 2015 Athena Capital Research LLC. All Rights Reserved. */
#include <bb/core/acct.h>

#include <bb/servers/tdcore/TdPositionSender.h>
#include <bb/servers/tdcore/ITdRiskControl.h>
#include <bb/servers/tdcore/TdRiskControl.h>

namespace bb {
namespace tdcore {

/*
 * This class is defined to contain the information that is unique to an account that
 * will be used at run-time to process its messages.  It is attached to each TdClient
 * for the account.
 */
class TdAccountManager
{
public:
    /// The constructor takes in the following parameters
    ///   account         - the bb account
    ///   combined_risk   - the risk control object that will be used when processing orders
    ///                     it combines the account riskControl Object with the Exchange Account object
    ///   accountRisk     - the risk Control object for this account. This is used to create a position Sender
    ///   mpid            - the mpid, if any, to be used when creating new clients
    ///   clearingAccount - the clearing account, if any, to be used when creating new clients
    TdAccountManager( acct_t account, ITdRiskControl& combined_risk, TdRiskControlPtr accountRisk, std::string& mpid, std::string& clearingAccount  )
        : m_account( account )
        , m_riskControl( combined_risk )
        , m_positionSender( *accountRisk,  account )
        , m_mpid( mpid )
        , m_clearingAccount( clearingAccount )
    {};

    acct_t                          m_account;
    ITdRiskControl&                 m_riskControl;
    TdPositionSender                m_positionSender;
    std::string                     m_mpid;
    std::string                     m_clearingAccount;
};

BB_DECLARE_SHARED_PTR( TdAccountManager );

}// end tdcore
}// end bb
#endif // BB_SERVERS_TDCORE_TDACCOUNTMANAGER_H

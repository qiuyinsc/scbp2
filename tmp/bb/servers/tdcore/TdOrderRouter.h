#ifndef BB_SERVERS_TDCORE_TDORDERROUTER_H
#define BB_SERVERS_TDCORE_TDORDERROUTER_H

/* Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved. */


#include <bb/servers/tdcore/TdOrder.h>
#include <bb/servers/tdcore/ITdOrderRouter.h>
#include <bb/servers/tdcore/MultiAccountTdServer.h>
#include <bb/servers/tdcore/TdPositionBroadcaster.h>
#include <bb/servers/tdcore/MultiAccountRiskControl.h>
#include <bb/servers/tdcore/TdPositionBroadcasterCore.h>
#include <bb/servers/tdcore/MultiAccountTdPositionBroadcaster.h>

namespace bb {
namespace tdcore {

class TdPositionSender;
class TdRiskControl;
class ITdRiskControl;

/// Common implementation to integrate with TdRiskControl
class TdOrderRouterBase : public ITdOrderRouter
{
public:
    TdOrderRouterBase( ITdPositionBroadcaster& position_broadcaster);

    virtual ~TdOrderRouterBase();

    virtual void halt();
    virtual void halt( const instrument_t& instr );
    virtual void resume();
    virtual void resume( const instrument_t& instr );
    virtual bool isHalted() const;

    virtual void sendNewOrderSpec( const StockOrder& o ) { sendNewOrder( o ); }
    virtual void sendNewOrderSpec( const FutureOrder& o ) { sendNewOrder( o ); }
    virtual void sendNewOrderSpec( const Order& o ) { sendNewOrder( o ); }

    virtual void sendNewOrderSpec( const ReserveOrder& o )
    {
        BB_THROW_ERROR( "ReserveOrders are not supported by this TradeDaemon" );
    }

    virtual void modifyOrder( const ModifyOrder& order )
    {
        BB_THROW_ERROR( "ModifyOrders are not supported by this TradeDaemon" );
    }

    /// Checks that exch_ref for an order is not 0
    /// Checks that fixid is not empty
    /// which handles orders for both Fix protocols and Binary protocols.
    virtual bool isModifiable( const TdOrder& order ) const
    {
        if ( 0 == order.getExchRef() )
        {
            return false;
        }

        return true;
    }

    virtual bool validateModifyOrder( const TdOrder& origOrder, const ModifyOrder& newOrder )
    {
        if( unlikely( opposite_dir( origOrder.getDir() ) == newOrder.getDir() ) )
        {
            const_cast<ModifyOrder&>(newOrder).setValidateState(TdOrder::VALIDATE_STATE_FAILURE);
            throw TdOrderManager::WarnInvalidRequest( "Side cannot be flipped while modifying an order" );
        }

        if( unlikely( origOrder.getSymbol() != newOrder.getSymbol() ) )
        {
            const_cast<ModifyOrder&>(newOrder).setValidateState(TdOrder::VALIDATE_STATE_FAILURE);
            throw TdOrderManager::WarnInvalidRequest( "Cannot change the instrument while modifying an order" );
        }

        const_cast<ModifyOrder&>(newOrder).setValidateState(TdOrder::VALIDATE_STATE_SUCCESS);
        return true;
    }


    virtual void onOrderAction( const TdOrder&, const OrderExecution& );
    virtual void onOrderAction( const TdOrder&, const TradeBust& );
    virtual void onOrderAction( const TdOrder&, const OrderConfirm& );
    virtual void onOrderAction( const TdOrder&, const OrderReject& );
    virtual void onOrderAction( const TdOrder&, const OrderClose& );
    virtual void onOrderAction( const TdOrder&, const OrderCancelReject& );
    virtual void onOrderAction( const TdOrder& origOrder, const TdOrder& newOrder, const OrderModified& modify );
    virtual void onOrderAction( const TdOrder& origOrder, const TdOrder& newOrder, const OrderModifyReject& modifyReject );

    virtual void onUnknownConfirm( const OrderConfirm& );

protected:
    virtual void sendNewOrder( const TdOrder& ) = 0;

    // Call this function when the riskControl object is needed but you have no order
    virtual ITdRiskControl&             getRiskControl() const  = 0;

    // Call this function when the riskControl object is needed and you have an order
    virtual ITdRiskControl&             getRiskControl( const TdOrder& order ) const = 0;

    virtual ITdPositionBroadcaster&  getPositionBroadcaster();
    virtual TdPositionSender&           getPositionSender( const TdOrder& order ) = 0;

    ITdPositionBroadcaster&      m_positionBroadcaster;
};

class TdOrderRouter
    : public TdOrderRouterBase
{
public:
    TdOrderRouter( TdRiskControl& risk_control,
                   TdPositionBroadcaster& position_broadcaster,
                   TdPositionSender& position_sender )
        : TdOrderRouterBase(position_broadcaster)
        , m_riskControl( risk_control)
        , m_positionSender( position_sender )
    {};
    virtual ~TdOrderRouter(){};
protected:
    // For the base TdOrder router both getRiskControl functions act the same, returning the local reference
    virtual ITdRiskControl&             getRiskControl()                       const { return m_riskControl; };
    virtual ITdRiskControl&             getRiskControl( const TdOrder& order ) const { return m_riskControl; };
    virtual TdPositionSender&           getPositionSender( const TdOrder& order )    { return m_positionSender; };

    TdRiskControl&      m_riskControl;
    TdPositionSender&   m_positionSender;
};
BB_DECLARE_SHARED_PTR(TdOrderRouter);

class MultiAccountTdOrderRouter
    : public TdOrderRouterBase
{
public:
    MultiAccountTdOrderRouter( MultiAccountRiskControl& risk,
                               TdPositionBroadcasterCore& position_broadcaster,
                               MultiAccountTdServer* server);

    virtual ~MultiAccountTdOrderRouter(){};

    virtual void halt();
    virtual void halt( const instrument_t& instr );
    void halt( const acct_t& account );
    virtual void resume();
    virtual void resume( const instrument_t& instr );
    virtual bool isHalted() const;

    virtual void onUnknownConfirm( const OrderConfirm& );

protected:

    // This function will return a reference to the base MultiAccountRiskControl object
    virtual ITdRiskControl&             getRiskControl( )  const ;

    // This function will return the riskControl reference that is contained
    // within the TdAccountManager object pointed to by the TdClient used to send the provided order
    virtual ITdRiskControl&             getRiskControl( const TdOrder& order ) const ;
    virtual TdPositionSender&           getPositionSender( const TdOrder& order );

    MultiAccountRiskControl&      m_riskControl;

private:
    MultiAccountTdServer*         m_server;
};


} // tdcore
} // bb

#endif // BB_SERVERS_TDCORE_TDORDERROUTER_H

#ifndef BB_SERVERS_TDCORE_ITDCLIENT_H
#define BB_SERVERS_TDCORE_ITDCLIENT_H

/* Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved. */

#include <bb/core/side.h>
#include <bb/core/fillflag.h>
#include <bb/core/smart_ptr.h>
#include <bb/core/source.h>
#include <bb/core/fillflag.h>
#include <bb/core/instrument.h>
#include <bb/core/acct.h>
#include <bb/core/oreason.h>
#include <bb/core/ostatus.h>
#include <bb/core/cxlstatus.h>
#include <bb/core/liquidity.h>
#include <bb/core/mtype.h>
#include <bb/core/tid.h>

#include <bb/io/SendTransport.h>

#include <bb/servers/tdcore/TdTimingInfo.h>

namespace bb {
namespace tdcore {

class ITdClient: public ISendTransport
{
public:
    virtual ~ITdClient() {}

    virtual const std::string& getIdent() const=0;
 //   bool isAuthenticated() const;
 //   const char* authenticatedUsername() const;

    virtual uint32_t getId() const=0;
    virtual acct_t getAccount() const=0;


    /// send TdStatusChange (new type) message
    virtual bool sendChange( const bb::instrument_t& instr
        , const bb::tid_t& tradeid
        , uint32_t clientid
        , uint32_t orderid
        , uint32_t seq
        , bb::ostatus_t old_status
        , bb::ostatus_t new_status
        , bb::cxlstatus_t old_cxlstatus
        , bb::cxlstatus_t new_cxlstatus
        , bb::oreason_t reason
        , uint64_t exchref
        , const bb::timeval_t& exch_time
        , const TdTimingInfo & timing_info
        , const bb::timeval_t& application_time
        , const bb::timeval_t& nic_time
        ) = 0;

    /// This message is introduced to send status changes back after
    /// orders are modified
    virtual bool sendChange( const instrument_t& instr
                     , const tid_t& tradeid
                     , uint32_t clientid
                     , uint32_t orderid
                     , uint32_t seq
                     , ostatus_t old_status
                     , ostatus_t new_status
                     , cxlstatus_t old_cxlstatus
                     , cxlstatus_t new_cxlstatus
                     , oreason_t reason
                     , uint64_t exchref
                     , const timeval_t& exch_time
                     , const TdTimingInfo & timing_info

                     /// Fields related to the original order
                     , const tid_t& orig_tradeid
                     , uint32_t orig_orderid
                     , uint32_t orig_seq
                     , ostatus_t old_status_orig
                     , ostatus_t new_status_orig
                     , cxlstatus_t old_cxlstatus_orig
                     , cxlstatus_t new_cxlstatus_orig
                     , oreason_t orig_reason
                     , uint64_t orig_exchref
                     ///

                     , const bb::timeval_t& application_time
                     , const bb::timeval_t& nic_time
        ) = 0;

    // for stocks
    virtual bool sendFill( bb::symbol_t sym
        , const bb::tid_t& tradeid
        , unsigned int clientid
        , unsigned int orderid
        , unsigned int seq
        , double px
        , unsigned int size
        , unsigned int left
        , bb::dir_t dir
        , bb::mktdest_t dest
        , int32_t cur_pos
        , const bb::timeval_t& exch_time
        , bb::liquidity_t liquidity = bb::LIQ_UNKNOWN
        , bb::mktdest_t contra_broker = bb::MKT_UNKNOWN
        , const std::string& fee_code = ""
        )=0;

    virtual bool sendFillFuture( const bb::instrument_t& instr
        , const bb::tid_t& tradeid
        , unsigned int clientid
        , unsigned int orderid
        , unsigned int seq
        , double px
        , unsigned int size
        , unsigned int left
        , bb::dir_t dir
        , const char* cparty
        , bb::mktdest_t dest
        , int32_t cur_pos
        , const bb::timeval_t& exch_time
        , bb::liquidity_t liquidity = bb::LIQ_UNKNOWN
        , bb::fillflag_t fillflags = bb::FILLFLAG_NONE
        , const uint64_t exchangeOrderId = 0
        , const bb::timeval_t& application_time = timeval_t()
        , const bb::timeval_t& nic_time = timeval_t()
        )=0;

    // for options (and eventually for everything)
    virtual bool sendFill( const bb::instrument_t& instr
        , const bb::tid_t& tradeid
        , unsigned int clientid
        , unsigned int orderid
        , double px
        , unsigned int size
        , unsigned int left
        , bb::dir_t dir
        , bb::mktdest_t dest
        , int32_t cur_pos
        , const bb::timeval_t& exch_time
        , bb::liquidity_t liquidity
        , bb::mktdest_t contra_broker
        , bb::fillflag_t fillflags )=0;

    virtual bool sendCancelReject( bb::symbol_t sym, unsigned int orderid )=0;

    virtual bool sendReprice( const bb::instrument_t& instr
        , const bb::tid_t& tradeid
        , unsigned int clientid
        , unsigned int orderid
        , double px
        , const bb::timeval_t& exch_time )=0;

    virtual bool sendRoute( const bb::instrument_t& instr
        , const bb::tid_t& tradeid
        , unsigned int orderid
        , mktdest_t via_mkt )=0;
};

BB_DECLARE_SHARED_PTR( ITdClient );

} // tdcore
} // bb

#endif // BB_SERVERS_TDCORE_ITDCLIENT_H

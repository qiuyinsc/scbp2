#ifndef BB_SERVERS_TDCORE_BINARYUTILS_H
#define BB_SERVERS_TDCORE_BINARYUTILS_H

/* Contents Copyright 2016 Athena Capital Research LLC. All Rights Reserved. */

#include <iostream>
#include <string>
#include <sstream>
#include <iomanip>

namespace bb {
namespace tdcore {
namespace BinaryUtils {

std::string memToHexString( const void* ptr, size_t size )
{
    std::ostringstream ret;

    const unsigned char* str = reinterpret_cast<const unsigned char*>( ptr );
    for(size_t i = 0; i < size; ++i)
    {
        ret << std::hex << std::setfill('0') << std::setw(2) << (int)str[i] << " ";
    }

    return ret.str();
}

}  // namespace BinaryUtils
}  // namespace tdcore
}  // namespace bb

#endif // BB_SERVERS_TDCORE_BINARYUTILS_H

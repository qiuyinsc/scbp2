#ifndef BB_SERVERS_TDCORE_ITDORDERROUTER_H
#define BB_SERVERS_TDCORE_ITDORDERROUTER_H

/* Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved. */


#include <stdexcept>
#include <bb/core/oreason.h>
#include <bb/core/Error.h>

namespace bb {
namespace tdcore {
class TdOrder;
class StockOrder;
class FutureOrder;
class Order;
class ModifyOrder;
struct ReserveOrder;


class OrderExecution;
class TradeBust;
class OrderConfirm;
class OrderReject;
class OrderClose;
class OrderCancelReject;
class OrderModified;
class OrderModifyReject;

class TdOrderManager;

///
/// Manages translation of order messages
///
class ITdOrderRouter
{
public:
    ITdOrderRouter() : orderMgr( NULL ) {}
    virtual ~ITdOrderRouter() {}

    // to return true if the router is ready to send orders or cancels
    virtual bool isActive() const = 0;

    // to external
    virtual void sendNewOrderSpec( const StockOrder& ) = 0;
    virtual void sendNewOrderSpec( const FutureOrder& ) = 0;
    virtual void sendNewOrderSpec( const Order& ) = 0;
    virtual void sendNewOrderSpec( const ReserveOrder& ) = 0;


    // these functions can be made pure virtual when they are implemented in ASIA TD
    virtual void modifyOrder ( const ModifyOrder& )
    {
        BB_THROW_ERROR( "ModifyOrders are not supported by this TradeDaemon" );
    }
    virtual bool isModifiable( const TdOrder& ) const
    {
        BB_THROW_ERROR( "ModifyOrders are not supported by this TradeDaemon" );
    }
    virtual bool validateModifyOrder( const TdOrder&, const ModifyOrder& )
    {
        BB_THROW_ERROR( "ModifyOrders are not supported by this TradeDaemon" );
    }

    virtual void sendCancel  ( const TdOrder& ) = 0;

    // event notification
    virtual void onOrderAction( const TdOrder&, const OrderExecution&    ) = 0;
    virtual void onOrderAction( const TdOrder&, const TradeBust&         ) = 0;
    virtual void onOrderAction( const TdOrder&, const OrderConfirm&      ) = 0;
    virtual void onOrderAction( const TdOrder&, const OrderReject&       ) = 0;
    virtual void onOrderAction( const TdOrder&, const OrderClose&        ) = 0;
    virtual void onOrderAction( const TdOrder&, const OrderCancelReject& ) = 0;
    virtual void onOrderAction( const TdOrder&, const TdOrder&, const OrderModified& )
    {
        BB_THROW_ERROR( "ModifyOrders are not supported by this TradeDaemon" );
    }
    virtual void onOrderAction( const TdOrder&, const TdOrder&, const OrderModifyReject& )
    {
        BB_THROW_ERROR( "ModifyOrders are not supported by this TradeDaemon" );
    }

    virtual void onUnknownExecution( const OrderExecution& ) = 0;
    virtual void onUnknownConfirm  ( const OrderConfirm&   ) = 0;

    void initOrderManager( TdOrderManager* om ) { orderMgr = om; }
    TdOrderManager* getOrderManager() { return orderMgr; }
    const TdOrderManager* getOrderManager() const { return orderMgr; }

    struct WarnSendFailedNotLoggedOn : std::runtime_error
    {
        WarnSendFailedNotLoggedOn() : std::runtime_error( "not logged on" ) {}
    };
    struct WarnSendFailedMsgRateExceeded : std::runtime_error
    {
        WarnSendFailedMsgRateExceeded() : std::runtime_error( "msg rate exceeded" ) {}
    };
    struct WarnSendFailedWithRejectReason : std::runtime_error
    {
        WarnSendFailedWithRejectReason( const oreason_t& reason, const std::string& text
            ) : std::runtime_error( text ),
                m_reason( reason ) {}
        oreason_t getReason() const { return m_reason; }

        const oreason_t m_reason;
    };
    struct WarnNotSupported : std::runtime_error
    {
        WarnNotSupported() : std::runtime_error( "not supported" ) {}
    };
    struct WarnSendFailed : std::runtime_error
    {
        WarnSendFailed() : runtime_error( "send failed" ) {}
    };

private:
    TdOrderManager* orderMgr;
};

} // namespace tdcore
} // namespace bb

#endif // BB_SERVERS_TDCORE_ITDORDERROUTER_H

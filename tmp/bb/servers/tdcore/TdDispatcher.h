#ifndef BB_SERVERS_TDCORE_TDDISPATCHER_H
#define BB_SERVERS_TDCORE_TDDISPATCHER_H

/* Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved. */


#include <bb/core/EFeedType.h>

#include <bb/servers/tdcore/TdDispatcherBase.h>
#include <bb/servers/tdcore/TdOrderManager.h>
#include <bb/servers/tdcore/TdRiskControl.h>

namespace bb {
class TdCancelMsg;

namespace tdcore {


/// like TdDispatcherBase, but with additional functionality for order entry, not just receiving fills
class TdDispatcher : public TdDispatcherBase
{
protected:
    // The first constructor is for the normal mode of operation for
    // TdDispatcher. Use the second constructor to inhibit the normal
    // construction of an internal SelectDispatcher, for cases where
    // the TdDispatcher will be driven by an external event loop.
    TdDispatcher( const acct_t& account, EFeedType sourceType = SRC_TD );
    TdDispatcher( const acct_t& account, FDSet& fdSet, EFeedType sourceType = SRC_TD );

    virtual bool handleClientMsgHook( const TdClientPtr&, const Msg& );

    // derived classes can override this to check whether a given clearing configuation is valid
    // if a problem is found, a string describing it is returned, otherwise NULL is returned
    // if not overridden, the default implementation assumes the TD does not support clearing configuration
    virtual const char* validateClearingConfig( const std::string& mpid, const std::string& account ) const;

    // some TDs, like NYSE, use something other than the MPID to identify the firm
    // the default implementation returns the MPID passed in
    virtual const std::string& mapClearingMPID( const std::string& mpid ) const;


    template<typename OrderMsgT>
    TdHandleOrderResult handleOrderRequest( const OrderMsgT&, const TdClientPtr& );

    virtual bool handleCancelRequest( const TdCancelMsg&, const TdClientPtr& );

    virtual TdHandleOrderResult handleModifyRequest( const TdOrderModifyMsg&, const TdClientPtr& );

    virtual void cancelAll( uint32_t clientId );

    virtual TdOrderManager& getOrderManager() = 0;

private:
    void handleTdCancelBySide(const TdCancelBySideMsg&, const TdClientPtr&);
};

} // namespace tdcore
} // namespace bb

#endif // BB_SERVERS_TDCORE_TDDISPATCHER_H

#ifndef BB_IO_SSE_MSG_H
#define BB_IO_SSE_MSG_H 

namespace bb {

#pragma pack(push, 1)
struct SSEFileIndex
{
    char Code[16];
    uint64_t CodeBeginPosition;
    uint64_t CodeEndPosition;
};

struct SSEFileHead
{
    char Category[16];
    uint64_t DateTime;
    uint32_t HeadSize;
    uint32_t DescriptionSize;
    uint32_t IndexSize;
    uint32_t RecordSum;
    uint32_t RecordLen;
    uint32_t FieldCount;
    uint32_t CodeCount;
};

struct SSEFileDescription
{
    uint32_t FieldPosition;
    uint32_t FieldLen;
    char FieldType[4];
    uint32_t Precision;
};

const size_t SSE_FILE_HEAD_LEN = sizeof(SSEFileHead);
const size_t SSE_FILE_DESCRIPTION_LEN = sizeof(SSEFileDescription);
const size_t SSE_FILE_INDEX_LEN = sizeof(SSEFileIndex);

struct SSETick
{
    uint64_t DateTime;
    uint32_t Price; 
    uint32_t Volume;
    uint64_t Amount;
    uint64_t BuyNo;
    uint64_t SellNo; 
};

struct SSEAuction
{
    uint64_t DateTime;
    uint32_t Price;
    uint64_t VirtualAuctionQty;
    uint64_t LeaveQty;
    char Side[8];
};

#define MAX_LEVEL_SIZE 5
#define MAX_LEVEL2_SIZE  10
#define LEVEL2_ORDER_SIZE  50

struct SSESnapL1
{
    uint64_t uiDateTime;
    uint32_t uiPreClosePx;
    uint32_t uiOpenPx;
    uint32_t uiHighPx;
    uint32_t uiLowPx;
    uint32_t uiLastPx;
    uint64_t uiVolume;
    uint64_t uiAmount;
    uint32_t arrBidPx[MAX_LEVEL_SIZE];
    uint32_t arrOfferPx[MAX_LEVEL_SIZE];
    uint64_t arrBidSize[MAX_LEVEL_SIZE];
    uint64_t arrOfferSize[MAX_LEVEL_SIZE];
    uint64_t uiNumTrades;
    uint32_t uiIOPV;
    uint32_t uiNAV;
    char strPhaseCode[8];
};

struct SSESnapL2
{
    uint64_t        uiDateTime;
    uint32_t        uiPreClosePx;
    uint32_t        uiOpenPx;
    uint32_t        uiHighPx;
    uint32_t        uiLowPx;
    uint32_t        uiLastPx;
    uint32_t        uiNumTrades;
    char            strInstrumentStatus[8];
    uint64_t        uiTotalVolumeTrade;
    uint64_t        uiTotalValueTrade;
    uint64_t        uiTotalBidQty;
    uint64_t        uiTotalOfferQty;
    uint32_t        uiWeightedAvgBidPx;
    uint32_t        uiWeightedAvgOfferPx;
    uint32_t        uiWithdrawBuyNumber;
    uint32_t        uiWithdrawSellNumber;
    uint64_t        uiWithdrawBuyAmount;
    uint64_t        uiWithdrawBuyMoney;
    uint64_t        uiWithdrawSellAmount;
    uint64_t        uiWithdrawSellMoney;
    uint32_t        uiTotalBidNumber;
    uint32_t        uiTotalOfferNumber;
    uint32_t        uiBidTradeMaxDuration;
    uint32_t        uiOfferTradeMaxDuration;
    uint32_t        uiNumBidOrders;
    uint32_t        uiNumOfferOrders;
    uint32_t        arrBidPrice[MAX_LEVEL2_SIZE];
    uint64_t        arrBidOrderQty[MAX_LEVEL2_SIZE];
    uint32_t        arrBidNumOrders[MAX_LEVEL2_SIZE];
    uint64_t        arrBidOrders[LEVEL2_ORDER_SIZE];
    uint32_t        arrOfferPrice[MAX_LEVEL2_SIZE];
    uint64_t        arrOfferOrderQty[MAX_LEVEL2_SIZE];
    uint32_t        arrOfferNumOrders[MAX_LEVEL2_SIZE];
    uint64_t        arrOfferOrders[LEVEL2_ORDER_SIZE];
    uint32_t        uiIOPV;
    uint32_t        uiETFBuyNumber;
    uint64_t        uiETFBuyAmount;
    uint64_t        uiETFBuyMoney;
    uint32_t        uiETFSellNumber;
    uint64_t        uiETFSellAmount;
    uint64_t        uiETFSellMoney;
};

struct SSEDayMin
{
    uint64_t        uiDateTime;
    uint32_t        uiPreClosePx;
    uint32_t        uiOpenPx;
    uint32_t        uiHighPx;
    uint32_t        uiLowPx;
    uint32_t        uiLastPx;
    uint64_t        uiVolume;
    uint64_t        uiAmount;
    uint64_t        uiIOPV;
};

const size_t SSE_TICK_LEN = sizeof(SSETick);
const size_t SSE_AUCTION_LEN = sizeof(SSEAuction);
const size_t SSE_SNAP_L1_LEN = sizeof(SSESnapL1);
const size_t SSE_SNAP_L2_LEN = sizeof(SSESnapL2);
const size_t SSE_DAY_MIN_LEN = sizeof(SSEDayMin);
#pragma pack(pop)
}
#endif

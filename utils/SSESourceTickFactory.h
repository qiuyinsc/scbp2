#ifndef BB_IO_SSE_SOURCETICKFACTORY_H
#define BB_IO_SSE_SOURCETICKFACTORY_H 

#include <fstream>
#include <queue>
#include <string>

#include <bb/clientcore/TickFactory.h>

#include "SSETickProvider.h"

using namespace std;

namespace {
bb::ITickProviderPtr g_tp;
};

namespace bb {

class SSESourceTickFactory : public SourceTickFactory
{
public:
    SSESourceTickFactory(ClientContextPtr const& spClientContext)
        : SourceTickFactory(spClientContext)
    {
        m_clientContext = spClientContext;
    }

public:
    virtual ITickProviderPtr getTickProvider( const instrument_t& instr, source_t src, bool create ) {
        if (src.type() == SRC_SHSE) {
             g_tp.reset(new SSETickProvider(m_clientContext, instr, src, string("tickdesc")));
             return g_tp;
        } 
        return getTickProvider(instr, src, create);
    }
private:
    ClientContextPtr m_clientContext;
};

BB_DECLARE_SHARED_PTR(SSESourceTickFactory);

}
#endif

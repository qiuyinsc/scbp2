#ifndef BB_IO_SSE_BOOKBUILDER_H
#define BB_IO_SSE_BOOKBUILDER_H 

#include <fstream>
#include <queue>
#include <vector>

#include <bb/clientcore/BookBuilder.h>

#include "SSEBook.h"

using namespace std;
namespace bb {

namespace {
    vector<bb::IBookPtr> g_books;
};

class SSEBookBuilder : public BookBuilder
{
public:
    SSEBookBuilder(source_t& src, HistClientContextPtr& ctx)
        : BookBuilder(ctx)
        , m_src(src)
        , m_ctx(ctx) {} 

public:
    virtual IBookPtr buildBook(IBookSpecCPtr spec)
    {
        if (MKT_SHSE == (spec->getInstrument()).mkt) {
            bb::IBookPtr spBook(new SseBook(m_ctx, spec->getInstrument(), m_src, string("desc")));
            g_books.push_back(spBook); 
            return spBook;
        } else {
            return BookBuilder::buildBook(spec);
        }
    }

private:
    source_t m_src;
    HistClientContextPtr m_ctx;
};

BB_DECLARE_SHARED_PTR(SSEBookBuilder);

}
#endif

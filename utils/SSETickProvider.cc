#include <bb/core/CommoditiesSpecifications.h>
#include <bb/clientcore/ClientContext.h>
#include <bb/clientcore/MsgHandler.h>
#include <bb/clientcore/PriceProviderBuilder.h>
#include <bb/clientcore/TickFactory.h>
#include <bb/core/LuaCodeGen.h>
#include <bb/core/LuabindScripting.h>
#include <bb/core/messages.h>
#include <bb/core/ptime.h>
#include <boost/bind.hpp>
#include <boost/date_time/posix_time/posix_time.hpp>

#include "SSETickProvider.h"

using namespace std;

namespace bb {

SSETickProvider::SSETickProvider(const ClientContextPtr& context, const instrument_t& instr, source_t source, const std::string& desc)
    : TickProviderImpl(source, instr, desc, true)
    , m_tickSize(0.0)
    , m_contractSize(1.0)
    , m_bInitialized(false)
    , m_bTickReceived(false)
    , m_lastMsgTimeval(0.0)
    , m_subSSEQdMsg(MsgHandler::create<SseHistoricalTickMsg>(source, m_instr, context->getEventDistributor()
            , boost::bind(&SSETickProvider::onSSEQdMsg, this, _1)
            , PRIORITY_CC_Ticks))
    , m_notionalTurnover(0.0)
    , m_notionalAvgPx(0.0)
    , m_openInterest(0)
    , m_lastNotionalTurnover(0.0)
    , m_lastTotalVolume(0)
    , m_aboveVwap(0,0,source)
    , m_belowVwap(0,0,source)
{
}

void SSETickProvider::onSSEQdMsg(const SseHistoricalTickMsg& msg)
{
    if(msg.hdr->source.type() != getSource().type())
        return;

    if(msg.getCode().exp != getInstrument().exp)
        return;

    TradeTick singlePricedTradeTick(PriceSize(msg.getPrice(), msg.getVolume())
        , msg.hdr->source
        , msg.getDateTime()
        , msg.hdr->time_sent
        , 0
        , false
        , MKT_SHSE);

    setTotalVolume(msg.getVolume());
    setLastTimestamps(msg.getDateTime(), msg.hdr->time_sent);

    setLastTradeTick(singlePricedTradeTick);
    m_bTickReceived = true;

    notifyTickReceived();
}

boost::optional<double> SSETickProvider::getAvgPxInLastTick(double lot_size) const
{
    uint32_t vol_change = getTotalVolume() - m_lastTotalVolume;

    mktdest_t mkt = getInstrument().mkt;

    if(mkt == MKT_SHSE)
    {
        return (vol_change != 0 ? getLastPrice() : boost::optional<double>());
    }
    else
    {
        return (vol_change != 0 ?
                (m_notionalTurnover - m_lastNotionalTurnover) / (vol_change * lot_size) :
                boost::optional<double>());
    }
}

}

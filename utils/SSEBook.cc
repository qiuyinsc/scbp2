// Contents Copyright 2011 Athena Capital Research LLC. All Rights Reserved.

#include <boost/bind.hpp>

#include <bb/core/LuabindScripting.h>
#include <bb/core/LuaCodeGen.h>
#include <bb/core/messages.h>

#include <bb/clientcore/BookBuilder.h>
#include <bb/clientcore/ClientContext.h>
#include <bb/clientcore/MsgHandler.h>

#include "SSEBook.h"

using namespace std;

namespace bb {

SseBook::SseBook(const ClientContextPtr& context, const instrument_t& instr, source_t src
        , const std::string& desc, int _vbose)
    : L1Book(instr, src, context, desc, _vbose)
    , m_subSseQdMsg(MsgHandler::create<SseHistoricalSnap1Msg>(src, m_instr, context->getEventDistributor()
            , boost::bind(&SseBook::onSseQdMsg, this, _1)
            , PRIORITY_CC_Book))
    , m_subSseQdMsg2(MsgHandler::create<SseHistoricalSnap2Part1Msg>(src, m_instr, context->getEventDistributor()
            , boost::bind(&SseBook::onSseQdMsg2, this, _1)
            , PRIORITY_CC_Book))
{
}

void SseBook::onSseQdMsg(const SseHistoricalSnap1Msg& msg)
{
    if(msg.hdr->source.type() != getSource().type())
        return;

    if(msg.getCode().exp == getInstrument().exp)
    {
        m_limitUpPrice = msg.getHighPx();
        m_limitDownPrice = msg.getLowPx();

        m_lastChangeTv = m_spCM->getTime();
        m_exchangeTv = msg.getDateTime();

        const sse_historical_depth_level& bestBid = (msg.bid())[0];
        const sse_historical_depth_level& bestAsk = (msg.ask())[0];
        bool bid_changed = setTopLevel(BID, bestBid.getPrice(), bestBid.getSize());
        bool ask_changed = setTopLevel(ASK, bestAsk.getPrice(), bestAsk.getSize());

        if(bid_changed || ask_changed) {
            notifyBookChanged(&msg, (int32_t) (bid_changed ? 0 : -1), (int32_t) (ask_changed ? 0 : -1));
        }
    }
}

void SseBook::onSseQdMsg2(const SseHistoricalSnap2Part1Msg& msg)
{
    if(msg.hdr->source.type() != getSource().type())
        return;

    if(msg.getCode().exp == getInstrument().exp)
    {
        m_limitUpPrice = msg.getHighPx();
        m_limitDownPrice = msg.getLowPx();

        m_lastChangeTv = m_spCM->getTime();
        m_exchangeTv = msg.getDateTime();

        bool bidChanged = false;
        bool askChanged = false;
        for (int i = 0; i < 10; i++) {
            const sse_historical_depth_level& bid = (msg.bid())[i];
            const sse_historical_depth_level& ask = (msg.ask())[i];
            bidChanged = (bidChanged || setLevel(BID, i, bid.getPrice(), bid.getSize()));
            askChanged = (askChanged || setLevel(ASK, i, ask.getPrice(), ask.getSize()));
        }

        if(bidChanged || askChanged) {
            notifyBookChanged(&msg, (int32_t) (bidChanged ? 0 : -1), (int32_t) (askChanged ? 0 : -1));
        }
    }
}

bool SseBook::isOK() const
{
    bool bid_ok = m_topOfBook[BID].isOK()
        || (m_limitDownPrice
            && EQ(m_topOfBook[ASK].getPrice(), m_limitDownPrice.get()));
    bool ask_ok = m_topOfBook[ASK].isOK()
        || (m_limitUpPrice
            && EQ(m_topOfBook[BID].getPrice(), m_limitUpPrice.get()));
    if(bid_ok && ask_ok)
    {
        book_rdy_fg = true;
        return Book::isOK();
    }
    else
        return false;
}

double SseBook::getMidPrice() const
{
    // Bad book
    // BookLevel m_bookLevels[2][10];
    const BookLevel& topBid = m_bookLevels[BID][0];
    const BookLevel& topASK = m_bookLevels[ASK][0];
    if(!topBid.isOK() && !topASK.isOK())
        return 0;

    if(m_limitDownPrice)
    {
        if(!topBid.isOK() && EQ(topASK.getPrice(), m_limitDownPrice.get()))
            return topASK.getPrice();
    }

    if(m_limitUpPrice)
    {
        // Limit up there is only one reported price, the bid.
        if(!topASK.isOK() && EQ(topBid.getPrice(), m_limitUpPrice.get()))
            return topBid.getPrice();
    }

    // Normal 2-sided book
    return (topBid.getPrice() + topASK.getPrice()) / 2.0;
}

} // namespace bb
